<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m180214_095737_alter_table_home_update_fields migration
 */
class m180214_154342_alter_table_city_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%city}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'phone',  $this->string()->null()->comment('city phone'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'phone');
    }
}