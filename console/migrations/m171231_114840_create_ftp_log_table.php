<?php

use console\components\Migration;

/**
 * Class m171231_114840_create_ftp_log_table migration
 */
class m171231_114840_create_ftp_log_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%ftp_log}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'type' => $this->integer()->null()->comment('message type'),
                'message' => $this->text()->null()->comment('message text'),
                'is_show' => $this->boolean()->null(),
                'is_shown' => $this->boolean()->null(),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
