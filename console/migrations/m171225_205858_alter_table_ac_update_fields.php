<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m171224_204946_alter_table_ac_marker_update_fields migration
 */
class m171225_205858_alter_table_ac_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%apartment_complex}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'show_on_home',  $this->boolean()->null()->comment('Show on home'));
        $this->addColumn($this->tableName, 'position_on_home',  $this->integer()->notNull()->defaultValue(0)->comment('Position on home'));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName,'show_on_home');
        $this->dropColumn($this->tableName,'position_on_home');
    }
}