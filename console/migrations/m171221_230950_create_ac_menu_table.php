<?php

use console\components\Migration;

/**
 * Class m171221_230950_create_ac_menu_table migration
 */
class m171221_230950_create_ac_menu_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%ac_menu}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'apartment_complex_id' => $this->integer()->null()->comment('Apartment complex'),
                'label' => $this->string()->notNull()->comment('Label'),
                'alias' => $this->string()->null()->comment('Alias'),
                'link' => $this->string()->null()->comment('Link'),
                'show_right' => $this->boolean()->null()->comment('Show right'),
                'prepared_page_id' => $this->integer()->null()->comment('Prepared page'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-ac_menu-apartment_complex_id-apartment_complex-id',
            $this->tableName,
            'apartment_complex_id',
            '{{%apartment_complex}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-ac_menu-apartment_complex_id-apartment_complex-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
