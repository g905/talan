<?php

use console\components\Migration;

/**
 * Class m190228_111232_add_choose_house_title_to_apartment_complex_table migration
 */
class m190228_111232_add_choose_house_title_to_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'choose_house_title', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumnt($this->table, 'choose_house_title');
    }
}
