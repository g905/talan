<?php

use console\components\Migration;

/**
 * Class m180821_124951_create_partner_request_table migration
 */
class m180821_124951_create_partner_request_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%partner_request}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Name'),
            'phone' => $this->string()->notNull()->comment('Phone'),
            'created_at' => $this->integer()->notNull()->comment('Created At'),
            'updated_at' => $this->integer()->notNull()->comment('Updated At'),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
