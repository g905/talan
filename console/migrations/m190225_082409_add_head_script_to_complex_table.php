<?php

use console\components\Migration;

/**
 * Class m190225_082409_add_head_script_to_complex_table migration
 */
class m190225_082409_add_head_script_to_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'head_script', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'head_script');
    }
}
