<?php

use common\models\Configuration;
use console\components\Migration;

/**
 * Class m180606_095705_add_config_keys migration
 */
class m180606_095705_add_config_keys extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%configuration}}';

    public function safeUp()
    {
        $this->insert($this->table, [
            'id' => 'hypothecQuestionnaireDoc',
            'type' => Configuration::TYPE_FILE,
            'value' => '',
            'description' => 'Document that contains questionnaire blank for hypothec request',
            'created_at' => time(),
            'updated_at' => time(),
        ]);
        $this->insert($this->table, [
            'id' => 'hypothecListOfNeededDocs',
            'type' => Configuration::TYPE_FILE,
            'value' => '',
            'description' => 'Document that contains list of needed docs for hypothec request',
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->table, ['id' => 'hypothecQuestionnaireDoc']);
        $this->delete($this->table, ['id' => 'hypothecListOfNeededDocs']);
    }
}
