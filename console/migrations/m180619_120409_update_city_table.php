<?php

use console\components\Migration;

/**
 * Class m180619_120409_update_city_table migration
 */
class m180619_120409_update_city_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%city}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'show_prices', $this->boolean()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'show_prices');
    }
}
