<?php

use console\components\Migration;

/**
 * Class m180322_144654_create_visit_count_table migration
 */
class m180322_144654_create_visit_count_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%visit_count}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'user_hash' => $this->string()->notNull(),
                'model_class' => $this->string()->notNull(),
                'model_id' => $this->integer()->notNull(),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
