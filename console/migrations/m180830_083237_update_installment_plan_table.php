<?php

use console\components\Migration;

/**
 * Class m180830_083237_update_installment_plan_table migration
 */
class m180830_083237_update_installment_plan_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%installment_plan}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'min_term', $this->integer()->null()->comment('Min Term'));
        $this->addColumn($this->table, 'max_term', $this->integer()->null()->comment('Max Term'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'min_term');
        $this->dropColumn($this->table, 'max_term');
    }
}
