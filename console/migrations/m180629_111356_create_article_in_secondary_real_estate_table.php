<?php

use console\components\Migration;

/**
 * Class m180629_111356_create_article_in_secondary_real_estate_table migration
 */
class m180629_111356_create_article_in_secondary_real_estate_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%article_in_secondary_real_estate}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'secondary_real_estate_id' => $this->integer()->notNull()->comment('Secondary real estate'),
                'article_id' => $this->integer()->notNull()->comment('Article'),
            ],
            $this->tableOptions
        );
        $this->createIndex(
            'idx-a_in_s_r_estate-s_r_estate_id',
            'article_in_secondary_real_estate',
            'secondary_real_estate_id',
            false
        );
        $this->createIndex(
            'idx-a_in_s_r_estate-article_id',
            'article_in_secondary_real_estate',
            'article_id',
            false
        );
        $this->addForeignKey(
            'fk-a_in_s_r_estate-s_r_estate_id-s_r_estate-id',
            $this->tableName,
            'secondary_real_estate_id',
            '{{%secondary_real_estate}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-a_in_secondary_real_estate-a_id-article-id',
            $this->tableName,
            'article_id',
            '{{%article}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-a_in_s_r_estate-s_r_estate_id-s_r_estate-id', $this->tableName);
        $this->dropForeignKey('fk-a_in_secondary_real_estate-a_id-article-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
