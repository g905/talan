<?php

use console\components\Migration;

/**
 * Class m181001_142554_update_apartment_building_table migration
 */
class m181001_142554_update_apartment_building_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_building}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'show_chess', $this->boolean()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'show_chess');
    }
}
