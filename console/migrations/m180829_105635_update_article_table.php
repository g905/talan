<?php

use console\components\Migration;

/**
 * Class m180829_105635_update_article_table migration
 */
class m180829_105635_update_article_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%article}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'show_on_home', $this->smallInteger(1)->unsigned()->notNull()->defaultValue(0)->comment('Show On Home Page'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'show_on_home');
    }
}
