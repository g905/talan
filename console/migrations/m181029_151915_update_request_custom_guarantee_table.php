<?php

use console\components\Migration;

/**
 * Class m181029_151915_update_request_custom_guarantee_table migration
 */
class m181029_151915_update_request_custom_guarantee_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%request_custom_guarantee}}';

    public function safeUp()
    {
        $this->dropColumn($this->table, 'address');
        $this->addColumn($this->table, 'complex', $this->integer()->comment('Complex'));
        $this->addColumn($this->table, 'building_number', $this->string()->comment('Building number'));
        $this->addColumn($this->table, 'apartment_number', $this->string()->comment('Apartment number'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'complex');
        $this->dropColumn($this->table, 'building_number');
        $this->dropColumn($this->table, 'apartment_number');
        $this->addColumn($this->table, 'address', $this->string()->notNull()->comment('Address'));
    }
}
