<?php

use console\components\Migration;

/**
 * Class m180427_013457_create_request_commercial_table migration
 */
class m180427_013457_create_request_commercial_table extends Migration
{
    const FK = 'rcomm-city_id-city-id';
    /**
     * @var string migration table names.
     */
    public $cityTable = '{{%city}}';
    public $table = '{{%request_commercial}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'city_id' => $this->integer()->null()->comment('Associated city'),
                'name' => $this->string()->notNull()->comment('Name'),
                'phone' => $this->string()->notNull()->comment('Phone'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(self::FK, $this->table, 'city_id', $this->cityTable, 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $this->dropForeignKey(self::FK, $this->table);
        $this->dropTable($this->table);
    }
}
