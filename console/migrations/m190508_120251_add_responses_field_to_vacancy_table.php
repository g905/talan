<?php

use console\components\Migration;

/**
 * Class m190508_120251_add_responses_field_to_vacancy_table migration
 */
class m190508_120251_add_responses_field_to_vacancy_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%vacancy}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'responses', 'integer');
        $this->addColumn($this->table, 'detail_description', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'responses');
        $this->dropColumn($this->table, 'detail_description');
    }
}
