<?php

use console\components\Migration;

/**
 * Class m180623_144235_update_ac_advantages_table migration
 */
class m180623_144235_update_ac_advantages_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%ac_advantages}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'apartment_building_id', $this->integer()->after('apartment_complex_id')->null()->comment('Apartment building'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'apartment_building_id');
    }
}
