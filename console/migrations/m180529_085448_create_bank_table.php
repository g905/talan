<?php

use console\components\Migration;

/**
 * Class m180529_085448_create_bank_table migration
 */
class m180529_085448_create_bank_table extends Migration
{
    const PK = 'bth_pk';
    const FK_BANK = 'fk-bth-bank_id-bank-id';
    const FK_HYPOTHEC = 'fk-bth-hypothec_id-hypothec-id';

    /**
     * @var string migration table name.
     */
    public $table = '{{%bank}}';
    public $tableHypothec = '{{%hypothec}}';
    public $tableAssign = '{{%bank_to_hypothec}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'label' => $this->string()->notNull()->comment('Label'),
            'mortgage_rate' => $this->decimal(10, 2)->notNull()->comment('Mortgage rate in percents'),
            'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
        ], $this->tableOptions);

        $this->createTable($this->tableAssign, [
            'bank_id' => $this->integer()->notNull()->comment('Bank'),
            'hypothec_id' => $this->integer()->notNull()->comment('Hypothec'),
            'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
        ], $this->tableOptions);

        $this->addPrimaryKey(self::PK, $this->tableAssign, ['bank_id', 'hypothec_id']);
        $this->addForeignKey(self::FK_BANK, $this->tableAssign, 'bank_id', $this->table, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_HYPOTHEC, $this->tableAssign, 'hypothec_id', $this->tableHypothec, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_HYPOTHEC, $this->tableAssign);
        $this->dropForeignKey(self::FK_BANK, $this->tableAssign);
        $this->dropTable($this->tableAssign);
        $this->dropTable($this->table);
    }
}
