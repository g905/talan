<?php

use console\components\Migration;

/**
 * Class m180919_094249_add_column_apartment_complex migration
 */
class m180919_094249_add_column_apartment_complex extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%apartment_complex}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'submit_apartment_script', $this->text()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'submit_apartment_script');
    }
}
