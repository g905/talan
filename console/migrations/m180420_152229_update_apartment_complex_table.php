<?php

use common\helpers\Property;
use console\components\Migration;

/**
 * Class m180420_152229_update_apartment_complex_table migration
 */
class m180420_152229_update_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'type', 'smallint(6) NOT NULL DEFAULT 0 AFTER id');
        $this->addCommentOnColumn($this->table, 'type', 'Property type');
        $this->getDb()->createCommand()->update($this->table, ['type' => Property::TYPE_RESIDENTIAL], ['type' => 0])->execute();
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'type');
    }
}
