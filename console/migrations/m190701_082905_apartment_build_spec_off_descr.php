<?php
use console\components\Migration;

/**
 * Class m190701_082905_apartment_build_spec_off_descr migration
 */
class m190701_082905_apartment_build_spec_off_descr extends Migration
{

	public function safeUp()
	{
		$this->addColumn('apartment_building', 'special_offer_description', $this->text());
	}

	public function safeDown()
	{
		$this->dropColumn('apartment_building', 'special_offer_description');
		return true;
	}
}
