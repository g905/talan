<?php

use console\components\Migration;

/**
 * Class m190119_200625_add_type_field_to_apartment_building_table migration
 */
class m190119_200625_add_type_field_to_apartment_building_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $tableName = '{{%apartment_building}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'type', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'type');
    }
}
