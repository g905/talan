<?php

use console\components\Migration;

/**
 * Class m180625_094856_create_request_guarantee_table migration
 */
class m180625_094856_create_request_guarantee_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%request_guarantee}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'city_id' => $this->integer()->null()->comment('City'),
                'building_id' => $this->integer()->null()->comment('Building'),
                'problem_type' => $this->integer()->null()->comment('Problem type'),
                'name' => $this->string()->null()->comment('Name'),
                'phone' => $this->string()->null()->comment('Phone'),
                'email' => $this->string()->null()->comment('Email'),
                'problem' => $this->text()->null()->comment('Problem'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-request_guarantee-city_id-city-id',
            $this->table,
            'city_id',
            '{{%city}}',
            'id',
            'SET NULL',
            'SET NULL'
        );

        $this->addForeignKey(
            'fk-request_guarantee-building_id-apartment_building-id',
            $this->table,
            'building_id',
            '{{%apartment_building}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-request_guarantee-city_id-city-id', $this->table);
        $this->dropForeignKey('fk-request_guarantee-building_id-apartment_building-id', $this->table);
        $this->dropTable($this->table);
    }
}
