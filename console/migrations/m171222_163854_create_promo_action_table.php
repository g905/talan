<?php

use console\components\Migration;

/**
 * Class m171222_163854_create_promo_action_table migration
 */
class m171222_163854_create_promo_action_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%promo_action}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'content' => $this->text()->null()->comment('Content'),
                'show_on_home' => $this->boolean()->null()->comment('Show on home'),
                'position_on_home' => $this->integer()->notNull()->defaultValue(0)->comment('Position on home'),
                'manager_id' => $this->integer()->null()->comment('manager'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-promo_action-manager_id-manager-id',
            $this->tableName,
            'manager_id',
            '{{%manager}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-promo_action-manager_id-manager-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
