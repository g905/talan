<?php

use console\components\Migration;

/**
 * Class m180727_094230_update_public_data_widget_table migration
 */
class m180727_094230_update_public_data_widget_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%public_data_widget}}';

    public function safeUp()
    {
        $this->alterColumn($this->table, 'value', $this->text()->null());
    }

    public function safeDown()
    {
        $this->alterColumn($this->table, 'value', $this->string()->null());
    }
}
