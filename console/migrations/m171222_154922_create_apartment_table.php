<?php

use console\components\Migration;

/**
 * Class m171222_154922_create_apartment_table migration
 */
class m171222_154922_create_apartment_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%apartment}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'apartment_complex_id' => $this->integer()->null()->comment('Apartment complex'),
                'label' => $this->string()->notNull()->comment('Label'),
                'rooms_number' => $this->integer()->null()->comment('Rooms number'),
                'total_area' => $this->decimal(10,2)->null()->comment('Total area'),
                'living_space' => $this->decimal(10,2)->null()->comment('Living space'),
                'delivery_date' => $this->date()->null()->comment('Delivery date'),
                'price_from' => $this->decimal(10,2)->null()->comment('Price from'),
                'description_title' => $this->string()->null()->comment('Description title'),
                'description_subtitle' => $this->string()->null()->comment('Description subtitle'),
                'description_text' => $this->text()->null()->comment('Description text'),
                'floor_plan_title' => $this->string()->null()->comment('Floor plan title'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-apartment-apartment_complex_id-apartment_complex-id',
            $this->tableName,
            'apartment_complex_id',
            '{{%apartment_complex}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-apartment-apartment_complex_id-apartment_complex-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
