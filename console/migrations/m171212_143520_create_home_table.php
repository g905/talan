<?php

use console\components\Migration;

/**
 * Class m171212_143520_create_home_table migration
 */
class m171212_143520_create_home_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%home}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'city_id' => $this->integer()->notNull()->comment('City'),
                'top_screen_title' => $this->string()->null()->comment('Top screen title'),
                'top_screen_btn_label' => $this->string()->null()->comment('Top screen btn label'),
                'top_screen_btn_link' => $this->string()->null()->comment('Top screen btn link'),
                'about_company_title' => $this->string()->null()->comment('About company title'),
                'about_company_desc' => $this->text()->null()->comment('About company description'),
                'about_company_link_label' => $this->string()->null()->comment('About company link label'),
                'about_company_link' => $this->string()->null()->comment('About company link'),
                'how_we_build_title' => $this->string()->null()->comment('How we build title'),
                'apartment_complex_title' => $this->string()->null()->comment('Apartment complex title'),
                'apartment_complex_subtitle' => $this->string()->null()->comment('Apartment complex subtitle'),
                'apartment_complex_description' => $this->text()->null()->comment('Apartment complex description'),
                'apartment_complex_link_label' => $this->string()->null()->comment('Apartment complex link label'),
                'promo_title' => $this->string()->null()->comment('Promo title'),
                'promo_link_label' => $this->string()->null()->comment('Promo link label'),
                'review_title' => $this->string()->null()->comment('Review title'),
                'final_cta_title' => $this->string()->null()->comment('Final cta title'),
                'final_cta_description' => $this->text()->null()->comment('Final cta description'),
                'final_cta_btn_label' => $this->string()->null()->comment('Final cta btn label'),
                'final_cta_btn_link' => $this->string()->null()->comment('Final cta btn link'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-home-city_id-city-id',
            $this->tableName,
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-home-city_id-city-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
