<?php

use console\components\Migration;
use \vintage\i18n\models\SourceMessage;
use vintage\i18n\models\Message;

/**
 * Class m191001_094808_g55_backend_translate migration
 */
class m191001_094808_g55_backend_translate extends Migration
{
	// 'back/menu', 'Request for full amount'
	// 'back/menu', 'Request prices', 'Запрос цен'
	// 'back/request-fullamount', 'ID'
	// 'back/request-fullamount', 'City'
	// 'back/request-fullamount', 'Apartment complex'
	// 'back/request-fullamount', 'Name'
	// 'back/request-fullamount', 'Phone'
	// 'back/request-fullamount', 'Created At'
	// 'back/request-fullamount', 'Updated At'


	protected $sourceMessageArr = [
		'Request for full amount' => 'back/menu',
		'Request prices' => 'back/menu',
		'ID' => 'back/request-fullamount',
		'City' => 'back/request-fullamount',
		'Name' => 'back/request-fullamount',
		'Phone' => 'back/request-fullamount',
		'Created At' => 'back/request-fullamount',
		'Updated At' => 'back/request-fullamount',
	];
	protected $messageArr = [
		'Request for full amount' => [
			'ru' => 'Заявки на всю сумму',
		],
		'Request prices' => [
			'ru' => 'Запрос цен',
		],
		'ID' => [
			'ru' => 'ID',
		],
		'City' => [
			'ru' => 'Город',
		],
		'Name' => [
			'ru' => 'Имя',
		],
		'Phone' => [
			'ru' => 'Телефон',
		],
		'Created At' => [
			'ru' => 'Дата создания',
		],
		'Updated At' => [
			'ru' => 'Дата редактирования',
		],
	];

	public function safeUp()
	{
		// id category message
		//SourceMessage
		// id language translation
		//Message

		// переводы страницы оферта
		foreach($this->sourceMessageArr as $oneMessage => $oneCategory) {
			$sm = new SourceMessage();
			$sm->category = $oneCategory;
			$sm->message = $oneMessage;
			if($sm->save()) {

				if(isset($this->messageArr[$oneMessage]) && is_array($this->messageArr[$oneMessage]) && count($this->messageArr[$oneMessage])) {

					foreach($this->messageArr[$oneMessage] as $lang => $oneTranslate) {
						$msg = new Message();
						$msg->id = $sm->id;
						$msg->language = $lang;
						$msg->translation = $oneTranslate;
						$msg->save();
					}
				}
			}
		}

		return true;
	}

	public function safeDown()
	{
		SourceMessage::deleteAll(['in', 'message', array_keys($this->sourceMessageArr)]);

		return true;
	}
}
