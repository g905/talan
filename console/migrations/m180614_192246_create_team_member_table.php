<?php

use console\components\Migration;

/**
 * Class m180614_192246_create_team_member_table migration
 */
class m180614_192246_create_team_member_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%team_member}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'manager_id' => $this->integer()->null()->comment('Manager'),
                'team_id' => $this->integer()->notNull()->comment('Team'),
            ],
            $this->tableOptions
        );
        $this->createIndex(
            'idx-team_member-manager_id',
            'team_member',
            'manager_id',
            false
        );
        $this->createIndex(
            'idx-team_member-team_id',
            'team_member',
            'team_id',
            false
        );
        $this->addForeignKey(
            'fk-team_member-manager_id-manager-id',
            $this->tableName,
            'manager_id',
            '{{%manager}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-team_member-team_id-team-id',
            $this->tableName,
            'team_id',
            '{{%team}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-team_member-manager_id-manager-id', $this->tableName);
        $this->dropForeignKey('fk-team_member-team_id-team-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
