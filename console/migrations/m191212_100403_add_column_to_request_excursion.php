<?php

use console\components\Migration;

/**
 * Class m191212_100403_add_column_to_request_excursion migration
 */
class m191212_100403_add_column_to_request_excursion extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%request_excursion}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'request_type', $this->integer()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'request_type');
    }
}
