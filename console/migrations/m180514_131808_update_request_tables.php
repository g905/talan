<?php

use console\components\Migration;

/**
 * Class m180514_131808_update_request_tables migration
 */
class m180514_131808_update_request_tables extends Migration
{
    /**
     * @var string|string[] migration table names.
     */
    public $complexTable = '{{%apartment_complex}}';
    public $tables = [
        '{{%request_callback}}' => 'rclbk-ac_id-ac-id',
        '{{%request_commercial}}' => 'rcmrcl-ac_id-ac-id',
        '{{%request_custom}}' => 'rcstm-ac_id-ac-id',
        '{{%request_question}}' => 'rqstn-ac_id-ac-id',
        '{{%subscribe_request}}' => 'srqst-ac_id-ac-id',
    ];

    public function safeUp()
    {
        foreach ($this->tables as $table => $fk) {
            $this->addColumn($table, 'ac_id', 'int(11) NULL AFTER id');
            $this->addCommentOnColumn($table, 'ac_id', 'Associated complex');
            $this->addForeignKey($fk, $table, 'ac_id', $this->complexTable, 'id', 'SET NULL', 'CASCADE');
        }
    }

    public function safeDown()
    {
        foreach ($this->tables as $table => $fk) {
            $this->dropForeignKey($fk, $table);
            $this->dropColumn($table, 'ac_id');
        }
    }
}
