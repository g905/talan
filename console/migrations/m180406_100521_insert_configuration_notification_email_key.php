<?php

use console\components\Migration;

/**
 * Class m180406_100521_insert_configuration_notification_email_key migration
 */
class m180406_100521_insert_configuration_notification_email_key extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%configuration}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $time = time();

        $this->insert($this->tableName, [
            'id' => 'notification_emails',
            'type' => \common\models\Configuration::TYPE_TEXT,
            'description' => 'Emails for receiving requests notifications',
            'value' => 'admin@dev.dev',
            'created_at' => $time,
            'updated_at' => $time,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 'notification_emails']);
    }
}
