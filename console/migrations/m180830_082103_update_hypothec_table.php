<?php

use console\components\Migration;

/**
 * Class m180830_082103_update_hypothec_table migration
 */
class m180830_082103_update_hypothec_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%hypothec}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'min_term', $this->integer()->null()->comment('Min Term'));
        $this->addColumn($this->table, 'max_term', $this->integer()->null()->comment('Max Term'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'min_term');
        $this->dropColumn($this->table, 'max_term');
    }
}
