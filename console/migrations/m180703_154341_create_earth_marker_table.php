<?php

use console\components\Migration;

/**
 * Class m180703_154341_create_earth_marker_table migration
 */
class m180703_154341_create_earth_marker_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%earth_marker}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->null()->comment('Label'),
                'earth_id' => $this->integer()->null()->comment('Earth'),
                'marker_type' => $this->integer()->null()->comment('Marker type'),
                'lat' => $this->decimal(13, 8)->null()->comment('Lat'),
                'lng' => $this->decimal(13, 8)->null()->comment('Lng'),
                'description' => $this->text()->null()->comment('Description'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-earth_marker-earth_id-earth-id',
            $this->table,
            'earth_id',
            '{{%earth}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-earth_marker-earth_id-earth-id', $this->table);
        $this->dropTable($this->table);
    }
}
