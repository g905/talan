<?php

use console\components\Migration;

/**
 * Class m171213_221321_create_site_menu_table migration
 */
class m171213_221321_create_site_menu_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%site_menu}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'city_id' => $this->integer()->notNull()->comment('City'),
                'menu_position_id' => $this->integer()->notNull()->comment('Menu position'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-site_menu-city_id-city-id',
            $this->tableName,
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-site_menu-city_id-city-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
