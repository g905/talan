<?php

use console\components\Migration;

/**
 * Class m180828_134110_update_apartment_constructor_price_table migration
 */
class m180828_134110_update_apartment_constructor_price_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_constructor_price}}';

    public function safeUp()
    {
        $this->dropColumn($this->table, 'price');
        $this->addColumn($this->table, 'room_1', $this->integer()->null()->comment('One Room'));
        $this->addColumn($this->table, 'room_2', $this->integer()->null()->comment('Two Rooms'));
        $this->addColumn($this->table, 'room_3', $this->integer()->null()->comment('Three Rooms'));
        $this->addColumn($this->table, 'room_4', $this->integer()->null()->comment('Four Rooms'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'room_1');
        $this->dropColumn($this->table, 'room_2');
        $this->dropColumn($this->table, 'room_3');
        $this->dropColumn($this->table, 'room_4');
        $this->addColumn($this->table, 'price', $this->integer()->null());
    }
}
