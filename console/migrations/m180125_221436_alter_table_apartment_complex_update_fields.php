<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m180125_221436_alter_table_apartment_complex_update_fields migration
 */
class m180125_221436_alter_table_apartment_complex_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%apartment_complex}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'guid',  $this->string()->null()->comment('1C Guid'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'guid');
    }
}