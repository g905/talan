<?php

use console\components\Migration;

/**
 * Class m171218_230008_add_config_key_v2 migration
 */
class m171218_230008_add_config_key_v2 extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%configuration}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 'headerPhone',
            'type' => \common\models\Configuration::TYPE_STRING,
            'value' => '',
            'description' => 'phone in header',
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 'headerPhone']);
    }
}
