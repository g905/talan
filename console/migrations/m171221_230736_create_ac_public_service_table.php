<?php

use console\components\Migration;

/**
 * Class m171221_230736_create_ac_public_service_table migration
 */
class m171221_230736_create_ac_public_service_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%ac_public_service}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'apartment_complex_id' => $this->integer()->null()->comment('apartment complex'),
                'label' => $this->string()->notNull()->comment('Label'),
                'content' => $this->text()->null()->comment('Content'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-ac_public_service-apartment_complex_id-apartment_complex-id',
            $this->tableName,
            'apartment_complex_id',
            '{{%apartment_complex}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-ac_public_service-apartment_complex_id-apartment_complex-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
