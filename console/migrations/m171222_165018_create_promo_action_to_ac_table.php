<?php

use console\components\Migration;

/**
 * Class m171222_165018_create_promo_action_to_ac_table migration
 */
class m171222_165018_create_promo_action_to_ac_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%promo_action_to_ac}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'action_id' => $this->integer()->null()->comment('action'),
                'ac_id' => $this->integer()->null()->comment('apartment complex'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-promo_action_to_ac-action_id-promo_action-id',
            $this->tableName,
            'action_id',
            '{{%promo_action}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-promo_action_to_ac-ac_id-apartment_complex-id',
            $this->tableName,
            'ac_id',
            '{{%apartment_complex}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-promo_action_to_ac-action_id-promo_action-id', $this->tableName);
        $this->dropForeignKey('fk-promo_action_to_ac-ac_id-apartment_complex-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
