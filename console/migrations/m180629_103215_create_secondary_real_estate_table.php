<?php

use console\components\Migration;

/**
 * Class m180629_103215_create_secondary_real_estate_table migration
 */
class m180629_103215_create_secondary_real_estate_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%secondary_real_estate}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'city_id' => $this->integer()->null()->comment('City'),
                'description' => $this->text()->null()->comment('Description'),
                'description_only_desktop' => $this->text()->null()->comment('Description only desktop'),
                'button_text' => $this->string()->null()->comment('Top button text'),
                'conditions_label' => $this->string()->null()->comment('Conditions label'),
                'opportunity_black_label' => $this->string()->null()->comment('Opportunity black label'),
                'opportunity_white_label' => $this->string()->null()->comment('Opportunity white label'),
                'opportunity_description' => $this->text()->null()->comment('Opportunity description'),
                'opportunity_button_text' => $this->string()->null()->comment('Opportunity buton text'),
                'apartments_label' => $this->string()->null()->comment('Apartments label'),
                'steps_label' => $this->string()->null()->comment('Steps label'),
                'steps_description' => $this->text()->null()->comment('Steps description'),
                'steps_link_text' => $this->string()->null()->comment('Steps link text'),
                'steps_link_url' => $this->text()->null()->comment('Steps link url'),
                'articles_label' => $this->string()->null()->comment('Articles label'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-s_r_estate-city_id-city-id',
            $this->tableName,
            'city_id',
            '{{%city}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-s_r_estate-city_id-city-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
