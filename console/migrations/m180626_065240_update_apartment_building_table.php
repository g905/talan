<?php

use console\components\Migration;

/**
 * Class m180626_065240_update_apartment_building_table migration
 */
class m180626_065240_update_apartment_building_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_building}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'articles_label', $this->string()->after('investment_btn_link')->null()->comment('Articles label'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'articles_label');
    }
}
