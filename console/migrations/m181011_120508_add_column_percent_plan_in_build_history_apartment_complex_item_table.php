<?php

use console\components\Migration;

/**
 * Class m181011_120508_add_column_percent_plan_in_build_history_apartment_complex_item_table migration
 */
class m181011_120508_add_column_percent_plan_in_build_history_apartment_complex_item_table extends Migration
{
    /**
     * migration table name
     */
    public $table = '{{%build_history_apartment_complex_item}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'percent_plan', $this->decimal(5,2)->defaultValue(0.00));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'percent_plan');
    }
}
