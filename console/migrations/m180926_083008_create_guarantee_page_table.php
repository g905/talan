<?php

use console\components\Migration;

/**
 * Class m180926_083008_create_guarantee_page_table migration
 */
class m180926_083008_create_guarantee_page_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%guarantee_page}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'manager_id' => $this->integer()->notNull()->comment('Manager'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'city_id' => $this->integer()->notNull()->comment('City'),
                'form_agree' => $this->text()->notNull()->comment('Agree'),
                'alias' => $this->string()->notNull()->comment('Alias'),
                'form_title' => $this->string()->notNull()->comment('Form Title'),
                'form_description' => $this->text()->notNull()->comment('Form Description'),
                'form_onsubmit' => $this->string()->notNull()->comment('Form Onsubmit'),
            ],
            $this->tableOptions
        );
        $this->createIndex(
            'idx-guarantee_page-published',
            'guarantee_page',
            'published',
            false
        );
        $this->createIndex(
            'idx-guarantee_page-alias',
            'guarantee_page',
            'alias',
            true
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
