<?php

use console\components\Migration;

/**
 * Class m180624_230849_create_request_job_table migration
 */
class m180624_230849_create_request_job_table extends Migration
{
    const FK_CITY = 'fk-rj-city_id-city-id';
    const FK_FILE = 'fk-rj-file_id-file-id';

    /**
     * @var string migration table name.
     */
    public $table = '{{%request_job}}';
    public $tableCity = '{{%city}}';
    public $tableFile = '{{%fpm_file}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->null()->comment('City'),
            'file_id' => $this->integer()->null()->comment('File'),
            'name' => $this->string()->null()->comment('Name'),
            'phone' => $this->string()->null()->comment('Phone'),
            'email' => $this->string()->null()->comment('Email'),
            'created_at' => $this->integer()->notNull()->comment('Created at'),
            'updated_at' => $this->integer()->notNull()->comment('Updated at'),
        ], $this->tableOptions);

        $this->addForeignKey(self::FK_CITY, $this->table, 'city_id', $this->tableCity, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey(self::FK_FILE, $this->table, 'file_id', $this->tableFile, 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_FILE, $this->table);
        $this->dropForeignKey(self::FK_CITY, $this->table);
        $this->dropTable($this->table);
    }
}
