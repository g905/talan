<?php

use console\components\Migration;

/**
 * Class m180620_192407_create_partner_page_menu_table migration
 */
class m180620_192407_create_partner_page_menu_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%partner_page_menu}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'partner_page_id' => $this->integer()->notNull()->comment('Partner page'),
                'link' => $this->text()->null()->comment('Link'),
                'show_right' => $this->boolean()->null()->defaultValue(0)->comment('Show right'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-partner_page_menu-partner_page_id-partner_page-id',
            $this->tableName,
            'partner_page_id',
            '{{%partner_page}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-partner_page_menu-partner_page_id-partner_page-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
