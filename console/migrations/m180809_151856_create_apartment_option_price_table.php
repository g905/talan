<?php

use console\components\Migration;

/**
 * Class m180809_151856_create_apartment_option_price_table migration
 */
class m180809_151856_create_apartment_option_price_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_constructor_option_price}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'option' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
