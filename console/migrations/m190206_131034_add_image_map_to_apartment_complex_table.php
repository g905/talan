<?php

use console\components\Migration;

/**
 * Class m190206_131034_add_image_map_to_apartment_complex_table migration
 */
class m190206_131034_add_image_map_to_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $tableName = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'map_image', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName);
    }
}
