<?php

use console\components\Migration;

/**
 * Class m180620_134309_update_block_table migration
 */
class m180620_134309_update_block_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%block}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'sub_title', 'VARCHAR(255) NULL DEFAULT NULL AFTER title');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'sub_title');
    }
}
