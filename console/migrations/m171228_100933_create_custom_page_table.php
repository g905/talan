<?php

use console\components\Migration;

/**
 * Class m171228_100933_create_custom_page_table migration
 */
class m171228_100933_create_custom_page_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%custom_page}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'city_id' => $this->integer()->null()->comment('City'),
                'label' => $this->string()->notNull()->comment('Label'),
                'alias' => $this->string()->notNull()->comment('Alias'),
                'content' => $this->text()->null()->comment('Content'),
                'manager_id' => $this->integer()->null()->comment('Manager'),
                'view_count' => $this->integer()->null()->comment('View count'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-custom_page-city_id-city-id',
            $this->tableName,
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-custom_page-manager_id-manager-id',
            $this->tableName,
            'manager_id',
            '{{%manager}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-custom_page-city_id-city-id', $this->tableName);
        $this->dropForeignKey('fk-custom_page-manager_id-manager-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
