<?php

use console\components\Migration;

/**
 * Class m180712_121036_update_block_table migration
 */
class m180712_121036_update_block_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%block}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'link_name', $this->string()->after('description')->null()->comment('Link name'));
        $this->addColumn($this->table, 'color', $this->string()->after('link')->null()->comment('Color'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'link_name');
        $this->dropColumn($this->table, 'color');
    }
}
