<?php

use console\components\Migration;

/**
 * Class m190109_100004_make_entrance_nullable_in_apartment_entrance_table migration
 */
class m190109_100004_make_entrance_nullable_in_apartment_entrance_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $tableName = '{{%apartment_entrance}}';

    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'entrance', $this->integer()->null());
    }

    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'entrance', $this->integer()->notNull());
    }
}
