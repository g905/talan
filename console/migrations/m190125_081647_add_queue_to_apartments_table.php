<?php

use console\components\Migration;

/**
 * Class m190125_081647_add_queue_to_apartments_table migration
 */
class m190125_081647_add_queue_to_apartments_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $tableName = '{{%apartment}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'queue', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'queue');
    }
}
