<?php

use console\components\Migration;

/**
 * Class m180417_135533_update_apartment_complex_table migration
 */
class m180417_135533_update_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'general_ig_link', 'varchar(255) NULL AFTER general_vk_link');
        $this->addCommentOnColumn($this->table, 'general_ig_link', 'general instagram link');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'general_ig_link');
    }
}
