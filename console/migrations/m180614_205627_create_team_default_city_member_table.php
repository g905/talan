<?php

use console\components\Migration;

/**
 * Class m180614_205627_create_team_default_city_members_table migration
 */
class m180614_205627_create_team_default_city_member_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%team_default_city_member}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'manager_id' => $this->integer()->null()->comment('Manager'),
                'page_id' => $this->integer()->notNull()->comment('Page'),
            ],
            $this->tableOptions
        );
        $this->createIndex(
            'idx-team_default_city_members-manager_id',
            'team_default_city_member',
            'manager_id',
            false
        );
        $this->addForeignKey(
            'fk-team_default_city_members-manager_id-manager-id',
            $this->tableName,
            'manager_id',
            '{{%manager}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-team_default_city_members-manager_id-manager-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
