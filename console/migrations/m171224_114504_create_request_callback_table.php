<?php

use console\components\Migration;

/**
 * Class m171224_114504_create_request_callback_table migration
 */
class m171224_114504_create_request_callback_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%request_callback}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull()->comment('Name'),
                'phone' => $this->string()->notNull()->comment('Phone'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
