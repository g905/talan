<?php

use console\components\Migration;

/**
 * Class m180911_142526_add_index_fpm_file_table migration
 */
class m180911_142526_add_index_fpm_file_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%fpm_file}}';

    public function safeUp()
    {
        $this->createIndex(
            'idx-f_file_estate-base_name',
            'fpm_file',
            'base_name',
            false
        );
    }

    public function safeDown()
    {
        $this->dropIndex(
            'idx-f_file_estate-base_name',
            'fpm_file'
        );
    }
}
