<?php

use console\components\Migration;

/**
 * Class m180109_173737_add_config_key_v4 migration
 */
class m180109_173737_add_config_key_v4 extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%configuration}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 'apartmentsPerPage',
            'type' => \common\models\Configuration::TYPE_INTEGER,
            'value' => '',
            'description' => 'Apartments per page at filter page',
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 'apartmentsPerPage']);
    }
}
