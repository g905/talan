<?php

use console\components\Migration;

/**
 * Class m181031_092216_add_column_build_history_apartment_building_item_table migration
 */
class m181031_092216_add_column_build_history_apartment_building_item_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%build_history_apartment_building_item}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'percent_plan', $this->decimal(5,2)->defaultValue(0.00));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'percent_plan');
    }
}
