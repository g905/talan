<?php

use console\components\Migration;

/**
 * Class m180606_113323_update_new_request_tables migration
 */
class m180606_113323_update_new_request_tables extends Migration
{
    const FK_HYPOTHEC_FILE = 'rh-file_id-file-id';

    /**
     * @var string migration table names.
     */
    public $tableFile = '{{%fpm_file}}';
    public $tableHypothec = '{{%request_hypothec}}';

    public function safeUp()
    {
        $this->addColumn($this->tableHypothec, 'file_id', $this->integer()->null()->comment('File'));
        $this->dropColumn($this->tableHypothec, 'type');
        $this->addForeignKey(self::FK_HYPOTHEC_FILE, $this->tableHypothec, 'file_id', $this->tableFile, 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_HYPOTHEC_FILE, $this->tableHypothec);
        $this->addColumn($this->tableHypothec, 'type', $this->integer()->defaultValue(0)->comment('Request source type'));
        $this->dropColumn($this->tableHypothec, 'file_id');
    }
}
