<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m171222_113600_alter_table_apartment_complex_update_fields migration
 */
class m171222_113600_alter_table_apartment_complex_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%apartment_complex}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'city_id',  $this->integer()->null()->comment('city id'));

        $this->addForeignKey(
            'fk-apartment_complex_city_id_city-id',
            $this->tableName,
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-apartment_complex_city_id_city-id', $this->tableName);
        $this->dropColumn($this->tableName,'city_id');
    }
}