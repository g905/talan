<?php

use console\components\Migration;

/**
 * Class m180423_104320_create_request_custom_table migration
 */
class m180423_104320_create_request_custom_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%request_custom}}';
    /**
     * @var string
     */
    public $cityTable = '{{%city}}';
    /**
     * @var string
     */
    public $customPopupTableName = '{{%custom_popup}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'name' => $this->string()->null()->comment('Name'),
                'phone' => $this->string()->null()->comment('Phone'),
                'email' => $this->string()->null()->comment('Email'),
                'city_id' => $this->integer()->null()->comment('City'),
                'custom_popup_id' => $this->integer()->null()->comment('Popup'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'request_custom-city_id-city-id',
            $this->tableName,
            'city_id',
            $this->cityTable,
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'request_custom-cp_id-cp-id',
            $this->tableName,
            'custom_popup_id',
            $this->customPopupTableName,
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('request_custom-cp_id-cp-id', $this->tableName);
        $this->dropForeignKey('request_custom-city_id-city-id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
