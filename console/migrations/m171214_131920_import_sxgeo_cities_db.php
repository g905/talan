<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 03.10.2017
 * Time: 11:00
 */

use console\components\Migration;

/**
 * Class m171214_131920_import_sxgeo_cities_db migration
 */
class m171214_131920_import_sxgeo_cities_db extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%sxgeo_cities}}';

    public function safeUp()
    {
        $this->execute(file_get_contents(__DIR__ .'/sxgeo_cities.sql'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}