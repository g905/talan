<?php

use console\components\Migration;

/**
 * Class m181205_093632_alter_table_promo_action migration
 */
class m181205_093632_alter_table_promo_action extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%promo_action}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'start_date', $this->integer()->null()->comment('Start Date'));
        $this->addColumn($this->table, 'end_date', $this->integer()->null()->comment('End Date'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'start_date');
        $this->dropColumn($this->table, 'end_date');
    }
}
