<?php

use console\components\Migration;

/**
 * Class m181031_150144_add_column_apartment_complex_table migration
 */
class m181031_150144_add_column_apartment_complex_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%apartment_complex}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'director', $this->string()->null());
        $this->addColumn($this->tableName, 'address', $this->string()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'director');
        $this->dropColumn($this->tableName, 'address');
    }
}
