<?php
use console\components\Migration;

/**
 * Class m190611_085720_form_asc_question_code migration
 */
class m190611_085720_form_asc_question_code extends Migration
{
	public $ascQuestionTitleId = 'formAscQuerstionTitle';

	public function safeUp()
	{
		$this->insert('configuration', [
			'id' => $this->ascQuestionTitleId,
			'value' => 'Оставьте заявку',
			'type' => '0',
			'description' => 'Текст для формы \"Узнать об акции\"',
			'preload' => '0',
			'published' => '1',
			'created_at' => time(),
			'updated_at' => time(),
			'show' => '1',
		]);


	}

	public function safeDown()
	{
		$this->delete('configuration', [
			'id' => $this->ascQuestionTitleId,
		]);
	}
}
