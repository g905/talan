<?php

use console\components\Migration;

/**
 * Class m180629_110454_create_secondary_real_estate_step_table migration
 */
class m180629_110454_create_secondary_real_estate_step_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%secondary_real_estate_step}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'content' => $this->text()->null()->comment('Content'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'secondary_real_estate_id' => $this->integer()->notNull()->comment('Secondary real estate'),
            ],
            $this->tableOptions
        );
        $this->createIndex(
            'idx-s_r_estate_step-real_estate_id',
            'secondary_real_estate_step',
            'secondary_real_estate_id',
            false
        );
        $this->addForeignKey(
            'fk-s_r_estate_step-s_r_estate_id-real_estate-id',
            $this->tableName,
            'secondary_real_estate_id',
            '{{%secondary_real_estate}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-s_r_estate_step-s_r_estate_id-real_estate-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
