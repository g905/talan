<?php

use console\components\Migration;

/**
 * Class m180625_130659_create_request_problem_table migration
 */
class m180625_130659_create_request_problem_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%request_problem}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),

                'label' => $this->string()->notNull()->comment('Label'),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
