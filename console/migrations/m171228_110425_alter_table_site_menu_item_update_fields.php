<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m171228_110425_alter_table_site_menu_item_update_fields migration
 */
class m171228_110425_alter_table_site_menu_item_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%site_menu_item}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'prepared_page_id', $this->string()->null()->comment('Prepared page'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'prepared_page_id', $this->integer()->null()->comment('Prepared page'));
    }
}