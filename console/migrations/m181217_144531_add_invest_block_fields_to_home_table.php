<?php

use console\components\Migration;

/**
 * Class m181217_144531_add_test_column_to_home_table migration
 */
class m181217_144531_add_invest_block_fields_to_home_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%home}}';

    public function safeUp()
    {
        $this->addColumn('home', 'invest_title', $this->string());
        $this->addColumn('home', 'invest_desc', $this->string());
        $this->addColumn('home', 'invest_button_label', $this->string());
        $this->addColumn('home', 'invest_button_link', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('home', 'invest_title');
        $this->dropColumn('home', 'invest_desc');
        $this->dropColumn('home', 'button_label');
        $this->dropColumn('home', 'button_link');
    }
}
