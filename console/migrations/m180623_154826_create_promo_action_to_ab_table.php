<?php

use console\components\Migration;

/**
 * Class m180623_154826_create_promo_action_to_ab_table migration
 */
class m180623_154826_create_promo_action_to_ab_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%promo_action_to_ab}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'action_id' => $this->integer()->null()->comment('Action'),
                'ab_id' => $this->integer()->null()->comment('Apartment building'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-promo_action_to_ab-action_id-promo_action-id',
            $this->table,
            'action_id',
            '{{%promo_action}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-promo_action_to_ab-ab_id-apartment_building-id',
            $this->table,
            'ab_id',
            '{{%apartment_building}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-promo_action_to_ab-action_id-promo_action-id', $this->table);
        $this->dropForeignKey('fk-promo_action_to_ab-ab_id-apartment_building-id', $this->table);
        $this->dropTable($this->table);
    }
}
