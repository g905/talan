<?php

use console\components\Migration;

/**
 * Class m190225_121012_add_guarantee_list_field_to_apartment_complex_table migration
 */
class m190225_121012_add_guarantee_list_field_to_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'guarantee_list', $this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'guarantee_list');
    }
}
