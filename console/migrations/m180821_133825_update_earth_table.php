<?php

use console\components\Migration;

/**
 * Class m180821_133825_update_earth_table migration
 */
class m180821_133825_update_earth_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%earth}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'form_onsubmit', $this->string()->null()->comment('Form on submit'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'form_onsubmit');
    }
}
