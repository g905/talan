<?php

use console\components\Migration;

/**
 * Class m180912_135419_update_completed_project_table migration
 */
class m180912_135419_update_completed_project_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%completed_project}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'url', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'url');
    }
}
