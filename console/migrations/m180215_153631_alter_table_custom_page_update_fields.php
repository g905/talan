<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m180215_153631_alter_table_custom_page_update_fields migration
 */
class m180215_153631_alter_table_custom_page_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%custom_page}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'form_emails',  $this->text()->null()->comment('Form emails'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'form_emails');
    }
}