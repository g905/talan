<?php

use console\components\Migration;

/**
 * Class m180426_132117_update_complex_list_table migration
 */
class m180426_132117_update_complex_list_table extends Migration
{
    /**
     * migration table name
     */
    public $table = '{{%complex_list}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'benefits_title', $this->string()->null()->comment('Benefits title'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'benefits_title');
    }
}
