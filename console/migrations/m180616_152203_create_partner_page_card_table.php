<?php

use console\components\Migration;

/**
 * Class m180616_152203_create_partner_page_card_table migration
 */
class m180616_152203_create_partner_page_card_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%partner_page_card}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'content' => $this->text()->null()->comment('Content'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'link_text' => $this->string()->null()->comment('Link text'),
                'link_url' => $this->text()->null()->comment('Link url'),
                'partner_page_id' => $this->integer()->notNull()->comment('Partner page'),
            ],
            $this->tableOptions
        );
        $this->createIndex(
            'idx-partner_page_card-partner_page_id',
            'partner_page_card',
            'partner_page_id',
            false
        );
        $this->addForeignKey(
            'fk-partner_page_card-partner_page_id-partner_page-id',
            $this->tableName,
            'partner_page_id',
            '{{%partner_page}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-partner_page_card-partner_page_id-partner_page-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
