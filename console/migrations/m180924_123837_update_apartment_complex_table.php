<?php

use console\components\Migration;

/**
 * Class m180924_123837_update_apartment_complex_table migration
 */
class m180924_123837_update_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $tableName = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'show_calc', $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'show_calc');
    }
}
