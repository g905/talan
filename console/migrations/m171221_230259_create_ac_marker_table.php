<?php

use console\components\Migration;

/**
 * Class m171221_230259_create_ac_marker_table migration
 */
class m171221_230259_create_ac_marker_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%ac_marker}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'apartment_complex_id' => $this->integer()->null()->comment('apartment complex'),
                'ac_marker_type_id' => $this->integer()->null()->comment('marker type'),
                'lat' => $this->decimal(13,8)->null()->comment('lat'),
                'lng' => $this->decimal(13,8)->null()->comment('lng'),
                'phone' => $this->string()->null()->comment('phone'),
                'address' => $this->string()->null()->comment('address'),
                'email' => $this->string()->null()->comment('email'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-ac_marker-apartment_complex_id-apartment_complex-id',
            $this->tableName,
            'apartment_complex_id',
            '{{%apartment_complex}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-ac_marker-ac_marker_type_id-ac_marker_type-id',
            $this->tableName,
            'ac_marker_type_id',
            '{{%ac_marker_type}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-ac_marker-apartment_complex_id-apartment_complex-id', $this->tableName);
        $this->dropForeignKey('fk-ac_marker-ac_marker_type_id-ac_marker_type-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
