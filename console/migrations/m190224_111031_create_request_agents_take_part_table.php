<?php

use console\components\Migration;

/**
 * Class m190224_111031_create_request_agents_take_part_table migration
 */
class m190224_111031_create_request_agents_take_part_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%request_agents_take_part}}';

    public $cityTable = '{{%city}}';

    public $fk = 'ratp-city_id-city-id';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull()->comment('Name'),
                'phone' => $this->string()->notNull()->comment('Phone'),
                'email' => $this->string()->notNull()->comment('Email'),
                'agency_name' => $this->string()->notNull()->comment('Agency Name'),
                'city_id' => $this->integer()->comment('City id'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey($this->fk, $this->table, 'city_id', $this->cityTable, 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->fk, $this->table);
        $this->dropTable($this->table);
    }
}
