<?php

use console\components\Migration;

/**
 * Class m180701_195413_create_secondary_real_estate_menu_table migration
 */
class m180701_195413_create_secondary_real_estate_menu_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%secondary_real_estate_menu}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'secondary_real_estate_id' => $this->integer()->notNull()->comment('Secondary real estate'),
                'link' => $this->text()->null()->comment('Link'),
                'show_right' => $this->boolean()->null()->comment('Show right'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-s_r_estate_menu-s_r_estate_id-s_r_estate-id',
            $this->tableName,
            'secondary_real_estate_id',
            '{{%secondary_real_estate}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-s_r_estate_menu-s_r_estate_id-s_r_estate-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
