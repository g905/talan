<?php

use console\components\Migration;

/**
 * Class m190221_093624_create_agents_table migration
 */
class m190221_093624_create_agents_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%agents}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'label' => $this->string()->notNull()->comment('Label'),
            'description' => $this->text()->null()->comment('Description'),
            'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
            'created_at' => $this->integer()->notNull()->comment('Created At'),
            'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            'city_id' => $this->integer()->null()->comment('City'),
            'agents_title' => $this->string()->null()->comment('Agents title'),
            'youtube_link' => $this->string()->null()->comment('Youtube video link'),
            'tab_label' => $this->string()->null()->comment('Tab label'),
        ], $this->tableOptions);
        $this->createIndex(
            'idx-agents-city_id',
            'agents',
            'city_id',
            false
        );
        $this->addForeignKey(
            'fk-agents-city_id-city-id',
            $this->table,
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
}

    public function safeDown()
    {
    	$this->dropForeignKey('fk-agents-city_id-city-id', $this->table);
        $this->dropTable($this->table);
    }
}
