<?php
use console\components\Migration;

class m190515_145909_complex_slider_spec_offer extends Migration
{
	public function safeUp()
	{
		$this->addColumn('apartment_complex', 'special_offer_text', $this->string(255));
		$this->addColumn('apartment_complex', 'special_offer_link', $this->string(255));
	}

	public function safeDown()
	{
		$this->dropColumn('apartment_complex', 'special_offer_text');
		$this->dropColumn('apartment_complex', 'special_offer_link');
		return true;
	}
}
