<?php

use console\components\Migration;

/**
 * Class m180710_160324_create_buy_page_table migration
 */
class m180710_160324_create_buy_page_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%buy_page}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'city_id' => $this->integer()->notNull()->comment('City'),

                'top_screen_title' => $this->string()->null()->comment('Title'),
                'top_screen_description' => $this->text()->null()->comment('Description'),
                'top_screen_btn' => $this->string()->null()->comment('Button Text'),

                'form_top_title' => $this->string()->null()->comment('Form top title'),
                'form_top_emails' => $this->text()->null()->comment('Form top emails'),
                'form_top_square' => $this->integer()->null()->comment('Form Top Square'),
                'form_top_btn' => $this->string()->null()->comment('Form Top Button Text'),
                'form_top_onsubmit' => $this->string()->null()->comment('Form OnSubmit event'),

                'advantages_title' => $this->string()->null()->comment('Title'),

                'steps_title' => $this->string()->null()->comment('Title'),

                'form_bottom_title' => $this->string()->null()->comment('Form bottom title'),
                'form_bottom_description' => $this->text()->null()->comment('Form bottom description'),
                'form_bottom_email' => $this->text()->null()->comment('Form bottom email'),
                'manager_id' => $this->integer()->null()->comment('Manager ID'),
                'form_bottom_onsubmit' => $this->string()->null()->comment('Form OnSubmit event'),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-buy_page-city_id-city-id',
            $this->table,
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-buy_page-city_id-city-id', $this->table);
        $this->dropTable($this->table);
    }
}
