<?php

use console\components\Migration;

/**
 * Class m180809_112050_create_apartment_constructor_price_table migration
 */
class m180809_112050_create_apartment_constructor_price_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_constructor_price}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
