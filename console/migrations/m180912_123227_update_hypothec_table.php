<?php

use console\components\Migration;

/**
 * Class m180912_123227_update_hypothec_table migration
 */
class m180912_123227_update_hypothec_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%hypothec}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'notification_email', $this->text()->null());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'notification_email');
    }
}
