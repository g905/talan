<?php

use console\components\Migration;

/**
 * Class m180423_095924_create_request_custom_table migration
 */
class m180423_095924_create_custom_popup_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%custom_popup}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'white_label' => $this->text()->null()->comment('White title'),
                'black_label' => $this->text()->null()->comment('Black title'),
                'sub_label' => $this->text()->null()->comment('Sub-title'),
                'label_placeholder' => $this->string()->null()->comment('Label placeholder'),
                'phone_placeholder' => $this->string()->null()->comment('Phone placeholder'),
                'email_placeholder' => $this->string()->null()->comment('Email placeholder'),
                'agreement_text' => $this->text()->null()->comment('Agreement text'),
                'button_label' => $this->string()->null()->comment('Send button text'),
                'raise_time' => $this->integer()->notNull()->defaultValue(0)->comment('Time for popup appearing'),
                'url_param_value' => $this->string()->null()->comment('Url param value to show this popup'),
                'is_default' => $this->boolean()->notNull()->defaultValue(false)->comment('Is default popup'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
