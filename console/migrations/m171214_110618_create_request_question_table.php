<?php

use console\components\Migration;

/**
 * Class m171214_110618_create_request_question_table migration
 */
class m171214_110618_create_request_question_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%request_question}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->null()->comment('user name'),
                'phone' => $this->string()->null()->comment('user phone'),
                'email' => $this->string()->null()->comment('user email'),
                'question' => $this->text()->null()->comment('question text'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
