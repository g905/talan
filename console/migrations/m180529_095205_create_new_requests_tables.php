<?php

use console\components\Migration;

/**
 * Class m180529_095205_create_new_requests_tables migration
 */
class m180529_095205_create_new_requests_tables extends Migration
{
    const FK_REQUEST_HYPOTHEC_CITY = 'rh-city_id-city-id';
    const FK_REQUEST_INSTALLMENT_PLAN_CITY = 'rip-city_id-city-id';

    /**
     * @var string migration table name.
     */
    public $tableHypothec = '{{%request_hypothec}}';
    public $tableInstallmentPlan = '{{%request_instalment_plan}}';
    public $cityTable = '{{%city}}';

    public function safeUp()
    {
        $this->createTable($this->tableHypothec, [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->null()->comment('Associated city'),
            'type' => $this->integer()->defaultValue(0)->comment('Request source type'),
            'name' => $this->string()->null()->comment('User name'),
            'phone' => $this->string()->null()->comment('User phone'),
            'email' => $this->string()->null()->comment('User email'),
            'created_at' => $this->integer()->notNull()->comment('Created At'),
            'updated_at' => $this->integer()->notNull()->comment('Updated At'),
        ], $this->tableOptions);

        $this->createTable($this->tableInstallmentPlan, [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->null()->comment('Associated city'),
            'name' => $this->string()->null()->comment('User name'),
            'phone' => $this->string()->null()->comment('User phone'),
            'created_at' => $this->integer()->notNull()->comment('Created At'),
            'updated_at' => $this->integer()->notNull()->comment('Updated At'),
        ], $this->tableOptions);

        $this->addForeignKey(self::FK_REQUEST_HYPOTHEC_CITY, $this->tableHypothec, 'city_id', $this->cityTable, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey(self::FK_REQUEST_INSTALLMENT_PLAN_CITY, $this->tableInstallmentPlan, 'city_id', $this->cityTable, 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_REQUEST_INSTALLMENT_PLAN_CITY, $this->tableInstallmentPlan);
        $this->dropForeignKey(self::FK_REQUEST_HYPOTHEC_CITY, $this->tableHypothec);
        $this->dropTable($this->tableInstallmentPlan);
        $this->dropTable($this->tableHypothec);
    }
}
