<?php

use console\components\Migration;

/**
 * Class m171222_162735_create_how_buy_table migration
 */
class m171222_162735_create_how_buy_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%how_buy}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'city_id' => $this->integer()->null()->comment('City'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-how_buy-city_id-city-id',
            $this->tableName,
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-how_buy-city_id-city-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
