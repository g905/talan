<?php

use console\components\Migration;

/**
 * Class m180425_151354_create_build_history_apartment_complex_item_slide_table migration
 */
class m180425_151354_create_build_history_apartment_complex_item_slide_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%build_history_apartment_complex_item_slide}}';
    public $apartmentComplexItemTableName = '{{build_history_apartment_complex_item}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'build_history_apartment_complex_item_id' => $this->integer()->null()->comment('Build history apartment complex item'),
                'day' => $this->string()->notNull()->comment('Day'),
                'content' => $this->text()->defaultValue(null)->comment('Content'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-bhaci-apartment_complex_item_id-bhapartment_complex-id',
            $this->tableName,
            'build_history_apartment_complex_item_id',
            $this->apartmentComplexItemTableName,
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
//        $this->dropForeignKey('fk-bhaci-apartment_complex_item_id-bhapartment_complex-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
