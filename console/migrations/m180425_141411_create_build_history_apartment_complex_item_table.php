<?php

use console\components\Migration;

/**
 * Class m180425_141411_create_build_history_apartment_complex_item_table migration
 */
class m180425_141411_create_build_history_apartment_complex_item_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%build_history_apartment_complex_item}}';
    /**
     * @var string
     */
    public $apartmentComplexTableName = '{{%apartment_complex}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'apartment_complex_id' => $this->integer()->notNull()->comment('Apartment complex'),
                'date' => $this->date()->notNull()->comment('Date (month + year)'),
                'percent' => $this->decimal(5,2)->defaultValue(0)->comment('Percent of completition'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-bhaci-apartment_complex_id-apartment_complex-id',
            $this->tableName,
            'apartment_complex_id',
            $this->apartmentComplexTableName,
            'id',
            'CASCADE', // we do not need history for removed items
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-bhaci-apartment_complex_id-apartment_complex-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
