<?php

use console\components\Migration;

/**
 * Class m180920_085706_add_column_to_block migration
 */
class m180920_085706_add_column_to_block extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%block}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'youtube_link', $this->string()->null()->comment('Youtube link'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'youtube_link');
    }
}
