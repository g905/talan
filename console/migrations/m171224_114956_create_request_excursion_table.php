<?php

use console\components\Migration;

/**
 * Class m171224_114956_create_request_excursion_table migration
 */
class m171224_114956_create_request_excursion_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%request_excursion}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'apartment_id' => $this->integer()->null()->comment('Apartment'),
                'name' => $this->string()->notNull()->comment('Name'),
                'phone' => $this->string()->notNull()->comment('Phone'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-request_excursion-apartment_id-apartment-id',
            $this->tableName,
            'apartment_id',
            '{{%apartment}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-request_excursion-apartment_id-apartment-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
