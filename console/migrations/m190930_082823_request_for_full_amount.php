<?php

use console\components\Migration;

/**
 * Class m190930_082823_request_for_full_amount migration
 */
class m190930_082823_request_for_full_amount extends Migration
{
	public $table = '{{%request_full_amount}}';
	public $cityTable = '{{%city}}';

	public function safeUp()
	{
		$this->createTable($this->table, [
			'id' => $this->primaryKey(),
			'city_id' => $this->integer(),
			'name' => $this->string(240)->notNull(),
			'phone' => $this->string(240)->notNull(),
			'created_at' => $this->integer()->notNull(),
			'updated_at' => $this->integer()->notNull(),
		], $this->tableOptions);

		$this->addForeignKey('fk-rfa-city-city_id', $this->table, 'city_id', $this->cityTable, 'id', 'SET NULL', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropTable($this->table);
	}
}
