<?php

use console\components\Migration;

/**
 * Class m180521_144539_add_notification_emails_fields_to_city_and_manager_tables migration
 */
class m180521_144539_add_notification_emails_fields_to_city_and_manager_tables extends Migration
{
    /**
     * @var array migration table name.
     */
    public $tables = [
        '{{%city}}',
        '{{%manager}}'
    ];

    public function safeUp()
    {
        foreach ($this->tables as $table) {
            $this->addColumn($table, 'notification_emails', $this->text()->defaultValue(null)->comment('Emails to notify about requests'));
        }
    }

    public function safeDown()
    {
        foreach ($this->tables as $table) {
            $this->dropColumn($table, 'notification_emails');
        }
    }
}
