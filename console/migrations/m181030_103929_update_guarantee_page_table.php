<?php

use console\components\Migration;

/**
 * Class m181030_103929_update_guarantee_page_table migration
 */
class m181030_103929_update_guarantee_page_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%guarantee_page}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'emails', $this->text()->comment('Emails'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'emails');
    }
}
