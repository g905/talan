<?php

use console\components\Migration;

/**
 * Class m180702_102444_update_apartment_building_table migration
 */
class m180702_102444_update_apartment_building_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_building}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'general_in_link', $this->string()->after('general_vk_link')->null()->comment('General inst link'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'general_in_link');
    }
}
