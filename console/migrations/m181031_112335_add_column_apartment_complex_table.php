<?php

use console\components\Migration;

/**
 * Class m181031_112335_add_column_apartment_complex_table migration
 */
class m181031_112335_add_column_apartment_complex_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%apartment_complex}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_sale', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_sale');
    }
}
