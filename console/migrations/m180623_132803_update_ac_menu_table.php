<?php

use console\components\Migration;

/**
 * Class m180623_132803_update_ac_menu_table migration
 */
class m180623_132803_update_ac_menu_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%ac_menu}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'apartment_building_id', $this->integer()->after('apartment_complex_id')->null()->comment('Apartment building'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'apartment_building_id');
    }
}
