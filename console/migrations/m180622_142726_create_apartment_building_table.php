<?php

use console\components\Migration;

/**
 * Class m180622_142726_create_apartment_building_table migration
 */
class m180622_142726_create_apartment_building_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_building}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),

                'building_complex_id' => $this->integer()->null()->comment('Building complex'),

                'label' => $this->string()->notNull()->comment('Label'),

                'top_screen_description' => $this->text()->null()->comment('Top screen description'),
                'top_screen_btn_label' => $this->string()->null()->comment('Top screen button label'),
                'top_screen_btn_link' => $this->string()->null()->comment('Top screen button link'),

                'general_progress_title' => $this->string()->null()->comment('General progress title'),
                'general_progress_percent' => $this->string()->null()->comment('General progress percent'),
                'general_description' => $this->text()->null()->comment('General description'),
                'general_docs_link' => $this->string()->null()->comment('General docs link'),
                'general_docs_link_title' => $this->string()->null()->comment('General_docs link title'),
                'general_fb_link' => $this->string()->null()->comment('General fb link'),
                'general_vk_link' => $this->string()->null()->comment('General vk link'),

                'investment_title_first' => $this->string()->null()->comment('Title first part'),
                'investment_title_second' => $this->string()->null()->comment('Title second part'),
                'investment_description' => $this->text()->null()->comment('Description'),
                'investment_btn_label' => $this->string()->null()->comment('Button label'),
                'investment_btn_link' => $this->string()->null()->comment('Button link'),

                'advantages_title_first_part' => $this->string()->null()->comment('Advantages title first part'),
                'advantages_title_second_part' => $this->string()->null()->comment('Advantages title first second'),

                'form_top_title' => $this->string()->null()->comment('Form top title'),
                'form_top_description' => $this->text()->null()->comment('Form top description'),
                'form_top_emails' => $this->text()->null()->comment('Form top emails'),
                'form_top_onsubmit' => $this->string()->null()->comment('Form OnSubmit event'),

                'form_middle_title_first' => $this->string()->null()->comment('Form middle title first part'),
                'form_middle_title_second' => $this->string()->null()->comment('Form middle title second part'),
                'form_middle_description' => $this->text()->null()->comment('Form middle description'),
                'form_middle_problem' => $this->integer()->null()->comment('Form middle problem type'),
                'form_middle_emails' => $this->text()->null()->comment('Form middle emails'),
                'form_middle_onsubmit' => $this->string()->null()->comment('Form OnSubmit event'),

                'form_bottom_title' => $this->string()->null()->comment('Form bottom title'),
                'form_bottom_description' => $this->text()->null()->comment('Form bottom description'),
                'form_bottom_email' => $this->text()->null()->comment('Form bottom email'),
                'manager_id' => $this->integer()->null()->comment('Manager ID'),
                'form_bottom_onsubmit' => $this->string()->null()->comment('Form OnSubmit event'),

                'view_count' => $this->integer()->notNull()->defaultValue(0)->comment('View count'),
                'guid' => $this->string()->null()->comment('1C Guid'),
                'show_label' => $this->boolean()->defaultValue(true)->comment('Whether to show label'),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-apartment_building-building_complex_id-apartment_complex-id',
            $this->table,
            'building_complex_id',
            '{{%apartment_complex}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-apartment_building-manager_id-manager-id',
            $this->table,
            'manager_id',
            '{{%manager}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-apartment_building-building_complex_id-apartment_complex-id', $this->table);
        $this->dropForeignKey('fk-apartment_building-manager_id-manager-id', $this->table);
        $this->dropTable($this->table);
    }
}
