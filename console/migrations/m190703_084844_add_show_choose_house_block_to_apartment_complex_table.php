<?php

use console\components\Migration;

/**
 * Class m190703_084844_add_show_choose_house_block_to_apartment_complex_table migration
 */
class m190703_084844_add_show_choose_house_block_to_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'show_choose_house_block', $this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'show_choose_house_block');
    }
}
