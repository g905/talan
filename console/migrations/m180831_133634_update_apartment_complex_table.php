<?php

use console\components\Migration;

class m180831_133634_update_apartment_complex_table extends Migration
{
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'show_prices', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'show_prices');
    }
}
