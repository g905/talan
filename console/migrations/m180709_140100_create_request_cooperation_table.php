<?php

use console\components\Migration;

/**
 * Class m180709_140100_create_request_cooperation_table migration
 */
class m180709_140100_create_request_cooperation_table extends Migration
{
    const FK_CITY = 'fk-request_cooperation-city_id-city-id';

    /**
     * @var string migration table name.
     */
    public $table = '{{%request_cooperation}}';
    public $tableCity = '{{%city}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->null(),
            'name' => $this->string()->null(),
            'phone' => $this->string()->null(),
            'email' => $this->string()->null(),
            'company_name' => $this->string()->null(),
            'chief_phone' => $this->string()->null(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey(self::FK_CITY, $this->table, 'city_id', $this->tableCity, 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_CITY, $this->table);
        $this->dropTable($this->table);
    }
}
