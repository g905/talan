<?php

use common\models\Configuration;
use console\components\Migration;

/**
 * Class m180919_095759_add_configuration migration
 */
class m180919_095759_add_configuration extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%configuration}}';

    public function safeUp()
    {
        $this->insert($this->table, [
            'id' => 'defaultPromoOnSubmit',
            'value' => "console.log('default promo submit');",
            'type' => Configuration::TYPE_STRING,
            'description' => 'onsubmit событие на промо-страницах по-умолчанию',
            'preload' => 1,
            'published' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'show' => 1
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->table, ['id' => 'defaultPromoOnSubmit']);
    }
}
