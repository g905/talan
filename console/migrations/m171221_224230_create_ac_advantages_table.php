<?php

use console\components\Migration;

/**
 * Class m171221_224230_create_ac_advantages_table migration
 */
class m171221_224230_create_ac_advantages_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%ac_advantages}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'apartment_complex_id' => $this->integer()->notNull()->comment('apartment complex'),
                'title' => $this->string()->notNull()->comment('title'),
                'description' => $this->text()->null()->comment('descriptioncv'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-ac_advantages-apartment_complex_id-apartment_complex-id',
            $this->tableName,
            'apartment_complex_id',
            '{{%apartment_complex}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-ac_advantages-apartment_complex_id-apartment_complex-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
