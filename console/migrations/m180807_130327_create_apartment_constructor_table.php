<?php

use console\components\Migration;

/**
 * Class m180807_130327_create_apartment_constructor_table migration
 */
class m180807_130327_create_apartment_constructor_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_constructor}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'label' => $this->string()->notNull(),
            'src' => $this->string()->notNull(),
            'preview' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(null),
            'option' => $this->integer()->defaultValue(null),
            'type' => $this->integer()->notNull(),
            'position' => $this->integer()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->batchInsert($this->table, ['label', 'description', 'src', 'preview', 'type', 'option'], [
            [
                'Бежево-розовый',
                'Description',
                '/static/img/vue-constructor/room/walls/beig_pink.png',
                '/static/img/vue-constructor/preview/room/walls/beig_pink.jpg',
                1,
                null
            ],
            [
                'Бежевый',
                'Description',
                '/static/img/vue-constructor/room/walls/beig.png',
                '/static/img/vue-constructor/preview/room/walls/beig.jpg',
                1,
                null
            ],
            [
                'Оливковый',
                'Description',
                '/static/img/vue-constructor/room/walls/olive.png',
                '/static/img/vue-constructor/preview/room/walls/olive.jpg',
                1,
                null
            ],
            [
                'Pозовый',
                'Description',
                '/static/img/vue-constructor/room/walls/pink.png',
                '/static/img/vue-constructor/preview/room/walls/pink.jpg',
                1,
                null
            ],
            [
                'Светло-бежевый',
                'Description',
                '/static/img/vue-constructor/room/walls/light_beige.png',
                '/static/img/vue-constructor/preview/room/walls/light_beig.jpg',
                1,
                null
            ],
            [
                'Симфония',
                'Description',
                '/static/img/vue-constructor/room/walls/simphone.png',
                '/static/img/vue-constructor/preview/room/walls/simphone.jpg',
                1,
                null
            ],
            [
                'Дуб каньон',
                'Description',
                '/static/img/vue-constructor/room/floor/oak_canyon.png',
                '/static/img/vue-constructor/preview/room/floor/oak_canyon.png',
                2,
                null
            ],
            [
                'Дуб меринос',
                'Description',
                '/static/img/vue-constructor/room/floor/oak_merino.png',
                '/static/img/vue-constructor/preview/room/floor/oak_merino.png',
                2,
                null
            ],
            [
                'Дуб пастельный',
                'Description',
                '/static/img/vue-constructor/room/floor/oak_pastel.png',
                '/static/img/vue-constructor/preview/room/floor/oak_pastel.png',
                2,
                null
            ],
            [
                'Дуб роял',
                'Description',
                '/static/img/vue-constructor/room/floor/oak_royal.png',
                '/static/img/vue-constructor/preview/room/floor/oak_royal.png',
                2,
                null
            ],
            [
                'Венге',
                'Description',
                '/static/img/vue-constructor/room/doors/vange.png',
                '/static/img/vue-constructor/preview/room/doors/vange.jpg',
                3,
                null
            ],
            [
                'Дуб беленый',
                'Description',
                '/static/img/vue-constructor/room/doors/oak_white.png',
                '/static/img/vue-constructor/preview/room/doors/oak_white.jpg',
                3,
                null
            ],
            [
                'Миланский орех',
                'Description',
                '/static/img/vue-constructor/room/doors/milan_nut.png',
                '/static/img/vue-constructor/preview/room/doors/milan_nut.jpg',
                3,
                null
            ],
            [
                'Бризер',
                'Description',
                '/static/img/vue-constructor/room/equipment/brizer-shadow.png',
                '/static/img/vue-constructor/preview/room/equipment/brizer-shadow.jpg',
                4,
                3
            ],
            [
                'Видеодо- мофон',
                'Description',
                '/static/img/vue-constructor/room/equipment/videomophone-shadow.png',
                '/static/img/vue-constructor/preview/room/equipment/videomophone-shadow.jpg',
                4,
                2
            ],
            [
                'Кондиционер',
                'Description',
                '/static/img/vue-constructor/room/equipment/conditioner-shadow.png',
                '/static/img/vue-constructor/preview/room/equipment/conditioner-shadow.jpg',
                4,
                1
            ],
            [
                'Бежевый и венге',
                'Description',
                '/static/img/vue-constructor/bathroom/walls/beig.jpg',
                '/static/img/vue-constructor/preview/bathroom/walls/beig.png',
                5,
                null
            ],
            [
                'Бежевый и коричневый',
                'Description',
                '/static/img/vue-constructor/bathroom/walls/beige_brown.jpg',
                '/static/img/vue-constructor/preview/bathroom/walls/beig.png',
                5,
                null
            ],
            [
                'Лимонный и белый',
                'Description',
                '/static/img/vue-constructor/bathroom/walls/lemon.jpg',
                '/static/img/vue-constructor/preview/bathroom/walls/lemon.png',
                5,
                null
            ],
            [
                'Салатный и белый',
                'Description',
                '/static/img/vue-constructor/bathroom/walls/white.jpg',
                '/static/img/vue-constructor/preview/bathroom/walls/white.png',
                5,
                null
            ],
            [
                'Сиреневый и белый',
                'Description',
                '/static/img/vue-constructor/bathroom/walls/lilac.jpg',
                '/static/img/vue-constructor/preview/bathroom/walls/lilac.png',
                5,
                null
            ],
            [
                'Яблочный и белый',
                'Description',
                '/static/img/vue-constructor/bathroom/walls/apple.jpg',
                '/static/img/vue-constructor/preview/bathroom/walls/apple.png',
                5,
                null
            ],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
