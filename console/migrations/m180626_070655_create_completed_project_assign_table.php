<?php

use console\components\Migration;

/**
 * Class m180626_070655_create_completed_project_assign_table migration
 */
class m180626_070655_create_completed_project_assign_table extends Migration
{
    const FK_COMPLETED_PROJECT_ASSIGN_TO_COMPLETED_PROJECT = 'fk-completed_project_assign-completed_project';
    const IX_COMPLETED_PROJECT_ASSIGN = 'ix-completed_project_assign';

    /**
     * @var string migration table name.
     */
    public $table = '{{%completed_project_assign}}';

    public $tableProject = '{{%completed_project}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer(),
            'object_id' => $this->integer(),
            'type' => $this->tinyInteger()->unsigned()->comment('Object type identifier'), // Instead of model class will store constant values.
            'position' => $this->integer(),
        ], $this->tableOptions);

        $this->createIndex(self::IX_COMPLETED_PROJECT_ASSIGN, $this->table, ['project_id', 'object_id', 'type']);
        $this->addForeignKey(self::FK_COMPLETED_PROJECT_ASSIGN_TO_COMPLETED_PROJECT, $this->table, 'project_id', $this->tableProject, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->table, self::FK_COMPLETED_PROJECT_ASSIGN_TO_COMPLETED_PROJECT);
        $this->dropTable($this->table);
    }
}
