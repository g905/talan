<?php

use console\components\Migration;

/**
 * Class m160917_172448_add_column_show_in_configuration_table migration
 */
class m160917_172448_add_column_show_in_configuration_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%configuration}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->tableName,
            'show',
            $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->after('published')->comment('Show in configuration list view')
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'show');
    }
}
