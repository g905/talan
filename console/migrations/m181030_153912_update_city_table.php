<?php

use console\components\Migration;

/**
 * Class m181030_153912_update_city_table migration
 */
class m181030_153912_update_city_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%city}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'link_itunes', $this->string()->comment('Itunes'));
        $this->addColumn($this->table, 'link_play', $this->string()->comment('GPlay'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'link_itunes');
        $this->dropColumn($this->table, 'link_play');
    }
}
