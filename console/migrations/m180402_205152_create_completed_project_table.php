<?php

use console\components\Migration;

/**
 * Class m180402_205152_create_completed_project_table migration
 */
class m180402_205152_create_completed_project_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%completed_project}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'title' => $this->string()->notNull()->comment('Title'),
                'description' => $this->text()->defaultValue(null)->comment('Description'),
                'published' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1)->comment('Published'),
            ],
            $this->tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
