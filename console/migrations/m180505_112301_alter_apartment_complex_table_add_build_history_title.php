<?php

use console\components\Migration;

/**
 * Class m180505_112301_alter_apartment_complex_table_add_build_history_title migration
 */
class m180505_112301_alter_apartment_complex_table_add_build_history_title extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%apartment_complex}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'build_history_title', $this->string()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'build_history_title');
    }
}
