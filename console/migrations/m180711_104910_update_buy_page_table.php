<?php

use console\components\Migration;

/**
 * Class m180711_104910_update_buy_page_table migration
 */
class m180711_104910_update_buy_page_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%buy_page}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'top_screen_sub_title', $this->string()->null()->comment('Sub title')->after('top_screen_title'));
        $this->addColumn($this->table, 'advantages_title_dark', $this->string()->null()->comment('Title dark')->after('advantages_title'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'top_screen_sub_title');
        $this->dropColumn($this->table, 'advantages_title_dark');
    }
}
