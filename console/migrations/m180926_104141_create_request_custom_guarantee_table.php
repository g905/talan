<?php

use console\components\Migration;

/**
 * Class m180926_104141_create_request_custom_guarantee_table migration
 */
class m180926_104141_create_request_custom_guarantee_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%request_custom_guarantee}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'name' => $this->string()->notNull()->comment('Name'),
                'phone' => $this->string()->notNull()->comment('Phone'),
                'email' => $this->string()->notNull()->comment('Email'),
                'city' => $this->integer()->notNull()->comment('City'),
                'address' => $this->string()->notNull()->comment('Address'),
                'comment' => $this->text()->notNull()->comment('Comment'),
                'file_id' => $this->integer()->comment('File Id'),
                'pdf_id' => $this->text()->comment('PDF Id'),
            ],
            $this->tableOptions
        );
        $this->createIndex(
            'idx-request_custom_guarantee-created_at',
            'request_custom_guarantee',
            'created_at',
            false
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
