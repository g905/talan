<?php

use console\components\Migration;

/**
 * Class m180427_062711_update_apartment_table migration
 */
class m180427_062711_update_apartment_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'profile', $this->string()->null()->comment('Profile'));
        $this->addColumn($this->table, 'income', $this->string()->null()->comment('Income'));
        $this->addColumn($this->table, 'recoupment', $this->string()->null()->comment('Recoupment'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
