<?php

use console\components\Migration;

/**
 * Class m180625_150429_update_apartment_building_table migration
 */
class m180625_150429_update_apartment_building_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_building}}';

    public function safeUp()
    {
        $this->addForeignKey(
            'fk-apartment_building-form_middle_problem-request_problem-id',
            $this->table,
            'form_middle_problem',
            '{{%request_problem}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-apartment_building-form_middle_problem-request_problem-id', $this->table);
    }
}
