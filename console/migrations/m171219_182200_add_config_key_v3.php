<?php

use console\components\Migration;

/**
 * Class m171219_182200_add_config_key_v3 migration
 */
class m171219_182200_add_config_key_v3 extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%configuration}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 'vkPageLink',
            'type' => \common\models\Configuration::TYPE_STRING,
            'value' => '',
            'description' => 'vk page link',
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert($this->tableName, [
            'id' => 'fbPageLink',
            'type' => \common\models\Configuration::TYPE_STRING,
            'value' => '',
            'description' => 'facebook page link',
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert($this->tableName, [
            'id' => 'okPageLink',
            'type' => \common\models\Configuration::TYPE_STRING,
            'value' => '',
            'description' => 'odnoklassniki page link',
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert($this->tableName, [
            'id' => 'instagramPageLink',
            'type' => \common\models\Configuration::TYPE_STRING,
            'value' => '',
            'description' => 'instagram page link',
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 'vkPageLink']);
        $this->delete($this->tableName, ['id' => 'fbPageLink']);
        $this->delete($this->tableName, ['id' => 'okPageLink']);
        $this->delete($this->tableName, ['id' => 'instagramPageLink']);
    }
}
