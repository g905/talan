<?php

use console\components\Migration;

/**
 * Class m180515_155000_create_city_to_article_table migration
 */
class m180515_155000_create_city_to_article_table extends Migration
{
    const FK_CITY = 'fk-cta-city_id-city-id';
    const FK_ARTICLE = 'fk-cta-article_id-article-id';

    /**
     * @var string migration table names.
     */
    public $table = '{{%city_to_article}}';
    public $tableCity = '{{%city}}';
    public $tableArticle = '{{%article}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull()->comment('City'),
            'article_id' => $this->integer()->notNull()->comment('Article'),
        ], $this->tableOptions);

        $this->addForeignKey(self::FK_CITY, $this->table, 'city_id', $this->tableCity, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_ARTICLE, $this->table, 'article_id', $this->tableArticle, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_ARTICLE, $this->table);
        $this->dropForeignKey(self::FK_CITY, $this->table);
        $this->dropTable($this->table);
    }
}
