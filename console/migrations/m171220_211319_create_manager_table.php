<?php

use console\components\Migration;

/**
 * Class m171220_211319_create_manager_table migration
 */
class m171220_211319_create_manager_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%manager}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'fio' => $this->string()->notNull()->comment('Fio'),
                'job_position' => $this->string()->null()->comment('Job position'),
                'phone' => $this->string()->null()->comment('phone'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
