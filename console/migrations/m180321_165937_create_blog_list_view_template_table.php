<?php

use console\components\Migration;

/**
 * Class m180321_165937_create_blog_list_view_template_table migration
 */
class m180321_165937_create_blog_list_view_template_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%blog_list_view_template}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'item_type_id' => $this->integer()->notNull(),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
