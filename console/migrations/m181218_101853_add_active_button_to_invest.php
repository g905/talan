<?php

use console\components\Migration;

/**
 * Class m181218_101853_add_active_button_to_invest migration
 */
class m181218_101853_add_active_button_to_invest extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%home}}';

    public function safeUp()
    {
        $this->addColumn('home', 'invest_active', $this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn('home', 'invest_active');
    }
}
