<?php

use console\components\Migration;

/**
 * Class m180616_145958_create_partner_page_table migration
 */
class m180616_145958_create_partner_page_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%partner_page}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'city_id' => $this->integer()->null()->comment('City'),
                'manager_id' => $this->integer()->null()->comment('Manager'),
                'form_bottom_title' => $this->string()->null()->comment('Forn title'),
                'form_bottom_description' => $this->text()->null()->comment('Form description'),
                'form_onsubmit' => $this->string()->null()->comment('Form onsubmit'),
            ],
            $this->tableOptions
        );
        $this->createIndex(
            'idx-partner_page-city_id',
            'partner_page',
            'city_id',
            false
        );
        $this->createIndex(
            'idx-partner_page-manager_id',
            'partner_page',
            'manager_id',
            false
        );
        $this->addForeignKey(
            'fk-partner_page-city_id-city-id',
            $this->tableName,
            'city_id',
            '{{%city}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-partner_page-manager_id-manager-id',
            $this->tableName,
            'manager_id',
            '{{%manager}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-partner_page-city_id-city-id', $this->tableName);
        $this->dropForeignKey('fk-partner_page-manager_id-manager-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
