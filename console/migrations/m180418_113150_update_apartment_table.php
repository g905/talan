<?php

use console\components\Migration;

/**
 * Class m180418_113150_update_apartment_table migration
 */
class m180418_113150_update_apartment_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'entrance', 'smallint(6) NOT NULL DEFAULT 0 AFTER label');
        $this->addColumn($this->table, 'subtype', 'smallint(6) NOT NULL DEFAULT 0 AFTER label');
        $this->addColumn($this->table, 'type', 'smallint(6) NOT NULL DEFAULT 0 AFTER label');
        $this->addCommentOnColumn($this->table, 'entrance', 'Entrance number');
        $this->addCommentOnColumn($this->table, 'subtype', 'Object subtype');
        $this->addCommentOnColumn($this->table, 'type', 'Object type');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'entrance');
        $this->dropColumn($this->table, 'subtype');
        $this->dropColumn($this->table, 'type');
    }
}
