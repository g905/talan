<?php

use console\components\Migration;

/**
 * Class m190202_134847_add_is_secondary_to_complex_table migration
 */
class m190202_134847_add_is_secondary_to_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $tableName = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_secondary', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_secondary');
    }
}
