<?php

use console\components\Migration;

/**
 * Class m190402_083655_add_choose_house_subtitle_to_apartment_complex migration
 */
class m190402_083655_add_choose_house_subtitle_to_apartment_complex extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'choose_house_subtitle', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'choose_house_subtitle');
    }
}
