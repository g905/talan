<?php

use console\components\Migration;

/**
 * Class m180504_095229_alter_apartment_complex_table_add_build_history_subtext migration
 */
class m180504_095229_alter_apartment_complex_table_add_build_history_subtext extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%apartment_complex}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'build_history_end_text', $this->string()->null());
        $this->addColumn($this->tableName, 'build_history_current_text', $this->string()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'build_history_current_text');
        $this->dropColumn($this->tableName, 'build_history_end_text');
    }
}
