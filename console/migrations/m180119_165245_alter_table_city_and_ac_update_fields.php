<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m180119_165245_alter_table_city_and_ac_update_fields migration
 */
class m180119_165245_alter_table_city_and_ac_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableCity = '{{%city}}';
    public $tableAc = '{{%apartment_complex}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableCity, 'common_script',  $this->text()->null()->comment('Common script'));
        $this->addColumn($this->tableAc, 'common_script',  $this->text()->null()->comment('Common script'));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableCity,'common_script');
        $this->dropColumn($this->tableAc,'common_script');
    }
}