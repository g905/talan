<?php

use console\components\Migration;

/**
 * Class m171223_220932_create_analytics_summary_table migration
 */
class m171223_220932_create_analytics_summary_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%analytics_summary}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'date' => $this->date()->null(),
                'visit_count' => $this->integer()->notNull()->defaultValue(0),
                'unique_visit_count' => $this->integer()->notNull()->defaultValue(0),
                'apartment_complex_view_count' => $this->integer()->notNull()->defaultValue(0),
                'apartment_view_count' => $this->integer()->notNull()->defaultValue(0),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
