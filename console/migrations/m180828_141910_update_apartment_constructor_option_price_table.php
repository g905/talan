<?php

use console\components\Migration;

/**
 * Class m180828_141910_update_apartment_constructor_option_price_table migration
 */
class m180828_141910_update_apartment_constructor_option_price_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_constructor_option_price}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'published', $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'published');
    }
}
