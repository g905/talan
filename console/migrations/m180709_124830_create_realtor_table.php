<?php

use console\components\Migration;

/**
 * Class m180709_124830_create_realtor_table migration
 */
class m180709_124830_create_realtor_table extends Migration
{
    const FK_CITY = 'fk-realtor-city_id-city-id';
    const FK_MANAGER = 'fk-realtor-manager_id-manager-id';

    /**
     * @var string migration table name.
     */
    public $table = '{{%realtor}}';
    public $tableCity = '{{%city}}';
    public $tableManager = '{{%manager}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->null(),

            'top_label' => $this->string()->notNull(),
            'top_description' => $this->text()->null(),
            'top_link_label' => $this->string()->null(),
            'top_btn_label' => $this->string()->null(),

            'benefits_label' => $this->string()->null(),
            'benefits_description' => $this->text()->null(),

            'about_label' => $this->string()->null(),
            'about_description' => $this->text()->null(),

            'partners_label' => $this->string()->null(),

            'app_label' => $this->string()->null(),
            'app_description' => $this->text()->null(),
            'app_link_label' => $this->string()->null(),
            'app_btn_ios_label' => $this->string()->null(),
            'app_btn_ios_link' => $this->string()->null(),
            'app_btn_android_label' => $this->string()->null(),
            'app_btn_android_link' => $this->string()->null(),

            'form_label' => $this->string()->null(),
            'form_onsubmit' => $this->string()->null(),
            'form_description' => $this->text()->null(),
            'form_agreement' => $this->text()->null(),
            'manager_id' => $this->integer()->null(),

            'files_label' => $this->string()->null(),

            'view_count' => $this->integer()->defaultValue(0),
            'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'position' => $this->integer()->notNull()->defaultValue(0),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey(self::FK_CITY, $this->table, 'city_id', $this->tableCity, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey(self::FK_MANAGER, $this->table, 'manager_id', $this->tableManager, 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_CITY, $this->table);
        $this->dropForeignKey(self::FK_MANAGER, $this->table);
        $this->dropTable($this->table);
    }
}
