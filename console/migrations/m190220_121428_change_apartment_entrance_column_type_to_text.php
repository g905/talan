<?php

use console\components\Migration;

/**
 * Class m190220_121428_change_apartment_entrance_column_type_to_text migration
 */
class m190220_121428_change_apartment_entrance_column_type_to_text extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment}}';

    public function safeUp()
    {
        $this->alterColumn($this->table, 'entrance', $this->string());
    }

    public function safeDown()
    {
        $this->alterColumn($this->table, 'entrance', $this->integer());
    }
}
