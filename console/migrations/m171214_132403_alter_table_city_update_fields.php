<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m171214_132403_alter_table_city_update_fields migration
 */
class m171214_132403_alter_table_city_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%city}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'sxgeo_city_id',  $this->integer()->null()->comment('sxgeo city'));

        $this->addForeignKey(
            'fk-city-sxgeo_city_id_sxgeo_cities-id',
            $this->tableName,
            'sxgeo_city_id',
            '{{%sxgeo_cities}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-city-sxgeo_city_id_sxgeo_cities-id', $this->tableName);
        $this->dropColumn($this->tableName,'sxgeo_city_id');
    }
}