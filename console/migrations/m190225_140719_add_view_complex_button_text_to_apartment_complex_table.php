<?php

use console\components\Migration;

/**
 * Class m190225_140719_add_view_complex_button_text_to_apartment_complex_table migration
 */
class m190225_140719_add_view_complex_button_text_to_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'button_view_complex_text', $this->string());
        $this->addColumn($this->table, 'button_view_complex_link', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'button_view_complex_text');
        $this->dropColumn($this->table, 'button_view_complex_link');
    }
}
