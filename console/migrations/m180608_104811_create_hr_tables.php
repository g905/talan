<?php

use console\components\Migration;

/**
 * Class m180608_104811_create_hr_tables migration
 */
class m180608_104811_create_hr_tables extends Migration
{
    const FK_CAREER_CITY = 'fk-career-city_id-city-id';
    const FK_CAREER_MANAGER = 'fk-career-manager_id-manager-id';

    const FK_SPECIALIZATION_CITY = 'fk-specialization-city_id-city-id';
    const FK_SPECIALIZATION_MANAGER = 'fk-specialization-manager_id-manager-id';

    const FK_VACANCIES_PAGE_CITY = 'fk-vacancies_page-city_id-city-id';
    const FK_VACANCIES_PAGE_MANAGER = 'fk-vacancies_page-manager_id-manager-id';

    const FK_VACANCY_CITY = 'fk-vacancy-city_id-city-id';
    const FK_VACANCY_MANAGER = 'fk-vacancy-manager_id-manager-id';
    const FK_VACANCY_SPECIALIZATION = 'fk-vacancy-specialization_id-specialization-id';

    const FK_ARTICLE_ASSIGN_TO_ARTICLE = 'fk-article_assign-article';
    const IX_ARTICLE_ASSIGN = 'ix-article_assign';

    /**
     * @var string migration table names.
     */
    public $tableCareer = '{{%career}}';
    public $tableSpecialization = '{{%specialization}}';
    public $tableVacancyPage = '{{%vacancies_page}}';
    public $tableVacancy = '{{%vacancy}}';
    public $tableArticleAssign = '{{%article_assign}}';
    /**
     * @var string migration ref tables.
     */
    public $tableCity = '{{%city}}';
    public $tableManager = '{{%manager}}';
    public $tableArticle = '{{%article}}';

    public function safeUp()
    {
        $this->createTable($this->tableCareer, merge([
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->null(),
            'top_label' => $this->string(),
            'top_description' => $this->text(),
            'top_btn_label' => $this->string(),
            'top_btn_link' => $this->string(),
            'specializations_label' => $this->string(),
            'director_name' => $this->string(),
            'director_post' => $this->string(),
            'director_text' => $this->text(),
            'reviews_label' => $this->string(),
            'articles_label' => $this->string(),
            'articles_btn_label' => $this->string(),
        ], $this->defaultFields()), $this->tableOptions);

        $this->createTable($this->tableSpecialization, merge([
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->null(),
            'top_label' => $this->string(),
            'top_description' => $this->text(),
            'top_btn_label' => $this->string(),
            'top_btn_link' => $this->string(),
            'details_label' => $this->string(),
            'details_description' => $this->text(),
            'mission_label' => $this->string(),
            'mission_sub_label' => $this->string(),
            'principles_label' => $this->string(),
            'gallery_label' => $this->string(),
            'gallery_description' => $this->text(),
            'articles_label' => $this->string(),
            'articles_btn_label' => $this->string(),
            'reviews_label' => $this->string(),
            'vacancies_label' => $this->string(),
            'vacancies_btn_label' => $this->string(),
            'color' => $this->string(),
        ], $this->defaultFields()), $this->tableOptions);

        $this->createTable($this->tableVacancyPage, merge([
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->null(),
            'label' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
        ], $this->defaultFields()), $this->tableOptions);

        $this->createTable($this->tableVacancy, merge([
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->null(),
            'specialization_id' => $this->integer()->null(),
            'label' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
            'description' => $this->string(),
            'related_label' => $this->string()->null(),
            'responsibilities_label' => $this->string()->null(),
            'responsibilities_content' => $this->text()->null(),
            'requirements_label' => $this->string()->null(),
            'requirements_content' => $this->text()->null(),
            'benefits_label' => $this->string()->null(),
            'benefits_content' => $this->text()->null(),
            'faq_label' => $this->string()->null(),
            'principles_label' => $this->string()->null(),
        ], $this->defaultFields()), $this->tableOptions);

        $this->createTable($this->tableArticleAssign, [
            'id' => $this->primaryKey(),
            'article_id' => $this->integer(),
            'object_id' => $this->integer(),
            'type' => $this->tinyInteger()->unsigned()->comment('Object type identifier'), // Instead of model class will store constant values.
            'position' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(self::FK_CAREER_CITY, $this->tableCareer, 'city_id', $this->tableCity, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey(self::FK_CAREER_MANAGER, $this->tableCareer, 'manager_id', $this->tableManager, 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey(self::FK_SPECIALIZATION_CITY, $this->tableSpecialization, 'city_id', $this->tableCity, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey(self::FK_SPECIALIZATION_MANAGER, $this->tableSpecialization, 'manager_id', $this->tableManager, 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey(self::FK_VACANCIES_PAGE_CITY, $this->tableVacancyPage, 'city_id', $this->tableCity, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey(self::FK_VACANCIES_PAGE_MANAGER, $this->tableVacancyPage, 'manager_id', $this->tableManager, 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey(self::FK_VACANCY_CITY, $this->tableVacancy, 'city_id', $this->tableCity, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey(self::FK_VACANCY_MANAGER, $this->tableVacancy, 'manager_id', $this->tableManager, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey(self::FK_VACANCY_SPECIALIZATION, $this->tableVacancy, 'specialization_id', $this->tableSpecialization, 'id', 'SET NULL', 'CASCADE');

        $this->createIndex(self::IX_ARTICLE_ASSIGN, $this->tableArticleAssign, ['article_id', 'object_id', 'type']);
        $this->addForeignKey(self::FK_ARTICLE_ASSIGN_TO_ARTICLE, $this->tableArticleAssign, 'article_id', $this->tableArticle, 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_ARTICLE_ASSIGN_TO_ARTICLE, $this->tableArticleAssign);

        $this->dropForeignKey(self::FK_VACANCY_CITY, $this->tableVacancy);
        $this->dropForeignKey(self::FK_VACANCY_MANAGER, $this->tableVacancy);
        $this->dropForeignKey(self::FK_VACANCY_SPECIALIZATION, $this->tableVacancy);

        $this->dropForeignKey(self::FK_VACANCIES_PAGE_CITY, $this->tableVacancyPage);
        $this->dropForeignKey(self::FK_VACANCIES_PAGE_MANAGER, $this->tableVacancyPage);

        $this->dropForeignKey(self::FK_SPECIALIZATION_CITY, $this->tableSpecialization);
        $this->dropForeignKey(self::FK_SPECIALIZATION_MANAGER, $this->tableSpecialization);

        $this->dropForeignKey(self::FK_CAREER_CITY, $this->tableCareer);
        $this->dropForeignKey(self::FK_CAREER_MANAGER, $this->tableCareer);

        $this->dropTable($this->tableCareer);
        $this->dropTable($this->tableSpecialization);
        $this->dropTable($this->tableVacancyPage);
        $this->dropTable($this->tableVacancy);
        $this->dropTable($this->tableArticleAssign);
    }

    private function defaultFields()
    {
        return [
            'manager_id' => $this->integer()->null(),
            'form_label' => $this->string()->null(),
            'form_description' => $this->text()->null(),
            'form_agreement' => $this->string()->null(),
            'form_emails' => $this->text(),
            'form_onsubmit' => $this->string(),
            'view_count' => $this->integer()->defaultValue(0),
            'position' => $this->integer(),
            'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ];
    }
}
