<?php

use console\components\Migration;

/**
 * Class m190513_135542_add_content_field_to_article_table migration
 */
class m190513_135542_add_content_field_to_article_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%article}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'article_content', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'article_content');
    }
}
