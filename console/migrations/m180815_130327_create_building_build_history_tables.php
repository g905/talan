<?php

use console\components\Migration;

/**
 * Class m180815_130327_create_building_build_history_tables migration
 */
class m180815_130327_create_building_build_history_tables extends Migration
{
    const FK_ITEM = 'fk-bhabi-apartment_building_id-apartment_building-id';
    const FK_SLIDE = 'fk-bhabi-bhabii-bhabis-id';

    public $buildingTable = '{{%apartment_building}}';
    public $itemTable = '{{%build_history_apartment_building_item}}';
    public $slideTable = '{{%build_history_apartment_building_item_slide}}';

    public function safeUp()
    {
        $this->addColumn($this->buildingTable, 'build_video', $this->text()->null());
        $this->addColumn($this->buildingTable, 'build_start_date', $this->date()->null());
        $this->addColumn($this->buildingTable, 'build_end_date', $this->date()->null());
        $this->addColumn($this->buildingTable, 'build_history_title', $this->string()->null());
        $this->addColumn($this->buildingTable, 'build_history_end_text', $this->string()->null());
        $this->addColumn($this->buildingTable, 'build_history_current_text', $this->string()->null());

        $this->createTable($this->itemTable, [
            'id' => $this->primaryKey(),
            'apartment_building_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'percent' => $this->decimal(5,2)->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createTable($this->slideTable, [
            'id' => $this->primaryKey(),
            'build_history_apartment_building_item_id' => $this->integer()->null(),
            'day' => $this->string()->notNull(),
            'content' => $this->text()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey(self::FK_ITEM, $this->itemTable, 'apartment_building_id', $this->buildingTable, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_SLIDE, $this->slideTable, 'build_history_apartment_building_item_id', $this->itemTable, 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_SLIDE, $this->slideTable);
        $this->dropForeignKey(self::FK_ITEM, $this->itemTable);

        $this->dropTable($this->slideTable);
        $this->dropTable($this->itemTable);

        $this->dropColumn($this->buildingTable, 'build_video');
        $this->dropColumn($this->buildingTable, 'build_start_date');
        $this->dropColumn($this->buildingTable, 'build_end_date');
        $this->dropColumn($this->buildingTable, 'build_history_title');
        $this->dropColumn($this->buildingTable, 'build_history_end_text');
        $this->dropColumn($this->buildingTable, 'build_history_current_text');
    }
}
