<?php

use console\components\Migration;

/**
 * Class m171213_125706_create_about_company_values_table migration
 */
class m171213_125706_create_about_company_values_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%about_company_values}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'about_company_id' => $this->integer()->null()->comment('About company'),
                'label' => $this->string()->notNull()->comment('Label'),
                'content' => $this->text()->null()->comment('Content'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-about_company_values-about_company_id-about_company-id',
            $this->tableName,
            'about_company_id',
            '{{%about_company}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-about_company_values-about_company_id-about_company-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
