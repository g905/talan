<?php

use console\components\Migration;

/**
 * Class m180426_124725_create_benefit_to_complex_list_table migration
 */
class m180426_124725_create_benefit_to_complex_list_table extends Migration
{
    /**
     * @var string PK, FK names.
     */
    const PK = 'bi_cli_pk';
    const FK_CL = 'btcl_cli_cl-id-fk';
    const FK_CP = 'btcl_bi_b-id-fk';
    /**
     * @var string migration table name.
     */
    public $table = '{{%benefit_to_complex_list}}';
    public $tableComplexList = '{{%complex_list}}';
    public $tableBenefit = '{{%benefit}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'complex_list_id' => $this->integer()->notNull()->comment('Complex list ID'),
                'benefit_id' => $this->integer()->notNull()->comment('Benefit ID'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
            ],
            $this->tableOptions
        );

        $this->addPrimaryKey(self::PK, $this->table, ['complex_list_id', 'benefit_id']);
        $this->addForeignKey(self::FK_CL, $this->table, 'complex_list_id', $this->tableComplexList, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_CP, $this->table, 'benefit_id', $this->tableBenefit, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_CP, $this->table);
        $this->dropForeignKey(self::FK_CL, $this->table);
        $this->dropTable($this->table);
    }
}
