<?php

use console\components\Migration;

/**
 * Class m190613_090328_create_view_count_table migration
 */
class m190613_090328_create_view_count_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table1 = '{{%view_count}}';
    public $table2 = '{{%view_item}}';

    public function safeUp()
    {
        $this->createTable($this->table1, [
            'id' => $this->primaryKey(),
            'ip' => $this->integer(),
            'item_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->createIndex('vct_ip', $this->table1, 'ip');
        $this->createIndex('vc_item_id', $this->table1, 'item_id');
        $this->createIndex('vc_created', $this->table1, 'created_at');

        $this->createTable($this->table2, [
            'id' => $this->primaryKey(),
            'item' => $this->string(),
            'item_index' => $this->integer(),
            'period' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ], $this->tableOptions);

        $this->createIndex('vi_item', $this->table2, 'item');
        $this->createIndex('vi_item_index', $this->table2, 'item_index');
        $this->createIndex('vi_updated', $this->table2, 'updated_at');

        $this->addForeignKey('vc_item_vi_id', $this->table1, 'item_id', $this->table2, 'id', 'cascade');


    }

    public function safeDown()
    {
        $this->dropTable($this->table1);
        $this->dropTable($this->table2);
    }
}
