<?php

use console\components\Migration;

/**
 * Class m181108_132353_add_column_apartment_complex_table migration
 */
class m181108_132353_add_column_apartment_complex_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%apartment_complex}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'custom_choose_label', $this->string()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'custom_choose_label');
    }
}
