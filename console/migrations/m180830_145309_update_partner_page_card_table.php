<?php

use console\components\Migration;

/**
 * Class m180830_145309_update_partner_page_card_table migration
 */
class m180830_145309_update_partner_page_card_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%partner_page_card}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'color', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'color');
    }
}
