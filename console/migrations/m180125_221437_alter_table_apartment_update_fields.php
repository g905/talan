<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m180125_221437_alter_table_apartment_update_fields migration
 */
class m180125_221437_alter_table_apartment_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%apartment}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'price_from', $this->decimal(15,2)->null()->comment('Price from'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'price_from', $this->decimal(10,2)->null()->comment('Price from'));
    }
}