<?php

use console\components\Migration;

/**
 * Class m171221_214733_create_apartment_complex_table migration
 */
class m171221_214733_create_apartment_complex_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%apartment_complex}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'top_screen_title' => $this->string()->notNull()->comment('top screen title'),
                'top_screen_btn_label' => $this->string()->null()->comment('top screen btn label'),
                'top_screen_btn_link' => $this->string()->null()->comment('top screen btn link'),
                'general_progress_title' => $this->string()->null()->comment('general progress title'),
                'general_progress_percent' => $this->string()->null()->comment('general progress percent'),
                'general_description' => $this->text()->null()->comment('general description'),
                'general_docs_link' => $this->string()->null()->comment('general docs link'),
                'general_docs_link_title' => $this->string()->null()->comment('general_docs link title'),
                'general_fb_link' => $this->string()->null()->comment('general fb link'),
                'general_vk_link' => $this->string()->null()->comment('general vk link'),
                'form_top_title' => $this->string()->null()->comment('form top title'),
                'form_top_description' => $this->text()->null()->comment('form top description'),
                'form_top_emails' => $this->text()->null()->comment('form top emails'),
                'advantages_title' => $this->string()->null()->comment('advantages title'),
                'map_title' => $this->string()->null()->comment('map title'),
                'map_subtitle' => $this->string()->null()->comment('map subtitle'),
                'map_description' => $this->text()->null()->comment('map description'),
                'public_service_title' => $this->string()->null()->comment('public service title'),
                'architecture_title' => $this->string()->null()->comment('architecture title'),
                'architecture_fio' => $this->string()->null()->comment('architecture fio'),
                'architecture_job_position' => $this->string()->null()->comment('architecture job position'),
                'architecture_description' => $this->text()->null()->comment('architecture description'),
                'form_bottom_title' => $this->string()->null()->comment('form bottom title'),
                'form_bottom_description' => $this->text()->null()->comment('form bottom description'),
                'form_bottom_email' => $this->text()->null()->comment('form bottom email'),
                'manager_id' => $this->integer()->null()->comment('manager id'),
                'filter_price_deviation' => $this->decimal(10,2)->null()->comment('filter price deviation percent'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-apartment_complex-manager_id-manager-id',
            $this->tableName,
            'manager_id',
            '{{%manager}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-apartment_complex-manager_id-manager-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
