<?php

use console\components\Migration;

/**
 * Class m180912_130332_update_earth_table migration
 */
class m180912_130332_update_earth_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%earth}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'notification_email', $this->text()->null());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'notification_email');
    }
}
