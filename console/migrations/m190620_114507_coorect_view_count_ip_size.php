<?php

use console\components\Migration;

/**
 * Class m190620_114507_coorect_view_count_ip_size migration
 */
class m190620_114507_coorect_view_count_ip_size extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%y}}';

    public function safeUp()
    {
        $this->alterColumn('view_count', 'ip', $this->bigInteger());
    }

    public function safeDown()
    {
        $this->alterColumn('view_count', 'ip', $this->integer());
    }
}
