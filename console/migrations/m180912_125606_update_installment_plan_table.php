<?php

use console\components\Migration;

/**
 * Class m180912_125606_update_installment_plan_table migration
 */
class m180912_125606_update_installment_plan_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%installment_plan}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'notification_email', $this->text()->null());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'notification_email');
    }
}
