<?php

use common\models\base\Configuration;
use console\components\Migration;

/**
 * Class m180424_122307_add_onsubmit_onclick_events_fields_to_tables migration
 */
class m180424_122307_add_onsubmit_onclick_events_fields_to_tables extends Migration
{
    const CALLBACK_ONSUBMIT_CONFIG_KEY = 'globalCallbackOnSubmit';
    const EXCURSION_ONSUBMIT_CONFIG_KEY = 'apartmentExcursionOnSubmit';

    /**
     * @var string migration tables.
     */
    public $tableHome = '{{%home}}';
    public $tableComplex = '{{%apartment_complex}}';
    public $tableApartment = '{{%apartment}}';
    public $tableCustomPage = '{{%custom_page}}';
    public $tablePromoAction = '{{%promo_action}}';
    public $tableConfig = '{{%configuration}}';

    public function safeUp()
    {
        $this->addColumn($this->tableHome, 'top_screen_btn_onclick', $this->string()->null()->comment('Button OnClick event'));

        $this->addColumn($this->tableComplex, 'choose_apartment_onclick', $this->string()->null()->comment('Form OnClick event'));
        $this->addColumn($this->tableComplex, 'form_top_onsubmit', $this->string()->null()->comment('Form OnSubmit event'));
        $this->addColumn($this->tableComplex, 'form_bottom_onsubmit', $this->string()->null()->comment('Form OnSubmit event'));

        $this->addColumn($this->tableApartment, 'form_onsubmit', $this->string()->null()->comment('Form OnSubmit event'));

        $this->addColumn($this->tableCustomPage, 'form_onsubmit', $this->string()->null()->comment('Form OnSubmit event'));

        $this->addColumn($this->tablePromoAction, 'form_onsubmit', $this->string()->null()->comment('Form OnSubmit event'));

        $this->insert($this->tableConfig, [
            'id' => self::CALLBACK_ONSUBMIT_CONFIG_KEY,
            'value' => "ga('send', 'event', 'form', 'zakaz_zvonka'); yaCounter47010621.reachGoal('form6'); return true;",
            'type' => Configuration::TYPE_STRING,
            'description' => 'OnSubmit event for global callback form',
            'preload' => 1,
            'published' => 1,
            'show' => 1,
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert($this->tableConfig, [
            'id' => self::EXCURSION_ONSUBMIT_CONFIG_KEY,
            'value' => "ga('send', 'event', 'form', 'forma_kv'); yaCounter47010621.reachGoal('form7'); return true;",
            'type' => Configuration::TYPE_STRING,
            'description' => 'OnSubmit event for apartment excursion popup form',
            'preload' => 1,
            'published' => 1,
            'show' => 1,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableHome, 'top_screen_btn_onclick');

        $this->dropColumn($this->tableComplex, 'choose_apartment_onclick');
        $this->dropColumn($this->tableComplex, 'form_top_onsubmit');
        $this->dropColumn($this->tableComplex, 'form_bottom_onsubmit');

        $this->dropColumn($this->tableApartment, 'form_onsubmit');

        $this->dropColumn($this->tableCustomPage, 'form_onsubmit');

        $this->dropColumn($this->tablePromoAction, 'form_onsubmit');

        $this->delete($this->tableConfig, ['id' => self::CALLBACK_ONSUBMIT_CONFIG_KEY]);
        $this->delete($this->tableConfig, ['id' => self::EXCURSION_ONSUBMIT_CONFIG_KEY]);
    }
}
