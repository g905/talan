<?php

use console\components\Migration;

/**
 * Class m180924_133729_update_apartment_building_table migration
 */
class m180924_133729_update_apartment_building_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_building}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'cta_black_label', $this->string()->null());
        $this->addColumn($this->table, 'cta_white_label', $this->string()->null());
        $this->addColumn($this->table, 'cta_description', $this->text()->null());
        $this->addColumn($this->table, 'cta_button_label', $this->string()->null());
        $this->addColumn($this->table, 'cta_button_link', $this->string()->null());
        $this->addColumn($this->table, 'cta_published', $this->smallInteger()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'cta_black_label');
        $this->dropColumn($this->table, 'cta_white_label');
        $this->dropColumn($this->table, 'cta_description');
        $this->dropColumn($this->table, 'cta_button_label');
        $this->dropColumn($this->table, 'cta_button_link');
        $this->dropColumn($this->table, 'cta_published');
    }
}
