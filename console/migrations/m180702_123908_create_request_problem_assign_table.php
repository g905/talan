<?php

use console\components\Migration;

/**
 * Class m180702_123908_create_request_problem_assign_table migration
 */
class m180702_123908_create_request_problem_assign_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%request_problem_assign}}';
    public $tableAssign = '{{%request_problem}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'problem_id' => $this->integer(),
            'object_id' => $this->integer(),
            'type' => $this->tinyInteger()->unsigned()->comment('Object type identifier'), // Instead of model class will store constant values.
            'position' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk-request_problem_assign-request_problem',
            $this->table,
            'problem_id',
            $this->tableAssign,
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-request_problem_assign-request_problem', $this->table);
        $this->dropTable($this->table);
    }
}
