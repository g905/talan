<?php

use console\components\Migration;

/**
 * Class m180417_154429_update_request_tables migration
 */
class m180417_154429_update_request_tables extends Migration
{
    /**
     * @var string|string[] migration table names.
     */
    public $cityTable = '{{%city}}';
    public $tables = [
        '{{%subscribe_request}}' => 'sr-city_id-city-id',
        '{{%request_callback}}' => 'rc-city_id-city-id',
        '{{%request_question}}' => 'rq-city_id-city-id',
        '{{%request_excursion}}' => 're-city_id-city-id',
        '{{%request_complex_presentation}}' => 'rcp-city_id-city-id',
    ];

    public function safeUp()
    {
        foreach ($this->tables as $table => $fk) {
            $this->addColumn($table, 'city_id', 'int(11) NULL AFTER id');
            $this->addCommentOnColumn($table, 'city_id', 'Associated city');
            $this->addForeignKey($fk, $table, 'city_id', $this->cityTable, 'id', 'SET NULL', 'CASCADE');
        }
    }

    public function safeDown()
    {
        foreach ($this->tables as $table => $fk) {
            $this->dropForeignKey($fk, $table);
            $this->dropColumn($table, 'city_id');
        }
    }
}
