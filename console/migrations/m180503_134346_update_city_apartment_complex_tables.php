<?php

use console\components\Migration;

/**
 * Class m180503_134346_update_city_apartment_complex_tables migration
 */
class m180503_134346_update_city_apartment_complex_tables extends Migration
{
    const FK_CITY = 'fk-city-cpi-cp-id';
    const FK_COMPLEX = 'fk-ac-cpi-cp-id';

    public $table = '{{%custom_popup}}';
    public $tableCity = '{{%city}}';
    public $tableComplex = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->tableCity, 'custom_popup_id', $this->integer()->null()->comment('Custom popup'));
        $this->addColumn($this->tableComplex, 'custom_popup_id', $this->integer()->null()->comment('Custom popup'));

        $this->addForeignKey(self::FK_CITY, $this->tableCity, 'custom_popup_id', $this->table, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey(self::FK_COMPLEX, $this->tableComplex, 'custom_popup_id', $this->table, 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_COMPLEX, $this->tableComplex);
        $this->dropForeignKey(self::FK_CITY, $this->tableCity);

        $this->dropColumn($this->tableComplex, 'custom_popup_id');
        $this->dropColumn($this->tableCity, 'custom_popup_id');
    }
}
