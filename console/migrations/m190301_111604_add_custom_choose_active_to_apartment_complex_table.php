<?php

use console\components\Migration;

/**
 * Class m190301_111604_add_custom_choose_active_to_apartment_complex_table migration
 */
class m190301_111604_add_custom_choose_active_to_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'custom_choose_active', 'integer default 1');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'custom_choose_active');
    }
}
