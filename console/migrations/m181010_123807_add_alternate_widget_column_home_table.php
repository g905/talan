<?php

use console\components\Migration;

/**
 * Class m181010_123807_add_alternate_widget_column_home_table migration
 */
class m181010_123807_add_alternate_widget_column_home_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%home}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'alternate_widget', $this->boolean()->notNull()->defaultValue(0));
        $this->addColumn($this->table, 'alternate_title', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'alternate_widget');
        $this->dropColumn($this->table, 'alternate_title');
    }
}
