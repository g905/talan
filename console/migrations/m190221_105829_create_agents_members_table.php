<?php

use console\components\Migration;

/**
 * Class m190221_105829_create_agents_members_table migration
 */
class m190221_105829_create_agents_members_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%agents_members}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'fio' => $this->string()->notNull()->comment('Fio'),
            'description' => $this->string()->null()->comment('Description'),
            'phone' => $this->string()->null()->comment('phone'),
            'phone_shows' => $this->integer()->null()->comment('phone shows count'),
            'deals_amount' => $this->integer()->null()->comment('amount of deals'),
            'service_evaluation' => $this->integer()->null()->comment('evaluation for service'),
            'agents_id' => $this->integer()->defaultValue(1)->comment('Agents'),
            'youtube_link' => $this->string()->null()->comment('Youtube video link'),
            'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
            'created_at' => $this->integer()->notNull()->comment('Created At'),
            'updated_at' => $this->integer()->notNull()->comment('Updated At'),
        ], $this->tableOptions);
        $this->addForeignKey(
            'fk-agents_members-agents_id-agents-id',
            $this->table,
            'agents_id',
            '{{%agents}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
    	$this->dropForeignKey('fk-agents_members-agents_id-agents-id', $this->table);
        $this->dropTable($this->table);
    }
}
