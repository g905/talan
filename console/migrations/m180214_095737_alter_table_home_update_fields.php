<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m180214_095737_alter_table_home_update_fields migration
 */
class m180214_095737_alter_table_home_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%home}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'apartment_complex_link',  $this->string()->null()->comment('apartment complex link'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'apartment_complex_link');
    }
}