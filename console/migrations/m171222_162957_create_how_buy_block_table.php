<?php

use console\components\Migration;

/**
 * Class m171222_162957_create_how_buy_block_table migration
 */
class m171222_162957_create_how_buy_block_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%how_buy_block}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'how_buy_id' => $this->integer()->null()->comment('How buy'),
                'label' => $this->string()->notNull()->comment('Label'),
                'short_desc' => $this->text()->null()->comment('Short description'),
                'content' => $this->text()->null()->comment('Content'),
                'link_label' => $this->string()->null()->comment('Link label'),
                'link' => $this->string()->null()->comment('Link (if exist)'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-how_buy_block-how_buy_id-how_buy-id',
            $this->tableName,
            'how_buy_id',
            '{{%how_buy}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-how_buy_block-how_buy_id-how_buy-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
