<?php

use console\components\Migration;

/**
 * Class m190225_132845_add_apartment_chess_text_to_apartment_complex_table migration
 */
class m190225_132845_add_apartment_chess_text_to_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'apartment_chess_text', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'apartment_chess_text');
    }
}
