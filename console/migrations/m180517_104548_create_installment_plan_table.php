<?php

use console\components\Migration;

/**
 * Class m180517_104548_create_installment_plan_table migration
 */
class m180517_104548_create_installment_plan_table extends Migration
{
    const FK_CITY = 'fk-installment_plan-city_id-city-id';
    const FK_MANAGER = 'fk-installment_plan-manager_id-manager-id';

    /**
     * @var string migration table names.
     */
    public $table = '{{%installment_plan}}';
    public $tableCity = '{{%city}}';
    public $tableManager = '{{%manager}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'city_id' => $this->integer()->null()->comment('City'),
                'manager_id' => $this->integer()->null()->comment('Manager'),
                'label' => $this->string()->notNull()->comment('Label'),
                'alias' => $this->string()->notNull()->comment('Alias'),
                'description_label' => $this->string()->null()->comment('Description label'),
                'description_text' => $this->text()->null()->comment('Description text'),

                'default_apartment_price' => $this->integer()->comment('Apartment price in rub'),
                'default_time_term' => $this->integer()->comment('Time term in months'),
                'default_initial_fee' => $this->integer()->comment('Initial fee percent'),

                'benefits_block_label' => $this->string()->null()->comment('Benefits block label'),
                'steps_block_label' => $this->string()->null()->comment('Steps block label'),

                'form_calc_onsubmit' => $this->string()->null()->comment('Calculator form OnSubmit event'),
                'form_bottom_onsubmit' => $this->string()->null()->comment('Bottom form OnSubmit event'),

                'view_count' => $this->integer()->null()->comment('View count'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(self::FK_CITY, $this->table, 'city_id', $this->tableCity, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_MANAGER, $this->table, 'manager_id', $this->tableManager, 'id', 'SET NULL', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_MANAGER, $this->table);
        $this->dropForeignKey(self::FK_CITY, $this->table);
        $this->dropTable($this->table);
    }
}
