<?php

use console\components\Migration;

/**
 * Class m180425_133337_alter_apartment_complex_table_add_history_columns migration
 */
class m180425_133337_alter_apartment_complex_table_add_history_columns extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%apartment_complex}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'build_start_date', $this->date()->null()->comment('Build start date'));
        $this->addColumn($this->tableName, 'build_end_date', $this->date()->null()->comment('Build start date'));
        $this->addColumn($this->tableName, 'build_video', $this->text()->null()->comment('Build video'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'build_video');
        $this->dropColumn($this->tableName, 'build_end_date');
        $this->dropColumn($this->tableName, 'build_start_date');
    }
}
