<?php

use console\components\Migration;

/**
 * Class m180403_091726_alter_article_table_add_columns migration
 */
class m180403_091726_alter_article_table_add_columns extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%article}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'manager_id', $this->integer()->null()->comment('Manager'));
        $this->addColumn($this->tableName, 'short_description', $this->text()->null()->comment('Short Description'));

        $this->addForeignKey(
            'fk-article_manager_id_manager-id',
            $this->tableName,
            'manager_id',
            '{{%manager}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addColumn($this->tableName, 'form_title', $this->text()->null()->comment('Form title'));
        $this->addColumn($this->tableName, 'form_description', $this->text()->null()->comment('Form description'));
        $this->addColumn($this->tableName, 'form_agreement', $this->text()->null()->comment('Form agreement'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-article_manager_id_manager-id', $this->tableName);
        $this->dropColumn($this->tableName, 'manager_id');
        $this->dropColumn($this->tableName, 'short_description');

        $this->dropColumn($this->tableName, 'form_agreement');
        $this->dropColumn($this->tableName, 'form_description');
        $this->dropColumn($this->tableName, 'form_title');
    }
}
