<?php

use console\components\Migration;

/**
 * Class m170117_052626_create_builder_widget_table migration
 */
class m170117_052626_create_builder_widget_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%builder_widget}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'target_class' => $this->string()->notNull(),
                'target_id' => $this->integer()->notNull(),
                'target_sign' => $this->string()->null(),
                'target_attribute' => $this->string()->notNull(),
                'widget_class' => $this->string()->null(),
                'position' => $this->integer()->notNull()->defaultValue(0),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
