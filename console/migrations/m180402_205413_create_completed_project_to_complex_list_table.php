<?php

use console\components\Migration;

/**
 * Class m180402_205413_create_completed_project_to_complex_list_table migration
 */
class m180402_205413_create_completed_project_to_complex_list_table extends Migration
{
    /**
     * @var string PK, FK names.
     */
    const PK = 'cpi_cli_pk';
    const FK_CL = 'cptcl_cli_cl-id-fk';
    const FK_CP = 'cptcl_cpi_cp-id-fk';
    /**
     * @var string migration table name.
     */
    public $table = '{{%completed_project_to_complex_list}}';
    public $tableComplexList = '{{%complex_list}}';
    public $tableCompletedProject = '{{%completed_project}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'complex_list_id' => $this->integer()->notNull()->comment('Complex list ID'),
                'completed_project_id' => $this->integer()->notNull()->comment('Completed project ID'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
            ],
            $this->tableOptions
        );

        $this->addPrimaryKey(self::PK, $this->table, ['complex_list_id', 'completed_project_id']);
        $this->addForeignKey(self::FK_CL, $this->table, 'complex_list_id', $this->tableComplexList, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_CP, $this->table, 'completed_project_id', $this->tableCompletedProject, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_CP, $this->table);
        $this->dropForeignKey(self::FK_CL, $this->table);
        $this->dropTable($this->table);
    }
}
