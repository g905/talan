<?php

use console\components\Migration;

/**
 * Class m180912_112405_update_apartment_complex_table migration
 */
class m180912_112405_update_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'room_1', $this->text()->null()->comment('One Room'));
        $this->addColumn($this->table, 'room_2', $this->text()->null()->comment('Two Rooms'));
        $this->addColumn($this->table, 'room_3', $this->text()->null()->comment('Three Rooms'));
        $this->addColumn($this->table, 'room_4', $this->text()->null()->comment('Four Rooms'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'room_1');
        $this->dropColumn($this->table, 'room_2');
        $this->dropColumn($this->table, 'room_3');
        $this->dropColumn($this->table, 'room_4');
    }
}
