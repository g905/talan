<?php

use console\components\Migration;

/**
 * Class m181220_092757_create_entrance_table migration
 */
class m181220_092757_create_entrance_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%apartment_entrance}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'entrance' => $this->integer()->notNull()->comment('Entrance'),
                /*'content' => $this->text()->null()->comment('Content'),*/
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'building_id' => $this->integer()->null(),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-entrance-building_id-apartment_building-id',
            $this->tableName,
            'building_id',
            '{{%apartment_building}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-entrance-building_id-apartment_building-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
