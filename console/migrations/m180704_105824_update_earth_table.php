<?php

use console\components\Migration;

/**
 * Class m180704_105824_update_earth_table migration
 */
class m180704_105824_update_earth_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%earth}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'map_lat', $this->decimal(13, 8)->null()->comment('Lat')->after('map_demands'));
        $this->addColumn($this->table, 'map_lng', $this->decimal(13, 8)->null()->comment('Lng')->after('map_lat'));
        $this->addColumn($this->table, 'map_zoom', $this->smallInteger()->defaultValue(15)->comment('Zoom')->after('map_lng'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'map_lat');
        $this->dropColumn($this->table, 'map_lng');
        $this->dropColumn($this->table, 'map_zoom');
    }
}
