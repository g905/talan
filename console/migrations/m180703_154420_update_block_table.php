<?php

use console\components\Migration;

/**
 * Class m180703_154420_update_block_table migration
 */
class m180703_154420_update_block_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%block}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'link', $this->string()->after('description')->null());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'link');
    }
}
