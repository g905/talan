<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m171224_200326_alter_table_apartment_complex_update_fields migration
 */
class m171224_200326_alter_table_apartment_complex_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%apartment_complex}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'lat',  $this->decimal(13,8)->null()->comment('lat'));
        $this->addColumn($this->tableName, 'lng',  $this->decimal(13,8)->null()->comment('lng'));
        $this->addColumn($this->tableName, 'alias',  $this->string()->comment('alias'));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName,'lat');
        $this->dropColumn($this->tableName,'lng');
        $this->dropColumn($this->tableName,'alias');
    }
}