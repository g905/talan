<?php

use console\components\Migration;

/**
 * Class m180517_124553_create_block_table migration
 */
class m180517_124553_create_block_table extends Migration
{
    const IX_OBJECT_TYPE = 'block-object_id-type-ix';
    /**
     * @var string migration table names.
     */
    public $table = '{{%block}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'type' => $this->smallInteger()->notNull()->comment('Object type'),
            'object_id' => $this->integer()->notNull()->comment('Object ID'),
            'title' => $this->string()->notNull()->comment('Title'),
            'description' => $this->text()->defaultValue(null)->comment('Description'),
            'published' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1)->comment('Published'),
            'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
        ], $this->tableOptions);

        //$this->createIndex(self::IX_OBJECT_TYPE, $this->table, ['type', 'object_id'], true);
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
