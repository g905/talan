<?php

use console\components\Migration;

/**
 * Class m190702_101230_add_map_image_field_to_contact_page migration
 */
class m190702_101230_add_map_image_field_to_contact_page extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%contact_page}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'map_image', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'map_image');
    }
}
