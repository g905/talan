<?php

use console\components\Migration;

/**
 * Class m190418_130527_agency_api_part3 migration
 */
class m190418_130527_agency_api_part3 extends Migration
{

	public function safeUp()
	{
		$this->dropIndex('uk-api_agency-email', '{{api_agency}}');
	}

	public function safeDown()
	{
		$this->createIndex('uk-api_agency-email', '{{api_agency}}', ['email'], true);
	}
}
