<?php

use console\components\Migration;

/**
 * Class m180404_040250_update_complex_list_table migration
 */
class m180404_040250_update_complex_list_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%complex_list}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'menu_title', 'varchar(255) NOT NULL AFTER top_title');
        $this->addColumn($this->table, 'form_agreement', 'varchar(255) NOT NULL AFTER form_email');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'menu_title');
        $this->dropColumn($this->table, 'form_agreement');
    }
}
