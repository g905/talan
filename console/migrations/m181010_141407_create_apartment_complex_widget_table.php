<?php

use console\components\Migration;

/**
 * Class m181010_141407_create_apartment_complex_widget_table migration
 */
class m181010_141407_create_apartment_complex_widget_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $tableName = '{{%apartment_complex_widget}}';

    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Title'),
                'link' => $this->string()->notNull()->comment('Link'),
                'complex_id' => $this->integer()->notNull()->comment('Complex page'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-apartment_complex_widget-complex_id-apartment_complex-id',
            $this->tableName,
            'complex_id',
            '{{%apartment_complex}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-apartment_complex_widget-apartment_complex_id-apartment_complex-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
