<?php

use console\components\Migration;

/**
 * Class m171228_081804_create_request_complex_presentation_table migration
 */
class m171228_081804_create_request_complex_presentation_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%request_complex_presentation}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'ac_id' => $this->integer()->null()->comment('Apartment complex'),
                'name' => $this->string()->comment('Name'),
                'phone' => $this->string()->comment('Phone'),
                'email' => $this->string()->comment('Email'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-request_ac_id-complex-id',
            $this->tableName,
            'ac_id',
            '{{%apartment_complex}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-request_ac_id-complex-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
