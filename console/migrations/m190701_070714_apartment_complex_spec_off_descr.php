<?php
use console\components\Migration;

/**
 * Class m190701_070714_apartment_complex_spec_off_descr migration
 */
class m190701_070714_apartment_complex_spec_off_descr extends Migration
{

	public function safeUp()
	{
		$this->addColumn('apartment_complex', 'special_offer_description', $this->text());
	}

	public function safeDown()
	{
		$this->dropColumn('apartment_complex', 'special_offer_description');
		return true;
	}
}
