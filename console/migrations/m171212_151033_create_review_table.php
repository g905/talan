<?php

use console\components\Migration;

/**
 * Class m171212_151033_create_review_table migration
 */
class m171212_151033_create_review_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%review}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'fio' => $this->string()->notNull()->comment('Name'),
                'label' => $this->string()->null()->comment('Short description'),
                'review_text' => $this->text()->notNull()->comment('Review text'),
                'youtube_video' => $this->string()->null()->comment('Youtube video'),
                'home_page_id' => $this->integer()->null()->comment('Home page'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-review-home_page_id-home-id',
            $this->tableName,
            'home_page_id',
            '{{%home}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-review-home_page_id-home-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
