<?php

use console\components\Migration;

/**
 * Class m180702_133252_update_request_question_table migration
 */
class m180702_133252_update_request_question_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%request_question}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'ab_id', $this->integer()->after('ac_id')->null()->comment('Building ID'));
        $this->addForeignKey(
            'fk-request_question_ab_id-building-id',
            $this->table,
            'ab_id',
            '{{%apartment_building}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-request_question_ab_id-building-id', $this->table);
        $this->dropColumn($this->table, 'ab_id');
    }
}
