<?php

use console\components\Migration;

/**
 * Class m190322_110315_add_city_id_to_apartment_constructor_table migration
 */
class m190322_110315_add_city_id_to_apartment_constructor_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_constructor}}';
    public $fk = 'apartment_constructor-city_id-city-id';

    public function safeUp()
    {
        $this->addColumn($this->table, 'city_id', 'integer default 1');
        $this->addForeignKey($this->fk, $this->table, 'city_id', '{{%city}}', 'id', 'CASCADE', 'CASCADE');


    }

    public function safeDown()
    {
        $this->dropForeignKey($this->fk, $this->table);
        $this->dropColumn($this->table, 'city_id');
    }
}
