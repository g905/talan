<?php

use console\components\Migration;

/**
 * Class m190417_110219_add_invest_block_to_catalog_table migration
 */
class m190417_110219_add_invest_block_to_catalog_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%catalog}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'invest_title', $this->string());
        $this->addColumn($this->table, 'invest_desc', $this->string());
        $this->addColumn($this->table, 'invest_button_label', $this->string());
        $this->addColumn($this->table, 'invest_button_link', $this->string());
        $this->addColumn($this->table, 'invest_active', $this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'invest_title');
        $this->dropColumn($this->table, 'invest_desc');
        $this->dropColumn($this->table, 'invest_button_label');
        $this->dropColumn($this->table, 'invest_button_link');
        $this->dropColumn($this->table, 'invest_active');
    }
}
