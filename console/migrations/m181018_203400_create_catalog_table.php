<?php

use console\components\Migration;

/**
 * Class m181018_203400_create_catalog_table migration
 */
class m181018_203400_create_catalog_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%catalog}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'city_id' => $this->integer()->notNull()->comment('City Id'),
                'residental_label' => $this->string()->notNull()->comment('Residental Label'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'commerical_label' => $this->string()->notNull()->comment('Commerical Label'),
                'completed_label' => $this->string()->notNull()->comment('Completed Label'),
                'form_title' => $this->string()->notNull()->comment('Form Title'),
                'form_description' => $this->text()->notNull()->comment('Form Description'),
                'form_onsubmit' => $this->string()->null()->comment('Form Onsubmit'),
                'resale_label' => $this->string()->notNull()->comment('Resale Label'),
                'manager_id' => $this->integer()->null()->comment('Manager Id'),
                'label' => $this->string()->notNull()->comment('Label'),
                'projects_id' => $this->text()->null()->comment('Projects'),
                'completed_sublabel' => $this->text()->null()->comment('Completed Sublabel'),
            ],
            $this->tableOptions
        );
        $this->createIndex(
            'idx-catalog-city_id',
            'catalog',
            'city_id',
            false
        );
        $this->createIndex(
            'idx-catalog-published',
            'catalog',
            'published',
            false
        );
        $this->addForeignKey(
            'fk-catalog-city_id-city-id',
            $this->tableName,
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-catalog-city_id-city-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
