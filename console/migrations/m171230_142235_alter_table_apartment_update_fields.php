<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m171230_142235_alter_table_apartment_update_fields migration
 */
class m171230_142235_alter_table_apartment_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%apartment}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'floor',  $this->integer()->notNull()->comment('Floor number'));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName,'floor');
    }
}