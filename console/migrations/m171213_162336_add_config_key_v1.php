<?php

use console\components\Migration;

/**
 * Class m171213_162336_add_config_key_v1 migration
 */
class m171213_162336_add_config_key_v1 extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%configuration}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 'formsEmail',
            'type' => \common\models\Configuration::TYPE_STRING,
            'value' => '',
            'description' => 'forms email',
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 'formsEmail']);
    }
}
