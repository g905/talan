<?php

use console\components\Migration;

/**
 * Class m181004_080202_add_synchronize_table migration
 */
class m181004_080202_add_synchronize_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%synchronize}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'add' => $this->integer(),
            'replaced' => $this->integer(),
            'status' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
