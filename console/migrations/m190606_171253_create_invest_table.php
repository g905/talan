<?php

use console\components\Migration;

/**
 * Class m190606_171253_create_invest_table migration
 */
class m190606_171253_create_invest_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%request_invest}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
        //    'ac_id' => $this->integer(),
            'city_id' => $this->integer(),
            'phone' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createIndex('ri-city_id-city-id', $this->table, 'city_id');
    //    $this->createIndex('ri-ac_id-ac-id', $this->table, 'ac_id');

        $this->addForeignKey('ri-city_id-city-id', $this->table, 'city_id', 'city', 'id');
    //    $this->addForeignKey('ri-ac_id-ac-id', $this->table, 'ac_id', 'apartment_complex', 'id');
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
