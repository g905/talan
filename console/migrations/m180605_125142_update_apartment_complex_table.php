<?php

use console\components\Migration;

/**
 * Class m180605_125142_update_apartment_complex_table migration
 */
class m180605_125142_update_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'btn_label', $this->string()->null()->comment('View apartment button label'));
        $this->addColumn($this->table, 'btn_link', $this->string()->null()->comment('View apartment button link'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'btn_label');
        $this->dropColumn($this->table, 'btn_link');
    }
}
