<?php

use console\components\Migration;

/**
 * Class m180627_123726_update_request_complex_presentation_table migration
 */
class m180627_123726_update_request_complex_presentation_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%request_complex_presentation}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'ab_id', $this->integer()->after('ac_id')->null()->comment('Building ID'));
        $this->addForeignKey(
            'fk-request_ab_id-building-id',
            $this->table,
            'ab_id',
            '{{%apartment_building}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-request_ab_id-building-id', $this->table);
        $this->dropColumn($this->table, 'ab_id');
    }
}
