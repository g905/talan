<?php

use console\components\Migration;

/**
 * Class m190624_102350_correct_table_apartmnet_building migration
 */
class m190624_102350_correct_table_apartmnet_building extends Migration
{
    /**
     * @var string migration table name.
     */


    public function safeUp()
    {
        $this->addColumn('apartment_building', 'special_offer_text', $this->string());
        $this->addColumn('apartment_building', 'special_offer_link', $this->string());
        $this->addColumn('apartment_building', 'btn_label', $this->string());
        $this->addColumn('apartment_building', 'btn_link', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('apartment_building', 'special_offer_text', $this->string());
        $this->dropColumn('apartment_building', 'special_offer_link', $this->string());
        $this->dropColumn('apartment_building', 'btn_label', $this->string());
        $this->dropColumn('apartment_building', 'btn_link', $this->string());
    }
}
