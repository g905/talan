<?php
use console\components\Migration;
use \vintage\i18n\models\SourceMessage;

/**
 * Class m190329_095404_agency_api_part2 migration
 */
class m190329_095404_agency_api_part2 extends Migration
{
	public static function getSourceMessageArr() {
		return [
			['back/menu', 'Export from site', 'Выгрузка с сайта'],
			['back/menu', 'Agencies', 'Агенства'],
			['back/menu', 'Statistic', 'Статистика'],
			['back/menu', 'Lead', 'Лид'],
			['back/menu', 'Widget Visits', 'Посещение виджета'],
			['back/menu', 'Conversion', 'Конверсия'],
			['back/api', 'No', 'Нет'],
			['back/api', 'Yes', 'Да'],
			['back/api', 'Save', 'Сохранить'],

			['back/api', 'ID', 'ID'],
			['back/api', 'Name', 'Название'],
			['back/api', 'Email', 'E-mail'],
			['back/api', 'Active', 'Включено'],
			['back/api', 'Site Callback Url', 'Callback Url сайта'],
			['back/api', 'Token', 'Токен'],
			['back/api', 'Created At', 'Дата создания'],
			['back/api', 'Updated At', 'Дата изменения'],
			['back/api', 'Agencies List', 'Список агенств'],
			['back/api', 'Create', 'Создать'],
			['back/api', 'Creating new Agency', 'Создание нового агентства'],
			['back/api', 'Updating Agency', 'Редактирование агенства'],
			['back/api', 'Update', 'Редактирование'],
			['back/api_', 'Update', 'Редактировать'],
			['back/api_', 'Delete', 'Удалить'],
			['back/api', 'Are you sure you want to delete this item?', 'Вы уверены, что хотите удалить этот элемент?'],
			['back/api', 'Select complexes', 'Выбор комплексов'],
			['back/api', 'Select complexes for the agency', 'Выберите ЖК для агенства'],
			['back/api', 'Show Price on client side', 'Показать цену на стороне клиента'],
			['back/api', 'Select complex', 'Выберите комплекс'],
			['back/api', 'Embed code to the site', 'Код для вставки на сайт'],
			['back/api', 'Add', 'Добавить'],
			['back/api', 'Phone', 'Телефон'],
			['back/api', 'Comment', 'Комментарий'],
			['back/api', 'Apartment Name', 'Название апартаментов'],
			['back/api', 'Lead list', 'Список лидов'],
			['back/api', 'Visit date', 'Дата визита'],
			['back/api', 'Visit count', 'Кол-во отображений'],

			/*['back/api', , ''],
			['back/api', , ''],
			/*['back/api', , ''],
			['back/api', , ''],
			/**/



		];
	}

	public function safeUp() {
		$arrTranslates = self::getSourceMessageArr();
		$messageArr = [];

		// добавление переводов
		if(is_array($arrTranslates) && count($arrTranslates)) {

			foreach($arrTranslates as $oneTranslate) {
				$sm = new SourceMessage();
				$sm->category = $oneTranslate[0];
				$sm->message = $oneTranslate[1];
				if($sm->save()) {
					$messageArr[] = [
						$sm->id, 'ru', $oneTranslate[2]
					];
				} else {
					throw new \Exception($sm->category . '__' . $sm->message);
				}
			}

			if(is_array($messageArr) && count($messageArr)) {
				$this->batchInsert('message', ['id', 'language', 'translation'], $messageArr);
			}
		}
	}

	public function safeDown()
	{
		$idsForRemove = [];
		$arrTranslates = self::getSourceMessageArr();

		// удаление переводов
		if(is_array($arrTranslates) && count($arrTranslates)) {
			$selectQuery = SourceMessage::find()
				->select('id')
				->asArray()
			;
			foreach($arrTranslates as $oneTranslate) {
				$selectQuery->orWhere([
					'category' => $oneTranslate[0],
					'message' => $oneTranslate[1],
				]);
			}
			$idsForRemove = $selectQuery->all();

			if(is_array($idsForRemove) && count($idsForRemove)) {
				$idsForRemove = \yii\helpers\ArrayHelper::getColumn($idsForRemove, 'id');

				SourceMessage::deleteAll(['id' => $idsForRemove]);
			}
		}

		return true;
	}
}
