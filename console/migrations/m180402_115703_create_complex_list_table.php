<?php

use console\components\Migration;

/**
 * Class m180402_115703_create_complex_list_table migration
 */
class m180402_115703_create_complex_list_table extends Migration
{
    /**
     * @var string foreign keys constants.
     */
    const FK_CITY = 'fk-complex_list-city_id-city-id';
    const FK_MANAGER = 'fk-complex_list-manager_id-manager-id';
    /**
     * @var string migration table names.
     */
    public $table = '{{%complex_list}}';
    public $tableCity = '{{%city}}';
    public $tableManager = '{{%manager}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'type' => $this->tinyInteger()->notNull()->comment('Property type'),
                'city_id' => $this->integer()->notNull()->comment('City'),

                'top_title' => $this->string()->notNull()->comment('top title'),
                'complex_more_btn_label' => $this->string()->null()->comment('complex more button label'),

                'completed_projects_title' => $this->string()->null()->comment('completed projects title'),
                'completed_projects_description' => $this->text()->null()->comment('completed projects description'),
                'completed_projects_more_btn_label' => $this->text()->null()->comment('completed projects more button label'),

                'form_title' => $this->string()->null()->comment('form title'),
                'form_description' => $this->text()->null()->comment('form description'),
                'form_email' => $this->text()->null()->comment('form email'),
                'manager_id' => $this->integer()->null()->comment('manager id'),

                'created_at' => $this->integer()->notNull()->comment('created at'),
                'updated_at' => $this->integer()->notNull()->comment('updated at'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(self::FK_CITY, $this->table, 'city_id', $this->tableCity, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_MANAGER, $this->table, 'manager_id', $this->tableManager, 'id', 'SET NULL', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_MANAGER, $this->table);
        $this->dropForeignKey(self::FK_CITY, $this->table);
        $this->dropTable($this->table);
    }
}
