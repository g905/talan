<?php

use console\components\Migration;

/**
 * Class m180614_183827_create_team_table migration
 */
class m180614_183827_create_team_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%team}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'description' => $this->text()->null()->comment('Description'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'city_id' => $this->integer()->null()->comment('City'),
                'team_title' => $this->string()->null()->comment('Team title'),
                'tab_label' => $this->string()->null()->comment('Tab label'),
            ],
            $this->tableOptions
        );
        $this->createIndex(
            'idx-team-city_id',
            'team',
            'city_id',
            false
        );
        $this->addForeignKey(
            'fk-team-city_id-city-id',
            $this->tableName,
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-team-city_id-city-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
