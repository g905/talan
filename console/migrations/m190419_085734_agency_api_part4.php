<?php

use console\components\Migration;

/**
 * Class m190419_085734_agency_api_part4 migration
 */
class m190419_085734_agency_api_part4 extends Migration
{
	public function safeUp()
	{
		$this->addColumn('{{api_customer}}', 'agency_id', $this->integer());
	}

	public function safeDown()
	{
		$this->dropColumn('{{api_customer}}', 'agency_id');
		return true;
	}
}
