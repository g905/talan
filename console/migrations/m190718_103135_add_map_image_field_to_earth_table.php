<?php

use console\components\Migration;

/**
 * Class m190718_103135_add_map_image_field_to_earth_table migration
 */
class m190718_103135_add_map_image_field_to_earth_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%earth}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'map_image', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'map_image');
    }
}
