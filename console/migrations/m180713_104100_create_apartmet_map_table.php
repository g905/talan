<?php

use console\components\Migration;

/**
 * Class m180713_104100_create_apartmet_map_table migration
 */
class m180713_104100_create_apartmet_map_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_map}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),

                'label' => $this->string()->notNull()->comment('Label'),
                'class' => $this->string()->notNull()->comment('Class'),
                'content' => $this->text()->defaultValue(null)->comment('Content'),
                'mob_content' => $this->text()->defaultValue(null)->comment('Content'),

                'complex_id' => $this->integer()->notNull()->comment('Complex Id'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
