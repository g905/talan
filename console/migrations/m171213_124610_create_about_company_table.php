<?php

use console\components\Migration;

/**
 * Class m171213_124610_create_about_company_table migration
 */
class m171213_124610_create_about_company_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%about_company}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'city_id' => $this->integer()->notNull()->comment('City'),
                'top_screen_title' => $this->string()->null()->comment('Top screen title'),
                'top_screen_description' => $this->text()->null()->comment('Top screen description'),
                'mission_label' => $this->text()->null()->comment('Mission label'),
                'company_values_title' => $this->string()->null()->comment('Company values title'),
                'ceo_name' => $this->string()->null()->comment('Ceo name'),
                'ceo_position' => $this->string()->null()->comment('Ceo position'),
                'ceo_quotation' => $this->text()->null()->comment('Ceo quotation'),
                'form_title' => $this->string()->null()->comment('Form_title'),
                'form_description' => $this->text()->null()->comment('Form_description'),
                'form_emails' => $this->text()->null()->comment('Form_emails'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-about_company-city_id-city-id',
            $this->tableName,
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-about_company-city_id-city-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
