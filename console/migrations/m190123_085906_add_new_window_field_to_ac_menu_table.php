<?php

use console\components\Migration;

/**
 * Class m190123_085906_add_new_window_field_to_ac_menu_table migration
 */
class m190123_085906_add_new_window_field_to_ac_menu_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $tableName = '{{%ac_menu}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'new_window', "boolean DEFAULT false");
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'new_window');
    }
}
