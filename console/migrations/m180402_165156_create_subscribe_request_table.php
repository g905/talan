<?php

use console\components\Migration;

/**
 * Class m180402_165156_create_subscribe_request_table migration
 */
class m180402_165156_create_subscribe_request_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%subscribe_request}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'email' => $this->string()->notNull()->comment('Email'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
