<?php

use console\components\Migration;

/**
 * Class m190304_132904_add_city_to_partner_request_table migration
 */
class m190304_132904_add_city_to_partner_request_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%partner_request}}';
    public $fk = 'partner_request-city-city-id';

    public function safeUp()
    {
        $this->addColumn($this->table, 'city', $this->integer());
        $this->addForeignKey($this->fk, $this->table, 'city', '{{%city}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->fk, $this->table);
        $this->dropColumn($this->table, 'city');
    }
}
