<?php

use console\components\Migration;

/**
 * Class m171212_145901_create_how_we_build_table migration
 */
class m171212_145901_create_how_we_build_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%how_we_build}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Title'),
                'content' => $this->text()->null()->comment('Description'),
                'home_page_id' => $this->integer()->notNull()->comment('Home page'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-how_we_build-home_page_id-home-id',
            $this->tableName,
            'home_page_id',
            '{{%home}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-how_we_build-home_page_id-home-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
