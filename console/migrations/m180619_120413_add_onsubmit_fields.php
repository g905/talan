<?php

use common\models\Configuration;
use console\components\Migration;

/**
 * Class m180619_120413_add_onsubmit_fields migration
 */
class m180619_120413_add_onsubmit_fields extends Migration
{
    public $tableConfig = '{{%configuration}}';

    /**
     * @var string[] migration table names.
     */
    public $tables = [
        '{{%about_company}}',
        '{{%article}}',
        '{{%custom_popup}}',
    ];

    public $configTableKeys = [
        'request_custom_onsubmit' => 'Request Custom popup onsubmit',
        'request_callback_onsubmit' => 'Request Callback onsubmit',
        'request_excursion_onsubmit' => 'Request Excursion onsubmit',
        'request_question_onsubmit' => 'Request Question onsubmit',
        'request_subscribe_onsubmit' => 'Request Subscribe onsubmit',
        'request_commercial_onsubmit' => 'Request Commercial onsubmit',
    ];

    public function safeUp()
    {
        foreach ($this->tables as $table) {
            $this->addFormOnSubmitColumn($table);
        }

        $this->insertConfigOnSubmit();
    }

    public function safeDown()
    {
        foreach ($this->tables as $table) {
            $this->dropFormOnSubmitColumn($table);
        }

        $this->deleteConfigOnSubmit();
    }

    private function addFormOnSubmitColumn($table)
    {
        $this->addColumn($table, 'form_onsubmit', $this->string()->null()->comment('Form OnSubmit'));
    }

    private function dropFormOnSubmitColumn($table)
    {
        $this->dropColumn($table, 'form_onsubmit');
    }

    private function getConfigField($key, $description)
    {
        return [
            'id' => $key,
            'type' => Configuration::TYPE_STRING,
            'description' => $description,
            'value' => null,
            'created_at' => time(),
            'updated_at' => time(),
        ];
    }

    private function insertConfigOnSubmit()
    {
        $rows = [];
        foreach ($this->configTableKeys as $configTableKey => $configTableDescription) {
            $rows[] = $this->getConfigField($configTableKey, $configTableDescription);
        }

        $this->batchInsert($this->tableConfig, ['id', 'type', 'description', 'value', 'created_at', 'updated_at'], $rows);
    }

    private function deleteConfigOnSubmit()
    {
        $this->delete($this->tableConfig, ['id' => array_keys($this->configTableKeys)]);
    }
}
