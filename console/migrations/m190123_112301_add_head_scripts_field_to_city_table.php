<?php

use console\components\Migration;

/**
 * Class m190123_112301_add_head_scripts_field_to_city_table migration
 */
class m190123_112301_add_head_scripts_field_to_city_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $tableName = '{{%city}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'head_script', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'head_script');
    }
}
 