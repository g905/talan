<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m171223_143216_alter_table_apartment_complex_update_fields migration
 */
class m171223_143216_alter_table_apartment_complex_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%apartment_complex}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn($this->tableName, 'top_screen_title',  'label');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameColumn($this->tableName,'label', 'top_screen_title');
    }
}