<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m171224_204946_alter_table_ac_marker_update_fields migration
 */
class m171224_204946_alter_table_ac_marker_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%ac_marker}}';
    public $markerTypeTableName = '{{%ac_marker_type}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-ac_marker-ac_marker_type_id-ac_marker_type-id', $this->tableName);
        $this->dropTable($this->markerTypeTableName);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->createTable(
            $this->markerTypeTableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-ac_marker-ac_marker_type_id-ac_marker_type-id',
            $this->tableName,
            'ac_marker_type_id',
            '{{%ac_marker_type}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }
}