<?php

use console\components\Migration;

/**
 * Class m181010_082119_add_apartment_address_column migration
 */
class m181010_082119_add_apartment_address_column extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'address', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'address');
    }
}
