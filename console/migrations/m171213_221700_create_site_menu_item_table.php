<?php

use console\components\Migration;

/**
 * Class m171213_221700_create_site_menu_item_table migration
 */
class m171213_221700_create_site_menu_item_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%site_menu_item}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'site_menu_id' => $this->integer()->null()->comment('Site menu'),
                'label' => $this->string()->notNull()->comment('Label'),
                'link' => $this->string()->null()->comment('Link'),
                'prepared_page_id' => $this->integer()->null()->comment('Prepared page'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-site_menu_item-site_menu_id-site_menu-id',
            $this->tableName,
            'site_menu_id',
            '{{%site_menu}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-site_menu_item-site_menu_id-site_menu-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
