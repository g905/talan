<?php

use console\components\Migration;

/**
 * Class m181010_131851_create_alternate_widget_table migration
 */
class m181010_131851_create_alternate_widget_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $tableName = '{{%alternate_widget}}';

    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Title'),
                'link' => $this->string()->notNull()->comment('Link'),
                'home_page_id' => $this->integer()->notNull()->comment('Home page'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-alternate_widget-home_page_id-home-id',
            $this->tableName,
            'home_page_id',
            '{{%alternate_widget}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-alternate_widget-home_page_id-home-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
