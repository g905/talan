<?php

use console\components\Migration;

/**
 * Class m180703_145638_create_manager_assign_table migration
 */
class m180703_145638_create_manager_assign_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%manager_assign}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'manager_id' => $this->integer(),
                'object_id' => $this->integer(),
                'type' => $this->tinyInteger()->unsigned()->comment('Object type identifier'),
                'position' => $this->integer(),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-manager_assign-manager_id-manager-id',
            $this->table,
            'manager_id',
            '{{%manager}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-manager_assign-manager_id-manager-id', $this->table);
        $this->dropTable($this->table);
    }
}
