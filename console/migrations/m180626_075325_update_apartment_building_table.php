<?php

use console\components\Migration;

/**
 * Class m180626_075325_update_apartment_building_table migration
 */
class m180626_075325_update_apartment_building_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_building}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'projects_label', $this->string()->after('articles_label')->null()->comment('Projects label'));
        $this->addColumn($this->table, 'projects_description', $this->text()->after('projects_label')->null()->comment('Projects description'));
        $this->renameColumn($this->table, 'form_top_title', 'form_top_title_first');
        $this->addColumn($this->table, 'form_top_title_second', $this->text()->after('form_top_title_first')->null()->comment('Form top title second part'));
        $this->addColumn($this->table, 'alias', $this->string()->after('view_count')->notNull()->comment('Alias'));
        $this->addColumn($this->table, 'custom_popup_id', $this->integer()->null()->comment('Popup'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'projects_label');
        $this->dropColumn($this->table, 'projects_description');
        $this->renameColumn($this->table, 'form_top_title_first', 'form_top_title');
        $this->dropColumn($this->table, 'form_top_title_second');
        $this->dropColumn($this->table, 'alias');
        $this->dropColumn($this->table, 'custom_popup_id');
    }
}
