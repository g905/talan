<?php

use console\components\Migration;

/**
 * Class m180711_123459_create_request_secondary_table migration
 */
class m180711_123459_create_request_secondary_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%request_secondary}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'city_id' => $this->integer()->null()->comment('City'),

                'name' => $this->string()->null()->comment('Name'),
                'phone' => $this->string()->null()->comment('Phone'),

                'square' => $this->string()->null()->comment('Square'),
                'address' => $this->string()->null()->comment('Address'),
                'age' => $this->integer()->null()->comment('Age'),
                'redevelopment' => $this->integer()->null()->comment('Redevelopment'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-request_secondary-city_id-city-id',
            $this->table,
            'city_id',
            '{{%city}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-request_secondary-city_id-city-id', $this->table);
        $this->dropTable($this->table);
    }
}
