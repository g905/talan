<?php

use console\components\Migration;

/**
 * Class m180712_112610_create_how_buy_page_table migration
 */
class m180712_112610_create_how_buy_page_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%how_buy_page}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'city_id' => $this->integer()->notNull()->comment('City'),

                'label' => $this->string()->notNull()->comment('Label'),

                'form_bottom_title' => $this->string()->null()->comment('Form bottom title'),
                'form_bottom_description' => $this->text()->null()->comment('Form bottom description'),
                'form_bottom_email' => $this->text()->null()->comment('Form bottom email'),
                'manager_id' => $this->integer()->null()->comment('Manager ID'),
                'form_bottom_onsubmit' => $this->string()->null()->comment('Form OnSubmit event'),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-how_buy_page-city_id-city-id',
            $this->table,
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-how_buy_page-city_id-city-id', $this->table);
        $this->dropTable($this->table);
    }
}
