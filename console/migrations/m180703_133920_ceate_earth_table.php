<?php

use console\components\Migration;

/**
 * Class m180703_133920_ceate_earth_table migration
 */
class m180703_133920_ceate_earth_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%earth}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'city_id' => $this->integer()->notNull()->comment('City'),

                'top_screen_title' => $this->string()->null()->comment('Title'),
                'top_screen_description' => $this->text()->null()->comment('Description'),
                'presentation_link' => $this->string()->null()->comment('Link'),

                'company_title' => $this->string()->null()->comment('Title'),
                'company_description' => $this->text()->null()->comment('Description'),

                'map_title' => $this->string()->null()->comment('Title'),
                'map_subtitle' => $this->string()->null()->comment('Subtitle'),
                'map_demands' => $this->text()->null()->comment('Demands'),

                'plan_title' => $this->string()->null()->comment('Title'),

                'boss_title' => $this->string()->null()->comment('Title'),
                'boss_name' => $this->string()->null()->comment('Name'),
                'boss_position' => $this->string()->null()->comment('Position'),
                'boss_description' => $this->text()->null()->comment('Description'),

                'offer_title_light' => $this->string()->null()->comment('Title light'),
                'offer_title_dark' => $this->string()->null()->comment('Title dark'),
                'offer_link' => $this->string()->null()->comment('Link'),

                'advantages_title' => $this->string()->null()->comment('Title'),

                'model_title' => $this->string()->null()->comment('Title'),
                'model_description' => $this->text()->null()->comment('Description'),

                'manager_title' => $this->string()->null()->comment('Title'),

                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-earth-city_id-city-id',
            $this->table,
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-earth-city_id-city-id', $this->table);
        $this->dropTable($this->table);
    }
}
