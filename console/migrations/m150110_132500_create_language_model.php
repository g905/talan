<?php

use yii\db\Schema;
use console\components\Migration;

/**
 * Class m150110_132500_create_language_model migration
 */
class m150110_132500_create_language_model extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%language}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string(20)->notNull()->comment('Label'),
                'code' => $this->string(5)->notNull()->unique()->comment('Code'),
                'locale' => $this->string(5)->notNull()->comment('locale'),
                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer(1)->unsigned()->notNull()->defaultValue(0)->comment('Position'),
                'is_default' => $this->smallInteger(1)->notNull()->defaultValue(0)->comment('Is language default'),
                'created_at' => $this->integer()->notNull()->comment('Created at'),
                'updated_at' => $this->integer()->notNull()->comment('Updated at'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
