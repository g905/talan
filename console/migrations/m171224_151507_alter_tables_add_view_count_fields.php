<?php

use common\models\ProductAvailability;
use console\components\Migration;

/**
 * Class m171224_151507_alter_tables_add_view_count_fields migration
 */
class m171224_151507_alter_tables_add_view_count_fields extends Migration
{

    /**
     * migration table name
     */
    public $cityTableName = '{{%city}}';
    public $actionTableName = '{{%promo_action}}';
    public $apartmentTableName = '{{%apartment}}';
    public $complexTableName = '{{%apartment_complex}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->cityTableName, 'view_count', $this->integer()->notNull()->defaultValue(0)->comment('View count'));
        $this->addColumn($this->actionTableName, 'view_count', $this->integer()->notNull()->defaultValue(0)->comment('View count'));
        $this->addColumn($this->apartmentTableName, 'view_count', $this->integer()->notNull()->defaultValue(0)->comment('View count'));
        $this->addColumn($this->complexTableName, 'view_count', $this->integer()->notNull()->defaultValue(0)->comment('View count'));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->cityTableName,'view_count');
        $this->dropColumn($this->actionTableName,'view_count');
        $this->dropColumn($this->apartmentTableName,'view_count');
        $this->dropColumn($this->complexTableName,'view_count');
    }
}