<?php

use console\components\Migration;

/**
 * Class m181029_144730_alter_guarantee_page_table migration
 */
class m181029_144730_alter_guarantee_page_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%guarantee_page}}';

    public function safeUp()
    {
        $this->alterColumn($this->table, 'manager_id', $this->integer()->null()->comment('Manager'));
    }

    public function safeDown()
    {
        $this->alterColumn($this->table, 'manager_id', $this->integer()->notNull()->comment('Manager'));
    }
}
