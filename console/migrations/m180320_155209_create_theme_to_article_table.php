<?php

use console\components\Migration;

/**
 * Class m180320_155209_create_theme_to_article_table migration
 */
class m180320_155209_create_theme_to_article_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%theme_to_article}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'blog_theme_id' => $this->integer()->notNull(),
                'article_id' => $this->integer()->notNull(),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-theme_to_article-blog_theme_id-blog_theme-id',
            $this->tableName,
            'blog_theme_id',
            '{{%blog_theme}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-theme_to_article-article_id-article-id',
            $this->tableName,
            'article_id',
            '{{%article}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-theme_to_article-blog_theme_id-blog_theme-id', $this->tableName);
        $this->dropForeignKey('fk-theme_to_article-article_id-article-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
