<?php

use console\components\Migration;

/**
 * Class m190321_095440_add_custom_choose_link_column_to_apartment_complex_table migration
 */
class m190321_095440_add_custom_choose_link_column_to_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'custom_choose_link', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'custom_choose_link');
    }
}
