<?php

use console\components\Migration;

/**
 * Class m180810_084719_create_constructor_request_table migration
 */
class m180810_084719_create_constructor_request_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%constructor_request}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'price' => $this->string()->notNull(),
            'city_id' => $this->integer()->notNull(),
            'complex_id' => $this->integer()->notNull(),
            'flat_id' => $this->integer()->notNull(),
            'data' => $this->text()->notNull(),
            'type' => $this->string()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
