<?php

use console\components\Migration;

/**
 * Class m180426_124528_create_benefit_table migration
 */
class m180426_124528_create_benefit_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%benefit}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'title' => $this->string()->notNull()->comment('Title'),
                'description' => $this->text()->defaultValue(null)->comment('Description'),
                'published' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1)->comment('Published'),
            ],
            $this->tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
