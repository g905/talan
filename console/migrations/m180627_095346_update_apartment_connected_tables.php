<?php

use console\components\Migration;

/**
 * Class m180627_095346_update_apartment_connected_tables migration
 */
class m180627_095346_update_apartment_connected_tables extends Migration
{
    const FK_APARTMENT_BUILDING = 'fk_apartment_building_id-building-id';

    /**
     * @var string migration table names.
     */
    public $tableComplex = '{{%apartment_complex}}';
    public $tableBuilding = '{{%apartment_building}}';
    public $tableApartment = '{{%apartment}}';

    public function safeUp()
    {
        $this->alterColumn($this->tableApartment, 'entrance', $this->smallInteger()->null()->defaultValue(null));
        $this->addColumn($this->tableApartment, 'apartment_building_id', $this->integer()->null());
        $this->addColumn($this->tableApartment, 'apartment_number', $this->integer()->null());
        $this->addColumn($this->tableApartment, 'apartment_number_on_floor', $this->integer()->null());
        $this->addColumn($this->tableApartment, 'installment_price', $this->decimal(15, 2)->notNull()->defaultValue(0));
        $this->addColumn($this->tableApartment, 'price_per_meter', $this->decimal(15, 2)->notNull()->defaultValue(0));
        $this->addColumn($this->tableApartment, 'discount', $this->decimal(15, 2)->notNull()->defaultValue(0));
        $this->addColumn($this->tableApartment, 'is_promotional', $this->boolean()->defaultValue(false));
        $this->addColumn($this->tableApartment, 'is_secondary', $this->boolean()->defaultValue(false));
        $this->addColumn($this->tableApartment, 'status', $this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn($this->tableApartment, 'deleted', $this->boolean()->defaultValue(false));

        $this->addForeignKey(self::FK_APARTMENT_BUILDING, $this->tableApartment, 'apartment_building_id', $this->tableBuilding, 'id', 'SET NULL', 'CASCADE');

        $this->addColumn($this->tableBuilding, 'entrance', $this->smallInteger()->null()->defaultValue(null));
        $this->addColumn($this->tableBuilding, 'floors', $this->integer()->null());
        $this->addColumn($this->tableBuilding, 'block_number', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_APARTMENT_BUILDING, $this->tableApartment);

        // $this->alterColumn($this->tableApartment, 'entrance', $this->smallInteger()->notNull()->defaultValue(0));
        $this->dropColumn($this->tableApartment, 'apartment_building_id');
        $this->dropColumn($this->tableApartment, 'apartment_number');
        $this->dropColumn($this->tableApartment, 'apartment_number_on_floor');
        $this->dropColumn($this->tableApartment, 'installment_price');
        $this->dropColumn($this->tableApartment, 'price_per_meter');
        $this->dropColumn($this->tableApartment, 'discount');
        $this->dropColumn($this->tableApartment, 'is_promotional');
        $this->dropColumn($this->tableApartment, 'is_secondary');
        $this->dropColumn($this->tableApartment, 'status');
        $this->dropColumn($this->tableApartment, 'deleted');

        $this->dropColumn($this->tableBuilding, 'entrance');
        $this->dropColumn($this->tableBuilding, 'floors');
        $this->dropColumn($this->tableBuilding, 'block_number');
    }
}
