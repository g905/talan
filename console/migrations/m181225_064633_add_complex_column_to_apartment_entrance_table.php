<?php

use console\components\Migration;

/**
 * Class m181225_064633_add_complex_column_to_apartment_entrance_table migration
 */
class m181225_064633_add_complex_column_to_apartment_entrance_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $tableName = '{{%apartment_entrance}}';

    public function safeUp()
    {
        $this->addColumn('apartment_entrance', 'building_complex_id', $this->integer());
        $this->addForeignKey(
            'fk-entrance-building_complex_id-apartment_complex-id',
            $this->tableName,
            'building_complex_id',
            '{{%apartment_complex}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-entrance-building_complex_id-apartment_complex-id', $this->tableName);
        $this->dropColumn($this->tableName, 'building_complex_id');
    }
}
