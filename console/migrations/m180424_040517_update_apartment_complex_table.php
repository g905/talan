<?php

use console\components\Migration;

/**
 * Class m180424_040517_update_apartment_complex_table migration
 */
class m180424_040517_update_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'map_zoom', $this->smallInteger()->defaultValue(15)->comment('GMap zoom'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'map_zoom');
    }
}
