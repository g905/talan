<?php
use console\components\Migration;

class m190329_072948_agency_api extends Migration
{

	public function safeUp()
	{
		$this->createTable('{{api_agency}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(255)->notNull(),
			'email' => $this->string(50)->notNull(),
			'active' => $this->smallInteger()->defaultValue(0)->notNull(),
			'callback_url' => $this->string(240),
			'token' => $this->string(128)->notNull(),
			'show_price' => $this->smallInteger()->notNull()->defaultValue(0),
			'created_at' => $this->timestamp()->defaultExpression('NOW()')->notNull(),
			'updated_at' => $this->timestamp()->defaultExpression('NOW()')->notNull(),
		], $this->tableOptions);

		$this->createIndex('ix-api_agency-active-code', '{{api_agency}}', ['active', 'token']);
		$this->createIndex('uk-api_agency-email', '{{api_agency}}', ['email'], true);
		$this->execute('CREATE TRIGGER `api_agency_BEFORE_UPDATE` BEFORE UPDATE ON `api_agency` FOR EACH ROW BEGIN 	SET NEW.updated_at = NOW(); END');

		$this->createTable('{{api_agency_to_apartment_complex}}', [
			'api_agency_id' => $this->integer()->notNull(),
			'apartment_complex_id' => $this->integer()->notNull(),
		], $this->tableOptions);

		$this->addPrimaryKey('pk-api_atab-aai-abi', '{{api_agency_to_apartment_complex}}', ['api_agency_id', 'apartment_complex_id']);

		// количество посещений виджета
		$this->createTable('{{api_agency_stat_visits}}', [
			'id' => $this->bigPrimaryKey(),
			'api_agency_id' => $this->integer()->notNull(),
			'created_at' => $this->timestamp()->notNull()->defaultExpression('NOW()'),
		], $this->tableOptions);

		//$this->createIndex('ix-api_agency_stat_visits-1', 'api_agency_stat_visits', ['api_agency_id', 'created_at']);
		$this->addForeignKey('fk-api_agency_stat-aai', '{{api_agency_stat_visits}}', ['api_agency_id'], '{{api_agency}}', ['id'], 'CASCADE', 'CASCADE');

		$this->createTable('{{api_customer}}', [
			'id' => $this->bigPrimaryKey(),
			'name' => $this->string(50)->notNull(),
			'phone' => $this->string(21)->notNull(),
			'email' => $this->string(50),
			'comment' => $this->text(),
			// не привязываем FK
			'apartment_id' => $this->integer()->notNull(),
			'created_at' => $this->timestamp()->notNull()->defaultExpression('NOW()'),
		], $this->tableOptions);
	}

	public function safeDown()
	{
		$this->dropTable('{{api_agency_to_apartment_complex}}');
		$this->dropTable('{{api_agency_stat_visits}}');

		$this->dropTable('{{api_agency}}');
		$this->dropTable('{{api_customer}}');

		return true;
	}
}
