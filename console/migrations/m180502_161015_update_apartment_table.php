<?php

use console\components\Migration;

/**
 * Class m180502_161015_update_apartment_table migration
 */
class m180502_161015_update_apartment_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment}}';

    public function safeUp()
    {
        app()->getDb()->createCommand()->update($this->table, ['total_area' => 0], ['total_area' => null])->execute();
        app()->getDb()->createCommand()->update($this->table, ['living_space' => 0], ['living_space' => null])->execute();
        app()->getDb()->createCommand()->update($this->table, ['price_from' => 0], ['price_from' => null])->execute();

        $this->alterColumn($this->table, 'total_area', $this->decimal(10,2)->notNull()->defaultValue(0)->comment('Total area'));
        $this->alterColumn($this->table, 'living_space', $this->decimal(10,2)->notNull()->defaultValue(0)->comment('Living space'));
        $this->alterColumn($this->table, 'price_from', $this->decimal(15,2)->notNull()->defaultValue(0)->comment('Price from'));
    }

    public function safeDown()
    {
        $this->alterColumn($this->table, 'total_area', $this->decimal(10,2)->null()->comment('Total area'));
        $this->alterColumn($this->table, 'living_space', $this->decimal(10,2)->null()->comment('Living space'));
        $this->alterColumn($this->table, 'price_from', $this->decimal(15,2)->null()->comment('Price from'));
    }
}
