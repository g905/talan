<?php

use console\components\Migration;

/**
 * Class m180510_090020_update_city_table migration
 */
class m180510_090020_update_city_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%city}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'link_ig', $this->string()->null()->comment('Social IG link'));
        $this->addColumn($this->table, 'link_ok', $this->string()->null()->comment('Social OK link'));
        $this->addColumn($this->table, 'link_vk', $this->string()->null()->comment('Social VK link'));
        $this->addColumn($this->table, 'link_fb', $this->string()->null()->comment('Social FB link'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'link_ig');
        $this->dropColumn($this->table, 'link_ok');
        $this->dropColumn($this->table, 'link_vk');
        $this->dropColumn($this->table, 'link_fb');
    }
}
