<?php

use console\components\Migration;

/**
 * Class m180622_232409_create_sub_menu_table migration
 */
class m180622_232409_create_sub_menu_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%sub_menu}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'type' => $this->tinyInteger(),
            'object_id' => $this->integer(),
            'label' => $this->string()->notNull(),
            'link' => $this->string()->null(),
            'prepared_page_id' => $this->string()->null(), // ¯\_(ツ)_/¯
            'show_right' => $this->boolean()->notNull()->defaultValue(false),
            'published' => $this->boolean()->notNull()->defaultValue(1),
            'position' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
