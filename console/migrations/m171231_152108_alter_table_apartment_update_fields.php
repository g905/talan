<?php

use console\components\Migration;
use frontend\modules\cabinet\models\AuthForm;

/**
 * Class m171231_152108_alter_table_apartment_update_fields migration
 */
class m171231_152108_alter_table_apartment_update_fields extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%apartment}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'delivery_date', $this->string()->null()->comment('Delivery date'));
        $this->alterColumn($this->tableName, 'floor', $this->integer()->null()->comment('Floor number'));
        $this->addColumn($this->tableName, 'guid',  $this->string()->null()->comment('1C Guid'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'floor', $this->integer()->notNull()->comment('Floor number'));
        $this->alterColumn($this->tableName, 'delivery_date', $this->date()->null()->comment('Delivery date'));
        $this->dropColumn($this->tableName, 'guid');
    }
}