<?php

use console\components\Migration;

/**
 * Class m180614_202801_alter_manager_table_add_contact_email_column migration
 */
class m180614_202801_alter_manager_table_add_contact_email_column extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%manager}}';

    /**
     * @return bool|void
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'contact_email', $this->string()->defaultValue(null)->null()->comment('Contact email'));
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'contact_email');
    }
}
