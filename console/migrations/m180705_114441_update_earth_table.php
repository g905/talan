<?php

use console\components\Migration;

/**
 * Class m180705_114441_update_earth_table migration
 */
class m180705_114441_update_earth_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%earth}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'boss_video', $this->string()->null()->comment('Video Link')->after('boss_description'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'boss_video');
    }
}
