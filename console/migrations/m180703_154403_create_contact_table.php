<?php

use console\components\Migration;

/**
 * Class m180703_154403_create_contact_table migration
 */
class m180703_154403_create_contact_table extends Migration
{
    const FK_CITY = 'fk-contact_page-city_id-city-id';
    const FK_MANAGER = 'fk-contact_form-manager_id-manager-id';
    const FK_MARKER = 'fk-contact_marker-contact_page_id-contact_page-id';

    /**
     * @var string migration table name.
     */
    public $table = '{{%contact_page}}';
    public $tableForm = '{{%contact_form}}';
    public $tableMarker = '{{%contact_marker}}';
    public $tableCity = '{{%city}}';
    public $tableManager = '{{%manager}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->null(),
            'label' => $this->string()->notNull(),
            'sales_phones' => $this->string()->null(),
            'reception_phones' => $this->string()->null(),
            'address' => $this->string()->null(),
            'email' => $this->string()->null(),
            'timetable' => $this->text()->null(),
            'hot_to_get_label' => $this->string()->null(),
            'forms_label' => $this->string()->null(),
            'map_zoom' => $this->smallInteger()->notNull()->defaultValue(15),
            'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'view_count' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createTable($this->tableForm, [
            'id' => $this->primaryKey(),
            'contact_page_id' => $this->integer(),
            'nav_label' => $this->string()->notNull(),
            'form_label' => $this->string()->null(),
            'form_description' => $this->text()->null(),
            'form_agreement' => $this->text()->null(),
            'manager_id' => $this->integer()->null(),
            'position' => $this->integer()->notNull()->defaultValue(0),
            'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createTable($this->tableMarker, [
            'id' => $this->primaryKey(),
            'contact_page_id' => $this->integer(),
            'lat' => $this->decimal(13,8)->null(),
            'lng' => $this->decimal(13,8)->null(),
            'label' => $this->string()->null(),
            'phone' => $this->string()->null(),
            'address' => $this->string()->null(),
            'published' => $this->boolean()->notNull()->defaultValue(1),
            'position' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey(self::FK_CITY, $this->table, 'city_id', $this->tableCity, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey(self::FK_MANAGER, $this->tableForm, 'manager_id', $this->tableManager, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey(self::FK_MARKER, $this->tableMarker, 'contact_page_id', $this->table, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_MARKER, $this->tableMarker);
        $this->dropForeignKey(self::FK_MANAGER, $this->tableForm);
        $this->dropForeignKey(self::FK_CITY, $this->table);

        $this->dropTable($this->tableMarker);
        $this->dropTable($this->tableForm);
        $this->dropTable($this->table);
    }
}
