<?php

use console\components\Migration;

/**
 * Class m180521_132138_update_apartment_complex_table migration
 */
class m180521_132138_update_apartment_complex_table extends Migration
{
    /**
     * @var string migration table name.
     */
    public $table = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'show_label', $this->boolean()->defaultValue(true)->comment('Whether to show label'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'show_label');
    }
}
