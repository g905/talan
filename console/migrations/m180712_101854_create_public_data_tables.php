<?php

use console\components\Migration;

/**
 * Class m180712_101854_create_public_data_tables migration
 */
class m180712_101854_create_public_data_tables extends Migration
{
    const FK_MENU = 'fk-pd-menu_id-pdm-id';
    const FK_CHART = 'fk-pdcd-widget_id-pdw-id';
    const FK_COMPLEX = 'fk-pdw-complex_id-ac-id';

    /**
     * @var string migration table names.
     */
    public $table = '{{%public_data}}';
    public $tableMenu = '{{%public_data_menu}}';
    public $tableWidget = '{{%public_data_widget}}';
    public $tableChartData = '{{%public_data_chart_data}}';
    public $tableComplex = '{{%apartment_complex}}';

    public function safeUp()
    {
        $this->createTable($this->tableMenu, [
            'id' => $this->primaryKey(),
            'label' => $this->string()->notNull(),

            'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'position' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'menu_id' => $this->integer(),

            'label' => $this->string()->notNull(),
            'alias' => $this->text()->defaultValue(null),

            'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'position' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createTable($this->tableWidget, [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->notNull(),
            'city_id' => $this->integer()->null(),
            'complex_id' => $this->integer()->null(),
            'label' => $this->string()->null(),
            'chart_name' => $this->string()->null(),
            'link' => $this->string()->null(),
            'value' => $this->string()->null(),
            'status' => $this->boolean()->defaultValue(true),
            'placed' => $this->string()->null(),
            'seria' => $this->string()->null(),
            'plan' => $this->integer()->null(),
            'fact' => $this->integer()->null(),
            'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'position' => $this->integer()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createTable($this->tableChartData, [
            'id' => $this->primaryKey(),
            'widget_id' => $this->integer(),
            'label' => $this->string()->null(),
            'value' => $this->string()->null(),
            'color' => $this->string()->null(),
            'position' => $this->integer()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->addForeignKey(self::FK_MENU, $this->table, 'menu_id', $this->tableMenu, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey(self::FK_CHART, $this->tableChartData, 'widget_id', $this->tableWidget, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_COMPLEX, $this->tableWidget, 'complex_id', $this->tableComplex, 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_COMPLEX, $this->tableWidget);
        $this->dropForeignKey(self::FK_CHART, $this->tableChartData);
        $this->dropForeignKey(self::FK_MENU, $this->table);
        $this->dropTable($this->tableChartData);
        $this->dropTable($this->tableWidget);
        $this->dropTable($this->table);
        $this->dropTable($this->tableMenu);
    }
}
