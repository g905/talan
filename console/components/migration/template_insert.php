<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */
/* @var $tableName string the new migration table name */

echo "<?php\n";
?>

use console\components\Migration;

/**
 * Class <?= $className ?> migration
 */
class <?= $className ?> extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%<?= $tableName; ?>}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $time = time();

        $this->insert($this->tableName, [
            'id' => 'id',
            'type' => Configuration::TYPE_STRING,
            'description' => 'Description',
            'value' => '',
            'created_at' => $time,
            'updated_at' => $time,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 'id']);
    }
}
