<?php
/**
 * Created by PhpStorm.
 * User: dvixi
 * Date: 4/17/16
 * Time: 3:55 PM
 */

namespace console\controllers;


use common\models\Article;
use common\models\BuilderWidget;
use common\models\BuilderWidgetAttribute;
use common\models\Manager;
use yii\console\Controller;

class ExportController extends Controller
{
    public function actionIndex($db, $login, $password)
    {
        $connection = new \yii\db\Connection([
            'dsn' => 'mysql:host=localhost;dbname='.$db,
            'username' => $login,
            'password' => $password,
            'charset' => 'utf8mb4',
        ]);
        $connection->open();

        $command = $connection->createCommand('SELECT * FROM news');
        $posts = $command->queryAll();
        $command->execute();

        $this->actionSave($posts);
    }

    public function actionSave($posts)
    {
        $managers = Manager::find()->all();
        $count = count($managers) - 1;

        foreach ($posts as $key => $post) {
            $article = new Article();
            $article->label = strip_tags($post['title']);
            $article->alias = $this->translit(strip_tags($post['title'])).'-'.$key;
            $article->published = 1;
            $article->position = 0;
            $article->date_published = $post['date'];
            $article->show_on_home = 0;
            $article->manager_id = $managers[rand(0,$count)]->id;
            $article->short_description = mb_substr(strip_tags($post['text']), 0, 300) . (strlen(strip_tags($post['text'])) > 300 ? '...' : '');
            $article->form_title = 'Мы всегда готовы ответить на любые ваши вопросы';
            $article->form_description = 'Мы поможем подобрать то, что вам подойдет лучше всего. Оставьте ваши данные и наш специалист перезвонит и проконсультирует по любому вопросу.';
            $article->form_agreement = '<p>Даю <a href="#" target="_blank">согласие</a> на обработку моих персональных данных, с условиями <a href="#" target="_blank">Политики</a> ознакомлен.</p>';
            $article->save();
            $widget = new BuilderWidget();
            $widget->target_class = 'Article';
            $widget->target_id = $article->id;
            $widget->target_attribute = 'content';
            $widget->widget_class = 'common\models\builder\BlogHtmlBuilderModel';
            $widget->position = 0;
            $widget->widget_visible = 1;
            $widget->save();
            $attr = new BuilderWidgetAttribute();
            $attr->widget_id = $widget->id;
            $attr->attribute = 'text';
            $attr->value = strip_tags($post['text']);
            $attr->save();
        }
    }

    public function translit($str){
        $translit=array(
            "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g","Д"=>"d","Е"=>"e","Ё"=>"e","Ж"=>"zh","З"=>"z","И"=>"i","Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n","О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t","У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch","Ш"=>"sh","Щ"=>"shch","Ъ"=>"","Ы"=>"y","Ь"=>"","Э"=>"e","Ю"=>"yu","Я"=>"ya",
            "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"zh","з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l","м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r","с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h","ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"shch","ъ"=>"","ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
            "A"=>"a","B"=>"b","C"=>"c","D"=>"d","E"=>"e","F"=>"f","G"=>"g","H"=>"h","I"=>"i","J"=>"j","K"=>"k","L"=>"l","M"=>"m","N"=>"n","O"=>"o","P"=>"p","Q"=>"q","R"=>"r","S"=>"s","T"=>"t","U"=>"u","V"=>"v","W"=>"w","X"=>"x","Y"=>"y","Z"=>"z"
        );
        $result=strtr($str,$translit);
        $result=preg_replace("/[^a-zA-Z0-9_]/i","-",$result);
        $result=preg_replace("/\-+/i","-",$result);
        $result=preg_replace("/(^\-)|(\-$)/i","",$result);
        return $result;
    }
}