<?php

namespace console\controllers;

use backend\modules\ftpModel\models\FVacancy;
use Exception;
use notgosu\yii2\modules\metaTag\models\MetaTagContent;
use yii\helpers\Json;
use yii\console\Controller;
use backend\modules\ftpModel\models\FPhoto;
use backend\modules\ftpModel\models\FApartment;
use yii\base\Event;
use common\models\RequestCallback;
use frontend\modules\form\models\FormCallback;

/**
 * Class FtpController
 *
 * @author art
 * @package console\controllers
 */
class FtpController extends Controller
{

    public function actionHeadhunterVacancies()
    {

        try {

            $vacancies = new FVacancy();
            //$vacancies->deactivateVacsAndSpecs();
            $vacancies->getVacanciesList();
            //dd($vacs);
        }
        catch (Exception $e){
            echo Json::encode($e);
        }
    }

    public function actionDeactivateUnusedVacancies()
    {
        $fvac = new FVacancy();
        $fvac->deactivateVacanciesFromUnavailableCities();
    }

    public function actionHeadhunterDeleteAll()
    {
        try {

            $vacancies = new FVacancy();
            $vacancies->deleteVacancies();
            //dd($vacs);
        }
        catch (Exception $e){
            echo Json::encode($e);
        }
    }

    public function actionSynchronizeApartment()
    {
        try{
            $ftpApartment = new FApartment();
            $ftpApartment->synchronizeWithDb();
        }
        catch (Exception $e){
            echo Json::encode($e);
        }
    }

    public function actionSynchronizeApartmentNew()
    {
        try{
            $ftpApartment = new FApartment();
            $ftpApartment->synchronizeWithDbNew();
        }
        catch (Exception $e){
            echo Json::encode($e);
        }
    }

    public function actionSynchronizePhoto()
    {
        try{
            $ftpApartment = new FPhoto();
            $ftpApartment->synchronizeWithDb();
        }
        catch (Exception $e){
            echo Json::encode($e);
        }
    }

    public function actionDeletePhoto()
    {
        $delete = new FPhoto();
        $delete->deletePhoto();
    }

    public function actionLead()
    {
    	$formatter = \Yii::$app->formatter;
        $d = $formatter->asTimestamp('6-01-2019');
        //echo $d;
		$callbacks = RequestCallback::find()->where(['>','created_at',$d])->all();
		//dump($callbacks[0]->toArray());
		$test_callback = $callbacks[1]->toArray();
		$test_callback["ac_id"] = null;


		\Yii::$app->trigger('bitrixData', new \yii\base\Event(['sender' => ['title' => 'Обратный звонок', 'source_description' => 'заявка с сайта - Обратный звонок', 'form' => $test_callback]]));

		foreach ($callbacks as $cb) {
			//echo $cb->name."; ";
			//echo $cb->id."<br>";
			//echo  $formatter->asDate($cb->created_at, 'long')."<br>"; 
			
		}
    }

    public function actionSeoTags()
    {
        $metatags = MetaTagContent::find()
            ->alias('m')
            ->where(['m.model_name' => 'Article'])
            ->innerJoin('article', 'm.model_id = article.id')
            ->select(['article.*', 'm.*'])
            ->asArray()
            ->all();

        foreach($metatags as $article)
        {
            $title = $article["label"]." — Новости «Талана»";
            $keywords = "застройщик, недвижимость, статья, блог, журнал, новость";
            $description = $article["short_description"];

            $meta = MetaTagContent::find()
                ->where(['meta_tag_id' => $article["meta_tag_id"], 'model_name' => 'Article', 'model_id' => $article['id']])
                ->one();

            switch ($meta->meta_tag_id)
            {
                case 1:
                    {
                        $meta->content = $title;
                        break;
                    }
                case 2:
                    {
                        $meta->content = $keywords;
                        break;
                    }
                case 3:
                    {
                        $meta->content = $description;
                        break;
                    }
                default: break;
            }

            if($meta->save(false))
            {
                dump($meta->content);
            } else {
                dump("not saved");
            }
        }
    }
}
