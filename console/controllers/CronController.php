<?php

namespace console\controllers;

use backend\modules\ftpModel\models\base\BaseFtpPhotoModel;
use yii\console\ExitCode;
use yii\console\Controller;
use yii\base\InvalidConfigException;
use yii\helpers\Console;
use yii2tech\crontab\CronTab;
use yii2tech\crontab\CronJob;

/**
 * Manages cron jobs.
 *
 * @author Bogdan K. Fedun <delagics@gmail.com>
 * @package console\controllers
 */
class CronController extends Controller
{
    /**
     * @var array options to be past to CronTab
     * @see CronTab
     */
    public $jobsManagerConfig = [];
    /**
     * @var array jobs pull.
     * Example of specifying jobs in three different ways (string|array|callable):
     *
     * ```
     * CronController::$jobs = [
     *    '1 * * * * php yii controller/action --arg',
     *    [
     *        'min' => '0',
     *        'hour' => '0',
     *        'weekDay' => '5',
     *        'command' => 'php /path/to/project/yii weekly-cron',
     *    ],
     *    function (CronTab $manager, string $yiiExecutable) {
     *        // return "php $yiiExecutable controller/action";
     *        return new CronJob();
     *    },
     * ];
     * ```
     */
    private $jobs = [];
    /**
     * @var CronTab cron tab manager.
     */
    private $jobsManager;

    public function actionTesting1()
    {
        $base = new BaseFtpPhotoModel();
        $base->getFile();
    }


    /**
     * Register all available project cron jobs, specified via configuration.
     *
     * @return int
     * @throws \yii\base\Exception
     */
    public function actionUp()
    {
        if (empty($this->getJobs())) {
            $this->stdout("No jobs specified to be added to crontab.\n", Console::FG_YELLOW);
            $this->listCurrentJobs();
            return ExitCode::OK;
        }

        $manager = $this->getJobsManager();
        $manager->setJobs($this->getJobs());
        $manager->apply();
        $this->listCurrentJobs();

        return ExitCode::OK;
    }

    /**
     * Removes cron jobs from crontab.
     *
     * @return int
     * @throws \yii\base\Exception
     */
    public function actionDown()
    {
        $manager = $this->getJobsManager();
        if ($this->confirm("Answer 'yes' if you want remove all crontab jobs?\nOtherwise, only jobs from config will be removed.", false)) {
            // Removing all cron jobs.
            $manager->removeAll();
        } else {
            // Removing only jobs specified in jobs property.
            $manager->setJobs($this->getJobs());
            $manager->remove();
        }

        $this->listCurrentJobs();

        return ExitCode::OK;
    }

    /**
     * List all cron jobs.
     *
     * @throws \yii\base\Exception
     */
    public function actionList()
    {
        $this->listCurrentJobs();

        return ExitCode::OK;
    }

    /**
     * List crontab lines.
     *
     * @throws \yii\base\Exception
     */
    private function listCurrentJobs()
    {
        $lines = $this->getJobsManager()->getCurrentLines();
        if ($lines) {
            $this->stdout("Here is the current list of crontab jobs:\n", Console::BOLD, Console::FG_PURPLE);
            $this->stdout(implode("\n", $this->getJobsManager()->getCurrentLines()) . "\n", Console::FG_GREEN);
        } else {
            $this->stdout("Crontab is empty.\n", Console::BOLD, Console::FG_PURPLE);
        }
    }

    /**
     * Jobs getter.
     *
     * @return CronJob[]
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Jobs setter.
     *
     * @param array|string[]|callable[] $jobs
     * @throws InvalidConfigException
     */
    public function setJobs(array $jobs)
    {
        if ($jobs) {
            foreach ($jobs as $k => $jobConfig) {
                $this->jobs[$k] = $this->prepareJob($jobConfig);
            }
        }
    }

    /**
     * Prepare cron job from given configuration.
     *
     * @param string|array|callable $config in case it is callable it must return CronJob instance.
     * @return CronJob
     * @throws InvalidConfigException
     */
    private function prepareJob($config)
    {
        if ($config && is_string($config)) {
            return (new CronJob())->setLine($config);
        }

        if ($config && is_array($config)) {
            return new CronJob($config);
        }

        if (is_callable($config)) {
            $yiiExecutablePath = implode(DIRECTORY_SEPARATOR, [dirname(__DIR__, 2), 'yii']);
            $job = call_user_func($config, $this->getJobsManager(), $yiiExecutablePath);

            return $job instanceof CronJob ? $job : $this->prepareJob($job);
        }

        throw new InvalidConfigException('Cannot create cron job from given configuration');
    }

    /**
     * @return CronTab
     */
    public function getJobsManager()
    {
        if ($this->jobsManager === null) {
            $this->jobsManager = createObject(CronTab::class, $this->jobsManagerConfig);
        }

        return $this->jobsManager;
    }
}
