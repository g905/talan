<?php

use yii2tech\crontab\CronTab;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'aliases' => [
        '@webroot' => dirname(dirname(__FILE__)) . '/web',
    ],
    'controllerMap' => [
        'migrate' => [
            'class' => '\console\controllers\MigrateController',
        ],
        'cron' => [
            'class' => console\controllers\CronController::class,
            'jobsManagerConfig' => [
                // 'username' => 'laradock',
            ],
            'jobs' => [
                // Send emails (every 5 minutes)
                function (CronTab $manager, string $yii) {
                    return "*/5 * * * * php $yii send/email";
                },
                // Import apartments data from csv file (everyday at 00:00)
                function (CronTab $manager, string $yii) {
                    return "0 0 * * * php $yii ftp/synchronize-apartment";
                },
                // Import apartments photos (everyday at 01:00)
                function (CronTab $manager, string $yii) {
                    return "0 1 * * * php $yii ftp/synchronize-photo";
                },
            ],
        ],
        // Tinker is used for testing purposes
        'tinker' => [
            'class' => Yii2Tinker\TinkerController::class,
        ],
    ],
    'components' => [
        'errorHandler' => [
            'class' => yii\console\ErrorHandler::class,
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'class' => '\common\components\UrlManager',
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'enableLanguageDetection' => false,
            'rules' => require(__DIR__ . '/../../common/config/url-rules.php'),
        ],
    ],
    'params' => $params,
];
