<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 06.04.18
 * Time: 13:14
 */

namespace common\helpers;

use common\models\City;
use yii\helpers\ArrayHelper;
use rmrevin\yii\postman\ViewLetter;

/**
 * Class MailerHelper
 * @package common\helpers
 */
class MailerHelper
{

    /**
     * @param $subject
     * @param $data
     * @param array $notifyEmails
     * @param bool $appendCity
     * @param bool $sendNow
     * @param bool $file
     * @return bool
     * @throws \rmrevin\yii\postman\LetterException
     */
    public static function sendNotificationEmail($subject, $data, $notifyEmails = [], $appendCity = true, $sendNow = false, $file = false)
    {
        $sendNow = true;
        $city = City::getUserCity();
        //$notifyEmails = empty($notifyEmails) ? $city->getNotificationEmailsList() : $notifyEmails;
        //$notifyEmails = [];

        if ($appendCity) {
            $data = ArrayHelper::merge(['city' => $city->label ?? 'Не определно'], $data);
        }

        $content = DetailTableView::widget(['data' => $data]);
        $letter = (new ViewLetter())->setSubject($subject)->setBody($content);
        $notifyEmails = empty($notifyEmails) ? ConfigHelper::getAdminNotificationEmails() : $notifyEmails;

        if ($file) {
            $letter->addAttachment(getAlias('@webroot').'/uploads/pdf/'.$file.'.pdf', 'Заявка.pdf');
        }

        foreach ($notifyEmails as $email) {
            $letter->addAddress($email);
        }

        if (!$letter->send($sendNow)) {
            return false;
        }

        return true;
    }

    public static function sendNotificationEmailClient($subject, $data, $notifyEmails, $appendCity = true, $sendNow = false, $file = false)
    {
        $sendNow = true;
        $city = City::getUserCity();
        //$notifyEmails = empty($notifyEmails) ? $city->getNotificationEmailsList() : $notifyEmails;
        //$notifyEmails = [];

        if ($appendCity) {
            $data = ArrayHelper::merge(['city' => $city->label ?? 'Не определно'], $data);
        }

        $content = DetailTableView::widget(['data' => $data]);
        $letter = (new ViewLetter())->setSubject($subject)->setBody($content);
        $notifyEmails = empty($notifyEmails) ? ConfigHelper::getAdminNotificationEmails() : $notifyEmails;

        if ($file) {
            $letter->addAttachment(getAlias('@webroot').'/uploads/pdf/'.$file.'.pdf', 'Заявка.pdf');
        }


            $letter->addAddress($notifyEmails);


        if (!$letter->send($sendNow)) {
            return false;
        }

        return true;
    }

    /**
     * @param $subject
     * @param $email
     * @param $data
     * @param bool $sendNow
     * @param $file
     * @return bool
     * @throws \rmrevin\yii\postman\LetterException
     */
    public static function sendClientEmail($subject, $email, $data, $sendNow = false, $file)
    {
        $content = DetailTableView::widget(['data' => $data]);

        $letter = (new ViewLetter())
            ->setSubject($subject)
            ->setBody($content);

        $letter->addAttachment(getAlias('@webroot').'/uploads/pdf/'.$file.'.pdf', 'Заявка.pdf');

        $letter->addAddress($email);

        if (!$letter->send($sendNow)) {
            return false;
        }

        return true;
    }
}
