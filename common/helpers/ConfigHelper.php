<?php

namespace common\helpers;

use frontend\helpers\FrontendHelper;
use metalguardian\fileProcessor\helpers\FPM;


/**
 * Site controller
 */
class ConfigHelper
{
    /**
     * @param $configKey
     * @param $defaultValue
     * @return mixed
     */
    public static function isKeyExist($configKey, $defaultValue)
    {
        $configValue = \Yii::$app->config->get($configKey);
        return $configValue ? $configValue : $defaultValue;
    }


    /**
     * @return mixed
     */
    public static function getHeaderPhone($city)
    {
        if (FrontendHelper::isNullOrEmpty($city->phone)) {
            return self::isKeyExist('headerPhone', '');
        }
        else{
            return $city->phone;
        }
    }

    /**
     * @return mixed
     */
    public static function getGlobalCallbackOnSubmit()
    {
        return self::isKeyExist('globalCallbackOnSubmit', "ga('send', 'event', 'form', 'zakaz_zvonka'); yaCounter47010621.reachGoal('form6'); return true;");
    }

    /**
     * @return mixed
     */
    public static function getApartmentExcursionOnSubmit()
    {
        return self::isKeyExist('apartmentExcursionOnSubmit', "ga('send', 'event', 'form', 'forma_kv'); yaCounter47010621.reachGoal('form7'); return true;");
    }

    /**
     * @return mixed
     */
    public static function getVkPageLink()
    {
        return self::isKeyExist('vkPageLink', 'https://vk.com/');
    }

    /**
     * @return mixed
     */
    public static function getFbPageLink()
    {
        return self::isKeyExist('fbPageLink', 'https://www.facebook.com/');
    }

    /**
     * @return mixed
     */
    public static function getOkPageLink()
    {
        return self::isKeyExist('okPageLink', 'https://ok.ru/');
    }

    /**
     * @return mixed
     */
    public static function getInstagramPageLink()
    {
        return self::isKeyExist('instagramPageLink', 'https://www.instagram.com/');
    }

    /**
     * @return mixed
     */
    public static function getHeaderPhoneFormatted($city)
    {
        if (FrontendHelper::isNullOrEmpty($city->phone)) {
            $phone = self::isKeyExist('headerPhone', '');
            $phone = preg_replace('/[^0-9]/', '', $phone);
            return $phone;
        }
        else{
            $phone =  $city->phone;
            $phone = preg_replace('/[^0-9]/', '', $phone);
            return $phone;
        }

    }

    /**
     * @return mixed
     */
    public static function getApartmentsPerPage()
    {
        return self::isKeyExist('apartmentsPerPage', 9);
    }

    /**
     * @return array
     */
    public static function getAdminNotificationEmails()
    {
        $emailsString = \Yii::$app->config->get('notification_emails', 'admin@dev.dev');
        $result = explode("\n", $emailsString);
        if (empty($result)) {
            $result = ['admin@dev.dev']; // fuse to prevent email sending errors in case of missing email
        }

        return $result;
    }

    public static function getConfigFile($configKey, $default = false)
    {
        $fileId = (int)static::isKeyExist($configKey, false);

        return $fileId ? FPM::originalSrc($fileId) : $default;
    }
}
