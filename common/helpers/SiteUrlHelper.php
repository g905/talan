<?php

namespace common\helpers;

use common\components\model\Helper;
use yii\helpers\Url;
use Yii;

/**
 * Site controller
 */
class SiteUrlHelper
{
    use Helper;

    public static function listModelSelect2Url(string $type, $key = 'id', $label = 'label')
    {
        return self::createUrl('/site/list-model-select2', compact('type', 'key', 'label'));
    }

    public static function listModelSelectizeUrl(string $type, $key = 'id', $label = 'label')
    {
        return self::createUrl('/site/list-model-selectize', compact('type', 'key', 'label'));
    }

    public static function listPublicDataWidgetSelectizeUrl(string $class, int $type, $key = 'id', $label = 'label', $city, $complex)
    {
        return self::createUrl('/site/list-public-data-widget', compact('class', 'type', 'key', 'label', 'city', 'complex'));
    }

    public static function createHomePageUrl($params = [])
    {
        return self::createUrl('/page/page/home', $params);
    }

    public static function createAboutCompanyPageUrl($params = [])
    {
        return self::createUrl('/page/page/about', $params);
    }

    public static function createComplexListResidentialUrl()
    {
        return self::createUrl('/apartment/apartment-complex/list', ['type' => Property::TYPE_RESIDENTIAL]);
    }

    public static function createComplexListCommercialUrl()
    {
        return self::createUrl('/apartment/apartment-complex/list', ['type' => Property::TYPE_COMMERCIAL]);
    }

    public static function createComplexListSecondaryUrl()
    {
        return self::createUrl('/apartment/apartment-complex/list', ['type' => Property::TYPE_SECONDARY]);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function createComplexListUniversalUrl($params = [])
    {
        return self::createUrl('/apartment/apartment-complex/list', $params);
    }

    public static function createApartmentComplexUrl($params = [])
    {
        //ToDo url with city subdomain
        return self::createUrl('/apartment/apartment-complex/view', $params);
    }

    public static function createApartmentComplexApartmentsListUrl($params = [])
    {
        //ToDo url with city subdomain
        return self::createUrl('/apartment/apartment-complex/apartments-list', $params);
    }

    public static function createApartmentComplexConfiguratorUrl($params = [])
    {
        //ToDo url with city subdomain
        return self::createUrl('/apartment/apartment-complex/apartments-configurator', $params);
    }

    public static function createApartmentComplexPlanUrl($params = [])
    {
        //ToDo url with city subdomain
        return self::createUrl('/page/page/building-apartments', $params);
    }

    public static function createApartmentBuildingApartmentsListUrl($params = [])
    {
        //ToDo url with city subdomain
        return self::createUrl('/apartment/apartment-building/apartments-list', $params);
    }

    public static function createApartmentUrl($params = [])
    {
        //ToDo url with city subdomain
        return self::createUrl('/apartment/apartment/view', $params);
    }

    public static function createPromoActionViewUrl($params = [])
    {
        return self::createUrl('/apartment/promo-action/view', $params);
    }

    public static function createHowBuyBlockViewUrl($params = [])
    {
        return self::createUrl('/apartment/how-buy-block/view', $params);
    }

    public static function createFormQuestionUrl($params = [])
    {
        return self::createUrl('/form/form/form-question', $params);
    }

    public static function createGeolocationUrl($params = [])
    {
        return self::createUrl('/site/geolocation', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function createFormQuestionValidationUrl($params = [])
    {
        return self::createUrl('/form/form/form-question-validate', $params);
    }

    public static function createFormCallbackPopupUrl($params = [])
    {
        return self::createUrl('/form/form/form-callback-popup', $params);
    }

    public static function createFormPartnerPopupUrl($params = [])
    {
        return self::createUrl('/form/form/form-partner-popup', $params);
    }

    public static function createFormHypothecPopupUrl($params = [])
    {
        return self::createUrl('/form/form/form-hypothec-popup', $params);
    }

    public static function createFormHypothecWithDocUrl($params = [])
    {
        return self::createUrl('/form/form/form-hypothec-with-doc', $params);
    }

    public static function createFormCooperationUrl($params = [])
    {
        return self::createUrl('/form/form/form-cooperation', $params);
    }

    public static function createFormCooperationValidationUrl($params = [])
    {
        return self::createUrl('/form/form/form-cooperation-validate', $params);
    }

    public static function createFormJobWithDocUrl($params = [])
    {
        return self::createUrl('/form/form/form-job-with-doc', $params);
    }

    public static function createFormCustomGuaranteeUrl($params = [])
    {
        return self::createUrl('/form/form/form-custom-guarantee', $params);
    }

    public static function createFormCustomGuaranteeValidationUrl($params = [])
    {
        return self::createUrl('/form/form/form-custom-guarantee-validate', $params);
    }

    public static function createFormHypothecWithDocValidationUrl($params = [])
    {
        return self::createUrl('/form/form/form-hypothec-with-doc-validate', $params);
    }

    public static function createFormJobWithDocValidationUrl($params = [])
    {
        return self::createUrl('/form/form/form-job-with-doc-validate', $params);
    }

    public static function createFormInstalmentPlanPopupUrl($params = [])
    {
        return self::createUrl('/form/form/form-instalment-plan-popup', $params);
    }


    public static function createFormCommercialPopupUrl($params = [])
    {
        return self::createUrl('/form/form/form-commercial-popup', $params);
    }

    public static function createFormExcursionPopupUrl($params = [])
    {
        return self::createUrl('/form/form/form-excursion-popup', $params);
    }

    public static function createFormComplexPresentationUrl($params = [])
    {
        return self::createUrl('/form/form/form-complex-presentation', $params);
    }

    public static function createFormGuaranteeUrl($params = [])
    {
        return self::createUrl('/form/form/form-guarantee', $params);
    }

    public static function createFormSecondaryUrl($params = [])
    {
        return self::createUrl('/form/form/form-secondary', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function createFormComplexPresentationValidateUrl($params = [])
    {
        return self::createUrl('/form/form/form-complex-presentation-validate', $params);
    }

    public static function createFormGuaranteeValidateUrl($params = [])
    {
        return self::createUrl('/form/form/form-guarantee-validate', $params);
    }

    public static function createFormSecondaryValidateUrl($params = [])
    {
        return self::createUrl('/form/form/form-secondary-validate', $params);
    }

    public static function createCustomPageUrl($params = [])
    {
        return self::createUrl('/page/page/view', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getBlogAllUrl($params = [])
    {
        return self::createUrl('/blog/blog/index', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getBlogViewUrl($params = [])
    {
        return self::createUrl('/blog/blog/view', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getSubscribeFormSubmitUrl($params = [])
    {
        return self::createUrl('/form/form/subscribe-form-submit', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getSubscribeBottomFormSubmitUrl($params = [])
    {
        return self::createUrl('/form/form/subscribe-bottom-form-submit', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getCustomPopupRequestSubmitUrl($params = [])
    {
        return self::createUrl('/form/form/custom-popup-request-submit', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getApartmentComplexHistoryInfo($params = [])
    {
        return self::createUrl('/apartment/apartment-complex/apartment-history-info', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getApartmentBuildingHistoryInfo($params = [])
    {
        return self::createUrl('/apartment/apartment-building/apartment-history-info', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getApartmentUrl($params = [])
    {
        return self::createUrl('/apartment/apartment/view', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getApartmentConstructorSubmitUrl($params = [])
    {
        return self::createUrl('/apartment/constructor/form-submit', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getApartmentComplexHistoryPageUrl($params = [])
    {
        return self::createUrl('/apartment/apartment-complex/history', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getTeamPageUrl($params = [])
    {
        return self::createUrl('/team/team/index', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getTeamCityItemsUrl($params = [])
    {
        return self::createUrl('/team/team/city-items', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getPartnersPageUrl($params = [])
    {
        return self::createUrl('/partner/partner/index', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function getSecondaryRealEstatePageUrl($params = [])
    {
        return self::createUrl('/resale/secondary-real-estate/index', $params);
    }


    public static function getCareerViewUrl($params = [])
    {
        return self::createUrl('/hr/career/view', $params);
    }

    public static function getSpecializationViewUrl($params = [])
    {
        return self::createUrl('/hr/specialization/view', $params);
    }

    public static function getVacanciesUrl($params = [])
    {
        return self::createUrl('/hr/vacancy/index', $params);
    }

    public static function getVacancyViewUrl($params = [])
    {
        return self::createUrl('/hr/vacancy/view', $params);
    }

    public static function getChartUrl($params = [])
    {
        return self::createUrl('/page/page/get-chart', $params);
    }

    public static function getRatingUrl($params = [])
    {
        return self::createUrl('/page/page/get-rating', $params);
    }

    public static function getCustomDduUrl($params = [])
    {
        return self::createUrl('/page/page/get-custom-chart', $params);
    }

    public static function getScheduleChartUrl($params = [])
    {
        return self::createUrl('/page/page/get-schedule-chart', $params);
    }

    public static function getPublicDataDownloadFileUrl($id)
    {
        return self::createUrl('/form/form/public-data-download-file', ['id' => $id]);
    }

    public static function getDomainName()
    {
        $pieces = parse_url(Url::base(true));

        $domain = isset($pieces['host']) ? $pieces['host'] : '';

        $rx = '/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i';

        if (preg_match($rx, $domain, $regs)) {
            return $regs['domain'];
        }

        return false;
    }

    /**
     * @param array $params
     * @return string
     */
    public static function createShowPhoneUrl($params = [])
    {
        return self::createUrl('/agents-show-phone-popup', $params);
    }
    /**
     * @param array $params
     * @return string
     */

    public static function getAgentsCityItemsUrl($params = [])
    {
        return self::createUrl('/agents/agents/city-items', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public static function agentsTakePartPopupForm ($params = [])
    {
        return self::createUrl('/agents-take-part-form', $params);
    }

    public static function agentsVideoPopup ($params = [])
    {
        return self::createUrl('/agents-video-popup', $params);
    }

    public static function createFormAscAboutDiscPopupUrl($params = []) {
		return self::createUrl('/form-asc-about-discount', $params);
	}

	public static function getAgreementUrl() {
		//return Yii::$app->params['baseFrontUrl'] . '/files/agreement.docx';
		return  '/files/agreement.docx';
	}

	public static function getPrivatePolicyUrl() {
		//return Yii::$app->params['baseFrontUrl'] . '/files/privacy-policy.docx';
		return '/files/privacy-policy.docx';
	}
}
