<?php

namespace common\helpers;

/**
 * Property helper.
 *
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package common\helpers
 */
class Property
{
    /**
     * @var int property type constants.
     * Type `Undefined` is not used anywhere in the application,
     * properties with this type even should not appear in the db.
     */
    const TYPE_UNDEFINED = 0;
    const TYPE_RESIDENTIAL = 1;
    const TYPE_COMMERCIAL = 2;
    const TYPE_SECONDARY = 3;

    public static function types(): array
    {
        return [
            self::TYPE_UNDEFINED => 'Не определен',
            self::TYPE_RESIDENTIAL => 'Жилая недвижимость',
            self::TYPE_COMMERCIAL => 'Коммерческая недвижимость',
            self::TYPE_SECONDARY => 'Вторичная недвижимость',
        ];
    }

    public static function buildingTypes(): array
    {
        return [
            self::TYPE_UNDEFINED => 'Не определен',
            self::TYPE_RESIDENTIAL => 'Жилое',
            self::TYPE_COMMERCIAL => 'Нежилое',
        ];
    }

    public static function typeLabel(int $type): string
    {
        return obtain($type, static::types(), t('Undefined', 'property'));
    }

    public static function typeFromLabel(string $typeLabel): int
    {
        return obtain($typeLabel, array_flip(static::types()), self::TYPE_UNDEFINED);
    }

    public static function buildingTypeFromLabel(string $typeLabel): int
    {
        return obtain($typeLabel, array_flip(static::buildingTypes()), self::TYPE_UNDEFINED);
    }

    /**
     * @var string property object status constants, that are used by the application.
     */
    const STATUS_SOLD_OUT = 0;
    const STATUS_AVAILABLE = 1;
    const STATUS_PLEDGED = 2;
    const STATUS_ORDERED = 3;
    const STATUS_RESERVED = 4;

    /**
     * @var string property object status constants that are stored in 1C file.
     */
    const SYNC_STATUS_SOLD_OUT = 'Продана';
    const SYNC_STATUS_AVAILABLE = 'Свободна';
    const SYNC_STATUS_PLEDGED = 'В залоге';
    const SYNC_STATUS_ORDERED = 'В приказе';
    const SYNC_STATUS_RESERVED = 'В резерве';

    public static function statusLabel(int $status): string
    {
        return obtain($status, static::statuses(), self::SYNC_STATUS_SOLD_OUT);
    }

    public static function statusFromLabel(string $statusLabel): int
    {
        return obtain($statusLabel, array_flip(static::statuses()), self::STATUS_SOLD_OUT);
    }

    public static function statuses(): array
    {
        return [
            self::STATUS_SOLD_OUT => self::SYNC_STATUS_SOLD_OUT,
            self::STATUS_AVAILABLE => self::SYNC_STATUS_AVAILABLE,
            self::STATUS_PLEDGED => self::SYNC_STATUS_PLEDGED,
            self::STATUS_ORDERED => self::SYNC_STATUS_ORDERED,
            self::STATUS_RESERVED => self::SYNC_STATUS_RESERVED,
        ];
    }

    /**
     * @var string property object subtype constants, that are used by the application.
     */
    const SUBTYPE_UNDEFINED = 0;
    const SUBTYPE_PHARMACY = 1;
    const SUBTYPE_BANK = 2;
    const SUBTYPE_EARLY_DEVELOPMENT_GROUP = 2;
    const SUBTYPE_INTERIOR = 4;
    const SUBTYPE_CAFE = 5;
    const SUBTYPE_APARTMENT = 6;
    const SUBTYPE_APARTMENT_STUDIO = 7;
    const SUBTYPE_APARTMENT_VN = 8;
    const SUBTYPE_STORAGE_ROOM = 9;
    const SUBTYPE_COTTAGE = 10;
    const SUBTYPE_STORE = 11;
    const SUBTYPE_OFFICE_SPACE = 12;
    const SUBTYPE_PARKING = 13;
    const SUBTYPE_SPACE_TSJ = 14;
    const SUBTYPE_RESTAURANT = 15;
    const SUBTYPE_COMMUNICATION_SALON = 16;
    const SUBTYPE_SUPERMARKET = 17;

    /**
     * @var string property object type constants that are stored in 1C file.
     */
    const SYNC_SUBTYPE_UNDEFINED = 'Не определен';
    const SYNC_SUBTYPE_PHARMACY = 'Аптека';
    const SYNC_SUBTYPE_BANK = 'Банк';
    const SYNC_SUBTYPE_EARLY_DEVELOPMENT_GROUP = 'Группы раннего развития';
    const SYNC_SUBTYPE_INTERIOR = 'Иинтерьер'; // mistake in importing doc, not in code
    const SYNC_SUBTYPE_CAFE = 'Кафе';
    const SYNC_SUBTYPE_APARTMENT = 'Квартира';
    const SYNC_SUBTYPE_APARTMENT_STUDIO = 'Квартира студия';
    const SYNC_SUBTYPE_APARTMENT_VN = 'Квартира_ВН';
    const SYNC_SUBTYPE_STORAGE_ROOM = 'Кладовое помещение';
    const SYNC_SUBTYPE_COTTAGE = 'Коттедж';
    const SYNC_SUBTYPE_STORE = 'Магазин';
    const SYNC_SUBTYPE_OFFICE_SPACE = 'Офисное помещение';
    const SYNC_SUBTYPE_PARKING = 'Парковка';
    const SYNC_SUBTYPE_SPACE_TSJ = 'Помещение ТСЖ';
    const SYNC_SUBTYPE_RESTAURANT = 'Ресторан';
    const SYNC_SUBTYPE_COMMUNICATION_SALON = 'Салон связи';
    const SYNC_SUBTYPE_SUPERMARKET = 'Супермаркет';

    public static function subtypeLabel(int $subtype): string
    {
        return obtain($subtype, static::subtypes(), self::SYNC_SUBTYPE_UNDEFINED);
    }

    public static function subtypeFromLabel(string $subtypeLabel): int
    {
        return obtain($subtypeLabel, array_flip(static::subtypes()), self::TYPE_UNDEFINED);
    }

    public static function subtypes(): array
    {
        return [
            self::SUBTYPE_UNDEFINED => self::SYNC_SUBTYPE_UNDEFINED,
            self::SUBTYPE_PHARMACY => self::SYNC_SUBTYPE_PHARMACY,
            self::SUBTYPE_BANK => self::SYNC_SUBTYPE_BANK,
            self::SUBTYPE_EARLY_DEVELOPMENT_GROUP => self::SYNC_SUBTYPE_EARLY_DEVELOPMENT_GROUP,
            self::SUBTYPE_INTERIOR => self::SYNC_SUBTYPE_INTERIOR,
            self::SUBTYPE_CAFE => self::SYNC_SUBTYPE_CAFE,
            self::SUBTYPE_APARTMENT => self::SYNC_SUBTYPE_APARTMENT,
            self::SUBTYPE_APARTMENT_STUDIO => self::SYNC_SUBTYPE_APARTMENT_STUDIO,
            self::SUBTYPE_APARTMENT_VN => self::SYNC_SUBTYPE_APARTMENT_VN,
            self::SUBTYPE_STORAGE_ROOM => self::SYNC_SUBTYPE_STORAGE_ROOM,
            self::SUBTYPE_COTTAGE => self::SYNC_SUBTYPE_COTTAGE,
            self::SUBTYPE_STORE => self::SYNC_SUBTYPE_STORE,
            self::SUBTYPE_OFFICE_SPACE => self::SYNC_SUBTYPE_OFFICE_SPACE,
            self::SUBTYPE_PARKING => self::SYNC_SUBTYPE_PARKING,
            self::SUBTYPE_SPACE_TSJ => self::SYNC_SUBTYPE_SPACE_TSJ,
            self::SUBTYPE_RESTAURANT => self::SYNC_SUBTYPE_RESTAURANT,
            self::SUBTYPE_COMMUNICATION_SALON => self::SYNC_SUBTYPE_COMMUNICATION_SALON,
            self::SUBTYPE_SUPERMARKET => self::SYNC_SUBTYPE_SUPERMARKET,
        ];
    }

    public static function subtypesByType(): array
    {
        return [
            self::TYPE_UNDEFINED => [
                self::SUBTYPE_UNDEFINED => self::SYNC_SUBTYPE_UNDEFINED,
                self::SUBTYPE_INTERIOR => self::SYNC_SUBTYPE_INTERIOR,
                self::SUBTYPE_SPACE_TSJ => self::SYNC_SUBTYPE_SPACE_TSJ,
            ],
            self::TYPE_RESIDENTIAL => [
                self::SUBTYPE_APARTMENT => self::SYNC_SUBTYPE_APARTMENT,
                self::SUBTYPE_COTTAGE => self::SYNC_SUBTYPE_COTTAGE,
                self::SUBTYPE_APARTMENT_STUDIO => self::SYNC_SUBTYPE_APARTMENT_STUDIO,
                self::SUBTYPE_STORAGE_ROOM => self::SYNC_SUBTYPE_STORAGE_ROOM,
                self::SUBTYPE_PARKING => self::SYNC_SUBTYPE_PARKING,
            ],
            self::TYPE_COMMERCIAL => [
                self::SUBTYPE_PHARMACY => self::SYNC_SUBTYPE_PHARMACY,
                self::SUBTYPE_BANK => self::SYNC_SUBTYPE_BANK,
                self::SUBTYPE_EARLY_DEVELOPMENT_GROUP => self::SYNC_SUBTYPE_EARLY_DEVELOPMENT_GROUP,
                self::SUBTYPE_CAFE => self::SYNC_SUBTYPE_CAFE,
                self::SUBTYPE_STORE => self::SYNC_SUBTYPE_STORE,
                self::SUBTYPE_OFFICE_SPACE => self::SYNC_SUBTYPE_OFFICE_SPACE,
                self::SUBTYPE_RESTAURANT => self::SYNC_SUBTYPE_RESTAURANT,
                self::SUBTYPE_COMMUNICATION_SALON => self::SYNC_SUBTYPE_COMMUNICATION_SALON,
                self::SUBTYPE_SUPERMARKET => self::SYNC_SUBTYPE_SUPERMARKET,
            ],
            self::TYPE_SECONDARY => [
                self::SUBTYPE_APARTMENT_VN => self::SYNC_SUBTYPE_APARTMENT_VN,
            ],
        ];
    }

    public static function typeFromSubtype(int $subtype): int
    {
        foreach (static::subtypesByType() as $type => $subtypes) {
            if (keyExists($subtype, $subtypes)) {
                return $type;
            }
        }

        return self::TYPE_UNDEFINED;
    }

    public static function typeSubtypes(int $type): array
    {
        return obtain($type, static::subtypesByType(), []);
    }
}
