<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 06.04.18
 * Time: 13:24
 */

namespace common\helpers;


use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class DetailTableView extends Widget
{
    /**
     * @var []
     */
    public $data;
    /**
     * @var array
     */
    public $options = [];
    /**
     * @var array
     */
    public $itemOptions = [];
    /**
     * @var bool
     */
    public $encodeItems = true;


    /**
     * @return string
     */
    public function run()
    {
        $result = '';

        foreach ($this->data as $key => $item) {
            $labelTagOptions = [
                'style' => 'text-align: right;'
            ];
            if (is_array($item)) {
                $label = ArrayHelper::getValue($item, 'label', null);
                $value = ArrayHelper::getValue($item, 'value', null);
                $label = $this->encodeItems
                    ? Html::encode($label)
                    : $key;
                $value = $this->encodeItems
                    ? Html::encode($value)
                    : $item;
                $label = Html::tag('b', $label);
                $row = Html::tag('td', $label, $labelTagOptions) . ': ' . Html::tag('td', $value);
                $row = Html::tag('tr', $row, $this->itemOptions);
                $result .= $row;
                // todo: maybe add callback for item
            } else {
                $label = $this->encodeItems
                    ? Html::encode($key)
                    : $key;
                $value = $this->encodeItems
                    ? Html::encode($item)
                    : $item;
                $label = Html::tag('b', $label);
                $row = Html::tag('td', $label, $labelTagOptions) . ': ' . Html::tag('td', $value);
                $row = Html::tag('tr', $row, $this->itemOptions);
                $result .= $row;
            }
        }

        $result = Html::tag('table', $result, $this->options);

        return $result;
    }
}