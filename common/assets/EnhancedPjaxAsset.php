<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * This asset bundle provides the javascript files required by [[EnhancedPjax]] widget.
 *
 * @package common\assets
 * @author Bogdan Fedun <delagics@gmail.com>
 */
class EnhancedPjaxAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/js';
    public $js = [
        'jquery.enhanced.pjax.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
