<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%benefit_to_complex_list}}".
 *
 * @property integer $complex_list_id
 * @property integer $benefit_id
 * @property integer $position
 *
 * @property ComplexList $complexList
 * @property Benefit $benefit
 */
class BenefitToComplexList extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%benefit_to_complex_list}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplexList()
    {
        return $this->hasOne(ComplexList::class, ['id' => 'complex_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBenefit()
    {
        return $this->hasOne(Benefit::class, ['id' => 'benefit_id']);
    }
}
