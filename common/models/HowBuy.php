<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%how_buy}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $city_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property HowBuyBlock $howBuyBlocks
 */
class HowBuy extends ActiveRecord 
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%how_buy}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
        ];
    }

    /**
     * Find one "how buy" info for specified city.
     *
     * @param int $cityID city identifier.
     * @return HowBuy|null
     */
    public static function findOneForCity($cityID)
    {
        return static::find()
            ->where(['published' => self::IS_PUBLISHED, 'city_id' => $cityID])
            ->orderBy(['position' => SORT_DESC])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHowBuyBlocks()
    {
        return $this->hasMany(HowBuyBlock::class, ['how_buy_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }
}
