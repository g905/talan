<?php

namespace common\models;

use metalguardian\fileProcessor\models\File;
use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%request_hypothec}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $file_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property City $city
 * @property File $file
 */
class RequestHypothec extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%request_hypothec}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'request-hypothec'),
            'city_id' => bt('Associated city', 'request-hypothec'),
            'file_id' => bt('File', 'request-hypothec'),
            'type' => bt('Request source type', 'request-hypothec'),
            'name' => bt('User name', 'request-hypothec'),
            'phone' => bt('User phone', 'request-hypothec'),
            'email' => bt('User email', 'request-hypothec'),
            'created_at' => bt('Created at', 'request-hypothec'),
            'updated_at' => bt('Updated at', 'request-hypothec'),
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getFile()
    {
        return $this->hasOne(File::class, ['id' => 'file_id']);
    }
}
