<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%theme_to_article}}".
 *
 * @property integer $id
 * @property integer $blog_theme_id
 * @property integer $article_id
 *
 * @property Article $article
 * @property BlogTheme $blogTheme
 */
class ThemeToArticle extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%theme_to_article}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogTheme()
    {
        return $this->hasOne(BlogTheme::className(), ['id' => 'blog_theme_id']);
    }
}
