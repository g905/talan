<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%guarantee_page}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $manager_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $city_id
 * @property string $alias
 * @property string $form_title
 * @property string $form_description
 * @property string $form_onsubmit
 * @property EntityToFile $image
 */
class GuaranteePage extends ActiveRecord 
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%guarantee_page}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }


    /**
     * @param $city
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findByCity($city)
    {
        return self::find()->where([
            'city_id' => $city->id,
            'published' => 1
        ])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getPageById($id)
    {
        return self::find()->where(['id' => $id])->one();
    }
}
