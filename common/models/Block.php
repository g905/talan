<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%block}}".
 *
 * @property integer $id
 * @property integer $object_id
 * @property integer $type
 * @property integer $sub_title
 * @property string $title
 * @property string $description
 * @property string $youtube_link
 * @property integer $position
 * @property integer $published
 */
class Block extends ActiveRecord
{
    /**
     * @var string available type constants.
     */
    const HYPOTHEC_STEP_BLOCK = 1;
    const HYPOTHEC_BENEFIT_BLOCK = 2;

    const INSTALLMENT_PLAN_STEP_BLOCK = 3;
    const INSTALLMENT_PLAN_BENEFIT_BLOCK = 4;

    const CAREER_REVIEW = 5;

    const SPECIALIZATION_REVIEW = 6;
    const SPECIALIZATION_BENEFIT = 7;
    const SPECIALIZATION_PRINCIPLE = 8;

    const VACANCY_FAQ_ITEM = 9;
    const VACANCY_PRINCIPLE = 10;
    const VACANCY_PAGE_FAQ_ITEM = 25;

    const BUILDING_ADVANTAGE = 11;

    const EARTH_ADVANTAGE = 12;
    const EARTH_COMPANY = 13;
    const EARTH_PLAN = 14;

    const CONTACT_HOW_TO_GET_STEP = 15;

    const REALTOR_BENEFIT = 16;
    const REALTOR_PARTNER = 17;
    const REALTOR_FILE = 18;
    const REALTOR_FACT = 19;

    const BUY_ADVANTAGE = 20;
    const BUY_STEP = 21;

    const BUY_PAGE_HOW = 22;

    const APARTMENT_COMPLEX_SLIDER_VIDEO = 23;

    const FLOOR_PLAN = 24;

    public static function types()
    {
        return [
            self::HYPOTHEC_STEP_BLOCK,
            self::HYPOTHEC_BENEFIT_BLOCK,
            self::INSTALLMENT_PLAN_STEP_BLOCK,
            self::INSTALLMENT_PLAN_BENEFIT_BLOCK,
            self::CAREER_REVIEW,
            self::SPECIALIZATION_PRINCIPLE,
            self::SPECIALIZATION_BENEFIT,
            self::SPECIALIZATION_REVIEW,
            self::VACANCY_FAQ_ITEM,
            self::VACANCY_PRINCIPLE,
            self::BUILDING_ADVANTAGE,
            self::EARTH_ADVANTAGE,
            self::EARTH_COMPANY,
            self::EARTH_PLAN,
            self::CONTACT_HOW_TO_GET_STEP,
            self::REALTOR_BENEFIT,
            self::REALTOR_PARTNER,
            self::REALTOR_FILE,
            self::REALTOR_FACT,
            self::BUY_ADVANTAGE,
            self::BUY_STEP,
            self::BUY_PAGE_HOW,
            self::APARTMENT_COMPLEX_SLIDER_VIDEO,
            self::FLOOR_PLAN
        ];
    }

    public static function tableName()
    {
        return '{{%block}}';
    }
}
