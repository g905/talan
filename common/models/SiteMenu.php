<?php

namespace common\models;

use common\helpers\SiteUrlHelper;
use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%site_menu}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $menu_position_id
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 */
class SiteMenu extends ActiveRecord 
{
    const POSITION_HEADER = 1;
    const POSITION_FOOTER = 2;
    const POSITION_COMPLEXLIST = 3;

    const PREPARED_PAGE_HOME = 1;
    const PREPARED_PAGE_ABOUT_COMPANY = 2;
    const PREPARED_PAGE_COMPLEXLIST_RESIDENTIAL = 3;
    const PREPARED_PAGE_COMPLEXLIST_COMMERCIAL = 4;
    //const PREPARED_PAGE_COMPLEXLIST_SECONDARY = 5;

    public static function getMenuPositionDictionary()
    {
        return [
            self::POSITION_HEADER => Yii::t('back/site-menu', 'Header'),
            self::POSITION_FOOTER => Yii::t('back/site-menu', 'Footer'),
            self::POSITION_COMPLEXLIST => Yii::t('back/site-menu', 'Complex list (second row)'),
        ];
    }

    public static function getPreparedPageDictionary()
    {
        /** @var CustomPage[] $customPages */
        $customPages = CustomPage::find()->where(['published' => 1])->orderBy(['created_at' => SORT_DESC])->all();

        $returnData = [
            self::PREPARED_PAGE_HOME => Yii::t('back/site-menu', 'Home'),
            self::PREPARED_PAGE_ABOUT_COMPANY => Yii::t('back/site-menu', 'About company'),
            self::PREPARED_PAGE_COMPLEXLIST_RESIDENTIAL => Yii::t('back/site-menu', 'Residential complex list'),
            self::PREPARED_PAGE_COMPLEXLIST_COMMERCIAL => Yii::t('back/site-menu', 'Commercial complex list'),
            //self::PREPARED_PAGE_COMPLEXLIST_SECONDARY => Yii::t('back/site-menu', 'Secondary complex list')
        ];

        foreach ($customPages as $page){
            $returnData['custom' . $page->id] = $page->label;
        }

        return $returnData;
    }

    public static function getPreparedPageLinkDictionary()
    {
        /** @var CustomPage[] $customPages */
        $customPages = CustomPage::find()->where([
            'published' => 1
        ])->orderBy([
            'created_at' => SORT_DESC,
        ])->all();

        $returnData =  [
            self::PREPARED_PAGE_HOME => SiteUrlHelper::createHomePageUrl(),
            self::PREPARED_PAGE_ABOUT_COMPANY => SiteUrlHelper::createAboutCompanyPageUrl(),
            self::PREPARED_PAGE_COMPLEXLIST_RESIDENTIAL => SiteUrlHelper::createComplexListResidentialUrl(),
            self::PREPARED_PAGE_COMPLEXLIST_COMMERCIAL=> SiteUrlHelper::createComplexListCommercialUrl(),
            //self::PREPARED_PAGE_COMPLEXLIST_SECONDARY => SiteUrlHelper::createComplexListSecondaryUrl(),
        ];

        foreach ($customPages as $page){
            $returnData['custom'.$page->id] = SiteUrlHelper::createCustomPageUrl(['alias' => $page->alias]);
        }

        return $returnData;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_menu}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItems()
    {
        return $this->hasMany(SiteMenuItem::class, ['site_menu_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }
}
