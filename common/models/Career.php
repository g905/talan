<?php

namespace common\models;

use yii\db\ActiveQuery;
use common\models\blocks\CareerReview;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%career}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $top_label
 * @property string $top_description
 * @property string $top_btn_label
 * @property string $top_btn_link
 * @property string $specializations_label
 * @property string $director_name
 * @property string $director_post
 * @property string $director_text
 * @property string $reviews_label
 * @property string $articles_label
 * @property string $articles_btn_label
 * @property integer $manager_id
 * @property string $form_label
 * @property string $form_description
 * @property string $form_agreement
 * @property string $form_emails
 * @property string $form_onsubmit
 * @property integer $view_count
 * @property integer $position
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property Manager $manager
 * @property Specialization[] $specializations
 * @property CareerReview[] $reviewBlocks
 * @property SubMenu[] $subMenus
 * @property Article[] $articles
 */
class Career extends ActiveRecord
{
    const ATTR_TOP_IMAGE = 'careerTopImage';
    const ATTR_TOP_VIDEO = 'careerTopVideo';
    const ATTR_DIRECTOR_PHOTO = 'careerDirectorPhoto';

    /**
     * Find one published record by its city or throw not found exception.
     *
     * @param int city identifier.
     * @return null|\yii\db\ActiveRecord|static
     * @throws \yii\web\NotFoundHttpException
     */
    public static function findOnePublishedByCityOrFail($cityId)
    {
        return static::checkFail(
            static::find()->where(['city_id' => $cityId, 'published' => self::IS_PUBLISHED])->one()
        );
    }

    public static function tableName()
    {
        return '{{%career}}';
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getSpecializations()
    {
        return $this->hasMany(Specialization::class, ['city_id' => 'city_id'])
            ->alias('s')->andWhere(['s.published' => self::IS_PUBLISHED]);
    }

    public function getReviewBlocks()
    {
        return $this->hasMany(CareerReview::class, ['object_id' => 'id'])
            ->onCondition(['type' => CareerReview::CAREER_REVIEW, 'published' => 1]);
    }

    public function getSubMenus()
    {
        return $this->hasMany(SubMenu::class, ['object_id' => 'id'])
            ->onCondition(['type' => SubMenu::TYPE_CAREER]);
    }

    public function getArticles()
    {
        return $this->hasMany(Article::class, ['id' => 'article_id'])
            ->alias('a')
            ->where(['a.published' => self::IS_PUBLISHED])
            ->viaTable(ArticleAssign::tableName(), ['object_id' => 'id'], function (ActiveQuery $q) {
                $q->alias('aa')->andWhere(['aa.type' => ArticleAssign::TYPE_CAREER_ARTICLES]);
            })
            ->innerJoin(ArticleAssign::tableName() . ' aa', 'aa.article_id = a.id')
            ->orderBy(['aa.position' => SORT_ASC]);
    }

    public function getTopImageSrc()
    {
        return $this->getEntityThumb('topImage', 'career', 'topimage');
    }

    public function getTopVideoSrc()
    {
        return $this->getEntitySource('topVideo');
    }

    public function getDirectorPhotoSrc()
    {
        return $this->getEntityThumb('directorPhoto', 'career', 'directorphoto');
    }

    public function getTopImage()
    {
        return $this->getEntityModel(self::ATTR_TOP_IMAGE, 'careerTopImage');
    }

    public function getTopVideo()
    {
        return $this->getEntityModel(self::ATTR_TOP_VIDEO, 'careerTopVideo');
    }

    public function getDirectorPhoto()
    {
        return $this->getEntityModel(self::ATTR_DIRECTOR_PHOTO, 'careerDirectorPhoto');
    }
}
