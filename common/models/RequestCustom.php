<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%request_custom}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $city_id
 * @property integer $ac_id
 * @property integer $custom_popup_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property CustomPopup $customPopup
 * @property ApartmentComplex $apartmentComplex
 */
class RequestCustom extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%request_custom}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'city_id' => 'City',
            'ac_id' => 'Apartment complex',
            'custom_popup_id' => 'Popup',
        ];
    }

    public function behaviors()
    {
        return [TimestampBehavior::class];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getCustomPopup()
    {
        return $this->hasOne(CustomPopup::class, ['id' => 'custom_popup_id']);
    }

    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'ac_id']);
    }
}
