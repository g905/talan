<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%request_cooperation}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $company_name
 * @property string $chief_phone
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 */
class RequestCooperation extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%request_cooperation}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('request-cooperation', 'ID'),
            'city_id' => Yii::t('request-cooperation', 'City ID'),
            'name' => Yii::t('request-cooperation', 'Name'),
            'phone' => Yii::t('request-cooperation', 'Phone'),
            'email' => Yii::t('request-cooperation', 'Email'),
            'company_name' => Yii::t('request-cooperation', 'Company Name'),
            'chief_phone' => Yii::t('request-cooperation', 'Chief Phone'),
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
