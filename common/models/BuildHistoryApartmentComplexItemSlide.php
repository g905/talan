<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%build_history_apartment_complex_item_slide}}".
 *
 * @property integer $id
 * @property integer $build_history_apartment_complex_item_id
 * @property string $day
 * @property string $content
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property BuildHistoryApartmentComplexItem $buildHistoryApartmentComplexItem
 * @property EntityToFile $image
 */
class BuildHistoryApartmentComplexItemSlide extends \common\components\model\ActiveRecord
{
    const SAVE_ATTRIBUTE_IMAGE = 'BuildHistoryApartmentComplexItemSlideImage';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%build_history_apartment_complex_item_slide}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuildHistoryApartmentComplexItem()
    {
        return $this->hasOne(BuildHistoryApartmentComplexItem::class, ['id' => 'build_history_apartment_complex_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasMany(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'bhacis.entity_model_name' => static::formName(),
                'bhacis.attribute' => static::SAVE_ATTRIBUTE_IMAGE
            ])
            ->alias('bhacis')
            ->orderBy('bhacis.position DESC');
    }

    public static function getBuildHistory($cid)
    {
        if(!$cid) {
            $complex = \backend\modules\apartment\models\ApartmentComplex::find()->select(['id'])->all();
        } else {
            $complex = \backend\modules\apartment\models\ApartmentComplex::find()->select(['id'])->where(['guid' => $cid])->one();
        }
        //$complex = \backend\modules\apartment\models\ApartmentComplex::find()->select(['id'])->where(['guid' => $cid])->one();

        $v = self::find()
            ->joinWith('buildHistoryApartmentComplexItem b');
            //->where(['b.apartment_complex_id' => $complex->id]);
        if(isset($complex->id)){
            $v->where(['b.apartment_complex_id' => $complex->id]);
        }

        $v = $v->all();

        return $v;
        /*return self::find()
            ->alias('bhacis')
            ->innerJoin('apartment_complex ac', ['ac.id' => 'bhacis.apartment_complex_id'])
            ->all();*/
    }
}
