<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%about_company}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $top_screen_title
 * @property string $top_screen_description
 * @property string $mission_label
 * @property string $company_values_title
 * @property string $ceo_name
 * @property string $ceo_position
 * @property string $ceo_quotation
 * @property string $form_title
 * @property string $form_description
 * @property string $form_emails
 * @property string $form_onsubmit
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property EntityToFile $topScreenBackgroundImage
 * @property EntityToFile $topScreenBackgroundVideo
 * @property EntityToFile $mapImage
 * @property EntityToFile $ceoPhoto
 */
class AboutCompany extends ActiveRecord 
{
    const SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE = 'AboutCompanyTop_screen_background_image';
    const SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO = 'AboutCompanyTop_screen_background_video';
    const SAVE_ATTRIBUTE_MAP_IMAGE = 'AboutCompanyMap_image';
    const SAVE_ATTRIBUTE_CEO_PHOTO = 'AboutCompanyCeo_photo';

    public static function tableName()
    {
        return '{{%about_company}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getAboutCompanyMenu()
    {
        return $this->hasMany(AboutCompanyMenu::class, ['about_company_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getStatistic()
    {
        return $this->hasMany(AboutCompanyStat::class, ['about_company_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getValues()
    {
        return $this->hasMany(AboutCompanyValues::class, ['about_company_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getTopScreenBackgroundImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['top_screen_background_image.entity_model_name' => static::formName(), 'top_screen_background_image.attribute' => static::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE])
            ->alias('top_screen_background_image')
            ->orderBy('top_screen_background_image.position DESC');
    }

    public function getTopScreenBackgroundVideo()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['top_screen_background_video.entity_model_name' => static::formName(), 'top_screen_background_video.attribute' => static::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO])
            ->alias('top_screen_background_video')
            ->orderBy('top_screen_background_video.position DESC');
    }

    public function getMapImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['map_image.entity_model_name' => static::formName(), 'map_image.attribute' => static::SAVE_ATTRIBUTE_MAP_IMAGE])
            ->alias('map_image')
            ->orderBy('map_image.position DESC');
    }

    public function getCeoPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['ceo_photo.entity_model_name' => static::formName(), 'ceo_photo.attribute' => static::SAVE_ATTRIBUTE_CEO_PHOTO])
            ->alias('ceo_photo')
            ->orderBy('ceo_photo.position DESC');
    }

    /**
     * @param City $city
     * @return array|null|\yii\db\ActiveRecord|self
     */
    public static function findByCity($city)
    {
        return AboutCompany::find()->where([
            'city_id' => $city->id,
            'published' => 1
        ])->one();
    }
}
