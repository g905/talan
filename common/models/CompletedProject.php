<?php

namespace common\models;

use common\components\model\ActiveRecord;
use metalguardian\fileProcessor\helpers\FPM;

/**
 * This is the model class for table "{{%completed_project}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $published
 * @property CompletedProjectToComplexList[] $completedProjectToComplexLists
 * @property ComplexList[] $complexLists
 * @property EntityToFile $photo
 */
class CompletedProject extends ActiveRecord
{
    /**
     * @var string save image attributes.
     */
    const SAVE_ATTRIBUTE_PHOTO = 'CompletedProjectPhoto';

    public static function tableName()
    {
        return '{{%completed_project}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto()
    {
        return $this->getEntityModel(self::SAVE_ATTRIBUTE_PHOTO, 'completedProjectPhoto', 'CompletedProject');
    }

    /**
     * @return bool|null|string
     */
    public function getPhotoPreviewSrc()
    {
        return FPM::originalSrc(optional($this->photo)->file_id ?? '');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompletedProjectToComplexLists()
    {
        return $this->hasMany(CompletedProjectToComplexList::class, ['completed_project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplexLists()
    {
        return $this->hasMany(ComplexList::class, ['id' => 'complex_list_id'])
            ->viaTable('{{%completed_project_to_complex_list}}', ['completed_project_id' => 'id']);
    }

    public static function getItemsById($id)
    {
        return self::find()->isPublished()->andWhere(['id' => $id])->all();
    }
}
