<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "view_item".
 *
 * @property int $id
 * @property string $item
 * @property int $item_index
 * @property int $period
 * @property int $updated_at
 * @property int $created_at
 *
 * @property ViewCount[] $viewCounts
 * @property ViewCount $id0
 */
class ViewItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'view_item';
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_index', 'period', 'updated_at', 'created_at'], 'integer'],
            [['item'], 'string', 'max' => 255],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ViewCount::className(), 'targetAttribute' => ['id' => 'item_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item' => 'Item',
            'item_index' => 'Item Index',
            'period' => 'Period',
            'updated_at' => 'Updated At',
            'crated_at' => 'Crated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViewCounts()
    {
        return $this->hasMany(ViewCount::className(), ['item_id' => 'id']);
    }

}
