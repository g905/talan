<?php

namespace common\models;

use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%agents}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $city_id
 * @property string $agents_title
 * @property string $tab_label
 *
 * @property City $city
 * @property EntityToFile $backgroundImage
 * @property AgentsMember[] $agentsMembers
 */
class Agents extends ActiveRecord
{
    const SAVE_ATTRIBUTE_BACKGROUND_IMAGE = 'AgentsBackgroundImage';

    public $activeTab;

    const tabsText = ['deals' => 'По количеству сделок', 'service' => 'По оценке за сервис'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agents}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentsMembers($sort = null)
    {
        if (!$sort) $sort = 'deals_amount';
        return $this->hasMany(AgentsMembers::class, ['agents_id' => 'id'])
            ->orderBy([$sort => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBackgroundImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['backgroundImage.entity_model_name' => static::formName(), 'backgroundImage.attribute' => static::SAVE_ATTRIBUTE_BACKGROUND_IMAGE])
            ->alias('backgroundImage')
            ->orderBy('backgroundImage.position DESC');
    }

    /**
     * @param City $city
     * @return array|null|\yii\db\ActiveRecord|Agents
     */
    public static function getModelByCity($city)
    {
        if (!$city->id) {
            return null;
        }

        return self::find()
            ->alias('t')
            ->where([
                't.city_id' => $city->id,
                't.published' => 1,
            ])
            ->with(['agentsMembers'])
            ->limit(1)
            ->one();
    }

    /**
     * @return string
     */
    public function getTabText(): string
    {
        return $this->city->label ?? '';
    }

    public function getTabsLabels($id): string
    {
        $this->activeTab = $id;
        return $this::tabsText[$id];
    }

    /**
     * @return bool|null|string
     */
  /*  public function getPageBackgroundImage()
    {
        $customImage = $this->getEntityThumb('backgroundImage', 'agents', 'pagebackground');
        $staticImage = '&#x2F;static&#x2F;img&#x2F;agents&#x2F;agents_bg.png';

        return empty($customImage)
            ? $staticImage
            : $customImage;
    }*/

    public function getPageBackgroundImage()
    {
        $customImage = FPM::originalSrc($this->getBackgroundImage()->one()->file_id);
        $staticImage = '&#x2F;static&#x2F;img&#x2F;agents&#x2F;agents_bg.png';

        return empty($customImage)
            ? $staticImage
            : $customImage;
    }
}
