<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%team}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $city_id
 * @property string $team_title
 * @property string $tab_label
 *
 * @property City $city
 * @property EntityToFile $backgroundImage
 * @property TeamMember[] $teamMembers
 */
class Team extends ActiveRecord 
{
    const SAVE_ATTRIBUTE_BACKGROUND_IMAGE = 'TeamBackgroundImage';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%team}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamMembers()
    {
        return $this->hasMany(TeamMember::class, ['team_id' => 'id'])
            ->andOnCondition(['is not', 'manager_id', null])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getBackgroundImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['backgroundImage.entity_model_name' => static::formName(), 'backgroundImage.attribute' => static::SAVE_ATTRIBUTE_BACKGROUND_IMAGE])
            ->alias('backgroundImage')
            ->orderBy('backgroundImage.position DESC');
    }

    /**
     * @param City $city
     * @return array|null|\yii\db\ActiveRecord|Team
     */
    public static function getModelByCity($city)
    {
        if (!$city->id) {
            return null;
        }

        return self::find()
            ->alias('t')
            ->where([
                't.city_id' => $city->id,
                't.published' => 1,
            ])
            ->with(['teamMembers', 'teamMembers.manager'])
            ->limit(1)
            ->one();
    }

    /**
     * @return string
     */
    public function getTabText(): string
    {
        return $this->city->label ?? '';
    }

    /**
     * @return bool|null|string
     */
    public function getPageBackgroundImage()
    {
        $customImage = $this->getEntityThumb('backgroundImage', 'team', 'pagebackground');
        $staticImage = '&#x2F;static&#x2F;img&#x2F;team&#x2F;team_bg.png';

        return empty($customImage)
            ? $staticImage
            : $customImage;
    }
}
