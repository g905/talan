<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%ac_marker}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $apartment_complex_id
 * @property integer $ac_marker_type_id
 * @property string $lat
 * @property string $lng
 * @property string $phone
 * @property string $address
 * @property string $email
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property ApartmentComplex $apartmentComplex
 */
class AcMarker extends ActiveRecord
{
    const MARKER_TYPE_PATH = '/static/img/map/pin/';

    const MARKER_TYPE_SCHOOL = 1;
    const MARKER_TYPE_KINDERGARTEN = 2;
    const MARKER_TYPE_HOSPITAL = 3;
    const MARKER_TYPE_SUPERMARKET = 4;
    const MARKER_TYPE_PARK = 5;
    const MARKER_TYPE_SPORT = 6;
    const MARKER_TYPE_SHOP = 7;
    const MARKER_TYPE_MALL = 8;

    public static function getMarkerTypeDictionary()
    {
        return [
            self::MARKER_TYPE_SCHOOL => bt('School', 'ac-marker'),
            self::MARKER_TYPE_KINDERGARTEN => bt('Kindergarten', 'ac-marker'),
            self::MARKER_TYPE_HOSPITAL => bt('Hospital', 'ac-marker'),
            self::MARKER_TYPE_SUPERMARKET => bt('Supermarket', 'ac-marker'),
            self::MARKER_TYPE_PARK => bt('Park', 'ac-marker'),
            self::MARKER_TYPE_SPORT => bt('Sport', 'ac-marker'),
            self::MARKER_TYPE_SHOP => bt('Shop', 'ac-marker'),
            self::MARKER_TYPE_MALL => bt('Mall', 'ac-marker'),
        ];
    }

    public static function getMarkerTypeSvgClass()
    {
        return [
            self::MARKER_TYPE_SCHOOL => '#icon-map-object-0',
            self::MARKER_TYPE_KINDERGARTEN => '#icon-map-object-4',
            self::MARKER_TYPE_HOSPITAL => '#icon-map-object-1',
            self::MARKER_TYPE_SUPERMARKET => '#icon-map-object-5',
            self::MARKER_TYPE_PARK => '#icon-map-object-2',
            self::MARKER_TYPE_SPORT => '#icon-map-object-6',
            self::MARKER_TYPE_SHOP => '#icon-map-object-3',
            self::MARKER_TYPE_MALL => '#icon-map-object-7',
        ];
    }

    public static function getMarkerTypeMapIco()
    {
        return [
            self::MARKER_TYPE_SCHOOL => '3.png',
            self::MARKER_TYPE_KINDERGARTEN => '1.png',
            self::MARKER_TYPE_HOSPITAL => '7.png',
            self::MARKER_TYPE_SUPERMARKET => '5.png',
            self::MARKER_TYPE_PARK => '8.png',
            self::MARKER_TYPE_SPORT => '6.png',
            self::MARKER_TYPE_SHOP => '4.png',
            self::MARKER_TYPE_MALL => '2.png',
        ];
    }

    public static function tableName()
    {
        return '{{%ac_marker}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'apartment_complex_id']);
    }
}
