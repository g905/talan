<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%promo_action_to_ac}}".
 *
 * @property integer $action_id
 * @property integer $ac_id
 *
 * @property ApartmentComplex $ac
 * @property PromoAction $action
 */
class PromoActionToAc extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_action_to_ac}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAc()
    {
        return $this->hasOne(ApartmentComplex::className(), ['id' => 'ac_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(PromoAction::className(), ['id' => 'action_id']);
    }
}
