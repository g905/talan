<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%partner_page_card}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $content
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $link_text
 * @property string $link_url
 * @property integer $partner_page_id
 *
 * @property PartnerPage $partnerPage
 */
class PartnerPageCard extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_page_card}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerPage()
    {
        return $this->hasOne(PartnerPage::className(), ['id' => 'partner_page_id']);
    }
}
