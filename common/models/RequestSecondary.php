<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%request_secondary}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $name
 * @property string $phone
 * @property integer $square
 * @property string $address
 * @property integer $age
 * @property integer $redevelopment
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 */
class RequestSecondary extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%request_secondary}}';
    }

    public function behaviors()
    {
        return ['timestamp' => TimestampBehavior::class];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City'),
            'name' => Yii::t('app', 'Name'),
            'phone' => Yii::t('app', 'Phone'),
            'square' => Yii::t('app', 'Square'),
            'address' => Yii::t('app', 'Address'),
            'age' => Yii::t('app', 'Age'),
            'redevelopment' => Yii::t('app', 'Redevelopment'),
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
