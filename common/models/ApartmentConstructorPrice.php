<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "apartment_constructor_price".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $price
 * @property integer $created_at
 * @property integer $updated_at
 */
class ApartmentConstructorPrice extends \common\components\model\ActiveRecord
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'apartment_constructor_price';
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'room_1' => 'One Room Price',
            'room_2' => 'Two Rooms Price',
            'room_3' => 'Three Rooms Price',
            'room_4' => 'Four Rooms Price',
        ];
    }

    /**
     * @return bool|mixed
     */
    public static function getPrice($apartment)
    {
        $city = City::getUserCity();

        $option = self::find()->where(['city_id' => $city->id])->one();

        if ($option && $apartment) {
            $rooms = 'room_'.$apartment->rooms_number;

            return $option->$rooms ?? false;
        }

        return false;
    }
}
