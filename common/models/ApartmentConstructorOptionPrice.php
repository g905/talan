<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "apartment_constructor_option_price".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $type
 * @property integer $price
 */
class ApartmentConstructorOptionPrice extends \common\components\model\ActiveRecord
{
    const CONDITIONER = 1;
    const VIDEO_DOOR_PHONE = 2;
    const BRIZER = 3;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'apartment_constructor_option_price';
    }

    /**
     * @return array
     */
    public static function getTypeDictionary()
    {
        return [
            self::CONDITIONER => Yii::t('front/constructor', 'Кондиционер'),
            self::VIDEO_DOOR_PHONE => Yii::t('front/constructor', 'Видеодомофон'),
            self::BRIZER => Yii::t('front/constructor', 'Бризер'),
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'type' => 'Type',
            'price' => 'Price',
        ];
    }

    /**
     * @return array
     */
    public static function getDataOptions()
    {
        $returnData = [];

        $city = City::getUserCity();

        $data = ApartmentConstructor::find()
            ->where(['type' => ApartmentConstructor::CONSTRUCTOR_OPTIONS])
            ->orderBy(['position' => SORT_ASC])
            ->all();

        foreach ($data as $key => $item) {
            if (self::getOptionPrice($item, $city->id)) {
                $returnData[] = [
                    'id' => $key,
                    'name' => $item->label,
                    'src' => $item->src,
                    'about' => $item->description,
                    'price' => self::getOptionPrice($item, $city->id),
                    'preview' => $item->preview,
                    'type' => ApartmentConstructor::getTypeDictionary()[$item->type],
                ];
            }
        }

        return $returnData;
    }

    /**
     * @param $item
     * @param $city
     * @return bool|mixed
     */
    public static function getOptionPrice($item, $city)
    {
        $option = self::find()
            ->where([
                'option' => $item->option,
                'apartment_constructor_option_price.city_id' => $city,
                'apartment_constructor_option_price.published' => true
            ])
            ->one();

        if ($option) {
            return $option->price;
        }

        return false;
    }
}
