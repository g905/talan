<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%sub_menu}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $object_id
 * @property string $label
 * @property string $link
 * @property string $prepared_page_id
 * @property integer $show_right
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 */
class SubMenu extends ActiveRecord
{
    const TYPE_CAREER = 1;
    const TYPE_SPECIALIZATION = 2;
    const TYPE_VACANCIES_PAGE = 3;
    const TYPE_VACANCY = 4;
    const TYPE_EARTH = 5;
    const TYPE_REALTOR = 6;
    const TYPE_BUY_PAGE = 7;
    const TYPE_BUY_HOW_PAGE = 8;
    const TYPE_PUBLIC_DATA = 9;
    const TYPE_CATALOG = 10;
    const TYPE_HYPOTEC = 11;
    const TYPE_INSTALLMENT = 12;

    public static function tableName()
    {
        return '{{%sub_menu}}';
    }

    public function getMenuItemLink()
    {
        return $this->prepared_page_id
            ? SiteMenu::getPreparedPageLinkDictionary()[$this->prepared_page_id] ?? '#'
            : $this->link;
    }
}
