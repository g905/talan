<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "entrance".
 *
 * @property int $id
 * @property string $label Label
 * @property string $content Content
 * @property int $published Published
 * @property int $position Position
 * @property int $created_at Created At
 * @property int $updated_at Updated At
 * @property int $building_id
 *
 * @property ApartmentBuilding $building
 */
class Entrance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrance';
    }

    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
            [['label', 'created_at', 'updated_at'], 'required'],
            [['content'], 'string'],
            [['published', 'position', 'created_at', 'updated_at', 'building_id'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['building_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApartmentBuilding::className(), 'targetAttribute' => ['building_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/home', 'ID'),
            'label' => Yii::t('back/home', 'Label'),
            'content' => Yii::t('back/home', 'Content'),
            'published' => Yii::t('back/home', 'Published'),
            'position' => Yii::t('back/home', 'Position'),
            'created_at' => Yii::t('back/home', 'Created At'),
            'updated_at' => Yii::t('back/home', 'Updated At'),
            'building_id' => Yii::t('back/home', 'Building ID'),
        ];
    }
    public function behaviors()
    {
       return [
           'timestamp' => TimestampBehavior::class,
       ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(ApartmentBuilding::className(), ['id' => 'building_id']);
    }
}
