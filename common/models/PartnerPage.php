<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%partner_page}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $city_id
 * @property integer $manager_id
 * @property string $form_bottom_title
 * @property string $form_bottom_description
 * @property string $form_onsubmit
 *
 * @property PartnerPageCard[] $cards
 * @property City $city
 * @property Manager $manager
 * @property PartnerPageMenu $menus
 */
class PartnerPage extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_page}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCards()
    {
        return $this->hasMany(PartnerPageCard::class, ['partner_page_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(PartnerPageMenu::class, ['partner_page_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @param $city City`
     * @return array|null|\yii\db\ActiveRecord|static
     */
    public static function getModelByCity($city)
    {
        if (empty($city->id)) {
            return null;
        }

        return static::find()
            ->alias('p')
            ->where([
                'p.city_id' => $city->id,
                'p.published' => 1,
            ])
            ->with(['cards'])
            ->limit(1)
            ->one();
    }
}
