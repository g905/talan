<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%builder_widget_attribute_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $value
 */
class BuilderWidgetAttributeTranslation extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%builder_widget_attribute_translation}}';
    }
}
