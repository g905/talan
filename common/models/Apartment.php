<?php

namespace common\models;

use common\components\CommonDataModel;
use yii\behaviors\TimestampBehavior;
use common\helpers\Property;
use common\helpers\SiteUrlHelper;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%apartment}}".
 *
 * @property integer $id
 * @property integer $apartment_complex_id
 * @property string $label
 * @property integer $type
 * @property integer $subtype
 * @property integer $entrance
 * @property integer $rooms_number
 * @property string $total_area
 * @property string $living_space
 * @property string $delivery_date
 * @property float $price_from
 * @property string $description_title
 * @property string $description_subtitle
 * @property string $description_text
 * @property string $floor_plan_title
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $view_count
 * @property integer $floor
 * @property integer $guid
 * @property string $form_onsubmit
 * @property string $profile
 * @property string $income
 * @property string $recoupment
 * @property integer $apartment_building_id
 * @property integer $apartment_number
 * @property integer $apartment_number_on_floor
 * @property float $installment_price
 * @property float $price_per_meter
 * @property float $discount
 * @property bool $is_promotional
 * @property bool $is_secondary
 * @property integer $status
 * @property bool $deleted
 *
 * @property ApartmentComplex $apartmentComplex
 * @property ApartmentBuilding $apartmentBuilding
 * @property EntityToFile[] $photos
 * @property EntityToFile $descriptionPhoto
 * @property EntityToFile $floorPlanPhoto
 */
class Apartment extends ActiveRecord
{
    const SAVE_ATTRIBUTE_PHOTOS = 'ApartmentPhotos';
    const SAVE_ATTRIBUTE_DESCRIPTION_PHOTO = 'ApartmentDescription_photo';
    const SAVE_ATTRIBUTE_FLOOR_PLAN_PHOTO = 'ApartmentFloor_plan_photo';

    const NO_IMAGE_PATH = '/static/img/apartments/apartaments-hover.png';
    const ITEM_COUNT_PER_ROW = 3;

    const STATUS_UNDEFINED = 0;
    const STATUS_AVAILABLE = 1;
    const STATUS_SOLD = 2;
    const STATUS_RESERVED = 3;

    public static function tableName()
    {
        return '{{%apartment}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => ['class' => TimestampBehavior::class],
        ];
    }

    public function getViewUrl()
    {
        return SiteUrlHelper::createApartmentUrl(['id' => $this->id, 'alias' => $this->apartmentComplex->alias, 'type' => $this->type]);
    }

    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'apartment_complex_id']);
    }

    public function getApartmentBuilding()
    {
        return $this->hasOne(ApartmentBuilding::class, ['id' => 'apartment_building_id']);
    }

    public function getPhotos()
    {
        return $this->hasMany(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'photos.entity_model_name' => static::formName(),
                'photos.attribute' => static::SAVE_ATTRIBUTE_PHOTOS
            ])
            ->alias('photos')
            ->orderBy('photos.position DESC');
    }

    public function getPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'photos.entity_model_name' => static::formName(),
                'photos.attribute' => static::SAVE_ATTRIBUTE_PHOTOS
            ])
            ->alias('photos')
            ->orderBy('photos.position DESC');
    }

    public function getDescriptionPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'description_photo.entity_model_name' => static::formName(),
                'description_photo.attribute' => static::SAVE_ATTRIBUTE_DESCRIPTION_PHOTO
            ])
            ->alias('description_photo')
            ->orderBy('description_photo.position DESC');
    }

    public function getFloorPlanPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'floor_plan_photo.entity_model_name' => static::formName(),
                'floor_plan_photo.attribute' => static::SAVE_ATTRIBUTE_FLOOR_PLAN_PHOTO
            ])
            ->alias('floor_plan_photo')
            ->orderBy('floor_plan_photo.position DESC');
    }

    public function getHowBuyBlocks()
    {
        $city = $this->apartmentComplex->city;
        if (!isset($city)) {
            return null;
        }

        /** @var HowBuy $howBuy */
        $howBuy = HowBuy::findOneForCity($city->id);

        if (!isset($howBuy)) {
            return null;
        }

        return $howBuy->howBuyBlocks;
    }

    public function getRelatedFlats()
    {
        $morePrice = Apartment::find()
            ->where(['!=', 'id', $this->id])
            ->andWhere(['published' => 1, 'apartment_complex_id' => $this->apartment_complex_id])
            ->andWhere(['>=', 'price_from', $this->price_from])
            ->orderBy('price_from')
            ->limit(3);

        $lessPrice = Apartment::find()
            ->where(['!=', 'id', $this->id])
            ->andWhere(['published' => 1, 'apartment_complex_id' => $this->apartment_complex_id])
            ->andWhere(['<', 'price_from', $this->price_from])
            ->orderBy('price_from DESC')
            ->limit(3);

        $related = $morePrice->union($lessPrice)
            ->orderBy('price_from')
            ->all();

        return $related;
    }

    public function getSubtypeLabel()
    {
        return Property::subtypeLabel($this->subtype);
    }

    public function isResidential()
    {
        return $this->type == Property::TYPE_RESIDENTIAL;
    }

    public function isCommercial()
    {
        return $this->type == Property::TYPE_COMMERCIAL;
    }

    public function canShowPrice()
    {
        $complex = $this->apartmentComplex;
        $city = $complex->city;

        return $city->show_prices && $complex->show_prices && $this->price_from;
    }

    public function priceFormatted()
    {
        return formatter()->asInteger($this->price_from) . ' ' . CommonDataModel::getCurrencyText();
    }

    public function installmentPriceFormatted()
    {
        return formatter()->asInteger($this->installment_price) . ' ' . CommonDataModel::getCurrencyText();
    }

    public static function getApartmentsByTypeAndCity($type, $city)
    {
        return self::find()
            ->andWhere(['apartment.published' => true])
            ->andWhere(['apartment.type' => $type])
            ->joinWith(['apartmentComplex'])
            ->andWhere(['apartment_complex.city_id' => $city])
            ->limit(3)
            ->all();
    }

    public function getCity()
    {
        return $this->apartmentComplex->city;
    }

	/**
	 * Метод получает "Дату окончания строительства". Берется из даты окончания квартиры
	 * @param $complexId
	 * @param $apartmentComplexId
	 * @param null $entranceNum
	 * @param null $defaultDateValue - дата, которая будет подставляться, если результат поиска нулевой
	 * @return
	 */
	public static function getApartmentBuildingDateEnd($complexId, $apartmentComplexId, $entranceNum = null, $defaultDateValue = null) {
		$query1 = self::find()
			->select(['delivery_date'])
			->where([
				'apartment_complex_id' => $complexId,
				'apartment_building_id' => $apartmentComplexId,
				'published' => 1,
			])
			->orderBy('delivery_date DESC')
		;

		if(!empty($entranceNum)) {
			$query1->andWhere([
				'entrance' => $entranceNum,
			]);
		}

		$tmp = $query1->asArray()->one();
		// дата по умолчанию
		$resDate = $defaultDateValue;

		if(!empty($tmp['delivery_date'])) {
			$resDate = $tmp['delivery_date'];
		}
		$resDate = date('Y-m-d', strtotime($resDate));
		return $resDate;
	}
}
