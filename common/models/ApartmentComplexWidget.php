<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

class ApartmentComplexWidget extends ActiveRecord
{
    const SAVE_ATTRIBUTE_IMAGE = 'ApartmentComplexWidgetImage';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%apartment_complex_widget}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplexPage()
    {
        return $this->hasOne(ApartmentComplex::className(), ['id' => 'complex_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition([
                'image.entity_model_name' => static::formName(),
                'image.attribute' => static::SAVE_ATTRIBUTE_IMAGE
            ])
            ->alias('image')
            ->orderBy('image.position DESC');
    }

    public function getPositionFormatted()
    {
        if (strlen($this->position+1) == 1){
            return '0'.($this->position+1);
        }

        return $this->position+1;
    }
}
