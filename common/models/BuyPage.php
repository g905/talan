<?php

namespace common\models;

use common\models\blocks\BuyAdvantage;
use common\models\blocks\BuyStep;
use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%buy_page}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $top_screen_title
 * @property string $top_screen_sub_title
 * @property string $top_screen_description
 * @property string $top_screen_btn
 * @property string $form_top_title
 * @property string $form_top_emails
 * @property integer $form_top_square
 * @property string $form_top_btn
 * @property string $form_top_onsubmit
 * @property string $advantages_title
 * @property string $advantages_title_dark
 * @property string $steps_title
 * @property string $form_bottom_title
 * @property string $form_bottom_description
 * @property string $form_bottom_email
 * @property integer $manager_id
 * @property string $form_bottom_onsubmit
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property EntityToFile $topScreenBackgroundImage
 * @property SubMenu[] $subMenus
 */
class BuyPage extends ActiveRecord
{
    const SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE = 'BuyPageTop_screen_background_image';

    public static function tableName()
    {
        return '{{%buy_page}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City'),
            'top_screen_title' => Yii::t('app', 'Title'),
            'top_screen_sub_title' => Yii::t('app', 'Sub title'),
            'top_screen_description' => Yii::t('app', 'Description'),
            'top_screen_btn' => Yii::t('app', 'Button Text'),
            'top_screen_background_image' => Yii::t('app', 'Top screen background image'),
            'form_top_title' => Yii::t('app', 'Form top title'),
            'form_top_emails' => Yii::t('app', 'Form top emails'),
            'form_top_square' => Yii::t('app', 'Form Top Square'),
            'form_top_btn' => Yii::t('app', 'Form Top Button Text'),
            'form_top_onsubmit' => Yii::t('app', 'Form OnSubmit event'),
            'advantages_title' => Yii::t('app', 'Title'),
            'advantages_title_dark' => Yii::t('app', 'Title dark'),
            'steps_title' => Yii::t('app', 'Title'),
            'form_bottom_title' => Yii::t('app', 'Form bottom title'),
            'form_bottom_description' => Yii::t('app', 'Form bottom description'),
            'form_bottom_email' => Yii::t('app', 'Form bottom email'),
            'manager_id' => Yii::t('app', 'Manager ID'),
            'form_bottom_onsubmit' => Yii::t('app', 'Form OnSubmit event'),
            'published' => Yii::t('app', 'Published'),
            'position' => Yii::t('app', 'Position'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getSubMenus()
    {
        return $this->hasMany(SubMenu::class, ['object_id' => 'id'])
            ->onCondition([
                'type' => SubMenu::TYPE_BUY_PAGE,
                'published' => 1
                ]);
    }

    public function getTopScreenBackgroundImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'top_screen_background_image.entity_model_name' => static::formName(),
                'top_screen_background_image.attribute' => static::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE
            ])
            ->alias('top_screen_background_image')
            ->orderBy('top_screen_background_image.position DESC');
    }

    public function getAdvantages()
    {
        return $this->hasMany(BuyAdvantage::class, ['object_id' => 'id'])
            ->onCondition([
                'type' => BuyAdvantage::BUY_ADVANTAGE,
                'published' => 1
            ]);
    }

    public function getStepBlocks()
    {
        return $this->hasMany(BuyStep::class, ['object_id' => 'id'])
            ->onCondition([
                'type' => BuyStep::BUY_STEP,
                'published' => 1
            ]);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    /**
     * @param City $city
     * @return array|null|\yii\db\ActiveRecord|self
     */
    public static function findByCity($city)
    {
        return BuyPage::find()->where([
            'city_id' => $city->id,
            'published' => 1
        ])->one();
    }
}
