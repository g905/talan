<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%earth_marker}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $earth_id
 * @property integer $marker_type
 * @property string $lat
 * @property string $lng
 * @property string $description
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Earth $earth
 */
class EarthMarker extends ActiveRecord
{
    const MARKER_TYPE_HOME_REGION = 1;
    const MARKER_TYPE_NEW_REGION = 2;

    public static function tableName()
    {
        return '{{%earth_marker}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'label' => Yii::t('app', 'Label'),
            'earth_id' => Yii::t('app', 'Earth'),
            'marker_type' => Yii::t('app', 'Marker type'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
            'description' => Yii::t('app', 'Description'),
            'published' => Yii::t('app', 'Published'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    public static function getMarkerTypeClass()
    {
        return [
            self::MARKER_TYPE_HOME_REGION => 'home',
            self::MARKER_TYPE_NEW_REGION => 'new',
        ];
    }

    public static function getMarkerTypeMapIco()
    {
        return [
            self::MARKER_TYPE_HOME_REGION => '/static/img/map/main_pin.png',
            self::MARKER_TYPE_NEW_REGION => '/static/img/map/main_pin_2.png',
        ];
    }

    public static function getMarkerTypeDictionary()
    {
        return [
            self::MARKER_TYPE_HOME_REGION => bt('Home region', 'earth-marker'),
            self::MARKER_TYPE_NEW_REGION => bt('New region', 'earth-marker'),
        ];
    }

    public function getEarth()
    {
        return $this->hasOne(Earth::class, ['id' => 'earth_id']);
    }
}
