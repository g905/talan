<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%request_guarantee}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $building_id
 * @property integer $problem_type
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $problem
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ApartmentBuilding $building
 * @property City $city
 */
class RequestGuarantee extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%request_guarantee}}';
    }

    public function behaviors()
    {
        return ['timestamp' => TimestampBehavior::class];
    }

    public function getBuilding()
    {
        return $this->hasOne(ApartmentBuilding::class, ['id' => 'building_id']);
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
