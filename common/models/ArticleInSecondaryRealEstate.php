<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%article_in_secondary_real_estate}}".
 *
 * @property integer $id
 * @property integer $secondary_real_estate_id
 * @property integer $article_id
 *
 * @property SecondaryRealEstate $secondaryRealEstate
 * @property Article $article
 */
class ArticleInSecondaryRealEstate extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article_in_secondary_real_estate}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondaryRealEstate()
    {
        return $this->hasOne(SecondaryRealEstate::className(), ['id' => 'secondary_real_estate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }
}
