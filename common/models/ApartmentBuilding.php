<?php

namespace common\models;

//use backend\modules\block\models\block\BuildingAdvantage;
use backend\modules\apartment\models\PromoActionToAb;
use common\models\blocks\BuildingAdvantage;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\helpers\Url;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%apartment_building}}".
 *
 * @property integer $id
 * @property integer $building_complex_id
 * @property string $label
 * @property string $top_screen_description
 * @property string $top_screen_btn_label
 * @property string $top_screen_btn_link
 * @property string $general_progress_title
 * @property string $general_progress_percent
 * @property string $general_description
 * @property string $general_docs_link
 * @property string $general_docs_link_title
 * @property string $general_fb_link
 * @property string $general_vk_link
 * @property string $general_in_link
 * @property string $investment_title_first
 * @property string $investment_title_second
 * @property string $investment_description
 * @property string $investment_btn_label
 * @property string $investment_btn_link
 * @property string $articles_label
 * @property string $projects_label
 * @property string $projects_description
 * @property string $advantages_title_first_part
 * @property string $advantages_title_second_part
 * @property string $form_top_title_first
 * @property string $form_top_title_second
 * @property string $form_top_description
 * @property string $form_top_emails
 * @property string $form_middle_title_first
 * @property string $form_middle_title_second
 * @property string $form_middle_description
 * @property integer $form_middle_problem
 * @property string $form_middle_emails
 * @property string $form_middle_onsubmit
 * @property string $form_bottom_title
 * @property string $form_bottom_description
 * @property string $form_bottom_email
 * @property integer $manager_id
 * @property integer $view_count
 * @property integer $custom_popup_id
 * @property string $alias
 * @property string $guid
 * @property string $form_top_onsubmit
 * @property string $form_bottom_onsubmit
 * @property integer $show_label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $entrance
 * @property integer $floors
 * @property string $block_number
 *
 * @property string $build_video
 * @property string $build_start_date
 * @property string $build_end_date
 * @property string $build_history_title
 * @property string $build_history_end_text
 * @property string $build_history_current_text
 *
 * @property ApartmentComplex $buildingComplex
 * @property Manager $manager
 * @property RequestProblemAssign $problems
 * @property Article[] $articles
 * @property Apartment[] $apartments
 * @property CompletedProject[] $projects
 * @property CustomPopup $customPopup
 * @property AcMenu $apartmentBuildingMenu
 * @property BuildingAdvantage $advantages
 * @property PromoAction $promoActions
 *
 * @property EntityToFile $topScreenBackgroundImage
 * @property EntityToFile $topScreenBackgroundVideo
 * @property EntityToFile $investmentPhoto
 * @property EntityToFile[] $generalPhotos
 */
class ApartmentBuilding extends ActiveRecord
{
    const SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE = 'ApartmentComplexTop_screen_background_image';
    const SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO = 'ApartmentComplexTop_screen_background_video';
    const SAVE_ATTRIBUTE_GENERAL_PHOTOS = 'ApartmentComplexGeneral_photos';
    const SAVE_ATTRIBUTE_INVESTMENT_IMAGE = 'ApartmentBuildingInvestment_image';
    const SAVE_ATTRIBUTE_SECTIONS_IMAGE = 'ApartmentBuildingSections_image';

    public static function tableName()
    {
        return '{{%apartment_building}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'building_complex_id' => Yii::t('app', 'Building complex'),
            'label' => Yii::t('app', 'Label'),
            'top_screen_description' => Yii::t('app', 'Top screen description'),
            'top_screen_btn_label' => Yii::t('app', 'Top screen button label'),
            'top_screen_btn_link' => Yii::t('app', 'Top screen button link'),
            'general_progress_title' => Yii::t('app', 'General progress title'),
            'general_progress_percent' => Yii::t('app', 'General progress percent'),
            'general_description' => Yii::t('app', 'General description'),
            'general_docs_link' => Yii::t('app', 'General docs link'),
            'general_docs_link_title' => Yii::t('app', 'General_docs link title'),
            'general_fb_link' => Yii::t('app', 'General fb link'),
            'general_vk_link' => Yii::t('app', 'General vk link'),
            'general_in_link' => Yii::t('app', 'General inst link'),
            'investment_title_first' => Yii::t('app', 'Title first part'),
            'investment_title_second' => Yii::t('app', 'Title second part'),
            'investment_description' => Yii::t('app', 'Description'),
            'investment_btn_label' => Yii::t('app', 'Button label'),
            'investment_btn_link' => Yii::t('app', 'Button link'),
            'articles_label' => Yii::t('app', 'Articles label'),
            'projects_label' => Yii::t('app', 'Projects label'),
            'projects_description' => Yii::t('app', 'Projects description'),
            'advantages_title_first_part' => Yii::t('app', 'Advantages title first part'),
            'advantages_title_second_part' => Yii::t('app', 'Advantages title second part'),
            'promoIds' => bt('Promo actions', 'apartment-complex'),
            'form_top_title_first' => Yii::t('app', 'Form top title first part'),
            'form_top_title_second' => Yii::t('app', 'Form top title second part'),
            'form_top_description' => Yii::t('app', 'Form top description'),
            'form_top_emails' => Yii::t('app', 'Form top emails'),
            'form_middle_title_first' => Yii::t('app', 'Form middle title first part'),
            'form_middle_title_second' => Yii::t('app', 'Form middle title second part'),
            'form_middle_description' => Yii::t('app', 'Form middle description'),
            'form_middle_problem' => Yii::t('app', 'Form middle problem type'),
            'form_middle_emails' => Yii::t('app', 'Form middle emails'),
            'form_middle_onsubmit' => Yii::t('app', 'Form OnSubmit event'),
            'form_bottom_title' => Yii::t('app', 'Form bottom title'),
            'form_bottom_description' => Yii::t('app', 'Form bottom description'),
            'form_bottom_email' => Yii::t('app', 'Form bottom email'),
            'manager_id' => Yii::t('app', 'Manager ID'),
            'view_count' => Yii::t('app', 'View count'),
            'guid' => Yii::t('app', '1C Guid'),
            'form_top_onsubmit' => Yii::t('app', 'Form OnSubmit event'),
            'form_bottom_onsubmit' => Yii::t('app', 'Form OnSubmit event'),
            'show_label' => Yii::t('app', 'Whether to show label'),
            'published' => Yii::t('app', 'Published'),
            'position' => Yii::t('app', 'Position'),
            'articlesField' => Yii::t('app', 'Articles'),
            'projectsField' => Yii::t('app', 'Projects'),
            'alias' => Yii::t('app', 'Alias'),
            'custom_popup_id' => Yii::t('app', 'Popup'),
            'cta_black_label' => bt('Cta black label', 'apartment-complex'),
            'cta_white_label' => bt('Cta white label', 'apartment-complex'),
            'cta_description' => bt('Cta description', 'apartment-complex'),
            'cta_button_label' => bt('Cta button label', 'apartment-complex'),
            'cta_button_link' => bt('Cta button link', 'apartment-complex'),
            'cta_published' => bt('Cta published', 'apartment-complex'),
            'show_chess' => bt('Show Chess', 'apartment-complex'),
			'special_offer_text' => 'Текст для кнопки спец.предложение',
			'special_offer_link' => 'Ссылка для кнопки спец.предложение',
			'special_offer_description' => 'Текст описание спец.предложения',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getBuildingComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'building_complex_id']);
    }

    public function getApartments()
    {
        return $this->hasMany(Apartment::class, ['apartment_building_id' => 'id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getPromoActionToAbs()
    {
        return $this->hasMany(PromoAction::class, ['ab_id' => 'id']);
    }

    public function getInvestmentPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'investment_image.entity_model_name' => static::formName(),
                'investment_image.attribute' => static::SAVE_ATTRIBUTE_INVESTMENT_IMAGE
            ])
            ->alias('investment_image')
            ->orderBy('investment_image.position DESC');
    }

    public function getGeneralPhotos()
    {
        return $this->hasMany(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'general_photos.entity_model_name' => static::formName(),
                'general_photos.attribute' => static::SAVE_ATTRIBUTE_GENERAL_PHOTOS
            ])
            ->alias('general_photos')
            ->orderBy('general_photos.position DESC');
    }

    public function getGeneralPhotoSrc()
    {
        return $this->getEntityThumb('generalPhoto', 'building', 'list');
    }

    public function getGeneralPhoto()
    {
        return $this->getEntityModel(self::SAVE_ATTRIBUTE_GENERAL_PHOTOS, 'generalPhoto');
    }

    public function getCustomPopup()
    {
        return $this->hasOne(CustomPopup::class, ['id' => 'custom_popup_id']);
    }

    public function getApartmentBuildingMenu()
    {
        return $this->hasMany(AcMenu::class, ['apartment_building_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getAdvantages()
    {
        return $this->hasMany(BuildingAdvantage::class, ['object_id' => 'id'])
            ->onCondition(['type' => BuildingAdvantage::BUILDING_ADVANTAGE]);
    }

    public function getPromoActions()
    {
        return $this->hasMany(PromoAction::class, ['id' => 'action_id'])
            ->viaTable(PromoActionToAb::tableName(), ['ab_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->andWhere(['<', 'promo_action.start_date', time()])
            ->andWhere(['>', 'promo_action.end_date', time()])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getArticles()
    {
        return $this->hasMany(Article::class, ['id' => 'article_id'])
            ->alias('a')
            ->where(['a.published' => self::IS_PUBLISHED])
            ->viaTable(ArticleAssign::tableName(), ['object_id' => 'id'], function (ActiveQuery $q) {
                $q->alias('aa')->andWhere(['aa.type' => ArticleAssign::TYPE_BUILDING_ARTICLES])->limit(4);
            })
            ->innerJoin(ArticleAssign::tableName() . ' aa', 'aa.article_id = a.id')
            ->orderBy(['aa.position' => SORT_ASC]);
    }

    public function getProjects()
    {
        return $this->hasMany(CompletedProject::class, ['id' => 'project_id'])
            ->alias('p')
            ->where(['p.published' => self::IS_PUBLISHED])
            ->viaTable(CompletedProjectAssign::tableName(), ['object_id' => 'id'], function (ActiveQuery $q) {
                $q->alias('pa')->andWhere(['pa.type' => CompletedProjectAssign::TYPE_BUILDING_PROJECTS]);
            })
            ->innerJoin(CompletedProjectAssign::tableName() . ' pa', 'pa.project_id = p.id')
            ->orderBy(['pa.position' => SORT_ASC]);
    }

    public function getProblems()
    {
        return $this->hasMany(RequestProblem::class, ['id' => 'problem_id'])
            ->alias('p')
            ->where(['p.published' => self::IS_PUBLISHED])
            ->viaTable(RequestProblemAssign::tableName(), ['object_id' => 'id'], function (ActiveQuery $q) {
                $q->alias('pa')->andWhere(['pa.type' => RequestProblemAssign::TYPE_BUILDING_PROBLEMS]);
            })
            ->innerJoin(RequestProblemAssign::tableName() . ' pa', 'pa.problem_id = p.id')
            ->orderBy(['pa.position' => SORT_ASC]);
    }

    public function getTopScreenBackgroundImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'top_screen_background_image.entity_model_name' => static::formName(),
                'top_screen_background_image.attribute' => static::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE
            ])
            ->alias('top_screen_background_image')
            ->orderBy('top_screen_background_image.position DESC');
    }

    public function getTopScreenBackgroundVideo()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'top_screen_background_video.entity_model_name' => static::formName(),
                'top_screen_background_video.attribute' => static::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO
            ])
            ->alias('top_screen_background_video')
            ->orderBy('top_screen_background_video.position DESC');
    }

    /**
     * Find one published record by its alias or throw not found exception.
     *
     * @param string $alias record alias.
     * @return null|\yii\db\ActiveRecord|static
     * @throws \yii\web\NotFoundHttpException
     */
    public static function findOnePublishedByAliasOrFail($alias)
    {
        return static::checkFail(
            static::find()->where(['alias' => $alias, 'published' => self::IS_PUBLISHED])->one()
        );
    }

    /**
     * Find one published record by its id or throw not found exception.
     *
     * @param string $id record id.
     * @return null|\yii\db\ActiveRecord|static
     * @throws \yii\web\NotFoundHttpException
     */
    public static function findOnePublishedByIdOrFail($id)
    {
        return static::checkFail(
            static::find()->where(['id' => $id, 'published' => self::IS_PUBLISHED])->one()
        );
    }

    public static function findOneWithChessByIdOrFail($id)
    {
        return static::checkFail(
            static::find()->where(['id' => $id, 'show_chess' => true])->one()
        );
    }

    public function getSectionsImageSrc()
    {
        return $this->getEntitySource('sectionsImage');
    }

    public function getSectionsImage()
    {
        return $this->getEntityModel(self::SAVE_ATTRIBUTE_SECTIONS_IMAGE, 'building_sections_image');
    }

    /**
     * Return model view URL.
     *
     * @param $alias
     * @return string
     */
    public function getViewUrl($alias)
    {
        return static::createUrl('/apartment/apartment-building/view', ['alias' => $alias, 'id' => $this->id]);
    }

    public function getCity()
    {
        return $this->buildingComplex->city;
    }

    public function getPositionFormatted($position)
    {
        if (strlen($position+1) == 1) {
            return '0'.($position+1);
        }

        return $position+1;
    }
}
