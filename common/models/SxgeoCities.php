<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%sxgeo_cities}}".
 *
 * @property integer $id
 * @property integer $region_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $lat
 * @property string $lon
 * @property string $okato
 */
class SxgeoCities extends ActiveRecord 
{
    const TYPE_LOCATION = 0;
    const TYPE_PRELOAD = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sxgeo_cities}}';
    }

    public static function getCityById($id)
    {
        return self::find()->andWhere(['id' => $id])->one();
    }
}
