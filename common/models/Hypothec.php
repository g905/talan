<?php

namespace common\models;

use backend\modules\menu\models\HypotecSubMenu;
use yii\web\NotFoundHttpException;
use common\models\blocks\HypothecStep;
use common\models\blocks\HypothecBenefit;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%hypothec}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $manager_id
 * @property string $label
 * @property string $alias
 * @property string $description_label
 * @property string $description_text
 * @property string $benefit_label
 * @property string $benefit_text
 * @property integer $default_apartment_price
 * @property integer $min_term
 * @property integer $max_term
 * @property integer $default_time_term
 * @property integer $default_initial_fee
 * @property integer $benefits_block_label
 * @property integer $steps_block_label
 * @property string $form_calc_onsubmit
 * @property string $form_bottom_onsubmit
 * @property integer $view_count
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property City $city
 * @property Manager $manager
 * @property Bank[] $banks
 * @property HypothecBenefit[] $benefitBlocks
 * @property HypothecStep[] $stepBlocks
 */
class Hypothec extends ActiveRecord
{
    const ATTR_DESCRIPTION_IMAGE = 'descriptionImage';
    const ATTR_BENEFIT_IMAGE = 'benefitImage';

    public static function findByCityWithBenefitsAndStepsOrFail($cityId)
    {
        $model = self::find()
            ->alias('h')
            ->where(['h.published' => 1])
            ->joinWith(['benefitBlocks', 'stepBlocks'])
            ->andWhere(['h.city_id' => $cityId])->one();

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }

    public function getBenefitImage()
    {
        return $this->getEntityModel(self::ATTR_BENEFIT_IMAGE, 'hypothecBenefit', 'Hypothec');
    }

    public function getSubMenus()
    {
        return $this->hasMany(HypotecSubMenu::class, ['object_id' => 'id'])
            ->orderBy(['position' => SORT_ASC])
            ->onCondition(['type' => HypotecSubMenu::TYPE_HYPOTEC]);
    }

    public function getBenefitImageSrc()
    {
        return $this->getEntityThumb('benefitImage', 'hypothec', 'mainbenefit');
    }

    public function getDescriptionPhoto()
    {
        return $this->getEntityModel(self::ATTR_DESCRIPTION_IMAGE, 'hypothec', 'Hypothec');
    }

    public function getDescriptionPhotoPreviewSrc()
    {
        return $this->getEntityThumb('descriptionPhoto', 'hypothec', 'preview');
    }

    public static function tableName()
    {
        return '{{%hypothec}}';
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getBenefitBlocks()
    {
        return $this->hasMany(HypothecBenefit::class, ['object_id' => 'id'])
            ->alias('hb')
            ->andOnCondition([
                'hb.type' => HypothecBenefit::HYPOTHEC_BENEFIT_BLOCK,
                'hb.published' => self::IS_PUBLISHED,
            ]);
    }

    public function getStepBlocks()
    {
        return $this->hasMany(HypothecStep::class, ['object_id' => 'id'])
            ->alias('hs')
            ->andOnCondition([
                'hs.type' => HypothecStep::HYPOTHEC_STEP_BLOCK,
                'hs.published' => self::IS_PUBLISHED,
            ]);
    }

    public function getBanks()
    {
        return $this->hasMany(Bank::class, ['id' => 'bank_id'])->viaTable(BankToHypothec::tableName(), ['hypothec_id' => 'id']);
    }
}
