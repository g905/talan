<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;
use backend\modules\apartment\models\ApartmentBuilding;

/**
 * This is the model class for table "{{%request_complex_presentation}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $ac_id
 * @property integer $ab_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property ApartmentComplex $apartmentComplex
 * @property ApartmentBuilding $apartmentBuilding
 */
class RequestComplexPresentation extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%request_complex_presentation}}';
    }

    public function behaviors()
    {
        return ['timestamp' => TimestampBehavior::class];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'ac_id']);
    }

    public function getApartmentBuilding()
    {
        return $this->hasOne(ApartmentBuilding::class, ['id' => 'ab_id']);
    }
}
