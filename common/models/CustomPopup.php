<?php

namespace common\models;

/**
 * This is the model class for table "{{%custom_popup}}".
 *
 * @property integer $id
 * @property string $white_label
 * @property string $black_label
 * @property string $sub_label
 * @property string $label_placeholder
 * @property string $phone_placeholder
 * @property string $email_placeholder
 * @property string $agreement_text
 * @property string $button_label
 * @property integer $raise_time
 * @property string $url_param_value
 * @property integer $is_default
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $form_onsubmit
 */
class CustomPopup extends \common\components\model\ActiveRecord
{
    /**
     * Param name for setting popup from URL
     * @var string
     */
    const URL_PARAM_NAME = 'popup';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%custom_popup}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'white_label' => 'White title',
            'black_label' => 'Black title',
            'sub_label' => 'Sub-title',
            'label_placeholder' => 'Label placeholder',
            'phone_placeholder' => 'Phone placeholder',
            'email_placeholder' => 'Email placeholder',
            'agreement_text' => 'Agreement text',
            'button_label' => 'Send button text',
            'raise_time' => 'Time for popup appearing',
            'url_param_value' => 'Url param value to show this popup',
            'is_default' => 'Is default popup',
            'form_onsubmit' => 'Form onsubmit',
        ];
    }
}
