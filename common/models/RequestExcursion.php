<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%request_excursion}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $apartment_id
 * @property string $name
 * @property string $phone
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property Apartment $apartment
 * @property ApartmentComplex $apartmentComplex
 */
class RequestExcursion extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%request_excursion}}';
    }

    public function behaviors()
    {
        return ['timestamp' => TimestampBehavior::class];
    }

    public function getApartment()
    {
        return $this->hasOne(Apartment::class, ['id' => 'apartment_id']);
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'apartment_complex_id'])
            ->viaTable(Apartment::tableName(), ['id' => 'apartment_id']);
    }
}
