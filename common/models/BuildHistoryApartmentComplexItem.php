<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%build_history_apartment_complex_item}}".
 *
 * @property integer $id
 * @property integer $apartment_complex_id
 * @property string $date
 * @property string $percent
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ApartmentComplex $apartmentComplex
 */
class BuildHistoryApartmentComplexItem extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%build_history_apartment_complex_item}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'apartment_complex_id' => 'Apartment complex',
            'date' => 'Date (month + year)',
            'percent' => 'Percent of completition',
            'percent_plan' => 'Percent of completition plan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'apartment_complex_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlides()
    {
        return $this->hasMany(BuildHistoryApartmentComplexItemSlide::class, ['build_history_apartment_complex_item_id' => 'id']);
    }
}
