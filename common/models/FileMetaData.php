<?php

namespace common\models;

use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "{{%file_meta_data}}".
 *
 * @property integer $id
 * @property integer $file_id
 * @property string  $alt
 * @property integer $created_at
 * @property integer $updated_at
 */
class FileMetaData extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%file_meta_data}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'alt',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(FileMetaDataTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class'                 => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }
}
