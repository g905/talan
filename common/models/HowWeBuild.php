<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%how_we_build}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $content
 * @property integer $home_page_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Home $homePage
 * @property EntityToFile $image
 */
class HowWeBuild extends ActiveRecord
{
    const SAVE_ATTRIBUTE_IMAGE = 'HowWeBuildImage';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%how_we_build}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHomePage()
    {
        return $this->hasOne(Home::className(), ['id' => 'home_page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition([
                'image.entity_model_name' => static::formName(),
                'image.attribute' => static::SAVE_ATTRIBUTE_IMAGE
            ])
            ->alias('image')
            ->orderBy('image.position DESC');
    }

    public function getPositionFormatted()
    {
        if (strlen($this->position+1) == 1){
            return '0'.($this->position+1);
        }

        return $this->position+1;
    }
}
