<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%ac_public_service}}".
 *
 * @property integer $id
 * @property integer $apartment_complex_id
 * @property string $label
 * @property string $content
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ApartmentComplex $apartmentComplex
 * @property EntityToFile $photo
 */
class AcPublicService extends ActiveRecord 
{
    const SAVE_ATTRIBUTE_PHOTO = 'AcPublicServicePhoto';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ac_public_service}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::className(), ['id' => 'apartment_complex_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPhoto()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['photo.entity_model_name' => static::formName(), 'photo.attribute' => static::SAVE_ATTRIBUTE_PHOTO])
            ->alias('photo')
            ->orderBy('photo.position DESC');
    }
}
