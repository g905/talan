<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%about_company_menu}}".
 *
 * @property integer $id
 * @property integer $about_company_id
 * @property string $label
 * @property string $alias
 * @property string $link
 * @property integer $show_right
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AboutCompany $aboutCompany
 */
class AboutCompanyMenu extends ActiveRecord 
{
    public static function tableName()
    {
        return '{{%about_company_menu}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getAboutCompany()
    {
        return $this->hasOne(AboutCompany::class, ['id' => 'about_company_id']);
    }

    public function getMenuItemLink()
    {
        return $this->link;
    }
}
