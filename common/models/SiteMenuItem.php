<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%site_menu_item}}".
 *
 * @property integer $id
 * @property integer $site_menu_id
 * @property string $label
 * @property string $link
 * @property integer $prepared_page_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property SiteMenu $siteMenu
 */
class SiteMenuItem extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_menu_item}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteMenu()
    {
        return $this->hasOne(SiteMenu::className(), ['id' => 'site_menu_id']);
    }

    public function getMenuItemLink()
    {
        if ((isset($this->prepared_page_id)) && ($this->prepared_page_id != '')) {
            return SiteMenu::getPreparedPageLinkDictionary()[$this->prepared_page_id] ?? '';
        }

        return $this->link;
    }
}
