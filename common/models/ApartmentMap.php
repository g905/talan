<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "apartment_map".
 *
 * @property integer $id
 * @property string $label
 * @property string $content
 * @property integer $complex_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class ApartmentMap extends \common\components\model\ActiveRecord
{
    /**
     *
     */
    const IMAGE = 'complexMapImage';

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'apartment_map';
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'content' => 'Content',
            'mob_content' => 'Mobile Content',
            'complex_id' => 'Complex Id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])->andOnCondition([
            'AND',
            ['attribute' => self::IMAGE],
            ['entity_model_name' => self::formName()]
        ]);
    }

    public static function getByComplexId($complex_id)
    {
        if(!self::find()->where(['complex_id' => $complex_id])->one())
        {
            return self::find()->where(['complex_id' => 44])->one();
        }

        return self::find()->where(['complex_id' => $complex_id])->one();
    }
}
