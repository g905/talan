<?php

namespace common\models;

use common\components\ConfigurationComponent;
use common\models\base\StaticPage;
use Yii;

/**
 * This is class for static page model.
 */
class TeamPageDefaultCityTab extends StaticPage
{
    const DEFAULT_TAB_TEXT = 'teamPageDefaultCityTabDefaultTabText';

    public $defaultTabText;

    /**
     * Storing relation models of object to prevent multiple queries requests
     * @var array
     */
    protected $_relationsData = [];

    /**
    * @return array
    */
    public function getKeys()
    {
        return [
            self::DEFAULT_TAB_TEXT,
        ];
    }

    /**
    * @return $this
    */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->defaultTabText = $config->get(self::DEFAULT_TAB_TEXT);

        return $this;
    }

    /**
     * @return TeamDefaultCityMember[]
     */
    public function getTeamMembers()
    {
        $methodName = __METHOD__;
        if (!isset($this->_relationsData[$methodName])) {
            $this->_relationsData[$methodName] = TeamDefaultCityMember::find()
                ->where(['page_id' => $this->getId()])
                ->andWhere(['is not', 'manager_id', null])
                ->with(['manager'])
                ->orderBy(['position' => SORT_ASC])
                ->all();
        }

        return $this->_relationsData[$methodName];
    }
}
