<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "view_count".
 *
 * @property int $id
 * @property int $ip
 * @property int $item_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property ViewItem $item
 * @property ViewItem $viewItem
 */
class ViewCount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'view_count';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ip', 'item_id', 'created_at', 'updated_at'], 'integer'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => ViewItem::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'item_id' => 'Item ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViewItem()
    {
        return $this->hasOne(ViewItem::class, ['id' => 'item_id']);
    }

}
