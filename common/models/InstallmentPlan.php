<?php

namespace common\models;

use backend\modules\menu\models\InstallmentSubMenu;
use yii\web\NotFoundHttpException;
use common\components\model\ActiveRecord;
use common\models\blocks\InstallmentPlanBenefit;
use common\models\blocks\InstallmentPlanStep;

/**
 * This is the model class for table "{{%installment_plan}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $manager_id
 * @property string $label
 * @property string $alias
 * @property string $description_label
 * @property string $description_text
 * @property integer $default_apartment_price
 * @property integer $min_term
 * @property integer $max_term
 * @property integer $default_time_term
 * @property integer $default_initial_fee
 * @property string $benefits_block_label
 * @property string $steps_block_label
 * @property string $form_calc_onsubmit
 * @property string $form_bottom_onsubmit
 * @property integer $view_count
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property City $city
 * @property Manager $manager
 * @property InstallmentPlanBenefit[] $benefitBlocks
 * @property InstallmentPlanStep[] $stepBlocks
 */
class InstallmentPlan extends ActiveRecord
{
    const ATTR_DESCRIPTION_IMAGE = 'InstallmentPlanDescriptionPhoto';

    public static function tableName()
    {
        return '{{%installment_plan}}';
    }

    public static function findByCityWithBenefitsAndStepsOrFail($cityId)
    {
        $model = self::find()
            ->alias('ip')
            ->where(['ip.published' => 1])
            ->joinWith(['benefitBlocks', 'stepBlocks'])
            ->andWhere(['ip.city_id' => $cityId])->one();

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }

    public function getDescriptionPhoto()
    {
        return $this->getEntityModel(self::ATTR_DESCRIPTION_IMAGE, 'installmentPlan', 'InstallmentPlan');
    }

    public function getSubMenus()
    {
        return $this->hasMany(InstallmentSubMenu::class, ['object_id' => 'id'])
            ->orderBy(['position' => SORT_ASC])
            ->onCondition(['type' => InstallmentSubMenu::TYPE_INSTALLMENT]);
    }

    public function getDescriptionPhotoPreviewSrc()
    {
        return $this->getEntityThumb('descriptionPhoto', 'installmentplan', 'preview');
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getBenefitBlocks()
    {
        return $this->hasMany(InstallmentPlanBenefit::class, ['object_id' => 'id'])
            ->alias('bb')
            ->andOnCondition([
                'bb.type' => InstallmentPlanBenefit::INSTALLMENT_PLAN_BENEFIT_BLOCK,
                'bb.published' => self::IS_PUBLISHED,
            ]);
    }

    public function getStepBlocks()
    {
        return $this->hasMany(InstallmentPlanStep::class, ['object_id' => 'id'])
            ->alias('sb')
            ->andOnCondition([
                'sb.type' => InstallmentPlanStep::INSTALLMENT_PLAN_STEP_BLOCK,
                'sb.published' => self::IS_PUBLISHED,
            ]);
    }
}
