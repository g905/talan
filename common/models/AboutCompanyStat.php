<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%about_company_stat}}".
 *
 * @property integer $id
 * @property integer $about_company_id
 * @property string $number
 * @property string $label
 * @property string $content
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AboutCompany $aboutCompany
 */
class AboutCompanyStat extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%about_company_stat}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompany()
    {
        return $this->hasOne(AboutCompany::className(), ['id' => 'about_company_id']);
    }
}
