<?php

namespace common\models;

use common\components\model\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%public_data}}".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property string $label
 * @property string $alias
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PublicDataMenu $menu
 * @property SubMenu[] $subMenus
 */
class PublicData extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%public_data}}';
    }

    public function getMenu()
    {
        return $this->hasOne(PublicDataMenu::class, ['id' => 'menu_id'])->alias('menu');
    }

    public function getSubMenus()
    {
        return $this->hasMany(SubMenu::class, ['object_id' => 'id'])
            ->orderBy(['position' => SORT_ASC])
            ->onCondition(['type' => SubMenu::TYPE_PUBLIC_DATA]);
    }

    public function viewUrl()
    {
        return self::createUrl('/page/page/public-data', ['alias' => $this->alias]);
    }

    public static function prepareMenu()
    {
        $menus = PublicDataMenu::find()->innerJoinWith(['publicData' => function (ActiveQuery $q) {
            $q->alias('m')->andWhere(['m.published' => 1]);
        }])->ordered()->all();
        $pages = PublicData::find()->alias('d')
            ->andWhere(['d.menu_id' => null, 'd.published' => 1])->ordered()->all();

        return merge($menus, $pages);
    }

    public static function findByAliasOrFirstOrFail($alias = null)
    {
        $model = $alias === null
            ? static::find()->where(['published' => self::IS_PUBLISHED])->orderBy(['position' => SORT_ASC])->one()
            : static::find()->where(['alias' => $alias, 'published' => self::IS_PUBLISHED])->one();

        return static::checkFail($model);
    }
}
