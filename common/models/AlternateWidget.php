<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

class AlternateWidget extends ActiveRecord
{
    const SAVE_ATTRIBUTE_IMAGE = 'AlternateWidgetImage';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%alternate_widget}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHomePage()
    {
        return $this->hasOne(Home::className(), ['id' => 'home_page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition([
                'image.entity_model_name' => static::formName(),
                'image.attribute' => static::SAVE_ATTRIBUTE_IMAGE
            ])
            ->alias('image')
            ->orderBy('image.position DESC');
    }

    public function getPositionFormatted()
    {
        if (strlen($this->position+1) == 1){
            return '0'.($this->position+1);
        }

        return $this->position+1;
    }
}
