<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%team_default_city_members}}".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $manager_id
 * @property integer $page_id
 *
 * @property Manager $manager
 */
class TeamDefaultCityMember extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%team_default_city_member}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }
}
