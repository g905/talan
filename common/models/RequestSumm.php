<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "request_summ".
 *
 * @property int $id
 * @property int $ac_id
 * @property int $city_id
 * @property string $phone
 * @property int $created_at
 * @property int $updated_at
 *
 * @property ApartmentComplex $ac
 * @property City $city
 */
class RequestSumm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request_summ';
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            ['ac_id', 'integer'],
            [['city_id', 'created_at', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['phone'], 'string', 'max' => 255],
            [['ac_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApartmentComplex::className(), 'targetAttribute' => ['ac_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'phone' => 'Phone',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /*
    public function getAc()
    {
        return $this->hasOne(ApartmentComplex::className(), ['id' => 'ac_id']);
    }

*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
}
