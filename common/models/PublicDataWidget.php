<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%public_data_widget}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $city_id
 * @property integer $complex_id
 * @property string $label
 * @property string $chart_name
 * @property string $link
 * @property string $value
 * @property integer $status
 * @property string $placed
 * @property string $seria
 * @property integer $plan
 * @property integer $fact
 * @property integer $published
 * @property integer $position
 * @property string $content
 *
 * @property City $city
 * @property ApartmentComplex $complex
 * @property PublicDataChartData[] $chartData
 */
class PublicDataWidget extends ActiveRecord
{
    const TYPE_DIAGRAM_ROW = 1;
    const TYPE_DIAGRAM_DDU = 2;
    const TYPE_DIAGRAM_SCHEDULE = 3;
    const TYPE_RATING = 4;
    const TYPE_SERVICE_CHART_ROW = 5;
    const TYPE_DIAGRAM_CIRCLE = 6;
    const TYPE_COLUMN_CHART_ROW = 7;
    const TYPE_COLUMN_CHART = 8;
    const TYPE_CUSTOM_DIAGRAM_DDU = 9;

    public static function types()
    {
        return [
            self::TYPE_DIAGRAM_ROW => t('Diagram row', 'public-data-widget'),
            self::TYPE_DIAGRAM_DDU => t('Diagram ddu', 'public-data-widget'),
            self::TYPE_DIAGRAM_SCHEDULE => t('Diagram Schedule', 'public-data-widget'),
            self::TYPE_RATING => t('Rating Table', 'public-data-widget'),
            self::TYPE_DIAGRAM_CIRCLE => t('Diagram circle', 'public-data-widget'),
            self::TYPE_SERVICE_CHART_ROW => t('Service chart row', 'public-data-widget'),
            self::TYPE_COLUMN_CHART_ROW => t('Column chart row', 'public-data-widget'),
            self::TYPE_COLUMN_CHART => t('Column chart', 'public-data-widget'),
            self::TYPE_CUSTOM_DIAGRAM_DDU => t('Custom chart', 'public-data-widget'),
        ];
    }

    public static function tableName()
    {
        return '{{%public_data_widget}}';
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'complex_id']);
    }

    public function getChartData()
    {
        return $this->hasMany(PublicDataChartData::class, ['widget_id' => 'id']);
    }
}
