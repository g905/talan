<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%partner_page_menu}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $partner_page_id
 * @property string $link
 * @property integer $show_right
 *
 * @property PartnerPage $partnerPage
 */
class PartnerPageMenu extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_page_menu}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerPage()
    {
        return $this->hasOne(PartnerPage::className(), ['id' => 'partner_page_id']);
    }
}
