<?php

namespace common\models;

use Yii;
use yii\base\Event;

/**
 * This is the model class for table "constructor_request".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property integer $price
 * @property integer $city_id
 * @property integer $complex
 * @property integer $flat
 * @property string $data
 * @property string $type
 * @property integer $created_at
 * @property integer $updated_at
 */
class ConstructorRequest extends \common\components\model\ActiveRecord
{
    const FULL_PRICE_FORM = 0;
    const CREDIT_HOUSING_FORM = 1;
    const WITH_DECORATION_FORM = 2;
    public $apartment_id;
    public $apartment_complex_id;
    public $name;
    public $username;
    public $phone;
    public $email;

    public static function tableName()
    {
        return 'constructor_request';
    }

    /**
     * @return array
     */
    public static function getTypeDictionary()
    {
        return [
            self::FULL_PRICE_FORM => Yii::t('front/constructor', 'Вся сумма'),
            self::CREDIT_HOUSING_FORM => Yii::t('front/constructor', 'В зачет жилья'),
            self::WITH_DECORATION_FORM => Yii::t('front/constructor', 'С отделкой'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'price' => 'Price',
            'city_id' => 'City ID',
            'complex_id' => 'Complex',
            'flat_id' => 'Flat',
            'data' => 'Data',
            'type' => 'Type',
        ];
    }

    public function bitrixSend() {

        if($this->validate()) {
//var_dump($this->apartment_id);
            $formTitle = 'Цена с отделкой';
            //$apartment = Apartment::find()->where(["id"=>$this->apartment_id])->one();

            app()->trigger('bitrixData',
                new Event(
                    [
                        'sender' => [
                            'title' => $formTitle,
                            'source_description' => 'заявка с сайта - ' . $this->apartment_id,
                            'form' => [
                                'phone' => $this->phone,
                                'name' => $this->username,
                                'comment' => $this->apartment_complex_id.", ".$this->apartment_id,
                                //'comment' => $this->apartment_id
                            ],
                        ],
                    ]
                )
            );

            return true;
        }
        return null;
    }

}
