<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%completed_project_to_complex_list}}".
 *
 * @property integer $complex_list_id
 * @property integer $completed_project_id
 * @property integer $position
 *
 * @property ComplexList $complexList
 * @property CompletedProject $completedProject
 */
class CompletedProjectToComplexList extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%completed_project_to_complex_list}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplexList()
    {
        return $this->hasOne(ComplexList::class, ['id' => 'complex_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompletedProject()
    {
        return $this->hasOne(CompletedProject::class, ['id' => 'completed_project_id']);
    }
}
