<?php

namespace common\models;

use yii\web\NotFoundHttpException;
use common\components\model\ActiveRecord;
use common\models\blocks\ContactPageHowToGetStep;

/**
 * This is the model class for table "{{%contact_page}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $label
 * @property string $sales_phones
 * @property string $reception_phones
 * @property string $address
 * @property string $email
 * @property string $timetable
 * @property string $hot_to_get_label
 * @property string $forms_label
 * @property integer $map_zoom
 * @property integer $published
 * @property integer $view_count
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property ContactForm[] $forms
 * @property ContactMarker[] $markers
 * @property ContactPageHowToGetStep[] $howToGetStepBlocks
 */
class ContactPage extends ActiveRecord
{

    const SAVE_ATTRIBUTE_MAP_IMAGE = 'contact_page_map_image';

    public static function tableName()
    {
        return '{{%contact_page}}';
    }

    public static function findByCityWithFormsAndStepsOrFail($cityId)
    {
        $model = self::find()
            ->alias('c')
            ->where(['c.published' => 1])
            ->joinWith(['forms', 'markers', 'howToGetStepBlocks'])
            ->andWhere(['c.city_id' => $cityId])->one();

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getForms()
    {
        return $this->hasMany(ContactForm::class, ['contact_page_id' => 'id'])
            ->alias('cf')
            ->andOnCondition(['cf.published' => self::IS_PUBLISHED])
            ->orderBy(['cf.position' => SORT_ASC]);
    }

    public function getMarkers()
    {
        return $this->hasMany(ContactMarker::class, ['contact_page_id' => 'id'])
            ->alias('cm')
            ->andOnCondition(['cm.published' => self::IS_PUBLISHED])
            ->orderBy(['cm.position' => SORT_ASC]);
    }

    public function getHowToGetStepBlocks()
    {
        return $this->hasMany(ContactPageHowToGetStep::class, ['object_id' => 'id'])
            ->alias('s')
            ->andOnCondition([
                's.type' => ContactPageHowToGetStep::CONTACT_HOW_TO_GET_STEP,
                's.published' => self::IS_PUBLISHED,
            ]);
    }

    public function getMapImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['map_image.entity_model_name' => static::formName(), 'map_image.attribute' => static::SAVE_ATTRIBUTE_MAP_IMAGE])
            ->alias('map_image')
            ->orderBy('map_image.position DESC');
    }
}
