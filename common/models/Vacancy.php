<?php

namespace common\models;

use common\helpers\SiteUrlHelper;
use common\components\model\ActiveRecord;
use common\models\blocks\VacancyFaqItem;
use common\models\blocks\VacancyPrinciple;

/**
 * This is the model class for table "{{%vacancy}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $specialization_id
 * @property string $label
 * @property string $alias
 * @property string $description
 * @property string $detail_description
 * @property string $related_label
 * @property string $responsibilities_label
 * @property string $responsibilities_content
 * @property string $requirements_label
 * @property string $requirements_content
 * @property string $benefits_label
 * @property string $benefits_content
 * @property string $faq_label
 * @property string $principles_label
 * @property integer $manager_id
 * @property string $form_label
 * @property string $form_description
 * @property string $form_agreement
 * @property string $form_emails
 * @property string $form_onsubmit
 * @property integer $view_count
 * @property integer $position
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $responses
 *
 * @property City $city
 * @property Manager $manager
 * @property Specialization $specialization
 * @property SubMenu[] $subMenus
 * @property VacancyPrinciple[] $principleBlocks
 * @property VacancyFaqItem[] $faqBlocks
 * @property Vacancy[] $related
 */
class Vacancy extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%vacancy}}';
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getSpecialization()
    {
        return $this->hasOne(Specialization::class, ['id' => 'specialization_id']);
    }

    public function getPrincipleBlocks()
    {
        return $this->hasMany(VacancyPrinciple::class, ['object_id' => 'id'])
            ->onCondition(['type' => VacancyPrinciple::VACANCY_PRINCIPLE]);
    }

    public function getFaqBlocks()
    {
        return $this->hasMany(VacancyFaqItem::class, ['object_id' => 'id'])
            ->onCondition(['type' => VacancyFaqItem::VACANCY_FAQ_ITEM]);
    }

    public function getRelated()
    {
        return $this->hasMany(Vacancy::class, ['specialization_id' => 'specialization_id'])
            ->andWhere(['!=', 'id' , $this->id])
			->andWhere(['published' => self::IS_PUBLISHED])
            ->andWhere(['city_id' => $this->city_id])->limit(3);
    }

    public function getSubMenus()
    {
        return $this->hasMany(SubMenu::class, ['object_id' => 'id'])
            ->onCondition(['type' => SubMenu::TYPE_VACANCY]);
    }

    public static function findOnePublishedByAliasAndCityOrFail($alias, $cityId)
    {
        return static::checkFail(
            static::find()->where(['alias' => $alias, 'city_id' => $cityId, 'published' => self::IS_PUBLISHED])->one()
        );
    }

    public function viewUrl()
    {
        return SiteUrlHelper::getVacancyViewUrl(['city' => $this->city->alias, 'alias' => $this->alias]);
    }

    public static function getVacancyLayout($provider)
    {
        $pageSize = $provider->pagination->pageSize;

        $total = $provider->totalCount;

        $pages = (int) (($total + $pageSize - 1) / $pageSize);

        if ($pages > 1) {
            return '<div class="vacancy__card-block clearfix">{items}</div>
                <div class="apartaments__bottom apartaments__bottom--vacancy wow fadeIn">
                    <div class="apartaments__cell"><div class="apartaments__line"></div></div>
                    <div class="apartaments__cell">{pager}</div>
                </div>';
        }

        return '<div class="vacancy__card-block clearfix">{items}</div>';

    }

    public function incrementResponse($id)
    {
        $vac = static::find()->where(['id' => $id])->one();
        $vac->responses++;
        if($vac->save(false))
        {
            return true;
        }
        return false;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->view_count = 0;
            $this->responses = 0;

            return parent::beforeSave($insert);
        }
    }

}
