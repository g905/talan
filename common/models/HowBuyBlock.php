<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%how_buy_block}}".
 *
 * @property integer $id
 * @property integer $how_buy_id
 * @property string $label
 * @property string $short_desc
 * @property string $content
 * @property string $link_label
 * @property string $link
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property HowBuy $howBuy
 */
class HowBuyBlock extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%how_buy_block}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHowBuy()
    {
        return $this->hasOne(HowBuy::className(), ['id' => 'how_buy_id']);
    }
}
