<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%home}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $top_screen_title
 * @property string $top_screen_btn_label
 * @property string $top_screen_btn_link
 * @property string $about_company_title
 * @property string $about_company_desc
 * @property string $about_company_link_label
 * @property string $about_company_link
 * @property string $how_we_build_title
 * @property string $apartment_complex_title
 * @property string $apartment_complex_subtitle
 * @property string $apartment_complex_description
 * @property string $apartment_complex_link_label
 * @property string $promo_title
 * @property string $promo_link_label
 * @property string $review_title
 * @property string $final_cta_title
 * @property string $final_cta_description
 * @property string $final_cta_btn_label
 * @property string $final_cta_btn_link
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $apartment_complex_link
 * @property string $top_screen_btn_onclick
 *
 * @property City $city
 * @property EntityToFile $topScreenBackgroundImage
 * @property EntityToFile $topScreenBackgroundVideo
 * @property EntityToFile $aboutCompanyImage
 */
class Home extends ActiveRecord
{
    const SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE = 'HomeTop_screen_background_image';
    const SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO = 'HomeTop_screen_background_video';
    const SAVE_ATTRIBUTE_ABOUT_COMPANY_IMAGE = 'HomeAbout_company_image';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%home}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHowWeBuild()
    {
        return $this->hasMany(HowWeBuild::class, ['home_page_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlternateWidget()
    {
        return $this->hasMany(AlternateWidget::class, ['home_page_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::class, ['home_page_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopScreenBackgroundImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'top_screen_background_image.entity_model_name' => static::formName(),
                'top_screen_background_image.attribute' => static::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE
            ])
            ->alias('top_screen_background_image')
            ->orderBy('top_screen_background_image.position DESC');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopScreenBackgroundVideo()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'top_screen_background_video.entity_model_name' => static::formName(),
                'top_screen_background_video.attribute' => static::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO
            ])
            ->alias('top_screen_background_video')
            ->orderBy('top_screen_background_video.position DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompanyImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'about_company_image.entity_model_name' => static::formName(),
                'about_company_image.attribute' => static::SAVE_ATTRIBUTE_ABOUT_COMPANY_IMAGE
            ])
            ->alias('about_company_image')
            ->orderBy('about_company_image.position DESC');
    }

    /**
     * @param City $city
     * @return array|null|\yii\db\ActiveRecord|self
     */
    public static function findByCity($city)
    {
        return Home::find()->where([
            'city_id' => $city->id,
            'published' => 1
        ])->one();
    }
}
