<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "synchronize".
 *
 * @property integer $id
 * @property integer $add
 * @property integer $replaced
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Synchronize extends \common\components\model\ActiveRecord
{
    const EMPTY = 0;
    const SUCCSESS = 1;

    public static function tableName()
    {
        return 'synchronize';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'add' => 'Add',
            'replaced' => 'Replaced',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    public static function getLastUpdate()
    {
        $log = self::find()->where(['status' => self::SUCCSESS])->orderBy(['created_at' => SORT_DESC])->one();

        if ($log) {
            return $log->created_at;
        }

        return 0;
    }
}
