<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%request_commercial}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $ac_id
 * @property string $name
 * @property string $phone
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property ApartmentComplex $apartmentComplex
 */
class RequestCommercial extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%request_commercial}}';
    }

    public function behaviors()
    {
        return ['timestamp' => TimestampBehavior::class];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'ac_id']);
    }
}
