<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%request_instalment_plan}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $name
 * @property string $phone
 * @property integer $created_at
 * @property integer $updated_at
 * @property City $city
 */
class RequestInstallmentPlan extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%request_instalment_plan}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'request-instalment-plan'),
            'city_id' => bt('Associated city', 'request-instalment-plan'),
            'name' => bt('User name', 'request-instalment-plan'),
            'phone' => bt('User phone', 'request-instalment-plan'),
            'created_at' => bt('Created at', 'request-instalment-plan'),
            'updated_at' => bt('Updated at', 'request-instalment-plan'),
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
