<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%public_data_chart_data}}".
 *
 * @property integer $id
 * @property integer $widget_id
 * @property string $label
 * @property string $value
 * @property string $color
 * @property integer $position
 *
 * @property PublicDataWidget $widget
 */
class PublicDataChartData extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%public_data_chart_data}}';
    }

    public function getWidget()
    {
        return $this->hasOne(PublicDataWidget::class, ['id' => 'widget_id']);
    }
}
