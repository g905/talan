<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%ftp_log}}".
 *
 * @property integer $id
 * @property integer $type
 * @property string $message
 * @property integer $is_show
 * @property integer $is_shown
 * @property integer $created_at
 * @property integer $updated_at
 */
class FtpLog extends ActiveRecord
{

    const MESSAGE_TYPE_SUCCESS = 1;
    const MESSAGE_TYPE_ERROR = 2;

    public static function getTypeDictionary()
    {
        return [
            static::MESSAGE_TYPE_SUCCESS => 'success',
            static::MESSAGE_TYPE_ERROR => 'error',
        ];

    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ftp_log}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }
}
