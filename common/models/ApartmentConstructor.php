<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "apartment_constructor".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property integer $price
 * @property integer $type
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 */
class ApartmentConstructor extends \common\components\model\ActiveRecord
{
    const CONSTRUCTOR_WALLS = 1;
    const CONSTRUCTOR_FLOORS = 2;
    const CONSTRUCTOR_DOORS = 3;
    const CONSTRUCTOR_OPTIONS = 4;
    const CONSTRUCTOR_BATHROOM = 5;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'apartment_constructor';
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'description' => 'Description',
            'type' => 'Type',
            'published' => 'Published',
            'position' => 'Position',
            'city_id' => 'City'
        ];
    }

    /**
     * @return array
     */
    public static function getTypeDictionary()
    {
        return [
            self::CONSTRUCTOR_WALLS => Yii::t('front/constructor', 'Стены'),
            self::CONSTRUCTOR_FLOORS => Yii::t('front/constructor', 'Полы'),
            self::CONSTRUCTOR_DOORS => Yii::t('front/constructor', 'Двери'),
            self::CONSTRUCTOR_OPTIONS => Yii::t('front/constructor', 'Оборудование'),
            self::CONSTRUCTOR_BATHROOM => Yii::t('front/constructor', 'Стены'),
        ];
    }

    /**
     * @param $type
     * @return array
     */
    public static function getDataByType($type)
    {
        $returnData = [];

        $city = City::getUserCity();

        $count = self::find()
            ->where(['type' => $type, 'city_id' => $city->id])
            ->orderBy(['position' => SORT_ASC])
            ->count();
        if(!$count)
        {
            $data = self::find()
                ->where(['type' => $type, 'city_id' => 1])
                ->orderBy(['position' => SORT_ASC])
                ->all();
        } else {
            $data = self::find()
                ->where(['type' => $type, 'city_id' => $city->id])
                ->orderBy(['position' => SORT_ASC])
                ->all();
        }

        foreach ($data as $key => $item) {
            $returnData[] = [
                'name' => $item->label,
                'src' => $item->src,
                'about' => $item->description,
                'preview' => $item->preview,
                'type' => self::getTypeDictionary()[$item->type],
            ];
        }

        return $returnData;
    }

    /**
     * @return bool
     */
    public static function validateRender()
    {
        $city = City::getUserCity();

        $constructorPrice = ApartmentConstructorPrice::find()
            ->where(['city_id' => $city->id])
            ->isPublished()
            ->one();

        if ($constructorPrice) {
            return true;
        }

        return false;
    }

    public static function getData($request = false)
    {
        $data = [];

        if ($request) {
            foreach ($request as $key => $item) {
                if (preg_match("/^(\d+)?\item\d+$/", $key)) {
                    $data[] = $item;
                }
            }

            return implode('; ', $data);
        }
    }

}
