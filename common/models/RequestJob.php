<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use metalguardian\fileProcessor\models\File;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%request_job}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $file_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $created_at
 * @property integer $updated_at
 * @property City $city
 * @property File $file
 */
class RequestJob extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%request_job}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'request-job'),
            'city_id' => bt('Associated city', 'request-job'),
            'file_id' => bt('File', 'request-job'),
            'type' => bt('Request source type', 'request-job'),
            'name' => bt('User name', 'request-job'),
            'phone' => bt('User phone', 'request-job'),
            'email' => bt('User email', 'request-job'),
            'created_at' => bt('Created at', 'request-job'),
            'updated_at' => bt('Updated at', 'request-job'),
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getFile()
    {
        return $this->hasOne(File::class, ['id' => 'file_id']);
    }
}
