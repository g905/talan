<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%about_company_values}}".
 *
 * @property integer $id
 * @property integer $about_company_id
 * @property string $label
 * @property string $content
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AboutCompany $aboutCompany
 * @property EntityToFile $image
 */
class AboutCompanyValues extends ActiveRecord 
{
    const SAVE_ATTRIBUTE_IMAGE = 'AboutCompanyValuesImage';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%about_company_values}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompany()
    {
        return $this->hasOne(AboutCompany::className(), ['id' => 'about_company_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['image.entity_model_name' => static::formName(), 'image.attribute' => static::SAVE_ATTRIBUTE_IMAGE])
            ->alias('image')
            ->orderBy('image.position DESC');
    }
}
