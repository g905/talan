<?php

namespace common\models;

use backend\modules\block\models\block\EarthCompany;
use common\models\blocks\EarthAdvantage;
use Yii;
use common\components\model\ActiveRecord;
use common\models\blocks\EarthCompany as CommonEarthCompany;
use common\models\blocks\EarthCompany as CommonEarthPlan;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%earth}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $top_screen_title
 * @property string $top_screen_description
 * @property string $presentation_link
 * @property string $company_title
 * @property string $company_description
 * @property string $map_title
 * @property string $map_subtitle
 * @property string $map_demands
 * @property double $map_lat
 * @property double $map_lng
 * @property integer $map_zoom
 * @property string $plan_title
 * @property string $boss_title
 * @property string $boss_name
 * @property string $boss_position
 * @property string $boss_description
 * @property string $boss_video
 * @property string $offer_title_light
 * @property string $offer_title_dark
 * @property string $offer_link
 * @property string $advantages_title
 * @property string $model_title
 * @property string $model_description
 * @property string $manager_title
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property EarthMarker[] $earthMarkers
 * @property EntityToFile $topScreenBackgroundImage
 * @property EntityToFile $topScreenBackgroundVideo
 * @property EntityToFile $modelPhoto
 * @property EntityToFile $bossPhoto
 * @property SubMenu[] $subMenus
 * @property CommonEarthCompany $companyBlocks
 * @property CommonEarthPlan $planBlocks
 * @property EarthAdvantage $advantages
 * @property Manager[] $managers
 */
class Earth extends ActiveRecord
{
    const SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE = 'EarthTop_screen_background_image';
    const SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO = 'EarthTop_screen_background_video';
    const SAVE_ATTRIBUTE_MODEL_IMAGE = 'EarthModel_image';
    const SAVE_ATTRIBUTE_BOSS_IMAGE = 'EarthBoss_image';
    const SAVE_ATTRIBUTE_MAP_IMAGE = 'Earth_page_map_image';

    public static function tableName()
    {
        return '{{%earth}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City'),
            'top_screen_title' => Yii::t('app', 'Title'),
            'top_screen_description' => Yii::t('app', 'Description'),
            'top_screen_background_image' => Yii::t('app', 'Top screen background image'),
            'top_screen_background_video' => Yii::t('app', 'Top screen background video'),
            'presentation_link' => Yii::t('app', 'Link'),
            'company_title' => Yii::t('app', 'Title'),
            'company_description' => Yii::t('app', 'Description'),
            'map_title' => Yii::t('app', 'Title'),
            'map_subtitle' => Yii::t('app', 'Subtitle'),
            'map_demands' => Yii::t('app', 'Demands'),
            'map_lat' => Yii::t('app', 'Lat'),
            'map_lng' => Yii::t('app', 'Lng'),
            'map_zoom' => Yii::t('app', 'Zoom'),
            'plan_title' => Yii::t('app', 'Title'),
            'boss_title' => Yii::t('app', 'Title'),
            'boss_name' => Yii::t('app', 'Name'),
            'boss_position' => Yii::t('app', 'Position'),
            'boss_description' => Yii::t('app', 'Description'),
            'boss_video' => Yii::t('app', 'Video link'),
            'boss_image' => Yii::t('app', 'Video preview'),
            'offer_title_light' => Yii::t('app', 'Title light'),
            'offer_title_dark' => Yii::t('app', 'Title dark'),
            'offer_link' => Yii::t('app', 'Link'),
            'advantages_title' => Yii::t('app', 'Title'),
            'model_title' => Yii::t('app', 'Title'),
            'model_description' => Yii::t('app', 'Description'),
            'manager_title' => Yii::t('app', 'Title'),
            'published' => Yii::t('app', 'Published'),
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getEarthMarkers()
    {
        return $this->hasMany(EarthMarker::class, ['earth_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['marker_type' => SORT_ASC]);
    }

    public function getMarkerTypes()
    {
        $markersTypes = [];
        /** @var EarthMarker $marker */
        foreach ($this->earthMarkers as $marker) {
            if (!in_array($marker->marker_type, $markersTypes)) {
                $markersTypes[] = $marker->marker_type;
            }
        }

        return $markersTypes;
    }

    public function getTopScreenBackgroundImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'top_screen_background_image.entity_model_name' => static::formName(),
                'top_screen_background_image.attribute' => static::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE
            ])
            ->alias('top_screen_background_image')
            ->orderBy('top_screen_background_image.position DESC');
    }

    public function getTopScreenBackgroundVideo()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'top_screen_background_video.entity_model_name' => static::formName(),
                'top_screen_background_video.attribute' => static::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO
            ])
            ->alias('top_screen_background_video')
            ->orderBy('top_screen_background_video.position DESC');
    }

    public function getModelPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'model_image.entity_model_name' => static::formName(),
                'model_image.attribute' => static::SAVE_ATTRIBUTE_MODEL_IMAGE
            ])
            ->alias('model_image')
            ->orderBy('model_image.position DESC');
    }

    public function getBossPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'boss_image.entity_model_name' => static::formName(),
                'boss_image.attribute' => static::SAVE_ATTRIBUTE_BOSS_IMAGE
            ])
            ->alias('boss_image')
            ->orderBy('boss_image.position DESC');
    }

    public function getSubMenus()
    {
        return $this->hasMany(SubMenu::class, ['object_id' => 'id'])
            ->onCondition(['type' => SubMenu::TYPE_EARTH]);
    }

    public function getCompanyBlocks()
    {
        return $this->hasMany(CommonEarthCompany::class, ['object_id' => 'id'])
            ->onCondition([
                'type' => CommonEarthCompany::EARTH_COMPANY,
                'published' => 1
            ]);
    }

    public function getPlanBlocks()
    {
        return $this->hasMany(CommonEarthPlan::class, ['object_id' => 'id'])
            ->onCondition([
                'type' => CommonEarthPlan::EARTH_PLAN,
                'published' => 1
            ]);
    }

    public function getAdvantages()
    {
        return $this->hasMany(EarthAdvantage::class, ['object_id' => 'id'])
            ->onCondition([
                'type' => EarthAdvantage::EARTH_ADVANTAGE,
                'published' => 1
            ]);
    }

    public function getManagers()
    {
        return $this->hasMany(Manager::class, ['id' => 'manager_id'])
            ->alias('m')
            ->where(['m.published' => self::IS_PUBLISHED])
            ->viaTable(ManagerAssign::tableName(), ['object_id' => 'id'], function (ActiveQuery $q) {
                $q->alias('ma')->andWhere(['ma.type' => ManagerAssign::TYPE_EARTH_MANAGER])->limit(3);
            })
            ->innerJoin(ManagerAssign::tableName() . ' ma', 'ma.manager_id = m.id')
            ->orderBy(['ma.position' => SORT_ASC]);
    }

    /**
     * @param City $city
     * @return array|null|\yii\db\ActiveRecord|self
     */
    public static function findByCity($city)
    {
        return Earth::find()->where([
            'city_id' => $city->id,
            'published' => 1
        ])->one();
    }

    public function getPositionFormatted($position)
    {
        if (strlen($position+1) == 1) {
            return '0'.($position+1);
        }

        return $position+1;
    }

    public function getMapImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['map_image.entity_model_name' => static::formName(), 'map_image.attribute' => static::SAVE_ATTRIBUTE_MAP_IMAGE])
            ->alias('map_image')
            ->orderBy('map_image.position DESC');
    }
}
