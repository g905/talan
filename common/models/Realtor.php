<?php

namespace common\models;

use common\models\blocks\RealtorFile;
use common\models\blocks\RealtorFact;
use common\models\blocks\RealtorPartner;
use common\models\blocks\RealtorBenefit;
use common\components\model\ActiveRecord;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "{{%realtor}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $top_label
 * @property string $top_description
 * @property string $top_link_label
 * @property string $top_btn_label
 * @property string $benefits_label
 * @property string $benefits_description
 * @property string $about_label
 * @property string $about_description
 * @property string $partners_label
 * @property string $app_label
 * @property string $app_description
 * @property string $app_link_label
 * @property string $app_btn_ios_label
 * @property string $app_btn_ios_link
 * @property string $app_btn_android_label
 * @property string $app_btn_android_link
 * @property string $form_label
 * @property string $form_onsubmit
 * @property string $form_description
 * @property string $form_agreement
 * @property integer $manager_id
 * @property string $files_label
 * @property integer $view_count
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property Manager $manager
 * @property SubMenu[] $subMenus
 * @property RealtorFile[] $fileBlocks
 * @property RealtorFact[] $factBlocks
 * @property RealtorBenefit[] $benefitBlocks
 * @property RealtorPartner[] $partnerBlocks
 */
class Realtor extends ActiveRecord
{
    const TOP_IMAGE = 'realtorTopImage';
    const TOP_LINK_FILE = 'realtorTopLinkFile';
    const APP_LINK_FILE = 'realtorAppLinkFile';

    const APP_FIRST_IMAGE = 'realtorAppImageFirst';
    const APP_SECOND_IMAGE = 'realtorAppImageSecond';

    public static function tableName()
    {
        return '{{%realtor}}';
    }

    public static function findByCityOrFail($cityId)
    {
        $model = self::find()
            ->alias('r')
            ->where(['r.city_id' => $cityId, 'r.published' => self::IS_PUBLISHED])
            ->with(['subMenus', 'fileBlocks', 'factBlocks', 'benefitBlocks', 'partnerBlocks'])
            ->one();

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }

    public function topImageSrc()
    {
        return $this->getEntityThumb('topImage', 'realtor', 'top');
    }

    public function topLinkSrc()
    {
        return $this->getEntitySource('topLinkFile');
    }

    public function appLinkSrc()
    {
        return $this->getEntitySource('appLinkFile');
    }

    public function appImageFirstSrc()
    {
        return $this->getEntityThumb('appImageFirst', 'realtor', 'app1');
    }

    public function appImageSecondSrc()
    {
        return $this->getEntityThumb('appImageSecond', 'realtor', 'app2');
    }

    public function getTopImage()
    {
        return $this->getEntityModel(self::TOP_IMAGE, 'realtorTopImage');
    }

    public function getTopLinkFile()
    {
        return $this->getEntityModel(self::TOP_LINK_FILE, 'realtorTopFile');
    }

    public function getAppLinkFile()
    {
        return $this->getEntityModel(self::APP_LINK_FILE, 'realtorAppFile');
    }

    public function getAppImageFirst()
    {
        return $this->getEntityModel(self::APP_FIRST_IMAGE, 'realtorAppImageFirst');
    }

    public function getAppImageSecond()
    {
        return $this->getEntityModel(self::APP_SECOND_IMAGE, 'realtorAppImageSecond');
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getBenefitBlocks()
    {
        return $this->hasMany(RealtorBenefit::class, ['object_id' => 'id'])
            ->onCondition(['type' => RealtorBenefit::REALTOR_BENEFIT]);
    }

    public function getPartnerBlocks()
    {
        return $this->hasMany(RealtorPartner::class, ['object_id' => 'id'])
            ->onCondition(['type' => RealtorPartner::REALTOR_PARTNER]);
    }

    public function getFileBlocks()
    {
        return $this->hasMany(RealtorFile::class, ['object_id' => 'id'])
            ->onCondition(['type' => RealtorFile::REALTOR_FILE]);
    }

    public function getFactBlocks()
    {
        return $this->hasMany(RealtorFact::class, ['object_id' => 'id'])
            ->onCondition(['type' => RealtorFact::REALTOR_FACT]);
    }

    public function getSubMenus()
    {
        return $this->hasMany(SubMenu::class, ['object_id' => 'id'])
            ->onCondition(['type' => SubMenu::TYPE_REALTOR]);
    }
}
