<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%completed_project_assign}}".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $object_id
 * @property integer $type
 * @property integer $position
 *
 * @property CompletedProject $project
 */
class CompletedProjectAssign extends ActiveRecord
{
    const TYPE_BUILDING_PROJECTS = 1;

    public static function tableName()
    {
        return '{{%completed_project_assign}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'project_id' => Yii::t('app', 'Project ID'),
            'object_id' => Yii::t('app', 'Object ID'),
            'type' => Yii::t('app', 'Object type identifier'),
            'position' => Yii::t('app', 'Position'),
        ];
    }

    public function getProject()
    {
        return $this->hasOne(CompletedProject::class, ['id' => 'project_id']);
    }
}
