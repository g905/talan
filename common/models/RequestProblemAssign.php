<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%request_problem_assign}}".
 *
 * @property integer $id
 * @property integer $problem_id
 * @property integer $object_id
 * @property integer $type
 * @property integer $position
 *
 * @property RequestProblem $problem
 */
class RequestProblemAssign extends ActiveRecord
{
    const TYPE_BUILDING_PROBLEMS = 1;

    public static function tableName()
    {
        return '{{%request_problem_assign}}';
    }

    public function attributeLabels()
    {
        return [
            'problem_id' => bt('Problem ID', 'app'),
            'object_id' => bt('Object ID', 'app'),
            'type' => bt('Object type identifier', 'app'),
            'position' => bt('Position', 'app'),
        ];
    }

    public function getProblem()
    {
        return $this->hasOne(RequestProblem::class, ['id' => 'problem_id']);
    }
}
