<?php

namespace common\models;

use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\behaviors\TimestampBehavior;
use common\helpers\Property;
use common\helpers\ConfigHelper;
use common\components\model\ActiveRecord;
use frontend\widgets\FilterWidget;
use backend\modules\block\models\block\ApartmentComplexSlideVideo;

/**
 * This is the model class for table "{{%apartment_complex}}".
 *
 * @property integer $id
 * @property integer $type
 * @property string $label
 * @property string $top_screen_btn_label
 * @property string $top_screen_btn_link
 * @property string $general_progress_title
 * @property string $general_progress_percent
 * @property string $general_description
 * @property string $general_docs_link
 * @property string $general_docs_link_title
 * @property string $general_fb_link
 * @property string $general_vk_link
 * @property string $general_ig_link
 * @property string $form_top_title
 * @property string $form_top_description
 * @property string $form_top_emails
 * @property string $advantages_title
 * @property string $map_title
 * @property string $map_subtitle
 * @property string $map_description
 * @property string $public_service_title
 * @property string $architecture_title
 * @property string $architecture_fio
 * @property string $architecture_job_position
 * @property string $architecture_description
 * @property string $form_bottom_title
 * @property string $form_bottom_description
 * @property string $form_bottom_email
 * @property integer $manager_id
 * @property string $filter_price_deviation
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $city_id
 * @property integer $view_count
 * @property double $lat
 * @property double $lng
 * @property string $alias
 * @property boolean $show_on_home
 * @property integer $position_on_home
 * @property string $common_script
 * @property string $guid
 * @property integer $map_zoom
 * @property string $choose_apartment_onclick
 * @property string $form_top_onsubmit
 * @property string $form_bottom_onsubmit
 * @property string $custom_popup_id
 * @property string $build_start_date
 * @property string $build_end_date
 * @property string $build_video
 * @property string $build_history_end_text
 * @property string $build_history_current_text
 * @property string $build_history_title
 * @property string $btn_label
 * @property string $btn_link
 * @property boolean $show_label
 * @property boolean $show_prices
 * @property string $cta_black_label
 * @property string $cta_white_label
 * @property string $cta_description
 * @property string $cta_button_label
 * @property string $cta_button_link
 * @property int $cta_published
 * @property string $submit_apartment_script
 * @property boolean $is_secondary
 * @property string $map_image
 *
 * @property Manager $manager
 * @property City $city
 * @property AcAdvantages $advantages
 * @property AcMarker $markers
 * @property AcPublicService $publicServices
 * @property PromoAction $promoActions
 * @property CustomPopup $customPopup
 * @property ApartmentBuilding[] $buildings
 *
 * @property EntityToFile $topScreenBackgroundImage
 * @property EntityToFile $topScreenBackgroundVideo
 * @property EntityToFile[] $generalPhotos
 * @property EntityToFile $architecturePhoto
 * @property EntityToFile $architectureBottomPhoto
 * @property EntityToFile $mapImage
 * @property AcMenu $apartmentComplexMenu
 */
class ApartmentComplex extends ActiveRecord
{
    const SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE = 'ApartmentComplexTop_screen_background_image';
    const SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO = 'ApartmentComplexTop_screen_background_video';
    const SAVE_ATTRIBUTE_GENERAL_PHOTOS = 'ApartmentComplexGeneral_photos';
    const SAVE_ATTRIBUTE_ARCHITECTURE_PHOTO = 'ApartmentComplexArchitecture_photo';
    const SAVE_ATTRIBUTE_ARCHITECTURE_BOTTOM_PHOTO = 'ApartmentComplexArchitecture_bottom_photo';
    const SAVE_ATTRIBUTE_WIDGET_CALL_IMAGE = 'ApartmentComplexWidget_call_image';
    const SAVE_ATTRIBUTE_MAP_IMAGE = 'ApartmentComplexMap_image';
	const SAVE_ATTRIBUTE_VIEW_DOCUMENTATION = 'ApartmentComplex_view_documentation';

    public static function tableName()
    {
        return '{{%apartment_complex}}';
    }

    public function behaviors()
    {
        return ['timestamp' => TimestampBehavior::class];
    }

    public static function getComplexesByTypeAndCity($type, $city_id, $secondary = false)
    {
        return self::find()
            ->isPublished()
            ->andWhere(['type' => $type])
            ->andWhere(['city_id' => $city_id])
            ->andWhere(['is_secondary' => $secondary])
            ->orderBy(['position' => SORT_ASC])
            ->all();
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getTopScreenBackgroundImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'top_screen_background_image.entity_model_name' => static::formName(),
                'top_screen_background_image.attribute' => static::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE
            ])
            ->alias('top_screen_background_image')
            ->orderBy('top_screen_background_image.position DESC');
    }

    public function getTopScreenBackgroundVideo()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'top_screen_background_video.entity_model_name' => static::formName(),
                'top_screen_background_video.attribute' => static::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO
            ])
            ->alias('top_screen_background_video')
            ->orderBy('top_screen_background_video.position DESC');
    }

    public function getMapImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['map_image.entity_model_name' => static::formName(), 'map_image.attribute' => static::SAVE_ATTRIBUTE_MAP_IMAGE])
            ->alias('map_image')
            ->orderBy('map_image.position DESC');
    }

    public function getGeneralPhotos()
    {
        return $this->hasMany(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'general_photos.entity_model_name' => static::formName(),
                'general_photos.attribute' => static::SAVE_ATTRIBUTE_GENERAL_PHOTOS
            ])
            ->alias('general_photos')
            ->orderBy('general_photos.position DESC');
    }

    public function getSliderVideos()
    {
        return $this->hasMany(ApartmentComplexSlideVideo::class, ['object_id' => 'id'])
            ->andOnCondition(['type' => ApartmentComplexSlideVideo::APARTMENT_COMPLEX_SLIDER_VIDEO])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getGeneralPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'general_photos.entity_model_name' => static::formName(),
                'general_photos.attribute' => static::SAVE_ATTRIBUTE_GENERAL_PHOTOS
            ])
            ->alias('general_photos')
            ->orderBy('general_photos.position DESC');
    }

	public function getViewDocumentation() {
		return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
			->andOnCondition([
				'general_photos.entity_model_name' => 'ApartmentComplex',
				'general_photos.attribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_VIEW_DOCUMENTATION,
			])
			->alias('general_photos');
	}

    public function getArchitecturePhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'architecture_photo.entity_model_name' => static::formName(),
                'architecture_photo.attribute' => static::SAVE_ATTRIBUTE_ARCHITECTURE_PHOTO
            ])
            ->alias('architecture_photo')
            ->orderBy('architecture_photo.position DESC');
    }

    public function getWidgetCallIcon()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'entity_model_name' => static::formName(),
                'attribute' => static::SAVE_ATTRIBUTE_WIDGET_CALL_IMAGE
            ]);
    }

    public function getArchitectureBottomPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'architecture_bottom_photo.entity_model_name' => static::formName(),
                'architecture_bottom_photo.attribute' => static::SAVE_ATTRIBUTE_ARCHITECTURE_BOTTOM_PHOTO
            ])
            ->alias('architecture_bottom_photo')
            ->orderBy('architecture_bottom_photo.position DESC');
    }

    public function getCustomPopup()
    {
        return $this->hasOne(CustomPopup::class, ['id' => 'custom_popup_id']);
    }

    public function getApartmentComplexMenu()
    {
        return $this->hasMany(AcMenu::class, ['apartment_complex_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getAdvantages()
    {
        return $this->hasMany(AcAdvantages::class, ['apartment_complex_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getMarkers()
    {
        return $this->hasMany(AcMarker::class, ['apartment_complex_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getPublicServices()
    {
        return $this->hasMany(AcPublicService::class, ['apartment_complex_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getPromoActions()
    {
        return $this->hasMany(PromoAction::class, ['id' => 'action_id'])
            ->viaTable(PromoActionToAc::tableName(), ['ac_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->andWhere(['<', 'promo_action.start_date', time()])
            ->andWhere(['>', 'promo_action.end_date', time()])
            //->orderBy(['position_on_home' => SORT_DESC]);
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getApartments()
    {
        return $this->hasMany(Apartment::class, ['apartment_complex_id' => 'id'])
            ->alias('a')
            ->andOnCondition(['a.published' => 1])
            ->orderBy(['a.position' => SORT_ASC]);
    }

    public function getBuildings()
    {
        return $this->hasMany(ApartmentBuilding::class, ['building_complex_id' => 'id'])
            ->alias('b')
            ->andOnCondition(['b.published' => 1])
            ->orderBy(['b.position' => SORT_ASC]);
    }

    public function getApartmentComplexWidget()
    {
        return $this->hasMany(ApartmentComplexWidget::class, ['complex_id' => 'id'])
            ->andOnCondition(['published' => 1])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getBuildingsForChess()
    {
        return $this->hasMany(ApartmentBuilding::class, ['building_complex_id' => 'id'])
            ->alias('b')
            ->andOnCondition(['b.show_chess' => 1])
            ->orderBy(['b.position' => SORT_ASC]);
    }

    public function getBuildHistoryApartmentComplexItems()
    {
        return $this->hasMany(BuildHistoryApartmentComplexItem::class, ['apartment_complex_id' => 'id'])
            ->alias('b')
            ->orderBy(['b.date' => SORT_DESC]);
    }

    public function getMarkerTypes()
    {
        $markersTypes = [];
        /** @var AcMarker $marker */
        foreach ($this->markers as $marker) {
            if (!in_array($marker->ac_marker_type_id, $markersTypes)) {
                $markersTypes[] = $marker->ac_marker_type_id;
            }
        }

        return $markersTypes;
    }

    /**
     * Find one published record by its alias and type or throw not found exception.
     *
     * @param int $type property type.
     * @param string $alias record alias.
     * @return null|\yii\db\ActiveRecord|static
     * @throws \yii\web\NotFoundHttpException
     */
    public static function findOnePublishedByAliasAndTypeOrFail($alias, int $type)
    {
        return static::checkFail(
            static::find()->where(['alias' => $alias, 'type' => $type, 'published' => self::IS_PUBLISHED])->one()
        );
    }

    /**
     * Return model view URL.
     *
     * @return string
     */
    public function getViewUrl()
    {
        return Url::isRelative($this->alias)
            ? static::createUrl('/apartment/apartment-complex/view', ['alias' => $this->alias, 'type' => $this->type])
            : $this->alias;
    }

    public function getPreviewPhotoSrc()
    {
        return $this->getEntityThumb('topScreenBackgroundImage', 'complex', 'complexList');
    }

    public function isResidential()
    {
        return $this->type == Property::TYPE_RESIDENTIAL;
    }

    public function isCommercial()
    {
        return $this->type == Property::TYPE_COMMERCIAL;
    }

    public function getMap()
    {
        return $this->hasOne(ApartmentMap::class, ['complex_id' => 'id']);
    }

    /**
     * Filter apartments by given params.
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function filter($params)
    {
    	$queue = obtain('queue', $params);
    	if (!$queue) $queue = 0;
    	//dd($queue);
        $complexes = obtain('complex', $params, [$this->id]);
        $squareMin = obtain('square-min', $params);
        $squareMax = obtain('square-max', $params);
        $priceMin = obtain('price-min', $params);
        $priceMax = obtain('price-max', $params);
        //$floor = obtain('floor', $params);
        $floorMin = obtain('floor-min', $params);
        $floorMax = obtain('floor-max', $params);
        $rooms = obtain('rooms', $params);
        $for = obtain('for', $params);
        $queue = obtain('queue', $params);

        $query = Apartment::find()->alias('a')->where([
            'a.published' => Apartment::IS_PUBLISHED,
            'a.type' => $this->type,
            'a.apartment_complex_id' => $complexes,
        ]);

        // Filter by price asc by default
        //$query->orderBy('price_from asc');

        //dd($this->show_prices);
        $this->show_prices ? $query->orderBy('price_from asc') : $query->orderBy('total_area asc');

        // Filter by square
        $squareMin === null ?: $query->andFilterWhere(['>=', 'total_area', $squareMin]);
        $squareMax === null ?: $query->andFilterWhere(['<=', 'total_area', $squareMax]);
        // Filter by price
        $priceMin === null ?: $query->andFilterWhere(['>=', 'price_from', $priceMin]);
        $priceMax === null ?: $query->andFilterWhere(['<=', 'price_from', $priceMax]);
        // Filter by floor number
        //$floor === null || $floor == -1 ?: $query->andFilterWhere(['floor' => $floor]);
        $floorMin === null ?: $query->andFilterWhere(['>=', 'floor', $floorMin]);
        $floorMax === null ?: $query->andFilterWhere(['<=', 'floor', $floorMax]);
        // Filter by rooms count
        $rooms === null ?: $query->andFilterWhere(['rooms_number' => $rooms]);
        $queue === [1,2] ?: $query->andFilterWhere(['queue' => $queue]);
        $query->andFilterWhere(['IN', 'queue', $queue ]);
        // Filter by 'for whom' param
        if ($for) {
            $forMap = [
                FilterWidget::FILTER_YOUNG_FAMILY => ['<', 'a.rooms_number', 3],
                FilterWidget::FILTER_PENSIONER => ['<', 'a.rooms_number', 2],
                FilterWidget::FILTER_SINGLES => ['<', 'a.rooms_number', 2],
                FilterWidget::FILTER_BIG_FAMILY => ['>=', 'a.rooms_number', 3],
            ];
            $condition = obtain($for, $forMap);
            !is_array($condition) ? : $query->andFilterWhere($condition);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'position' => SORT_ASC,
                ],
            ],
            'pagination' => [
                'pageSize' => ConfigHelper::getApartmentsPerPage(),
                'defaultPageSize' => ConfigHelper::getApartmentsPerPage(),
            ],
        ]);
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public static function getStreamUrl($params = [])
    {
        return static::createUrl('/apartment/apartment-complex/stream', $params);
    }

    public static function getComplexById($id)
    {
        return self::find()->where(['id' => $id])->one();
    }

    public static function getComplexByAlias()
    {
        $alias = \Yii::$app->request->get('alias');
        if(isset($alias) && ($alias !== ""))
        {
            return self::find()->where(['alias' => $alias])->one();
        }
        return null;
    }

    public static function getHeadScript()
    {
        $complex = self::getComplexByAlias();
        if($complex)
        {
            return $complex->head_script;
        }
        return null;

    }

	public static function getCityComplexArr() {
		$cityComplexArr = self::find()
			->select([
				'ac.id',
				'complex_name' => 'ac.label',
				'city_name' => 'c.label',
			])
			->from(['ac' => self::tableName()])
			->where([
				'ac.published' => 1,
			])
			->leftJoin(['c' => City::tableName()], 'c.id = ac.city_id')
			->orderBy('c.label ASC')
			->asArray()
			->all();

		return $cityComplexArr;
	}
}
