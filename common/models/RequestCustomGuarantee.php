<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%request_custom_guarantee}}".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $city
 * @property string $address
 * @property string $comment
 * @property integer $file_id
 *
 * @property City $file
 * @property EntityToFile $image
 */
class RequestCustomGuarantee extends ActiveRecord 
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%request_custom_guarantee}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }
}
