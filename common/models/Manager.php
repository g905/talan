<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%manager}}".
 *
 * @property integer $id
 * @property string $fio
 * @property string $job_position
 * @property string $phone
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $notification_emails
 * @property string $contact_email
 * @property EntityToFile $photo
 */
class Manager extends ActiveRecord 
{
    const SAVE_ATTRIBUTE_PHOTO = 'ManagerPhoto';

    public static function tableName()
    {
        return '{{%manager}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['photo.entity_model_name' => static::formName(), 'photo.attribute' => static::SAVE_ATTRIBUTE_PHOTO])
            ->alias('photo')
            ->orderBy('photo.position DESC');
    }

    public function getNotificationEmailsList()
    {
        $list = [];
        if (!empty($this->notification_emails)) {
            $list = explode("\n", $this->notification_emails);
        }

        return $list;
    }
}
