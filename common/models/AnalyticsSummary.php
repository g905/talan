<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%analytics_summary}}".
 *
 * @property integer $id
 * @property string $date
 * @property integer $visit_count
 * @property integer $unique_visit_count
 * @property integer $apartment_complex_view_count
 * @property integer $apartment_view_count
 */
class AnalyticsSummary extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%analytics_summary}}';
    }
}
