<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%build_history_apartment_building_item}}".
 *
 * @property integer $id
 * @property integer $apartment_building_id
 * @property string $date
 * @property string $percent
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ApartmentBuilding $apartmentBuilding
 */
class BuildHistoryApartmentBuildingItem extends \common\components\model\ActiveRecord
{
    public static function tableName()
    {
        return '{{%build_history_apartment_building_item}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'apartment_building_id' => 'Apartment building',
            'date' => 'Date (month + year)',
            'percent' => 'Percent of completition',
        ];
    }

    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentBuilding::class, ['id' => 'apartment_building_id']);
    }

    public function getSlides()
    {
        return $this->hasMany(BuildHistoryApartmentBuildingItemSlide::class, ['build_history_apartment_building_item_id' => 'id']);
    }
}
