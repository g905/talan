<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%secondary_real_estate}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $description
 * @property string $description_only_desktop
 * @property string $button_text
 * @property string $conditions_label
 * @property string $opportunity_black_label
 * @property string $opportunity_white_label
 * @property string $opportunity_description
 * @property string $opportunity_button_text
 * @property string $apartments_label
 * @property string $steps_label
 * @property string $steps_description
 * @property string $steps_link_text
 * @property string $steps_link_url
 * @property string $articles_label
 * @property integer $city_id
 * @property EntityToFile $bgImage
 * @property EntityToFile $bgVideo
 * @property City $city
 * @property SecondaryRealEstateCondition[] $conditions
 * @property SecondaryRealEstateStep[] $steps
 * @property Article[] $articles
 * @property SecondaryRealEstateMenu[] $menus
 */
class SecondaryRealEstate extends ActiveRecord 
{
    const SAVE_ATTRIBUTE_BG_IMAGE = 'SecondaryRealEstateBgImage';
    const SAVE_ATTRIBUTE_BG_VIDEO = 'SecondaryRealEstateBgVideo';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%secondary_real_estate}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConditions()
    {
        return $this->hasMany(SecondaryRealEstateCondition::class, ['secondary_real_estate_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSteps()
    {
        return $this->hasMany(SecondaryRealEstateStep::class, ['secondary_real_estate_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::class, ['id' => 'article_id'])
            ->viaTable(ArticleInSecondaryRealEstate::tableName(), ['secondary_real_estate_id' => 'id'])
            ->limit(3);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(SecondaryRealEstateMenu::class, ['secondary_real_estate_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getBgImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['bgImage.entity_model_name' => static::formName(), 'bgImage.attribute' => static::SAVE_ATTRIBUTE_BG_IMAGE])
            ->alias('bgImage')
            ->orderBy('bgImage.position DESC');
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getBgVideo()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['bgVideo.entity_model_name' => static::formName(), 'bgVideo.attribute' => static::SAVE_ATTRIBUTE_BG_VIDEO])
            ->alias('bgVideo')
            ->orderBy('bgVideo.position DESC');
    }

    /**
     * @param City $city
     * @return array|null|\yii\db\ActiveRecord|SecondaryRealEstate
     */
    public static function getModelByCity($city)
    {
        if (!$city->id) {
            return null;
        }

        return self::find()
            ->alias('t')
            ->where([
                't.city_id' => $city->id,
                't.published' => 1,
            ])
            ->with(['conditions', 'steps'])
            ->limit(1)
            ->one();
    }

    /**
     * @param bool $withoutDefault
     * @return bool|null|string
     */
    public function getPageBackgroundImage($withoutDefault = false)
    {
        $customImage = $this->getEntityThumb('bgImage', 'resale', 'pagebackground');
        $staticImage = '&#x2F;static&#x2F;img&#x2F;home&#x2F;poster-bg.jpg';

        return (empty($customImage) && !$withoutDefault)
            ? $staticImage
            : $customImage;
    }

    /**
     * @param bool $withoutDefault
     * @return bool|null|string
     */
    public function getPageBackgroundVideo($withoutDefault = false)
    {
        $customVideo = $this->getEntitySource('bgVideo');
        $staticVideo = '&#x2F;static&#x2F;video&#x2F;bg.mp4';

        return (empty($customVideo) && !$withoutDefault)
            ? $staticVideo
            : $customVideo;
    }

    /**
     * @return bool
     */
    public function isDisplayableVideo()
    {
        return !empty($this->getPageBackgroundVideo(true))
            || (empty($this->getPageBackgroundVideo(true)) && empty($this->getPageBackgroundImage(true)));
    }
}
