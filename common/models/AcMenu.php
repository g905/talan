<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%ac_menu}}".
 *
 * @property integer $id
 * @property integer $apartment_complex_id
 * @property integer $apartment_building_id
 * @property string $label
 * @property string $alias
 * @property string $link
 * @property integer $show_right
 * @property integer $prepared_page_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ApartmentComplex $apartmentComplex
 */
class AcMenu extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ac_menu}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::className(), ['id' => 'apartment_complex_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentBuilding()
    {
        return $this->hasOne(ApartmentBuilding::className(), ['id' => 'apartment_building_id']);
    }

    public function getMenuItemLink()
    {
        return $this->prepared_page_id
            ? SiteMenu::getPreparedPageLinkDictionary()[$this->prepared_page_id] ?? '#'
            : $this->link;
    }
}
