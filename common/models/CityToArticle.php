<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%city_to_article}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $article_id
 *
 * @property City $city
 * @property Article $article
 */
class CityToArticle extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%city_to_article}}';
    }

    public function getArticle()
    {
        return $this->hasOne(Article::class, ['id' => 'article_id']);
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
