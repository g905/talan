<?php

namespace common\models;

use backend\modules\block\models\block\VacancyPageFaqItem;
use common\components\model\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%vacancies_page}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $label
 * @property string $alias
 * @property integer $manager_id
 * @property string $form_label
 * @property string $form_description
 * @property string $form_agreement
 * @property string $form_emails
 * @property string $form_onsubmit
 * @property integer $view_count
 * @property integer $position
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property Manager $manager
 * @property SubMenu[] $subMenus
 */
class VacanciesPage extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%vacancies_page}}';
    }

    /**
     * Find one published record by its city or throw not found exception.
     *
     * @param int $cityId city identifier.
     * @return null|\yii\db\ActiveRecord|static
     * @throws \yii\web\NotFoundHttpException
     */
    public static function findOnePublishedByCityOrFail($cityId)
    {
        return static::checkFail(
            static::find()->where(['city_id' => $cityId, 'published' => self::IS_PUBLISHED])->one()
        );
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getSubMenus()
    {
        return $this->hasMany(SubMenu::class, ['object_id' => 'id'])
            ->orderBy(['position' => SORT_ASC])
            ->onCondition(['type' => SubMenu::TYPE_VACANCIES_PAGE]);
    }

    /**
     * Filter vacancies by given params.
     *
     * @param array $params
     * @param int $limit
     * @return ActiveDataProvider
     */
    public function getDataProvider($params, $limit = 3)
    {
        $city = obtain('vacancy-city', $params, $this->city_id);
        $specialization = obtain('specialization', $params);
        $query = Vacancy::find()->alias('a')->where(['a.published' => Vacancy::IS_PUBLISHED]);

        $city === null ?: $query->andFilterWhere(['a.city_id' => $city]);
        //$specialization === null ?: $query->andFilterWhere(['specialization_id' => $specialization]);
        $query->joinWith(['specialization' => function($q) use ($specialization, $city) {
           $q->alias('b');
           $q->where(['b.published' => 1]);
           $specialization === null ?: $q->where(['b.top_label' => $specialization]);
        }]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['position' => SORT_ASC]],
            'pagination' => ['pageSize' => $limit, 'defaultPageSize' => $limit],
        ]);
    }

    public function getFaqBlocks()
    {
        return $this->hasMany(VacancyPageFaqItem::class, ['object_id' => 'id'])
            ->onCondition(['type' => VacancyPageFaqItem::VACANCY_PAGE_FAQ_ITEM]);
    }
}
