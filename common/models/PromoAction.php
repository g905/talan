<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%promo_action}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $content
 * @property integer $show_on_home
 * @property integer $position_on_home
 * @property integer $manager_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $view_count
 * @property string $form_onsubmit
 *
 * @property Manager $manager
 * @property EntityToFile $image
 */
class PromoAction extends ActiveRecord 
{
    const SAVE_ATTRIBUTE_IMAGE = 'PromoActionImage';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_action}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['id' => 'manager_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['image.entity_model_name' => static::formName(), 'image.attribute' => static::SAVE_ATTRIBUTE_IMAGE])
            ->alias('image')
            ->orderBy('image.position DESC');
    }
}
