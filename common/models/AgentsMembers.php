<?php

namespace common\models;

use metalguardian\fileProcessor\helpers\FPM;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use backend\modules\agents\models\AgentsMembers as BaseAgentsMembers;

/**
 * This is the model class for table "agents_members".
 *
 * @property int $id
 * @property string $fio Fio
 * @property string $description Description
 * @property string $phone phone
 * @property int $phone_shows phone shows count
 * @property int $published Published
 * @property int $created_at Created At
 * @property int $updated_at Updated At
 */
class AgentsMembers extends BaseAgentsMembers
{
    public $relModelIndex = null;

    const SAVE_ATTRIBUTE_PHOTO = 'AgentsMembersImage';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agents_members';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'created_at', 'updated_at'], 'required'],
            [['phone_shows', 'published', 'created_at', 'updated_at'], 'integer'],
            [['fio', 'description', 'phone', 'deals_amount', 'service_evaluation', 'youtube_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Fio',
            'description' => 'Description',
            'phone' => 'Phone',
            'phone_shows' => 'Phone Shows',
            'published' => 'Published',
            'youtube_link' => 'Youtube video link',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgents()
    {
        return $this->hasOne(Agents::class, ['id' => 'agents_id']);
    }

/*
    public function getBackgroundImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['backgroundImage.entity_model_name' => static::formName(), 'backgroundImage.attribute' => static::SAVE_ATTRIBUTE_IMAGE])
            ->alias('backgroundImage')
            ->orderBy('backgroundImage.position DESC');
    }*/

    public function getBackgroundImage()
    {
        if (FPM::originalSrc($this->getPhoto()->one()->file_id)) {
            $img = FPM::originalSrc($this->getPhoto()->one()->file_id);
        } else {
            $img = '&#x2F;static&#x2F;img&#x2F;agents&#x2F;agents_bg.png';
        }
        return $img;
    }

    public function getPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['photo.entity_model_name' => static::formName(), 'photo.attribute' => static::SAVE_ATTRIBUTE_PHOTO])
            ->alias('photo')
            ->orderBy('photo.position DESC');
    }
}
