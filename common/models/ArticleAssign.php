<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%article_assign}}".
 *
 * @property integer $id
 * @property integer $article_id
 * @property integer $object_id
 * @property integer $type
 * @property integer $position
 *
 * @property Article $article
 */
class ArticleAssign extends ActiveRecord
{
    const TYPE_CAREER_ARTICLES = 1;
    const TYPE_SPECIALIZATION_ARTICLES = 2;
    const TYPE_BUILDING_ARTICLES = 3;

    public static function tableName()
    {
        return '{{%article_assign}}';
    }

    public function attributeLabels()
    {
        return [
            'article_id' => bt('Article ID', 'article-assign'),
            'object_id' => bt('Object ID', 'article-assign'),
            'type' => bt('Object type identifier', 'article-assign'),
            'position' => bt('Position', 'article-assign'),
        ];
    }

    public function getArticle()
    {
        return $this->hasOne(Article::class, ['id' => 'article_id']);
    }
}
