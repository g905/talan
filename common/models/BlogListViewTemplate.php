<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%blog_list_view_template}}".
 *
 * item_state_id - see BlogListPage model constants
 *
 * @property integer $id
 * @property integer $item_type_id
 */
class BlogListViewTemplate extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_list_view_template}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_type_id' => 'Item Type ID',
        ];
    }
}
