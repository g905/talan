<?php

namespace common\models;

use common\components\model\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%contact_marker}}".
 *
 * @property integer $id
 * @property integer $contact_page_id
 * @property string $lat
 * @property string $lng
 * @property string $label
 * @property string $phone
 * @property string $address
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ContactPage $contactPage
 */
class ContactMarker extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%contact_marker}}';
    }

    public function getContactPage()
    {
        return $this->hasOne(ContactPage::class, ['id' => 'contact_page_id']);
    }
}
