<?php

namespace common\models;

use common\components\model\ActiveRecord;
use common\helpers\Property;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%complex_list}}".
 *
 * @property int $id
 * @property int $type
 * @property int $city_id
 * @property string $top_title
 * @property string $menu_title
 * @property string $complex_more_btn_label
 * @property string $completed_projects_title
 * @property string $completed_projects_description
 * @property string $completed_projects_more_btn_label
 * @property string $benefits_title
 * @property string $form_title
 * @property string $form_description
 * @property string $form_email
 * @property string $form_agreement
 * @property int $manager_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property City $city
 * @property Manager $manager
 * @property CompletedProject[] $completedProjects
 * @property Benefit[] $benefits
 * @property ApartmentComplex[] $complexes
 * @property EntityToFile $videoMp4
 * @property EntityToFile $poster
 * @property EntityToFile $presentationFile
 */
class ComplexList extends ActiveRecord
{
    const ATTRIBUTE_VIDEO_MP4 = 'complexListVideoMp4';
    const ATTRIBUTE_POSTER = 'complexListPoster';
    const ATTRIBUTE_PRESENTATION_FILE = 'complexListPresentationFile';

    public static function tableName()
    {
        return '{{%complex_list}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompletedProjects()
    {
        return $this->hasMany(CompletedProject::class, ['id' => 'completed_project_id'])
            ->viaTable(CompletedProjectToComplexList::tableName(), ['complex_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBenefits()
    {
        return $this->hasMany(Benefit::class, ['id' => 'benefit_id'])
            ->viaTable(BenefitToComplexList::tableName(), ['complex_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplexes()
    {
        return $this->hasMany(ApartmentComplex::class, ['city_id' => 'city_id', 'type' => 'type'])
            ->alias('ac')
            ->andWhere(['ac.published' => self::IS_PUBLISHED]);
    }

    /**
     * Find one record by city identifier or fail by throwing Not fount http exception.
     *
     * @param int $cityID city identifier.
     * @return array|null|\yii\db\ActiveRecord|static
     */
    public static function findByCityIdOrFail($cityID)
    {
        return static::checkFail(
            static::find()->where(['city_id' => $cityID])->one()
        );
    }

    /**
     * Find one record by city identifier and type, with complex relation, or fail by throwing Not fount http exception.
     *
     * @param int $cityID city identifier.
     * @param int $type complex list type.
     * @return array|null|\yii\db\ActiveRecord|static
     */
    public static function findByCityIdAndTypeWithComplexesOrFail($cityID, int $type)
    {
    	
        return static::checkFail(
            static::find()->alias('cl')->where(['cl.city_id' => $cityID, 'cl.type' => $type])
                ->innerJoinWith(['complexes'])->one()
        );
    }

    public function getVideoMp4()
    {
        return $this->getEntityModel(self::ATTRIBUTE_VIDEO_MP4);
    }

    public function getPoster()
    {
        return $this->getEntityModel(self::ATTRIBUTE_POSTER);
    }

    public function getPresentationFile()
    {
        return $this->getEntityModel(self::ATTRIBUTE_PRESENTATION_FILE);
    }

    public function getVideoMp4Src()
    {
        return $this->getEntitySource('videoMp4');
    }

    public function getPosterSrc()
    {
        return $this->getEntitySource('poster');
    }

    public function getPresentationFileSrc()
    {
        return $this->getEntitySource('presentationFile');
    }

    public function isResidential()
    {
        return $this->type == Property::TYPE_RESIDENTIAL;
    }

    public function isCommercial()
    {
        return $this->type == Property::TYPE_COMMERCIAL;
    }
}
