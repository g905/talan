<?php

namespace common\models;

use common\components\model\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%public_data_menu}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PublicData[] $publicData
 */
class PublicDataMenu extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%public_data_menu}}';
    }

    public function getPublicData()
    {
        return $this->hasMany(PublicData::class, ['menu_id' => 'id']);
    }

    public static function findAllWidthLinksOrFail()
    {
        return static::checkFail(
            static::find()->alias('m')->where(['m.published' => self::IS_PUBLISHED])
                ->joinWith(['publicData' => function (ActiveQuery $q) {
                    $q->alias('d')
                        ->andWhere(['d.published' => self::IS_PUBLISHED])
                        ->orderBy(['d.position' => SORT_ASC]);
                }])
                ->orderBy(['m.position' => SORT_ASC])->all()
        );
    }

    public static function findFirstPublishedWithFirstDataOrFail()
    {
        return static::checkFail(
            static::find()->where(['published' => self::IS_PUBLISHED])
                ->orderBy(['position' => SORT_ASC])->one()
        );
    }
}
