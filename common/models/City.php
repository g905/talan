<?php

namespace common\models;

use common\helpers\Property;
use jisoft\sypexgeo\Sypexgeo;
use yii\web\NotFoundHttpException;
use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%city}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property integer $is_default
 * @property integer $sxgeo_city_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $view_count
 * @property string $common_script
 * @property string $phone
 * @property string $custom_popup_id
 * @property string $link_ig
 * @property string $link_ok
 * @property string $link_vk
 * @property string $link_fb
 * @property string $link_itunes
 * @property string $link_play
 * @property string $notification_emails
 * @property string $important_emails
 * @property integer $show_prices
 * @property CustomPopup $customPopup
 */
class City extends ActiveRecord
{
    /**
     * @var array|null|\yii\db\ActiveRecord|static current city (filled by static method).
     */
    private static $currentCity;
    /**
     * @var array|null|\yii\db\ActiveRecord|static user city (filled by static method).
     */
    private static $userCity;

    public static function tableName()
    {
        return '{{%city}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getCustomPopup()
    {
        return $this->hasOne(CustomPopup::class, ['id' => 'custom_popup_id']);
    }

    public function getCitySiteUrl()
    {
        if ($this->is_default == 1) {
            return app()->params['protocol'] . '://' . app()->params['baseUrl'];
        }

        return app()->params['protocol'] . '://' . $this->alias . '.' . app()->params['baseUrl'];
    }

    /**
     * This function is used only while sending ajax request,
     * when selecting another city from dropdown widget.
     * TODO: Must be deprecated when fully released.
     *
     * @return int
     */
    public function getOldCityId()
    {
        switch ($this->id) {
            case 1:
                return 1;
            case 2:
                return 4;
            case 4:
                return 3;
            default:
                return null;
        }
    }

    /**
     * @param $label
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findByAlias($label)
    {
        $label = static::convertCityAlias($label);
        $city = City::find()->where(['alias' => $label, 'published' => 1])->one();

        return $city;
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findBySxgeoCity()
    {
        //get user city
        $geo = new Sypexgeo();
        $geo->get(app()->request->userIP);
        //$geo->get('195.3.159.90'); //91.221.64.7
        $sxGeoCity = $geo->city;
        // TODO: Не отправлять файл
       /* $sxGeoCity = [
            'id' => '479561',
        ];*/
        if (count($sxGeoCity) == 0) {
            return null;
        }

        $city = City::find()->where([
            'published' => 1,
            'sxgeo_city_id' => $sxGeoCity['id'],
        ])->one();

        return $city;
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getDefaultCity()
    {
        $city = City::find()->where([
            'is_default' => 1,
            'published' => 1
        ])->one();

        return $city;

    }

    /**
     * @param $city
     * @return array|null|\yii\db\ActiveRecord|static
     * @throws NotFoundHttpException
     */
    public static function getCurrentCity($city)
    {
        if (static::$currentCity === null) {
            if (!isset($city)) {
                static::$currentCity = City::findBySxgeoCity();
                if (static::$currentCity === null) {
                    static::$currentCity = City::getDefaultCity();
                }
            } else {
                static::$currentCity = City::findByAlias($city); // check city
            }

            if (static::$currentCity === null) {
                throw new NotFoundHttpException();
            }
        }

        return static::$currentCity;
    }

    public static function getUserCity()
    {
        if (static::$userCity === null) {
            $hostArray = explode('.', $_SERVER['HTTP_HOST']);
            $subDomain = array_shift($hostArray);
            $subDomain = static::convertCityAlias($subDomain);
            static::$userCity = City::find()->where(['alias' => $subDomain])->one();

            if (static::$userCity === null) {
                static::$userCity = self::getDefaultCity();
            }
        }

        return static::$userCity;
    }

    /**
     * Convert punycode city alias back to regular string.
     *
     * @param string $alias
     * @return string
     */
    public static function convertCityAlias($alias)
    {
        return idn_to_utf8($alias, 0, INTL_IDNA_VARIANT_UTS46);
    }

    public function getNotificationEmailsList($important = false)
    {
        $list = [];
        if (!empty($this->notification_emails)) {
            $list = explode("\n", $this->notification_emails);
        }

        return $list;
    }

    public static function getCities()
    {
        return self::find()->isPublished()->orderBy(['label' => SORT_ASC])->all();
    }

    public static function getCitiesWithGuarantee()
    {
        $currentCity = self::getUserCity();

        return self::find()->andWhere(['city.published' => true])->andWhere(['!=', 'city.id', $currentCity->id])->innerJoinWith('guarantee')->orderBy(['label' => SORT_ASC])->all();
    }

    public function getComplexes()
    {
        return $this->hasMany(ApartmentComplex::class, ['city_id' => 'id']);
    }

    public function getResidentialComplexes()
    {
        return $this->hasMany(ApartmentComplex::class, ['city_id' => 'id'])->andWhere(['apartment_complex.type' => Property::TYPE_RESIDENTIAL])->andWhere(['guarantee_list' => 1]);
    }

    public function getGuaranteeComplexes()
    {
        return $this->hasMany(ApartmentComplex::class, ['city_id' => 'id'])->andWhere(['guarantee_list' => 1]);
    }

    public static function getCityByLabel($label)
    {
        return self::find()->isPublished()->andWhere(['label' => $label])->one();
    }

    public static function getCityById($id)
    {
        return self::find()->where(['id' => $id])->one();
    }

    public function getGuarantee()
    {
        return $this->hasOne(GuaranteePage::class, ['city_id' => 'id']);
    }
}
