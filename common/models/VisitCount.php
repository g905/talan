<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;
use yii\web\Cookie;

/**
 * This is the model class for table "{{%visit_count}}".
 *
 * @property integer $id
 * @property string $user_hash
 * @property string $model_class
 * @property integer $model_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class VisitCount extends ActiveRecord 
{
    const EXPIRE_AFTER_SECONDS = 86400; // 1 day
    const CLEAN_AFTER_SECONDS = 604800; // 1 week
    const COOKIE_NAME = 'newsVisitHash';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%visit_count}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @param ActiveRecord $model
     * @param $id integer
     * @return bool
     */
    public static function addVisit(ActiveRecord $model, $id)
    {
        try {
            self::clearOldData();

            $hash = self::getExistingHash();
            if (!$hash) {
                $hash = self::writeNewHash();
            }

            if (!self::checkExistingVisit($model, $id, $hash)) {
                $record = $model::findOne($id);
                if (!$record) {
                    return false;
                }

                $visitModel = new VisitCount();
                $visitModel->model_class = $model->formName();
                $visitModel->model_id = $id;
                $visitModel->user_hash = $hash;
                $visitModel->save(false);
            }
        } catch (\Exception $e) {
            // do nothing
        }

        return true;
    }

    /**
     * @param ActiveRecord $model
     * @param $id integer
     * @return int|string
     * @throws \yii\base\InvalidConfigException
     */
    public static function getVisitsCount(ActiveRecord $model, $id)
    {
        return self::find()
            ->where([
                'model_class' => $model->formName(),
                'model_id' => $id,
            ])
            ->count();
    }

    /**
     * @return string
     */
    protected static function generateHash()
    {
        return Yii::$app->security->generateRandomString(50);
    }

    /**
     * @return string|null
     */
    protected static function getExistingHash()
    {
        $cookie = Yii::$app->request->cookies->get(self::COOKIE_NAME);

        return $cookie->value ?? null;
    }

    /**
     * @return string
     */
    protected static function writeNewHash()
    {
        $expireTimestamp = $expireTimestamp = self::getTimestampWithOffset(self::EXPIRE_AFTER_SECONDS);
        $hash = self::generateHash();
        $cookies = Yii::$app->response->cookies;
        $cookie = new Cookie([
            'name' => self::COOKIE_NAME,
            'value' => $hash,
            'expire' => $expireTimestamp,
        ]);

        $cookies->add($cookie);

        return $hash;
    }

    /**
     * removes old records from DB that older than hash expire time and not usable
     */
    protected static function clearOldData()
    {
        $expireTimestamp = self::getTimestampWithOffset(self::CLEAN_AFTER_SECONDS);

        self::deleteAll(['>', 'created_at', $expireTimestamp]);
    }

    /**
     * @param integer $seconds
     * @return string
     */
    protected static function getTimestampWithOffset($seconds)
    {
        $date = new \DateTime();
        $interval = new \DateInterval('PT' . $seconds . 'S');
        $date->add($interval);
        return Yii::$app->formatter->asTimestamp($date);
    }

    /**
     * @param ActiveRecord $model
     * @param string $id
     * @param string $hash
     * @return bool
     */
    protected static function checkExistingVisit(ActiveRecord $model, $id, $hash)
    {
        $expireTimestamp = self::getTimestampWithOffset(self::EXPIRE_AFTER_SECONDS);

        return self::find()
            ->where([
                'model_class' => $model->formName(),
                'model_id' => $id,
                'user_hash' => $hash
            ])
            ->andWhere(['<', 'created_at', $expireTimestamp])
            ->exists();
    }
}
