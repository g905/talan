<?php

namespace common\models;

/**
 * This is the model class for table "{{%build_history_apartment_building_item_slide}}".
 *
 * @property integer $id
 * @property integer $build_history_apartment_building_item_id
 * @property string $day
 * @property string $content
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property BuildHistoryApartmentBuildingItem $buildHistoryApartmentBuildingItem
 * @property EntityToFile $image
 */
class BuildHistoryApartmentBuildingItemSlide extends \common\components\model\ActiveRecord
{
    const SAVE_ATTRIBUTE_IMAGE = 'BuildHistoryApartmentBuildingItemSlideImage';

    public static function tableName()
    {
        return '{{%build_history_apartment_building_item_slide}}';
    }

    public function getBuildHistoryApartmentBuildingItem()
    {
        return $this->hasOne(BuildHistoryApartmentBuildingItem::class, ['id' => 'build_history_apartment_building_item_id']);
    }

    public function getImage()
    {
        return $this->hasMany(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'bhacis.entity_model_name' => static::formName(),
                'bhacis.attribute' => static::SAVE_ATTRIBUTE_IMAGE
            ])
            ->alias('bhacis')
            ->orderBy('bhacis.position DESC');
    }
}
