<?php

namespace common\models;

use backend\modules\menu\models\CatalogSubMenu;
use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%catalog}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $residental_label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $commerical_label
 * @property string $completed_label
 * @property string $form_title
 * @property string $form_description
 * @property string $form_onsubmit
 * @property string $resale_label
 * @property integer $manager_id
 * @property string $agree_text
 *
 * @property City $city
 * @property EntityToFile $image
 */
class Catalog extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getSubMenus()
    {
        return $this->hasMany(CatalogSubMenu::class, ['object_id' => 'id'])
            ->orderBy(['position' => SORT_ASC])
            ->onCondition(['type' => CatalogSubMenu::TYPE_CATALOG]);
    }

    /**
     * @param City $city
     * @return array|null|\yii\db\ActiveRecord|self
     */
    public static function findByCity($city)
    {
        return self::find()->where([
            'city_id' => $city->id,
            'published' => 1
        ])->one();
    }
}
