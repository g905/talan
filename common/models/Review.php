<?php

namespace common\models;

use frontend\helpers\ImgHelper;
use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%review}}".
 *
 * @property integer $id
 * @property string $fio
 * @property string $label
 * @property string $review_text
 * @property string $youtube_video
 * @property integer $home_page_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Home $homePage
 * @property EntityToFile $photo
 */
class Review extends ActiveRecord 
{
    const SAVE_ATTRIBUTE_PHOTO = 'ReviewPhoto';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%review}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHomePage()
    {
        return $this->hasOne(Home::className(), ['id' => 'home_page_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPhoto()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['photo.entity_model_name' => static::formName(), 'photo.attribute' => static::SAVE_ATTRIBUTE_PHOTO])
            ->alias('photo')
            ->orderBy('photo.position DESC');
    }

    /**
     * @return string
     */
    public function getYoutubeCode()
    {
        if ((isset($this->youtube_video)) && ($this->youtube_video != ''))
        {
            $parts = parse_url($this->youtube_video);
            if (isset($parts['query'])){
                parse_str($parts['query'], $query);
                $code =  $query['v'];
                return $code;
            }
        }
        return null;
    }

    /**
     * @return string
     */
    public function getCoverImage()
    {
        if (isset($this->photo))
        {
            return ImgHelper::getImageSrc($this, 'home', 'review', 'photo');

        }
        return 'https://img.youtube.com/vi/'.$this->getYoutubeCode().'/hqdefault.jpg';

    }
}
