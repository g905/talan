<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%UserAuthLog}}".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $date
 * @property integer $cookieBased
 * @property integer $duration
 * @property string $error
 * @property string $ip
 * @property string $host
 * @property string $url
 * @property string $userAgent
 */
class UserAuthLog extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_auth_log}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userId' => Yii::t('app', 'User ID'),
            'date' => Yii::t('app', 'Date'),
            'cookieBased' => Yii::t('app', 'Cookie Based'),
            'duration' => Yii::t('app', 'Duration'),
            'error' => Yii::t('app', 'Error'),
            'ip' => Yii::t('app', 'Ip'),
            'host' => Yii::t('app', 'Host'),
            'url' => Yii::t('app', 'Url'),
            'userAgent' => Yii::t('app', 'User Agent'),
        ];
    }

    /**
     * @return string
     */
    public function getErrorLabel()
    {
        switch ($this->error) {
            case 'password':
                return bt('Password error');
                break;
            default:
                return bt('No errors');
                break;
        }
    }
}
