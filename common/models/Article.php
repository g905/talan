<?php

namespace common\models;

use common\helpers\SiteUrlHelper;
use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;
use common\components\CommonBuilderBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%article}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $date_published
 * @property integer $manager_id
 * @property string $short_description
 * @property string $form_title
 * @property string $form_description
 * @property string $form_agreement
 * @property string $form_onsubmit
 * @property EntityToFile $miniImageAttr
 * @property EntityToFile $coverImageAttr
 * @property Manager $manager
 * @property BlogTheme[] $blogThemes
 * @property ThemeToArticle[] $themesToArticle
 * @property CityToArticle[] $citiesToArticle
 */
class Article extends ActiveRecord 
{
    const SAVE_ATTRIBUTE_MINI_IMAGE_ATTR = 'ArticleMiniImageAttr';
    const SAVE_ATTRIBUTE_COVER_IMAGE_ATTR = 'ArticleCoverImageAttr';

    public $content;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
            'content' => [
                'class' => CommonBuilderBehavior::class,
                'attributes' => ['content'],
            ],
        ];
    }

    public function getMiniImageAttr()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['miniImageAttr.entity_model_name' => $this::formName(), 'miniImageAttr.attribute' => static::SAVE_ATTRIBUTE_MINI_IMAGE_ATTR])
            ->alias('miniImageAttr')
            ->orderBy('miniImageAttr.position DESC');
    }

    public function getCoverImageAttr()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['coverImageAttr.entity_model_name' => $this::formName(), 'coverImageAttr.attribute' => static::SAVE_ATTRIBUTE_COVER_IMAGE_ATTR])
            ->alias('coverImageAttr')
            ->orderBy('coverImageAttr.position DESC');
    }

    public function getBlogThemes()
    {
        return $this->hasMany(BlogTheme::class, ['id' => 'blog_theme_id'])
            ->viaTable(ThemeToArticle::tableName(), ['article_id' => 'id']);
    }

    public function getThemesToArticle()
    {
        return $this->hasMany(ThemeToArticle::class, ['article_id' => 'id']);
    }

    public function getCities()
    {
        return $this->hasMany(City::class, ['id' => 'city_id'])
            ->viaTable(CityToArticle::tableName(), ['article_id' => 'id']);
    }

    public function getCitiesToArticle()
    {
        return $this->hasMany(CityToArticle::class, ['article_id' => 'id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getDisplayableListDate()
    {
        return Yii::$app->formatter->asDate($this->date_published, 'dd.MM.yyyy');
    }

    public function getDisplayableViewDate()
    {
        return Yii::$app->formatter->asDate($this->date_published, 'dd.MMMM.yyyy');
    }

    public function getVisitsCount()
    {
        return VisitCount::getVisitsCount($this, $this->id);
    }

    public function getPreviewImageSrc()
    {
        //return $this->getEntityThumb('miniImageAttr', 'article', 'preview');
        return \metalguardian\fileProcessor\helpers\FPM::originalSrc($this->miniImageAttr->file_id);
    }

    public function viewUrl()
    {
        return SiteUrlHelper::getBlogViewUrl(['alias' => $this->alias]);
    }

    public static function getYears()
    {
        return self::find()
            ->select(new Expression('DATE_FORMAT(date_published, "%Y") as year'))
            ->orderBy(['year' => SORT_DESC])
            ->distinct()
            ->column();
    }

    public static function getHomePageArticles()
    {
        $city = City::getUserCity();

        $articles = self::find()
            //->where(['show_on_home' => true])
            ->joinWith('cities')
            ->andWhere(['city_to_article.city_id' => $city->id])
            ->orWhere(['city_to_article.city_id' => null])
            ->andWhere(['article.published' => true])
            ->orderBy(['article.date_published' => SORT_DESC])
            ->limit(3)
            ->all();

        usort($articles, function($a, $b)
        {
            $date1 = $a->updated_at;
            $date2 = $b->updated_at;
            if ($date1 < $date2) return 1;
            if ($date1 == $date2) return 0;
            if ($date1 > $date2) return -1;
        });

        return $articles;
    }
}
