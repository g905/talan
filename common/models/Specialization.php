<?php

namespace common\models;

use common\models\blocks\SpecializationBenefit;
use common\models\blocks\SpecializationPrinciple;
use common\models\blocks\SpecializationReview;
use yii\db\ActiveQuery;
use common\helpers\SiteUrlHelper;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%specialization}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $top_label
 * @property string $top_description
 * @property string $top_btn_label
 * @property string $top_btn_link
 * @property string $details_label
 * @property string $details_description
 * @property string $mission_label
 * @property string $mission_sub_label
 * @property string $principles_label
 * @property string $gallery_label
 * @property string $gallery_description
 * @property string $articles_label
 * @property string $articles_btn_label
 * @property string $reviews_label
 * @property string $vacancies_label
 * @property string $vacancies_btn_label
 * @property string $color
 * @property integer $manager_id
 * @property string $form_label
 * @property string $form_description
 * @property string $form_agreement
 * @property string $form_emails
 * @property string $form_onsubmit
 * @property integer $view_count
 * @property integer $position
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property Manager $manager
 * @property Vacancy[] $vacancies
 * @property EntityToFile[] $galleryImages
 * @property Article[] $articles
 * @property SpecializationReview[] $reviewBlocks
 * @property SpecializationPrinciple[] $principleBlocks
 * @property SpecializationBenefit[] $benefitBlocks
 */
class Specialization extends ActiveRecord
{
    const ATTR_TOP_IMAGE = 'specializationTopImage';
    const ATTR_TOP_VIDEO = 'specializationTopVideo';
    const ATTR_PREVIEW_IMAGE = 'specializationPreviewImage';
    const ATTR_GALLERY_IMAGES = 'specializationGallery';

    public static function tableName()
    {
        return '{{%specialization}}';
    }

    /**
     * Find one published record by its city or throw not found exception.
     *
     * @param int $id identifier.
     * @param int $cityId city identifier.
     * @return null|\yii\db\ActiveRecord|static
     * @throws \yii\web\NotFoundHttpException
     */
    public static function findOnePublishedByCityAndIdOrFail($id, $cityId)
    {
        return static::checkFail(
            static::find()->where(['id' => $id, 'city_id' => $cityId, 'published' => self::IS_PUBLISHED])->one()
        );
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getVacancies()
    {
		return $this->hasMany(Vacancy::class, ['specialization_id' => 'id'])
			->andWhere(['published' => self::IS_PUBLISHED])
			//->andWhere(['city_id' => $this->city_id])
		;
    }

    public function getArticles()
    {
        return $this->hasMany(Article::class, ['id' => 'article_id'])
            ->alias('a')
            ->where(['a.published' => self::IS_PUBLISHED])
            ->viaTable(ArticleAssign::tableName(), ['object_id' => 'id'], function (ActiveQuery $q) {
                $q->alias('aa')->andWhere(['aa.type' => ArticleAssign::TYPE_SPECIALIZATION_ARTICLES]);
            })
            ->innerJoin(ArticleAssign::tableName() . ' aa', 'aa.article_id = a.id')
            ->orderBy(['aa.position' => SORT_ASC]);
    }

    public function getReviewBlocks()
    {
        return $this->hasMany(SpecializationReview::class, ['object_id' => 'id'])
            ->onCondition(['type' => SpecializationReview::SPECIALIZATION_REVIEW]);
    }

    public function getPrincipleBlocks()
    {
        return $this->hasMany(SpecializationPrinciple::class, ['object_id' => 'id'])
            ->onCondition(['type' => SpecializationPrinciple::SPECIALIZATION_PRINCIPLE]);
    }

    public function getBenefitBlocks()
    {
        return $this->hasMany(SpecializationBenefit::class, ['object_id' => 'id'])
            ->onCondition(['type' => SpecializationBenefit::SPECIALIZATION_BENEFIT]);
    }

    public function getPreviewImageSrc()
    {
        return $this->getEntityThumb('previewImage', 'specialization', 'preview');
    }

    public function getTopImageSrc()
    {
        return $this->getEntityThumb('topImage', 'specialization', 'topimage');
    }

    public function getTopVideoSrc()
    {
        return $this->getEntitySource('topVideo');
    }

    public function getPreviewImage()
    {
        return $this->getEntityModel(self::ATTR_PREVIEW_IMAGE, 'specializationPreviewImage');
    }

    public function getTopImage()
    {
        return $this->getEntityModel(self::ATTR_TOP_IMAGE, 'specializationTopImage');
    }

    public function getGalleryImages()
    {
        return $this->getEntityModel(self::ATTR_GALLERY_IMAGES, 'specializationGalleryImages', null, true);
    }

    public function getTopVideo()
    {
        return $this->getEntityModel(self::ATTR_TOP_VIDEO, 'specializationTopVideo');
    }

    public function getSubMenus()
    {
        return $this->hasMany(SubMenu::class, ['object_id' => 'id'])
            ->onCondition(['type' => SubMenu::TYPE_SPECIALIZATION]);
    }

    public function viewUrl()
    {
        return SiteUrlHelper::getSpecializationViewUrl(['id' => $this->id]);
    }

    public static function getUniqueLabels()
    {
        $specs = self::find()->select(['top_label'])->where(['published' => 1])->groupBy(['top_label'])->asArray()->all();
        $items = [];
        foreach ($specs as $spec)
        {
            $items[] = $spec['top_label'];
        }
        return $items;
    }
}
