<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%secondary_real_estate_menu}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $secondary_real_estate_id
 * @property string $link
 * @property integer $show_right
 *
 * @property SecondaryRealEstate $secondaryRealEstate
 */
class SecondaryRealEstateMenu extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%secondary_real_estate_menu}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondaryRealEstate()
    {
        return $this->hasOne(SecondaryRealEstate::className(), ['id' => 'secondary_real_estate_id']);
    }
}
