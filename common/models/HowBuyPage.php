<?php

namespace common\models;

use backend\modules\block\models\block\BuyPageHow;
use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%how_buy_page}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $label
 * @property string $form_bottom_title
 * @property string $form_bottom_description
 * @property string $form_bottom_email
 * @property integer $manager_id
 * @property string $form_bottom_onsubmit
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 */
class HowBuyPage extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%how_buy_page}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City'),
            'label' => Yii::t('app', 'Label'),
            'form_bottom_title' => Yii::t('app', 'Form bottom title'),
            'form_bottom_description' => Yii::t('app', 'Form bottom description'),
            'form_bottom_email' => Yii::t('app', 'Form bottom email'),
            'manager_id' => Yii::t('app', 'Manager ID'),
            'form_bottom_onsubmit' => Yii::t('app', 'Form OnSubmit event'),
            'published' => Yii::t('app', 'Published'),
            'position' => Yii::t('app', 'Position'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getSubMenus()
    {
        return $this->hasMany(SubMenu::class, ['object_id' => 'id'])
            ->orderBy(['position' => SORT_ASC])
            ->onCondition([
                'type' => SubMenu::TYPE_BUY_HOW_PAGE,
                'published' => 1
            ]);
    }

    public function getHowBlocks()
    {
        return $this->hasMany(BuyPageHow::class, ['object_id' => 'id'])
            ->onCondition([
                'type' => BuyPageHow::BUY_PAGE_HOW,
                'published' => 1
            ]);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    /**
     * @param City $city
     * @return array|null|\yii\db\ActiveRecord|self
     */
    public static function findByCity($city)
    {
        return HowBuyPage::find()->where([
            'city_id' => $city->id,
            'published' => 1
        ])->one();
    }
}
