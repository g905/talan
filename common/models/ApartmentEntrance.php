<?php

namespace common\models;

use backend\modules\block\models\block\FloorPlan;
use Yii;

/**
 * This is the model class for table "{{%apartment_entrance}}".
 *
 * @property integer $id
 * @property integer $entrance
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $building_id
 * @property integer $building_complex_id
 *
 * @property ApartmentBuilding $building
 */
class ApartmentEntrance extends \common\components\model\ActiveRecord
{
    public static function tableName()
    {
        return '{{%apartment_entrance}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'entrance' => Yii::t('app', 'Entrance'),
            'published' => Yii::t('app', 'Published'),
            'position' => Yii::t('app', 'Position'),
            'building_id' => Yii::t('app', 'Building ID'),
            'building_complex_id' => Yii::t('app', 'Building COMPLEX ID'),
        ];
    }

    public function getBuilding()
    {
        return $this->hasOne(ApartmentBuilding::class, ['id' => 'building_id']);
    }

    public function getComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'building_complex_id']);
    }

    public function getFloorPlans()
    {
        return $this->hasMany(FloorPlan::class, ['object_id' => 'id'])
            ->onCondition(['type' => FloorPlan::FLOOR_PLAN]);
    }
}
