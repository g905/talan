<?php

namespace common\models;

use Yii;
use common\models\base\StaticPage;
use common\components\ConfigurationComponent;
use frontend\helpers\StaticPageRelationTrait;

/**
 * This is class for static page model.
 */
class BlogListPage extends StaticPage
{
    use StaticPageRelationTrait;

    const LABEL = 'blogListPageLabel';
    const TOP_FORM_TITLE = 'blogListPageTopFormTitle';
    const TOP_FORM_TEXT = 'blogListPageTopFormText';
    const TOP_FORM_AGREEMENT = 'blogListPageTopFormAgreement';
    const TOP_FORM_BUTTON_TEXT = 'blogListPageTopFormButtonText';
    const TOP_FORM_ONSUBMIT = 'blogListPageTopFormOnSubmit';
    const BOTTOM_FORM_BLACK_TITLE = 'blogListPageBottomFormBlackTitle';
    const BOTTOM_FORM_WHITE_TITLE = 'blogListPageBottomFormWhiteTitle';
    const BOTTOM_FORM_AGREEMENT = 'blogListPageBottomFormAgreement';
    const BOTTOM_FORM_BUTTON_TEXT = 'blogListPageBottomFormButtonText';
    const BOTTOM_FORM_ONSUBMIT = 'blogListPageBottomFormOnSubmit';

    const SAVE_ATTRIBUTE_VIDEO_POSTER = 'blogListPageSAveAttributeVideoPoster';
    const SAVE_ATTRIBUTE_VIDEO_BG = 'blogListPageSAveAttributeVideoBG';


    const LAYOUT_LEFT = 1;
    const LAYOUT_RIGHT = 2;
    const LAYOUT_FULL_ROW = 3;


    public $label;
    public $topFormTitle;
    public $topFormText;
    public $topFormAgreement;
    public $topFormButtonText;
    public $topFormOnSubmit;
    public $bottomFormBlackTitle;
    public $bottomFormWhiteTitle;
    public $bottomFormAgreement;
    public $bottomFormButtonText;
    public $bottomFormOnSubmit;

    /**
    * @return array
    */
    public function getKeys()
    {
        return [
            self::LABEL,
            self::TOP_FORM_TITLE,
            self::TOP_FORM_TEXT,
            self::TOP_FORM_AGREEMENT,
            self::TOP_FORM_BUTTON_TEXT,
            self::TOP_FORM_ONSUBMIT,
            self::BOTTOM_FORM_BLACK_TITLE,
            self::BOTTOM_FORM_WHITE_TITLE,
            self::BOTTOM_FORM_AGREEMENT,
            self::BOTTOM_FORM_BUTTON_TEXT,
            self::BOTTOM_FORM_ONSUBMIT,
        ];
    }

    /**
    * @return $this
    */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->label = $config->get(self::LABEL);
        $this->topFormTitle = $config->get(self::TOP_FORM_TITLE);
        $this->topFormText = $config->get(self::TOP_FORM_TEXT);
        $this->topFormAgreement = $config->get(self::TOP_FORM_AGREEMENT);
        $this->topFormButtonText = $config->get(self::TOP_FORM_BUTTON_TEXT);
        $this->topFormOnSubmit = $config->get(self::TOP_FORM_ONSUBMIT);
        $this->bottomFormBlackTitle = $config->get(self::BOTTOM_FORM_BLACK_TITLE);
        $this->bottomFormWhiteTitle = $config->get(self::BOTTOM_FORM_WHITE_TITLE);
        $this->bottomFormAgreement = $config->get(self::BOTTOM_FORM_AGREEMENT);
        $this->bottomFormButtonText = $config->get(self::BOTTOM_FORM_BUTTON_TEXT);
        $this->bottomFormOnSubmit = $config->get(self::BOTTOM_FORM_ONSUBMIT);

        return $this;
    }
}
