<?php

namespace common\models;

use common\components\model\ActiveRecord;
use metalguardian\fileProcessor\helpers\FPM;

/**
 * This is the model class for table "{{%bank}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $mortgage_rate
 * @property integer $published
 *
 * @property BankToHypothec[] $bankToHypothecs
 * @property Hypothec[] $hypothecs
 */
class Bank extends ActiveRecord
{
    const ATTR_BANK_SVG_IMAGE = 'bankSvgImage';

    public static function tableName()
    {
        return '{{%bank}}';
    }

    public function getBankToHypothecs()
    {
        return $this->hasMany(BankToHypothec::class, ['bank_id' => 'id']);
    }

    public function getHypothecs()
    {
        return $this->hasMany(Hypothec::class, ['id' => 'hypothec_id'])->viaTable('{{%bank_to_hypothec}}', ['bank_id' => 'id']);
    }


    public function getImage()
    {
        return $this->getEntityModel(self::ATTR_BANK_SVG_IMAGE, 'bankSvgImage');
    }

    public function getImageSrc()
    {
        return $this->getEntitySource('image');
    }

    public function getSvgImagePath()
    {
        /** @var EntityToFile $entity */
        $path = $this->getEntitySource('image');

        return $path ? getAlias("@webroot$path") : false;
    }
}
