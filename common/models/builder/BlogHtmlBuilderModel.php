<?php
namespace common\models\builder;

use common\components\BuilderModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class TextBuilderModel
 *
 * @package backend\modules\builder\models\builder
 */
class BlogHtmlBuilderModel extends BuilderModel
{
    public $text;

    /***
     * @return string
     */
    public function getName()
    {
        return Yii::t('back/builder', 'Html block');
    }

    public function rules()
    {
        $rules = ArrayHelper::merge(parent::rules(), [
            [['text'], 'safe'],
        ]);

        return $this->prepareRules($rules);
    }

    public function getAttributeLabels()
    {
        $labels = [
            'text' => Yii::t('back/builder', 'Content'),
        ];

        return $this->prepareLabels($labels);
    }

    public function getConfig()
    {
        return ArrayHelper::merge(parent::getDefaultConfig(), [
            'text' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'text',
                ]
            ],
        ]);
    }

    public function getLocalized()
    {
        return [/*'text'*/];
    }
}
