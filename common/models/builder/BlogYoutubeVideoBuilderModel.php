<?php
namespace common\models\builder;

use backend\modules\builder\components\BuilderImageUpload;
use common\components\BuilderModel;
use frontend\helpers\FrontendHelper;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ImageBuilderModel
 *
 * @package backend\modules\builder\models\builder
 */
class BlogYoutubeVideoBuilderModel extends BuilderModel
{
    public $url;
    public $image;

    public function getName()
    {
        return Yii::t('back/builder', 'Youtube video block');
    }

    public function rules()
    {
        $rules = ArrayHelper::merge(parent::rules(), [
            [['url'], 'string', 'max' => 250],
        ]);

        return $this->prepareRules($rules);
    }

    public function getAttributeLabels()
    {
        $labels = [
            'url' => Yii::t('back/builder', 'Youtube video url'),
            'image' => Yii::t('back/builder', 'Preview image'),
        ];

        return $this->prepareLabels($labels);
    }

    public function getConfig()
    {
        return ArrayHelper::merge(parent::getDefaultConfig(), [
            'url' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ]
            ],
            'image' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => BuilderImageUpload::className(),
                'options' => [
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ],
            ],
        ]);
    }

    public function getLocalized()
    {
        return [];
    }

    /**
     * @return mixed
     */
    public function getYoutubeId()
    {
        return FrontendHelper::extractYoutubeVideoId($this->url);
    }
}
