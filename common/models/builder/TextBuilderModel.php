<?php
namespace common\models\builder;

use common\components\BuilderModel;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;

/**
 * Class TextBuilderModel
 *
 * @package backend\modules\builder\models\builder
 */
class TextBuilderModel extends BuilderModel
{
    public $label;

    public $text;

    public function getName()
    {
        return 'Text';
    }

    public function rules()
    {
        $rules = ArrayHelper::merge(parent::rules(), [
            [['label'], 'required'],
            [['text'], 'safe'],
        ]);

        return $this->prepareRules($rules);
    }

    public function getAttributeLabels()
    {
        $labels = [
            'images' => 'Image',
        ];

        return $this->prepareLabels($labels);
    }

    public function getConfig()
    {
        return ArrayHelper::merge(parent::getDefaultConfig(), [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'text' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'text',
                ]
            ],
        ]);
    }

    public function getLocalized()
    {
        return ['label', 'text'];
    }
}
