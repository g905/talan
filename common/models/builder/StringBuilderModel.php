<?php
namespace common\models\builder;

use common\components\BuilderModel;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;

/**
 * Class TextBuilderModel
 *
 * @package backend\modules\builder\models\builder
 */
class StringBuilderModel extends BuilderModel
{
    public $label;

    public function getName()
    {
        return 'String';
    }

    public function rules()
    {
        $rules = ArrayHelper::merge(parent::rules(), [
            ['label', 'required'],
        ]);

        return $this->prepareRules($rules);
    }

    public function attributeLabels()
    {
        $labels = [
            'label' => 'Label',
        ];

        return $this->prepareLabels($labels);
    }

    public function getConfig()
    {
        return ArrayHelper::merge(parent::getDefaultConfig(), [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
        ]);
    }

    public function getLocalized()
    {
        return ['label'];
    }
}
