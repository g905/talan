<?php
namespace common\models\builder;

use backend\modules\builder\components\BuilderImageUpload;
use common\components\BuilderModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ImageBuilderModel
 *
 * @package backend\modules\builder\models\builder
 */
class BlogImageWithTextBuilderModel extends BuilderModel
{
    public $text;
    public $image;

    public function getName()
    {
        return Yii::t('back/builder', 'Image with text block');
    }

    public function rules()
    {
        $rules = ArrayHelper::merge(parent::rules(), [
            [['text'], 'string', 'max' => 1000],
        ]);

        return $this->prepareRules($rules);
    }

    public function getAttributeLabels()
    {
        $labels = [
            'text' => Yii::t('back/builder', 'Text under image'),
            'image' => Yii::t('back/builder', 'Image'),
        ];

        return $this->prepareLabels($labels);
    }

    public function getConfig()
    {
        return ArrayHelper::merge(parent::getDefaultConfig(), [
            'image' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => BuilderImageUpload::className(),
                'options' => [
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ],
            ],
            'text' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                'options' => [
                    'maxlength' => true,
                ]
            ],
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
