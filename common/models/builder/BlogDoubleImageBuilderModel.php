<?php
namespace common\models\builder;

use backend\modules\builder\components\BuilderImageUpload;
use common\components\BuilderModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ImageBuilderModel
 *
 * @package backend\modules\builder\models\builder
 */
class BlogDoubleImageBuilderModel extends BuilderModel
{
    public $image;
    public $image2;

    public function getName()
    {
        return Yii::t('back/builder', 'Double image block');
    }

    public function getAttributeLabels()
    {
        $labels = [
            'image' => Yii::t('back/builder', 'Left image'),
            'image2' => Yii::t('back/builder', 'Right image'),
        ];

        return $this->prepareLabels($labels);
    }

    public function getConfig()
    {
        return ArrayHelper::merge(parent::getDefaultConfig(), [
            'image' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => BuilderImageUpload::className(),
                'options' => [
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ],
            ],
            'image2' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => BuilderImageUpload::className(),
                'options' => [
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ],
            ],
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
