<?php
namespace common\models\builder;

use backend\modules\builder\components\BuilderImageUpload;
use common\components\BuilderModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ImageBuilderModel
 *
 * @package backend\modules\builder\models\builder
 */
class BlogSliderBuilderModel extends BuilderModel
{
    public $images;

    public function getName()
    {
        return Yii::t('back/builder', 'Slider block');
    }

    public function getAttributeLabels()
    {
        $labels = [
            'image' => Yii::t('back/builder', 'Images'),
        ];

        return $this->prepareLabels($labels);
    }

    public function getConfig()
    {
        return ArrayHelper::merge(parent::getDefaultConfig(), [
            'images' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => BuilderImageUpload::className(),
                'options' => [
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => true, //Вкл/выкл множественную загрузку
                    'showMetaDataBtn' => true,
                ],
            ],
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
