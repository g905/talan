<?php

namespace common\models\builder\publicData;

use backend\helpers\StdInput;
use common\components\BuilderModel;

/**
 * Class FilesSection
 *
 * @package backend\modules\builder\models\builder\publicData
 */
class FilesSection extends BuilderModel
{
    const ATTR_FILES = 'files';

    public $label;
    public $description;
    public $files;

    public function getName()
    {
        return bt('Files section', 'builder');
    }

    public function rules()
    {
        return $this->prepareRules(merge(parent::rules(), [
            [['label', 'description'], 'required'],
            [['files'], 'safe'],
        ]));
    }

    public function getAttributeLabels()
    {
        return $this->prepareLabels([
            'label' => bt('Label', 'builder'),
            'description' => bt('Description', 'builder'),
            'files' => bt('Files', 'builder'),
        ]);
    }

    public function attributeHints()
    {
        return [
            'files' => 'Для установки пароля для скачки файла нажмите на иконку бейджа под загруженным файлом.'
        ];
    }

    public function getConfig()
    {
        return merge(parent::getDefaultConfig(), [
            'label' => StdInput::text(),
            'description' => StdInput::editor(),
            'files' => StdInput::fileUpload(self::ATTR_FILES, true, [], true),
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
