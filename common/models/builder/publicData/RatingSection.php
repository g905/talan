<?php

namespace common\models\builder\publicData;

use backend\helpers\StdInput;
use common\components\BuilderModel;
use backend\modules\page\models\publicDataWidget\Rating;

/**
 * Class RatingSection
 *
 * @package backend\modules\builder\models\builder\publicData
 */
class RatingSection extends BuilderModel
{
    public $label;
    public $description;
    public $tables;
    public $button_label;
    public $button_link;

    public function getName()
    {
        return bt('Rating', 'builder');
    }

    public function rules()
    {
        return $this->prepareRules(merge(parent::rules(), [
            [['label', 'description', 'tables'], 'required'],
            [['label', 'button_label', 'button_link'], 'string', 'max' => 255],
            [['description'], 'string'],
        ]));
    }

    public function getAttributeLabels()
    {
        return $this->prepareLabels([
            'label' => bt('Label', 'builder'),
            'description' => bt('Description', 'builder'),
            'button_label' => bt('Button Label', 'builder'),
            'button_link' => bt('Button Link', 'builder'),
            'tables' => bt('Tables', 'builder'),
        ]);
    }

    public function getConfig()
    {
        return merge(parent::getDefaultConfig(), [
            'label' => StdInput::text(),
            'description' => StdInput::editor(),
            'button_label' => StdInput::text(),
            'button_link' => StdInput::text(),
            'tables' => StdInput::publicDataWidgetSelect(Rating::class, Rating::TYPE_RATING, 'label', 'id', null, true),
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
