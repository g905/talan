<?php

namespace common\models\builder\publicData;

use metalguardian\formBuilder\ActiveFormBuilder;
use backend\helpers\StdInput;
use backend\modules\builder\components\BuilderImageUpload;
use common\models\EntityToFile;
use common\components\BuilderModel;

/**
 * Class ContentSection
 *
 * @package backend\modules\builder\models\builder\publicData
 * @property EntityToFile $previewImage
 */
class ContentSection extends BuilderModel
{
    const SAVE_ATTRIBUTE_PREVIEW_IMAGE = 'ContentSectionPreview_image';

    public $content;
    public $preview_image;
    public $video_link;

    public function getName()
    {
        return bt('Content section', 'builder');
    }

    public function rules()
    {
        return $this->prepareRules(merge(parent::rules(), [
            [['content'], 'required'],
            [['content', 'preview_image', 'video_link'], 'string'],
        ]));
    }

    public function getAttributeLabels()
    {
        return $this->prepareLabels([
            'content' => bt('Content', 'builder'),
            'preview_image' => bt('Preview video image', 'builder'),
            'video_link' => bt('Video', 'builder'),
        ]);
    }

    public function getConfig()
    {
        return merge(parent::getDefaultConfig(), [
            'content' => StdInput::editor(),
            'preview_image' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => BuilderImageUpload::class,
                'options' => [
                    'model' => $this,
                    'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                    'attribute' => 'preview_image',
                    'saveAttribute' => self::SAVE_ATTRIBUTE_PREVIEW_IMAGE,
                    'aspectRatio' => false,
                    'multiple' => false,
                ],
            ],
            'video_link' => StdInput::text(),
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
