<?php

namespace common\models\builder\publicData;

use backend\helpers\StdInput;
use backend\modules\page\models\publicDataWidget\DiagramDdu;
use common\components\BuilderModel;
use common\models\ApartmentComplex;
use yii\db\ActiveQuery;

/**
 * Class QuantityDdu
 *
 * @package backend\modules\builder\models\builder\publicData
 */
class QuantityDdu extends BuilderModel
{
    public $label;
    public $description;
    public $diagrams;

    public function getName()
    {
        return bt('Quantity Ddu', 'builder');
    }

    public function rules()
    {
        return $this->prepareRules(merge(parent::rules(), [
            [['label', 'description', 'diagrams'], 'required'],
            [['label'], 'string', 'max' => 255],
            [['description'], 'string'],
        ]));
    }

    public function getAttributeLabels()
    {
        return $this->prepareLabels([
            'label' => bt('Label', 'builder'),
            'description' => bt('Description', 'builder'),
            'diagrams' => bt('Diagrams', 'builder'),
        ]);
    }

    public function attributeHints()
    {
        return [
            'diagrams' => 'Внимание! Не выбирайте графики по одинаковым ЖК, так как отображается только один график в рамках одного ЖК.',
        ];
    }

    public function getConfig()
    {
        return merge(parent::getDefaultConfig(), [
            'label' => StdInput::text(),
            'description' => StdInput::editor(),
            'diagrams' => StdInput::publicDataWidgetSelect(DiagramDdu::class, DiagramDdu::TYPE_DIAGRAM_DDU, 'label', 'id', null, false, true),
        ]);
    }

    public function getLocalized()
    {
        return [];
    }

    public function getDropdownData()
    {
        $diagramsToComplexes = map(DiagramDdu::find()
            ->select(['id', 'complex_id'])
            ->where(['id' => explode(',', $this->diagrams)])
            ->asArray()->all(), 'id', 'complex_id');

        if ($diagramsToComplexes) {
            $complexes = ApartmentComplex::find()
                ->select(['id', 'label', 'city_id'])
                ->with(['city' => function (ActiveQuery $q) {
                    $q->select(['id', 'label']);
                }])
                ->where(['id' => array_values($diagramsToComplexes)])
                ->asArray()->indexBy('id')->all();

            foreach ($diagramsToComplexes as $diagramId => $complexId) {
                $diagramsToComplexes[$diagramId] = [
                    'city' => [
                        'id' => obtain([$complexId, 'city', 'id'], $complexes),
                        'label' => obtain([$complexId, 'city', 'label'], $complexes),
                    ],
                    'complex' => [
                        'id' => obtain([$complexId, 'id'], $complexes),
                        'label' => obtain([$complexId, 'label'], $complexes),
                    ],
                ];
            }
        }

        return $diagramsToComplexes;
    }
}
