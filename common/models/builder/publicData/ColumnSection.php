<?php

namespace common\models\builder\publicData;

use backend\helpers\StdInput;
use backend\modules\page\models\publicDataWidget\ColumnRow;
use common\components\BuilderModel;

/**
 * Class ColumnSection
 *
 * @package backend\modules\builder\models\builder\publicData
 */
class ColumnSection extends BuilderModel
{
    public $label;
    public $chart;

    public function getName()
    {
        return bt('Column chart block', 'builder');
    }

    public function rules()
    {
        return $this->prepareRules(merge(parent::rules(), [
            [['label', 'chart'], 'required'],
        ]));
    }

    public function getAttributeLabels()
    {
        return $this->prepareLabels([
            'label' => bt('Label', 'builder'),
            'chart' => bt('Chart', 'builder'),
        ]);
    }

    public function getConfig()
    {
        return merge(parent::getDefaultConfig(), [
            'label' => StdInput::text(),
            'chart' => StdInput::publicDataWidgetSelectList(ColumnRow::TYPE_COLUMN_CHART_ROW, 'value', 'id', true),
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
