<?php

namespace common\models\builder\publicData;

use backend\helpers\StdInput;
use backend\modules\page\models\publicDataWidget\DiagramRow;
use common\components\BuilderModel;

/**
 * Class DiagramTable
 *
 * @package backend\modules\builder\models\builder\publicData
 */
class DiagramTable extends BuilderModel
{
    public $label;
    public $description;
    public $diagrams;

    public function getName()
    {
        return bt('Diagram block', 'builder');
    }

    public function rules()
    {
        return $this->prepareRules(merge(parent::rules(), [
            [['label', 'description', 'diagrams'], 'required'],
        ]));
    }

    public function getAttributeLabels()
    {
        return $this->prepareLabels([
            'label' => bt('Label', 'builder'),
            'description' => bt('Description', 'builder'),
            'diagrams' => bt('Diagrams', 'builder'),
        ]);
    }

    public function getConfig()
    {
        return merge(parent::getDefaultConfig(), [
            'label' => StdInput::text(),
            'description' => StdInput::editor(),
            'diagrams' => StdInput::publicDataWidgetSelect(DiagramRow::class, DiagramRow::TYPE_DIAGRAM_ROW, 'value', 'id', null, true),
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
