<?php

namespace common\models\builder\publicData;

use backend\helpers\StdInput;
use backend\modules\page\models\publicDataWidget\DiagramSchedule;
use common\components\BuilderModel;

/**
 * Class Schedule
 *
 * @package backend\modules\builder\models\builder\publicData
 */
class Schedule extends BuilderModel
{
    public $label;
    public $description;
    public $diagrams;

    public function getName()
    {
        return bt('Schedule', 'builder');
    }

    public function rules()
    {
        return $this->prepareRules(merge(parent::rules(), [
            [['label', 'diagrams'], 'required'],
            [['label'], 'string', 'max' => 255],
            ['description', 'string'],
        ]));
    }

    public function getAttributeLabels()
    {
        return $this->prepareLabels([
            'label' => bt('Label', 'builder'),
            'description' => bt('Description', 'builder'),
            'diagrams' => bt('Diagrams', 'builder'),
        ]);
    }

    public function getConfig()
    {
        return merge(parent::getDefaultConfig(), [
            'label' => StdInput::text(),
            'description' => StdInput::editor(),
            'diagrams' => StdInput::publicDataWidgetSelect(DiagramSchedule::class, DiagramSchedule::TYPE_DIAGRAM_SCHEDULE, 'label', 'id', null, false, true),
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
