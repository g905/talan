<?php

namespace common\models\builder\publicData;

use backend\helpers\StdInput;
use backend\modules\page\models\publicDataWidget\CustomDdu;
use common\components\BuilderModel;
use common\models\ApartmentComplex;
use yii\db\ActiveQuery;

/**
 * Class CustomDduSection
 *
 * @package backend\modules\builder\models\builder\publicData
 */
class CustomDduSection extends BuilderModel
{
    public $label;
    public $description;
    public $diagrams;

    public function getName()
    {
        return bt('Custom Ddu', 'builder');
    }

    public function rules()
    {
        return $this->prepareRules(merge(parent::rules(), [
            [['label', 'description', 'diagrams'], 'required'],
            [['label'], 'string', 'max' => 255],
            [['description'], 'string'],
        ]));
    }

    public function getAttributeLabels()
    {
        return $this->prepareLabels([
            'label' => bt('Label', 'builder'),
            'description' => bt('Description', 'builder'),
            'diagrams' => bt('Diagrams', 'builder'),
        ]);
    }

//    public function attributeHints()
//    {
//        return [
//            'diagrams' => 'Внимание! Не выбирайте графики по одинаковым ЖК, так как отображается только один график в рамках одного ЖК.',
//        ];
//    }

    public function getConfig()
    {
        return merge(parent::getDefaultConfig(), [
            'label' => StdInput::text(),
            'description' => StdInput::editor(),
            'diagrams' => StdInput::publicDataWidgetSelect(CustomDdu::class, CustomDdu::TYPE_CUSTOM_DIAGRAM_DDU, 'label', 'id', null, false, false),
        ]);
    }

    public function getLocalized()
    {
        return [];
    }

    public function getDropdownData()
    {
        $diagramsToComplexes = CustomDdu::find()
            ->select(['id', 'placed', 'city_id'])
            ->where(['id' => explode(',', $this->diagrams)])
            ->all();

        return $diagramsToComplexes;
    }
}
