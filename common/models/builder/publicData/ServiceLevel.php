<?php

namespace common\models\builder\publicData;

use backend\helpers\StdInput;
use backend\modules\page\models\publicDataWidget\ServiceChartRow;
use common\components\BuilderModel;

/**
 * Class ServiceLevel
 *
 * @package common\models\builder\publicData
 */
class ServiceLevel extends BuilderModel
{
    public $label;
    public $description;
    public $number;
    public $numberTitle;
    public $numberDescription;
    public $charts;

    public function getName()
    {
        return bt('Service level block', 'builder');
    }

    public function rules()
    {
        return $this->prepareRules(merge(parent::rules(), [
            [['number'], 'number', 'max' => 10],
            [['description'], 'string'],
            [['label', 'numberTitle', 'numberDescription'], 'string', 'max' => 255],
            [['label', 'description', 'number', 'numberTitle', 'numberDescription', 'charts'], 'required'],
        ]));
    }

    public function getAttributeLabels()
    {
        return $this->prepareLabels([
            'label' => bt('Label', 'builder'),
            'description' => bt('Description', 'builder'),
            'number' => bt('Number', 'builder'),
            'numberTitle' => bt('Number title', 'builder'),
            'numberDescription' => bt('Number description', 'builder'),
            'charts' => bt('Charts', 'builder'),
        ]);
    }

    public function getConfig()
    {
        return merge(parent::getDefaultConfig(), [
            'label' => StdInput::text(),
            'description' => StdInput::editor(),
            'number' => StdInput::text(),
            'numberTitle' => StdInput::text(),
            'numberDescription' => StdInput::text(),
            'charts' => StdInput::publicDataWidgetSelect(ServiceChartRow::class, ServiceChartRow::TYPE_SERVICE_CHART_ROW, 'chart_name', 'id', null, true),
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
