<?php

namespace common\models\builder\publicData;

use backend\helpers\StdInput;
use backend\modules\page\models\publicDataWidget\DiagramCircle;
use common\components\BuilderModel;

/**
 * Class DiagramSection
 *
 * @package backend\modules\builder\models\builder\publicData
 */
class DiagramSection extends BuilderModel
{
    public $label;
    public $diagram;

    public function getName()
    {
        return bt('Diagram Circle', 'builder');
    }

    public function rules()
    {
        return $this->prepareRules(merge(parent::rules(), [
            [['label', 'diagram'], 'required'],
        ]));
    }

    public function getAttributeLabels()
    {
        return $this->prepareLabels([
            'label' => bt('Label', 'builder'),
            'diagram' => bt('Diagram', 'builder'),
        ]);
    }

    public function getConfig()
    {
        return merge(parent::getDefaultConfig(), [
            'label' => StdInput::text(),
            'diagram' => StdInput::publicDataWidgetSelectList(DiagramCircle::TYPE_DIAGRAM_CIRCLE),
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
