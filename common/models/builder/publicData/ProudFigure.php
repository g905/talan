<?php

namespace common\models\builder\publicData;

use backend\helpers\StdInput;
use common\components\BuilderModel;

/**
 * Class ProudFigure
 *
 * @package backend\modules\builder\models\builder\publicData
 */
class ProudFigure extends BuilderModel
{
    public $figure;
    public $description;

    public function getName()
    {
        return bt('Proud figure', 'builder');
    }

    public function rules()
    {
        return $this->prepareRules(merge(parent::rules(), [
            [['figure', 'description'], 'required'],
            [['figure', 'description'], 'string', 'max' => 255],
        ]));
    }

    public function getAttributeLabels()
    {
        return $this->prepareLabels([
            'figure' => bt('Figure', 'builder'),
            'description' => bt('Description', 'builder'),
        ]);
    }

    public function getConfig()
    {
        return merge(parent::getDefaultConfig(), [
            'figure' => StdInput::text(),
            'description' => StdInput::text(),
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
