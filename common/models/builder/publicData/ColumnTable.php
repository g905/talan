<?php

namespace common\models\builder\publicData;

use backend\helpers\StdInput;
use backend\modules\page\models\publicDataWidget\ColumnRow;
use common\components\BuilderModel;

/**
 * Class ColumnTable
 *
 * @package backend\modules\builder\models\builder\publicData
 */
class ColumnTable extends BuilderModel
{
    public $label;
    public $description;
    public $charts;

    public function getName()
    {
        return bt('Column charts block', 'builder');
    }

    public function rules()
    {
        return $this->prepareRules(merge(parent::rules(), [
            [['label', 'description', 'charts'], 'required'],
        ]));
    }

    public function getAttributeLabels()
    {
        return $this->prepareLabels([
            'label' => bt('Label', 'builder'),
            'description' => bt('Description', 'builder'),
            'charts' => bt('Charts', 'builder'),
        ]);
    }

    public function getConfig()
    {
        return merge(parent::getDefaultConfig(), [
            'label' => StdInput::text(),
            'description' => StdInput::editor(),
            'charts' => StdInput::publicDataWidgetSelect(ColumnRow::class, ColumnRow::TYPE_COLUMN_CHART_ROW, 'value', 'id', null, true),
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
