<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "partner_request".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $referer
 * @property integer $created_at
 * @property integer $updated_at
 */
class PartnerRequest extends \common\components\model\ActiveRecord
{
    public static function tableName()
    {
        return 'partner_request';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }
}
