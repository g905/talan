<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%custom_page}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $label
 * @property string $alias
 * @property string $content
 * @property integer $manager_id
 * @property integer $view_count
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $form_emails
 * @property string $form_onsubmit
 *
 * @property City $city
 * @property Manager $manager
 */
class CustomPage extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%custom_page}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['id' => 'manager_id']);
    }

    /**
     * @return CustomPage | null
     */
    public static function findByCityAndAlias($city, $alias)
    {
        /** @var CustomPage $page */
        $page = CustomPage::find()->where([
            'alias' => $alias,
            'published' => 1,
        ])->andWhere([
            'or',
            ['city_id' => $city->id],
            ['city_id' => null]
        ])->one();

        return $page;
    }
}
