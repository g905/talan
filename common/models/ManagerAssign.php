<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%manager_assign}}".
 *
 * @property integer $id
 * @property integer $manager_id
 * @property integer $object_id
 * @property integer $type
 * @property integer $position
 *
 * @property Manager $manager
 */
class ManagerAssign extends ActiveRecord
{
    const TYPE_EARTH_MANAGER = 1;

    public static function tableName()
    {
        return '{{%manager_assign}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'manager_id' => Yii::t('app', 'Manager ID'),
            'object_id' => Yii::t('app', 'Object ID'),
            'type' => Yii::t('app', 'Object type identifier'),
            'position' => Yii::t('app', 'Position'),
        ];
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }
}
