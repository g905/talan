<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%benefit}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $published
 * @property BenefitToComplexList[] $benefitToComplexLists
 * @property ComplexList[] $complexLists
 * @property EntityToFile $photo
 */
class Benefit extends ActiveRecord
{
    /**
     * @var string save image attributes.
     */
    const SAVE_ATTRIBUTE_PHOTO = 'benefitPhoto';

    public static function tableName()
    {
        return '{{%benefit}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto()
    {
        return $this->getEntityModel(self::SAVE_ATTRIBUTE_PHOTO, 'benefitPhoto', 'Benefit');
    }

    /**
     * @return bool|null|string
     */
    public function getPhotoPreviewSrc()
    {
        return $this->getEntityThumb('photo', 'benefit', 'photoPreview');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBenefitToComplexLists()
    {
        return $this->hasMany(BenefitToComplexList::class, ['benefit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplexLists()
    {
        return $this->hasMany(ComplexList::class, ['id' => 'complex_list_id'])
            ->viaTable('{{%benefit_to_complex_list}}', ['benefit_id' => 'id']);
    }
}
