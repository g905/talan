<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%request_problem}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 */
class RequestProblem extends \common\components\model\ActiveRecord
{
    public static function tableName()
    {
        return '{{%request_problem}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'label' => Yii::t('app', 'Label'),
            'published' => Yii::t('app', 'Published'),
            'position' => Yii::t('app', 'Position'),
        ];
    }
}
