<?php

namespace common\models\blocks;

use common\models\Block;
use common\models\EntityToFile;

/**
 * Class HypothecBenefit
 *
 * @package common\models\blocks
 *
 * @property EntityToFile $image
 * @property string|null $imageSrc
 */
class HypothecBenefit extends Block
{
    /**
     * @var string saving attributes.
     */
    const ATTR_ITEM_IMAGE = 'hypothecBenefitImage';

    public function getImage()
    {
        return $this->getEntityModel(self::ATTR_ITEM_IMAGE, 'hypothecBenefitImage');
    }

    public function getImageSrc()
    {
        return $this->getEntityThumb('image', 'hypothec', 'benefit');
    }
}
