<?php

namespace common\models\blocks;

use common\models\Block;
use common\models\EntityToFile;

/**
 * Class RealtorBenefit
 *
 * @package common\models\blocks
 *
 * @property EntityToFile $image
 * @property string|null $imageSrc
 */
class RealtorBenefit extends Block
{
    /**
     * @var string saving attributes.
     */
    const ATTR_ITEM_IMAGE = 'realtorBenefitImage';

    public function getImage()
    {
        return $this->getEntityModel(self::ATTR_ITEM_IMAGE, 'realtorBenefitImage');
    }

    public function getImageSrc()
    {
        return $this->getEntityThumb('image', 'realtor', 'benefit');
    }
}
