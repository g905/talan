<?php

namespace common\models\blocks;

use common\models\Block;
use common\models\EntityToFile;

/**
 * Class RealtorPartner
 *
 * @property string $title partner name
 * @property string $sub_title partner address
 * @property string $description partner phone
 * @property string $link partner link
 * @property EntityToFile $image
 * @property string|null $imageSrc partner logo
 * @package common\models\blocks
 */
class RealtorPartner extends Block
{
    /**
     * @var string saving attributes.
     */
    const ATTR_ITEM_IMAGE = 'realtorPartnerImage';

    public function getImage()
    {
        return $this->getEntityModel(self::ATTR_ITEM_IMAGE, 'realtorPartnerImage');
    }

    public function getImageSrc()
    {
        return $this->getEntityThumb('image', 'realtor', 'partner');
    }
}
