<?php

namespace common\models\blocks;

use common\models\Block;
use common\models\EntityToFile;

/**
 * Class RealtorFile
 *
 * @property EntityToFile $fileModel
 * @property string|null $fileSrc
 * @package common\models\blocks
 */
class RealtorFile extends Block
{
    /**
     * @var string saving attributes.
     */
    const ATTR_ITEM_FILE = 'realtorFile';

    public function getFileModel()
    {
        return $this->getEntityModel(self::ATTR_ITEM_FILE, 'realtorFile');
    }

    public function getFileSrc()
    {
        return $this->getEntitySource('fileModel');
    }
}
