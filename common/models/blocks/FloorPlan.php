<?php

namespace common\models\blocks;

use common\models\Block;
use common\models\EntityToFile;

/**
 * Class BuildingAdvantage
 *
 * @package common\models\blocks
 *
 * @property EntityToFile $image
 * @property string|null $imageSrc
 */
class FloorPlan extends Block
{
    /**
     * @var string saving attributes.
     */
    const ATTR_ITEM_IMAGE = 'floorPlanImage';

    public function getImage()
    {
        return $this->getEntityModel(self::ATTR_ITEM_IMAGE, 'floorPlanImage');
    }

    public function getImageSrc()
    {
        return $this->getEntityThumb('image', 'Entrance', 'floorPlans');
    }
}
