<?php

namespace common\models\blocks;

use common\models\Block;
use common\models\EntityToFile;

/**
 * Class CareerReview
 *
 * @package common\models\blocks
 *
 * @property EntityToFile $image
 * @property string|null $imageSrc
 */
class CareerReview extends Block
{
    /**
     * @var string saving attributes.
     */
    const ATTR_ITEM_IMAGE = 'careerReviewImage';

    public function getImage()
    {
        return $this->getEntityModel(self::ATTR_ITEM_IMAGE, 'careerReviewImage');
    }

    public function getImageSrc()
    {
        return $this->getEntityThumb('image', 'career', 'review');
    }

    /**
     * @return string
     */
    public function getYoutubeCode(): ?string
    {
        if (!empty($this->youtube_link)) {
            $video = '';
            if (preg_match("/(www.youtube.com\/watch.v=+)([^=]+)/i", $this->youtube_link, $key)) {
                $video = "https://www.youtube.com/embed/$key[2]";
            } elseif (preg_match("/(youtu.be\/+)([^\/]+)/i", $this->youtube_link, $key)) {
                $video = "https://www.youtube.com/embed/$key[2]";
            } elseif (preg_match("/(www.youtube.com\/embed\/+)([^\/]+)/i", $this->youtube_link, $key)) {
                $video = "https://www.youtube.com/embed/$key[2]";
            }
            return $video;
        }
        return null;
    }
}
