<?php

namespace common\models\blocks;

use common\models\Block;
use common\models\EntityToFile;

/**
 * Class InstallmentPlanBenefitBlock
 *
 * @package common\models\blocks
 *
 * @property EntityToFile $image
 * @property string|null $imageSrc
 */
class InstallmentPlanBenefit extends Block
{
    /**
     * @var string saving attributes.
     */
    const ATTR_ITEM_IMAGE = 'installmentPlanBenefitImage';

    public function getImage()
    {
        return $this->getEntityModel(self::ATTR_ITEM_IMAGE, 'installmentPlanBenefitImage');
    }

    public function getImageSrc()
    {
        return $this->getEntityThumb('image', 'installmentplan', 'benefit');
    }
}
