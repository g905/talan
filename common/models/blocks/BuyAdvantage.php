<?php

namespace common\models\blocks;

use common\models\Block;
use common\models\EntityToFile;

/**
 * Class BuyAdvantage
 *
 * @package common\models\blocks
 *
 * @property EntityToFile $image
 * @property string|null $imageSrc
 */
class BuyAdvantage extends Block
{
    /**
     * @var string saving attributes.
     */
    const ATTR_ITEM_IMAGE = 'buyAdvantageImage';

    public function getImage()
    {
        return $this->getEntityModel(self::ATTR_ITEM_IMAGE, 'buyAdvantageImage');
    }

    public function getImageSrc()
    {
        return $this->getEntityThumb('image', 'complex', 'advantages');
    }
}
