<?php

namespace common\models\blocks;

use common\models\Block;
use common\models\EntityToFile;

/**
 * Class SpecializationBenefit
 *
 * @package common\models\blocks
 *
 * @property EntityToFile $image
 * @property string|null $imageSrc
 */
class SpecializationBenefit extends Block
{
    /**
     * @var string saving attributes.
     */
    const ATTR_ITEM_IMAGE = 'specializationBenefitImage';

    public function getImage()
    {
        return $this->getEntityModel(self::ATTR_ITEM_IMAGE, 'specializationBenefitImage');
    }

    public function getImageSrc()
    {
        return $this->getEntityThumb('image', 'specialization', 'benefit');
    }
}
