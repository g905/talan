<?php

namespace common\models\blocks;

use common\models\Block;
use common\models\EntityToFile;

/**
 * Class BuildingAdvantage
 *
 * @package common\models\blocks
 *
 * @property EntityToFile $image
 * @property string|null $imageSrc
 */
class BuildingAdvantage extends Block
{
    const SAVE_ATTRIBUTE_PHOTOS = 'AcAdvantagesPhoto';
    /**
     * @var string saving attributes.
     */
    const ATTR_ITEM_IMAGE = 'buildingAdvantageImage';

    public function getImage()
    {
        return $this->getEntityModel(self::ATTR_ITEM_IMAGE, 'buildingAdvantageImage');
    }

    public function getImageSrc()
    {
        return $this->getEntityThumb('image', 'complex', 'advantages');
    }

    public function getPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'photos.entity_model_name' => static::formName(),
                'photos.attribute' => static::SAVE_ATTRIBUTE_PHOTOS
            ])
            ->alias('photos')
            ->orderBy('photos.position DESC');
    }

}
