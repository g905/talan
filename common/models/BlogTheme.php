<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%blog_theme}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 */
class BlogTheme extends ActiveRecord 
{
    public static function tableName()
    {
        return '{{%blog_theme}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::class, ['id' => 'article_id'])
            ->viaTable(ThemeToArticle::tableName(), ['blog_theme_id' => 'id']);
    }
}
