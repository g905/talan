<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%bank_to_hypothec}}".
 *
 * @property integer $bank_id
 * @property integer $hypothec_id
 * @property integer $position
 *
 * @property Bank $bank
 * @property Hypothec $hypothec
 */
class BankToHypothec extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%bank_to_hypothec}}';
    }

    public function getBank()
    {
        return $this->hasOne(Bank::class, ['id' => 'bank_id']);
    }

    public function getHypothec()
    {
        return $this->hasOne(Hypothec::class, ['id' => 'hypothec_id']);
    }
}
