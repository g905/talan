<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%contact_form}}".
 *
 * @property integer $id
 * @property integer $contact_page_id
 * @property string $nav_label
 * @property string $form_label
 * @property string $form_description
 * @property string $form_agreement
 * @property integer $manager_id
 * @property integer $position
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Manager $manager
 */
class ContactForm extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%contact_form}}';
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }
}
