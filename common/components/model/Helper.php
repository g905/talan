<?php

namespace common\components\model;

use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\EntityToFile;

/**
 * Class Helper
 * @package common\components\model
 */
trait Helper
{
    /**
     * @var array stack for storing entities fetched from configuration models.
     */
    private $relatedConfigEntities = [];

    /**
     * In Model required definition like this:
     *
     *  const TYPE_ONE = 1;
     *  const TYPE_TWO = 2;
     *  protected static $type = [
     *      self::TYPE_ONE => 'Type one language key',
     *      self::TYPE_TWO => 'Type two language key',
     *  ];
     *
     * @param string $attribute the attribute name
     * @param string $category the translation category
     * @return array
     */
    public static function getList($attribute, $category = 'app')
    {
        static $labels = null;
        static $categoryName = null;

        if ($labels === null || $categoryName !== $category) {
            $labels = [];
            $categoryName = $category;
            if (is_array(static::${$attribute})) {
                foreach (static::${$attribute} as $key => $value) {
                    $labels[$key] = Yii::t($category, $value);
                }
            }
        }

        return $labels;
    }

    /**
     * Additional method for the {getList}
     *
     * @param string $attribute the attribute name
     * @param string $category the translation category
     * @return array
     */
    public function getListLabel($attribute, $category = 'app')
    {
        return isset(static::getList($attribute, $category)[$this->$attribute])
            ? static::getList($attribute, $category)[$this->$attribute]
            : Yii::t('app', 'Error get attribute value label');
    }

    public static function getItems($key = false, $label = false)
    {
        $class = get_called_class();
        /**
         * @var \yii\db\ActiveRecord $class
         */
        $class = new $class();
        if ($key === false) {
            $key = $class->primaryKey();
            if (count($key) !== 1) {
                return [];
            }
            $key = $key[0];
        }

        if ($label === false) {
            $label = $key;
            if (in_array('label', $class->attributes(), true)) {
                $label = 'label';
            }
        }

        $all = $class::find()->select([$key, $label])->asArray()->all();

        return ArrayHelper::map($all, $key, $label);
    }

    /**
     * Get image source of specified size section.
     * Note: this method must not be used with files.
     *
     * @param string $relationName name of image relation.
     * @param string $section image section name.
     * @param string $size image section size name.
     * @return bool|null|string
     */
    public function getEntityThumb($relationName, $section, $size)
    {
        return FPM::src(optional($this->$relationName)->file_id, $section, $size);
    }

    /**
     * Get file source of specified file relation.
     *
     * @param string $relationName name of file relation.
     * @return bool|null|string
     */
    public function getEntitySource($relationName)
    {
        return FPM::originalSrc(optional($this->$relationName)->file_id);
    }

    /**
     * Get config model image source of specified size section.
     * Note: this method must not be used with files.
     *
     * @param string|int $attribute entity attribute name.
     * @param string $section image section name.
     * @param string $size image section size name.
     * @return bool|null|string
     */
    public function getEntityConfigThumb($attribute, $section, $size)
    {
        $model = $this->getEntityConfigModel($attribute);

        return FPM::src(optional($model)->file_id, $section, $size);
    }

    /**
     * Get file source of specified config file model.
     *
     * @param string|int $attribute entity attribute name.
     * @return bool|null|string
     */
    public function getEntityConfigSource($attribute)
    {
        $model = $this->getEntityConfigModel($attribute);

        return FPM::originalSrc(optional($model)->file_id);
    }

    /**
     * Method that simplifies image/file model fetching.
     *
     * @param string|int $attribute entity attribute name.
     * @param null|string $alias query alias.
     * @param null|string $entityModelName entity model name, if null
     * [[formName()]] will be used to automatically determine it.
     * @param bool $hasMany whether to use hasMany relation type.
     * @return yii\db\ActiveQuery
     */
    public function getEntityModel($attribute, $alias = null, $entityModelName = null, $hasMany = false)
    {
        $relationType = $hasMany ? 'hasMany' : 'hasOne';
        $entityModelName = $entityModelName ?? $this->formName();
        $alias = $alias ?? strtolower($entityModelName) . '_al';

        return $this->$relationType(EntityToFile::class, ['entity_model_id' => 'id'])
            ->alias($alias)
            ->orderBy(["$alias.position" => SORT_DESC])
            ->andOnCondition(["$alias.entity_model_name" => $entityModelName, "$alias.attribute" => $attribute]);
    }

    /**
     * Method that simplifies image/file model fetching from configuration model.
     *
     * @param string|int $attribute entity attribute name.
     * @param bool $multiple whether to fetch multiple models.
     * @param null|string $entityModelName entity model name, if null
     * @return EntityToFile
     */
    public function getEntityConfigModel($attribute, $multiple = false, $entityModelName = null)
    {
        $value = obtain($attribute, $this->relatedConfigEntities, false);
        if ($value) {
            return $value;
        }
        $type = $multiple ? 'all' : 'one';
        $entityModelName = $entityModelName ?? $this->formName();
        $this->relatedConfigEntities[$attribute] = EntityToFile::find()->andWhere([
            'entity_model_id' => 1,
            'attribute' => $attribute,
            'entity_model_name' => $entityModelName,
        ])->$type();

        return obtain($attribute, $this->relatedConfigEntities);
    }

    /**
     * Additional method for the {getList}
     *
     * @param string $attribute the attribute name
     * @param string|boolean $category the translation category
     * @return array
     */
    public function getListValue($attribute, $category = 'app')
    {
        return isset(static::getList($attribute, $category)[$this->$attribute])
            ? static::getList($attribute, $category)[$this->$attribute]
            : Yii::t('app', 'Error get attribute value label');
    }

    /**
     * @param $route
     * @param $params
     * @param bool $scheme
     *
     * @return string
     */
    public static function createUrl($route, $params = [], $scheme = false)
    {
        return Url::to(
            ArrayHelper::merge(
                [$route],
                $params
            ),
            $scheme
        );
    }

    public function showDeleteButton()
    {
        return true;
    }

    public function showUpdateButton()
    {
        return true;
    }
    public function showCreateButton()
    {
        return true;
    }

}
