<?php
/**
 * Created by anatolii
 */

namespace common\components\model;


/**
 * Trait RelatedFormHelpTrait
 *
 * @package common\components\model
 */
trait RelatedFormHelpTrait
{
    /**
     * For RelatedFormWidget
     * @var array | null
     */
    public $relModels = null;

    /**
     * For RelatedFormWidget
     * @var int | null
     */
    public $relModelIndex = null;

    /**
     * For RelatedFormWidget
     * @return array
     */
    public function getRelatedFormConfig()
    {
        return [];
    }
}
