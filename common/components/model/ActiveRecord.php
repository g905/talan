<?php

namespace common\components\model;

use common\models\City;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ActiveRecord
 *
 * @property mixed $title
 * @property mixed $searchModel
 * @property array $formConfig
 *
 * @package common\components\model
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    /** @var int values of published attribute. */
    const IS_PUBLISHED = 1;
    const IS_UNPUBLISHED = 0;

    const DELETED = 1;
    const NOT_DELETED = 0;

    use Helper;
    use RelatedFormHelpTrait;

    /**
     * Find one published record by its identifier or throw not found exception.
     *
     * @param int $id record identifier.
     * @return null|\yii\db\ActiveRecord|static
     */
    public static function findOnePublishedOrFail($id)
    {
        return static::checkFail(
            static::find()
                ->where(['id' => $id, 'published' => self::IS_PUBLISHED])
                ->one()
        );
    }

    /**
     * Find one published record by its alias or throw not found exception.
     *
     * @param string $alias record alias.
     * @return null|\yii\db\ActiveRecord|static
     */
    public static function findOnePublishedByAliasOrFail($alias)
    {
        return static::checkFail(
            static::find()->where(['alias' => $alias, 'published' => self::IS_PUBLISHED])->one()
        );
    }

    /**
     * @inheritdoc
     * @return DefaultQuery
     */
    public static function find()
    {
        return new DefaultQuery(get_called_class());
    }

    public static function getAdditionalHeaderConfig(){
        return [];
    }

    /**
     * @return mixed
     */
    public function getLabelForUpdateAction()
    {
        switch (true) {
            case $this->hasAttribute('label') && $this->label:
                return $this->label;
            case $this->hasAttribute('name') && $this->name:
                return $this->name;
        }

        return $this->id;
    }

    /**
     * Check whether model is found otherwise an exception will be thrown.
     *
     * @param static|null|array|self $model
     * @throws NotFoundHttpException
     * @return static|static[]
     */
    protected static function checkFail($model)
    {
        if ($model === null || empty($model)){
            throw new NotFoundHttpException();
        }

        return $model;
    }

    /**
     * Increment record views count by one.
     *
     * @param string $attribute attribute to be updated (incremented).
     * @return bool
     */
    public function incrementViewsCount($attribute = 'view_count')
    {
        return $this->updateCounters([$attribute => 1]);
    }
}
