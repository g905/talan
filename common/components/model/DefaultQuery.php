<?php
/**
 * Author: metal
 * Email: metal
 */

namespace common\components\model;

use Cake\Chronos\Chronos;
use yii\db\ActiveQuery;

/**
 * Class DefaultQuery
 * @package common\models
 */
class DefaultQuery extends ActiveQuery
{
    public function filterByDateRange(string $attribute, ?string $value, string $format = 'Y-m-d H:i:s', string $separator = ' - ', string $tz = null)
    {
        if ($value) {
            $tz = $tz ?? app()->timeZone;
            [$start, $end] = explode($separator, $value);
            $this->filterByDateField('>=', $attribute, $start, $format, $tz);
            $this->filterByDateField('<=', $attribute, $end, $format, $tz);
        }

        return $this;
    }

    public function filterByDateField(string $operator, string $attribute, string $value, string $format, string $tz = null)
    {
        if ($value !== null) {
            $value = Chronos::createFromFormat($format, $value, $tz)->format('Y-m-d');
            $this->andFilterWhere([$operator, $attribute, $value]);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function isPublished()
    {
        $this->andWhere(['published' => 1]);

        return $this;
    }

    public function ordered()
    {
        $this->orderBy(['position' => SORT_ASC]);

        return $this;
    }
}
