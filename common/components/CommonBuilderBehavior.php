<?php
namespace common\components;

use common\components\model\ActiveQuery;
use common\components\model\ActiveRecord;
use common\models\BuilderWidget;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;

class CommonBuilderBehavior extends Behavior
{
    public $attributes = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'findContent',
        ];
    }

    public function findContent()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner;
        $className = $owner->formName();
        $id = $owner->getPrimaryKey();

        $locales = \common\helpers\LanguageHelper::getApplicationLanguages();

        $currentLocale = \Yii::$app->language;

        $defaultLocale = \common\helpers\LanguageHelper::getDefaultLanguage();
        $defaultLocale = \yii\helpers\ArrayHelper::getValue($defaultLocale, 'code');

        foreach ($this->attributes as $attribute) {
            $tableName = BuilderWidget::tableName();

            /** @var ActiveQuery $query */
            $query = BuilderWidget::find()
                ->where([
                    $tableName . '.target_class' => $className,
                    $tableName . '.target_id' => $id,
                    $tableName . '.target_attribute' => $attribute
                ])
                ->joinWith(['builderWidgetAttributes'])
                ->orderBy([
                    $tableName . '.position' => SORT_ASC,
                    $tableName . '.id' => SORT_ASC
                ]);

            /** @var BuilderWidget[] $widgets */
            $widgets = $query->all();

            $tmpWidgets = [];

            foreach ($widgets as $widget) {
                /** @var BuilderModel $widgetModel */
                $widgetModel = new $widget->widget_class;

                $tmpAttributes = [];

                foreach ($widget->builderWidgetAttributes as $builderWidgetAttribute) {
                    $tmpAttributes[$builderWidgetAttribute->attribute] = $builderWidgetAttribute->value;
                }

                if ($currentLocale != $defaultLocale) {
                    foreach ($tmpAttributes as $tmpAttr => $tmpVal) {
                        if (in_array($tmpAttr, $widgetModel->getLocalized())) {
                            $tmpLocAttr = $tmpAttr . '_' . $defaultLocale;

                            $tmpLocVal = ArrayHelper::getValue($tmpAttributes, $tmpLocAttr);

                            $tmpAttributes[$tmpAttr] = $tmpLocVal;
                        }
                    }
                }

                $tmpAttributes['id'] = $widget->id;
                $tmpAttributes['position'] = $widget->position;
                $tmpAttributes['target_attribute'] = $widget->target_attribute;
                $tmpAttributes['sign'] = \Yii::$app->security->generateRandomString();

                $widgetModel->setIsNewRecord(false);

                $widgetModel->setAttributes($tmpAttributes);

                $widgetModel->proccessFiles();

                $tmpWidgets[] = $widgetModel;
            }

            $this->owner->$attribute = $tmpWidgets;
        }
    }
}
