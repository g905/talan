<?php

namespace common\components;

class SxGeo extends \jisoft\sypexgeo\SxGeo
{
    const DB_FILE_NAME = 'SxGeoCity.dat';
    const DB_FILE_DIR = 'geoData';

    public function __construct($db_file = 'SxGeoCityMax.dat', $type = SXGEO_FILE){
        $filePath = \Yii::getAlias('@common').'/'.static::DB_FILE_DIR.'/'.static::DB_FILE_NAME;
        $this->fh = fopen($filePath, 'rb');
        // Сначала убеждаемся, что есть файл базы данных
        $header = fread($this->fh, 40); // В версии 2.2 заголовок увеличился на 8 байт
        if(substr($header, 0, 3) != 'SxG') die("Can't open {$db_file}\n");
        $info = unpack('Cver/Ntime/Ctype/Ccharset/Cb_idx_len/nm_idx_len/nrange/Ndb_items/Cid_len/nmax_region/nmax_city/Nregion_size/Ncity_size/nmax_country/Ncountry_size/npack_size', substr($header, 3));
        if($info['b_idx_len'] * $info['m_idx_len'] * $info['range'] * $info['db_items'] * $info['time'] * $info['id_len'] == 0) die("Wrong file format {$db_file}\n");
        $this->range       = $info['range'];
        $this->b_idx_len   = $info['b_idx_len'];
        $this->m_idx_len   = $info['m_idx_len'];
        $this->db_items    = $info['db_items'];
        $this->id_len      = $info['id_len'];
        $this->block_len   = 3 + $this->id_len;
        $this->max_region  = $info['max_region'];
        $this->max_city    = $info['max_city'];
        $this->max_country = $info['max_country'];
        $this->country_size= $info['country_size'];
        $this->batch_mode  = $type & SXGEO_BATCH;
        $this->memory_mode = $type & SXGEO_MEMORY;
        $this->pack        = $info['pack_size'] ? explode("\0", fread($this->fh, $info['pack_size'])) : '';
        $this->b_idx_str   = fread($this->fh, $info['b_idx_len'] * 4);
        $this->m_idx_str   = fread($this->fh, $info['m_idx_len'] * 4);

        $this->db_begin = ftell($this->fh);
        if ($this->batch_mode) {
            $this->b_idx_arr = array_values(unpack("N*", $this->b_idx_str)); // Быстрее в 5 раз, чем с циклом
            unset ($this->b_idx_str);
            $this->m_idx_arr = str_split($this->m_idx_str, 4); // Быстрее в 5 раз чем с циклом
            unset ($this->m_idx_str);
        }
        if ($this->memory_mode) {
            $this->db  = fread($this->fh, $this->db_items * $this->block_len);
            $this->regions_db = $info['region_size'] > 0 ? fread($this->fh, $info['region_size']) : '';
            $this->cities_db  = $info['city_size'] > 0 ? fread($this->fh, $info['city_size']) : '';
        }
        $this->info = $info;
        $this->info['regions_begin'] = $this->db_begin + $this->db_items * $this->block_len;
        $this->info['cities_begin']  = $this->info['regions_begin'] + $info['region_size'];
    }
}