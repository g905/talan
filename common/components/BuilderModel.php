<?php

namespace common\components;

use metalguardian\fileProcessor\models\File;
use yii\base\Behavior;
use yii\helpers\Inflector;
use yii\helpers\ArrayHelper;
use yii\base\InvalidCallException;
use yii\base\UnknownPropertyException;
use common\models\EntityToFile;
use common\models\BuilderWidget;
use common\components\model\Model;
use common\helpers\LanguageHelper;
use common\components\model\ActiveRecord;
use common\components\model\RelatedFormHelpTrait;
use backend\components\FormBuilder;
use backend\modules\builder\models\BuilderWidgetAttribute;

/**
 * Class BuilderModel
 *
 * @package backend\modules\builder\components
 */
abstract class BuilderModel extends Model
{
    use RelatedFormHelpTrait;

    public $id;

    /**
     * Temporary sign which used for saving images before model save
     *
     * @var string
     */
    public $sign;

    /**
     * Widget position
     *
     * @var int
     */
    public $position;

    /**
     * Widget visible
     *
     * @var int
     */
    public $widget_visible;

    /**
     * Widget Attribute
     *
     * @var int
     */
    public $target_attribute;

    /**
     * @var array attribute values indexed by attribute names
     */
    private $_attributes = [];

    /**
     * @var array|null old attribute values indexed by attribute names.
     * This is `null` if the record [[isNewRecord|is new]].
     */
    private $_oldAttributes;

    private $_localizedAttributes = [];

    /**
     * @var Behavior[]|null the attached behaviors (behavior name => behavior). This is `null` when not initialized.
     */
    private $_behaviors = [];

    /**
     * Return name of widget
     *
     * @return string;
     */
    abstract public function getName();

    /**
     * Return field config
     *
     * @return array
     */
    abstract public function getConfig();

    /**
     * Return localized attributes
     *
     * @return array
     */
    abstract public function getLocalized();

    /**
     * Init model
     */
    public function init()
    {
        parent::init();

        $locales = LanguageHelper::getApplicationLanguages();
        $defaultLocale = LanguageHelper::getDefaultLanguage();
        $defaultLocale = ArrayHelper::getValue($defaultLocale, 'locale');

        $localized = $this->getLocalized();

        foreach ($localized as $localeAttributes) {
            foreach ($locales as $locale) {
                if ($locale != $defaultLocale) {
                    $this->_localizedAttributes["{$localeAttributes}_{$locale}"] = null;
                }
            }
        }
    }

    /**
     * Returns a value indicating whether the current record is new.
     *
     * @return bool whether the record is new and should be inserted when calling [[save()]].
     */
    public function getIsNewRecord()
    {
        return $this->_oldAttributes === null;
    }

    /**
     * Sets the value indicating whether the record is new.
     *
     * @param bool $value whether the record is new and should be inserted when calling [[save()]].
     *
     * @see getIsNewRecord()
     */
    public function setIsNewRecord($value)
    {
        $this->_oldAttributes = $value ? null : $this->_attributes;
    }

    /**
     * Return default & localized validation rules
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            [['id', 'sign', 'position', 'target_attribute', 'widget_visible'], 'safe']
        ];

        $rules[] = [array_keys($this->_localizedAttributes), 'safe'];

        return $rules;
    }

    /**
     * Return prepared validation rules
     *
     * @param $rules
     *
     * @return array
     */
    protected function prepareRules($rules)
    {
        foreach ($rules as $rule) {
            $attribute = $rule[0];

            $localizedAttributes = $this->_localizedAttributes;

            foreach ($localizedAttributes as $localizedAttribute => $tmpV) {
                $tmpRule = $rule;

                $original = explode('_', $localizedAttribute);
                array_pop($original);
                $original = implode('_', $original);

                if (is_array($attribute)) {
                    if (in_array($original, $attribute)) {
                        $tmpRule[0] = [$localizedAttribute];
                    }
                } else {
                    if ($original === $attribute) {
                        $tmpRule[0] = [$localizedAttribute];
                    }
                }

                $rules[] = $tmpRule;
            }
        }

        return $rules;
    }

    /**
     * Return prepared labels
     *
     * @param $labels
     *
     * @return mixed
     */
    protected function prepareLabels($labels)
    {
        $localizedAttributes = $this->_localizedAttributes;

        foreach ($localizedAttributes as $localizedAttribute => $tmpV) {
            if (!isset($labels[$localizedAttribute])) {
                $original = explode('_', $localizedAttribute);
                $locale = array_pop($original);
                $original = implode('_', $original);

                $label = ArrayHelper::getValue($labels, $original);

                $labels[$localizedAttribute] = $label . ' ' . Inflector::camel2words($locale);
            }
        }

        return $labels;
    }

    /**
     * Default field config
     *
     * @return array
     */
    public function getDefaultConfig()
    {
        return [
            'id' => [
                'type' => FormBuilder::INPUT_HIDDEN,
                'fieldOptions' => ['options' => ['tag' => false], 'template' => '{input}'],
            ],
            'sign' => [
                'type' => FormBuilder::INPUT_HIDDEN,
                'fieldOptions' => ['options' => ['tag' => false], 'template' => '{input}'],
            ],
            'target_attribute' => [
                'type' => FormBuilder::INPUT_HIDDEN,
                'fieldOptions' => ['options' => ['tag' => false], 'template' => '{input}'],
            ],
            'widget_visible' => [
                'type' => FormBuilder::INPUT_HIDDEN,
                'fieldOptions' => ['options' => ['tag' => false], 'template' => '{input}'],
                'options' => ['class' => 'widget_visible'],
            ],
        ];
    }

    /**
     * List of default models to an exception from save data
     *
     * @return array
     */
    public function getDefaultAttributes()
    {
        return [
            'id',
            'sign',
            'position',
            'target_attribute',
            'relModels',
            'relModelIndex',
            'widget_visible'
        ];
    }

    /**
     * Save widget data
     *
     * @param ActiveRecord $target
     *
     * @return bool|int
     */
    public function save(ActiveRecord $target)
    {
        $attributes = $this->getAttributes();

        foreach ($this->getDefaultAttributes() as $default) {
            ArrayHelper::remove($attributes, $default);
        }

        if ($this->id == '' && is_null($this->id)) {
            $builderWidget = new BuilderWidget();
        } else {
            /** @var BuilderWidget $builderWidget */
            $builderWidget = BuilderWidget::findOne($this->id);

            if (!$builderWidget) {
                $builderWidget = new BuilderWidget();
            }
        }

        $builderWidget->target_class = $target->formName();
        $builderWidget->target_id = $target->getPrimaryKey();
        $builderWidget->target_attribute = $this->target_attribute;
        $builderWidget->target_sign = $this->sign;

        $builderWidget->widget_class = $this::className();

        $builderWidget->position = $this->position;
        $builderWidget->widget_visible = ($this->widget_visible) ? 1 : 0;

        if ($builderWidget->save()) {
            $valid = true;

            EntityToFile::updateImages($builderWidget->id, $builderWidget->target_sign);

            foreach ($attributes as $attribute => $value) {
                $attributeModel = BuilderWidgetAttribute::findOne([
                    'widget_id' => $builderWidget->id,
                    'attribute' => $attribute
                ]);

                if (!$attributeModel) {
                    $attributeModel = new BuilderWidgetAttribute();

                    $attributeModel->widget_id = $builderWidget->id;
                    $attributeModel->attribute = $attribute;
                }

                $attributeModel->value = $value;

                $valid = $attributeModel->save() && $valid;
            }

            if ($valid) {
                return $builderWidget->id;
            }
        }

        return false;
    }

    /**
     * @param string $name
     *
     * @return mixed
     * @throws UnknownPropertyException
     */
    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            // read property, e.g. getName()
            return $this->$getter();
        }

        // behavior property
        $this->ensureBehaviors();
        foreach ($this->_behaviors as $behavior) {
            if ($behavior->canGetProperty($name)) {
                return $behavior->$name;
            }
        }

        if (method_exists($this, 'set' . $name)) {
            throw new InvalidCallException('Getting write-only property: ' . get_class($this) . '::' . $name);
        }

        if (array_key_exists($name, $this->_localizedAttributes)) {
            return ArrayHelper::getValue($this->_localizedAttributes, $name);
        }

        throw new UnknownPropertyException('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

    /**
     * @param string $name
     * @param mixed $value
     *
     * @throws UnknownPropertyException
     */
    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            // set property
            $this->$setter($value);

            return;
        } elseif (strncmp($name, 'on ', 3) === 0) {
            // on event: attach event handler
            $this->on(trim(substr($name, 3)), $value);

            return;
        } elseif (strncmp($name, 'as ', 3) === 0) {
            // as behavior: attach behavior
            $name = trim(substr($name, 3));
            $this->attachBehavior($name, $value instanceof Behavior ? $value : Yii::createObject($value));

            return;
        }

        // behavior property
        $this->ensureBehaviors();
        foreach ($this->_behaviors as $behavior) {
            if ($behavior->canSetProperty($name)) {
                $behavior->$name = $value;
                return;
            }
        }

        if (method_exists($this, 'get' . $name)) {
            throw new InvalidCallException('Setting read-only property: ' . get_class($this) . '::' . $name);
        }

        if (array_key_exists($name, $this->_localizedAttributes)) {
            $this->_localizedAttributes[$name] = $value;
            return;
        }

        throw new UnknownPropertyException('Setting unknown property: ' . get_class($this) . '::' . $name);
    }

    /**
     * Returns attribute values.
     *
     * @param array $names list of attributes whose value needs to be returned.
     * Defaults to null, meaning all attributes listed in [[attributes()]] will be returned.
     * If it is an array, only the attributes in the array will be returned.
     * @param array $except list of attributes whose value should NOT be returned.
     *
     * @return array attribute values (name => value).
     */
    public function getAttributes($names = null, $except = [])
    {
        $values = [];
        if ($names === null) {
            $names = $this->attributes();
        }
        foreach ($names as $name) {
            $values[$name] = $this->$name;
        }
        foreach ($this->_localizedAttributes as $attr => $value) {
            $values[$attr] = $value;
        }
        foreach ($except as $name) {
            unset($values[$name]);
        }

        return $values;
    }

    /**
     * Sets the attribute values in a massive way.
     *
     * @param array $values attribute values (name => value) to be assigned to the model.
     * @param bool $safeOnly whether the assignments should only be done to the safe attributes.
     * A safe attribute is one that is associated with a validation rule in the current [[scenario]].
     *
     * @see safeAttributes()
     * @see attributes()
     */
    public function setAttributes($values, $safeOnly = true)
    {
        if (is_array($values)) {
            $attributes = array_flip($safeOnly ? $this->safeAttributes() : $this->attributes());
            foreach ($values as $name => $value) {
                if (isset($attributes[$name])) {
                    $this->$name = $value;
                } elseif (array_key_exists($name, $this->_localizedAttributes)) {
                    $this->_localizedAttributes[$name] = $value;
                } elseif ($safeOnly) {
                    $this->onUnsafeAttribute($name, $value);
                }
            }
        }
    }

    /**
     * Attach [[File]] to model ( use on frontend )
     */
    public function proccessFiles()
    {
        $entity = EntityToFile::find()->where([
            'entity_model_name' => static::formName(),
            'entity_model_id' => $this->id
        ])->orderBy(['position' => SORT_DESC])->all();

        /** @var File[][] $files */
        $files = ArrayHelper::map($entity, 'id', function ($data) {
            return ($data->file_id) ? $data->file : null;
        }, 'attribute');

        $this->setAttributes($files, false);
    }

    /**
     * Attach translations ( use on frontend )
     */
    public function proccessTranslation()
    {
        $locale = \Yii::$app->language;
        $defaultLocale = LanguageHelper::getDefaultLanguage();
        $defaultLocale = ArrayHelper::getValue($defaultLocale, 'locale');

        if ($locale != $defaultLocale) {
            foreach ($this->attributes as $attribute => $value) {
                if (in_array($attribute, $this->getLocalized())) {
                    $localeAttr = "{$attribute}_{$locale}";

                    $this->{$attribute} = $this->{$localeAttr};
                }
            }
        }
    }

    /***
     * @param $attribute string
     * @return mixed
     */
    public function getFirstFile($attribute = 'image')
    {
        if (is_array($this->$attribute)) {
            foreach ($this->$attribute as $item) {
                return $item; // return only first
            }
        }
    }
}
