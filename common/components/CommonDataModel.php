<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 07.01.17
 * Time: 2:58
 */

namespace common\components;

use Yii;

class CommonDataModel
{
    /**
     * Common currency text for centralized storing
     *
     * @return string
     */
    public static function getCurrencyText()
    {
        return Yii::t('common/data', 'rub');
    }

    /**
     * Common area unit for centralized storing
     *
     * @return string
     */
    public static function getAreaUnit()
    {
        return Yii::t('common/data', 'm<sup>2</sup>');
    }

    /**
     * Common month text for centralized storing
     *
     * @return string
     */
    public static function getMonthsText()
    {
        return Yii::t('common/data', 'month(s)');
    }

    /**
     * Common percent text for centralized storing
     *
     * @return string
     */
    public static function getPercentText()
    {
        return Yii::t('common/data', '%');
    }


}
