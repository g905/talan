<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 14.12.2017
 * Time: 14:32
 */

namespace common\components;

class Sypexgeo extends \jisoft\sypexgeo\Sypexgeo
{
    /**
     * Creates SxGeo instance.
     * @return \SxGeo instance.
     */
    protected function createSxGeo()
    {
        return new SxGeo();
    }
}