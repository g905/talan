<?php

use metalguardian\fileProcessor\helpers\FPM;

return [
    /*'module' => [
        'size' => [
            'action' => 'frame',
            'width' => 400,
            'height' => 200,
            'startX' => 100,
            'startY' => 100,
        ],
    ],*/
    'admin' => [
        'file' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 200,
            'height' => 200,
        ],
        'fileAdaptive' => [
            'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
            'width' => 100,
            'height' => 100,
        ],
    ],
    'builder' => [
        'preview' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 800,
            'height' => 600,
        ],
        'doubleimage' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 360,
            'height' => 260,
        ],
        'slider' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 750,
            'height' => 450,
        ],
        'youtube' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 750,
            'height' => 400,
        ],
    ],
    'home' => [
        'aboutCompany' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1540,
            'height' => 960,
        ],
        'howWeBuild' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1540,
            'height' => 960,
        ],
        'review' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1540,
            'height' => 910,
        ],
        'complex' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1280,
            'height' => 900,
        ],
        'complexnew' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 550,
            'height' => 406,
        ],
    ],
    'about' => [
        'map' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1828,
            'height' => 1080,
        ],
        'value' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 640,
            'height' => 380,
        ],
        'ceo' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1500,
            'height' => 910,
        ],
    ],
    'complex' => [
        'gallery' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1700,
            'height' => 910,
        ],
        'advantagesTop' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1500,
            'height' => 910,
        ],
        'advantages' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1080,
            'height' => 610,
        ],
        'publicService' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 2880,
            'height' => 1050,
        ],
        'architecture' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1500,
            'height' => 940,
        ],
        'architectureBottom' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 2880,
            'height' => 1070,
        ],
        'action' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 720,
            'height' => 726,
        ],
        'manager' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 940,
            'height' => 588,
        ],
        'apartmentList' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 524,
            'height' => 390,
        ],
        'complexList' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 550,
            'height' => 405,
        ],
        'historyslide' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 607,
            'height' => 494,
        ],
    ],
    'promoAction' => [
        'viewMain' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1416,
            'height' => 888,
        ],
    ],
    'apartment' => [
        'gallery' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1236,
            'height' => 970,
        ],
        'galleryThumb' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 290,
            'height' => 180,
        ],
        'description' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1500,
            'height' => 940,
        ],
        'floorPlan' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 2560,
            'height' => 700,
        ],
        'plan' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 200,
            'height' => 175,
        ],
    ],
    'article' => [
        'itemfull' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 750,
            'height' => 540,
        ],
        'itemthird' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 360,
            'height' => 260,
        ],
        'preview' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 360,
            'height' => 260,
        ],
        'view' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 750,
            'height' => 470,
        ],
        'header' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1600,
            'height' => 680,
        ]
    ],
    'completedProject' => [
        'photoPreview' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 370,
            'height' => 217,
        ],
    ],
    'installmentplan' => [
        'preview' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 845,
            'height' => 450,
        ],
        'benefit' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 320,
            'height' => 190,
        ],
    ],
    'hypothec' => [
        'preview' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 845,
            'height' => 450,
        ],
        'benefit' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 320,
            'height' => 190,
        ],
        'mainbenefit' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 765,
            'height' => 415,
        ],
    ],
    'team' => [
        'member' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 327,
            'height' => 338,
        ],
        'pagebackground' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1920,
            'height' => 1080,
        ],
    ],
    'career' => [
        'topimage' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1280,
            'height' => 675,
        ],
        'directorphoto' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 750,
            'height' => 455,
        ],
        'review' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 750,
            'height' => 455,
        ],
    ],
    'specialization' => [
        'preview' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 486,
            'height' => 341,
        ],
        'topimage' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1280,
            'height' => 675,
        ],
        'gallery' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1000,
            'height' => 788,
        ],
        'benefit' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 320,
            'height' => 190,
        ],
        'principle' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 320,
            'height' => 190,
        ],
        'review' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 750,
            'height' => 455,
        ],
    ],
    'vacancy' => [
        'principle' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 320,
            'height' => 190,
        ],
    ],
    'resale' => [
        'pagebackground' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1920,
            'height' => 1080,
        ],
        'condition' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 320,
            'height' => 190,
        ],
        'apartment' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 295,
            'height' => 182,
        ]
    ],
    'building' => [
        'list' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 550,
            'height' => 405,
        ],
    ],
    'realtor' => [
        'top' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 1000,
            'height' => 480,
        ],
        'app1' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 220,
            'height' => 460,
        ],
        'app2' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 288,
            'height' => 505,
        ],
        'benefit' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 320,
            'height' => 190,
        ],
        'partner' => [
            'action' => FPM::ACTION_THUMBNAIL,
            'width' => 280,
            'height' => 190,
        ],
    ],
];
