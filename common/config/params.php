<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'baseUrl' => 'talan',
    'baseFrontUrl' => 'xn--80aa6ajv.xn--p1ai',
    'protocol' => 'http',
];
