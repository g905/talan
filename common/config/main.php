<?php

$config =  [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    // 'catchAll' => ['site/maintenance'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'modules' => [
        'fileProcessor' => [
            'class' => '\metalguardian\fileProcessor\Module',
            //Alert! all sizes in x2 - for retina
            'imageSections' => require 'fpm-image-sections.php',
        ],
    ],
    'components' => [
        'errorHandler' => [
            'class' => common\components\ErrorHandler::class,
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'formatter' => [
            'timeFormat' => 'H:i:s',
            'dateFormat' => 'Y-m-d',
        ],
        'i18n' => [
            'class' => vintage\i18n\components\I18N::class,
            'enableCaching' => false, //YII_ENV === 'prod',
            'cache' => 'i18nCache',
        ],
        'i18nCache' => [
            'class' => \yii\caching\FileCache::class,
            'cachePath' => '@frontend/runtime/cache',
        ],
        'urlManager' => [
            'class' => common\components\UrlManager::class,
            'showScriptName' => false,
            //'enableStrictParsing' => true,
            'enableLanguageDetection' => false,
            'normalizer' => [
                'class' => yii\web\UrlNormalizer::class,
                'collapseSlashes' => true,
                'normalizeTrailingSlash' => true,
            ],
            'rules' => [],
            'enablePrettyUrl' => true
        ],
/*
        'postman' => [
            'class' => 'rmrevin\yii\postman\Component',
            'driver' => 'smtp',
            'default_from' => ['noreply@somehost.com', 'Mailer'],
            'subject_prefix' => 'Sitename / ',
            'subject_suffix' => null,
            'table' => '{{%postman_letter}}',
            'view_path' => '/email',
            'smtp_config' => [
                'host' => 'smtp.domain.com',
                'port' => 465,
                'auth' => true,
                'user' => 'email@domain.cpom',
                'password' => 'password',
                'secure' => 'ssl',
                'debug' => false,
            ]
        ],
*/
    ],
    'sourceLanguage' => 'xx',
    'language' => 'en',
];

// configuration adjustments for YII_DEBUG = true
if (YII_DEBUG ) {

    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*']
    ];
}
// configuration adjustments for develop mode
if (YII_ENV == 'dev') {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*']
    ];
}

return $config;