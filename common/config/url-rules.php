<?php

use common\helpers\Property;

$config= (merge(include __DIR__.'/params.php', include __DIR__.'/params-local.php'));
$domain = $config['baseUrl'];

return [

    "//<city>.$domain" => "page/page/home",
    "//<city>.$domain/about" => "page/page/about",
    "//<city>.$domain/catalog" => "page/page/catalog",

    "//<city>.$domain/installment-plan" => 'page/page/installment-plan',
    "//<city>.$domain/hypothec" => 'page/page/hypothec',
    "//<city>.$domain/land-purchase" => 'page/page/earth',
    "//<city>.$domain/secondary-housing" => 'page/page/buy',
    "//<city>.$domain/how-to-buy" => 'page/page/how-buy',
    "//<city>.$domain/contact" => 'page/page/contact',
    "//<city>.$domain/guarantee" => 'page/page/guarantee',
    "//<city>.$domain/realtors" => 'page/page/realtor',
    "//<city>.$domain/public-data/<alias>" => 'page/page/public-data',
    "//<city>.$domain/public-data" => 'page/page/public-data',
    "//<city>.$domain/get-chart" => 'page/page/get-chart',

    "//<city>.$domain/team" => 'team/team/index',
    "//<city>.$domain/team/members" => 'team/team/city-items',

    "//<city>.$domain/agents" => 'agents/agents/index',

    "//<city>.$domain/partners" => 'partner/partner/index',

    "//<city>.$domain/apartment-complex/stream" => 'apartment/apartment-complex/stream',

    // "//<city>.$domain/apartment-complex/list" => "apartment/apartment-complex/list",
    // "//<city>.$domain/apartment-complex/<alias>" => "apartment/apartment-complex/view",
    // "//<city>.$domain/apartment-complex/<alias>/apartments" => "apartment/apartment-complex/apartments-list",
    // "//<city>.$domain/apartment-complex/<alias>/apartments/<id>" => "apartment/apartment/view",

    // Apartment complex list
    ['pattern' => "//<city>.$domain/apartment-complex/list", 'route' => 'apartment/apartment-complex/list', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => "//<city>.$domain/commercial-complex/list", 'route' => 'apartment/apartment-complex/list', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Secondary real estate page
    ['pattern' => "//<city>.$domain/apartment-complex/resale", 'route' => 'resale/secondary-real-estate/index', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => "//<city>.$domain/commercial-complex/resale", 'route' => 'resale/secondary-real-estate/index', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Apartments complex
    ['pattern' => "//<city>.$domain/apartment-complex/<alias>", 'route' => 'apartment/apartment-complex/view', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => "//<city>.$domain/complete-complex/<alias>", 'route' => 'apartment/apartment-complex/view', 'defaults' => ['type' => Property::TYPE_UNDEFINED]],
    ['pattern' => "//<city>.$domain/commercial-complex/<alias>", 'route' => 'apartment/apartment-complex/view', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // ajax receiving history json data
    ['pattern' => "//<city>.$domain/ajax/apartment-complex/history/<alias>", 'route' => 'apartment/apartment-complex/apartment-history-info'],
    ['pattern' => "//<city>.$domain/ajax/apartment-building/history/<alias>", 'route' => 'apartment/apartment-building/apartment-history-info'],
    // apartment complex history page
    ['pattern' => "//<city>.$domain/apartment-complex/<alias>/timeline", 'route' => 'apartment/apartment-complex/history', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => "//<city>.$domain/commercial-complex/<alias>/timeline", 'route' => 'apartment/apartment-complex/history', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Apartments list
    ['pattern' => "//<city>.$domain/apartment-complex/<alias>/apartments", 'route' => 'apartment/apartment-complex/apartments-list', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => "//<city>.$domain/commercial-complex/<alias>/apartments", 'route' => 'apartment/apartment-complex/apartments-list', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Apartments configurator
    ['pattern' => "//<city>.$domain/apartment-complex/<alias>/choose-house", 'route' => 'apartment/apartment-complex/apartments-configurator', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => "//<city>.$domain/commercial-complex/<alias>/choose-house", 'route' => 'apartment/apartment-complex/apartments-configurator', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Apartments list
    ['pattern' => "//<city>.$domain/apartment-complex/<alias>/filter", 'route' => 'apartment/apartment-complex/filter', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => "//<city>.$domain/commercial-complex/<alias>/filter", 'route' => 'apartment/apartment-complex/filter', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Apartment
    ['pattern' => "//<city>.$domain/apartment-complex/<alias>/apartments/<id>", 'route' => 'apartment/apartment/view', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => "//<city>.$domain/commercial-complex/<alias>/apartments/<id>", 'route' => 'apartment/apartment/view', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Apartments building
    ['pattern' => "//<city>.$domain/apartment-complex/<alias>/<id>", 'route' => 'apartment/apartment-building/view'],
    ['pattern' => "//<city>.$domain/apartment-complex/<alias>/<id>/apartments", 'route' => 'page/page/building-apartments'],
    ['pattern' => "//<city>.$domain/apartment-complex/<alias>/<id>/apartments/<entrance>", 'route' => 'page/page/building-apartments'],

    ['pattern' => "//<city>.$domain/chat-apartments-list", 'route' => 'apartment/apartment-complex/chat-apartments-list'],

    "//<city>.$domain/promo-action/<id>" => "apartment/promo-action/view",
    "//<city>.$domain/how-buy/<id>" => "apartment/how-buy-block/view",
    "//<city>.$domain/form-question" => "form/form/form-question",
    "//<city>.$domain/form-question-validate" => "form/form/form-question-validate",
    "//<city>.$domain/form-callback-popup" => "form/form/form-callback-popup",
    "//<city>.$domain/form-commercial-popup" => "form/form/form-commercial-popup",
    "//<city>.$domain/form-excursion-popup" => "form/form/form-excursion-popup",
    "//<city>.$domain/form-complex-presentation" => "form/form/form-complex-presentation",
    "//<city>.$domain/form-complex-presentation-validate" => "form/form/form-complex-presentation-validate",
    "//<city>.$domain/form-guarantee" => "form/form/form-guarantee",
    "//<city>.$domain/form-subscribe" => "form/form/subscribe-form-submit",
    "//<city>.$domain/form-subscribe-bottom" => "form/form/subscribe-bottom-form-submit",
    "//<city>.$domain/custom-popup-request-submit" => "form/form/custom-popup-request-submit",
    "//<city>.$domain/form-custom-guarantee" => "form/form/form-custom-guarantee",
    "//<city>.$domain/public-data-download-file/<id>" => 'form/form/public-data-download-file',
    "//<city>.$domain/agents-show-phone-popup/<id>" => 'form/form/agents-show-phone-popup',
    "//<city>.$domain/agents-take-part-form" => 'form/form/agents-take-part-form',
    "//<city>.$domain/agents-video-popup" => 'form/form/agents-video-popup',
    "//<city>.$domain/geolocation" => 'site/geolocation',
    "//<city>.$domain/show-map" => 'page/page/show-map',

    "//<city>.$domain/page/<alias>" => "page/page/view",
    "//<city>.$domain/journal" => 'blog/blog/index',
    "//<city>.$domain/journal/<alias>" => 'blog/blog/view',

    "//<city>.$domain/career" => 'hr/career/view',
    "//<city>.$domain/specialization/<id>" => 'hr/specialization/view',
    "//<city>.$domain/vacancy/<alias>" => 'hr/vacancy/view',
    "//<city>.$domain/vacancies/<vacancy-city>/<specialization>" => 'hr/vacancy/index',
    "//<city>.$domain/vacancies" => 'hr/vacancy/index',
    "//<city>.$domain/constructor-submit" => 'apartment/constructor/form-submit',
    "//<city>.$domain/apartment/constructor/index.json" => 'apartment/constructor/index',
    "//<city>.$domain/grab/<name>" => 'grabber/grabber/index',
    "//<city>.$domain/construction/<timestamp>" => 'construction/construction/index',

    "//<city>.$domain/<module>/<controller>/<action>" => "<module>/<controller>/<action>",
    "//<city>.$domain/<controller>/<action>" => "<controller>/<action>",

    '' => 'page/page/home',
    '/about' => 'page/page/about',
    '/catalog' => 'page/page/catalog',
    '/team' => 'team/team/index',
    '/team/members' => 'team/team/city-items',
    '/agents' => 'agents/agents/index',
    '/partners' => 'partner/partner/index',
    '/land-purchase' => 'page/page/earth',
    '/secondary-housing' => 'page/page/buy',
    '/how-to-buy' => 'page/page/how-buy',
    '/installment-plan' => 'page/page/installment-plan',
    '/hypothec' => 'page/page/hypothec',
    '/contact' => 'page/page/contact',
    '/guarantee/<alias>' => 'page/page/guarantee',
    "/realtors" => 'page/page/realtor',
    "/public-data/<alias>" => 'page/page/public-data',
    "/public-data" => 'page/page/public-data',
    "/get-chart" => 'page/page/get-chart',
    "/get-schedule-chart" => 'page/page/get-schedule-chart',
    "/get-rating" => 'page/page/get-rating',

    // Apartment complex list
    ['pattern' => '/apartment-complex/list', 'route' => 'apartment/apartment-complex/list', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => '/commercial-complex/list', 'route' => 'apartment/apartment-complex/list', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Secondary real estate page
    ['pattern' => '/apartment-complex/resale', 'route' => 'resale/secondary-real-estate/index', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => '/commercial-complex/resale', 'route' => 'resale/secondary-real-estate/index', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Apartments complex
    ['pattern' => '/apartment-complex/<alias>', 'route' => 'apartment/apartment-complex/view', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => '/complete-complex/<alias>', 'route' => 'apartment/apartment-complex/view', 'defaults' => ['type' => Property::TYPE_UNDEFINED]],
    ['pattern' => '/commercial-complex/<alias>', 'route' => 'apartment/apartment-complex/view', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    //
    ['pattern' => '/ajax/apartment-complex/history/<alias>', 'route' => 'apartment/apartment-complex/apartment-history-info'],
    ['pattern' => '/ajax/apartment-building/history/<alias>', 'route' => 'apartment/apartment-building/apartment-history-info'],
    // apartment complex history page
    ['pattern' => "/apartment-complex/<alias>/timeline", 'route' => 'apartment/apartment-complex/history', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => "/commercial-complex/<alias>/timeline", 'route' => 'apartment/apartment-complex/history', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Apartments list
    ['pattern' => '/apartment-complex/<alias>/apartments', 'route' => 'apartment/apartment-complex/apartments-list', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => '/commercial-complex/<alias>/apartments', 'route' => 'apartment/apartment-complex/apartments-list', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Apartments configurator
    ['pattern' => "/apartment-complex/<alias>/choose-house", 'route' => 'apartment/apartment-complex/apartments-configurator', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => "/commercial-complex/<alias>/choose-house", 'route' => 'apartment/apartment-complex/apartments-configurator', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Apartments list
    ['pattern' => '/apartment-complex/<alias>/filter', 'route' => 'apartment/apartment-complex/filter', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => '/commercial-complex/<alias>/filter', 'route' => 'apartment/apartment-complex/filter', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Apartment
    ['pattern' => '/apartment-complex/<alias>/apartments/<id>', 'route' => 'apartment/apartment/view', 'defaults' => ['type' => Property::TYPE_RESIDENTIAL]],
    ['pattern' => '/commercial-complex/<alias>/apartments/<id>', 'route' => 'apartment/apartment/view', 'defaults' => ['type' => Property::TYPE_COMMERCIAL]],
    // Apartments building
    ['pattern' => "/apartment-complex/<alias>/<id>", 'route' => 'apartment/apartment-building/view'],
    ['pattern' => "/apartment-complex/<alias>/<id>/apartments", 'route' => 'page/page/building-apartments'],
    ['pattern' => "/apartment-complex/<alias>/<id>/apartments/<entrance>", 'route' => 'page/page/building-apartments'],
    ['pattern' => "/chat-apartments-list", 'route' => 'page/page/chat-apartments-list'],

    '/promo-action/<id>' => 'apartment/promo-action/view',
    '/how-buy/<id>' => 'apartment/how-buy-block/view',
    '/form-question' => 'form/form/form-question',
    '/form-question-validate' => 'form/form/form-question-validate',
    '/form-callback-popup' => 'form/form/form-callback-popup',
    '/form-partner-popup' => 'form/form/form-partner-popup',
    '/form-commercial-popup' => 'form/form/form-commercial-popup',
    '/form-excursion-popup' => 'form/form/form-excursion-popup',
    '/form-complex-presentation' => 'form/form/form-complex-presentation',
    '/form-complex-presentation-validate' => 'form/form/form-complex-presentation-validate',
    '/form-guarantee' => 'form/form/form-guarantee',
    '/form-guarantee-validate' => 'form/form/form-guarantee-validate',
    '/form-custom-guarantee' => 'form/form/form-custom-guarantee',
    '/form-custom-guarantee-validate' => 'form/form/form-custom-guarantee-validate',
    '/form-subscribe' => 'form/form/subscribe-form-submit',
    '/form-subscribe-bottom' => 'form/form/subscribe-bottom-form-submit',
	'/form-asc-about-discount' => 'form/form/form-asc-about-discount',
    '/custom-popup-request-submit' => 'form/form/custom-popup-request-submit',
    '/public-data-download-file/<id>' => 'form/form/public-data-download-file',
    '/geolocation' => 'site/geolocation',
    '/show-map/<map>' => 'page/page/show-map',

    '/agents-take-part-form' => 'form/form/agents-take-part-form',
    "/agents-video-popup" => 'form/form/agents-video-popup',

    '/page/<alias>' => 'page/page/view',
    '/journal' => 'blog/blog/index',
    '/journal/<alias>' => 'blog/blog/view',

    '/career' => 'hr/career/view',
    '/specialization/<id>' => 'hr/specialization/view',
    '/vacancy/<alias>' => 'hr/vacancy/view',
    '/vacancies/<vacancy-city>/<specialization>' => 'hr/vacancy/index',
    '/vacancies' => 'hr/vacancy/index',
    '/constructor-submit' => 'apartment/constructor/form-submit',
    '/apartment/constructor/index.json' => 'apartment/constructor/index',

    'robots.txt' => 'site/robots',
    'sitemap.xml' => 'sitemap/default/index',
    'grab/<name>' => 'grabber/grabber/index',
    'construction/<timestamp>' => 'construction/construction/index',
];
