<?php
return [
    [
        'id' => 1,
        'entity_model_name' => 'testEntity',
        'entity_model_id' => 3,
        'file_id' => 1
    ],
    [
        'id' => 2,
        'entity_model_name' => 'testEntity',
        'entity_model_id' => 4,
        'file_id' => 2
    ],
    [
        'id' => 3,
        'entity_model_name' => 'testEntity',
        'entity_model_id' => 5,
        'file_id' => 3
    ],
    [
        'id' => 4,
        'entity_model_name' => 'testUpdateImages',
        'entity_model_id' => 0,
        'file_id' => 1,
        'temp_sign' => Yii::$app->params['sign']
    ],
];
