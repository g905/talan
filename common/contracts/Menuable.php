<?php

namespace common\contracts;

/**
 * Interface Menuable
 *
 * @package common\contracts
 */
interface Menuable
{
    public function menuType(): int;
}
