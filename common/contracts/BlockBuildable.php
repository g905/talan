<?php

namespace common\contracts;

/**
 * Interface BlockBuildable
 *
 * @package common\contracts
 */
interface BlockBuildable
{
    public function blockType(): int;
}
