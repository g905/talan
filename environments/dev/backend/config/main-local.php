<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*'],
        'controllerNamespace' => 'backend\components\gii\controllers',
        'generators' => [
            'advanced-module' => [
                'class' => 'backend\components\gii\module\Generator',
                'templates' => [
                    'default' => '@backend/components/gii/module/default',
                ]
            ],
            'migrations' => [
                'class' => 'backend\components\gii\migration\Generator',
                'templates' => [
                    'default' => '@backend/components/gii/migration/default',
                ]
            ],
            'static-page-model' => [
                'class' => 'backend\components\gii\staticPageModel\Generator',
                'templates' => [
                    'default' => '@backend/components/gii/staticPageModel/default',
                ]
            ],
            'advanced-model' => [
                'class' => 'backend\components\gii\model\Generator',
                'templates' => [
                    'default' => '@backend/components/gii/model/default',
                ]
            ],
            'advanced-crud' => [
                'class' => 'backend\components\gii\crud\Generator',
                'templates' => [
                    'default' => '@backend/components/gii/crud/default',
                ]
            ],
            'import-modules' => [
                'class' => 'backend\components\gii\importModules\Generator',
                'templates' => [
                    'default' => '@backend/components/gii/importModules/default',
                ]
            ],
            'export-modules' => [
                'class' => 'backend\components\gii\exportModules\Generator',
                'templates' => [
                    'default' => '@backend/components/gii/exportModules/default',
                ]
            ],
            'remove-modules' => [
                'class' => 'backend\components\gii\removeModules\Generator',
                'templates' => [
                    'default' => '@backend/components/gii/removeModules/default',
                ]
            ],
        ],
    ];
}

return $config;
