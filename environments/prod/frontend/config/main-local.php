<?php

use frontend\components\BitrixConnect;

return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'bitrixConnect' => [
            'class' => BitrixConnect::class,
            'baseUrl' => 'https://81.4.241.235/',
            'syncUserId' => 1140,
            'hookAuthCode' => 'kngddp8i1i3b8sl2',
        ],
    ],
];
