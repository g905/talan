### Наложение watermark на изображения

FPM позволяет накладывать водяные знаки на изображения, к примеру наложение полупрозрачного фона поверх картинок 
продуктов для магазинов, мелкие логотипы в углах и тд, зависит от извращенной фантазии автора.
Watermark накладывается на превьюшки сгенерированые FPM - оригинал изображения не затрагивается. Без указания watermark 
в параметрах - FPM работает в штатном режиме без каких либо изменений.

Пример конфига (fileName - обязателен):
````
$wmarkPath = __DIR__  . '/../../frontend/web/static/img/content/watermark.png';

...

'modules' => [
        'fileProcessor' => [
            'class' => '\metalguardian\fileProcessor\Module',
            'imageSections' => [
                'product' => [
                    'sliderThumb' => [
                        'action' => FPM::ACTION_THUMBNAIL,
                        'width' => 330,
                        'height' => 330,
                        // опция наложения watermark
                        'watermark' => [ 
                            'fileName' => $wmarkPath,
                        ],
                    ],
                ],
            ],
        ],
````

Конфиг выше накладывает watermark начиная с левой верхней точки (0,0).

ВАЖНО!!! Размер накладываемой картинки должен быть не больше ширины\высоты размеров превьюшного изображения, на которое 
она накладывается. То есть в примере выше - не более 330х330 пикселей. Иначе никакого наложения не произойдет 
(так работает наложение Image класса).

Для более экзотических вариантов наложения - можно указать дополнительные параметры наложения, point и size не 
обязательны:
````
'modules' => [
        'fileProcessor' => [
            'class' => '\metalguardian\fileProcessor\Module',
            'imageSections' => [
                'product' => [
                    'sliderThumb' => [
                        'action' => FPM::ACTION_THUMBNAIL,
                        'width' => 330,
                        'height' => 330,
                        // опция наложения watermark
                        'watermark' => [ 
                            'fileName' => $wmarkPath,
                            'point' => [
                                'x' => 150,
                                'y' => 200,
                            ],
                            'size' => [
                                'width' => 30,
                                'height' => 50,
                            ]
                        ],
                    ],
                ],
            ],
        ],
````
````
'modules' => [
        'fileProcessor' => [
            'class' => '\metalguardian\fileProcessor\Module',
            'imageSections' => [
                'product' => [
                    'sliderThumb' => [
                        'action' => FPM::ACTION_THUMBNAIL,
                        'width' => 330,
                        'height' => 330,
                        // опция наложения watermark
                        'watermark' => [ 
                            'fileName' => $wmarkPath,
                            'point' => [
                                'x' => 112,
                                'y' => 249,
                            ],
                        ],
                    ],
                ],
            ],
        ],
````
где point - координата где должен находится левый верхний угол watermark на оригинальном изображении, а size - размер 
watermark до которого его нужно сресайзить. УТОЧНЕНИЕ - watermark в любом случае ресайзится (если он не меньше 
ширины\высоты оригинального thumbnail), если не указан size - он ресайзится до размеров оригинального изображения 
thumbnail (это условие адекватной работы с учетом специфики работы класса Image). Так же, если заданы координаты point - 
максимальная ширина\высота watermark не должна быть больше original width > (point[X] + watermark width) 
и не больше original height > (point[y] + watermark height).