Модуль comment
==============

Модуль использует функционал внешнего extension [https://github.com/yii2mod/yii2-comments](https://github.com/yii2mod/yii2-comments)
Позволяет:
1. Добавлять комментарии аяксом.
2. Удалять комментарии.
3. Отвечать на комментарии.
4. Модерировать на стороне админки.
5. Поддерживается многоуровневая вложенность.

Содержит backend, common, frontend части.

Как подключить:
1. Import modules
2. Поднять миграциии ./yii migrate --migrationPath=@vendor/yii2mod/yii2-comments/migrations
3. В нужной вьюхе добавить код
```php
<?= \frontend\modules\comment\widgets\comments\Widget::widget([
    'model' => $model //Модель, к которой привязываются комментарии
]); ?>
```
