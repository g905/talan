'use strict';

var IS_DEBUG = true,
// Small function for debugging purposes.
    dd = function () {
        IS_DEBUG ? console.log.apply(console, arguments) : undefined;
    },
    Filter = (function ($) {
        // Filter Class constructor
        function Filter(options) {
            var defaults = {
                form: '#filters-form',
                sliders: '.nouislider__slider',
                pjaxContainer: '#pjax-catalog-list',
                filterInputs: {
                    'complex': '[name="complex[]"]',
                    'rooms': '[name="rooms"]',
                    'queue': '[name="queue"]',
                    'square-min': '[name="square-min"]',
                    'square-max': '[name="square-max"]',
                    'price-min': '[name="price-min"]',
                    'price-max': '[name="price-max"]',
                    'floor-min': '[name="floor-min"]',
                    'floor-max': '[name="floor-max"]',
                    //'floor': '[name="floor"]',
                    'for': '[name="for"]'
                }
            };
            // Proceeding plugin options, by merging two objects (modifying the first).
            this.opts = $.extend({}, defaults, options);
            // Initializing properties
            this.form = $(this.opts.form);
            this.sliders = $(this.opts.sliders);
            this.container = this.opts.pjaxContainer;
        }


        Filter.prototype = {
            init: function () {
                this.checkDependencies();
                this.triggerReInit();
                this.registerPjaxErrorHandler();
                //this.fixFloorFilter();
                this.fixSlidersInputs();
                this.registerOnChangeEventHandlers();
                this.registerPjaxSuccessHandler();
                dd('Filters: initialized');
            },
            // Add 'change' event handler for all filter inputs.
            registerOnChangeEventHandlers: function () {
                var _ = this;
                $.each(_.opts.filterInputs, function(k, v) {
                    var input = $(v);
                    if (input.length > 0) {
                        input.on('change', function(e) {
                            dd('triggering submit 1');
                            _.sendRequest();
                        });
                    }
                });
            },
            registerPjaxErrorHandler: function () {
                $(document).on('pjax:error', function(xhr, textStatus, error, options) {
                    dd(error);
                    return false;
                });
            },
            registerPjaxSuccessHandler: function () {
                $(document).on('pjax:success', function(xhr, textStatus, error, options) {
                    window.preloader.done();
                });
            },
            /**
             * Fix floor filter dropdown by re-updating filter input value, each time it changes through custom dropdown.
             */
            fixFloorFilter: function () {
                var _ = this;
                $(document).on('click', '.floor .custom-select__list button', function () {
                    var input = $('[name="floor"]');
                    input.val($(this).data('select-item-value'));
                    dd('triggering submit 2');
                    _.sendRequest();
                });

            },
            /**
             * Fix all sliders(rage inputs) filters by re-updating filter inputs values, each time it changes through noUiSlider plugin.
             */
            fixSlidersInputs: function () {
                var _ = this;

                $.each(_.sliders, function (k, obj) {
                    //console.log(obj);
                    try {
                        obj.noUiSlider.on('change', function (values, handle, unencoded, tap, positions) {
                            var minValue = values[0],
                                maxValue = values[1],
                                ranges = $(this.target).siblings('.ranges').eq(0),
                                minInput = ranges.find('[data-no-ui-input-min]').eq(0),
                                maxInput = ranges.find('[data-no-ui-input-max]').eq(0);
                            minInput.val(minValue);
                            maxInput.val(maxValue);
                            dd('triggering submit 3');
                            _.sendRequest();
                        });
                    } catch(err) {
                        dd(err);
                        dd('error function fixSlidersInputs, it is trying to fix Floor-slider, which does not exist');
                    }
                });
            },
            /**
             * Send request via pjax plugin.
             */
            sendRequest: function () {
                $('.preloader').removeClass('preloader_done');
                var _ = this;
                $.pjax({
                    url: _.form.attr('action'),
                    container: _.container,
                    data: _.form.serialize(),
                    scrollTo: false,
                    timeout: 15000,
                    type: 'get',
                    push: true
                });
            },
            /**
             * Reinitialize page plugins by triggering custom event.
             */
            triggerReInit: function () {
                $(document).trigger('reinit');
            },
            /**
             * Check whether jQuery is loaded
             * @throws Error
             */
            checkDependencies: function () {
                if (typeof $ === undefined) {
                    throw new Error('This plugin requires jQuery.');
                }
                if (typeof $.pjax === undefined) {
                    throw new Error('This plugin requires Pjax.');
                }
            },

        };
        return Filter;
    })(jQuery);
