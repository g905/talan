<?php
namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class LightBoxAsset extends AssetBundle {

	public $sourcePath = '@frontend/assets/lightbox/';

	public $css = [
		'css/lightbox.css',
	];

	public $js = [
		'js/lightbox.js',
	];

	public $depends = [
		JqueryAsset::class,
	];
}