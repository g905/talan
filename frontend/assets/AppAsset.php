<?php

namespace frontend\assets;

use yii\web\YiiAsset;
use yii\web\AssetBundle;

/**
 * AppAsset general application asset.
 *
 * @package frontend\assets
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'static/css/index.css',
        'css/frontend.css',
        'css/electron.css'
    ];
    public $js = [

        'static/js/map.js',
        'js/filter.js',
 //       'js/electron.js',
        'static/js/index.js',
        'static/js/custom.js',
//        'js/frontend.js',
        'js/apartment-js.js',
//        'js/electron2.js'
    ];
    public $depends = [
        YiiAsset::class,
    ];
}
