<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class CdnAsset
 *
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package frontend\assets
 */
class CdnAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyBo0G9m2PW45wuZ80uwEDz21eroTv3itxY&callback=map.onApiLoaded',
        'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js',
        'js/filter.js',
    ];
/*
    public $js = [
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyD1hIE9f0s8ZwwtWAJ35gZ9BcASy77IcKU&callback=map.onApiLoaded',
        'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js',
        'js/filter.js'
    ];*/
    
}
