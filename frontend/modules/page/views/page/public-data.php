<?php

use yii\widgets\Breadcrumbs;
use common\models\City;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PublicDataMenuWidget;
use frontend\modules\page\widgets\PublicDataConstructorWidget;

/**
 * @var $this yii\web\View
 * @var $menu common\models\PublicDataMenu[]
 * @var $model common\models\PublicData
 * @var $currentMenu common\models\PublicDataMenu|null
 */
if ($currentMenu) {
    $this->params['breadcrumbs'][] = $currentMenu->label;
}
$this->params['breadcrumbs'][] = $model->label;
$this->params['headerMenu'] = $model->subMenus;
$this->params['headerMenuType'] = $model;
//$this->params['breadcrumbsContent'] = Breadcrumbs::widget([
//    'tag' => 'div',
//    'itemTemplate' => "{link}\n",
//    'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
//    'homeLink' => ['label' => bt('_Breadcrumbs home label', 'header'), 'url' => app()->homeUrl, 'class' => 'bread-crumbs__link'],
//    'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
//    'options' => ['class' => 'bread-crumbs header__bread-crumbs constructor__bread-crumbs']
//]);
$this->registerJs('window.preloader.done();')
?>

<div class="wrap wrap_mobile_full">
    <h1 class="h1 sales__h1 wow fadeInUp"><?php //obtain('label', $currentMenu) ?></h1>
</div>

<div class="wrap wrap_mobile_full clearfix">
    <?= PublicDataMenuWidget::widget(['menu' => $menu]) ?>

    <main data-pages="constructor-page" class="constructor-page">

        <section class="title-section">
            <div class="wrap wrap_small wrap_mobile_full">
                <h2 class="h2 h2_title wow fadeInUp"><?= $model->label ?></h2>
            </div>
        </section>

        <?= PublicDataConstructorWidget::widget(compact('model')) ?>
    </main>
</div>
<?= PageFooterWidget::widget(['currentCity' => City::getUserCity()])?>
