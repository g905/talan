<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 25.12.2017
 * Time: 13:51
 */

use common\models\City;
use frontend\helpers\FrontendHelper;
use frontend\helpers\ImgHelper;
use frontend\modules\apartment\widgets\promoAction\ActionBottomFormWidget;
use frontend\modules\page\widgets\HeaderPageMenuWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\page\widgets\PageShareWidget;
use common\models\ApartmentBuilding;

/** @var \common\models\PromoAction $model */
$this->params['breadcrumbs'][] = $model->label;

$cookies = Yii::$app->request->cookies;
$menuItemId = $_COOKIE['menu-item-model-id'] ?? null;
$menuItemType = $_COOKIE['menu-item-model-type'] ?? null;
$menuItemHref = $_COOKIE['menu-item-href'] ?? null;
$currentUrl = $_SERVER['REQUEST_URI'];

if ($currentUrl == $menuItemHref) {

    switch ($menuItemType){
        case HeaderPageMenuWidget::MENU_TYPE_APARTMENT_COMPLEX:
            $complex = \common\models\ApartmentComplex::findOne($menuItemId);
            if (isset($complex)) {
                $this->params['headerMenu'] = $complex->apartmentComplexMenu;
                $this->params['headerMenuType'] = $complex;
            }
            break;

        case HeaderPageMenuWidget::MENU_TYPE_APARTMENT_BUILDING:
            $building = ApartmentBuilding::findOne($menuItemId);
            if (isset($building)) {
                $this->params['headerMenu'] = $building->apartmentBuildingMenu;
                $this->params['headerMenuType'] = $building;
            }
            break;

        case HeaderPageMenuWidget::MENU_TYPE_ABOUT:
            $about = \common\models\AboutCompany::findOne($menuItemId);
            if (isset($about)) {
                $this->params['headerMenu'] = $about->aboutCompanyMenu;
                $this->params['headerMenuType'] = $about;
            }
            break;
    }
}
?>

    <div class="header__row header__row_article">
        <div class="wrap wrap_mobile_full">
            <?= \yii\widgets\Breadcrumbs::widget([
                'tag' => 'div',
                'itemTemplate' => "{link}\n",
                'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
                'homeLink' => [
                    'label' => Yii::t('front/header', '_Breadcrumbs home label'),
                    'url' => Yii::$app->homeUrl,
                    'class' => 'bread-crumbs__link',
                ],
                'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
                'options' => [
                    'class' => 'bread-crumbs bread-crumbs_grey header__bread-crumbs',
                ]
            ]) ?>
        </div>
    </div>

<?= PageShareWidget::widget(['orientation' => PageShareWidget::ORIENTATION_VERTICAL]) ?>
    <section class="custom-article article">
        <div class="wrap wrap_mobile_full wrap_small">
            <h3 class="article__title wow fadeInUp">
                <?= $model->label ?>
            </h3>
            <div class="article__content wow fadeInUp" data-wow-delay="0.5s">
                <?php
                $imgPath = ImgHelper::getImageSrc($model, 'promoAction', 'viewMain', 'image');
                if (($imgPath != null) && ($imgPath != '') && ($imgPath != false)): ?>
                    <img src="<?= ImgHelper::getImageSrc($model, 'promoAction', 'viewMain', 'image') ?>" alt="">
                <?php endif; ?>
                <?= $model->content ?>
            </div>
        </div>
    </section>

    <div class="wrap wrap_mobile_full wrap_small">
        <?= PageShareWidget::widget(['orientation' => PageShareWidget::ORIENTATION_HORIZONTAL]) ?>
    </div>

<?php if (isset($model->manager)): ?>
    <?= PageFormBottomBlockWidget::widget([
        'formTitle' => Yii::t('front/promo-action', '_We are always ready to answer any of your questions'),
        'formDescription' => Yii::t('front/promo-action', '_Leave your data and write the question you are interested in. Our managers will contact you as soon as possible, and they will consult you in a qualified manner'),
        'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
        'manager' => $model->manager,
        'blockClass' => 'questions_article catalogue_question commercial-question',
        'onsubmitJS' => $model->form_onsubmit,
    ]) ?>
<?php else: ?>
    <div data-bg-src=""></div>
<?php endif; ?>


<?= PageFooterWidget::widget(['currentCity' => City::getUserCity()]) ?>
