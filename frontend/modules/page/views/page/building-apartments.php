<?php

use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\apartment\widgets\building\BuildingPlanWidget;
use common\models\ApartmentComplex;
use common\models\ApartmentBuilding;
use yii\widgets\Breadcrumbs;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\ApartmentTabsWidget;

/**
 * @author dimarychek
 * @var ApartmentComplex $complex
 * @var ApartmentBuilding $building
 */
$this->params['currentCity'] = $complex->city;
$this->params['breadcrumbs'][] = $complex->label;
$this->params['headerMenu'] = $complex->apartmentComplexMenu;
$this->params['headerMenuType'] = $complex;
?>

<div class="header__row header__row_apartment">
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'homeLink' => [
                'label' => bt('_Breadcrumbs home label', 'header'),
                'url' => app()->homeUrl,
                'class' => 'bread-crumbs__link',
            ],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'options' => [
                'class' => 'bread-crumbs bread-crumbs_many bread-crumbs_grey header__bread-crumbs',
            ]
        ]) ?>
    </div>
</div>

<?= ApartmentTabsWidget::widget(['complex' => $complex]) ?>

<?= BuildingPlanWidget::widget(['building' => $building, 'entrance' => $entrance]) ?>

<?= PageFormBottomBlockWidget::widget([
    'formTitle' => $building->form_bottom_title,
    'formDescription' => $building->form_bottom_description,
    'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
    'manager' => $building->manager,
    'blockClass' => 'questions_article catalogue_question commercial-question',
    'onsubmitJS' => $building->form_bottom_onsubmit,
]) ?>

<?= PageFooterWidget::widget(['currentCity' => $complex->city, 'additionalClass' => 'footer_no_margin']) ?>
