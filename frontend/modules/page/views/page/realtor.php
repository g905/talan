<?php

use yii\widgets\Breadcrumbs;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\RealtorTopWidget;
use frontend\modules\page\widgets\RealtorAppWidget;
use frontend\modules\page\widgets\RealtorAboutWidget;
use frontend\modules\page\widgets\RealtorFilesWidget;
use frontend\modules\page\widgets\RealtorBenefitsWidget;
use frontend\modules\page\widgets\RealtorPartnersWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;

/**
 * @var $this yii\web\View
 * @var $city common\models\City
 * @var $model common\models\Realtor
 */

$this->params['data-page'] = 'realtors';
$this->params['breadcrumbs'][] = $model->top_label;
$this->params['headerMenu'] = $model->subMenus;
$this->params['headerMenuType'] = $model;
?>

<div class="header__row header__row_apartment">
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'homeLink' => [
                'label' => bt('_Breadcrumbs home label', 'header'),
                'url' => app()->homeUrl,
                'class' => 'bread-crumbs__link',
            ],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'options' => [
                'class' => 'bread-crumbs bread-crumbs_many bread-crumbs_grey header__bread-crumbs',
            ]
        ]) ?>
    </div>
</div>

<?= RealtorTopWidget::widget(compact('model')) ?>
<?= RealtorBenefitsWidget::widget(compact('model')) ?>
<?= RealtorAboutWidget::widget(compact('model')) ?>
<?= RealtorPartnersWidget::widget(compact('model')) ?>
<?= RealtorAppWidget::widget(compact('model')) ?>

<?php if ($model->manager) : ?>
    <?= PageFormBottomBlockWidget::widget([
        'formType' => PageFormBottomBlockWidget::FORM_TYPE_COOPERATION,
        'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
        'blockClass' => 'questions_article commercial-question',
        'formWrapperClass' => 'form-content questions__form-content',
        'formTitle' => $model->form_label,
        'formDescription' => $model->form_description,
        'manager' => $model->manager,
        'onsubmitJS' => $model->form_onsubmit,
    ]) ?>
<?php endif ?>

<?= RealtorFilesWidget::widget(compact('model')) ?>
<?= PageFooterWidget::widget(['currentCity' => $city]) ?>
