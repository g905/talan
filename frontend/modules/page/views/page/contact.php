<?php

use common\models\City;
use frontend\modules\page\widgets\PageFooterWidget;
use yii\widgets\Breadcrumbs;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\ContactPageMapWidget;
use frontend\modules\page\widgets\ContactPageStepsWidget;
use frontend\modules\page\widgets\ContactPageFormsWidget;

/**
 * @var $this yii\web\View
 * @var $model common\models\ContactPage
 */
$this->params['breadcrumbs'][] = $model->label;
$this->registerJs('window.preloader.done();');
?>

<div class="header__row header__row_apartment">
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'options' => ['class' => 'bread-crumbs bread-crumbs_many bread-crumbs_grey header__bread-crumbs'],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'homeLink' => ['label' => t('_Breadcrumbs home label', 'header'), 'url' => app()->homeUrl, 'class' => 'bread-crumbs__link'],
        ]) ?>
    </div>
</div>
<?= ContactPageMapWidget::widget(compact('model')) ?>
<?= ContactPageStepsWidget::widget(compact('model')) ?>
<?= ContactPageFormsWidget::widget(compact('model')) ?>
<?= PageFooterWidget::widget(['currentCity' => City::getUserCity()]) ?>
