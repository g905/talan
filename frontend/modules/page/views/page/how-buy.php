<?php

use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\page\widgets\BuyHowPageWidget;
use common\models\HowBuyPage;
use yii\widgets\Breadcrumbs;
use frontend\helpers\FrontendHelper;

/**
 * @author dimarychek
 * @var HowBuyPage $model
 */
$this->params['currentCity'] = $model->city;
$this->params['breadcrumbs'][] = Yii::t('front/page', 'How to buy');
$this->params['headerMenu'] = $model->subMenus;
$this->params['headerMenuType'] = $model;
?>

<div class="header__row header__row_apartment">
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'homeLink' => [
                'label' => bt('_Breadcrumbs home label', 'header'),
                'url' => app()->homeUrl,
                'class' => 'bread-crumbs__link',
            ],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'options' => [
                'class' => 'bread-crumbs bread-crumbs_many bread-crumbs_grey header__bread-crumbs',
            ]
        ]) ?>
    </div>
</div>

<?= BuyHowPageWidget::widget(['model' => $model]) ?>

<?= PageFormBottomBlockWidget::widget([
    'formTitle' => $model->form_bottom_title,
    'formDescription' => $model->form_bottom_description,
    'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
    'manager' => $model->manager,
    'blockClass' => 'questions_article catalogue_question commercial-question',
    'onsubmitJS' => $model->form_bottom_onsubmit,
]) ?>

<?= PageFooterWidget::widget(['currentCity' => $model->city, 'additionalClass' => 'footer_no_margin']) ?>
