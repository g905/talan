<?php

use common\models\Earth;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageHeaderWidget;
use frontend\modules\page\widgets\earth\EarthTopBlockWidget;
use frontend\modules\page\widgets\earth\EarthCompanyWidget;
use frontend\modules\page\widgets\earth\EarthPlanWidget;
use frontend\modules\page\widgets\earth\EarthBossWidget;
use frontend\modules\page\widgets\earth\EarthOfferWidget;
use frontend\modules\page\widgets\earth\EarthAdvantagesWidget;
use frontend\modules\page\widgets\earth\EarthModelWidget;
use frontend\modules\page\widgets\earth\EarthManagersWidget;
use frontend\modules\page\widgets\earth\EarthMapWidget;

/**
 * @author dimarychek
 * @var Earth $model
 */
$this->params['currentCity'] = $model->city;
$this->params['breadcrumbs'][] = Yii::t('front/page', 'Land purchase');
$this->params['headerMenu'] = $model->subMenus;
$this->params['headerMenuType'] = $model;
?>

<div data-pages="complex-page" class="land_purchase">
    <?= EarthTopBlockWidget::widget(['model' => $model]) ?>

    <?= PageHeaderWidget::widget(['currentCity' => $model->city]) ?>

    <?= EarthCompanyWidget::widget(['model' => $model]) ?>

    <?= EarthMapWidget::widget(['model' => $model]) ?>

    <?= EarthPlanWidget::widget(['model' => $model]) ?>

    <?= EarthBossWidget::widget(['model' => $model]) ?>

    <?= EarthOfferWidget::widget(['model' => $model]) ?>

    <?= EarthAdvantagesWidget::widget(['model' => $model]) ?>

    <?= EarthModelWidget::widget(['model' => $model]) ?>

    <?= EarthManagersWidget::widget(['model' => $model]) ?>

    <?= PageFooterWidget::widget(['currentCity' => $model->city, 'additionalClass' => 'footer_no_margin']) ?>
</div>




