<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 18.12.2017
 * Time: 19:03
 */

use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use common\helpers\Property;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\catalog\CatalogWidget;
use frontend\modules\page\widgets\catalog\CatalogCompletedWidget;
use frontend\modules\page\widgets\catalog\CatalogSecondaryWidget;
use frontend\modules\page\widgets\catalog\CatalogInvestBlockWidget;

$this->params['currentCity'] = $model->city;
$this->params['breadcrumbs'][] = $model->label;
$this->params['headerMenu'] = $model->subMenus;
$this->params['headerMenuType'] = $model;
?>

<div class="header__row header__row_apartment">
    <div class="wrap wrap_mobile_full">
        <?= \yii\widgets\Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'homeLink' => [
                'label' => t('_Breadcrumbs home label', 'header'),
                'url' => app()->homeUrl,
                'class' => 'bread-crumbs__link',
            ],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'options' => ['class' => 'bread-crumbs bread-crumbs_many bread-crumbs_grey header__bread-crumbs']
        ]) ?>
    </div>
</div>

<?= CatalogWidget::widget(['label' => $model->residental_label, 'type' => Property::TYPE_RESIDENTIAL, 'model' => $model]) ?>

<?= CatalogInvestBlockWidget::widget(['test' => $model])?>

<?= CatalogWidget::widget(['label' => $model->commerical_label, 'type' => Property::TYPE_COMMERCIAL, 'model' => $model]) ?>

<?= CatalogSecondaryWidget::widget(['model' => $model]) ?>

<?= CatalogCompletedWidget::widget(['model' => $model]) ?>

<?= PageFormBottomBlockWidget::widget([
    'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
    'manager' => $model->manager,
    'formTitle' => $model->form_title,
    'formDescription' => $model->form_description,
    'blockClass' => 'questions_article commercial-question',
    'onsubmitJS' => $model->form_onsubmit,
]) ?>

<?= PageFooterWidget::widget(['currentCity' => $model->city])?>




