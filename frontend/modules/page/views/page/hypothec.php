<?php

use yii\widgets\Breadcrumbs;
use common\models\City;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\PageIntroWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\HypothecCalcWidget;
use frontend\modules\page\widgets\HypothecStepsWidget;
use frontend\modules\page\widgets\HypothecBenefitWidget;
use frontend\modules\page\widgets\HypothecBenefitsWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;

/** @var \common\models\Hypothec $model */
$this->params['breadcrumbs'][] = $model->label;
$this->params['headerMenu'] = $model->subMenus;
$this->params['headerMenuType'] = $model;
?>

<div class="header__row header__row_article">
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'options' => ['class' => 'bread-crumbs bread-crumbs_grey header__bread-crumbs'],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'homeLink' => ['label' => t('_Breadcrumbs home label', 'header'), 'url' => app()->homeUrl, 'class' => 'bread-crumbs__link'],
        ]) ?>
    </div>
</div>

<?= PageIntroWidget::widget(compact('model')) ?>

<?= HypothecCalcWidget::widget(compact('model')) ?>

<?= HypothecBenefitWidget::widget(compact('model')) ?>

<?= HypothecBenefitsWidget::widget(compact('model')) ?>

<?= HypothecStepsWidget::widget(compact('model')) ?>

<?php if (isset($model->manager)): ?>
    <?= PageFormBottomBlockWidget::widget([
        'formType' => PageFormBottomBlockWidget::FORM_TYPE_HYPOTHEC_WITH_DOC,
        'formWrapperClass' => 'form-content questions__form-content ipot-form-content',
        'formTitle' => 'Оформить заявку',
        'formDescription' => '',
        'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
        'manager' => $model->manager,
        'blockClass' => 'questions_article catalogue_question commercial-question',
        'onsubmitJS' => $model->form_bottom_onsubmit,
    ]) ?>
<?php else: ?>
    <div data-bg-src=""></div>
<?php endif; ?>


<?= PageFooterWidget::widget(['currentCity' => City::getUserCity()]) ?>
