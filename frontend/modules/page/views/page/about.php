<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 20.12.2017
 * Time: 12:53
 */

use frontend\modules\page\widgets\about\AboutDirectorWidget;
use frontend\modules\page\widgets\about\AboutMissionWidget;
use frontend\modules\page\widgets\about\AboutStatisticWidget;
use frontend\modules\page\widgets\about\AboutTopBlockWidget;
use frontend\modules\page\widgets\about\AboutValuesWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\page\widgets\PageHeaderWidget;

/** @var \common\models\AboutCompany $model */
$this->params['currentCity'] = $model->city;

$this->params['breadcrumbs'][] = Yii::t('front/about', '_About company');
$this->params['headerMenu'] = $model->aboutCompanyMenu;
$this->params['headerMenuType'] = $model;
?>

    <?= AboutTopBlockWidget::widget(['aboutPage' => $model]) ?>

    <?= PageHeaderWidget::widget(['currentCity' => $model->city]) ?>



    <?= AboutStatisticWidget::widget(['aboutPage' => $model]) ?>

    <?= AboutMissionWidget::widget(['aboutPage' => $model]) ?>

    <?= AboutValuesWidget::widget(['aboutPage' => $model]) ?>

    <?= AboutDirectorWidget::widget(['aboutPage' => $model]) ?>


    <?= PageFormBottomBlockWidget::widget([
        'formTitle' => $model->form_title,
        'formDescription' => $model->form_description,
        'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_RIGHT,
        //'blockClass' => 'questions_article catalogue_question commercial-question',
    ]) ?>


    <?= PageFooterWidget::widget(['currentCity' => $model->city, 'additionalClass' => 'footer_no_margin']) ?>



