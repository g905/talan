<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 18.12.2017
 * Time: 19:03
 */

use frontend\modules\page\widgets\home\HomeAboutCompanyWidget;
use frontend\modules\page\widgets\home\HomeArticleWidget;
use frontend\modules\page\widgets\home\HomeActionsWidget;
use frontend\modules\page\widgets\home\HomeApartmentComplexWidget;
use frontend\modules\page\widgets\home\HomeBottomCtaWidget;
use frontend\modules\page\widgets\home\HomeHowWeBuildWidget;
use frontend\modules\page\widgets\home\HomeTestimonialsWidget;
use frontend\modules\page\widgets\home\HomeTopBlockWidget;
use frontend\modules\page\widgets\home\HomeInvestBlockWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageHeaderWidget;

/** @var \common\models\Home $model */
$this->params['currentCity'] = $model->city;
?>

<?= HomeTopBlockWidget::widget(['homePage' => $model])?>
<?= PageHeaderWidget::widget(['currentCity' => $model->city])?>
<?= HomeAboutCompanyWidget::widget(['homePage' => $model])?>
<?= HomeApartmentComplexWidget::widget(['homePage' => $model])?>
<?= HomeHowWeBuildWidget::widget(['homePage' => $model])?>
<?= HomeActionsWidget::widget(['homePage' => $model])?>
<?= HomeTestimonialsWidget::widget(['homePage' => $model])?>
<?= HomeArticleWidget::widget() ?>
<?= HomeBottomCtaWidget::widget(['homePage' => $model])?>
<?= PageFooterWidget::widget(['currentCity' => $model->city])?>
