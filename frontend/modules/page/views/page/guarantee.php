<?php

use common\models\City;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;

?>

<?= PageFormBottomBlockWidget::widget([
    'formTitle' => $model->form_title,
    'formType' => PageFormBottomBlockWidget::FORM_TYPE_CUSTOM_GUARANTEE,
    'formDescription' => $model->form_description,
    'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
    'manager' => $model->manager,
    'agreementText' => $model->form_agree,
    'blockClass' => 'questions questions_article commercial-question warranty_obligations',
    'onsubmitJS' => $model->form_onsubmit,
    'modelId' => $model->id
]) ?>

<?= PageFooterWidget::widget(['currentCity' => City::getUserCity()]) ?>
