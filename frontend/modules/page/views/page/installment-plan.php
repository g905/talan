<?php

use yii\widgets\Breadcrumbs;
use common\models\City;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\PageIntroWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\page\widgets\InstallmentPlanCalcWidget;
use frontend\modules\page\widgets\InstallmentPlanStepsWidget;
use frontend\modules\page\widgets\InstallmentPlanBenefitsWidget;

/** @var \common\models\InstallmentPlan $model */
$this->params['breadcrumbs'][] = $model->label;
$this->params['headerMenu'] = $model->subMenus;
$this->params['headerMenuType'] = $model;
?>

<div class="header__row header__row_article">
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'options' => ['class' => 'bread-crumbs bread-crumbs_grey header__bread-crumbs'],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'homeLink' => ['label' => t('_Breadcrumbs home label', 'header'), 'url' => app()->homeUrl, 'class' => 'bread-crumbs__link'],
        ]) ?>
    </div>
</div>

<?= PageIntroWidget::widget(compact('model')) ?>

<?= InstallmentPlanCalcWidget::widget(compact('model')) ?>

<?= InstallmentPlanBenefitsWidget::widget(compact('model')) ?>

<?= InstallmentPlanStepsWidget::widget(compact('model')) ?>

<?php if (isset($model->manager)): ?>
    <?= PageFormBottomBlockWidget::widget([
        'formTitle' => Yii::t('front/promo-action', '_We are always ready to answer any of your questions'),
        'formDescription' => Yii::t('front/promo-action', '_Leave your data and write the question you are interested in. Our managers will contact you as soon as possible, and they will consult you in a qualified manner'),
        'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
        'manager' => $model->manager,
        'blockClass' => 'questions_article catalogue_question commercial-question',
        'onsubmitJS' => $model->form_bottom_onsubmit,
    ]) ?>
<?php else: ?>
    <div data-bg-src=""></div>
<?php endif; ?>


<?= PageFooterWidget::widget(['currentCity' => City::getUserCity()]) ?>
