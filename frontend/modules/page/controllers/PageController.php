<?php

namespace frontend\modules\page\controllers;


use common\helpers\Property;
use common\models\Apartment;
use common\models\ApartmentEntrance;
use common\models\Catalog;
use common\models\GuaranteePage;
use DateTime;
use frontend\modules\apartment\components\ApartmentsListView;
use frontend\modules\form\widgets\CustomPopupRequestWidget;
use frontend\modules\page\widgets\ContactPageMapWidget;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use common\models\Home;
use common\models\City;
use common\models\Earth;
use common\models\BuyPage;
use common\models\Realtor;
use common\models\Hypothec;
use common\models\HowBuyPage;
use common\models\PublicData;
use common\models\CustomPage;
use common\models\ContactPage;
use common\models\AboutCompany;
use common\models\InstallmentPlan;
use common\models\ApartmentComplex;
use common\models\ApartmentBuilding;
use backend\modules\page\models\PublicDataWidget;
use frontend\components\MetaRegistrar;
use frontend\components\FrontendController;
use yii\web\Response;

/**
 * Class PageController
 *
 * @author art
 * @package frontend\modules\page\controllers
 */
class PageController extends FrontendController
{
    /**
     * Home page action.
     *
     * @param string|null $city
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionHome($city = null)
    {
        /** @var City $currentCity */
        $currentCity = City::getCurrentCity($city);
        $model = Home::findByCity($currentCity);

        if (!isset($model)) {
            throw new NotFoundHttpException();
        }

        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->top_screen_title);

        app()->params['footerPopup'] = CustomPopupRequestWidget::widget([
            'delayBeforeOpen' => true,
            'customPopupModel' => $currentCity->customPopup,
            'complexId' => $currentCity->id,
        ]);

        return $this->render('home', compact('model'));
    }

    public function actionCatalog($city = null)
    {
        /** @var City $currentCity */
        $currentCity = City::getCurrentCity($city);
        $model = Catalog::findByCity($currentCity);

        if (!isset($model)) {
            throw new NotFoundHttpException();
        }

        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model);

        return $this->render('catalog', compact('model'));
    }

    /**
     * About page action.
     *
     * @param string|null $city
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAbout($city = null)
    {
        /** @var City $currentCity */
        $currentCity = City::getCurrentCity($city);
        $model = AboutCompany::findByCity($currentCity);

        if (!isset($model)) {
            throw new NotFoundHttpException();
        }

        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->top_screen_title);

        return $this->render('about', compact('model'));
    }

    public function actionContact($city = null)
    {
        $city = City::getCurrentCity($city);
        $model = ContactPage::findByCityWithFormsAndStepsOrFail($city->id);
        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->label);

        return $this->render('contact', compact('model'));
    }

    public function actionShowMap()
    {
        $data = [];
        //dd('test');
        //$city = City::getCurrentCity($city);
        response()->format = Response::FORMAT_JSON;
        $map = ContactPageMapWidget::widget(compact('model'));
        $v = "tes222t";
        $content = 'alert("'.print_r(request()->post()).'");';
        if (request()->getIsAjax()) {
            $data = [
                'js' => Html::script($content),

            ];
            return $data;
        }
    }

    public function actionRealtor($city = null)
    {
        $city = City::getCurrentCity($city);
        $model = Realtor::findByCityOrFail($city->id);
        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->top_label);

        return $this->render('realtor', compact('model', 'city'));
    }

    public function actionPublicData($alias = null)
    {
        $model = PublicData::findByAliasOrFirstOrFail($alias);
        $currentMenu = $model->menu;
        $menu = PublicData::prepareMenu();

        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->label);

        return $this->render('public-data', compact('menu', 'model', 'currentMenu'));
    }

    /**
     * Page view action.
     *
     * @param string|null $city
     * @param $alias
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($city = null, $alias)
    {
        /** @var City $currentCity */
        $currentCity = City::getCurrentCity($city);
        $model = CustomPage::findByCityAndAlias($currentCity, $alias);

        if (!isset($model)) {
            throw new NotFoundHttpException();
        }

        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->label);

        return $this->render('custom_view', compact('model'));
    }

    public function actionHypothec($city = null)
    {
        $city = City::getCurrentCity($city);
        $model = Hypothec::findByCityWithBenefitsAndStepsOrFail($city->id);
        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->label);

        return $this->render('hypothec', compact('model'));
    }

    public function actionInstallmentPlan($city = null)
    {
        $city = City::getCurrentCity($city);
        $model = InstallmentPlan::findByCityWithBenefitsAndStepsOrFail($city->id);
        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->label);

        return $this->render('installment-plan', compact('model'));
    }

    /**
     * Earth page action.
     *
     * @param string|null $city
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEarth($city = null)
    {
        /** @var City $currentCity */
        $currentCity = City::getCurrentCity($city);
        $model = Earth::findByCity($currentCity);
        //dd($model);

        if (!isset($model)) {
            throw new NotFoundHttpException();
        }

        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->top_screen_title);

        return $this->render('earth', compact('model'));
    }

    /**
     * Secondary page action.
     *
     * @param string|null $city
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionBuy($city = null)
    {
        /** @var City $currentCity */
        $currentCity = City::getCurrentCity($city);
        $model = BuyPage::findByCity($currentCity);

        if (!isset($model)) {
            throw new NotFoundHttpException();
        }

        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->top_screen_title);

        return $this->render('buy', compact('model'));
    }

    /**
     * How to buy page action.
     *
     * @param string|null $city
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionHowBuy($city = null)
    {
        /** @var City $currentCity */
        $currentCity = City::getCurrentCity($city);
        $model = HowBuyPage::findByCity($currentCity);

        if (!isset($model)) {
            throw new NotFoundHttpException();
        }

        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->label);

        return $this->render('how-buy', compact('model'));
    }

    public function actionGuarantee($city = null)
    {
        $currentCity = City::getCurrentCity($city);
        $model = GuaranteePage::findByCity($currentCity);

        if (!isset($model)) {
            throw new NotFoundHttpException();
        }

        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model);

        return $this->render('guarantee', compact('model'));
    }

    /**
     * Building Apartments block page action.
     *
     * @param string $alias
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionBuildingApartments($alias, $id, $entrance = false)
    {
        if($entrance)
        {
            $entrance = explode(",", $entrance);
        }

        //dd($entrance);
        $complex = ApartmentComplex::findOnePublishedByAliasOrFail($alias);


        $building = ApartmentBuilding::findOneWithChessByIdOrFail($id);

        if (!isset($complex) || !isset($building)) {
            throw new NotFoundHttpException();
        }

        $this->attachLastModifiedHeader($building->updated_at);
        MetaRegistrar::register($building, $building->label);

        return $this->render('building-apartments', [
            'complex' => $complex,
            'building' => $building,
            'entrance' => $entrance
        ]);
    }

    public function actionBuildingAjaxPopup()
    {
        if (\Yii::$app->request->isAjax)
        {
            //file_put_contents("/var/www/EGORLOG.txt", print_r(\Yii::$app->request->post()));
            $building_id = \Yii::$app->request->post()["building"];
            $alias = \Yii::$app->request->post()["complex"];
            $section = null ?? obtain('section', \Yii::$app->request->post());

            $complex = ApartmentComplex::findOnePublishedByAliasOrFail($alias);
            $build_end = Apartment::getApartmentBuildingDateEnd($complex->id, $building_id, $section, $complex->build_end_date);

            $building = ApartmentBuilding::findOneWithChessByIdOrFail($building_id);

            $equery = ApartmentEntrance::find()->alias('e')
                ->where(['e.building_id' => $building_id, 'e.entrance' => $section]);
            $equery->innerJoin('apartment a', 'a.entrance = e.entrance')
				->where(['a.apartment_building_id' => $building_id, 'a.published' => 1,])
                ->select(['COUNT(*) AS cnt', 'MIN(a.price_from) as min_price', 'a.rooms_number'])
                ->groupBy(['a.rooms_number'])
			;

            $equery2 = Apartment::find()
                ->where(['apartment_building_id' => $building_id, 'status' => Property::STATUS_AVAILABLE])
                //->orWhere(['apartment_building_id' => $building_id, 'status' => Property::STATUS_AVAILABLE])
                ->select(['COUNT(*) AS cnt']);

            if($section)
            {
                $equery2->andWhere(['entrance' => $section]);
				$equery->andWhere(['a.entrance' => $section]);
            }

            $d = $equery2->asArray()->all();

            $c = $equery->asArray()->all();


            if (!isset($complex) || !isset($building)) {
                throw new NotFoundHttpException();
            }

            $data = $c;
            $data["build_end"] = $build_end;
            $data["count"] = $d[0]['cnt'];
           /* if($alias == "sovremennyj-kvartal-serdce-goroda")
            {
                $data['sksg'] = true;
                $data['sksg_3'] = $data['sksg_3.1'] = '31.12.2019';
                $data['sksg_1'] = $data['sksg_2'] = $data['sksg_4'] = $data['sksg_5'] = $data['sksg_6'] = '31.12.2020';
            }*/

            sleep(0);

            return $this->asJson($data);
        }
        return new NotFoundHttpException();
    }

    public function actionGetChart()
    {
        if (request()->getIsPost()) {
            $id = request()->post('chartId');
            if ($id) {
                $chartObj = PublicDataWidget::find()->andWhere(['id' => $id])->one();
                //var_dump($chartObj);die();
                if ($chartObj) {
                    $data = [];
                    $labels = [];
                    foreach ($chartObj->chartData as $chartData) {
                        $labels[] = mb_convert_case(
                            formatter()->asDate(DateTime::createFromFormat('m-Y', $chartData->label), 'LLLL Y'),
                            MB_CASE_TITLE,
                            'UTF-8'
                        );
                        $data[] = $chartData->value;
                    }

                    $chart = [
                        'labels' => $labels,
                        'datasets' => [
                            [
                                'label' => $chartObj->chart_name,
                                'data' => $data,
                                'backgroundColor' => ['rgba(34,166,98, 0.2)'],
                                'borderColor' => ['rgba(34,166,98,1)'],
                                'borderWidth' => 1,
                                'pointBackgroundColor' => 'rgba(34,166,98,1)',
                                'tension' => 0
                            ],
                        ]
                    ];

                    return json_encode($chart);
                }

                return false;
            }

            return false;
        }

        return false;
    }


    public function actionGetCustomChart()
    {
        if (request()->getIsPost()) {
            $id = request()->post('chartId');
            if ($id) {
                $chartObj = PublicDataWidget::find()->andWhere(['id' => $id])->one();
                if ($chartObj) {
                    $data = [];
                    $labels = [];
                    foreach ($chartObj->chartData as $chartData) {
                        $labels[] = mb_convert_case(
                            formatter()->asDate(DateTime::createFromFormat('m-Y', $chartData->label), 'LLLL Y'),
                            MB_CASE_TITLE,
                            'UTF-8'
                        );
                        $data[] = $chartData->value;
                    }

                    $chart = [
                        'labels' => $labels,
                        'datasets' => [
                            [
                                'label' => $chartObj->chart_name,
                                'data' => $data,
                                'backgroundColor' => ['rgba(34,166,98, 0.2)'],
                                'borderColor' => ['rgba(34,166,98,1)'],
                                'borderWidth' => 1,
                                'pointBackgroundColor' => 'rgba(34,166,98,1)',
                                'tension' => 0
                            ],
                        ]
                    ];

                    return json_encode($chart);
                }

                return false;
            }

            return false;
        }

        return false;
    }

    public function actionGetRating()
    {
        if(request()->getIsPost()) {
            $id = request()->post('tableId');
            if ($id) {
                $dataWidget = PublicDataWidget::find()->isPublished()
                    ->andWhere(['id' => $id, 'type' => PublicDataWidget::TYPE_RATING])
                    ->orderBy(['position' => SORT_ASC])
                    ->one();

                if ($dataWidget) {
                    return $dataWidget->value;
                }
            }

            return false;
        }

        return false;
    }

    public function actionGetScheduleChart()
    {
        if (request()->getIsPost()) {
            $id = request()->post('chartId');
            if ($id) {
                $chartObj = PublicDataWidget::find()
                    ->isPublished()
                    ->andWhere(compact('id'))
                    ->orderBy(['position' => SORT_ASC])
                    ->one();

                $labels = explode(',', $chartObj->chart_name);
                $html = '<div class="progress-block wow fadeInUp">';
                $html .= '<div class="progress-title"> '.$chartObj->label.'&nbsp;'.$chartObj->fact.'% </div>';
                $html .= '<div class="progress">';
                $html .= '<div class="progress-bar progress-bar-ready" role="progressbar" style="width:'.$chartObj->fact.'%">';
                $html .= '<a class="progress__link">'.$labels[1].'&nbsp;'.$chartObj->fact.'%</a>';
                $html .= '</div>';
                $html .= '<div class="progress-bar progress-bar-plan" role="progressbar" style="width:'.$chartObj->plan.'%">';
                $html .= '<a class="progress__link">'.$labels[0].'&nbsp;'.$chartObj->plan.'%</a>';
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</div>';

                if($chartObj->chartData) {
                    $html .= '<div class="progress-block__lists">';
                    foreach ($chartObj->chartData as $chartData) {
                        $labels = explode(',', $chartData->label);
                        $values = explode(',', $chartData->value);
                        $html .= '<div class="progress-block">';
                        $html .= '<div class="progress-title"> ' . $chartData->color . ' </div>';
                        $html .= '<div class="progress">';
                        $html .= '<div class="progress-bar progress-bar-ready" role="progressbar" style="width:' . $values[1] . '%">';
                        $html .= '<span>' . $labels[1] . '&nbsp;' . $values[1] . '%</span>';
                        $html .= '</div>';
                        $html .= '<div class="progress-bar progress-bar-plan" role="progressbar" style="width:' . $values[0] . '%">';
                        $html .= '<span>' . $labels[0] . '&nbsp;' . $values[0] . '%</span>';
                        $html .= '</div>';
                        $html .= '</div>';
                        $html .= '</div>';
                    }
                    $html .= '</div>';
                }

                return $html;
            }
            return false;
        }
        return false;
    }
}