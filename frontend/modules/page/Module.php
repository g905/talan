<?php

namespace frontend\modules\page;

use frontend\helpers\SiteUrlHelper;
use yii\helpers\Url;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\page\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
