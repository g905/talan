<?php

namespace frontend\modules\page\widgets;

use backend\modules\apartment\models\ApartmentComplex;
use common\models\builder\publicData\ColumnSection;
use common\models\builder\publicData\ColumnTable;
use common\models\builder\publicData\CustomDdu;
use common\models\builder\publicData\CustomDduSection;
use common\models\builder\publicData\Schedule;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\models\City;
use common\models\PublicData;
use common\models\BuilderWidget;
use common\components\BuilderModel;
use common\models\PublicDataWidget;
use common\models\builder\publicData\ProudFigure;
use common\models\builder\publicData\QuantityDdu;
use common\models\builder\publicData\DiagramTable;
use common\models\builder\publicData\FilesSection;
use common\models\builder\publicData\ServiceLevel;
use common\models\builder\publicData\RatingSection;
use common\models\builder\publicData\DiagramSection;
use common\models\builder\publicData\ContentSection;

/**
 * Class PublicDataConstructorWidget
 *
 * @package frontend\modules\page\widgets
 */
class PublicDataConstructorWidget extends Widget
{
    /**
     * @var PublicData associated models.
     */
    public $model;
    /**
     * @var BuilderModel[]
     */
    private $blocks;

    public function run()
    {
        $output = '';
        $blocks = $this->getBlocks();

        if ($blocks) {
            foreach ($blocks as $block) {
                $output .= $this->handleBlock($block);
            }
        }

        return $output;
    }

    private function handleBlock(BuilderModel $block)
    {
        $output = '';
        $method = 'render' . $block->formName();

        if (method_exists($this, $method)) {
            $output = $this->$method($block);
        }

        return $output;
    }

    public function renderProudFigure(ProudFigure $block)
    {
        $title = $block->figure;
        $description = $block->description;

        return $this->render('public-data-constructor/_proud-figure', compact('title', 'description'));
    }

    public function renderQuantityDdu(QuantityDdu $block)
    {
        $title = $block->label;
        $description = $block->description;
        $dropdownData = $block->getDropdownData();

        return $this->render('public-data-constructor/_quantity-ddu', compact('title', 'description', 'dropdownData'));
    }

    public function renderCustomDduSection(CustomDduSection $block)
    {
        $title = $block->label;
        $description = $block->description;
        $dropdownData = $block->getDropdownData();

        return $this->render('public-data-constructor/_custom-ddu', compact('title', 'description', 'dropdownData'));
    }

    /*public function renderDiagramTable(DiagramTable $block)
    {
        $title = $block->label;
        $description = $block->description;
        $dataIds = explode(',', $block->diagrams);
        $dataRows = PublicDataWidget::find()->alias('pdw')->where([
            'pdw.published' => PublicDataWidget::IS_PUBLISHED,
            'pdw.id' => $dataIds
        ])->with(['chartData', 'city'])->all();

        return $this->render('public-data-constructor/_data-disclosure', compact('title', 'description', 'dataRows'));
    }*/

    public function renderColumnTable(ColumnTable $block)
    {
        $title = $block->label;
        $description = $block->description;
        $dataIds = explode(',', $block->charts);
        $dataRows = PublicDataWidget::find()->alias('pdw')->where([
            'pdw.published' => PublicDataWidget::IS_PUBLISHED,
            'pdw.id' => $dataIds
        ])->with(['chartData', 'city'])->all();

        return $this->render('public-data-constructor/_data-column', compact('title', 'description', 'dataRows'));
    }

    public function renderServiceLevel(ServiceLevel $block)
    {
        $title = $block->label;
        $description = $block->description;
        $number = strpos($block->number, '.') === false
            ? $block->number
            : str_replace('.', ',', (float)(number_format((float)$block->number , 2, '.', '')));
        $numberTitle = $block->numberTitle;
        $numberDescription = $block->numberDescription;

        $dataIds = explode(',', $block->charts);
        $dataRows = PublicDataWidget::find()->alias('pdw')->where([
            'pdw.published' => PublicDataWidget::IS_PUBLISHED,
            'pdw.id' => $dataIds
        ])->with(['chartData', 'city'])->all();

        return $this->render(
            'public-data-constructor/_service-level',
            compact('title', 'description', 'number', 'numberTitle', 'numberDescription', 'dataRows')
        );
    }

    /*public function renderDiagramSection(DiagramSection $block)
    {
        $label = $block->label;
        $diagram = PublicDataWidget::find()->alias('pdw')->where([
            'pdw.published' => PublicDataWidget::IS_PUBLISHED,
            'pdw.id' => $block->diagram
        ])->with(['chartData'])->one();

        if ($diagram === null || empty($diagram->chartData)) {
            return '';
        }

        $labels = [];
        $dataset = [];
        $colors = [];

        foreach ($diagram->chartData as $k => $data) {
            $labels[$k] = $data->label;
            $dataset[$k] = $data->value;
            $colors[$k] = $data->color;
        }

        $labels = Json::encode($labels);
        $dataset = Json::encode($dataset);
        $colors = Json::encode($colors);

        return $this->render('public-data-constructor/_diagram-section', compact('label', 'labels', 'dataset', 'colors'));
    }*/

    public function renderColumnSection(ColumnSection $block)
    {
        $label = $block->label;
        $chart = PublicDataWidget::find()->alias('pdw')->where([
            'pdw.published' => PublicDataWidget::IS_PUBLISHED,
            'pdw.id' => $block->chart
        ])->with(['chartData'])->one();

        if ($chart === null || empty($chart->chartData)) {
            return '';
        }

        return $this->render('public-data-constructor/_column-section', compact('label', 'chart'));
    }

    public function renderContentSection(ContentSection $block)
    {
        $content = $block->content;
        $preview = $this->getFileId($block->preview_image);
        $video = '';
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $block->video_link, $match)) {
            $video = $match[1];
        }

        return $this->render('public-data-constructor/_content-section', compact('content', 'preview', 'video'));
    }

    public function renderRatingSection(RatingSection $block)
    {
        $dropdownData = [];
        $title = $block->label;
        $description = $block->description;
        $buttonLabel = $block->button_label;
        $buttonLink = $block->button_link;
        $dataIds = explode(',', $block->tables);
        $dataRows = PublicDataWidget::find()->alias('pdw')->where([
            'pdw.published' => PublicDataWidget::IS_PUBLISHED,
            'pdw.id' => $dataIds
        ])->with(['city'])->all();

        foreach ($dataRows as $dataRow) {
            $dropdownData[$dataRow->id] = obtain(['city', 'label'], $dataRow);
        }

        return $this->render('public-data-constructor/_sales-rating', compact('title', 'description', 'dropdownData', 'buttonLabel', 'buttonLink'));
    }

    private function getBlocks()
    {
        if ($this->blocks === null) {
            $id = $this->model->getPrimaryKey();
            $className = $this->model->formName();
            $widgets = BuilderWidget::find()
                ->alias('bw')
                ->where(['bw.target_class' => $className, 'bw.target_id' => $id, 'bw.target_attribute' => 'content'])
                ->joinWith(['builderWidgetAttributes'])
                ->orderBy(['bw.position' => SORT_ASC, 'bw.id' => SORT_ASC])->all();
            if ($widgets) {
                foreach ($widgets as $widget) {
                    $tmp = [];
                    /** @var BuilderModel $widgetModel */
                    $widgetModel = new $widget->widget_class;
                    foreach ($widget->builderWidgetAttributes as $builderWidgetAttribute) {
                        $tmp[$builderWidgetAttribute->attribute] = $builderWidgetAttribute->value;
                    }
                    $tmp['id'] = $widget->id;
                    $tmp['position'] = $widget->position;
                    $tmp['target_attribute'] = $widget->target_attribute;
                    $tmp['sign'] = security()->generateRandomString();
                    $widgetModel->setIsNewRecord(false);
                    $widgetModel->setAttributes($tmp);
                    $widgetModel->proccessFiles();
                    $this->blocks[] = $widgetModel;
                }
            }
        }

        return $this->blocks;
    }

    public function renderFilesSection(FilesSection $block)
    {
        $title = $block->label;
        $description = $block->description;
        $files = $block->files;

        return $this->render('public-data-constructor/_files-with-description', compact('title', 'description', 'files'));
    }

    public function renderSchedule(Schedule $block)
    {
        $title = $block->label;
        $description = $block->description;
        $dataIds = explode(',', $block->diagrams);

        $dataRows = PublicDataWidget::find()->alias('pdw')->where([
            'pdw.published' => PublicDataWidget::IS_PUBLISHED,
            'pdw.id' => $dataIds
        ])->with(['chartData', 'city'])->all();

        if($dataIds && $dataRows) {
            $complexIds = [];
            $complexes = [];
            $citiesIds = [];
            $cities = [];

            foreach ($dataRows as $dataRow) {
                $complexIds[] = $dataRow->complex_id;
            }

            if ($complexIds) {
                $complexes = ApartmentComplex::find()->where(['id' => $complexIds])->distinct()->orderBy(['label' => SORT_ASC])->all();
            }

            foreach ($complexes as $complex) {
                $citiesIds[] = $complex->city_id;
            }

            if ($citiesIds) {
                $cities = City::find()->where(['id' => $citiesIds])->distinct()->orderBy(['label' => SORT_ASC])->all();
            }

            return $this->render('public-data-constructor/_schedule', compact('title', 'description', 'complexes', 'cities'));
        }
    }

    public function getFileId($array)
    {
        $id = '';

        if (!isset($array) || !is_array($array)) {
            return $id;
        }

        reset($array);
        $first_key = key($array);
        $id = $array[$first_key]['id'];

        return $id;
    }
}
