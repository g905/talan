<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\Hypothec;

/**
 * Class HypothecBenefitWidget
 *
 * @package frontend\modules\page\widgets
 */
class HypothecBenefitWidget extends Widget
{
    /**
     * @var Hypothec associated model.
     */
    public $model;

    public function run()
    {
        if (!$this->model) {
            return false;
        }

        $output = '';

        if ($this->model->benefit_label && $this->model->benefit_text) {
            $output = $this->render('hypothec-benefit', [
                'title' => $this->model->benefit_label,
                'description' => $this->model->benefit_text,
                'image' => $this->model->getBenefitImageSrc()
            ]);
        }

        return $output;
    }
}
