<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets;

use common\models\City;
use sevenfloor\morpher\Morpher;
use yii\base\Widget;

class PageShareWidget extends Widget
{
    const ORIENTATION_VERTICAL = 1;
    const ORIENTATION_HORIZONTAL = 2;

    public $orientation;

    protected $windowOptions = 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0';

    public function run()
    {
        if (!isset($this->orientation)) {
            return null;
        }

        echo $this->render('pageShareWidgetView', [
            'orientation' => $this->orientation,
            'windowOptions' => $this->windowOptions,
        ]);
    }
}
