<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets;

use common\models\City;
use common\models\SiteMenu;
use yii\base\Widget;

class GetHeaderMenuWidget extends Widget
{
    const POSITION_TOP = 1;
    const POSITION_BOTTOM = 2;
    const POSITION_FIXED_PANEL = 3;

    public function getPositionClass()
    {
        return[
            self::POSITION_TOP => 'header__nav',
            self::POSITION_BOTTOM => 'footer__nav',
            self::POSITION_FIXED_PANEL => 'panel-menu__nav',
        ];
    }

    public $position = self::POSITION_TOP;
    public $currentCity;

    public function run()
    {
        if (!isset($this->currentCity)) {
            return null;
        }

        $menu = $this->getModel();

        if (!isset($menu)){
            return null;
        }

        $positionClass = $this->getPositionClass()[$this->position];

        $currentCity = City::getUserCity();

        echo $this->render('getHeaderMenuWidgetView', [
            'menu' => $menu,
            'positionClass' => $positionClass,
            'position' => $this->position,
            'currentCity' => $currentCity,
        ]);
    }

    protected function getModel()
    {
        $menu = SiteMenu::find()
            ->where([
                'city_id' => $this->currentCity->id,
                'published' => 1,
                'menu_position_id' => SiteMenu::POSITION_HEADER
            ])
            ->one();

        return $menu;
    }

}
