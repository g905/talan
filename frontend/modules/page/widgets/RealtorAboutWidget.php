<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\Realtor;

/**
 * Class RealtorAboutWidget
 *
 * @package frontend\modules\page\widgets
 */
class RealtorAboutWidget extends Widget
{
    /**
     * @var Realtor associated model.
     */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model === null) {
            return $output;
        }

        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('realtor-about', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        $output = [];
        $facts = $this->model->factBlocks;
        if ($facts) {
            $output = [
                'title' => $this->model->about_label,
                'description' => $this->model->about_description,
                'facts' => $facts,
            ];
        }

        return $output;
    }
}
