<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;

/**
 * Class BuyHowPageWidget
 *
 * @package frontend\modules\page\widgets
 * @author dimarychek
 */
class BuyHowPageWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        if (count($this->model->howBlocks) == 0) {
            return null;
        }

        $howBlocks = $this->model->howBlocks;

        echo $this->render('buyHowPageWidgetView', [
            'model' => $this->model,
            'howBlocks' => $howBlocks
        ]);
    }
}
