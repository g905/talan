<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\InstallmentPlan;

/**
 * Class InstallmentPlanBenefitsWidget
 *
 * @package frontend\modules\page\widgets
 */
class InstallmentPlanBenefitsWidget extends Widget
{
    /**
     * @var InstallmentPlan associated model.
     */
    public $model;

    public function run()
    {
        $output = '';
        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('installment-plan-benefits', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        $output = [];
        if ($this->model !== null) {
            $output = [
                'title' => $this->model->benefits_block_label,
                'benefits' => $this->model->benefitBlocks,
            ];
        }

        return $output;
    }
}
