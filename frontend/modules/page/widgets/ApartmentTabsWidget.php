<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;

/**
 * Class ApartmentTabsWidget
 *
 * @package frontend\modules\page\widgets
 * @author dimarychek
 */
class ApartmentTabsWidget extends Widget
{
    public $complex;

    public function run()
    {
        if (!isset($this->complex)) {
            return null;
        }

        echo $this->render('apartmentTabsWidgetView', [
            'complex' => $this->complex
        ]);
    }
}
