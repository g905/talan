<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\Realtor;

/**
 * Class RealtorBenefitsWidget
 *
 * @package frontend\modules\page\widgets
 */
class RealtorBenefitsWidget extends Widget
{
    /**
     * @var Realtor associated model.
     */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model === null) {
            return $output;
        }

        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('realtor-benefits', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        $output = [];
        $benefits = $this->model->benefitBlocks;
        if ($benefits) {
            $output = [
                'title' => $this->model->benefits_label,
                'description' => $this->model->benefits_description,
                'benefits' => $benefits,
            ];
        }

        return $output;
    }
}
