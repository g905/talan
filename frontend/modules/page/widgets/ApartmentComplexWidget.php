<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets;

use metalguardian\fileProcessor\helpers\FPM;
use yii\base\Widget;

class ApartmentComplexWidget extends Widget
{
    public $model;

    public function run()
    {

        if (!isset($this->model)){
            return null;
        }

        $callIcon = '/static/img/gk/phone.png';

        if ($this->model->widgetCallIcon->file_id ?? false) {
            $callIcon = FPM::originalSrc($this->model->widgetCallIcon->file_id);
        }

        return $this->render('apartmentWidgetView',[
            'model' => $this->model,
            'callIcon' => $callIcon,
        ]);
    }
}
