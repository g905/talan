<?php

namespace frontend\modules\page\widgets\earth;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

/**
 * Class EarthManagersWidget
 *
 * @package frontend\modules\page\widgets\earth
 * @author dimarychek
 */
class EarthManagersWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        if (count($this->model->managers) == 0) {
            return null;
        }

        $managers = $this->model->managers;

        echo $this->render('earthManagersWidgetView', [
            'model' => $this->model,
            'managers' => $managers
        ]);
    }
}
