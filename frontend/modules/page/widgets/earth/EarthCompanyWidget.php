<?php

namespace frontend\modules\page\widgets\earth;

use common\models\Earth;
use yii\base\Widget;

/**
 * Class EarthCompanyWidget
 *
 * @package frontend\modules\page\widgets\earth
 * @author dimarychek
 */
class EarthCompanyWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        if (count($this->model->companyBlocks) == 0) {
            return null;
        }

        $companyBlocks = $this->model->companyBlocks;

        echo $this->render('earthCompanyWidgetView', [
            'model' => $this->model,
            'companyBlocks' => $companyBlocks
        ]);
    }
}
