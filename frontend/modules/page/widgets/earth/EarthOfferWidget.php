<?php

namespace frontend\modules\page\widgets\earth;

use yii\base\Widget;

/**
 * Class EarthOfferWidget
 *
 * @package frontend\modules\page\widgets\earth
 * @author dimarychek
 */
class EarthOfferWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        echo $this->render('earthOfferWidgetView', [
            'model' => $this->model
        ]);
    }
}
