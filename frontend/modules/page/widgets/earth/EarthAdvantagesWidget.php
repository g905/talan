<?php

namespace frontend\modules\page\widgets\earth;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

/**
 * Class EarthAdvantagesWidget
 *
 * @package frontend\modules\page\widgets\earth
 * @author dimarychek
 */
class EarthAdvantagesWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        if (count($this->model->advantages) == 0) {
            return null;
        }

        $advantages = $this->model->advantages;

        echo $this->render('earthAdvantagesWidgetView', [
            'model' => $this->model,
            'advantages' => $advantages
        ]);
    }
}
