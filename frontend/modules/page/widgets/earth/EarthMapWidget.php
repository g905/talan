<?php

namespace frontend\modules\page\widgets\earth;

use common\models\EarthMarker;
use frontend\helpers\FrontendHelper;
use yii\base\Widget;
use yii\helpers\Json;

/**
 * Class EarthMapWidget
 *
 * @package frontend\modules\page\widgets\earth
 * @author dimarychek
 */
class EarthMapWidget extends Widget
{
    /**
     * @var string main pin icon path.
     */
    const MAIN_ICON_PATH = '/static/img/map/main_pin.png';

    public $model;

    /**
     * @var string marker template with replaceable tokens.
     */
    public $markerTemplate = <<<HTML
<div class="infowindow"><p><span class="{class}">{region}</span>{description}</p></div>
HTML;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        $markerTypes = $this->model->markerTypes;
        $mapOptions = $this->generateMapOptions();

        echo $this->render('earthMapWidgetView', [
            'model' => $this->model,
            'markerTypes' => $markerTypes,
            'mapOptions' => $mapOptions,
            'mapImage' => $this->model->mapImage
        ]);
    }

    private function generateMapOptions(): string
    {
        $earth = $this->model;
        $markers = $earth->earthMarkers;
        $map = [
            'pos' => ['lat' => $earth->map_lat, 'lng' => $earth->map_lng],
            'zoom' => $earth->map_zoom,
            'mainMarker' => [
                'pos' => ['lat' => $earth->map_lat, 'lng' => $earth->map_lng],
                'icon' => ''
            ],
            'markers' => [],
        ];

        if ($markers) {
            foreach ($markers as $marker) {
                $map['markers'][] = $this->prepareMarkerOptions($marker);
            }
        }

        return Json::encode($map, JSON_NUMERIC_CHECK);
    }

    private function prepareMarkerOptions(EarthMarker $marker): array
    {
        return [
            'type' => $marker->marker_type,
            'pos' => ['lat' => $marker->lat, 'lng' => $marker->lng],
            'icon' => obtain($marker->marker_type, EarthMarker::getMarkerTypeMapIco()),
            'html' => strtr($this->markerTemplate, [
                '{class}' => obtain($marker->marker_type, EarthMarker::getMarkerTypeClass()),
                '{region}' => obtain($marker->marker_type, EarthMarker::getMarkerTypeDictionary()),
                '{description}' => $marker->description,
            ]),
        ];
    }
}
