<?php

use frontend\helpers\ImgHelper;
use common\models\Earth;

/**
 * @author dimarychek
 * @var Earth $model
 * @var array $advantages
 */
?>

<div class="new-quality">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 new-quality__h2 wow fadeInUp"><?= $model->advantages_title ?></h2>
        <div class="new-quality__list">
            <?php for ($i = 0; $i < count($advantages); $i++) : ?>
                <div class="card new-quality__card wow fadeInUp">
                    <div class="card__img card__img_desk" data-bg-src="<?= $advantages[$i]->getImageSrc() ?>" data-bg-pos="center" data-bg-size="cover"></div>
                    <div class="card__content">
                        <h3 class="h3 card__h3"><span class="card_span"><?= $advantages[$i]->title ?></span></h3>
                        <div class="card__number"><?= $model->getPositionFormatted($i) ?></div>
                        <div class="card__divider"></div>
                        <div class="card__text without_height">
                            <?= $advantages[$i]->description ?>
                        </div>
                        <?php if($advantages[$i]->link_name): ?>
                        <a href="<?= $advantages[$i]->link ?>" target="_blank" rel="nofollow" class="button button_green get_to_us__item-btn wow fadeInUp">
                            <span class="button__blip button__blip_hover"></span>
                            <span class="button__blip button__blip_click"></span> <?= $advantages[$i]->link_name ?>
                        </a>
                    <?php endif; ?>
                    </div>
                    <div class="card__img card__img_mob" data-bg-src="<?= $advantages[$i]->getImageSrc() ?>" data-bg-pos="center" data-bg-size="cover"></div>
                </div>
            <?php endfor ?>
        </div>
    </div>
</div>
