<?php

use frontend\helpers\ImgHelper;
use common\models\Earth;
use common\models\EarthMarker;

/**
 * @author dimarychek
 * @var Earth $model
 * @var array $markerTypes
 * @var string $mapOptions
 */
?>

<section class="contacts">
    <div class="wrap wrap_mobile_full">
        <div class="purchase_wrapper">
            <div class="col">
                <h2 class="wow fadeInUp"><?= $model->map_title ?></h2>
            </div>
            <div class="col wow fadeInUp">
                <p><?= $model->map_subtitle ?></p>
                <?= $model->map_demands ?>
            </div>
        </div>
        <div class="marker-wrapper">
            <?php foreach ($markerTypes as $markerType) : ?>
                <div class="col wow fadeInLeft">
                    <img src="<?= obtain($markerType, EarthMarker::getMarkerTypeMapIco()) ?>" alt="">
                    <p><?= obtain($markerType, EarthMarker::getMarkerTypeDictionary()) ?></p>
                </div>
            <?php endforeach ?>
        </div>
    </div>
    <div class="contacts__wrap">
        <div class="land-purchase__wrap-map" data-wow-delay="0.2s">
            <?php if ($mapImage):?>
                <div id="map" class="contacts__map wow fadeIn" data-map-options='<?= $mapOptions ?>' style="background: url(<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($mapImage->file_id ?? ''); ?>); background-position: center; background-repeat: no-repeat;" onclick="map.data.onApiLoaded = 1; map.init();"></div>
            <?php else: ?>
                <div id="map" class="contacts__map wow fadeIn" data-url="" data-map-options='<?= $mapOptions ?>'></div>
            <?php endif; ?>

        </div>
    </div>
</section>
