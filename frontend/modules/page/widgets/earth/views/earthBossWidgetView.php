<?php

use frontend\helpers\ImgHelper;
use frontend\modules\page\widgets\PageSliderWidget;
use common\models\Earth;

/**
 * @author dimarychek
 * @var Earth $model
 */
$preview = ImgHelper::getFileSrc($model, 'bossPhoto');
$video_id = '';
if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $model->boss_video, $match)) {
    $video_id = $match[1];
}
?>

<section class="director">
    <div class="wrap wrap_mobile_full">
        <div class="main-title">
            <h2 class="h2 wow fadeInUp"><?= $model->boss_title ?></h2>
        </div>
        <?php if ($video_id) : ?>
            <div class="director__img director__img_desk wow fadeIn" data-bg-src="<?= $preview ?>" data-bg-pos="center" data-bg-size="cover" data-wow-delay="0.5s" data-wow-duration="2s">
                <div class="video">
                    <div data-youtube="<?= $video_id ?>" data-bg-src="<?= $preview ?>" data-bg-size="cover" data-bg-pos="center">
                        <div class="play video__play">
                            <div class="play__blip play__blip_hover"></div>
                            <div class="play__blip play__blip_click"></div>
                        </div>
                        <iframe src="" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <div class="custom_director director__img director__img_desk wow fadeIn">
                <img src="<?= $preview ?>" alt="">
            </div>
        <?php endif; ?>
        <div class="director__content">
            <h5 class="h5 director__h5 wow fadeInRight"><?= $model->boss_name ?></h5>
            <h6 class="h6 director__h6 wow fadeInRight" data-wow-delay="0.3s"><?= $model->boss_position ?></h6>
            <div class="divider director__divider wow fadeInRight" data-wow-delay="0.6s"></div>
            <div class="director__text wow fadeInRight" data-wow-delay="0.9s">
                <?= $model->boss_description ?>
            </div>
        </div>
        <div class="director__img director__img_tab wow fadeIn" data-bg-src="<?= $preview ?>" data-bg-pos="center" data-bg-size="cover" data-wow-delay="0.5s" data-wow-duration="2s"></div>
    </div>
</section>
