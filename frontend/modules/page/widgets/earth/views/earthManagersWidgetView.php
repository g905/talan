<?php

use frontend\helpers\ImgHelper;
use common\models\Earth;
use common\models\Manager;

/**
 * @author dimarychek
 * @var Earth $model
 * @var Manager $managers
 */
?>

<section class="branch_team">
    <div class="branch_team__wrap wrap wrap_mobile_full">
        <h2 class="h2 branch_team__title wow fadeInUp"><?= $model->manager_title ?></h2>
        <div id="players_wrap" class=" branch_team__tabs">
            <div class="branch_team__tab branch_team__tab-active has-content">
                <div class="branch_team__players">
                    <?php foreach ($managers as $manager) : ?>
                        <div class="branch_team__players-player wow fadeInUp">
                            <div class="branch_team__players-player__img">
                                <img src="<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($manager->photo->file_id ?? '') ?>">
                            </div>
                            <h6 class="branch_team__players-player__name wow fadeInUp" data-wow-delay="0.05s"><?= $manager->fio ?></h6>
                            <hr class="branch_team__players-player__line wow fadeInUp" data-wow-delay="0.1s">
                            <p class="branch_team__players-player__position wow fadeInUp" data-wow-delay="0.15s"><?= $manager->job_position ?></p>
                            <?php if ($manager->contact_email) : ?>
                                <a href="mailto:<?= $manager->contact_email ?>" class="button button_green branch_team__players-player__btn wow fadeInUp" data-wow-delay="0.2s">
                                    <?= Yii::t('front/team', 'write_to_manager') ?>
                                    <span class="button__bg"></span>
                                    <div class="button__blip button__blip_hover"></div>
                                    <div class="button__blip button__blip_click"></div>
                                </a>
                            <?php endif ?>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</section>
