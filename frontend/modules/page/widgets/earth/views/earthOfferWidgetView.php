<?php

use frontend\helpers\ImgHelper;
use frontend\modules\page\widgets\PageSliderWidget;
use common\models\Earth;

/**
 * @author dimarychek
 * @var Earth $model
 */
?>

<div class="questions questions_request">
    <div class="form-content questions__form-content">
        <div class="wrap wrap_mobile_full">
            <div class="questions__columns">
                <h2 class="h2 questions__h2 wow fadeInLeft"><?= $model->offer_title_light ?>
                    <strong><?= $model->offer_title_dark ?></strong>
                </h2>
                <?php if ($model->offer_link) : ?>
                    <a href="<?= $model->offer_link ?>" class="download-file-link wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                        <?= Yii::t('front/home', 'DOWNLOAD THE CONTRACT AGREEMENT') ?>
                    </a>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>