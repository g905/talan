<?php

use frontend\helpers\ImgHelper;
use frontend\modules\page\widgets\PageSliderWidget;
use common\models\Earth;

/**
 * @author dimarychek
 * @var Earth $model
 */
?>

<section class="rules tal-section__rules">
    <div class="wrap wrap_mobile_full">
        <h2 class="rules__h2 wow fadeInUp">
            <span class="tal-section__span"><?= $model->model_title ?></span>
        </h2>
    </div>
    <div class="rules__list">
        <div class="card card_left rules__card">
            <div class="wrap wrap_mobile_full">
                <div class="card__inner">
                    <div class="card__content tal-section__content">
                        <div class="card__text wow fadeInLeft" data-wow-delay="0.8s">
                            <?= $model->model_description ?>
                        </div>
                    </div>
                </div>
                <div class="card__img wow fadeIn" data-bg-src="<?= ImgHelper::getFileSrc($model, 'modelPhoto') ?>" data-bg-pos="center bottom" data-bg-size="cover" data-wow-duration="2s" data-wow-delay="0.5s"></div>
            </div>
        </div>
    </div>
</section>
