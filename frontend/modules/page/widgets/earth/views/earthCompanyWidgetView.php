<?php

use frontend\helpers\ImgHelper;
use common\models\Earth;
use common\models\blocks\EarthCompany;

/**
 * @var Earth $model
 * @var EarthCompany $companyBlocks
 */
?>

<section class="statistics">
    <div class="wrap wrap_mobile_full">
        <div class="statistics__title wow fadeInUp">
            <h2 class="h2"><?= $model->company_title ?></h2>
        </div>
        <div class="statistics__desc wow fadeInUp">
            <?= $model->company_description ?>
        </div>
        <div class="statistics__list">
            <?php foreach ($companyBlocks as $block) : ?>
                <div class="card statistics__card wow fadeIn" data-wow-delay="0s">
                    <h3 class="h3 card__h3"><span class="card_span"><?= $block->title ?></span></h3>
                    <h5 class="h5 card__h5"><?= $block->sub_title ?></h5>
                    <div class="divider card__divider"></div>
                    <div class="card__text">
                        <?= $block->description ?>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>