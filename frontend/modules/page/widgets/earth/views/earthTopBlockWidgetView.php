<?php

use common\models\Earth;
use common\helpers\SiteUrlHelper;
use frontend\helpers\ImgHelper;
use frontend\modules\page\widgets\PageSliderWidget;

/**
 * @author dimarychek
 * @var Earth $model
 */
?>

<section class="poster poster_padding">
    <?= PageSliderWidget::widget([
        'imagePath' => ImgHelper::getFileSrc($model, 'topScreenBackgroundImage'),
        'videoPath' => ImgHelper::getFileSrc($model, 'topScreenBackgroundVideo'),
    ])?>

    <div class="poster__inner">
        <div class="poster__content">
            <h1 class="h1 poster__h1 wow fadeInUp"><?= $model->top_screen_title ?></h1>
            <h5 class="h5 poster__h5 wow fadeInUp" data-wow-delay="0.3s"><?= $model->top_screen_description ?></h5>
            <div class="btns-wrapper">
                <a href="#" class="button button_transparent poster__button wow fadeInUp button_inline ajax-link" data-href="<?= SiteUrlHelper::createFormPartnerPopupUrl() ?>" data-wow-duration="1s" data-wow-delay="1s">
                    <?= Yii::t('front/home', 'Leave request') ?>
                    <div class="button__bg"></div>
                    <div class="button__blip button__blip_hover"></div>
                    <div class="button__blip button__blip_click"></div>
                </a>
                <?php if ($model->presentation_link) : ?>
                    <a href="<?= $model->presentation_link ?>" class="download-file-link wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                        <?= Yii::t('front/home', 'Download presentation') ?>
                    </a>
                <?php endif ?>
            </div>
        </div>
    </div>
    <div class="down poster__down wow down-to-top">
        <button class="down__button" data-scroll-down></button>
        <div class="down__label">
            <?= Yii::t('front/home', '_down') ?>
        </div>
    </div>
</section>
