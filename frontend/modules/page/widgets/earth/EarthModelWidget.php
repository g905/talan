<?php

namespace frontend\modules\page\widgets\earth;

use yii\base\Widget;

/**
 * Class EarthModelWidget
 *
 * @package frontend\modules\page\widgets\earth
 * @author dimarychek
 */
class EarthModelWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        if ($this->model->model_title && $this->model->model_description) {
            echo $this->render('earthModelWidgetView', [
                'model' => $this->model
            ]);
        }
    }
}
