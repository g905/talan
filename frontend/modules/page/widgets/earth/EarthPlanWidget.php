<?php

namespace frontend\modules\page\widgets\earth;

use common\models\Earth;
use yii\base\Widget;

/**
 * Class EarthPlanWidget
 *
 * @package frontend\modules\page\widgets\earth
 * @author dimarychek
 */
class EarthPlanWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        if (count($this->model->planBlocks) == 0) {
            return null;
        }

        $planBlocks = $this->model->planBlocks;

        echo $this->render('earthPlanWidgetView', [
            'model' => $this->model,
            'planBlocks' => $planBlocks
        ]);
    }
}
