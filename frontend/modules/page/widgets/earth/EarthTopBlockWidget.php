<?php

namespace frontend\modules\page\widgets\earth;

use yii\base\Widget;

/**
 * Class EarthTopBlockWidget
 *
 * @package frontend\modules\page\widgets\earth
 * @author dimarychek
 */
class EarthTopBlockWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        echo $this->render('earthTopBlockWidgetView', [
            'model' => $this->model
        ]);
    }
}
