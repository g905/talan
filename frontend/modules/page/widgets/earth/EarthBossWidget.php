<?php

namespace frontend\modules\page\widgets\earth;

use yii\base\Widget;

/**
 * Class EarthBossWidget
 *
 * @package frontend\modules\page\widgets\earth
 * @author dimarychek
 */
class EarthBossWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        echo $this->render('earthBossWidgetView', [
            'model' => $this->model
        ]);
    }
}
