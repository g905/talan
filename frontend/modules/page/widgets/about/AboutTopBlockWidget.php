<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets\about;

use yii\base\Widget;

class AboutTopBlockWidget extends Widget
{

    public $aboutPage;
    public function run()
    {
        if (!isset($this->aboutPage)) {
            return null;
        }

        echo $this->render('aboutTopBlockWidgetView',[
            'model' => $this->aboutPage
        ]);
    }
}
