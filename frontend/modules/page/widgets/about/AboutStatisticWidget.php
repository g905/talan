<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets\about;

use yii\base\Widget;

class AboutStatisticWidget extends Widget
{

    public $aboutPage;
    public function run()
    {
        if (!isset($this->aboutPage)) {
            return null;
        }

        echo $this->render('aboutStatisticWidgetView',[
            'model' => $this->aboutPage
        ]);
    }
}
