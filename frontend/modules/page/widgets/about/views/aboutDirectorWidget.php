<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 20.12.2017
 * Time: 18:08
 */

use frontend\helpers\ImgHelper;

/** @var \common\models\AboutCompany $model */
?>
<section class="director">
    <div class="wrap wrap_mobile_full">

        <div class="director__img director__img_desk wow fadeIn" data-wow-delay="0.5s" data-wow-duration="2s" data-bg-src="<?= ImgHelper::getImageSrc($model, 'about', 'ceo', 'ceoPhoto')?>" data-bg-pos="center" data-bg-size="cover"></div>
        <div class="director__content">
            <h5 class="h5 director__h5 wow fadeInRight">
                <?=$model->ceo_name?>
            </h5>
            <h6 class="h6 director__h6 wow fadeInRight" data-wow-delay="0.3s">
                <?=$model->ceo_position?>
            </h6>
            <div class="divider director__divider wow fadeInRight" data-wow-delay="0.6s"></div>
            <div class="director__text wow fadeInRight" data-wow-delay="0.9s">
                <p>
                    <?=$model->ceo_quotation?>
                </p>
            </div>
        </div>
        <div class="director__img director__img_tab wow fadeIn" data-wow-delay="0.5s" data-wow-duration="2s" data-bg-src="<?= ImgHelper::getImageSrc($model, 'about', 'ceo', 'ceoPhoto')?>" data-bg-pos="center" data-bg-size="cover"></div>

    </div>
</section>
