<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 20.12.2017
 * Time: 17:50
 */

use frontend\helpers\ImgHelper;

/** @var \common\models\AboutCompany $model */
/** @var \common\models\AboutCompanyValues $value */
?>
<section class="values">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 values__h2">
            <?=$model->company_values_title?>
        </h2>
        <div class="values__list">

            <?php foreach ($model->values as $value): ?>
                <div class="card values__card wow fadeInUp">
                <div class="card__img" data-bg-src="<?= ImgHelper::getImageSrc($value, 'about', 'value', 'image')?>" data-bg-pos="center" data-bg-size="cover"></div>
                <h5 class="h5 card__h5">
                    <?=$value->label?>
                </h5>
                <div class="divider card__divider"></div>
                <div class="card__text">
                    <p>
                        <?=$value->content?>
                    </p>
                </div>
            </div>
            <?php endforeach; ?>


            <?php for ($i=0; $i < (3 - (count($model->values) % 3)); $i++): ?>
            <div class="card card_stub values__card wow fadeInUp">
                <div class="card__img" data-bg-src="./img/logo_stub.png" data-bg-pos="center" data-bg-size="auto"></div>
            </div>
            <?php endfor; ?>

        </div>
    </div>
</section>
