<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 20.12.2017
 * Time: 17:39
 */
/** @var \common\models\AboutCompany $model */
?>
<section class="mission">
    <h2 class="h2 mission__h2 wow fadeInLeft">
       <?=$model->mission_label?>
    </h2>
</section>
