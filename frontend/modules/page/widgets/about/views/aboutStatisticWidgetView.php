<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 20.12.2017
 * Time: 17:19
 */

use frontend\helpers\ImgHelper;

/** @var \common\models\AboutCompany $model */
/** @var \common\models\AboutCompanyStat $stat */
?>
<section class="statistics">
    <div class="wrap wrap_mobile_full">
        <div class="statistics__list">
            <?php $delay=0; foreach ($model->statistic as $stat): ?>
            <div class="card statistics__card wow fadeIn" data-wow-delay="<?=$delay?>s">
                <h3 class="h3 card__h3">
                    <span class="card_span"><?=$stat->number?></span>
                </h3>
                <h5 class="h5 card__h5">
                    <?=$stat->label?>
                </h5>
                <div class="divider card__divider"></div>
                <div class="card__text">
                    <p>
                        <?=$stat->content?>
                    </p>
                </div>
            </div>
            <?php $delay+=0.3; endforeach; ?>

        </div>
    </div>
    <div class="map statistics__map wow fadeInUp">
        <div class="map__wrap">
            <div class="map__img" data-bg-src="<?= ImgHelper::getImageSrc($model,'about', 'map','mapImage')?>" data-bg-pos="center" data-bg-size="cover"></div>
        </div>
        <div class="map__prompt">< <?=Yii::t('front/about', '_move map')?> ></div>
    </div>

</section>
