<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 19.12.2017
 * Time: 20:45
 */

use frontend\helpers\ImgHelper;
use frontend\modules\page\widgets\PageSliderWidget;

/** @var \common\models\AboutCompany $model */
?>
<section class="poster poster_padding">
    <?= PageSliderWidget::widget([
        'imagePath' => ImgHelper::getFileSrc($model, 'topScreenBackgroundImage'),
        'videoPath' => ImgHelper::getFileSrc($model,'topScreenBackgroundVideo'),
    ])?>

    <div class="poster__inner">
        <div class="poster__content">
            <h1 class="h1 poster__h1 wow fadeInUp">
                <?= $model->top_screen_title?>
            </h1>
            <h5 class="h5 poster__h5 wow fadeInUp" data-wow-delay="0.3s">
                <?= $model->top_screen_description?>
            </h5>
        </div>
    </div>
    <div class="down poster__down wow down-to-top">
        <button class="down__button" data-scroll-down></button>
        <div class="down__label">
            <?=Yii::t('front/home', '_down')?>
        </div>
    </div>
</section>
