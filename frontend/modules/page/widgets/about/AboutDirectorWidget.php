<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets\about;

use yii\base\Widget;

class AboutDirectorWidget extends Widget
{

    public $aboutPage;
    public function run()
    {
        if (!isset($this->aboutPage)) {
            return null;
        }

        echo $this->render('aboutDirectorWidget',[
            'model' => $this->aboutPage
        ]);
    }
}
