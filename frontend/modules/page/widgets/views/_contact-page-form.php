<?php
/**
 * @var $this yii\web\View
 * @var $active bool
 * @var $model common\models\ContactForm
 * @var $manager common\models\Manager
 */

use yii\bootstrap\ActiveForm;
use common\helpers\SiteUrlHelper;
use frontend\helpers\ImgHelper;
use frontend\modules\form\models\FormAskQuestion;
use yii\helpers\Html;

$formModel = new FormAskQuestion();
?>

<li class="get_in_touch__tabs-tab <?= $active ? 'get_in_touch__tabs-tab-active get_in_touch__tabs-tab-visible' : '' ?>">
    <div class="get_in_touch__tabs-title_block">
        <h4 class="h4 get_in_touch__tabs-form_title"><?= $model->form_label ?></h4>
        <p class="p"><?= $model->form_description ?></p>
    </div>
    <div class="get_in_touch__tabs-form">
        <?php $form = ActiveForm::begin([
            'action' => SiteUrlHelper::createFormQuestionUrl(),
            'options' => [
                'id' => "question-form-$model->id",
                'class' => 'form questions__form ajax-form',
                'data-onsubmit' => config()->get('request_question_onsubmit', null),
                //'data' => ['form' => null]
            ],
            'enableAjaxValidation' => true,
            'validationUrl' => SiteUrlHelper::createFormQuestionValidationUrl(),
            'fieldConfig' => ['options' => ['class' => 'form__row']],
        ]) ?>
            <?= Html::activeHiddenInput($formModel, 'managerId') ?>
            <div class="form__row form__row_split_2">
                <?= $form->field($formModel, 'name', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input', 'id' => "form-question-{$model->id}--name", 'data-validation-name' => 'name']]) ?>
                <?= $form->field($formModel, 'phone', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => [ 'type' => 'tel', 'class' => 'input form__input input_phone', 'id' => "form-question--{$model->id}-phone", 'data-validation-name' => 'phone']]) ?>
            </div>

            <?= $form->field($formModel, 'email', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input', 'id' => "form-question--{$model->id}-email", 'data-validation-name' => 'email']]) ?>
            <?php // $form->field($formModel, 'question', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>

            <div class="form__row">
                <div class="field form__field">
                    <label class="checkbox form__checkbox">
                        <input type="checkbox" class="checkbox__checkbox" name="FormAskQuestion[iAgree]" data-validation-name="agree">
                        <span class="checkbox__mask"></span>
                        <span class="checkbox__label">
                            <?= $model->form_agreement ?: obtain('agreementCheckboxText', app()->params, '') ?>
                        </span>
                    </label>
                </div>
            </div>

            <div class="form__row">
                <button type="submit" class="button form__button">
                    <?= t('_Send', 'form-ask-question') ?>
                    <span class="button__blip button__blip_hover"></span>
                    <span class="button__blip button__blip_click"></span>
                </button>
            </div>
        <?php $form::end() ?>

        <?php if ($manager !== null): ?>
            <div class="manager questions__manager">
                <div class="manager__bg" data-bg-src="<?= ImgHelper::getImageSrc($manager, 'complex', 'manager', 'photo') ?>" data-bg-size="cover" data-bg-pos="75% 0"></div>
                <div class="manager__text-wrapper">
                    <div class="h5 manager__h5"><?= $manager->fio ?></div>
                    <div class="divider"></div>
                    <div class="manager__text"><p><?= $manager->job_position ?></p></div>
                    <a href="tel:<?= preg_replace('/[^0-9]/', '', $manager->phone) ?>" class="manager__tel" target="_blank"><?= $manager->phone ?></a>
                </div>
            </div>
        <?php endif ?>
    </div>
</li>
