<?php
/**
 * @var $this yii\web\View
 * @var string $formOnSubmit
 * @var string $defaultTimeTerm
 * @var string $defaultInitialFee
 * @var string $defaultApartmentPrice
 */

use common\helpers\SiteUrlHelper;
?>

<section class="calc">
    <div class="title">
        <div class="wrap wrap_mobile_full">
            <h3 class="h3 wow fadeIn" data-wow-delay="0.5s" data-wow-duration="0.5s">Рассчитайте рассрочку</h3>
        </div>
    </div>
    <div class="calc__block ">
        <div class="wrap wrap_mobile_full">
            <form class="calc_form calc_form-installment clearfix">
                <fieldset class="calc__fieldset--left wow fadeInLeft" data-wow-delay="1s" data-wow-duration="1s">
                    <legend>Введите данные:</legend>
                    <div class="filters__section clearfix">
                        <div class="filters__heading">Стоимость квартиры:</div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-wnumb='{"decimals":"0", "thousand": " ", "postfix":" руб"}' data-range-min="500000" data-range-max="5000000" data-nouislider-type='price'>
                                <div class="ranges nouislider__ranges">
                                    <input class="ranges__input" placeholder="2 060 000 руб" type="text" value="<?= $defaultApartmentPrice ?>" name="square-max" data-no-ui-input> </div>
                                <div class="nouislider__slider"></div>
                            </div>
                        </div>
                    </div>
                    <div class="filters__section clearfix">
                        <div class="filters__heading">Сроки рассрочки:</div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-wnumb='{"decimals":"0", "postfix":" мес"}' data-range-min="<?= $minTerm ? $minTerm : '1' ?>" data-range-max="<?= $maxTerm ? $maxTerm : '12' ?>" data-nouislider-type='term'>
                                <div class="ranges nouislider__ranges">
                                    <input class="ranges__input" placeholder="6 мес" value="<?= $defaultTimeTerm ?>" type="text" name="square-max" data-no-ui-input> </div>
                                <div class="nouislider__slider"></div>
                            </div>
                        </div>
                    </div>
                    <div class="filters__section clearfix">
                        <div class="filters__heading">
                            <span class="tabl-hidden">Первоначальный взнос:</span>
                            <span class="desk-hidden">Первонач. взнос:</span>
                        </div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-wnumb='{"decimals":"0", "postfix":"%"}' data-range-min="1" data-range-max="100" data-nouislider-type='first_value'>
                                <div class="ranges nouislider__ranges">
                                    <input class="ranges__input" placeholder="60%" value="<?= $defaultInitialFee ?>" type="text" name="square-max" data-no-ui-input> </div>
                                <div class="nouislider__slider"></div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="calc__fieldset--right">
                    <div class="calc__calculated-block  wow fadeInRight" data-wow-delay="1.3s" data-wow-duration="1.3s">
                        <div class="calc__calculated--row">
                            <div class="calc__month-payment">Ежемесячный платеж:</div>
                            <span class="calc__month-payment--price2" data-wnumb='{"decimals": 0, "thousand":" ", "postfix":" р"}'>225 000 р</span>
                        </div>
                        <button type="button" class="send-request ajax-link" data-href="<?= SiteUrlHelper::createFormInstalmentPlanPopupUrl()?>">
                            <span class="tabl-hidden">Отправить заявку на рассрочку</span>
                            <span class="desk-hidden">Отправить заявку</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
