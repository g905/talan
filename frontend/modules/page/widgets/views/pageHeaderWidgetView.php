<?php

use yii\widgets\Breadcrumbs;
use common\helpers\ConfigHelper;
use common\helpers\SiteUrlHelper;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\GetHeaderMenuWidget;
use frontend\modules\page\widgets\HeaderPageMenuWidget;
use frontend\modules\page\widgets\SelectCityWidget;
use frontend\widgets\LocationPopup;

/**
 * @author art
 * @var $this yii\web\View
 * @var $currentCity common\models\City
 */
?>
<header class="header">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <div class="wrap header__wrap wrap_mobile_full">
        <a href="<?= SiteUrlHelper::createHomePageUrl()?>" class="logo header__logo">
            <svg class="logo__svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo_header_white"></use></svg>
        </a>
        <?= SelectCityWidget::widget(['currentCity' => $currentCity, 'position' => SelectCityWidget::POSITION_HEADER_TOP])?>
        <?= GetHeaderMenuWidget::widget(['currentCity' => $currentCity])?>
        <button class="btn-menu header__btn-menu" data-open-menu>
            <span class="btn-menu__element"></span>
            <span class="btn-menu__element"></span>
            <span class="btn-menu__element"></span>
        </button>
        <button class="call header__call ajax-link" data-href="<?=SiteUrlHelper::createFormCallbackPopupUrl()?>">
            <svg class="call__svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-phone"></use></svg>
        </button>
        <a href="tel:<?= ConfigHelper::getHeaderPhoneFormatted($currentCity)?>" class="header__phone">
            <?= ConfigHelper::getHeaderPhone($currentCity)?>
        </a>
        <?= LocationPopup::widget() ?>
    </div>
    <?php if (isset($this->params['headerMenu'])) : ?>
        <?= HeaderPageMenuWidget::widget(['menu' => $this->params['headerMenu'], 'menuType' => $this->params['headerMenuType']]); ?>
    <?php endif ?>
    <?= isset($this->params['headerComplexList']) ? $this->params['headerComplexList'] : '' ?>
    <?= isset($this->params['headerArticleTop']) ? $this->params['headerArticleTop'] : '' ?>
    <?php if (isset($this->params['breadcrumbs'])) : ?>
        <div class="header__row">
            <div class="wrap wrap_mobile_full">
                <?= Breadcrumbs::widget([
                    'tag' => 'div',
                    'itemTemplate' => "{link}\n",
                    'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
                    'options' => ['class' => 'bread-crumbs header__bread-crumbs'],
                    'homeLink' => ['label' => t('_Breadcrumbs home label', 'header'), 'url' => app()->homeUrl, 'class' => 'bread-crumbs__link'],
                    'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
                ]) ?>
            </div>
        </div>
    <?php endif ?>
    <div class="header__line"></div>
</header>
