<?php

use common\helpers\ConfigHelper;
use common\helpers\SiteUrlHelper;
use common\models\City;
use frontend\modules\page\widgets\GetFooterMenuWidget;
use frontend\modules\page\widgets\GetHeaderMenuWidget;

/**
 * @author art
 * @var $this yii\web\View
 * @var $additionalClass string
 * @var $currentCity common\models\City
 */

$hostBase = '.' . \Yii::$app->params['baseUrl'];

$cities = City::find()->orderBy('position ASC')
    ->andWhere(['published' => 1])
    ->all();

?>

<footer class="footer <?= $additionalClass ?>">
    <div class="footer__line"></div>
    <div class="wrap footer__wrap">
    <nav class="nav footer__nav">
    <div class="left-nav">
          <a href="#">
            Приложение талан
          </a>
          <ul class="left-nav__misk">
            <li><button onclick="location.href='https://go.onelink.me/app/footerios'" class="btn-apple"> 
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135 39.9"><defs><style>.cls-1{fill:#fff;}</style></defs><g id="Слой_2" data-name="Слой 2"><g id="artwork"><path d="M130.2,0H4.7A4.69,4.69,0,0,0,0,4.7V35.2a4.69,4.69,0,0,0,4.7,4.7H130.2a4.78,4.78,0,0,0,4.8-4.7V4.7A4.78,4.78,0,0,0,130.2,0Z"/><path style="fill: #fff" class="cls-1" d="M30.1,19.8a5.87,5.87,0,0,1,2.8-4.9,5.87,5.87,0,0,0-4.7-2.5c-2-.2-3.9,1.2-4.9,1.2s-2.6-1.2-4.2-1.1a6.2,6.2,0,0,0-5.2,3.2c-2.3,3.9-.6,9.7,1.6,12.9,1.1,1.6,2.4,3.3,4,3.2s2.2-1,4.2-1,2.5,1,4.2,1,2.8-1.6,3.9-3.1a11.46,11.46,0,0,0,1.8-3.6C33.5,24.9,30.2,23.6,30.1,19.8Zm-3.2-9.5a5.49,5.49,0,0,0,1.3-4.1,6.58,6.58,0,0,0-3.8,1.9A5.29,5.29,0,0,0,23.1,12,4.3,4.3,0,0,0,26.9,10.3ZM53.6,31.5H51.3l-1.2-3.9H45.8l-1.2,3.9H42.4l4.3-13.3h2.6ZM49.8,26l-1.1-3.5c-.1-.4-.3-1.2-.7-2.5-.1.6-.3,1.4-.6,2.5L46.2,26Zm14.9.6a5.58,5.58,0,0,1-1.3,3.9,4.11,4.11,0,0,1-2.9,1.3,2.85,2.85,0,0,1-2.7-1.4v5.1H55.7V25.1c0-1,0-2.1-.1-3.2h1.9l.1,1.5a3.61,3.61,0,0,1,3.2-1.7A3.41,3.41,0,0,1,63.6,23,5.72,5.72,0,0,1,64.7,26.6Zm-2.2.1a4.14,4.14,0,0,0-.6-2.3,2.32,2.32,0,0,0-1.9-.9,2.39,2.39,0,0,0-1.4.5,2.48,2.48,0,0,0-.8,1.4,1.76,1.76,0,0,0-.1.6v1.6a2.79,2.79,0,0,0,.6,1.8,2.06,2.06,0,0,0,1.7.7,2.32,2.32,0,0,0,1.9-.9A4.87,4.87,0,0,0,62.5,26.7Zm13.2-.1a5.58,5.58,0,0,1-1.3,3.9,4.11,4.11,0,0,1-2.9,1.3,2.85,2.85,0,0,1-2.7-1.4v5.1H66.7V25.1c0-1,0-2.1-.1-3.2h1.9l.1,1.5a3.61,3.61,0,0,1,3.2-1.7A3.41,3.41,0,0,1,74.6,23,5.72,5.72,0,0,1,75.7,26.6Zm-2.2.1a4.14,4.14,0,0,0-.6-2.3,2.32,2.32,0,0,0-1.9-.9,2.39,2.39,0,0,0-1.4.5,2.48,2.48,0,0,0-.8,1.4,1.76,1.76,0,0,0-.1.6v1.6a2.79,2.79,0,0,0,.6,1.8,2.06,2.06,0,0,0,1.7.7,2.32,2.32,0,0,0,1.9-.9A4.87,4.87,0,0,0,73.5,26.7ZM88,27.8a3.63,3.63,0,0,1-1.2,2.8,5.22,5.22,0,0,1-3.6,1.2,6.57,6.57,0,0,1-3.4-.8l.5-1.8a5.61,5.61,0,0,0,3.1.9,3.56,3.56,0,0,0,1.9-.5,2,2,0,0,0,.7-1.5,2,2,0,0,0-.6-1.4,5,5,0,0,0-1.8-1c-2.3-.9-3.5-2.1-3.5-3.8a3.42,3.42,0,0,1,1.2-2.7,4.83,4.83,0,0,1,3.3-1,6.68,6.68,0,0,1,3,.6L87,20.4a5.42,5.42,0,0,0-2.5-.6,2.61,2.61,0,0,0-1.8.6,1.35,1.35,0,0,0-.5,1.2,1.61,1.61,0,0,0,.6,1.3,6.49,6.49,0,0,0,1.9,1,7.19,7.19,0,0,1,2.5,1.6A3.09,3.09,0,0,1,88,27.8Zm7.1-4.3H92.8v4.7Q92.8,30,94,30a2.22,2.22,0,0,0,.9-.1l.1,1.6a4.16,4.16,0,0,1-1.7.2,2.79,2.79,0,0,1-2-.8,3.68,3.68,0,0,1-.7-2.6V23.5H89.2V21.9h1.4V20.1l2.1-.6v2.4H95A8.6,8.6,0,0,1,95.1,23.5Zm10.6,3.1a4.84,4.84,0,0,1-1.3,3.6,4.54,4.54,0,0,1-3.5,1.5,4.6,4.6,0,0,1-3.4-1.4,4.75,4.75,0,0,1-1.3-3.5,5.24,5.24,0,0,1,1.3-3.7,4.67,4.67,0,0,1,3.5-1.4,4.6,4.6,0,0,1,3.4,1.4A4.75,4.75,0,0,1,105.7,26.6Zm-2.2.1a4.38,4.38,0,0,0-.6-2.3,2,2,0,0,0-1.9-1.1,2.31,2.31,0,0,0-2,1.1,4.14,4.14,0,0,0-.6,2.3A4.38,4.38,0,0,0,99,29a2.15,2.15,0,0,0,3.8-.1A3.23,3.23,0,0,0,103.5,26.7Zm9.1-2.9c-.2,0-.4-.1-.7-.1a1.85,1.85,0,0,0-1.7.9,2.85,2.85,0,0,0-.5,1.9v5h-2.1V24.9a28.12,28.12,0,0,0-.1-3h1.9l.1,1.8h.1a3.27,3.27,0,0,1,1.1-1.5,3,3,0,0,1,1.5-.5h.5A14.77,14.77,0,0,0,112.6,23.8Zm9.6,2.5a3.08,3.08,0,0,1-.1,1h-6.4a2.79,2.79,0,0,0,.9,2.2,3.3,3.3,0,0,0,2.1.7,7.51,7.51,0,0,0,2.6-.5l.3,1.5a7.94,7.94,0,0,1-3.2.6,4.46,4.46,0,0,1-3.5-1.3,5,5,0,0,1-1.3-3.5,5.4,5.4,0,0,1,1.2-3.6,4.06,4.06,0,0,1,3.4-1.5,3.5,3.5,0,0,1,3.1,1.5A4.2,4.2,0,0,1,122.2,26.3Zm-2.1-.6a2.73,2.73,0,0,0-.4-1.6,1.85,1.85,0,0,0-1.7-.9,2,2,0,0,0-1.7.9,2.52,2.52,0,0,0-.6,1.7l4.4-.1Z"/><path style="fill: #fff" class="cls-1" d="M43.4,7.3a6.89,6.89,0,0,1,.8-.3,3.08,3.08,0,0,1,1-.1,2.41,2.41,0,0,1,1.5.4,1.35,1.35,0,0,1,.5,1.2,1.28,1.28,0,0,1-.4,1,1.19,1.19,0,0,1-.9.5,1.42,1.42,0,0,1,.6.2c.2.1.3.2.5.3a2.19,2.19,0,0,1,.3.5,1.27,1.27,0,0,1,.1.6,2,2,0,0,1-.2.9,1.7,1.7,0,0,1-.6.6,1.14,1.14,0,0,1-.8.3c-.3,0-.6.1-.9.1a2.77,2.77,0,0,1-.9-.1,6.89,6.89,0,0,1-.8-.3l.3-.8a1.45,1.45,0,0,0,.7.2,2.2,2.2,0,0,0,.8.1,1.49,1.49,0,0,0,1-.3,1.23,1.23,0,0,0,.4-.8,1.27,1.27,0,0,0-.1-.6c-.1-.1-.2-.3-.4-.3s-.3-.1-.5-.2-.4,0-.6,0h-.4V9.6h.4a1.27,1.27,0,0,0,.6-.1c.2-.1.3-.1.4-.2l.3-.3a.6.6,0,0,0,.1-.4.91.91,0,0,0-.3-.7,1.08,1.08,0,0,0-.8-.2,1.7,1.7,0,0,0-.7.1,3.33,3.33,0,0,0-.6.3Zm8.4,6.1-.1-.5a1.68,1.68,0,0,1-1.4.6,1.27,1.27,0,0,1-.6-.1c-.2-.1-.3-.2-.5-.3s-.2-.3-.3-.4a1.27,1.27,0,0,1-.1-.6,1.36,1.36,0,0,1,.7-1.3,4,4,0,0,1,2-.4v-.1c0-.6-.3-.9-1-.9a2.13,2.13,0,0,0-1.2.3L49.2,9a2.73,2.73,0,0,1,1.6-.4c1.2,0,1.8.6,1.8,1.9v1.7a3.11,3.11,0,0,0,.1,1.1Zm-.2-2.3a3.92,3.92,0,0,0-1.3.2.67.67,0,0,0-.4.7,1.42,1.42,0,0,0,.2.6.76.76,0,0,0,.5.2.6.6,0,0,0,.4-.1c.1,0,.2-.1.3-.2s.2-.2.2-.3.1-.2.1-.4Zm6.1-2.4v.9h-2v3.9h-1V8.8h3Zm6,2.3a3,3,0,0,1-.2,1,2,2,0,0,1-.4.8,1.79,1.79,0,0,1-.7.5,2.35,2.35,0,0,1-.8.2,1.27,1.27,0,0,1-1.3-.7v2.5h-1V8.6h.9l.1.7a1.62,1.62,0,0,1,1.6-.8,2.35,2.35,0,0,1,.8.2,2.65,2.65,0,0,1,.6.5,2,2,0,0,1,.4.8A3.35,3.35,0,0,0,63.7,11Zm-1.1.1a1.27,1.27,0,0,0-.1-.6c-.1-.2-.1-.4-.2-.5a1.38,1.38,0,0,0-.4-.3.9.9,0,0,0-.5-.1.6.6,0,0,0-.4.1c-.1.1-.3.2-.4.3a1.38,1.38,0,0,0-.3.4.9.9,0,0,0-.1.5v.8a.9.9,0,0,0,.1.5c.1.1.1.3.2.4a1.38,1.38,0,0,0,.4.3.75.75,0,0,0,.5.1.87.87,0,0,0,.9-.5A3.19,3.19,0,0,0,62.6,11.1Zm6.7-2.4L68.1,12c-.3.8-.6,1.4-.8,1.9a3.47,3.47,0,0,1-.8,1.1,2.73,2.73,0,0,1-1.2.6l-.3-.9a3,3,0,0,0,.8-.4,5.55,5.55,0,0,0,.6-.8.25.25,0,0,0,0-.4L64.7,8.8h1.2l.9,2.6c0,.1.1.3.1.4s.1.3.1.5c0-.1.1-.2.1-.4s.1-.3.2-.5l.8-2.6,1.2-.1Zm1.8,2h.5a1.55,1.55,0,0,0,.8-.2.55.55,0,0,0,.3-.5c0-.2-.1-.3-.2-.5a1.45,1.45,0,0,0-.7-.2,1.27,1.27,0,0,0-.6.1c-.2.1-.4.1-.5.2L70.4,9c.3-.1.5-.2.8-.3a2.2,2.2,0,0,1,.8-.1,1.27,1.27,0,0,1,.6.1,1.42,1.42,0,0,1,.6.2.78.78,0,0,1,.4.4,1.42,1.42,0,0,1,.2.6.6.6,0,0,1-.1.4c-.1.1-.1.2-.2.3s-.2.2-.3.2-.2.1-.4.1c.2,0,.3.1.5.1s.3.1.4.2l.3.3a.75.75,0,0,1,.1.5,1.45,1.45,0,0,1-.2.7,1,1,0,0,1-.5.4,1.45,1.45,0,0,1-.7.2c-.3,0-.5.1-.7.1a2.77,2.77,0,0,1-.9-.1c-.3-.1-.5-.2-.8-.3l.3-.7a1.42,1.42,0,0,0,.6.2,1.48,1.48,0,0,0,.7.1,1.55,1.55,0,0,0,.8-.2.55.55,0,0,0,.3-.5.37.37,0,0,0-.1-.3c-.1-.1-.2-.2-.3-.2a.6.6,0,0,0-.4-.1h-1v-.6Zm5.6-2v3.7a2.51,2.51,0,0,1,.4-.7,2.51,2.51,0,0,1,.4-.7l1.2-2.2h1.4v4.7h-1V10.8a4.1,4.1,0,0,1,.1-1.1c-.1.3-.2.5-.3.8a4.88,4.88,0,0,0-.3.7L78,12.4l-.6,1.2H76V8.9h.7Zm8.7,0v.9H84v3.9H83V9.6H81.6V8.7Zm5.4,2.2v.5H87.7a1.53,1.53,0,0,0,.4,1,1.82,1.82,0,0,0,1.1.4,5.07,5.07,0,0,0,1.3-.2l.2.7a3.81,3.81,0,0,1-1.6.3,3,3,0,0,1-1-.2,1.79,1.79,0,0,1-.7-.5,3,3,0,0,1-.5-.7,3,3,0,0,1-.2-1,3,3,0,0,1,.2-1,2.18,2.18,0,0,1,.5-.8,1.79,1.79,0,0,1,.7-.5,2.92,2.92,0,0,1,.9-.2,1.88,1.88,0,0,1,.8.2,2.65,2.65,0,0,1,.6.5,1.61,1.61,0,0,1,.4.7C90.7,10.2,90.8,10.5,90.8,10.9Zm-1-.3a1.22,1.22,0,0,0-.3-.9,1,1,0,0,0-.8-.3,1.14,1.14,0,0,0-.8.3,1.88,1.88,0,0,0-.4.9ZM95,8.8c.2,0,.4-.1.7-.1h1.6a1.85,1.85,0,0,1,.7.2,2.09,2.09,0,0,1,.6.4.84.84,0,0,1,.2.7.6.6,0,0,1-.1.4c-.1.1-.1.2-.2.3s-.2.2-.3.2-.2.1-.4.1a2,2,0,0,1,.8.4,1,1,0,0,1,.3.8,1,1,0,0,1-.3.8,1.61,1.61,0,0,1-.7.4,2.35,2.35,0,0,1-.8.2H94.9V8.8Zm1,1.8h1c.1,0,.3-.1.4-.1s.2-.1.3-.2a.37.37,0,0,0,.1-.3.55.55,0,0,0-.3-.5,2,2,0,0,0-.8-.1h-.6A4.87,4.87,0,0,0,96,10.6Zm0,2.1h1c.1,0,.3-.1.4-.1s.2-.1.3-.2.1-.2.1-.4a.55.55,0,0,0-.4-.6,2.51,2.51,0,0,0-1-.2H96Z"/></g></g></svg></button></li>
            <li><button onclick="location.href='https://go.onelink.me/app/footerandrd'" class="btn-google"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 135 40"><defs><style>.cls-1{fill:url(#Безымянный_градиент);}.cls-2{fill:url(#Безымянный_градиент_2);}.cls-3{fill:url(#Безымянный_градиент_3);}.cls-4{fill:url(#Безымянный_градиент_4);}.cls-5{opacity:0.2;}.cls-5,.cls-6,.cls-8{isolation:isolate;}.cls-6{opacity:0.12;}.cls-7,.cls-8,.cls-9{fill:#fff;}.cls-8{opacity:0.25;}.cls-9{stroke:#fff;stroke-miterlimit:10;stroke-width:0.2px;}</style><linearGradient id="Безымянный_градиент" x1="21.77" y1="-48.14" x2="4.99" y2="-64.92" gradientTransform="matrix(1, 0, 0, -1, 0, -39.46)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#00a0ff"/><stop offset="0.01" stop-color="#00a1ff"/><stop offset="0.26" stop-color="#00beff"/><stop offset="0.51" stop-color="#00d2ff"/><stop offset="0.76" stop-color="#00dfff"/><stop offset="1" stop-color="#00e3ff"/></linearGradient><linearGradient id="Безымянный_градиент_2" x1="33.83" y1="-59.51" x2="9.64" y2="-59.51" gradientTransform="matrix(1, 0, 0, -1, 0, -39.46)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ffe000"/><stop offset="0.41" stop-color="#ffbd00"/><stop offset="0.78" stop-color="orange"/><stop offset="1" stop-color="#ff9c00"/></linearGradient><linearGradient id="Безымянный_градиент_3" x1="24.82" y1="-61.74" x2="2.06" y2="-84.5" gradientTransform="matrix(1, 0, 0, -1, 0, -39.46)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ff3a44"/><stop offset="1" stop-color="#c31162"/></linearGradient><linearGradient id="Безымянный_градиент_4" x1="7.26" y1="-39.67" x2="17.42" y2="-49.83" gradientTransform="matrix(1, 0, 0, -1, 0, -39.46)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#32a071"/><stop offset="0.07" stop-color="#2da771"/><stop offset="0.48" stop-color="#15cf74"/><stop offset="0.8" stop-color="#06e775"/><stop offset="1" stop-color="#00f076"/></linearGradient></defs><g id="Слой_2" data-name="Слой 2"><g id="artwork"><path d="M130,40H5a5,5,0,0,1-5-5V5A5,5,0,0,1,5,0H130a5,5,0,0,1,5,5V35A5,5,0,0,1,130,40Z"/><path class="cls-1" d="M10.4,7.5a2,2,0,0,0-.5,1.4V31a2,2,0,0,0,.5,1.4l.1.1L22.9,20.1v-.2Z"/><path class="cls-2" d="M27,24.3l-4.1-4.1v-.3L27,15.8l.1.1L32,18.7c1.4.8,1.4,2.1,0,2.9Z"/><path class="cls-3" d="M27.1,24.2,22.9,20,10.4,32.5c.5.5,1.2.5,2.1.1l14.6-8.4"/><path class="cls-4" d="M27.1,15.8,12.5,7.5a1.62,1.62,0,0,0-2.1.1L22.9,20Z"/><path class="cls-5" d="M27,24.1,12.5,32.3a1.64,1.64,0,0,1-2,0l-.1.1.1.1a1.64,1.64,0,0,0,2,0Z"/><path class="cls-6" d="M10.4,32.3a2,2,0,0,1-.4-1.4V31a2,2,0,0,0,.5,1.4v-.1ZM32,21.3l-5,2.8.1.1L32,21.3A1.44,1.44,0,0,0,33,20C33,20.5,32.6,20.9,32,21.3Z"/><path class="cls-7" d="M68.1,21.8a4.21,4.21,0,0,0-4.3,4.3,4.27,4.27,0,0,0,4.3,4.3,4.21,4.21,0,0,0,4.3-4.3A4.15,4.15,0,0,0,68.1,21.8Zm0,6.8a2.61,2.61,0,0,1,0-5.2A2.42,2.42,0,0,1,70.5,26,2.48,2.48,0,0,1,68.1,28.6Zm-9.3-6.8a4.21,4.21,0,0,0-4.3,4.3,4.27,4.27,0,0,0,4.3,4.3,4.21,4.21,0,0,0,4.3-4.3A4.15,4.15,0,0,0,58.8,21.8Zm0,6.8a2.61,2.61,0,0,1,0-5.2A2.42,2.42,0,0,1,61.2,26,2.48,2.48,0,0,1,58.8,28.6ZM47.7,23.1v1.8H52a3.78,3.78,0,0,1-1,2.3,4.43,4.43,0,0,1-3.3,1.3A4.66,4.66,0,0,1,43,23.7a4.72,4.72,0,0,1,4.7-4.8A5,5,0,0,1,51,20.2l1.3-1.3a6.41,6.41,0,0,0-4.5-1.8,6.75,6.75,0,0,0-6.7,6.6,6.75,6.75,0,0,0,6.7,6.6,5.75,5.75,0,0,0,4.6-1.9A6,6,0,0,0,54,24.2a3.75,3.75,0,0,0-.1-1.1Zm45.4,1.4a3.89,3.89,0,0,0-3.6-2.7,4,4,0,0,0-4,4.3,4.18,4.18,0,0,0,4.2,4.3,4.1,4.1,0,0,0,3.5-1.9l-1.4-1a2.44,2.44,0,0,1-2.1,1.2,2.17,2.17,0,0,1-2.1-1.3L93.3,25Zm-5.8,1.4a2.43,2.43,0,0,1,2.2-2.5,1.77,1.77,0,0,1,1.6.9ZM82.6,30h1.9V17.5H82.6Zm-3-7.3a3.29,3.29,0,0,0-2.3-1A4.3,4.3,0,0,0,73.2,26a4.14,4.14,0,0,0,4.1,4.2,2.87,2.87,0,0,0,2.2-1h.1v.6a2.27,2.27,0,0,1-4.4,1l-1.6.7A4.13,4.13,0,0,0,77.4,34c2.2,0,4-1.3,4-4.4V22H79.6Zm-2.2,5.9a2.61,2.61,0,0,1,0-5.2A2.41,2.41,0,0,1,79.7,26,2.41,2.41,0,0,1,77.4,28.6Zm24.4-11.1H97.3V30h1.9V25.3h2.6a3.9,3.9,0,1,0,0-7.8Zm.1,6H99.2V19.2h2.7a2.2,2.2,0,0,1,2.2,2.1A2.29,2.29,0,0,1,101.9,23.5Zm11.5-1.8a3.36,3.36,0,0,0-3.3,1.9l1.7.7a1.77,1.77,0,0,1,1.7-.9,1.88,1.88,0,0,1,2,1.6v.1a4,4,0,0,0-1.9-.5c-1.8,0-3.6,1-3.6,2.8a2.93,2.93,0,0,0,3.1,2.8,2.81,2.81,0,0,0,2.4-1.2h.1v1h1.8V25.2A3.78,3.78,0,0,0,113.4,21.7Zm-.2,6.9c-.6,0-1.5-.3-1.5-1.1,0-1,1.1-1.3,2-1.3a3.87,3.87,0,0,1,1.7.4A2.33,2.33,0,0,1,113.2,28.6ZM123.7,22l-2.1,5.4h-.1L119.3,22h-2l3.3,7.6-1.9,4.2h1.9L125.7,22Zm-16.8,8h1.9V17.5h-1.9Z"/><path class="cls-8" d="M12.5,7.6,32,18.7c.6.4,1,.8,1,1.3a1.66,1.66,0,0,0-1-1.4L12.5,7.5C11.1,6.7,10,7.4,10,9v.1C10,7.5,11.1,6.8,12.5,7.6Z"/><path class="cls-9" d="M42.1,14.3h-.7v-2H42a2.89,2.89,0,0,0,.8-2.3V7h4v5.3h.7v2h-.7V13H42.2v1.3ZM43.5,10a4.14,4.14,0,0,1-.6,2.3h3V7.8H43.4Zm9.9,2.2A3.1,3.1,0,0,1,48.1,10a3.1,3.1,0,1,1,6.2,0A2.94,2.94,0,0,1,53.4,12.2Zm-3.8-.5a2.36,2.36,0,0,0,1.6.7,2,2,0,0,0,1.6-.7,2.41,2.41,0,0,0,.7-1.7,2.06,2.06,0,0,0-.7-1.7,2.36,2.36,0,0,0-1.6-.7,2,2,0,0,0-1.6.7,2.41,2.41,0,0,0-.7,1.7A2.06,2.06,0,0,0,49.6,11.7Zm8.6,1.4A3.07,3.07,0,0,1,55.1,10a3.07,3.07,0,0,1,3.1-3.1,2.7,2.7,0,0,1,2.2,1l-.5.5a2.14,2.14,0,0,0-1.7-.8,2.19,2.19,0,0,0-1.7.7,2.06,2.06,0,0,0-.7,1.7,2.06,2.06,0,0,0,.7,1.7,2.41,2.41,0,0,0,1.7.7,2.27,2.27,0,0,0,1.8-.9l.5.5a3.92,3.92,0,0,1-1,.8A2.77,2.77,0,0,1,58.2,13.1Zm5.5-.1h-.8V7.7H61.2V7h4.1v.7H63.6Zm6.9-6-2.3,5.1a1.57,1.57,0,0,1-1.4,1.1,1.27,1.27,0,0,1-.6-.1l.2-.7a.6.6,0,0,0,.4.1c.2,0,.3,0,.4-.1a1.38,1.38,0,0,0,.3-.4l.2-.5-2-4.4h.9l1.5,3.4,1.4-3.4h1Zm.9,6V7h4.3v6H75V7.7H72.2V13Zm5.7,0V7H78V9.6h3V7h.8v6H81V10.3H78V13Zm10.9-.8A3.1,3.1,0,0,1,82.8,10,3.1,3.1,0,1,1,89,10,4,4,0,0,1,88.1,12.2Zm-3.9-.5a2.36,2.36,0,0,0,1.6.7,2,2,0,0,0,1.6-.7,2.41,2.41,0,0,0,.7-1.7,2.06,2.06,0,0,0-.7-1.7,2.36,2.36,0,0,0-1.6-.7,2,2,0,0,0-1.6.7,2.41,2.41,0,0,0,0,3.4ZM92.1,13V7h2.2A1.71,1.71,0,0,1,96,8.7a2.35,2.35,0,0,1-.2.8,1,1,0,0,1-.6.5,1.79,1.79,0,0,1,.7.5,1.22,1.22,0,0,1,.3.9,1.66,1.66,0,0,1-.5,1.2,2.11,2.11,0,0,1-1.3.5H92.1Zm.8-3.4h1.4a.91.91,0,0,0,.7-.3.86.86,0,0,0,.3-.6.86.86,0,0,0-.3-.6.91.91,0,0,0-.7-.3H92.9Zm0,2.7h1.6a1,1,0,0,0,1-1,1.08,1.08,0,0,0-.3-.7.91.91,0,0,0-.7-.3H93Z"/></g></g></svg></button></li>

          </ul>
        </div>
          <ul class="nav-center">
            <li><a href="#" class="nav__link">Города</a>
              <ul class="nav-city">
                  <?php foreach($cities as $city){ ?>
                <li>
                  <a href="http://<?= $city->alias ?><?=$hostBase?>"><?= $city->label ?></a>
                </li>
                  <?php } ?>

              </ul>

            </li>
            <li>
             
              <ul class="menu-footer">
                 <a href="#" class="nav__link">меню</a>
                <li><a href="/catalog">Недвижимость</a></li>
                 <li><a href="http://журнал<?=$hostBase?>">Журнал</a></li>
                  <li><a href="/contact">Контакты</a></li>
              </ul>
               <ul class="menu-footer menu-footer-2">
                <a href="/partners" class="nav__link">Партнерам</a>
                <li><a href="/realtors">Риелторам</a></li>
                 <li><a href="land-purchase">Владельцам участков</a></li>
                  <li><a href="http://тендеры<?=$hostBase?>">Тендеры</a></li>
                  <li><a href="http://инвест<?=$hostBase?>">Инвестирование</a></li>
<!--                  <li><a href="/public-data/investoram">Инвесторам</a></li>-->
              </ul>

            </li>
            <li>
               <a href="#" class="nav__link">О компании</a>
               <ul>
                 <li><a href="/about">Кто мы</a></li>
                  <li><a href="/team">Команда</a></li>
                  <li><a href="/journal">Новости</a></li>
                   <li><a href="/public-data">Открытые данные</a></li>
<!--                   <li><a href="/public-data/investoram">Инвесторам</a></li>-->
                   <li><a href="/vacancies">Вакансии</a></li>
                   <li><a href="/career">Карьера</a></li>

               </ul>
            </li>
          </ul>
         
        </nav>
        <div class="social footer__social">
        <button class="footer__button ajax-link" data-href="<?= SiteUrlHelper::createFormCallbackPopupUrl()?>">
            <?= t('_ask question', 'footer')?>
        </button>
            <?php if ($currentCity->link_ig) : ?>
                <a href="<?= $currentCity->link_ig ?>" class="social__item socicon-instagram" target="_blank"></a>
            <?php endif; ?>
            <?php if ($currentCity->link_ok) : ?>
                <a href="<?= $currentCity->link_ok ?>" class="social__item socicon-odnoklassniki" target="_blank"></a>
            <?php endif; ?>
            <?php if ($currentCity->link_vk) : ?>
                <a href="<?= $currentCity->link_vk ?>" class="social__item socicon-vkontakte" target="_blank"></a>
            <?php endif; ?>
            <?php if ($currentCity->link_fb) : ?>
                <a href="<?= $currentCity->link_fb ?>" class="social__item socicon-facebook" target="_blank"></a>
            <?php endif; ?>
            
            
           
        </div>
       
    </div>
    <div class="wrap footer__wrap">
        
        <div class="footer__copyright">
            <span class="copy">&copy;</span> <?= date('Y') ?>
            <?= t('_LTD «TALAN-IZHEVSK»', 'footer') ?>
        </div>
        <?= GetFooterMenuWidget::widget(['currentCity' => $currentCity])?>
        <div class="developers footer__developers">
            <sup class="developers__by">by</sup>
            <a class="developers__link" href="https://vintage.agency/" target="_blank">
                <svg class="developers__logo icon-vintage-logo">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-vintage-logo"></use>
                </svg>
            </a>
        </div>
    </div>
    <div class="wrap footer__advertising __web-inspector-hide-shortcut__">
        Обращаем внимание, что настоящий рекламный материал носит исключительно информационный характер и не является публичной офертой, определяемой положениями статьи 437 Гражданского кодекса РФ.
        <div>
            Общество с ограниченной ответственностью «ТАЛАН-ФИНАНС» (ОГРН 1117746313739) публикует информацию, подлежащую раскрытию эмитентами эмиссионных ценных бумаг, на странице в сети Интернет, предоставленной распространителем информации на рынке ценных бумаг ООО «Интерфакс-ЦРКИ», аккредитованным ЦБ РФ на раскрытие информации, по адресу: <a href = "http://www.e-disclosure.ru/portal/company.aspx?id=37631">http://www.e-disclosure.ru/portal/company.aspx?id=37631</a>
        </div>
    </div>
</footer>

<!-- for tablet -->

<footer class="footer footer_tablet <?=$additionalClass?>">
    <div class="footer__line"></div>
    <div class="wrap footer__wrap">
           <button class="footer__button ajax-link" data-href="<?= SiteUrlHelper::createFormCallbackPopupUrl()?>">
            <?= t('_ask question', 'footer')?>
        </button>

        <div class="social footer__social">
            <?php if ($currentCity->link_ig) : ?>
                <a href="<?= $currentCity->link_ig ?>" class="social__item socicon-instagram" target="_blank"></a>
            <?php endif; ?>
            <?php if ($currentCity->link_ok) : ?>
                <a href="<?= $currentCity->link_ok ?>" class="social__item socicon-odnoklassniki" target="_blank"></a>
            <?php endif; ?>
            <?php if ($currentCity->link_vk) : ?>
                <a href="<?= $currentCity->link_vk ?>" class="social__item socicon-vkontakte" target="_blank"></a>
            <?php endif; ?>
            <?php if ($currentCity->link_fb) : ?>
                <a href="<?= $currentCity->link_fb ?>" class="social__item socicon-facebook" target="_blank"></a>
            <?php endif; ?>
            <?php if ($currentCity->link_itunes) : ?>
                <a href="<?= $currentCity->link_itunes ?>" class="social__item socicon-appstore" target="_blank"></a>
            <?php endif; ?>
            <?php if ($currentCity->link_play) : ?>
                <a href="<?= $currentCity->link_play ?>" class="social__item socicon-play" target="_blank"></a>
            <?php endif; ?>
        </div>
        <?= GetHeaderMenuWidget::widget(['currentCity' => $currentCity, 'position' => GetHeaderMenuWidget::POSITION_BOTTOM]) ?>
    </div>
    <div class="wrap footer__wrap">
        <?= GetFooterMenuWidget::widget(['currentCity' => $currentCity]) ?>
        <div class="footer__copyright">
            <span class="copy">&copy;</span> <?= date('Y') ?>
            <?= t('_LTD «TALAN-IZHEVSK»', 'footer') ?>
        </div>
        <div class="developers footer__developers">
            <sup class="developers__by">by</sup>
            <a class="developers__link" href="https://vintage.agency/" target="_blank">
                <svg class="developers__logo">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-vintage-logo"></use>
                </svg>
            </a>
        </div>
    </div>
    <div class="wrap footer__advertising __web-inspector-hide-shortcut__">
        Обращаем внимание, что настоящий рекламный материал носит исключительно информационный характер и не является публичной офертой, определяемой положениями статьи 437 Гражданского кодекса РФ.
        <div>
            Общество с ограниченной ответственностью «ТАЛАН-ФИНАНС» (ОГРН 1117746313739) публикует информацию, подлежащую раскрытию эмитентами эмиссионных ценных бумаг, на странице в сети Интернет, предоставленной распространителем информации на рынке ценных бумаг ООО «Интерфакс-ЦРКИ», аккредитованным ЦБ РФ на раскрытие информации, по адресу: <a href = "http://www.e-disclosure.ru/portal/company.aspx?id=37631">http://www.e-disclosure.ru/portal/company.aspx?id=37631</a>
        </div>
    </div>
</footer>

<!-- for tablet -->


<!-- for mobile -->

<footer class="footer footer_mobile <?= $additionalClass ?>">
    <div class="footer__line"></div>
    <div class="wrap footer__wrap">
   
        
    <button class="footer__button ajax-link" data-href="<?= SiteUrlHelper::createFormCallbackPopupUrl() ?>">
            <?= t('_ask question', 'footer') ?>
        </button>
        <?= GetHeaderMenuWidget::widget(['currentCity' => $currentCity, 'position' => GetHeaderMenuWidget::POSITION_BOTTOM]) ?>
        <div class="social footer__social">
          
            <?php if ($currentCity->link_ig) : ?>
                <a href="<?= $currentCity->link_ig ?>" class="social__item socicon-instagram" target="_blank"></a>
            <?php endif; ?>
            <?php if ($currentCity->link_ok) : ?>
                <a href="<?= $currentCity->link_ok ?>" class="social__item socicon-odnoklassniki" target="_blank"></a>
            <?php endif; ?>
            <?php if ($currentCity->link_vk) : ?>
                <a href="<?= $currentCity->link_vk ?>" class="social__item socicon-vkontakte" target="_blank"></a>
            <?php endif; ?>
            <?php if ($currentCity->link_fb) : ?>
                <a href="<?= $currentCity->link_fb ?>" class="social__item socicon-facebook" target="_blank"></a>
            <?php endif; ?>
            <?php if ($currentCity->link_itunes) : ?>
                <a href="<?= $currentCity->link_itunes ?>" class="social__item socicon-appstore" target="_blank"></a>
            <?php endif; ?>
            <?php if ($currentCity->link_play) : ?>
                <a href="<?= $currentCity->link_play ?>" class="social__item socicon-play" target="_blank"></a>
            <?php endif; ?>
        </div>
        <nav class="submenu footer__submenu">
            <?= GetFooterMenuWidget::widget(['currentCity' => $currentCity])?>
        </nav>
        <div class="footer__copyright">
            <span class="copy">&copy;</span> <?= date('Y') ?>
            <?= t('_LTD «TALAN-IZHEVSK»', 'footer') ?>
        </div>
        <div class="developers footer__developers">
            <sup class="developers__by">by</sup>
            <a class="developers__link" href="https://vintage.agency/" target="_blank">
                <svg class="developers__logo">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-vintage-logo"></use>
                </svg>
            </a>
        </div>
    </div>
    <div class="wrap footer__advertising __web-inspector-hide-shortcut__">
        <div>Обращаем внимание, что настоящий рекламный материал носит исключительно информационный характер и не является публичной офертой, определяемой положениями статьи 437 Гражданского кодекса РФ.</div>
        <div style="margin-top: 10px;">
            Общество с ограниченной ответственностью «ТАЛАН-ФИНАНС» (ОГРН 1117746313739) публикует информацию, подлежащую раскрытию эмитентами эмиссионных ценных бумаг, на странице в сети Интернет, предоставленной распространителем информации на рынке ценных бумаг ООО «Интерфакс-ЦРКИ», аккредитованным ЦБ РФ на раскрытие информации, по адресу: <a href = "http://www.e-disclosure.ru/portal/company.aspx?id=37631">http://www.e-disclosure.ru/portal/company.aspx?id=37631</a>
        </div>
    </div>



</footer>

<!-- for mobile -->
