<?php

use common\models\PublicDataMenu;

/**
 * @var $this yii\web\View
 * @var $titleMobile string
 * @var $items common\models\PublicDataMenu[]|common\models\PublicData[]
 */
?>

<section class="sales__row" data-sticky_column>
    <div class="wrap wrap_small wrap_mobile_full">
        <div class="sales__category-btn"><?= $titleMobile ?></div>
        <div class="sales__select-block wow fadeInLeft">
            <?php foreach ($items as $item) : ?>
                <?php if ($item instanceof PublicDataMenu) : ?>
                    <div class="sales__select-item">
                        <a class="sales__select-btn"><span class="sales__select-btn_text"><?= $item->label ?></span></a>
                        <ul class="sales__lists">
                            <?php foreach ($item->publicData as $data) : ?>
                                <?php $alias = Yii::$app->request->get()['alias'] ?? false ?>
                                <li><a class="<?= $alias == $data->alias ? 'active js_state' : '' ?>" href="<?= $data->viewUrl() ?>"><span class="sales__select-btn_text"><?= $data->label ?></span></a></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                <?php else : ?>
                    <div class="sales__select-item">
                        <a href="<?= $item->viewUrl() ?>" class="sales__select-btn"><span class="sales__select-btn_text"><?= $item->label ?></span></a>
                    </div>
                <?php endif ?>

            <?php endforeach ?>
        </div>
    </div>
</section>
