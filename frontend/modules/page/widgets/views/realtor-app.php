<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $description string
 * @var $firstImage string
 * @var $secondImage string
 * @var $link string
 * @var $linkLabel string
 * @var $iOSlink string
 * @var $iOSlinkLabel string
 * @var $androidLink string
 * @var $androidLinkLabel string
 */
?>

<section class="download_app">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 download_app__title wow fadeInUp"><?= $title ?></h2>
        <div class="download_app__content">
            <div class="download_app__content-iphone wow fadeInLeft">
                <?php if ($firstImage) : ?>
                    <div class="download_app__content-img">
                        <img src="<?= $firstImage ?>" alt="iphone" title="phone">
                    </div>
                <?php endif ?>

                <?php if ($secondImage) : ?>
                    <div class="download_app__content-img">
                        <img src="<?= $secondImage ?>" alt="iphone" title="phone">
                    </div>
                <?php endif ?>
            </div>
            <div class="download_app__content-info">
                <?= $description ?>
                <?php if ($link && $linkLabel) : ?>
                    <div class="download_app__content-download wow fadeInUp">
                        <a href="<?= $link ?>" class="download_link download_app__content-download_link" download><?= $linkLabel ?></a>
                    </div>
                <?php endif ?>

                <div class="download_app__content-btns">
                    <?php if ($iOSlink && $iOSlinkLabel) : ?>
                        <div class="download_app__content-btn_wrap wow fadeInUp">
                            <a href="<?= $iOSlink ?>" class="button download_app__btn button_green" rel="nofollow" target="_blank">
                                <?= $iOSlinkLabel ?>
                                <span class="button__blip button__blip_hover"></span>
                                <span class="button__blip button__blip_click"></span>
                            </a>
                        </div>
                    <?php endif ?>

                    <?php if ($androidLink && $androidLinkLabel) : ?>
                        <div class="download_app__content-btn_wrap wow fadeInUp">
                            <a href="<?= $androidLink ?>" class="button download_app__btn button_green" rel="nofollow" target="_blank">
                                <?= $androidLinkLabel ?>
                                <span class="button__blip button__blip_hover"></span>
                                <span class="button__blip button__blip_click"></span>
                            </a>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</section>
