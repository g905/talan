<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $descriptionLabel string
 * @var $descriptionText string
 * @var $image string
 */
?>

<section class="intro">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 about__h2 wow fadeInRight"><?= $title ?></h2>
        <div class="intro__block clearfix">
            <div class="intro__item-left">
                <?php if ($descriptionLabel) : ?>
                    <h6 class="wow fadeInRight" data-wow-delay="0.3s" data-wow-duration="2s"><?= $descriptionLabel ?></h6>
                <?php endif ?>
                <p class="wow fadeInRight" data-wow-delay="1.3s" data-wow-duration="2s"><?= $descriptionText ?></p>
            </div>
            <?php if ($image) : ?>
                <div class="intro__item-right wow fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                    <img src="<?= $image ?>" alt="intro-img" width="844" height="450">
                </div>
            <?php endif ?>
        </div>
    </div>
</section>
