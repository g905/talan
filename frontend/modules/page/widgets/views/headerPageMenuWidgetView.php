<?php

/**
 * @var $rightItems array
 * @var $leftItems array
 * @var $modelId int
 * @var $modelType int
 * @author art
 */

$parsedCurrentUrl = parse_url($_SERVER['REQUEST_URI']);
$currentUrl = $parsedCurrentUrl['path'] ?? '';
$selectedItem = 1;
$i = 0;
if (!empty($leftItems)) {
    foreach ($leftItems as $item) {
        $i++;
        //$parsed = parse_url($item->link);
        $parsed = $item->link;

        $item->link = $parsed['path'] ?? $item->link;
//        dump($currentUrl);die();
////        if (strpos($item->link, "#")) {
////            $item->link = substr($item->link, 0, strpos($item->link, "#"));
////        }
        if ($currentUrl == $item->link) {
            $selectedItem = $i;
        }
    }
}
?>

<div class="header__row">
    <div class="wrap wrap_mobile_full">
        <div class="header__line"></div>
        <?php if (!empty($leftItems)) : ?>
            <div class="submenu header__submenu">
                <?php $i = 0;
                foreach ($leftItems as $item): $i++; ?>
                    <?php $parsed_item = parse_url($item->link)?>
                    <a href="<?= $item->link ?>" data-model-id="<?= $i ?>" data-model-type="<?= $modelType ?>" data-name="menu-item-link" class="submenu__item <?= ($i == $selectedItem) ? 'submenu__item_active' : '' ?>" 
                    	<?php if (isset($item->new_window) && $item->new_window) { 
                    			echo "target='blank'";
                    		} elseif (array_key_exists('scheme', $parsed_item) && in_array($parsed_item['scheme'], ['http', 'https'])) {
                                echo "target='_blank'" ;
                        }
                    	?>
                    	 >
                        <?= $item->label ?>
                    </a>
                <?php endforeach ?>
            </div>
            <div class="select select_submenu-fix header__select">
                <button class="select__btn" data-dropdown-cities-btn="about-submenu-fix">
                    <?= obtain([0, 'label'], $leftItems) ?>
                </button>
                <div class="select__options" data-dropdown-cities="about-submenu-fix">
                    <?php foreach ($leftItems as $item): ?>
                        <a href="<?= $item->link ?>" class="select__option"><?= $item->label ?></a>
                    <?php endforeach ?>
                </div>
            </div>
        <?php endif ?>
        <?php if (!empty($rightItems)) : ?>
            <?php foreach ($rightItems as $item): ?>
                <a href="<?= $item->link ?>" class="header__btn" 
                	 <?php if (isset($item->new_window) && $item->new_window) { 
                    			echo "target='blank'";
                    		}
                    	?>
                	>
                	<?= $item->label ?></a>
            <?php endforeach ?>
        <?php endif ?>
    </div>
</div>
