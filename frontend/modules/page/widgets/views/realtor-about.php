<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $description string
 * @var $facts common\models\blocks\RealtorFact[]
 */
?>

<section class="statistics">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 statistics__h2 wow fadeInUp"><?= $title ?></h2>
        <p class="statistics__description wow fadeInUp"><?= $description ?></p>
        <div class="statistics__list wow fadeInUp">
            <?php foreach ($facts as $fact) : ?>
                <div class="card statistics__card">
                    <h3 class="h3 card__h3"><span class="card_span"><?= $fact->sub_title ?></span></h3>
                    <h5 class="h5 card__h5"><?= $fact->title ?></h5>
                    <div class="divider card__divider"></div>
                    <div class="card__text"><?= $fact->description ?></div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
