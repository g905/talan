<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $image string
 * @var $description string
 * @var $link string
 * @var $linkLabel string
 * @var $btnLabel string
 */
?>

<section class="intro_realtors">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 intro_realtors__title wow fadeInLeft"><?= $title ?></h2>
    </div>
    <div class="intro_realtors__items">
        <div class="intro_realtors__items-left wow fadeIn" data-wow-delay=".2s">
            <?php if ($image) : ?>
                <img src="<?= $image ?>" alt="realtors_pic">
            <?php endif ?>
        </div>
        <div class="intro_realtors__items-right">
            <?php if ($description) : ?>
                <p class="intro_realtors__items-description wow fadeInRight" data-wow-delay=".3s"><?= $description ?></p>
            <?php endif ?>

            <?php if ($link && $linkLabel) : ?>
                <div class="intro_realtors__items-download wow fadeInRight" data-wow-delay=".4s">
                    <a href="<?= $link ?>" class="download_link intro_realtors__items-download_link" download><?= $linkLabel ?></a>
                </div>
            <?php endif ?>

            <?php if ($btnLabel) : ?>
                <button type="submit" class="button intro_realtors__btn button_green wow fadeInRight" data-wow-delay=".5s" data-modal-open="call-back">
                    <?= $btnLabel ?>
                    <span class="button__blip button__blip_hover"></span>
                    <span class="button__blip button__blip_click"></span>
                </button>
            <?php endif ?>
        </div>
    </div>
</section>
