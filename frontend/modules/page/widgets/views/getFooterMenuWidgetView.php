<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 19.12.2017
 * Time: 20:34
 */

/** @var \common\models\SiteMenu $menu */
/** @var \common\models\SiteMenuItem $menuItem */
?>

<nav class="submenu footer__submenu">
    <?php foreach ($menu->menuItems as $menuItem): ?>
    <a href="<?=$menuItem->getMenuItemLink()?>" class="submenu__link">
        <?=$menuItem->label?>
    </a>
    <?php endforeach; ?>
</nav>
