<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $files common\models\blocks\RealtorFile[]
 */
?>

<section class="download_presentation">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 download_presentation__title wow fadeInUp"><?= $title ?></h2>
        <div class="download_presentation__documents">
            <?php foreach ($files as $file) : ?>
                <div class="download_presentation__documents-item wow fadeInUp">
                <a href="<?= $file->getFileSrc() ?>" class="file__link" target="_blank">
                    <img src="/static/img/constructor/file.png" alt="">
                    <span><?= $file->title ?></span>
                </a>
            </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
