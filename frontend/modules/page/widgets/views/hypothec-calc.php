<?php
/**
 * @var $this yii\web\View
 * @var string $formOnSubmit
 * @var string $defaultTimeTerm
 * @var string $defaultInitialFee
 * @var string $defaultApartmentPrice
 * @var common\models\Bank[] $banks
 */

use common\helpers\SiteUrlHelper;
?>

<section class="calc stavka">
    <div class="title">
        <div class="wrap wrap_mobile_full">
            <h3 class="h3 wow fadeIn" data-wow-delay="0.5s" data-wow-duration="0.5s">Узнайте свою ставку</h3>
        </div>
    </div>
    <div class="calc__block ">
        <div class="wrap wrap_mobile_full">
            <form class="calc_form calc_form-ipoteka clearfix">
                <fieldset class="calc__fieldset--left wow fadeInLeft" data-wow-delay="0.7s" data-wow-duration="2s">
                    <legend>1) Введите данные:</legend>
                    <div class="filters__section clearfix">
                        <div class="filters__heading">Стоимость квартиры:</div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1000}' data-wnumb='{"decimals":"0", "thousand": " ", "postfix":" руб"}' data-range-min="50000" data-range-max="10000000" data-nouislider-type='price'>
                                <div class="ranges nouislider__ranges">
                                    <input class="ranges__input" placeholder="" type="text" value="<?= $defaultApartmentPrice ?>" name="square-max" data-no-ui-input> </div>
                                <div class="nouislider__slider"></div>
                            </div>
                        </div>
                    </div>
                    <div class="filters__section clearfix">
                        <div class="filters__heading">Сроки:</div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-wnumb='{"decimals":"0", "postfix":" мес"}' data-range-min="<?= $minTerm ? $minTerm : '1' ?>" data-range-max="<?= $maxTerm ? $maxTerm : '240' ?>" data-nouislider-type='term'>
                                <div class="ranges nouislider__ranges">
                                    <input class="ranges__input" placeholder="" value="<?= $defaultTimeTerm ?>" type="text" name="square-max" data-no-ui-input> </div>
                                <div class="nouislider__slider"></div>
                            </div>
                        </div>
                    </div>
                    <div class="filters__section clearfix">
                        <div class="filters__heading">
                            <span class="tabl-hidden">Первоначальный взнос:</span>
                            <span class="desk-hidden">Первонач. взнос:</span>
                        </div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-wnumb='{"decimals":"0", "postfix":"%"}' data-range-min="1" data-range-max="100" data-nouislider-type='first_value'>
                                <div class="ranges nouislider__ranges">
                                    <input class="ranges__input" placeholder="" value="<?= $defaultInitialFee ?>" type="text" name="square-max" data-no-ui-input> </div>
                                <div class="nouislider__slider"></div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="calc__fieldset--right wow fadeInRight" data-wow-delay="1.5s" data-wow-duration="2s">
                    <legend>2) Выберите банк:</legend>
                    <div class="bank-section" data-stavka-slider data-slick='{ "rows": 3,
                            "slidesPerRow": 4, "dots": false, "prevArrow": ".stavka .navigation__btn_prev", "nextArrow": ".stavka .navigation__btn_next","responsive": [{
                              "breakpoint": 1200,
                              "settings": {
                                "rows": 3,
                                "slidesPerRow": 3
                              }
                            }]}'>
                        <?php foreach ($banks as $i => $bank) : ?>
                            <?php $svg = $bank->getSvgImagePath() ?>
                            <div>
                                <div class="bank-section__item <?= $i === 0 ? 'active' : '' ?>" data-monthBid="<?= $bank->mortgage_rate ?>" data-bankname="<?= $bank->label ?>">
                                    <?= $svg ? @file_get_contents($svg) : '' ?>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                    <!-- /.bank-section -->
                    <div class="navigation__content">
                        <button type="button" class="navigation__btn navigation__btn_prev slick-arrow" data-stavka-arrow-prev="" style="display: inline-block;">
                            <span class="arrow arrow_left navigation__arrow"></span>
                        </button>
                        <div class="navigation__status">
                            <span class="navigation__current" data-stavka-status-current>01</span> \
                            <span class="navigation__total" data-stavka-status-total>02</span>
                        </div>
                        <button type="button" class="navigation__btn navigation__btn_next slick-arrow" data-stavka-arrow-next="" style="display: inline-block;">
                            <span class="arrow arrow_right navigation__arrow"></span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <form class="calc_form-bottom clearfix">
            <div class="wrap wrap_mobile_full">
                <fieldset class="calc__fieldset--bottom">
                    <div class="calc__calculated-block wbg wow fadeInUp" data-wow-delay="1s" data-wow-duration="2s">
                        <div class="bank-names">
                            Ставка<span class="bank-name"></span>
                        </div>
                        <div class="calc__calculated--row">
                            <div class="calc__month-payment">Ежемесячный платеж:</div>
                            <span class="calc__month-payment--price" data-wnumb='{"decimals": 0, "thousand":" ", "postfix":" р"}'>225 000 р</span>
                        </div>
                        <!--
                        <button type="button" class="send-request ajax-link" data-href="<?= SiteUrlHelper::createFormHypothecPopupUrl()?>">
                            <span class="tabl-hidden">Отправить заявку на ипотеку</span>
                            <span class="desk-hidden">Отправить заявку</span>
                        </button>
                        -->
                        <button type="button" class="send-request" data-href="" onclick="window.open('https://онлайнпокупка.талан.рф')">
                            <span class="tabl-hidden">Отправить заявку на ипотеку</span>
                            <span class="desk-hidden">Отправить заявку</span>
                        </button>
                    </div>
                </fieldset>
            </div>
        </form>
    </div>
</section>
