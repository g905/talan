<?php
/**
 * @var $this yii\web\View
 * @var string $title
 * @var common\models\blocks\InstallmentPlanBenefit[] $benefits
 */
?>

<section class="advantage values">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 new-quality__h2 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
            <?= $title ?>
        </h2>
        <div class="values__list">
            <?php foreach ($benefits as $benefit) : ?>
                <div class="card values__card wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                    <div class="card__img" data-bg-src="<?= $benefit->getImageSrc() ?>" data-bg-pos="center" data-bg-size="cover" style="background: url(<?= $benefit->getImageSrc() ?>) center center / cover no-repeat;"></div>
                    <h5 class="h5 card__h5"><?= $benefit->title ?></h5>
                    <div class="divider card__divider"></div>
                    <div class="card__text"><?= $benefit->description ?></div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
