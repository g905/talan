<?php
/**
 * @var $this yii\web\View
 * @var $menu common\models\SiteMenu
 * @var $menuItem common\models\SiteMenuItem
 * @var $items common\models\SiteMenuItem[]
 */
$currentUrl = $_SERVER['REQUEST_URI'];
$items = $menu->menuItems;
?>
<div class="header__row">
    <div class="wrap wrap_mobile_full">
        <div class="header__line"></div>
        <div class="submenu header__submenu">
            <?php foreach ($items as $menuItem) : ?>
                <a href="<?= $menuItem->getMenuItemLink() ?>" class="submenu__item <?= $menuItem->getMenuItemLink() == $currentUrl ? 'submenu__item_active' : '' ?>"><?= $menuItem->label ?></a>
            <?php endforeach ?>
        </div>
        <div class="select select_submenu-top header__select">
            <?php foreach ($items as $i => $item) : ?>
                <?php if ($item->getMenuItemLink() == $currentUrl) : ?>
                    <?php unset($items[$i]) ?>
                    <button class="select__btn" data-dropdown-cities-btn="about-submenu-top"><?= $item->label ?></button>
                <?php endif ?>
            <?php endforeach ?>
            <div class="select__options" data-dropdown-cities="about-submenu-top">
                <?php foreach ($items as $menuItem) : ?>
                    <a href="<?= $menuItem->getMenuItemLink() ?>" class="select__option"><?= $menuItem->label ?></a>
                <?php endforeach ?>
            </div>
        </div>
        <button data-href="/form-callback-popup" class="header__btn ajax-link"> Заказать звонок </button>
    </div>
</div>
