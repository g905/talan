<?php
/**
 * @var $this yii\web\View
 */
?>

<div class="wrap wrap_small wrap_mobile_full">
    <div class="random-table">
        <table>
            <tr>
                <td>№</td>
                <td>Город</td>
                <td>Сервис</td>
                <td>Подробнее</td>
                <td>Бросай</td>
                <td>И </td>
            </tr>
            <tr>
                <td>1</td>
                <td>Бахчисарай</td>
                <td>Чьоткий</td>
                <td>О том</td>
                <td>Курить</td>
                <td>Жизнью</td>
            </tr>
            <tr>
                <td>2</td>
                <td>СарайБахчи</td>
                <td>НеЧьоткий</td>
                <td>Про то</td>
                <td>Вставай</td>
                <td>Будешь</td>
            </tr>
            <tr>
                <td>3</td>
                <td>НеБахчи</td>
                <td>Чьоткий но НЕ</td>
                <td>НЕ про это</td>
                <td>На лыжи</td>
                <td>Не обижен</td>
            </tr>
        </table>
    </div>
</div>
