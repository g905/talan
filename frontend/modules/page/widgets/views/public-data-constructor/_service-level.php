<?php

use yii\helpers\Json;

/**
 * @var $this yii\web\View
 * @var $title string
 * @var $description string
 * @var $number string
 * @var $numberTitle string
 * @var $numberDescription string
 * @var $dataRows common\models\PublicDataWidget[]
 */
?>

<section class="service">
    <div class="wrap wrap_small wrap_mobile_full">
        <h3 class="h3 sales__h3 wow fadeInUp"><?= $title ?></h3>
        <div class="sales__paragraf wow fadeInRight"><?= $description ?></div>
        <div class="service__assessment wow fadeInLeft">
            <span class="service__label"><?= $number ?></span>
            <div class="service__descr">
                <div class="service__descr-title"><?= $numberTitle ?></div>
                <span class="service__descr-text"><?= $numberDescription ?></span>
            </div>
        </div>
        <div class="service__table-block wow fadeInUp">
            <table class="service__table service__table-head">
                <tr class="service__table-row">
                    <th>№</th>
                    <th>Город</th>
                    <th>Уровень сервиса</th>
                </tr>
            </table>
            <?php foreach ($dataRows as $i => $row) : ?>
                <table class="service__table">
                    <tr class=" service__table-row">
                        <td><?= $i + 1 ?></td>
                        <td><?= $row->city->label ?></td>
                        <td><?= $row->value ?>
                            <span class="service__arrow<?= $row->status == 1 ? '-up' : '' ?>"></span>
                        </td>
                    </tr>
                </table>
                <div class="service__chart">
                    <span class="service__btn"></span>
                    <div class="chart-block">
                        <div class="chart" style="position: relative; width:100%">
                            <canvas id="service-level-<?= $i ?>"></canvas>
                            <?php

                            $labels = [];
                            $dataset = [];

                            foreach ($row->chartData as $k => $data) {
                                $labels[$k] = $data->label;
                                $dataset[$k] = $data->value;
                            }

                            $labels = Json::encode($labels);
                            $dataset = Json::encode($dataset);

                            ?>

                            <?php $this->registerJs(<<<JS
                            new Chart($(".service canvas#service-level-$i"), {
                                    type: 'line',
                                    options: {
                                        responsive: true,
                                        legend: {display: true, labels: {fontColor: 'rgb(52, 52, 52)', fontSize: 10, fontFamily: "'Northern', 'Helvetica', 'Arial', sans-serif", fontWeight: 'bold'}},
                                        layout: {padding: {left: 0, right: 0, top: 0, bottom: 0}},
                                        scales: {yAxes: [{stacked: false}]}
                                    },
                                    data: {
                                        "labels":  $labels,
                                        "datasets": [{
                                            "label": "$row->chart_name",
                                            "data": $dataset,
                                            "backgroundColor": ["rgba(34,166,98, 0.2)"],
                                            "borderColor": ["rgba(34,166,98,1)"],
                                            "borderWidth": 1,
                                            "pointBackgroundColor": "rgba(34,166,98,1)",
                                            "tension": 0
                                        }]
                                    },
                                });
JS
                            ) ?>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
