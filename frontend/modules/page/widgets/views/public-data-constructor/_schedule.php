<?php
/**
 * @var $this yii\web\View
 */
?>
<section class="schedule" data-submit-url="<?= \common\helpers\SiteUrlHelper::getScheduleChartUrl() ?>">
    <div class="wrap wrap_small wrap_mobile_full">
        <h3 class="h3 sales__h3 wow fadeInUp"> <?= $title ?> </h3>
        <div class="vacancy__cities first-select wow fadeInRight" data-wow-delay="1s" data-wow-duration="1s">
            <div class="filters__section">
                <div class="filters__inner filters__inner_side_right">
                    <div class="custom-select filters__custom-select city_schedule" data-custom-select>
                        <select name="cities" class="city_select_schedule">
                            <?php if ($cities) : ?>
                                <?php foreach ($cities as $city) : ?>
                                    <option value="<?= $city->id ?>"><?= $city->label ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="vacancy__cities wow fadeInRight" data-wow-delay="1s" data-wow-duration="1s">
            <div class="filters__section">
                <div class="filters__inner filters__inner_side_right">
                    <div class="custom-select filters__custom-select complex_schedule" data-custom-select>
                        <select name="cities" class="complex_select_schedule">
                            <?php if ($complexes) : ?>
                                <?php foreach ($complexes as $complex) : ?>
                                    <option data-cityid="<?= $complex->city->id ?>" value="<?= $complex->scheduleChart->id ?>"><?= $complex->label ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($description) : ?>
            <div class="sales__paragraf wow fadeInLeft"> <?= $description ?> </div>
        <?php endif; ?>
        <div class="render_schedule"></div>
    </div>
</section>
