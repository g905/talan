<?php

use yii\helpers\Json;

/**
 * @var $this yii\web\View
 * @var $title string
 * @var $description string
 * @var $dataRows common\models\PublicDataWidget[]
 */
?>

<section class="disclosure">
    <div class="wrap wrap_small wrap_mobile_full">
        <h3 class="h3 sales__h3 wow fadeInUp"><?= $title ?></h3>
        <div class="sales__paragraf wow fadeInLeft"><?= $description ?></div>
        <div class="service__table-block wow fadeInUp">
            <table class="service__table service__table-head">
                <tr class="service__table-row">
                    <th>№</th>
                    <th>Город</th>
                    <th>Уровень сервиса</th>
                </tr>
            </table>
            <?php foreach ($dataRows as $i => $row) : ?>
                <table class="service__table">
                    <tr class=" service__table-row">
                        <td><?= $i + 1 ?></td>
                        <td><?= $row->city->label ?></td>
                        <td>
                            <?= $row->value ?>
                            <span class="service__arrow<?= $row->status == 1 ? '-up' : '' ?>"></span>
                        </td>
                    </tr>
                </table>
                <div class="service__diagramm">

                    <span class="disclosure__btn"></span>
                    <div class="chart-block">
                        <div class="chart-bar" style="position: relative; width: 100%;">
                            <canvas></canvas>
                            <?php

                            $columns = [];
                            $labels = explode(',', $row->chart_name);
                            $dataset = ['first' => [], 'second' => [], 'third' => []];
                            $colors = explode('|', $row->seria);

                            foreach ($row->chartData as $k => $data) {
                                $values = explode(',', $data->value);
                                $dataset['first']['data'][$k] = obtain(0, $values);
                                $dataset['second']['data'][$k] = obtain(1, $values);
                                $dataset['third']['data'][$k] = obtain(2, $values);
                                $columns[] = mb_convert_case(
                                    formatter()->asDate(DateTime::createFromFormat('m-Y', $data->label), 'MMM Y'),
                                    MB_CASE_TITLE,
                                    'UTF-8'
                                );
                            }

                            $dataset['first']['label'] = obtain(0, $labels);
                            $dataset['first']['backgroundColor'] = obtain(0, $colors);
                            $dataset['second']['label'] = obtain(1, $labels);
                            $dataset['second']['backgroundColor'] = obtain(1, $colors);
                            $dataset['third']['label'] = obtain(2, $labels);
                            $dataset['third']['backgroundColor'] = obtain(2, $colors);
                            $dataset1 = Json::encode(obtain('first', $dataset), JSON_NUMERIC_CHECK);
                            $dataset2 = Json::encode(obtain('second', $dataset), JSON_NUMERIC_CHECK);
                            $dataset3 = Json::encode(obtain('third', $dataset), JSON_NUMERIC_CHECK);
                            $columns = Json::encode($columns);
                            ?>
                            <script type="application/json">
                                {
                                    "labels": <?= $columns ?>,
                                    "datasets": [
                                        <?= $dataset1 ?>,
                                        <?= $dataset2 ?>,
                                        <?= $dataset3 ?>
                                    ]
                                }
                            </script>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
