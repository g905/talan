<?php

use yii\bootstrap\ActiveForm;
use common\helpers\SiteUrlHelper;
use frontend\modules\form\models\FormPublicDataFile;

/**
 * @var $this yii\web\View
 * @var $file common\models\FpmFile
 */
$model = new FormPublicDataFile();
?>

<div class="modal modals__modal" data-modal="file-<?= $file->id ?>">
    <div class="modal__outer">
        <div class="modal__overlay" data-modal-close></div>
        <div class="modal__inner modal__file">
            <div class="modal__content">
                <div class="popup">
                    <button class="modal__close" data-modal-close></button>
                    <h5 class="h5 popup__h5"> Чтобы скачать файл <span>введите пароль</span></h5>
                    <div class="popup__text">
                        <p>Если у вас нет пароля, свяжитесь с нами.</p>
                    </div>
                    <?php $form = ActiveForm::begin([
                        'action' => SiteUrlHelper::getPublicDataDownloadFileUrl($file->id),
                        'options' => [
                            'id' => "pd-file-form-{$file->id}",
                            'class' => "form popup__form ajax-form public-data-file-{$file->id}",
                        ],
                        'enableAjaxValidation' => true,
                        //    'validateOnBlur' => true,
                        //    'validateOnChange' => true,
                        //    'validateOnSubmit' => false,
                        'enableClientValidation' => false,
                        'fieldConfig' => ['options' => ['tag' => false]],
                    ]) ?>
                    <form class="" method="POST" action="<?= SiteUrlHelper::getPublicDataDownloadFileUrl($file->id) ?>">
                        <?= $form->field($model, 'filePass')->passwordInput(['placeholder' => 'Пароль', 'class' => 'form__input', 'id' => "formpublicdatafile-filepass-{$file->id}", 'autocomplete' => 'new-password'])->label(false) ?>
                        <button class="button button_green form__button" type="submit">
                            Cкачать
                            <span class="button__blip button__blip_hover"></span>
                            <span class="button__blip button__blip_click"></span>
                        </button>
                    <?php $form::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
