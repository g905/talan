<?php

use common\helpers\SiteUrlHelper;

/**
 * @var $this yii\web\View
 * @var $title string
 * @var $description string
 * @var $cities common\models\City
 * @var $dropdownData array
 */
?>

<section class="quantity" data-submit-url="<?= SiteUrlHelper::getChartUrl() ?>">
    <div class="wrap wrap_small wrap_mobile_full">
        <h3 class="h3 sales__h3 wow fadeInUp"> <?= $title ?> </h3>
        <div class="vacancy__cities first-select wow fadeInRight" data-wow-delay="1s" data-wow-duration="1s">
            <div class="filters__section">
                <div class="filters__inner filters__inner_side_right">
                    <div class="custom-select filters__custom-select city_ddu" data-custom-select>
                        <select name="cities" class="city_select_ddu">
                            <?php if ($dropdownData) : ?>
                                <?php $outputtedCities = []; ?>
                                <?php foreach ($dropdownData as $diagram => $data) : ?>
                                    <?php $currentCityId = obtain(['city', 'id'], $data); ?>
                                    <?php if (!in_array($currentCityId, $outputtedCities)) : ?>
                                        <?php $outputtedCities[] = $currentCityId; ?>
                                        <option value="<?= $currentCityId ?>"><?= obtain(['city', 'label'], $data) ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="vacancy__cities wow fadeInRight" data-wow-delay="1s" data-wow-duration="1s">
            <div class="filters__section">
                <div class="filters__inner filters__inner_side_right">
                    <div class="custom-select filters__custom-select complex_ddu" data-custom-select>
                        <select name="cities" class="complex_select_ddu">
                            <?php if ($dropdownData) : ?>
                                <?php foreach ($dropdownData as $diagram => $data) : ?>
                                    <?php $currentComplexLabel = obtain(['complex', 'label'], $data); ?>
                                    <option data-cityid="<?= obtain(['city', 'id'], $data) ?>" value="<?= $diagram ?>"><?= $currentComplexLabel ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="sales__block-info">
            <div class="sales__paragraf wow fadeInRight"> <?= $description ?> </div>
        </div>
        <div class="chart-block">
            <div class="chart render wow fadeInUp" style="position: relative; width:100%"></div>
        </div>
    </div>
</section>
