<?php

use metalguardian\fileProcessor\helpers\FPM;

/**
 * @var $this yii\web\View
 * @var $title string
 * @var $description string
 * @var $files common\models\FpmFile[]
 */
?>

<section class="file">
    <div class="wrap wrap_small wrap_mobile_full">
        <h3 class="h3 sales__h3 wow fadeInUp"><?= $title ?></h3>
        <div class="sales__paragraf wow fadeInLeft"><?= $description ?></div>
        <?php if ($files) : ?>
            <?php foreach ($files as $file) : ?>
                <?php $meta = $file->metaData; ?>
                <?php if ($meta) : ?>
                    <?php $this->params['footerModals'][] = $this->render('_files-with-description-modal', compact('file')) ?>
                <?php endif ?>
                <div class="file__link-block wow fadeInRight">
                    <a href="<?= $meta ? '#' : FPM::originalSrc($file->id) ?>" class="file__link" <?= $meta ? "data-modal-open=\"file-{$file->id}\"" : '' ?>>
                        <img src="/static/img/constructor/file.png" alt="file">
                        <span><?= $file->base_name ?> (<?= strtoupper($file->extension) ?> <?= $file->fileSize() ?>) </span>
                    </a>
                </div>
            <?php endforeach ?>
        <?php endif ?>
    </div>
</section>
