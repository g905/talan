<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $description string
 */
?>

<section class="letter">
    <div class="wrap wrap_small wrap_mobile_full">
        <div class="letter__block  wow fadeInDown"><?= $title ?></div>
        <div class="letter__descr wow fadeInUp"><?= $description ?></div>
    </div>
</section>
