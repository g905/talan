<?php

use yii\helpers\Json;

/**
 * @var $this yii\web\View
 * @var $label string
 * @var $labels array
 * @var $dataset array
 * @var $colors array
 */
?>

<section class="service__diagramm">
    <div class="wrap wrap_small wrap_mobile_full">
        <h3 class="h3 sales__h3 wow fadeInUp"><?= $label ?></h3>
        <div class="chart-block chart-block-pie">
            <div class="chart-pie  wow fadeInUp" style="position: relative; width:70%">
                <canvas></canvas>
                <script type="application/json">
                    {
                        "labels": <?= $labels ?>,
                        "datasets": [
                            {
                                "data": <?= $dataset ?>,
                                "backgroundColor": <?= $colors ?>
                            }]
                    }
                </script>
            </div>
        </div>
    </div>
</section>
