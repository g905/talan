<?php

use yii\helpers\Json;

/**
 * @var $this yii\web\View
 * @var $label string
 * @var $chart common\models\PublicDataWidget
 */
?>

<section class="service__diagramm">
    <div class="wrap wrap_small wrap_mobile_full">
        <h3 class="h3 sales__h3 wow fadeInUp"><?= $label ?></h3>
        <div class="chart-block chart-block-bar">
            <div class="chart-bar wow fadeInUp" style="position: relative; width: 100%;">
                <canvas></canvas>
                <?php

                $columns = [];
                $labels = explode(',', $chart->chart_name);
                $dataset = ['first' => [], 'second' => [], 'third' => []];
                $colors = explode('|', $chart->seria);

                foreach ($chart->chartData as $k => $data) {
                    $values = explode(',', $data->value);
                    $dataset['first']['data'][$k] = obtain(0, $values);
                    $dataset['second']['data'][$k] = obtain(1, $values);
                    $dataset['third']['data'][$k] = obtain(2, $values);
                    $columns[] = mb_convert_case(
                        formatter()->asDate(DateTime::createFromFormat('m-Y', $data->label), 'MMM Y'),
                        MB_CASE_TITLE,
                        'UTF-8'
                    );
                }

                $dataset['first']['label'] = obtain(0, $labels);
                $dataset['first']['backgroundColor'] = obtain(0, $colors);
                $dataset['second']['label'] = obtain(1, $labels);
                $dataset['second']['backgroundColor'] = obtain(1, $colors);
                $dataset['third']['label'] = obtain(2, $labels);
                $dataset['third']['backgroundColor'] = obtain(2, $colors);
                $dataset1 = Json::encode(obtain('first', $dataset), JSON_NUMERIC_CHECK);
                $dataset2 = Json::encode(obtain('second', $dataset), JSON_NUMERIC_CHECK);
                $dataset3 = Json::encode(obtain('third', $dataset), JSON_NUMERIC_CHECK);
                $columns = Json::encode($columns);
                ?>
                <script type="application/json">
                    {
                        "labels": <?= $columns ?>,
                        "datasets": [
                            <?= $dataset1 ?>,
                            <?= $dataset2 ?>,
                            <?= $dataset3 ?>
                        ]
                    }
                </script>
            </div>
        </div>
    </div>
</section>
