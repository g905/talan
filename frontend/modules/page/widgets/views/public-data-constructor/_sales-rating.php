<?php

use common\helpers\SiteUrlHelper;

/**
 * @var $this yii\web\View
 * @var $title string
 * @var $description string
 * @var $buttonLink string
 * @var $buttonLabel string
 * @var $dropdownData array
 */
?>

<section class="sales rating" data-submit-url="<?= SiteUrlHelper::getRatingUrl() ?>">
    <div class="wrap wrap_small wrap_mobile_full">
        <div class="sales__block">
            <div class="sales__block-description">
                <h3 class="h3 sales__h3 wow fadeInUp"> <?= $title ?> </h3>
                <div class="vacancy__cities wow fadeInRight" data-wow-delay="1s" data-wow-duration="1s">
                    <div class="filters__section">
                        <div class="filters__inner filters__inner_side_right">
                            <div class="custom-select filters__custom-select" data-custom-select>
                                <select name="cities" class="city_select_rating">
                                    <?php if ($dropdownData) : ?>
                                        <?php foreach ($dropdownData as $tableId => $cityLabel) : ?>
                                            <option value="<?= $tableId ?>"><?= $cityLabel ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sales__block-info">
                    <div class="sales__paragraf wow fadeInRight"><?= $description ?></div>
                    <div class="sales__table-block wow fadeInDown render_rating"></div>
                    <?php if ($buttonLink && $buttonLabel) : ?>
                        <a href="<?= $buttonLink ?>" rel="nofollow" class="button sales__button button_green get_to_us__item-btn">
                            <span class="button__blip button__blip_hover"></span>
                            <span class="button__blip button__blip_click"></span>
                            <?= $buttonLabel ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
