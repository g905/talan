<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $description string
 * @var $dataRows common\models\PublicDataWidget[]
 */

use yii\helpers\Json;

?>

<section class="disclosure">
    <div class="wrap wrap_small wrap_mobile_full">
        <h3 class="h3 sales__h3 wow fadeInUp"><?= $title ?></h3>
        <div class="sales__paragraf wow fadeInLeft"><?= $description ?></div>
        <div class="service__table-block wow fadeInUp">
            <table class="service__table service__table-head">
                <tr class="service__table-row">
                    <th>№</th>
                    <th>Город</th>
                    <th>Уровень сервиса</th>
                </tr>
            </table>
            <?php foreach ($dataRows as $i => $row) : ?>
                <table class="service__table">
                    <tr class=" service__table-row">
                        <td><?= $i + 1 ?></td>
                        <td><?= $row->city->label ?></td>
                        <td>
                            <?= $row->value ?>
                            <span class="service__arrow<?= $row->status == 1 ? '-up' : '' ?>"></span>
                        </td>
                    </tr>
                </table>
                <div class="service__diagramm">
                    <span class="disclosure__btn"></span>
                    <div class="chart-block">
                        <div class="chart-pie" style="position: relative; width:70%">
                            <canvas></canvas>
                            <?php

                            $labels = [];
                            $dataset = [];
                            $colors = [];

                            foreach ($row->chartData as $k => $data) {
                                $labels[$k] = $data->label;
                                $dataset[$k] = $data->value;
                                $colors[$k] = $data->color;
                            }

                            $labels = Json::encode($labels);
                            $dataset = Json::encode($dataset);
                            $colors = Json::encode($colors);

                            ?>
                            <script type="application/json">{
                                "labels": <?= $labels ?>,
                                "datasets": [{"data": <?= $dataset ?>, "backgroundColor": <?= $colors ?>}]}
                            </script>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
