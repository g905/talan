<?php

use metalguardian\fileProcessor\helpers\FPM;

/**
 * @var $this yii\web\View
 * @var $content string
 * @var $preview int
 * @var $video string
 */
?>

<section class="article article_constructor">
    <div class="wrap wrap_small wrap_mobile_full">
        <div class="article__content wow fadeInUp" data-wow-delay="0.5s">
            <?= $content ?>
            <?php if ($video) : ?>
                <div class="video">
                    <div data-youtube="<?= $video ?>" data-bg-src="<?= FPM::originalSrc($preview) ?>" data-bg-size="cover" data-bg-pos="center">
                        <div class="play video__play">
                            <div class="play__blip play__blip_hover"></div>
                            <div class="play__blip play__blip_click"></div>
                        </div>
                        <iframe src="" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</section>
