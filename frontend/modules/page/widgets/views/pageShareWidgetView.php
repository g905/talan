<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 25.12.2017
 * Time: 14:37
 */

use frontend\modules\page\widgets\PageShareWidget;

/** @var integer $orientation */
/** @var string $windowOptions */

/* links from share42 */
?>
<section class="wow fadeIn share <?= ($orientation == PageShareWidget::ORIENTATION_HORIZONTAL) ? 'share_horizontal' : 'share_vertical'?>">

    <?php if ($orientation == PageShareWidget::ORIENTATION_VERTICAL): ?>
    <div class="share__text">
        <?=Yii::t('front/share', '_share')?>
    </div>
    <?php endif; ?>

    <?php if ($orientation == PageShareWidget::ORIENTATION_HORIZONTAL): ?>
    <div class="share__line"></div>
    <?php endif; ?>

    <div class="social share__social">
        <a class="social__item socicon-vkontakte pointer" onclick="window.open('//vk.com/share.php?url='+location.href+'&title='+document.title+'&description=',  '_blank', '<?=$windowOptions?>')" target="_blank"></a>
        <a class="social__item socicon-facebook pointer" onclick="window.open('//www.facebook.com/sharer/sharer.php?u='+location.href,  '_blank', '<?=$windowOptions?>')" target="_blank"></a>
        <a class="social__item socicon-odnoklassniki pointer" onclick="window.open('//ok.ru/dk?st.cmd=addShare&st._surl='+location.href+'&title='+document.title,  '_blank', '<?=$windowOptions?>')" target="_blank"></a>
<!--        <a href="#" class="social__item socicon-instagram" target="_blank"></a>-->
    </div>
    <?php if ($orientation == PageShareWidget::ORIENTATION_HORIZONTAL): ?>
        <div class="share__text">
            <?=Yii::t('front/share', '_share')?>
        </div>
    <?php endif; ?>
</section>
