<?php

/**
 * @var $this yii\web\View
 * @var $title string
 * @var $salesTitle string
 * @var $salesPhones string
 * @var $receptionTitle string
 * @var $receptionPhones string
 * @var $addressTitle string
 * @var $address string
 * @var $emailTitle string
 * @var $email string
 * @var $timetableTitle string
 * @var $timetable string
 * @var string $mapOptions
 */
?>
<section class="contacts">
    <div class="wrap wrap_mobile_full">
        <h2 class="contacts__title h2 wow fadeInLeft"><?= $title ?></h2>
    </div>
    <div class="contacts__wrap">
        <div class="contacts__wrap-map wow fadeInLeft" data-wow-delay="0.2s">
            <?php if ($mapImage):?>
                <div id="map" class="contacts__map wow fadeIn" data-map-options='<?= $mapOptions ?>' style="background: url(<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($mapImage->file_id ?? ''); ?>)" onclick="map.data.onApiLoaded = 1; map.init();"></div>
            <?php else: ?>
                <div id="map" class="contacts__map wow fadeIn" data-url="" data-map-options='<?= $mapOptions ?>'></div>
            <?php endif; ?>

        </div>
        <div class="contacts__wrap-contacts wow fadeInRight">
            <div class="contacts__wrap-tel">
                <p class="contacts__wrap-group"><?= $salesTitle ?></p>
                <?= $salesPhones ?>
            </div>
            <div class="contacts__wrap-tel">
                <p class="contacts__wrap-group"><?= $receptionTitle ?></p>
                <?= $receptionPhones ?>
            </div>
            <div class="contacts__wrap-address contacts__wrap-item">
                <p class="text"><?= $addressTitle ?></p>
                <?= $address ?>
            </div>
            <div class="contacts__wrap-email contacts__wrap-item">
                <p class="text"><?= $emailTitle ?></p>
                <a href="mailto:<?= $email ?>"><?= $email ?></a>
            </div>
            <div class="contacts__wrap-work_time contacts__wrap-item">
                <p class="text"><?= $timetableTitle ?></p>
                <div class="contacts__wrap-time"><?= $timetable ?></div>
            </div>
        </div>
    </div>
</section>

<?php


?>