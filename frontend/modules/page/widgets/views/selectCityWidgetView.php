<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 18.12.2017
 * Time: 23:45
 */

use morphos\Russian\GeographicalNamesInflection;

/** @var \common\models\City $currentCity */
/** @var \common\models\City[] $cityArray */
/** @var string[] $positionClass */
/** @var string $positionMorpher */
/** @var string $positionPrefix */
?>


<div class="select select_<?= $positionClass[0].' '.$positionClass[1]?>">
    <button class="select__btn" data-dropdown-cities-btn="<?= $positionClass[0]?>">
        <?= $positionPrefix.GeographicalNamesInflection::getCases($currentCity->label)[$positionMorphos];  ?>
    </button>
    <div class="select__options" data-dropdown-cities="<?= $positionClass[0]?>">
        <?php foreach ($cityArray as $city): ?>
        <a href="<?= $city->getCitySiteUrl()?>" class="select__option" data-old-id="<?= $city->getOldCityId() ?>">
            <?= $positionPrefix.GeographicalNamesInflection::getCases($city->label)[$positionMorphos];  ?>
        </a>
        <?php endforeach; ?>
    </div>
</div>

