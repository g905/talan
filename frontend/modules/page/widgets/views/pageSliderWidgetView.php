<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 18.12.2017
 * Time: 19:22
 */
/** @var string $videoPath */
/** @var string $imagePath */

?>
<video class="poster__bg poster__bg_video" autoplay loop muted data-video-src="<?= $videoPath?>" data-poster-video data-autoplay data-keepplaying></video>
<div class="poster__bg poster__bg_photo" data-bg-src="<?=$imagePath?>" data-bg-pos="center" data-bg-size="cover"></div>
<div class="poster__cover"></div>
