<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $steps common\models\blocks\ContactPageHowToGetStep[]
 */
?>

<section class="get_to_us">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 get_to_us__title wow fadeInUp"><?= $title ?></h2>
        <ul class="get_to_us__steps">
            <?php foreach ($steps as $i => $step) : ?>
                <li class="get_to_us__item">
                    <h5 class="wow fadeInUp"><?= $step->title ?></h5>
                    <div class="get_to_us__item-number wow fadeInUp">
                        <span>0<?= ++$i ?></span>
                    </div>
                    <p class="p get_to_us__item-description wow fadeInUp"><?= $step->description ?></p>
                    <?php if ($step->sub_title && $step->link) : ?>
                        <a href="<?= $step->link ?>" target="_blank" rel="nofollow" class="button button_green get_to_us__item-btn wow fadeInUp">
                            <span class="button__blip button__blip_hover"></span>
                            <span class="button__blip button__blip_click"></span>
                            <?= $step->sub_title ?>
                        </a>
                    <?php endif ?>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
</section>
