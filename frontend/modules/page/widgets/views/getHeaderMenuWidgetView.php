<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 19.12.2017
 * Time: 0:34
 */

use frontend\modules\page\widgets\GetHeaderMenuWidget;

/** @var string $positionClass */
/** @var integer $position */
/** @var \common\models\SiteMenu $menu */
/** @var \common\models\SiteMenuItem $menuItem */

$currentUrl = $_SERVER['REQUEST_URI'];
?>
<nav class="nav <?= $positionClass?>">
    <?php if ($position != GetHeaderMenuWidget::POSITION_BOTTOM): ?>
    <div class="nav__wrap">
        <?php foreach ($menu->menuItems as $menuItem): ?>
        <?php $activeClass = $menuItem->getMenuItemLink() == $currentUrl ? 'active_nav' : '' ?>
        <a href="<?=$menuItem->getMenuItemLink()?>" class="nav__link <?= $activeClass ?>">
            <?=$menuItem->label?>
        </a>
        <?php endforeach; ?>
    </div>
   <?php else: ?>
        <?php foreach ($menu->menuItems as $menuItem): ?>
            <?php $activeClass = $menuItem->getMenuItemLink() == $currentUrl ? 'active_nav' : '' ?>
            <a href="<?=$menuItem->getMenuItemLink()?>" class="nav__link <?= $activeClass ?>">
                <?=$menuItem->label?>
            </a>
        <?php endforeach; ?>
   <?php endif; ?>
</nav>
<?php if ($position == GetHeaderMenuWidget::POSITION_FIXED_PANEL) : ?>
    <div class="social panel-menu__social">
        <?php if ($currentCity->link_ig) : ?>
            <a href="<?= $currentCity->link_ig ?>" class="social__item socicon-instagram" target="_blank"></a>
        <?php endif; ?>
        <?php if ($currentCity->link_ok) : ?>
            <a href="<?= $currentCity->link_ok ?>" class="social__item socicon-odnoklassniki" target="_blank"></a>
        <?php endif; ?>
        <?php if ($currentCity->link_vk) : ?>
            <a href="<?= $currentCity->link_vk ?>" class="social__item socicon-vkontakte" target="_blank"></a>
        <?php endif; ?>
        <?php if ($currentCity->link_fb) : ?>
            <a href="<?= $currentCity->link_fb ?>" class="social__item socicon-facebook" target="_blank"></a>
        <?php endif; ?>
        <?php if ($currentCity->link_itunes) : ?>
            <a href="<?= $currentCity->link_itunes ?>" class="social__item socicon-appstore" target="_blank"></a>
        <?php endif; ?>
        <?php if ($currentCity->link_play) : ?>
            <a href="<?= $currentCity->link_play ?>" class="social__item socicon-play" target="_blank"></a>
        <?php endif; ?>
    </div>
<?php endif; ?>
