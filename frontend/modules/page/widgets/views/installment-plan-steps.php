<?php
/**
 * @var $this yii\web\View
 * @var string $title
 * @var common\models\blocks\InstallmentPlanStep[] $steps
 */
?>

<section class="zayavka statistics">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 values__h2 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
            <?= $title ?>
        </h2>
        <div class="statistics__list">
            <?php foreach ($steps as $i => $step) : ?>
                <div class="card statistics__card wow fadeIn" data-wow-delay="0s" style="visibility: visible; animation-delay: 0s; animation-name: fadeIn;">
                    <h3 class="h3 card__h3"><span class="card_span"><?= ++$i ?></span></h3>
                    <h5 class="h5 card__h5"><?= $step->title ?></h5>
                    <div class="divider card__divider"></div>
                    <div class="card__text"><?= $step->description ?></div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
