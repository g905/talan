<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $firstForm common\models\ContactForm
 * @var $forms common\models\ContactForm[]
 */
?>

<section class="get_in_touch">
    <div class="get_in_touch__wrap wrap wrap_mobile_full">
        <h2 class="h2 get_in_touch__title wow fadeInUp"><?= $title ?></h2>
        <div class="get_in_touch__tabs wow fadeInUp">
            <div class="get_in_touch__tabs-col">
                <ul class="get_in_touch__tabs-menu">
                    <li class="get_in_touch__tabs-btn get_in_touch__tabs-btn-active"><?= $firstForm->nav_label ?></li>
                    <?php if ($forms) : ?>
                        <?php foreach ($forms as $i => $form) : ?>
                            <li class="get_in_touch__tabs-btn"><?= $form->nav_label ?></li>
                        <?php endforeach ?>
                    <?php endif ?>
                </ul>
            </div>
            <div class="get_in_touch__tabs-col">
                <div class="dropdown_item">
                    <input id="menu_title" hidden>
                    <span class="dropdown_item__text"><?= $firstForm->nav_label ?></span>
                    <div class="dropdown_item__menu">
                        <div class="dropdown_item__link"><?= $firstForm->nav_label ?></div>
                        <?php if ($forms) : ?>
                            <?php foreach ($forms as $form) : ?>
                                <div class="dropdown_item__link"><?= $form->nav_label ?></div>
                            <?php endforeach ?>
                        <?php endif ?>
                    </div>
                </div>
                <ul class="get_in_touch__tabs-tabs">
                    <?= $this->render('_contact-page-form', ['model' => $firstForm, 'manager' => $firstForm->manager, 'active' => true]) ?>
                    <?php if ($forms) : ?>
                        <?php foreach ($forms as $form) : ?>
                            <?= $this->render('_contact-page-form', ['model' => $form, 'manager' => $form->manager, 'active' => false]) ?>
                        <?php endforeach ?>
                    <?php endif ?>
                </ul>
            </div>
        </div>
    </div>
</section>
