<?php
/**
 * @var $this yii\web\View
 * @var string $title
 * @var string $description
 * @var string $image
 */
?>

<section class="rules econom">
    <div class="rules__list">
        <div class="card  rules__card">
            <div class="wrap wrap_mobile_full">
                <div class="card__inner">
                    <div class="card__content">
                        <h3 class="h3 card__h3 wow fadeInLeft"><span class="card_span"><?= $title ?></span></h3>
                        <div class="card__text wow fadeInLeft" data-wow-delay="0.8s"><?= $description ?></div>
                    </div>
                </div>
                <?php if ($image) : ?>
                    <div class="card__img wow fadeIn" data-bg-src="<?= $image ?>" data-bg-pos="center bottom" data-bg-size="cover" data-wow-duration="2s" data-wow-delay="0.5s"></div>
                <?php endif ?>
            </div>
        </div>
    </div>
</section>
