<?php

use common\models\HowBuyPage;

/**
 * @var HowBuyPage $model
 * @var array $howBlocks
 */
?>

<div class="how-to-buy">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 how-to-buy__h2 wow fadeInLeft"><?= $model->label ?></h2>
        <div class="how-to-buy__list">
            <?php foreach ($howBlocks as $block) : ?>
                <a <?= $block->link ? 'href="'.$block->link.'"' : '' ?>  class="card how-to-buy__card wow fadeInUp" style="background-color: <?= $block->color ?>;">
                    <h3 class="h3 card__h3"><span class="card_span"><?= $block->title ?></span></h3>
                    <div class="card__text">
                        <?= $block->description ?>
                    </div>
                    <?php if ($block->link) : ?>
                        <div class="card__more">
                            <?= $block->link_name ?>
                        </div>
                    <?php endif; ?>
                </a>
            <?php endforeach ?>
        </div>
    </div>
</div>