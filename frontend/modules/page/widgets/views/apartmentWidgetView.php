<?php

use metalguardian\fileProcessor\helpers\FPM;

?>

<div class="vidjet">
    <div class="vidjet__body">
        <div class="vidjet__item">
            <a href="#" data-href="/form-callback-popup" class="vidjet__link ajax-link"></a>
            <div class="vidjet__img">
                <img src="<?= $callIcon ?>"> </div>
            <div class="vidjet__desc"> <?= Yii::t('apartment-complex', 'Заказать звонок') ?> </div>
        </div>
        <?php foreach ($model->apartmentComplexWidget as $item) : ?>
            <div class="vidjet__item">
                <a href="<?= $item->link ?>" class="vidjet__link"></a>
                <div class="vidjet__img">
                    <img src="<?= FPM::originalSrc($item->image->file_id ?? null) ?>"> </div>
                <div class="vidjet__desc"> <?= $item->label ?> </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
