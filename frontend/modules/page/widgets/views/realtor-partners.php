<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $partners common\models\blocks\RealtorPartner[]
 */
?>

<section class="partners">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 partners__title wow fadeInUp"><?= $title ?></h2>
        <div class="partners__slider_wrap wow fadeInUp">
            <div class="partners__slider" data-partners-slider data-slick='{"slidesToShow": 1,"rows":2, "cssEase": "cubic-bezier(.74,.1,.32,.98)", "slidesPerRow": 4, "slidesToScroll": 1, "dots": false, "prevArrow": ".partners .navigation__btn_prev", "nextArrow": ".partners .navigation__btn_next", "responsive": [ { "breakpoint": 1200, "settings": {"slidesPerRow": 2} }, { "breakpoint": 767, "settings": { "slidesToShow": 1, "rows": 2 , "slidesPerRow": 1 }}]}'>
                <?php foreach ($partners as $partner) : ?>
                    <div class="partners__slide">
                    <div class="partners__slide-content">
                        <img src="<?= $partner->getImageSrc() ?>" alt="logo" title="partner_logo">
                        <div class="partners__slide-info">
                            <p class="partners__slide-info__text"><?= $partner->title ?></p>
                            <p class="partners__slide-info__text"><?= $partner->sub_title ?></p>
                            <div>
                                <a href="tel:<?= $partner->description ?>" class="partners__slide-info__text"><?= $partner->description ?></a>
                            </div>
                            <div>
                                <a href="<?= $partner->link ?>" class="partners__slide-info__link" target="_blank" rel="nofollow"><?= $partner->link ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
    <div class="navigation partners__navigation wow fadeInUp">
        <div class="wrap wrap_mobile_full">
            <div class="navigation__line"></div>
            <div class="navigation__content">
                <button class="navigation__btn navigation__btn_prev" data-partners-arrow-prev>
                    <div class="arrow arrow_left navigation__arrow"></div>
                </button>
                <div class="navigation__status">
                    <span class="navigation__current" data-partners-status-current>..</span>\
                    <span class="navigation__total" data-partners-status-total>..</span>
                </div>
                <button class="navigation__btn navigation__btn_next" data-partners-arrow-next>
                    <div class="arrow arrow_right navigation__arrow"></div>
                </button>
            </div>
        </div>
    </div>
</section>
