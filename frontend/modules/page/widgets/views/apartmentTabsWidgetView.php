<?php

use common\models\ApartmentComplex;
use common\helpers\SiteUrlHelper;

/**
 * @var ApartmentComplex $complex
 * @author dimarychek
 */
?>

<div class="wrap wrap_mobile_full">
    <div class="apartment-head choose-ap">
        <h2 class="h2 apartment-head__h2 wow fadeInLeft"><?= $complex->label ?></h2>
        <div class="choose-ap__switch wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
            <span class="choose-ap__btn choose-ap__btn-a" target="_self"> Конфигуратор </span>
            <a class="choose-ap__btn" href="<?= SiteUrlHelper::createApartmentComplexApartmentsListUrl(['alias' => $complex->alias, 'type' => $complex->type]) ?>"> Планировки </a>
        </div>
        <?php if ($complex->general_progress_percent) : ?>
            <div class="diagram apartment-head__diagram wow fadeInLeft" data-wow-delay="0.5s">
                <div class="circle-progress" data-circle-progress="<?= $complex->general_progress_percent ?>">
                        <div class="circle-progress__outer">
                            <div class="circle-progress__progress circle-progress__progress_10"></div>
                            <div class="circle-progress__progress circle-progress__progress_20"></div>
                            <div class="circle-progress__progress circle-progress__progress_30"></div>
                            <div class="circle-progress__progress circle-progress__progress_40"></div>
                            <div class="circle-progress__progress circle-progress__progress_50"></div>
                            <div class="circle-progress__progress circle-progress__progress_60"></div>
                            <div class="circle-progress__progress circle-progress__progress_70"></div>
                            <div class="circle-progress__progress circle-progress__progress_80"></div>
                            <div class="circle-progress__progress circle-progress__progress_90"></div>
                            <div class="circle-progress__progress circle-progress__progress_100"></div>
                            <div class="circle-progress__value"><?= $complex->general_progress_percent ?></div>
                            <div class="circle-progress__percent">%</div>
                        </div>
                    <?php if ($complex->general_progress_title) : ?>
                        <div class="circle-progress__label"><?= $complex->general_progress_title ?></div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>