<?php

use metalguardian\fileProcessor\helpers\FPM;
use frontend\modules\form\widgets\GuaranteeFormWidget;
use frontend\modules\form\widgets\CooperationFormWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\form\widgets\CustomGuaranteeFormWidget;
use frontend\modules\form\widgets\BottomJobWithDocFormWidget;
use frontend\modules\form\widgets\BottomAskQuestionFormWidget;
use frontend\modules\form\widgets\ComplexPresentationFormWidget;
use frontend\modules\form\widgets\BottomHypothecWithDocFormWidget;
 
/**
 * @author art
 * @var $this yii\web\View
 * @var $cityId int
 * @var $formType int
 * @var $complexId int
 * @var $buildingId int
 * @var $problems object
 * @var $houseSquare int
 * @var $formTitle string
 * @var $formTitleDark string
 * @var $formWrapperClass
 * @var $formPosition int
 * @var $blockClass string
 * @var $agreementText string
 * @var $formDescription string
 * @var $manager common\models\Manager|null
 * @var $onsubmitJS string|null
 */
?>

<div class="questions <?= $blockClass ?>" data-form-outer>
    <div class="<?= $formWrapperClass ? : 'form-content questions__form-content' ?>">
        <div class="wrap wrap_mobile_full">
            <div class="questions__columns">
                <div class="questions__column">
                    <h2 class="h2 questions__h2 wow fadeInLeft"><?= $formTitle ?>
                        <?php if ($formTitleDark) : ?>
                            <strong><?= $formTitleDark ?></strong>
                        <?php endif; ?>
                    </h2>
                    <?php if ($formPosition == PageFormBottomBlockWidget::FORM_POSITION_LEFT): ?>
                        <?php switch ($formType) {
                            case PageFormBottomBlockWidget::FORM_TYPE_QUESTION:
                                echo BottomAskQuestionFormWidget::widget(['apartment_id'=>$apartment_id,'complexId' => $complexId, 'buildingId' => $buildingId, 'managerId' => $manager->id ?? null, 'agreementText' => $agreementText, 'onsubmitJS' => $onsubmitJS]);
                                break;
                            case PageFormBottomBlockWidget::FORM_TYPE_HYPOTHEC_WITH_DOC:
                                echo BottomHypothecWithDocFormWidget::widget(['agreementText' => $agreementText, 'onsubmitJS' => $onsubmitJS]);
                                break;
                            case PageFormBottomBlockWidget::FORM_TYPE_CUSTOM_GUARANTEE:
                                echo CustomGuaranteeFormWidget::widget(['agreementText' => $agreementText, 'onsubmitJS' => $onsubmitJS, 'modelId' => $modelId]);
                                break;
                            case PageFormBottomBlockWidget::FORM_TYPE_JOB_WITH_DOC:
                                echo BottomJobWithDocFormWidget::widget(['agreementText' => $agreementText, 'onsubmitJS' => $onsubmitJS, 'vacancy' => $vacancy]);
                                break;
                            case PageFormBottomBlockWidget::FORM_TYPE_COMPLEX_PRESENTATION:
                                echo ComplexPresentationFormWidget::widget(['complexId' => $complexId, 'buildingId' => $buildingId, 'managerId' => $manager->id ?? null, 'onsubmitJS' => $onsubmitJS]);
                                break;
                            case PageFormBottomBlockWidget::FORM_TYPE_COOPERATION:
                                echo CooperationFormWidget::widget(['onsubmitJS' => $onsubmitJS, 'agreementText' => $agreementText]);
                                break;
                        } ?>
                    <?php else : ?>
                         <div class="text questions__text questions__text_desk wow fadeInLeft" data-wow-delay="0.3s">
                             <p><?= $formDescription ?></p>
                         </div>
                    <?php endif ?>

                </div>
                <div class="questions__column">
                    <?php if ($formPosition == PageFormBottomBlockWidget::FORM_POSITION_RIGHT): ?>
                        <?php switch ($formType) {
                            case PageFormBottomBlockWidget::FORM_TYPE_QUESTION:
                                echo BottomAskQuestionFormWidget::widget(['complexId' => $complexId, 'buildingId' => $buildingId, 'agreementText' => $agreementText, 'managerId' => $manager->id ?? null, 'onsubmitJS' => $onsubmitJS]);
                                break;
                            case PageFormBottomBlockWidget::FORM_TYPE_COMPLEX_PRESENTATION:
                                echo ComplexPresentationFormWidget::widget(['complexId' => $complexId, 'buildingId' => $buildingId, 'managerId' => $manager->id ?? null, 'onsubmitJS' => $onsubmitJS]);
                                break;
                            case PageFormBottomBlockWidget::FORM_TYPE_GUARANTEE:
                                echo GuaranteeFormWidget::widget(['buildingId' => $buildingId, 'problems' => $problems, 'onsubmitJS' => $onsubmitJS]);
                                break;
                        } ?>
                    <?php else: ?>
                        <div class="text questions__text wow fadeInUp">
                            <p><?= $formDescription ?></p>
                        </div>
                        <?php if (isset($manager)): ?>
                            <div class="manager questions__manager wow fadeInUp">
                                <div class="manager__bg" data-bg-src="<?= FPM::originalSrc($manager->photo->file_id ?? '') ?>" data-bg-size="cover" data-bg-pos="center"></div>
                                <div class="manager__text-wrapper">
                                    <div class="h5 manager__h5"><?= $manager->fio ?></div>
                                    <div class="divider"></div>
                                    <div class="manager__text">
                                        <p><?= $manager->job_position ?></p>
                                    </div>
                                    <a href="tel:<?= preg_replace('/[^0-9]/', '', $manager->phone) ?>" class="manager__tel" target="_blank"><?= $manager->phone ?></a>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- QUESTIONS FORM SUCCESS -->
    <div class="form-success questions__form-success form-success_hidden">
        <div class="form-success__content">
            <div class="wrap wrap_mobile_full">
                <div class="questions__columns">
                    <div class="questions__column">
                        <h2 class="h2 questions__h2">
                            <strong>Спасибо!</strong>
                        </h2>
                    </div>
                    <div class="questions__column"> </div>
                </div>
                <div class="questions__columns">
                    <div class="questions__column">
                        <div class="text questions__text">
                            <p> Ваша заявка успешно отправлена. Наш менеджер свяжется с Вами в ближайшее время. </p>
                        </div>
                    </div>
                    <div class="questions__column">
                        <button class="button form__button" data-form-reset-btn> Отправить еще
                            <span class="button__blip button__blip_hover"></span>
                            <span class="button__blip button__blip_click"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /QUESTIONS FORM SUCCESS -->
</div>
