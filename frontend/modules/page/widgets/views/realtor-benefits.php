<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $description string
 * @var $benefits common\models\blocks\RealtorBenefit[]
 */
?>

<section class="values">
    <div class="wrap wrap_mobile_full">
        <div class="values__title_wrap">
            <h2 class="h2 values__h2 wow fadeInLeft"><?= $title ?></h2>
            <p class="values__description wow fadeInRight"><?= $description ?></p>
        </div>
        <div class="values__list">
            <?php foreach ($benefits as $benefit) : ?>
                <div class="card values__card wow fadeInUp">
                    <?php $image = $benefit->getImageSrc() ?>
                    <?php if ($image) : ?>
                        <div class="card__img" data-bg-src="<?= $image ?>" data-bg-pos="center" data-bg-size="cover"></div>
                    <?php endif ?>
                    <h5 class="h5 card__h5"><?= $benefit->title ?></h5>
                    <div class="divider card__divider"></div>
                    <div class="card__text"><?= $benefit->description ?></div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
