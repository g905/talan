<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use yii\helpers\Json;
use yii\helpers\Html;
use common\models\ContactPage;
use common\models\ContactMarker;
use frontend\helpers\FrontendHelper;

/**
 * Class ContactPageMapWidget
 *
 * @package frontend\modules\page\widgets
 */
class ContactPageMapWidget extends Widget
{
    /**
     * @var string main pin icon path.
     */
    const MAIN_ICON_PATH = '/static/img/map/main_pin.png';
    /**
     * @var ContactPage associated model.
     */
    public $model;
    /**
     * @var string marker template with replaceable tokens.
     */
    public $markerTemplate = <<<HTML
    <div class="infowindow">
        <h3>{label}</h3>
        <p>{address}</p>
        <a href="tel:{phoneFormatted}" class="infowindow__tel">{phone}</a>
    </div>
HTML;

    public function run()
    {
        $output = '';
        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('contact-page-map', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        $output = [];
        if ($this->model !== null) {
            $output = [
                'title' => $this->model->label,
                'salesTitle' => t('Sales department', 'contact-page'),
                'salesPhones' => $this->preparePhones($this->model->sales_phones),
                'receptionTitle' => t('Reception', 'contact-page'),
                'receptionPhones' => $this->preparePhones($this->model->reception_phones),
                'addressTitle' => t('Address', 'contact-page'),
                'address' => $this->model->address,
                'emailTitle' => t('Email', 'contact-page'),
                'email' => $this->model->email,
                'timetableTitle' => t('The sales department works', 'contact-page'),
                'timetable' => $this->model->timetable,
                'mapOptions' => $this->generateMapOptions(),
                'mapImage' => $this->model->mapImage,
            ];
        }

        return $output;
    }

    private function preparePhones($val)
    {
        $output = '';
        $values = explode("\n", $val);

        foreach ($values as $value) {
            $formatted = FrontendHelper::formatPhone($value);
            $output .= Html::a($value, "tel:$formatted", ['class' => 'contacts__wrap-phone_number h3']);
        }

        return $output;
    }

    private function generateMapOptions(): string
    {
        $map = [];
        $model = $this->model;
        $markers = $model->markers;
        $firstMarker = remove($markers, 0, false);

        if ($firstMarker) {
            $map = [
                'pos' => ['lat' => $firstMarker->lat, 'lng' => $firstMarker->lng],
                'zoom' => $model->map_zoom,
                'mainMarker' => $this->prepareMarkerOptions($firstMarker),
                'markers' => [],
            ];
        }

        if ($markers) {
            foreach ($markers as $marker) {
                $map['markers'][] = $this->prepareMarkerOptions($marker);
            }
        }

        return Json::encode($map, JSON_NUMERIC_CHECK);
    }

    private function prepareMarkerOptions(ContactMarker $marker): array
    {
        return [
            'type' => $marker->contact_page_id,
            'pos' => ['lat' => $marker->lat, 'lng' => $marker->lng],
            'icon' => self::MAIN_ICON_PATH,
            'html' => strtr($this->markerTemplate, [
                '{label}' => $marker->label,
                '{phone}' => $marker->phone,
                '{address}' => $marker->address,
                '{phoneFormatted}' => FrontendHelper::formatPhone($marker->phone),
            ]),
        ];
    }
}
