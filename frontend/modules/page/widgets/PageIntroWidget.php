<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\Hypothec;
use common\models\InstallmentPlan;

/**
 * Class PageIntroWidget
 *
 * @package frontend\modules\page\widgets
 */
class PageIntroWidget extends Widget
{
    /**
     * @var InstallmentPlan|Hypothec associated model.
     */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model !== null) {
            $output = $this->render('page-intro', $this->prepareVars());
        }

        return $output;
    }

    private function prepareVars()
    {
        return [
            'title' => $this->model->label,
            'descriptionLabel' => $this->model->description_label,
            'descriptionText' => $this->model->description_text,
            'image' => $this->model->getDescriptionPhotoPreviewSrc(),
        ];
    }
}
