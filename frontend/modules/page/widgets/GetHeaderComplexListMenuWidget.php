<?php

namespace frontend\modules\page\widgets;

use common\models\City;
use yii\base\Widget;
use common\models\SiteMenu;

class GetHeaderComplexListMenuWidget extends Widget
{
    /**
     * @var int property type.
     */
    public $type;
    /**
     * @var City current city.
     */
    public $currentCity;

    public function run()
    {
        if (!isset($this->currentCity)) {
            return null;
        }

        $menu = $this->getModel();

        if (!isset($menu)){
            return null;
        }

        return $this->render('getHeaderComplexListMenuWidgetView', compact('menu'));
    }

    protected function getModel()
    {
        return SiteMenu::find()->where([
            'city_id' => $this->currentCity->id,
            'published' => 1,
            'menu_position_id' => SiteMenu::POSITION_COMPLEXLIST
        ])->one();
    }

}
