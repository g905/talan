<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\City;

class SelectCityWidget extends Widget
{
    const POSITION_HEADER_FIXED = 1;
    const POSITION_HEADER_TOP = 2;
    const POSITION_HOME_SECTION = 3;
    const POSITION_HEADER_PANEL = 4;

    public $currentCity;
    public $position = self::POSITION_HEADER_FIXED;

    public static function getPositionClass()
    {
        return [
            self::POSITION_HEADER_FIXED => ['header-fix', 'header__select'],
            self::POSITION_HEADER_TOP => ['header-top', 'header__select'],
            self::POSITION_HOME_SECTION => ['complexes', 'complexes__select'],
            self::POSITION_HEADER_PANEL => ['menu', 'panel-menu__select'],
        ];
    }

    public static function getPositionMorphos()
    {
        return [
            self::POSITION_HEADER_FIXED => 'nominative',
            self::POSITION_HEADER_TOP => 'nominative',
            self::POSITION_HOME_SECTION => 'prepositional',
            self::POSITION_HEADER_PANEL => 'nominative',
        ];
    }

    public static function getPositionPrefix()
    {
        return [
            self::POSITION_HEADER_FIXED => '',
            self::POSITION_HEADER_TOP => '',
            self::POSITION_HOME_SECTION => 'в ',
            self::POSITION_HEADER_PANEL => '',
        ];
    }

    public function run()
    {
        if (!isset($this->currentCity)) {
            return null;
        }

        $cityArray = $this->getModels();

        $positionClass = self::getPositionClass()[$this->position];
        $positionMorphos = self::getPositionMorphos()[$this->position];
        $positionPrefix = self::getPositionPrefix()[$this->position];

        echo $this->render('selectCityWidgetView', [
            'currentCity' => $this->currentCity,
            'cityArray' => $cityArray,
            'positionClass' => $positionClass,
            'positionMorphos' => $positionMorphos,
            'positionPrefix' => $positionPrefix,

        ]);
    }

    protected function getModels()
    {
        $models = obtain('otherCities', app()->params, false);

        if ($models === false) {
            $models = City::find()->orderBy('position ASC')->where(['<>', 'id', $this->currentCity->id])
                ->andWhere(['published' => 1])
               ->all();
            app()->params['otherCities'] = $models;
        }

        return $models;
    }

}
