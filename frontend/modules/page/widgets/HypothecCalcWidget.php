<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\Hypothec;

/**
 * Class HypothecCalcWidget
 *
 * @package frontend\modules\page\widgets
 */
class HypothecCalcWidget extends Widget
{
    /**
     * @var Hypothec associated model.
     */
    public $model;

    public function run()
    {
        $output = '';
        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('hypothec-calc', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        $output = [];
        if ($this->model !== null) {
            $output = [
                'formOnSubmit' => $this->model->form_calc_onsubmit,
                'defaultTimeTerm' => $this->model->default_time_term,
                'defaultInitialFee' => $this->model->default_initial_fee,
                'defaultApartmentPrice' => $this->model->default_apartment_price,
                'minTerm' => $this->model->min_term,
                'maxTerm' => $this->model->max_term,
                'banks' => $this->model->banks
            ];
        }
        return $output;
    }
}
