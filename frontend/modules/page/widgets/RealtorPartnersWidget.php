<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\Realtor;

/**
 * Class RealtorPartnersWidget
 *
 * @package frontend\modules\page\widgets
 */
class RealtorPartnersWidget extends Widget
{
    /**
     * @var Realtor associated model.
     */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model === null) {
            return $output;
        }

        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('realtor-partners', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        $output = [];
        $partners = $this->model->partnerBlocks;
        if ($partners) {
            $output = [
                'title' => $this->model->partners_label,
                'partners' => $partners
            ];
        }

        return $output;
    }
}
