<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets;

use common\models\SubMenu;
use yii\base\Widget;
use common\models\AcMenu;
use common\models\AboutCompanyMenu;
use common\models\ApartmentBuilding;
use common\models\ApartmentComplex;

class HeaderPageMenuWidget extends Widget
{
    const MENU_TYPE_DEFAULT = 0;
    const MENU_TYPE_APARTMENT_COMPLEX = 1;
    const MENU_TYPE_ABOUT = 2;
    const MENU_TYPE_APARTMENT_BUILDING = 3;

    public $menu;
    public $menuType;
    protected $objectId = null;
    protected $type = null;

    public function run()
    {
        if ((!isset($this->menu)) || (count($this->menu) == 0)) {
            return null;
        }

        $leftItems = [];
        $rightItems = [];

        foreach ($this->menu as $menuItem) {
            if ($menuItem->show_right) {
                $rightItems[] = $menuItem;
            } else {
                $leftItems[] = $menuItem;
            }
        }

        $firstMenuItem = obtain(0, $this->menu);
        if ($firstMenuItem instanceof AcMenu || $firstMenuItem instanceof \backend\modules\apartment\models\AcMenu) {
            if ($this->menuType instanceof ApartmentBuilding) {
                $this->objectId = $firstMenuItem->apartmentBuilding->id;
                $this->type = self::MENU_TYPE_APARTMENT_BUILDING;
            } else {
                $this->objectId = $firstMenuItem->apartmentComplex->id;
                $this->type = self::MENU_TYPE_APARTMENT_COMPLEX;
            }

//            if ($this->menuType instanceof ApartmentComplex) {
//                $this->objectId = $firstMenuItem->apartmentComplex->id;
//                $this->type = self::MENU_TYPE_APARTMENT_COMPLEX;
//            } elseif ($this->menuType instanceof ApartmentBuilding) {
//                $this->objectId = $firstMenuItem->apartmentBuilding->id;
//                $this->type = self::MENU_TYPE_APARTMENT_BUILDING;
//            }
        }

        if ($firstMenuItem instanceof AboutCompanyMenu || $firstMenuItem instanceof \backend\modules\page\models\AboutCompanyMenu) {
            $this->objectId = $firstMenuItem->aboutCompany->id;
            $this->type = self::MENU_TYPE_ABOUT;
        }

        if ($firstMenuItem instanceof SubMenu) {
            //$this->objectId = $firstMenuItem->aboutCompany->id;
            $this->type = self::MENU_TYPE_DEFAULT;
        }

        return $this->render('headerPageMenuWidgetView', [
            'rightItems' => $rightItems,
            'leftItems' => $leftItems,
            'modelId' => $this->objectId,
            'modelType' => $this->type,
        ]);
    }

}
