<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\ContactPage;

/**
 * Class ContactPageFormsWidget
 *
 * @package frontend\modules\page\widgets
 */
class ContactPageFormsWidget extends Widget
{
    /**
     * @var ContactPage associated model.
     */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model === null) {
            return $output;
        }

        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('contact-page-forms', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        $output = [];
        $forms = $this->model->forms;

        if ($forms) {
            $output = [
                'title' => $this->model->forms_label,
                'firstForm' => remove($forms, 0),
                'forms' => $forms,
            ];
        }

        return $output;
    }
}
