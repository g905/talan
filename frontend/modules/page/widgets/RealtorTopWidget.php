<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\Realtor;

/**
 * Class RealtorTopWidget
 *
 * @package frontend\modules\page\widgets
 */
class RealtorTopWidget extends Widget
{
    /**
     * @var Realtor associated model.
     */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model === null) {
            return $output;
        }

        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('realtor-top', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        return [
            'title' => $this->model->top_label,
            'image' => $this->model->topImageSrc(),
            'description' => $this->model->top_description,
            'link' => $this->model->topLinkSrc(),
            'linkLabel' => $this->model->top_link_label,
            'btnLabel' => $this->model->top_btn_label,
        ];
    }
}
