<?php

use metalguardian\fileProcessor\helpers\FPM;

?>

<div class="wrap wrap_mobile_full">
    <div class="apartment-head catalogue-head">
        <h2 class="h2 apartment-head__h2 wow fadeInLeft"> <?= $model->resale_label ?> </h2>
    </div>
</div>
<section class="complexes">
    <div class="wrap wrap_large wrap_mobile_full">
        <section class="complecs complecs__home">
            <div class="wrap wrap_mobile_full">
                <div class="complecs__list">
                    <?php foreach ($apartments as $apartment) : ?>
                        <div class="card complecs__card wow fadeInUp">
                            <div class="card__img" data-bg-src="<?= FPM::originalSrc($apartment->photo->file_id ?? '') ?>" data-bg-pos="center" data-bg-size="cover"></div>
                            <h5 class="h5 card__h5"> <?= $apartment->apartmentComplex->label ?? '' ?>, <?= $apartment->label ?> </h5>
                            <div class="complecs__card-links">
                                <a href="<?= $apartment->getViewUrl() ?>" class="button button_green" data-wow-delay="1.3s"> подробнее о квартире
                                    <span class="button__bg"></span>
                                    <div class="button__blip button__blip_hover"></div>
                                    <div class="button__blip button__blip_click"></div>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="card wow fadeInUp" style="float: right">
                <a href="/apartment-complex/<?=$apartments[0]->getApartmentComplex()->one()->alias?>/apartments" class="button button_green" data-wow-delay="1.3s">Смотреть все
                    <span class="button__bg"></span>
                    <div class="button__blip button__blip_hover"></div>
                    <div class="button__blip button__blip_click"></div>
                </a>
            </div>

        </section>
    </div>

</section>
