<?php

use common\helpers\SiteUrlHelper;
use frontend\modules\page\widgets\catalog\CatalogInvestBlockWidget;

?>

<div class="wrap wrap_mobile_full">
    <div class="apartment-head catalogue-head">
        <h2 class="h2 apartment-head__h2 wow fadeInLeft"> <?= $label ?> </h2>
    </div>
</div>
<section class="complexes">
    <div class="wrap wrap_large wrap_mobile_full">
        <section class="complecs complecs__home">
            <div class="wrap wrap_mobile_full">
                <div class="complecs__list">
                    <?php foreach ($complexes as $complex) : ?>
                        <div class="card complecs__card wow fadeInUp">
                            <a href="<?= $complex->getViewUrl() ?>" class="card__img-link"></a>
                            <div class="card__img" data-bg-src="<?= $complex->getPreviewPhotoSrc() ?>" data-bg-pos="center" data-bg-size="cover">
                                <div class="diagram apartment-head__diagram">
                                    <?php if ($complex->general_progress_percent) : ?>
                                        <div class="circle-progress" data-circle-progress="<?= trim($complex->general_progress_percent) ?>">
                                            <div class="circle-progress__outer">
                                                <div class="circle-progress__progress circle-progress__progress_10"></div>
                                                <div class="circle-progress__progress circle-progress__progress_20"></div>
                                                <div class="circle-progress__progress circle-progress__progress_30"></div>
                                                <div class="circle-progress__progress circle-progress__progress_40"></div>
                                                <div class="circle-progress__progress circle-progress__progress_50"></div>
                                                <div class="circle-progress__progress circle-progress__progress_60"></div>
                                                <div class="circle-progress__progress circle-progress__progress_70"></div>
                                                <div class="circle-progress__progress circle-progress__progress_80"></div>
                                                <div class="circle-progress__progress circle-progress__progress_90"></div>
                                                <div class="circle-progress__progress circle-progress__progress_100"></div>
                                                <div class="circle-progress__value"><?= $complex->general_progress_percent ?></div>
                                                <div class="circle-progress__percent">%</div>
                                            </div>
                                            <div class="circle-progress__label"> <?= $complex->general_progress_title ?> </div>
                                        </div>
                                    <?php endif ?>
                                </div>
                            </div>
                            <h5 class="h5 card__h5"> <?= $complex->label ?> </h5>
                            <div class="complecs__card-links">
                                <?php if($complex->button_view_complex_link):?>
                                    <a href="<?= $complex->button_view_complex_link ?>" class="button button_green" data-wow-delay="1.3s">
                                <?php else: ?>
                                    <a href="<?= $complex->getViewUrl() ?>" class="button button_green" data-wow-delay="1.3s">
                                <?php endif;?>
                                        <?php if($complex->button_view_complex_text):?>
                                            <?= $complex->button_view_complex_text?>
                                        <?php else:?>
                                            подробнее о ЖК
                                        <?php endif;?>
                                    <span class="button__bg"></span>
                                    <div class="button__blip button__blip_hover"></div>
                                    <div class="button__blip button__blip_click"></div>
                                </a>
                                        <?php if($complex->custom_choose_active) : ?>
                                            <?php if (isset($complex->custom_choose_link) && ($complex->custom_choose_link !== "")) : ?>
                                                <a href="<?= $complex->custom_choose_link ?>" class="more view-all__more">
                                            <?php else:?>
                                                <a href="<?= SiteUrlHelper::createApartmentComplexApartmentsListUrl(['alias' => $complex->alias, 'type' => $type]) ?>" class="more view-all__more">
                                            <?php endif; ?>
                                            <?php if (isset($complex->custom_choose_label) && ($complex->custom_choose_label !== "")) : ?>
                                                <?= $complex->custom_choose_label ?>
                                            <?php else:?>
                                                Выбрать квартиру
                                            <?php endif; ?>
                                                </a>
                                        <?php endif; ?>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
    </div>
</section>
