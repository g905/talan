<?php

use common\helpers\SiteUrlHelper;

?>

<section class="values done_projects">
    <div class="wrap wrap_mobile_full">
        <div class="done_projects__title">
            <h2 class="h2 values__h2 wow fadeInLeft"> <?= $model->completed_label ?> </h2>
            <p class="p values__p wow fadeInLeft"> <?= $model->completed_sublabel ?> </p>
        </div>
        <div class="values__list">
            <?php foreach ($projects as $project) : ?>
                <a href="<?= $project->url ?>" class="card values__card wow fadeInUp">
                    <div class="card__img" data-bg-src="<?= $project->getPhotoPreviewSrc() ?>" data-bg-pos="center" data-bg-size="cover"></div>
                    <h5 class="h5 card__h5"> <?= $project->title ?> </h5>
                    <div class="divider card__divider"></div>
                    <div class="card__text">
                        <p> <?= $project->description ?> </p>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</section>
