<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 19.12.2017
 * Time: 20:45
 */

use frontend\helpers\ImgHelper;
use frontend\modules\page\widgets\PageSliderWidget;

/** @var \common\models\Home $model */
?>

<section class="catalog-invest-block">
        <div class="invest_content">

            <div class="h4 invest-title poster__h1 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                <?= $model->invest_title?>
            </div>

            <div class="invest-desc-wrap">
              <div class="invest-desc wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                  <?= $model->invest_desc?>
              </div>
            </div>
              <a href="<?= $model->invest_button_link?>" class="button invest-button button_transparent poster__button wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                  <?= $model->invest_button_label?>
                  <div class="button__bg"></div>
                  <span class="button__blip button__blip_hover"></span>
                  <span class="button__blip button__blip_click"></span>
              </a>
        </div>
</section>
