<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets\catalog;

use common\helpers\Property;
use common\models\Apartment;
use common\models\City;
use yii\base\Widget;

class CatalogSecondaryWidget extends Widget
{
    public $model;

    public function run()
    {
        $city = City::getUserCity();

        $apartments = Apartment::getApartmentsByTypeAndCity(Property::TYPE_SECONDARY, $city->id);
        //$apartments = Apartment::getSecondaryApartmentsByTypeAndCity(Property::TYPE_RESIDENTIAL, $city->id);

        if (!$apartments){
            return null;
        }

        return $this->render('catalogSecondaryWidgetView',[
            'apartments' => $apartments,
            'model' => $this->model,
        ]);
    }
}
