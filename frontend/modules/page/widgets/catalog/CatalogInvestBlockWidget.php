<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets\catalog;

use yii\base\Widget;

class CatalogInvestBlockWidget extends Widget
{
    public $test;
    public function run()
    {
        if (!isset($this->test)) {
            return null;
        }
        if(!$this->test->invest_active) {
            return null;
        }
        //dd($this->test->invest_active);
        echo $this->render('catalogInvestBlockWidgetView',[
            'model' => $this->test
        ]);
    }
}
