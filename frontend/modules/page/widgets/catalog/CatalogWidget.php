<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets\catalog;

use common\models\ApartmentComplex;
use common\models\City;
use yii\base\Widget;

class CatalogWidget extends Widget
{
    public $label;

    public $type;

    public $model;

    public function run()
    {
        $city = City::getUserCity();

        $complexes = ApartmentComplex::getComplexesByTypeAndCity($this->type, $city->id);

        if (!$complexes){
            return null;
        }

        return $this->render('catalogWidgetView',[
            'label' => $this->label,
            'complexes' => $complexes,
            'type' => $this->type,
            'model' => $this->model
        ]);
    }
}
