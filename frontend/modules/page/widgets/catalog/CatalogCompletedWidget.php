<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets\catalog;

use common\models\ApartmentComplex;
use common\models\City;
use common\models\CompletedProject;
use yii\base\Widget;
use yii\helpers\Json;

class CatalogCompletedWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!$this->model->projects_id){
            return null;
        }

        $projects = CompletedProject::getItemsById(Json::decode($this->model->projects_id));

        return $this->render('catalogCompletedWidgetView',[
            'projects' => $projects,
            'model' => $this->model,
        ]);
    }
}
