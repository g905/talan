<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets;

use common\models\City;
use common\models\SiteMenu;
use yii\base\Widget;

class GetFooterMenuWidget extends Widget
{
    public $currentCity;

    public function run()
    {
        if (!isset($this->currentCity)) {
            return null;
        }

        $menu = $this->getModel();

        if (!isset($menu)){
            return null;
        }

        echo $this->render('getFooterMenuWidgetView', [
            'menu' => $menu,
        ]);
    }

    protected function getModel()
    {
        $menu = SiteMenu::find()
            ->where([
                'city_id' => $this->currentCity->id,
                'published' => 1,
                'menu_position_id' => SiteMenu::POSITION_FOOTER
            ])
            ->one();

        return $menu;
    }

}
