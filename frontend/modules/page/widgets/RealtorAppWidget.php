<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\Realtor;

/**
 * Class RealtorAppWidget
 *
 * @package frontend\modules\page\widgets
 */
class RealtorAppWidget extends Widget
{
    /**
     * @var Realtor associated model.
     */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model === null) {
            return $output;
        }

        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('realtor-app', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        return [
            'title' => $this->model->app_label,
            'description' => $this->model->app_description,
            'firstImage' => $this->model->appImageFirstSrc(),
            'secondImage' => $this->model->appImageSecondSrc(),
            'link' => $this->model->appLinkSrc(),
            'linkLabel' => $this->model->app_link_label,
            'iOSlink' => $this->model->app_btn_ios_link,
            'iOSlinkLabel' => $this->model->app_btn_ios_label,
            'androidLink' => $this->model->app_btn_android_link,
            'androidLinkLabel' => $this->model->app_btn_android_label,
        ];
    }
}
