<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets\home;

use common\models\Article;
use yii\base\Widget;

class HomeArticleWidget extends Widget
{
    private $articles;

    public function run()
    {
        $this->articles = Article::getHomePageArticles();

        if (!$this->articles) {
            return null;
        }

        echo $this->render('homeArticleWidgetView',[
           'articles' => $this->articles
        ]);
    }
}
