<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets\home;

use common\models\ApartmentComplex;
use common\models\PromoAction;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class HomeActionsWidget extends Widget
{

    public $homePage;
    public function run()
    {
        if (!isset($this->homePage)) {
            return null;
        }

        $apartmentComplexesIds = ArrayHelper::getColumn(ApartmentComplex::find()
            ->where([
                'published' => 1,
                'city_id' => $this->homePage->city->id
            ])
            ->asArray()
            ->all(), 'id');

        $promoActions = PromoAction::find()
            ->leftJoin('promo_action_to_ac', 'promo_action_to_ac.action_id = promo_action.id')
            ->where([
                'promo_action.published' => 1,
                'promo_action.show_on_home' => 1,
                'promo_action_to_ac.ac_id' => $apartmentComplexesIds
            ])
            ->andWhere(['<', 'promo_action.start_date', time()])
            ->andWhere(['>', 'promo_action.end_date', time()])
            ->orderBy([
                'promo_action.position_on_home' => SORT_DESC
            ])
            ->all();

        if ((!isset($promoActions)) || (count($promoActions) == 0)){
            return null;
        }

        echo $this->render('homeActionsWidgetView',[
            'model' => $this->homePage,
            'promoActions' => $promoActions
        ]);
    }
}
