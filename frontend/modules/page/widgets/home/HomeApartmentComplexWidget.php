<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets\home;

use common\models\ApartmentComplex;
use yii\base\Widget;

class HomeApartmentComplexWidget extends Widget
{
    public $homePage;

    public function run()
    {
        if (!isset($this->homePage)) {
            return null;
        }

        $apartmentComplexes = ApartmentComplex::find()
            ->where(['published' => 1, 'show_on_home' => 1, 'city_id' => $this->homePage->city->id])
            ->orderBy(['position_on_home' => SORT_DESC])
            ->all();

        if ((!isset($apartmentComplexes)) || (count($apartmentComplexes) == 0)) {
            return null;
        }

        if ($this->homePage->alternate_widget) {
            return $this->render('homeApartmentAlternateComplexWidgetView', [
                'model' => $this->homePage,
            ]);
        }

        return $this->render('homeApartmentComplexWidgetView', [
            'model' => $this->homePage,
            'apartmentComplexes' => $apartmentComplexes,
        ]);
    }
}
