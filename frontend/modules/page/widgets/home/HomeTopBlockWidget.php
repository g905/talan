<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets\home;

use yii\base\Widget;

class HomeTopBlockWidget extends Widget
{

    public $homePage;
    public function run()
    {
        if (!isset($this->homePage)) {
            return null;
        }

        echo $this->render('homeTopBlockWidgetView',[
            'model' => $this->homePage
        ]);
    }
}
