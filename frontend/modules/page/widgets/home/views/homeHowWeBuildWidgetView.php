<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 19.12.2017
 * Time: 18:10
 */

use frontend\helpers\ImgHelper;
/** @var \common\models\Home $model */
?>
<section class="rules">
    <div class="wrap wrap_mobile_full">
        <h2 class="rules__h2 wow fadeInUp"><?=$model->how_we_build_title?></h2>
    </div>
    <div class="rules__list">

        <?php $i=0;
        /** @var \common\models\HowWeBuild $howBuild */
        foreach ($model->howWeBuild as $howBuild): $i++;?>
            <div class="card <?= ($i % 2 == 0) ? 'card_right' : 'card_left' ?> rules__card">
                <div class="wrap wrap_mobile_full">
                    <div class="card__inner">
                        <div class="card__content">
                            <h3 class="h3 card__h3 wow fadeInLeft">
                                <span class="card_span"><?= $howBuild->label ?></span>
                            </h3>
                            <div class="card__number wow fadeInLeft" data-wow-delay="0.5s"> <?= $howBuild->getPositionFormatted() ?></div>
                            <div class="card__divider wow fadeInLeft" data-wow-delay="0.5s"></div>
                            <div class="card__text wow fadeInLeft" data-wow-delay="0.8s">
                                <p>
                                    <?= $howBuild->content ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card__img wow fadeIn"  data-wow-duration="2s" data-wow-delay="0.5s" data-bg-src="<?= ImgHelper::getImageSrc($howBuild, 'home', 'howWeBuild', 'image')?>" data-bg-pos="center bottom" data-bg-size="cover"></div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>
