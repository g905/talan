<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 19.12.2017
 * Time: 18:19
 */

use frontend\helpers\ImgHelper;

/** @var \common\models\Home $model */
/** @var \common\models\Review $review */
?>
<section class="testimonials">
    <div class="wrap wrap_mobile_full">
        <h2 class="testimonials__h2 wow fadeInUp"><?=$model->review_title?></h2>
    </div>
    <div class="testimonials__list" data-testimonials-slider data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "dots": false, "prevArrow": ".testimonials .navigation__btn_prev", "nextArrow": ".testimonials .navigation__btn_next", "responsive": [{"breakpoint": 1200, "settings": {"adaptiveHeight": true, "fade": true}}]}'>

        <?php foreach ($model->reviews as $review): ?>
        <div class="card testimonials__card">
            <div class="wrap wrap_mobile_full">
                <div class="card__inner">
                    <div class="card__content">
                        <h5 class="h5 card__h5 wow fadeInLeft">
                            <?=$review->fio?>
                        </h5>
                        <div class="card__excerpt wow fadeInLeft" data-wow-delay="0.3s">
                            <?=$review->label?>
                        </div>
                        <div class="card__divider wow fadeInLeft" data-wow-delay="0.6s"></div>
                        <div class="card__text wow fadeInLeft" data-wow-delay="0.9s">
                            <p>
                                <?=$review->review_text?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php if ($review->getYoutubeCode() != null): ?>
                <div data-youtube="<?=$review->getYoutubeCode()?>" class="card__img wow fadeIn"  data-wow-delay="0.5s" data-wow-duration="2s" data-bg-src="<?= $review->getCoverImage()?>" data-bg-size="cover" data-bg-pos="center">
                    <div class="play card__play">
                        <div class="play__blip play__blip_hover"></div>
                        <div class="play__blip play__blip_click"></div>
                    </div>
                    <iframe class="card__iframe" src="" frameborder="0" allowfullscreen></iframe>
                </div>
                <?php else: ?>
                <div class="card__img wow fadeIn"  data-wow-delay="0.5s" data-wow-duration="2s" data-bg-src="<?= $review->getCoverImage()?>" data-bg-size="cover" data-bg-pos="center">
                </div>
                <?php endif; ?>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="navigation testimonials__navigation wow fadeInUp">
        <div class="wrap wrap_mobile_full">
            <div class="navigation__line"></div>
            <div class="navigation__content">
                <button class="navigation__btn navigation__btn_prev" data-testimonials-arrow-prev>
                    <div class="arrow arrow_left navigation__arrow"></div>
                </button>
                <div class="navigation__status">
                    <span class="navigation__current" data-testimonials-status-current>..</span>\<span class="navigation__total" data-testimonials-status-total>..</span>
                </div>
                <button class="navigation__btn navigation__btn_next" data-testimonials-arrow-next>
                    <div class="arrow arrow_right navigation__arrow"></div>
                </button>
            </div>
        </div>
    </div>
</section>
