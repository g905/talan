<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 19.12.2017
 * Time: 18:07
 */

use frontend\helpers\ImgHelper;
/** @var \common\models\Home $model */
?>
<section class="about">
    <div class="wrap wrap_mobile_full">
        <div class="about__inner">
            <div class="about__content">
                <h2 class="h2 about__h2 wow fadeInRight">
                    <?=$model->about_company_title?>
                </h2>
                <div class="about__text wow fadeInRight" data-wow-delay="0.5s">
                    <p>
                        <?=$model->about_company_desc?>
                    </p>
                </div>
                <a href="<?=$model->about_company_link?>" class="more about__more wow fadeInRight" data-wow-delay="1s">
                    <?=$model->about_company_link_label?>
                </a>
            </div>
        </div>

        <div class="about__img wow fadeIn" data-wow-duration="2s" data-wow-delay="0.5s" data-bg-pos="center bottom" data-bg-size="cover">
            <img src="<?= ImgHelper::getImageSrc($model,'home','aboutCompany', 'aboutCompanyImage')?>" alt="">
        </div>
    </div>
</section>
