<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 19.12.2017
 * Time: 20:12
 */
/** @var \common\models\Home $model */
?>
<section class="text-block">
    <div class="wrap wrap_mobile_full text-block__wrap">
        <div class="text-block__left-col">
            <h2 class="text-block__title wow fadeInLeft"><?=$model->final_cta_title?></h2>
        </div>
        <div class="text-block__right-col">
            <div class="text wow fadeInUp" data-wow-delay="1s">
            <p><?=$model->final_cta_description?></p>
            </div>
            <a href="<?=$model->final_cta_btn_link?>" class="button button_green wow fadeInUp" data-wow-delay="1.3s">
                <?=$model->final_cta_btn_label?>
                <span class="button__bg"></span>
                <span class="button__blip button__blip_hover"></span>
                <span class="button__blip button__blip_click"></span>
            </a>
        </div>
    </div>
</section>
