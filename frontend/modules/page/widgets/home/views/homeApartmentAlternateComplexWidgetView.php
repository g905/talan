<?php

use metalguardian\fileProcessor\helpers\FPM;

?>

<section class="choose-appart">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 choose-appart__h2 wow fadeInRight"> <?= $model->alternate_title ?> </h2>
        <div class="choose-appart__body">
            <?php foreach ($model->alternateWidget as $item) : ?>
                <div class="choose-appart__item">
                    <a href="<?= $item->link ?>" class="choose-appart__link"></a>
                    <div class="choose-appart__img">
                        <img src="<?= FPM::originalSrc($item->image->file_id ?? null) ?>"> </div>
                    <div class="choose-appart__desc"> <?= $item->label ?>
                        <span></span>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
