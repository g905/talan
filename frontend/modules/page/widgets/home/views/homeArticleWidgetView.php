<?php
use common\helpers\SiteUrlHelper;
?>

<section class="articles tal-section__journal home__journal">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 tal-section__journal-title wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;"> <?= Yii::t('app', '_Journal') ?> </h2>
        <div class="articles__list">
            <?php foreach ($articles as $article) : ?>
                <div class="card articles__card wow fadeInUp">
                    <div class="card__img" data-bg-src="<?= $article->previewImageSrc ?>" data-bg-pos="center" data-bg-size="cover">
                        <a href="<?= SiteUrlHelper::getBlogViewUrl(['alias' => $article->alias]) ?>"></a>
                    </div>
                    <div class="card__content">
                        <h5 class="h5 card__h5">
                            <a href="<?= SiteUrlHelper::getBlogViewUrl(['alias' => $article->alias]) ?>"><?= $article->label ?></a>
                        </h5>
                        <span class="card__date"><?= $article->date_published ?></span>
                        <span class="divider card__divider"></span>
                        <?php if ($article->blogThemes) : ?>
                            <ul class="card__tags">
                                <?php foreach ($article->blogThemes as $theme) : ?>
                                    <li>
                                        <a href="<?= SiteUrlHelper::getBlogAllUrl(['theme' => $theme->alias]) ?>"><?= $theme->label ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <div class="card__text">
                            <?= $article->short_description ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="view-all">
            <div class="view-all__line"></div>
            <a href="<?= SiteUrlHelper::getBlogAllUrl() ?>" class="more view-all__more"> <?= Yii::t('app', '_All articles') ?> </a>
        </div>
    </div>
</section>