<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 19.12.2017
 * Time: 20:45
 */

use frontend\helpers\ImgHelper;
use frontend\modules\page\widgets\PageSliderWidget;
use frontend\modules\page\widgets\home\HomeInvestBlockWidget;

/** @var \common\models\Home $model */
?>

<section class="poster poster_padding">
    <?= PageSliderWidget::widget([
        'imagePath' => ImgHelper::getFileSrc($model, 'topScreenBackgroundImage'),
        'videoPath' => ImgHelper::getFileSrc($model,'topScreenBackgroundVideo'),
    ])?>

    <div class="poster__inner">
        <div class="poster__content">
            <?php if(isset($model->top_screen_title) && ($model->top_screen_title !== "")):?>
                <h1 class="h1 poster__h1 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                    <?= $model->top_screen_title?>
                </h1>
            <?php endif; ?>
            <?php if(isset($model->top_screen_btn_label) && ($model->top_screen_btn_label !== "")):?>
                <a href="<?= $model->top_screen_btn_link?>" onclick="<?= $model->top_screen_btn_onclick ?>" class="button button_transparent poster__button wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                    <?= $model->top_screen_btn_label?>
                    <div class="button__bg"></div>
                    <span class="button__blip button__blip_hover"></span>
                    <span class="button__blip button__blip_click"></span>
                </a>
            <?php endif;?>

        </div>

    </div>
    <div class="down poster__down wow down-to-top">
        <button class="down__button" data-scroll-down></button>
        <div class="down__label">
            <?=Yii::t('front/home', '_down')?>
        </div>
    </div>
</section>
<?= HomeInvestBlockWidget::widget(['test' => $model])?>
