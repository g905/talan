<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets\home;

use yii\base\Widget;

class HomeInvestBlockWidget extends Widget
{
    public $test;
    public function run()
    {
        if (!isset($this->test)) {
            return null;
        }
        if(!$this->test->invest_active) {
            return null;
        }
        //dd($this->test->invest_active);
        echo $this->render('homeInvestBlockWidgetView',[
            'model' => $this->test
        ]);
    }
}
