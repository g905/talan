<?php

namespace frontend\modules\page\widgets\buy;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

/**
 * Class BuyAdvantagesWidget
 *
 * @package frontend\modules\page\widgets\buy
 * @author dimarychek
 */
class BuyAdvantagesWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        if (count($this->model->advantages) == 0) {
            return null;
        }

        $advantages = $this->model->advantages;

        echo $this->render('buyAdvantagesWidgetView', [
            'model' => $this->model,
            'advantages' => $advantages
        ]);
    }
}
