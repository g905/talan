<?php

namespace frontend\modules\page\widgets\buy;

use frontend\modules\form\models\FormSecondary;
use yii\base\Widget;

/**
 * Class BuyFormWidget
 *
 * @package frontend\modules\page\widgets\buy
 * @author dimarychek
 */
class BuyFormWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        $formModel = new FormSecondary();

        echo $this->render('buyFormWidgetView', [
            'model' => $this->model,
            'formModel' => $formModel
        ]);
    }
}
