<?php

namespace frontend\modules\page\widgets\buy;

use common\models\Earth;
use yii\base\Widget;

/**
 * Class BuyStepWidget
 *
 * @package frontend\modules\page\widgets\buy
 * @author dimarychek
 */
class BuyStepWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        if (count($this->model->stepBlocks) == 0) {
            return null;
        }

        $stepBlocks = $this->model->stepBlocks;

        echo $this->render('buyStepWidgetView', [
            'model' => $this->model,
            'stepBlocks' => $stepBlocks
        ]);
    }
}
