<?php

use frontend\helpers\ImgHelper;
use common\models\BuyPage;
use common\models\blocks\BuyStep;

/**
 * @var BuyPage $model
 * @var array $stepBlocks
 */
?>

<section class="zayavka statistics ipot-zayavka">
    <div class="wrap wrap_mobile_full">
        <div class="wow fadeInLeft">
            <h2 class="h2 values__h2"><?= $model->steps_title ?></h2>
        </div>

        <div class="statistics__list">
            <?php foreach ($stepBlocks as $block) : ?>
                <div class="card statistics__card wow fadeIn" data-wow-delay="0s" style="visibility: visible; animation-delay: 0s; animation-name: fadeIn;">
                    <h3 class="h3 card__h3"><span class="card_span"><?= $block->title ?></span></h3>
                    <h5 class="h5 card__h5"><?= $block->sub_title ?></h5>
                    <div class="divider card__divider"></div>
                    <div class="card__text">
                        <?= $block->description ?>
                    </div>
                </div>
            <?php endforeach ?>

<!--            <div class="card card_stub statistics__card wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">-->
<!--                <div class="card__img" data-bg-src="./static/img/about/logo_stub.png" data-bg-pos="center" data-bg-size="auto" style="background: url(&quot;./static/img/about/logo_stub.png&quot;) center center / auto no-repeat;"></div>-->
<!--            </div>-->
        </div>
    </div>
</section>