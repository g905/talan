<?php

use frontend\helpers\ImgHelper;
use common\models\BuyPage;

/**
 * @author dimarychek
 * @var BuyPage $model
 * @var array $advantages
 */
?>

<section class="advantage values vtor-expectation">
    <div class="wrap wrap_mobile_full">
        <div class="wow fadeInUp" data-wow-delay="1s" data-wow-duration="2s">
            <h2 class="h2 new-quality__h2" style="visibility: visible; animation-name: fadeInUp;">
                <?= $model->advantages_title ?>
                <span><?= $model->advantages_title_dark ?></span>
            </h2>
        </div>
        <div class="values__list">
            <?php for ($i = 0; $i < count($advantages); $i++) : ?>
                <div class="card values__card wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                    <div class="card__img" data-bg-src="<?= $advantages[$i]->getImageSrc() ?>" data-bg-pos="center" data-bg-size="cover" style="background: url(<?= $advantages[$i]->getImageSrc() ?>) center center / cover no-repeat;"></div>
                    <h5 class="h5 card__h5"><?= $advantages[$i]->title ?></h5>
                    <div class="divider card__divider"></div>
                    <div class="card__text">
                        <?= $advantages[$i]->description ?>
                    </div>
                </div>
            <?php endfor ?>
        </div>
    </div>
</section>