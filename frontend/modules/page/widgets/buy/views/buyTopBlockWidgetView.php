<?php

use frontend\helpers\ImgHelper;
use frontend\modules\page\widgets\PageSliderWidget;
use common\models\BuyPage;

/**
 * @author dimarychek
 * @var BuyPage $model
 */
?>

<section class="intro">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 about__h2 wow fadeInRight"><?= $model->top_screen_title ?></h2>
        <div class="intro__block clearfix">
            <div class="intro__item-left">
                <h6 class="wow fadeInRight" data-wow-delay="0.3s" data-wow-duration="2s"><?= $model->top_screen_sub_title ?></h6>
                <div class="wow fadeInRight" data-wow-delay="1.3s" data-wow-duration="2s"><?= $model->top_screen_description ?></div>
                <button class="button button_green wow fadeInUp" data-wow-delay="1.3s" data-modal-open="call-back">
                    <?php if ($model->top_screen_btn) : ?>
                        <?= $model->top_screen_btn ?>
                    <?php else : ?>
                        <?= Yii::t('front/home', 'Leave request') ?>
                    <?php endif; ?>
                    <span class="button__bg"></span>
                    <div class="button__blip button__blip_hover"></div>
                    <div class="button__blip button__blip_click"></div>
                </button>
            </div>
            <!-- /.intro__item-left -->
            <div class="intro__item-right wow fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                <img src="<?= ImgHelper::getFileSrc($model, 'topScreenBackgroundImage') ?>" alt="intro-img" width="844" height="450"> </div>
            <!-- /.intro__item-right -->
        </div>
        <!-- /.intro__block -->
    </div>
</section>