<?php

namespace frontend\modules\page\widgets\buy;

use yii\base\Widget;

/**
 * Class BuyTopBlockWidget
 *
 * @package frontend\modules\page\widgets\buy
 * @author dimarychek
 */
class BuyTopBlockWidget extends Widget
{
    public $model;

    public function run()
    {
        if (!isset($this->model)) {
            return null;
        }

        echo $this->render('buyTopBlockWidgetView', [
            'model' => $this->model
        ]);
    }
}
