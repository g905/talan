<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets;

use yii\base\Widget;

class PageHeaderWidget extends Widget
{
    public $currentCity;

    public function run()
    {

        if (!isset($this->currentCity)){
            return null;
        }

        return $this->render('pageHeaderWidgetView',[
            'currentCity' => $this->currentCity
        ]);
    }
}
