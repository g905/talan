<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets;

use yii\base\Widget;

class PageFooterWidget extends Widget
{
    public $currentCity;
    public $additionalClass = '';

    public function run()
    {

        if (!isset($this->currentCity)){
            return null;
        }

        echo $this->render('pageFooterWidgetView',[
            'currentCity' => $this->currentCity,
            'additionalClass' => $this->additionalClass,
        ]);
    }
}
