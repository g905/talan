<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\PublicDataMenu;

/**
 * Class PublicDataMenuWidget
 *
 * @package frontend\modules\page\widgets
 */
class PublicDataMenuWidget extends Widget
{
    /**
     * @var PublicDataMenu[] menu models.
     */
    public $menu;

    public function run()
    {
        $output = '';

        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('public-data-menu', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        $output = [];
        if ($this->menu) {
            $output = [
                'titleMobile' => 'Категории',
                'items' => $this->menu,
            ];
        }

        return $output;
    }
}
