<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\Hypothec;

/**
 * Class HypothecBenefitsWidget
 *
 * @package frontend\modules\page\widgets
 */
class HypothecBenefitsWidget extends Widget
{
    /**
     * @var Hypothec associated model.
     */
    public $model;

    public function run()
    {
        $output = '';
        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('hypothec-benefits', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        $output = [];
        if ($this->model !== null) {
            $output = [
                'title' => $this->model->benefits_block_label,
                'benefits' => $this->model->benefitBlocks,
            ];
        }

        return $output;
    }
}
