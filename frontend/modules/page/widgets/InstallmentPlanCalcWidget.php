<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\InstallmentPlan;

/**
 * Class InstallmentPlanCalcWidget
 *
 * @package frontend\modules\page\widgets
 */
class InstallmentPlanCalcWidget extends Widget
{
    /**
     * @var InstallmentPlan associated model.
     */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model !== null) {
            $output = $this->render('installment-plan-calc', $this->prepareVars());
        }

        return $output;
    }

    private function prepareVars()
    {
        return [
            'formOnSubmit' => $this->model->form_calc_onsubmit,
            'defaultTimeTerm' => $this->model->default_time_term,
            'defaultInitialFee' => $this->model->default_initial_fee,
            'defaultApartmentPrice' => $this->model->default_apartment_price,
            'minTerm' => $this->model->min_term,
            'maxTerm' => $this->model->max_term,
        ];
    }
}
