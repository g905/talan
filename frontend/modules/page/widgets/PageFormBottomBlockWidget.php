<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use frontend\helpers\FrontendHelper;

/**
 * Class PageFormBottomBlockWidget
 *
 * @author art
 * @package frontend\modules\page\widgets
 */
class PageFormBottomBlockWidget extends Widget
{
    const FORM_POSITION_LEFT = 1;
    const FORM_POSITION_RIGHT = 2;

    const FORM_TYPE_QUESTION = 1;
    const FORM_TYPE_COMPLEX_PRESENTATION = 2;
    const FORM_TYPE_HYPOTHEC_WITH_DOC = 3;
    const FORM_TYPE_JOB_WITH_DOC = 4;
    const FORM_TYPE_GUARANTEE = 5;
    const FORM_TYPE_COOPERATION = 6;
    const FORM_TYPE_CUSTOM_GUARANTEE = 7;

    public $blockClass = '';
    public $formTitle = '';
    public $formTitleDark = '';
    public $formDescription = '';
    public $formPosition = self::FORM_POSITION_RIGHT;
    public $manager = null;
    public $formType = self::FORM_TYPE_QUESTION;
    public $complexId = null;
    public $buildingId = null;
    public $apartment_id = null;
    public $problems;
    public $agreementText;
    public $formWrapperClass;
    public $onsubmitJS;
    public $modelId = 0;
    public $vacancy;

    public function run()
    {
        if (FrontendHelper::isNullOrEmpty($this->formTitle)) {
            if ($this->formType == self::FORM_TYPE_QUESTION) {
                $this->formTitle = t('_We are always ready to answer any of your questions', 'promo-action');
            }
            if ($this->formType == self::FORM_TYPE_COMPLEX_PRESENTATION) {
                $this->formTitle = t('_Send a request for a full presentation of the complex', 'promo-action');
            }
        }

        if (FrontendHelper::isNullOrEmpty($this->formDescription)) {
            if ($this->formType == self::FORM_TYPE_QUESTION) {
                $this->formDescription = t('_Leave your data and write the question you are interested in. Our managers will contact you as soon as possible, and they will consult you in a qualified manner', 'promo-action');
            }
            if ($this->formType == self::FORM_TYPE_COMPLEX_PRESENTATION) {
                $this->formDescription = t('_Leave your details and we will send you a full presentation of the complex.', 'promo-action');
            }
        }

        if ($this->manager || $this->formType != self::FORM_TYPE_QUESTION) {
            return $this->render('pageFormBottomBlockWidgetView', [
                'manager' => $this->manager,
                'formType' => $this->formType,
                'formWrapperClass' => $this->formWrapperClass,
                'complexId' => $this->complexId,
                'buildingId' => $this->buildingId,
                'problems' => $this->problems,
                'formTitle' => $this->formTitle,
                'formTitleDark' => $this->formTitleDark,
                'blockClass' => $this->blockClass,
                'formPosition' => $this->formPosition,
                'agreementText' => $this->agreementText,
                'formDescription' => $this->formDescription,
                'onsubmitJS' => $this->onsubmitJS,
                'modelId' => $this->modelId,
                'vacancy' => $this->vacancy,
                'apartment_id' => $this->apartment_id,

            ]);
        }
    }
}
