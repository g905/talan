<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\Hypothec;

/**
 * Class InstallmentPlanStepsWidget
 *
 * @package frontend\modules\page\widgets
 */
class HypothecStepsWidget extends Widget
{
    /**
     * @var Hypothec associated model.
     */
    public $model;

    public function run()
    {
        $output = '';
        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('hypothec-steps', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        $output = [];
        if ($this->model !== null) {
            $output = [
                'title' => $this->model->steps_block_label,
                'steps' => $this->model->stepBlocks,
            ];
        }

        return $output;
    }
}
