<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\Realtor;

/**
 * Class RealtorFilesWidget
 *
 * @package frontend\modules\page\widgets
 */
class RealtorFilesWidget extends Widget
{
    /**
     * @var Realtor associated model.
     */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model === null) {
            return $output;
        }

        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('realtor-files', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        $output = [];
        $files = $this->model->fileBlocks;
        if ($files) {
            $output = [
                'title' => $this->model->files_label,
                'files' => $files
            ];
        }

        return $output;
    }
}
