<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\page\widgets;

use yii\base\Widget;

class PageSliderWidget extends Widget
{
    public $imagePath;
    public $videoPath;

    public function run()
    {
        if ((!isset($this->imagePath)) && (!isset($this->videoPath))) {
            return null;
        }

        echo $this->render('pageSliderWidgetView',[
            'imagePath' => $this->imagePath,
            'videoPath' => $this->videoPath,
        ]);
    }
}
