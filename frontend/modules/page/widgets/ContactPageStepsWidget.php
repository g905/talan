<?php

namespace frontend\modules\page\widgets;

use yii\base\Widget;
use common\models\ContactPage;

/**
 * Class ContactPageStepsWidget
 *
 * @package frontend\modules\page\widgets
 */
class ContactPageStepsWidget extends Widget
{
    /**
     * @var ContactPage associated model.
     */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model === null) {
            return $output;
        }

        $vars = $this->prepareVars();

        if (!empty($vars)) {
            $output = $this->render('contact-page-steps', $vars);
        }

        return $output;
    }

    private function prepareVars()
    {
        $output = [];
        $steps = $this->model->howToGetStepBlocks;
        if ($steps) {
            $output = [
                'title' => $this->model->hot_to_get_label,
                'steps' => $steps
            ];
        }

        return $output;
    }
}
