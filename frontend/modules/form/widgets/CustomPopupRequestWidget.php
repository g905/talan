<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 23.04.18
 * Time: 14:37
 */

namespace frontend\modules\form\widgets;


use common\models\CustomPopup;
use frontend\modules\form\models\CustomPopupForm;
use yii\base\Widget;
use Yii;
use yii\web\Cookie;

class CustomPopupRequestWidget extends Widget
{
    const COOKIE_NAME = 'popupRequestParam';

    /**
     * @var CustomPopupForm
     */
    public $requestModel;
    /**
     * @var CustomPopup
     */
    public $customPopupModel;
    /**
     * @var bool
     */
    public $delayBeforeOpen = false;
    /**
     * @var int|null complex identifier.
     */
    public $complexId;

    /**
     * @return string
     */
    public function run()
    {
    	
        $param = $this->getUrlParamValue();

        if ($param) {
            $this->customPopupModel = CustomPopup::find()->where(['url_param_value' => $param])->one();
        }
        if (!$this->customPopupModel) {
            $this->customPopupModel = CustomPopup::find()->where(['is_default' => 1])->one();
            if (!$this->customPopupModel) {
                $this->customPopupModel = new CustomPopup();
            }
        }
        
        if (!$this->requestModel) {
            $this->requestModel = new CustomPopupForm([
                'attributeLabelsData' => [
                    'name' => $this->customPopupModel->label_placeholder,
                    'email' => $this->customPopupModel->email_placeholder,
                    'phone' => $this->customPopupModel->phone_placeholder,
                    'agree' => $this->customPopupModel->agreement_text,
                ],
                'popupId' => $this->customPopupModel->id,
                'acId' => $this->complexId
            ]);
        }
        $visible = !$this->delayBeforeOpen;
        if ($this->delayBeforeOpen) {
            $this->addDelayBeforeShow();
        }


    	return $this->render('customPopupRequestWidgetView', [
            'model' => $this->requestModel,
            'popupModel' => $this->customPopupModel,
            'visible' => !$this->getShown(),
    	]);


        
    }

    /**
     * @return array|mixed
     */
    protected function getUrlParamValue()
    {
        $value = request()->get(CustomPopup::URL_PARAM_NAME);

        if ($value) {
            $this->writeNewValue();
            return $value;
        }
        if (is_null($value)) {
            $value = $this->getExistingCookieValue();
        }

        return $value;
    }

    /**
     * @return string|null
     */
    protected function getExistingCookieValue()
    {
        $cookie = Yii::$app->request->cookies->get(self::COOKIE_NAME);

        return $cookie->value ?? null;
    }

    /**
     * @return string|null
     */
    protected function writeNewValue()
    {
        $value = request()->get(CustomPopup::URL_PARAM_NAME);
        if (!$value) {
            return null;
        }
        $cookies = Yii::$app->response->cookies;
        $cookie = new Cookie([
            'name' => self::COOKIE_NAME,
            'value' => $value,
            'expire' => 0, // for current session only
        ]);

        $cookies->add($cookie);

        return $value;
    }

    /**
     * @return void
     */
    protected function addDelayBeforeShow()
    {
        $script = '
            $(document).ready(function() {
                var $popup = $("[data-popup_delay]").eq(0);
                var delay = $popup.data("popup_delay");
                delay = parseInt(delay); // strict integer
                delay = delay * 1000; // cast to milliseconds
                //visible = !'.$this->getShown().';
                visible = true;
                if(visible) {
	                setTimeout(function() {
	                    $popup.addClass("modal_active");
	                }, delay);	
                }
            });

        ';
        

        $this->view->registerJs($script);
    }

    public function getShown()
    {
    	$x = Yii::$app->request->cookies->has('banner_shown');
    	
		if(!$x)
		{
			$banner_cookie = new \yii\web\Cookie([
	    		'name' => 'banner_shown',
	    		'value' => true,
	    		'expire' => strtotime('+1 day', time()),
			]);
			Yii::$app->getResponse()->getCookies()->add($banner_cookie);
    		return "false";
		}
		return "true";
    }
}
