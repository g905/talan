<?php

namespace frontend\modules\form\widgets;

use backend\modules\apartment\models\ApartmentBuilding;
use frontend\modules\form\models\FormGuarantee;
use yii\base\Widget;
use frontend\modules\form\models\FormComplexPresentation;

/**
 * Class GuaranteeFormWidget
 *
 * @author dimarychek
 * @package frontend\modules\form\widgets
 */
class GuaranteeFormWidget extends Widget
{
    public $complexId;
    public $buildingId;
    public $problems;
    public $onsubmitJS;

    public function run()
    {
        $model = new FormGuarantee();
        $model->building_id = $this->buildingId;

        $buildingModel = new ApartmentBuilding();

        echo $this->render('guaranteeFormWidgetView', [
            'model' => $model,
            'problems' => $this->problems,
            'onsubmitJS' => $this->onsubmitJS,
        ]);
    }
}

