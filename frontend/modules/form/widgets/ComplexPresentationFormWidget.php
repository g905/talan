<?php

namespace frontend\modules\form\widgets;

use yii\base\Widget;
use frontend\modules\form\models\FormComplexPresentation;

/**
 * Class ComplexPresentationFormWidget
 *
 * @author art
 * @package frontend\modules\form\widgets
 */
class ComplexPresentationFormWidget extends Widget
{
    public $complexId;
    public $buildingId;
    public $managerId;
    public $onsubmitJS;

    public function run()
    {
        $model = new FormComplexPresentation();
        $model->managerId = $this->managerId;
        $model->acId = $this->complexId;
        $model->abId = $this->buildingId;

        echo $this->render('complexPresentationFormWidgetView',[
            'model' => $model,
            'onsubmitJS' => $this->onsubmitJS,
        ]);
    }
}

