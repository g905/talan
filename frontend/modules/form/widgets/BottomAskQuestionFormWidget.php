<?php

namespace frontend\modules\form\widgets;

use yii\base\Widget;
use frontend\modules\form\models\FormAskQuestion;

/**
 * Class BottomAskQuestionFormWidget
 *
 * @author art
 * @package frontend\modules\form\widgets
 */
class BottomAskQuestionFormWidget extends Widget
{
    /**
     * @var string
     */
    public $agreementText;
    public $complexId;
    public $buildingId;
    public $managerId;
    public $onsubmitJS;
    public $apartment_id;

    /**
     * @return string|void
     */
    public function run()
    {
        $model = new FormAskQuestion();
        $model->acId = $this->complexId;
        $model->abId = $this->buildingId;
        $model->managerId = $this->managerId;
        $model->apartment_id = $this->apartment_id;

        echo $this->render('bottomAskQuestionFormWidgetView', [
            'model' => $model,
            'onsubmitJS' => $this->onsubmitJS,
            'agreementText' => obtain('agreementCheckboxText', app()->params, ''), // strip_tags($this->agreementText, '<a><strong><b><i>'),
        ]);
    }
}

