<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\SiteUrlHelper;
use frontend\modules\form\models\FormAskQuestion;

/**
 * @author art
 * @var $this yii\web\View
 * @var $agreementText string
 * @var $model FormAskQuestion
 * @var $onsubmitJS string
 */



?>

<?php $form = ActiveForm::begin([
    'action' => SiteUrlHelper::createFormQuestionUrl(),
    'options' => [
        'id' => 'question-form',
        'class' => 'form questions__form ajax-form wow fadeInUp',
      //  'data-onsubmit' => $onsubmitJS ? : config()->get('request_question_onsubmit', null),
        'data-onsubmit' =>  config()->get('request_question_onsubmit'),
    ], // important
    'enableAjaxValidation' => true,
    'validationUrl' => SiteUrlHelper::createFormQuestionValidationUrl(),
    'fieldConfig' => ['options' => ['class' => 'form__row']],
]) ?>
<?= Html::activeHiddenInput($model, 'acId') ?>
<?= Html::activeHiddenInput($model, 'abId') ?>
<?= Html::activeHiddenInput($model, 'managerId') ?>
<input type="hidden" value="<?= $model->apartment_id ?>" class="form__input" name="FormAskQuestion[apartment_id]" >

<div class="form__row form__row_split_2-custom">
    <?= $form->field($model, 'name', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>
    <?= $form->field($model, 'phone', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['type' => 'tel','class' => 'input form__input input_phone']])->textInput([
        'type' => 'tel'
    ]) ?>
</div>

<?= $form->field($model, 'email', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>
<?= $form->field($model, 'question', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>

<div class="form__row">
    <div id="question-form-agree-block" class="field form__field">
        <label class="checkbox form__checkbox">
            <input type="checkbox" class="checkbox__checkbox" name="FormAskQuestion[iAgree]" data-validation-name="agree"><span class="checkbox__mask"></span>
            <span class="checkbox__label">
                <?php if (!empty($agreementText)) : ?>
                    <?= $agreementText ?>
                <?php else: ?>
                    <?= t('_I agree', 'form-ask-question') ?>
                    <a href="#" target="_blank"><?= t('_to the processing of personal data', 'form-ask-question') ?></a>
                <?php endif ?>
            </span>
        </label>
    </div>
</div>
<div class="form__row">
    <button id="btn-questions-form-submit" type="submit" class="button form__button wow fadeInUp">
        <?= t('_Send', 'form-ask-question') ?>
        <span class="button__blip button__blip_hover"></span>
        <span class="button__blip button__blip_click"></span>
    </button>
</div>
<?php ActiveForm::end(); ?>
