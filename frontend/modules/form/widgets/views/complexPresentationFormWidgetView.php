<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\SiteUrlHelper;
use frontend\modules\form\models\FormComplexPresentation;

/**
 * @author art
 * @var $this yii\web\View
 * @var $model FormComplexPresentation
 * @var $onsubmitJS string
 */
?>

<?php $form = ActiveForm::begin([
    'action' => SiteUrlHelper::createFormComplexPresentationUrl(['acId' => $model->acId]),
    'options' => [
        'id' => 'presentation-form',
        'class' => 'form questions__form ajax-form wow fadeInUp',
        'data-wow-delay' => '0.3s',
        'data-onsubmit' => $onsubmitJS ? : config()->get('request_excursion_onsubmit', null),
    ], // important
    'enableAjaxValidation' => true,
    'validationUrl' => SiteUrlHelper::createFormComplexPresentationValidateUrl(),
    'fieldConfig' => ['options' => ['class' => 'form__row']],
]) ?>
    <div class="form__row form__row_split_2-custom">
        <?= $form->field($model, 'name', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>
        <?= $form->field($model, 'phone', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['type' => 'tel','class' => 'input form__input input_phone']])->textInput([
            'type' => 'tel'
        ]) ?>
    </div>

    <?= $form->field($model, 'email', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>

    <input type="hidden" id="presentation-form-ac-id" class="input form__input" value="<?=$model->acId?>" name="FormComplexPresentation[acId]">
    <input type="hidden" id="presentation-form-ab-id" class="input form__input" value="<?=$model->abId?>" name="FormComplexPresentation[abId]">
    <?= Html::activeHiddenInput($model, 'managerId') ?>

    <div class="form__row">
        <div id="presentation-form-agree-block" class="field form__field">
            <label class="checkbox form__checkbox">
                <input type="checkbox" class="checkbox__checkbox" name="FormComplexPresentation[iAgree]" data-validation-name="agree"><span class="checkbox__mask"></span>
                <span class="checkbox__label"><?= obtain('agreementCheckboxText', app()->params, '') ?></span>
            </label>
        </div>
    </div>

    <div class="form__row">
        <button id="btn-presentation-form-submit" type="submit" class="button form__button">
            <?= t('_Send', 'form-ask-question') ?>
            <span class="button__blip button__blip_hover"></span>
            <span class="button__blip button__blip_click"></span>
        </button>
    </div>

<?php $form::end() ?>
