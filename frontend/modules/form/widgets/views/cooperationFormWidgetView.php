<?php

use yii\bootstrap\ActiveForm;
use common\helpers\SiteUrlHelper;

/**
 * @var $this yii\web\View
 * @var $agreementText string
 * @var $model frontend\modules\form\models\FormCooperation
 * @var $onsubmitJS string
 */
?>

<?php $form = ActiveForm::begin([
    'action' => SiteUrlHelper::createFormCooperationUrl(),
    'options' => [
        'id' => 'question-form',
        'class' => 'form ajax-form questions__form wow fadeInUp',
        'data-onsubmit' => $onsubmitJS,
        //'data' => ['form' => null],
    ],
    'enableAjaxValidation' => true,
    'validationUrl' => SiteUrlHelper::createFormCooperationValidationUrl(),
    'fieldConfig' => ['options' => ['class' => 'form__row']],
]) ?>

<div class="form__row form__row_split_2">
    <?= $form->field($model, 'name', ['options' => ['tag' => 'label', 'class' => 'field form__field'], 'template' => '{input}<span class="form__label">{labelTitle}</span>{error}', 'inputOptions' => ['class' => 'input form__input']]) ?>
    <?= $form->field($model, 'phone', ['options' => ['tag' => 'label', 'class' => 'field form__field'], 'template' => '{input}<span class="form__label">{labelTitle}</span>{error}', 'inputOptions' => ['class' => 'input form__input  input_phone']])->textInput([
        'type' => 'tel'
    ]) ?>
</div>

<div class="form__row form__row_split_2">
    <?= $form->field($model, 'company_name', ['options' => ['tag' => 'label', 'class' => 'field form__field'], 'template' => '{input}<span class="form__label">{labelTitle}</span>{error}', 'inputOptions' => ['class' => 'input form__input']]) ?>
    <?= $form->field($model, 'chief_phone', ['options' => ['tag' => 'label', 'class' => 'field form__field'], 'template' => '{input}<span class="form__label">{labelTitle}</span>{error}', 'inputOptions' => ['class' => 'input form__input']]) ?>
</div>

<?= $form->field($model, 'email', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>

<div class="form__row">
    <div id="question-form-agree-block" class="field form__field">
        <label class="checkbox form__checkbox">
            <input type="checkbox" class="checkbox__checkbox" name="FormCooperation[iAgree]" required data-validation-name="agree">
            <span class="checkbox__mask"></span>
            <span class="checkbox__label">
                <?php if (!empty($agreementText)) : ?>
                    <?= $agreementText ?>
                <?php else: ?>
                    <?= t('_I agree', 'form-ask-question') ?>
                    <a href="#" target="_blank"><?= t('_to the processing of personal data', 'form-ask-question') ?></a>
                <?php endif ?>
            </span>
        </label>
    </div>
</div>

<div class="form__row">
    <button id="btn-questions-form-submit" type="submit" class="button form__button wow fadeInUp">
        <?= t('_Send', 'form-ask-question') ?>
        <div class="button__blip button__blip_hover"></div>
        <div class="button__blip button__blip_click"></div>
    </button>
</div>

<?php ActiveForm::end(); ?>
