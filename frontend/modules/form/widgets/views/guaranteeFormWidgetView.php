<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\SiteUrlHelper;
use yii\helpers\ArrayHelper;
use frontend\modules\form\models\FormGuarantee;
use common\models\RequestGuarantee;
use backend\modules\apartment\models\ApartmentBuilding;

/**
 * @author dimarychek
 * @var $this yii\web\View
 * @var $model FormGuarantee
 * @var $onsubmitJS string
 * @var $problems object
 */
?>

<?php $form = ActiveForm::begin([
    'action' => SiteUrlHelper::createFormGuaranteeUrl(),
    'options' => [
        'id' => 'guarantee-form',
        'class' => 'form questions__form ajax-form wow fadeInUp',
        'data-wow-delay' => '0.3s',
        'data-onsubmit' => $onsubmitJS ? : config()->get('request_excursion_onsubmit', null),
    ], // important
    'enableAjaxValidation' => true,
    'validationUrl' => SiteUrlHelper::createFormGuaranteeValidateUrl(),
    'fieldConfig' => ['options' => ['class' => 'form__row']],
]) ?>

    <div class="form__row">
        <label class="field form__field">
            <span class="filters__inner filters__inner_side_right tal-section__filters-form">
                <span class="custom-select filters__custom-select" data-custom-select>
                    <?= Html::activeDropDownList($model, 'problem_type', ArrayHelper::map($problems, 'id', 'label')) ?>
                </span>
            </span>
        </label>
    </div>

    <div class="form__row form__row_split_2-custom">
        <?= $form->field($model, 'name', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>
        <?= $form->field($model, 'phone', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['type' => 'tel','class' => 'input form__input input_phone']])->textInput([
            'type' => 'tel'
        ]) ?>
    </div>

    <?= $form->field($model, 'email', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>
    <?= $form->field($model, 'problem', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>

    <?= Html::activeHiddenInput($model, 'building_id') ?>

    <div class="form__row">
        <div id="guarantee-form-agree-block" class="field form__field">
            <label class="checkbox form__checkbox">
                <input type="checkbox" class="checkbox__checkbox" name="FormGuarantee[iAgree]" data-validation-name="agree"><span class="checkbox__mask"></span>
                <span class="checkbox__label"><?= obtain('agreementCheckboxText', app()->params, '') ?></span>
            </label>
        </div>
    </div>

    <div class="form__row">
        <button id="btn-presentation-form-submit" type="submit" class="button form__button">
            <?= t('_Send', 'form-ask-question') ?>
            <span class="button__blip button__blip_hover"></span>
            <span class="button__blip button__blip_click"></span>
        </button>
    </div>

<?php $form::end() ?>
