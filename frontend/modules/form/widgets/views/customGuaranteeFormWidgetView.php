<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\SiteUrlHelper;

?>

<?php $form = ActiveForm::begin([
    'action' => SiteUrlHelper::createFormCustomGuaranteeUrl(),
    'options' => [
        'id' => 'question-form',
        'class' => 'form questions__form ipot-questions ajax-form wow fadeInUp hypothec-with-doc-form',
        'data-onsubmit' => $onsubmitJS,
        'enctype' => 'multipart/form-data',
    ],
    'enableAjaxValidation' => true,
    'validationUrl' => SiteUrlHelper::createFormCustomGuaranteeValidationUrl(),
    'fieldConfig' => ['options' => ['class' => 'field form__field']],
]) ?>

<?= Html::activeHiddenInput($model, 'modelId', ['value' => $modelId]) ?>

<div class="form__row form__row_split_2">
    <?= $form->field($model, 'name', ['template' => '<label>{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>
    <?= $form->field($model, 'phone', ['template' => '<label>{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['type' => 'tel','class' => 'input form__input input_phone']])->textInput([
        'type' => 'tel'
    ]) ?>
</div>

<div class="form__row form__row_split_2">
    <?= $form->field($model, 'email', ['template' => '<label>{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>

    <div class="filters__inner filters__inner_side_right">
        <div class="custom-select filters__custom-select city_select_guarantee" data-custom-select>
            <select class="guarantee_input_city_select">
                <?php foreach ($cities as $city) : ?>
                    <option value="<?= $city->id ?>" data-city="<?= $city->label ?>"><?= $city->label ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <?= Html::activeHiddenInput($model, 'city', ['class' => 'guarantee_input_city', 'value' => reset($cities)->id]) ?>
</div>

<div class="form__row form__row_split_3">
    <div class="filters__inner filters__inner_side_right">
        <div class="custom-select filters__custom-select complex_select_guarantee" data-custom-select>
            <select class="custom-select filters__custom-select guarantee_input_complex_select">
                <?php foreach ($cities as $city) : ?>
                    <?php foreach ($city->guaranteeComplexes as $complex) : ?>
                        <option value="<?= $complex->id ?>" data-cityid="<?= $city->id ?>"><?= $complex->label ?></option>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <?= Html::activeHiddenInput($model, 'complex', ['class' => 'guarantee_input_complex', 'value' => reset($cities)->complexes[0]->id]) ?>

    <?= $form->field($model, 'building_number', ['template' => '<label>{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>

    <?= $form->field($model, 'apartment_number', ['template' => '<label>{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>
</div>

<?= $form->field($model, 'comment', ['template' => '<label>{input}<span class="form__label">{labelTitle}</span>{error}</label>'])->textarea(['rows' => '6', 'class' => 'input form__textarea form__input form_field']) ?>

<?= $form->field($model, 'file', [
    'template' => '<label class="field form__field add-file">{input}<span class="form__label"><img src="/static/img/ipoteka/paper-clip.png" alt="paperclip"><span class="label_file" data-old-label="{labelTitle}">{labelTitle}</span></span><span class="file_error">{error}</span></label>',
    'inputOptions' => ['class' => 'input form__input'
    ]])->fileInput() ?>

<div class="form__row agreement_text_guarantee">
    <div id="question-form-agree-block" class="field form__field">
        <label class="checkbox form__checkbox">
            <input type="checkbox" class="checkbox__checkbox" name="FormAskQuestion[iAgree]" required data-validation-name="agree"><span class="checkbox__mask"></span>
            <span class="checkbox__label">
                <?php if (!empty($agreementText)) : ?>
                    <?= $agreementText ?>
                <?php else: ?>
                    <?= obtain('agreementCheckboxText', app()->params, '') ?>
                <?php endif ?>
            </span>
        </label>
    </div>
</div>
<div class="form__row">
    <button id="btn-questions-form-submit" type="submit" class="button form__button wow fadeInUp">
        <?= t('_Send', 'form-ask-question') ?>
        <div class="button__blip button__blip_hover"></div>
        <div class="button__blip button__blip_click"></div>
    </button>
</div>

<?php ActiveForm::end(); ?>
