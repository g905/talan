<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 23.04.18
 * Time: 14:41
 *
 * @var $model \frontend\modules\form\models\CustomPopupForm
 * @var $popupModel \common\models\CustomPopup
 * @var $visible boolean
 */
?>

<div class="popup_presentation modal modals__modal popup-replace" data-modal="presentation" data-popup_delay="60">
    <div class="modal__outer">
        <div class="modal__overlay" data-modal-close></div>
        <div class="modal__inner popup_presentation__inner">
            <div class="modal__content">
                <div class="popup_presentation__wrap">
                    <button class="modal__close" data-modal-close></button>
                    <h3 class="popup_presentation__title"><?= $popupModel->white_label ?></h3>
                    <h3 class="popup_presentation__title-sec"><?= $popupModel->black_label ?></h3>
                    <p class="popup_presentation__subtitle"><?= $popupModel->sub_label ?></p>
                    <?php $form = ActiveForm::begin([
                        'action' => \common\helpers\SiteUrlHelper::getCustomPopupRequestSubmitUrl([]),
                        'method' => 'POST',
                        'options' => [
                            'class' => 'popup_presentation__form ajax-form',
                            'data-onsubmit' => $popupModel->form_onsubmit ? : config()->get('request_custom_onsubmit', null),
                        ],
                        'enableAjaxValidation' => false,
                        'fieldConfig' => ['options' => ['class' => 'form__row']],
                    ]); ?>
                        <div class="popup_presentation__label-wrap">
                            <?= Html::activeHiddenInput($model, 'acId') ?>
                            <?= $form->field($model, 'name', [
                                'options' => [
                                    'class' => 'field form__field',
                                    'tag' => 'label',
                                ],
                                'template' => "{input}\n<span class='form__label'>" . $model->getAttributeLabel('name') . "</span>\n{error}",
                            ])->textInput([
                                'class' => 'input form__input'
                            ])->label(false)->error(['tag' => 'span', 'class' => 'field__error']) ?>
                            <?= $form->field($model, 'phone', [
                                'options' => [
                                    'class' => 'field form__field',
                                    'tag' => 'label',
                                ],
                                'template' => "{input}\n<span class='form__label'>" . $model->getAttributeLabel('phone') . "</span>\n{error}",
                            ])->textInput([
                                'class' => 'input form__input input_phone',
                                'type' => 'tel'
                            ])->label(false)->error(['tag' => 'span', 'class' => 'field__error']) ?>
                            <?= $form->field($model, 'email', [
                                'options' => [
                                    'class' => 'field form__field',
                                    'tag' => 'label',
                                ],
                                'template' => "{input}\n<span class='form__label'>" . $model->getAttributeLabel('email') . "</span>\n{error}",
                            ])->textInput([
                                'class' => 'input form__input'
                            ])->label(false)->error(['tag' => 'span', 'class' => 'field__error']) ?>
                        </div>
                        <?= $form->field($model, 'agree', [
                            'template' => '<label class="checkbox form__checkbox">{input}<span class="checkbox__mask"></span>{label}{error}</label>',
                            'options' => [
                                'class' => 'field form__field popup_presentation__checkbox-wrap',
                            ],
                            'errorOptions' => [
                                'class' => 'field__error',
                                'tag' => 'span'
                            ]
                        ])->checkbox([
                            'class' => 'checkbox__checkbox',
                            'required' => true,
                        ], false)->label($model->getAttributeLabel('agree'), [
                            'class' => 'checkbox__label',
                        ]) ?>
                        <?= $form->field($model, 'popupId')->hiddenInput()->label(false) ?>
                        <button class="button button_green form__button">
                            <?= $popupModel->button_label ?>
                            <span class="button__blip button__blip_hover"></span>
                            <span class="button__blip button__blip_click"></span>
                        </button>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
