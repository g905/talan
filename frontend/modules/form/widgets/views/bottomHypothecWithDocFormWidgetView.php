<?php

use yii\bootstrap\ActiveForm;
use common\helpers\SiteUrlHelper;
use frontend\modules\form\models\FormHypothecWithDoc;

/**
 * @author art
 * @var $this yii\web\View
 * @var $agreementText string
 * @var $model FormHypothecWithDoc
 * @var $onsubmitJS string
 * @var $hypothecQuestionnaireDoc string|false
 * @var $hypothecListOfNeededDocs string|false
 */
?>

<div class="text questions__text questions__text_tab wow fadeInUp">
    <p> Оставьте свои данные и напишите интересующий вопрос. Наши менеджеры как можно оперативнее свяжутся с вами, и квалифицированно проконсультируют. </p>
</div>

<?php if ($hypothecQuestionnaireDoc && $hypothecListOfNeededDocs) : ?>
<div class="questions__links"> Перед подачей заявки ознакомьтесь со
    <a href="<?= $hypothecListOfNeededDocs ?>" class="questions__link" target="_blank">списком необходимых документов</a> и
    <a href="<?= $hypothecQuestionnaireDoc ?>" class="questions__link" target="_blank" download> скачать бланк анкеты заемщика </a>
</div>
<?php endif ?>

<?php $form = ActiveForm::begin([
    'action' => SiteUrlHelper::createFormHypothecWithDocUrl(),
    'options' => [
        'id' => 'question-form',
        'class' => 'form questions__form ipot-questions ajax-form wow fadeInUp hypothec-with-doc-form',
        'data-onsubmit' => $onsubmitJS,
        'enctype' => 'multipart/form-data',
    ],
    'enableAjaxValidation' => true,
    'validationUrl' => SiteUrlHelper::createFormHypothecWithDocValidationUrl(),
    'fieldConfig' => ['options' => ['class' => 'form__row']],
]) ?>

<div class="form__row form__row_split_2-custom">
    <?= $form->field($model, 'name', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>
    <?= $form->field($model, 'phone', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['type' => 'tel','class' => 'input form__input input_phone']])->textInput([
        'type' => 'tel'
    ]) ?>
</div>

<?= $form->field($model, 'email', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>
<?= $form->field($model, 'file', [
    'template' => '<label class="field form__field add-file">{input}<span class="form__label"><img src="/static/img/ipoteka/paper-clip.png" alt="paperclip"><span class="label_file">{labelTitle}</span></span>{error}</label>',
    'inputOptions' => ['class' => 'input form__input'
]])->fileInput() ?>




<div class="form__row">
    <div id="question-form-agree-block" class="field form__field">
        <label class="checkbox form__checkbox">
            <input type="checkbox" class="checkbox__checkbox" name="FormAskQuestion[iAgree]" required data-validation-name="agree"><span class="checkbox__mask"></span>
            <span class="checkbox__label">
                <?php if (!empty($agreementText)) : ?>
                    <?= $agreementText ?>
                <?php else: ?>
                    <?= t('_I agree', 'form-ask-question') ?>
                    <a href="#" target="_blank"><?= t('_to the processing of personal data', 'form-ask-question') ?></a>
                <?php endif ?>
            </span>
        </label>
    </div>
</div>
<div class="form__row">
    <button id="btn-questions-form-submit" type="submit" class="button form__button wow fadeInUp">
        <?= t('_Send', 'form-ask-question') ?>
        <div class="button__blip button__blip_hover"></div>
        <div class="button__blip button__blip_click"></div>
    </button>
</div>
<?php ActiveForm::end(); ?>
