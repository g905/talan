<?php

namespace frontend\modules\form\widgets;

use backend\modules\apartment\models\ApartmentBuilding;
use common\models\City;
use frontend\modules\form\models\FormCustomGuarantee;
use frontend\modules\form\models\FormGuarantee;
use frontend\modules\form\models\FormJobWithDoc;
use yii\base\Widget;
use frontend\modules\form\models\FormComplexPresentation;

/**
 * Class GuaranteeFormWidget
 *
 * @author dimarychek
 * @package frontend\modules\form\widgets
 */
class CustomGuaranteeFormWidget extends Widget
{
    public $complexId;
    public $buildingId;
    public $problems;
    public $onsubmitJS;
    public $agreementText;
    public $modelId;

    public function run()
    {
        $model = new FormCustomGuarantee();

        $userCity[] = City::getUserCity();

        $cities = City::getCitiesWithGuarantee();

        $cities = array_merge($userCity, $cities);

        echo $this->render('customGuaranteeFormWidgetView', [
            'model' => $model,
            'problems' => $this->problems,
            'onsubmitJS' => $this->onsubmitJS,
            'agreementText' => $this->agreementText,
            'cities' => $cities,
            'modelId' => $this->modelId
        ]);
    }
}

