<?php

namespace frontend\modules\form\widgets;

use yii\base\Widget;
use frontend\modules\form\models\FormJobWithDoc;

/**
 * Class BottomJobWithDocFormWidget
 *
 * @package frontend\modules\form\widgets
 */
class BottomJobWithDocFormWidget extends Widget
{
    /**
     * @var string
     */
    public $agreementText;
    public $onsubmitJS;
    public $vacancy;

    public function run()
    {
        //dd($this->vacancy);
        $model = new FormJobWithDoc();

        return $this->render('bottomJobWithDocFormWidgetView', [
            'model' => $model,
            'onsubmitJS' => $this->onsubmitJS,
            'agreementText' => $this->agreementText ?? obtain('agreementCheckboxText', app()->params, ''),
            'vacancy' => $this->vacancy
        ]);
    }
}



