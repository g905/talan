<?php

namespace frontend\modules\form\widgets;

use common\helpers\ConfigHelper;
use yii\base\Widget;
use frontend\modules\form\models\FormHypothecWithDoc;

/**
 * Class BottomHypothecWithDocFormWidget
 *
 * @package frontend\modules\form\widgets
 */
class BottomHypothecWithDocFormWidget extends Widget
{
    /**
     * @var string
     */
    public $agreementText;
    public $onsubmitJS;

    public function run()
    {
        $model = new FormHypothecWithDoc();
        $hypothecQuestionnaireDoc = ConfigHelper::getConfigFile('hypothecQuestionnaireDoc', false);
        $hypothecListOfNeededDocs = ConfigHelper::getConfigFile('hypothecListOfNeededDocs', false);

        return $this->render('bottomHypothecWithDocFormWidgetView', [
            'model' => $model,
            'onsubmitJS' => $this->onsubmitJS,
            'hypothecQuestionnaireDoc' => $hypothecQuestionnaireDoc,
            'hypothecListOfNeededDocs' => $hypothecListOfNeededDocs,
            'agreementText' => obtain('agreementCheckboxText', app()->params, ''), // strip_tags($this->agreementText, '<a><strong><b><i>'),
        ]);
    }
}

