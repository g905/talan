<?php

namespace frontend\modules\form\widgets;

use yii\base\Widget;
use frontend\modules\form\models\FormCooperation;

/**
 * Class CooperationFormWidget
 *
 * @package frontend\modules\form\widgets
 */
class CooperationFormWidget extends Widget
{
    /**
     * @var string
     */
    public $agreementText;
    public $onsubmitJS;

    public function run()
    {
        return $this->render('cooperationFormWidgetView', [
            'model' => new FormCooperation(),
            'onsubmitJS' => $this->onsubmitJS,
            'agreementText' => $this->agreementText ?: obtain('agreementCheckboxText', app()->params, ''), // strip_tags($this->agreementText, '<a><strong><b><i>'),
        ]);
    }
}

