<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 27.12.2017
 * Time: 19:20
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\SiteUrlHelper;
?>

<div class="modal modals__modal popup-replace" data-modal="call-back">
    <div class="modal__outer">
        <div class="modal__overlay" data-modal-close></div>
        <div class="modal__inner">
            <div class="modal__content">
                <div class="popup">
                    <button class="modal__close" data-modal-close></button>
                    <h4 class="h4 popup__h4 "><?= $agent->fio ?></h4>
                    <h5 class="h5 popup__h5 "><?= $agent->phone ?></h5>

                </div>
            </div>
        </div>
    </div>
</div>
