<?php
/**
 * @var $this yii\web\View
 * @var $model frontend\modules\form\models\FormInstallmentPlan
 */

use yii\bootstrap\ActiveForm;
use common\helpers\SiteUrlHelper;
?>

<div class="modal modals__modal popup-replace" data-modal="rassrochka">
    <div class="modal__outer">
        <div class="modal__overlay" data-modal-close></div>
        <div class="modal__inner">
            <div class="modal__content">
                <div class="popup">
                    <button class="modal__close" data-modal-close></button>
                    <h5 class="h5 popup__h5 "><?= Yii::t('front/form-hypothec', 'Instalment plan request') ?></h5>
                    <?php $form = ActiveForm::begin([
                        'action' => SiteUrlHelper::createFormInstalmentPlanPopupUrl(),
                        'options' => [
                            'id' => 'question-form',
                            'class' => 'form popup__form ajax-form',
                            'data-onsubmit' => $pageModel->form_calc_onsubmit ?? false,
                        ],
                        'enableAjaxValidation' => true,
                        'fieldConfig' => ['options' => ['class' => 'form__row']],
                    ]); ?>
                    <?= $form->field($model, 'name', [
                        'template' => '{input}',
                        'inputOptions' => ['class' => 'form__input', 'required' => true, 'placeholder' => Yii::t('front/form-ask-question', '_Your name')]
                    ]) ?>
                    <?= $form->field($model, 'phone', [
                        'template' => '{input}',
                        'inputOptions' => ['class' => 'form__input input_phone', 'required' => true, 'placeholder' => Yii::t('front/form-ask-question', '_Phone')]
                    ])->textInput([
                        'type' => 'tel'
                    ]) ?>

                    <input type="hidden" value="<?= $apartmentId ?>" class="form__input" name="FormInstallmentPlan[apartment_id]"  aria-required="true">

                    <div class="field form__field">
                        <label class="checkbox form__checkbox">
                            <input type="checkbox" id="debug" class="checkbox__checkbox" name="_agree" required data-validation-name="agree">
                            <span class="checkbox__mask"></span>
                            <span class="checkbox__label"><?= obtain('agreementCheckboxText', app()->params, '') ?></span>
                        </label>
                    </div>
                    <button type="submit" class="button button_green form__button">
                        <?= Yii::t('front/form-ask-question', '_Send') ?>
                        <div class="button__blip button__blip_hover"></div>
                        <div class="button__blip button__blip_click"></div>
                    </button>
                    <input type="hidden" name="price" class="js-popup-price" value="">
                    <input type="hidden" name="term" class="js-popup-term" value="">
                    <input type="hidden" name="first_value" class="js-popup-first_value" value="">
                    <input type="hidden" name="payment" class="js-popup-payment" value="">
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
