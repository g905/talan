<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 27.12.2017
 * Time: 19:20
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\SiteUrlHelper;
?>
<div class="modal modals__modal popup-replace" data-modal="" >
    <div class="modal__outer">
        <div class="modal__overlay" data-modal-close onclick="$('iframe').attr('src', null);"></div>
        <div class="modal__inner">
            <div class="modal__content">
                <div class="popup" style="width: auto; padding: 0; max-height: 320px;">
                    <button class="modal__close" data-modal-close onclick="$('iframe').attr('src', null);"></button>
                    <iframe id="ytplayer" type="text/html" width="720" height="405"
                            src='<?= $video ?>?autoplay=1'
                            frameborder="0" allowfullscreen>


                </div>
            </div>
        </div>
    </div>
</div>
