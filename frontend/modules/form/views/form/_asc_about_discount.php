<?php

use common\models\City;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\SiteUrlHelper;

$city = City::getUserCity();
//$formTitle = Yii::$app->config->get('formAscQuerstionTitle');

if ($city->label == "Хабаровск" || $city->label == "Уфа" || $city->label == "Пермь" || $city->label == "Тюмень") {
    $formTitle = 'Отправьте контакты, чтобы забронировать квартиру';
}
else {
    $formTitle = 'Отправьте контакты, чтобы узнать стоимость квартиры';
}
//$formTitle = 'Узнать цену квартиры';
?>

<div class="modal modals__modal popup-replace" data-modal="call-back">
	<div class="modal__outer">
		<div class="modal__overlay" data-modal-close></div>
		<div class="modal__inner">
			<div class="modal__content">
				<div class="popup">
					<button class="modal__close" data-modal-close></button>
					<h5 class="h5 popup__h5 "><?= $formTitle; ?> </h5>

					<?php $form = ActiveForm::begin([
						'action' => SiteUrlHelper::createFormAscAboutDiscPopupUrl(),
						'options' => [
							'id' => 'question-form',
							'class' => 'form popup__form ajax-form',
							'data-onsubmit' => config()->get('request_callback_onsubmit', null),
						], // important
						'enableAjaxValidation' => true,
						'fieldConfig' => ['options' => ['class' => 'form__row']],
					]); ?>

					<?= $form->field($model, 'username', [
						'template' => '{input}',
						'inputOptions' => [
							'class' => 'form__input',
							'placeholder' => Yii::t('front/form-ask-question', '_Your name'),
						]
					]) ?>
					<?= $form->field($model, 'phone', [
						'template' => '{input}',
						'inputOptions' => [
							'class' => 'form__input input_phone',
							'placeholder' => Yii::t('front/form-ask-question', '_Phone'),
						]
					])->textInput([
                        'type' => 'tel'
                    ]) ?>
                    <input type="hidden" value="<?= $apartmentId ?>" class="form__input" name="FormAscAboutDiscount[apartment_id]" placeholder="_Phone" aria-required="true">
					<div class="field form__field">
						<label class="checkbox form__checkbox">
							<input type="checkbox" id="debug" class="checkbox__checkbox" name="_agree" required="" data-validation-name="agree">
							<span class="checkbox__mask"></span>
							<span class="checkbox__label">
                                <?= obtain('agreementCheckboxText', app()->params, '') ?>
                            </span>
						</label>
					</div>
					<button type="submit" class="button button_green form__button">
						<?= Yii::t('front/form-ask-question', '_Send') ?>
						<span class="button__blip button__blip_hover"></span>
						<span class="button__blip button__blip_click"></span>
					</button>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
