<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 27.12.2017
 * Time: 19:20
 */
?>
<div class="modal modals__modal popup-replace" data-modal="thanks">
    <div class="modal__outer">
        <div class="modal__overlay" data-modal-close></div>
        <div class="modal__inner">
            <div class="modal__content">

                <div class="popup">
                    <h5 class="h5 popup__h5">
                        <?=Yii::t('front/agents', '_Thanks')?>
                    </h5>

                    <div class="popup__text">
                        <p>
                            <?=Yii::t('front/agents', '_Your application has been successfully submitted. Our manager will contact you soon.')?>
                        </p>
                    </div>

                    <button class="button button_green popup__button" data-modal-close>
                        <?=Yii::t('front/agents', '_Close')?>
                        <span class="button__blip button__blip_hover"></span>
                        <span class="button__blip button__blip_click"></span>
                    </button>

                </div>
            </div>
        </div>
    </div>
</div>
