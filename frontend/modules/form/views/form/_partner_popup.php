<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 27.12.2017
 * Time: 19:20
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\SiteUrlHelper;
?>

<div class="modal modals__modal popup-replace" data-modal="call-back">
    <div class="modal__outer">
        <div class="modal__overlay" data-modal-close></div>
        <div class="modal__inner">
            <div class="modal__content">
                <div class="popup">
                    <button class="modal__close" data-modal-close></button>
                    <h5 class="h5 popup__h5 "><?= Yii::t('front/form-callback', '_Partner request') ?></h5>
                    <?php $form = ActiveForm::begin([
                        'action' => SiteUrlHelper::createFormPartnerPopupUrl(),
                        'options' => [
                            'id' => 'question-form',
                            'class' => 'form popup__form ajax-form',
                            'data-onsubmit' => $pageModel->form_onsubmit ?? false,
                        ], // important
                        'enableAjaxValidation' => true,
                        //    'validateOnBlur' => true,
                        //    'validateOnChange' => true,
                        //    'validateOnSubmit' => false,
                        //    'enableClientValidation' => false,
                        'fieldConfig' => ['options' => ['class' => 'form__row']],
                    ]); ?>

                    <?= $form->field($model, 'name', [
                        'template' => '{input}',
                        'inputOptions' => [
                            'class' => 'form__input',
                            'placeholder' => Yii::t('front/form-ask-question', '_Your name'),
                        ]
                    ]) ?>
                    <?= $form->field($model, 'phone', [
                        'template' => '{input}',
                        'inputOptions' => [
                            'class' => 'form__input input_phone',
                            'placeholder' => Yii::t('front/form-ask-question', '_Phone'),
                        ]
                    ])->textInput([
                        'type' => 'tel'
                    ]) ?>
                    <div class="field form__field">
                        <label class="checkbox form__checkbox">
                            <input type="checkbox" id="debug" class="checkbox__checkbox" name="_agree" required="" data-validation-name="agree">
                            <span class="checkbox__mask"></span>
                            <span class="checkbox__label">
                                <?= obtain('agreementCheckboxText', app()->params, '') ?>
                            </span>
                        </label>
                    </div>
                    <div>
                        <input type="hidden" id="formpartner-city" class="form__input" name="FormPartner[city]" placeholder="city" aria-required="false" value="<?= $city->id?>">
                    </div>
                    <button type="submit" class="button button_green form__button">
                        <?= Yii::t('front/form-ask-question', '_Send') ?>
                        <span class="button__blip button__blip_hover"></span>
                        <span class="button__blip button__blip_click"></span>
                    </button>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
