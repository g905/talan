<?php

namespace frontend\modules\form\controllers;

use backend\modules\request\models\RequestCallback;
use backend\modules\request\models\RequestExcursion;
use backend\modules\request\models\RequestFullAmount;
use common\models\Agents;
use common\models\AgentsMembers;
use common\models\Apartment;
use common\models\City;
use common\models\ConstructorRequest;
use common\models\Earth;
use common\models\Hypothec;
use common\models\InstallmentPlan;
use common\models\Vacancy;
use frontend\modules\form\models\FormAgentsTakePartRequest;
use frontend\modules\form\models\FormAscAboutDiscount;
use frontend\modules\form\models\FormCustomGuarantee;
use frontend\modules\form\models\FormInvest;
use frontend\modules\form\models\FormSumm;
use yii\helpers\Html;
use yii\web\Response;
use yii\web\Controller;
use frontend\modules\form\models\FormPartner;
use frontend\modules\form\models\FormHypothec;
use frontend\modules\form\models\FormCallback;
use frontend\modules\form\models\FormGuarantee;
use frontend\modules\form\models\FormExcursion;
use frontend\modules\form\models\FormSecondary;
use frontend\modules\form\models\SubscribeForm;
use frontend\modules\form\models\FormJobWithDoc;
use frontend\modules\form\models\FormCommercial;
use frontend\modules\form\models\CustomPopupForm;
use frontend\modules\form\models\FormCooperation;
use frontend\modules\form\models\FormAskQuestion;
use frontend\modules\form\models\FormPublicDataFile;
use frontend\modules\form\models\FormHypothecWithDoc;
use frontend\modules\form\models\FormInstallmentPlan;
use frontend\modules\form\models\BottomSubscribeForm;
use frontend\modules\form\models\FormComplexPresentation;
use frontend\modules\form\widgets\CustomPopupRequestWidget;
use frontend\modules\blog\widgets\footerSubscribeForm\FooterSubscribeFormWidget;

/**
 * Default controller for the `request` module
 */
class FormController extends Controller
{
    public function actionFormQuestion()
    {
        $data = [];
        $model = new FormAskQuestion();
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax() && $model->load(request()->post())) {
            $data = $model->ajaxValidation();
            if (!isset($model->iAgree)) {
                $data['js'] = Html::script('$("#question-form-agree-block").addClass("has-error")');
            }
        }

        if (empty($data)) {

            $apartment = Apartment::find()->where(["id" => $model->apartment_id])->one();
            if(!empty($apartment)){
                $apartment_id = $apartment->label;
                $apartment_complex_id = $apartment->apartmentComplex->label;
            }
            else{
                $apartment_id = "Не определено";
                $apartment_complex_id = "Не определено";
            }
            $model->apartment_id = $apartment_id;
            $model->apartment_complex_id = $apartment_complex_id;
            $model->save();
            $data = [
                'js' => Html::script('showPopup(); $("form[id^=\"question-form\"]").find("input").val(""); $("#question-form-agree-block").removeClass("has-error")'),
                'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                'replaces' => [
                    ['data' => $this->renderPartial('_thanks_popup', []), 'what' => '.popup-replace'],
                ],
            ];
        }

        return $data;
    }

    public function actionFormQuestionValidate()
    {
        $data = [];
        response()->format = Response::FORMAT_JSON;
        $model = new FormAskQuestion();
        if (request()->getIsAjax() && $model->load(request()->post())) {
            $data = $model->ajaxValidation();
            if (!isset($model->iAgree)) {
                $data['js'] = Html::script('$("#question-form-agree-block").addClass("has-error")');
            }
        }

        return $data;
    }

    public function actionFormComplexPresentation()
    {
        response()->format = Response::FORMAT_JSON;
        $model = new FormComplexPresentation();
        if (request()->getIsAjax() && $model->load(request()->post())) {
            if (request()->get('acId') !== null) {
                $model->acId = request()->get('acId');
            }

            $data = $model->ajaxValidation();
            if (!empty($data)) {
                return $data;
            }

            if (!isset($model->iAgree)) {
                return [
                    'js' => Html::script('$("#presentation-form-agree-block").addClass("has-error")')
                ];
            }
        }

        $model->save();

        return [
            'js' => Html::script('showPopup(); $("#presentation-form").find("input:not(#presentation-form-ac-id)").val(""); $("#presentation-form-agree-block").removeClass("has-error")'),
            'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
            'replaces' => [['data' => $this->renderPartial('_thanks_popup', []), 'what' => '.popup-replace'],],
        ];
    }

    public function actionFormComplexPresentationValidate()
    {
        response()->format = Response::FORMAT_JSON;
        $model = new FormComplexPresentation();
        if (request()->getIsAjax() && $model->load(request()->post())) {
            if (request()->get('acId') !== null) {
                $model->acId = request()->get('acId');
            }
            $errors = $model->ajaxValidation();
            if (!empty($errors)) {
                return $errors;
            }
            if (!isset($model->iAgree)) {
                return [
                    'js' => Html::script('$("#presentation-form-agree-block").addClass("has-error")')
                ];
            }
        }

        return [];
    }

    public function actionFormGuarantee()
    {
        response()->format = Response::FORMAT_JSON;
        $model = new FormGuarantee();
        if (request()->getIsAjax() && $model->load(request()->post())) {
            if (request()->get('building_id') !== null) {
                $model->building_id = request()->get('building_id');
            }

            $data = $model->ajaxValidation();
            if (!empty($data)) {
                return $data;
            }

            if (!isset($model->iAgree)) {
                return [
                    'js' => Html::script('$("#guarantee-form-agree-block").addClass("has-error")')
                ];
            }
        }

        $model->save();

        return [
            'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
            'js' => Html::script('showPopup(); $("#guarantee-form").find("input:not(#presentation-form-ac-id)").val(""); $("#guarantee-form-agree-block").removeClass("has-error")'),
            'replaces' => [['data' => $this->renderPartial('_thanks_popup', []), 'what' => '.popup-replace'],],
        ];
    }

    public function actionFormGuaranteeValidate()
    {
        response()->format = Response::FORMAT_JSON;
        $model = new FormGuarantee();
        if (request()->getIsAjax() && $model->load(request()->post())) {
            if (request()->get('building_id') !== null) {
                $model->building_id = request()->get('building_id');
            }
            $errors = $model->ajaxValidation();
            if (!empty($errors)) {
                return $errors;
            }
            if (!isset($model->iAgree)) {
                return [
                    'js' => Html::script('$("#guarantee-form-agree-block").addClass("has-error")')
                ];
            }
        }

        return [];
    }

    public function actionFormSecondary()
    {
        response()->format = Response::FORMAT_JSON;
        $model = new FormSecondary();
        if (request()->getIsAjax() && $model->load(request()->post())) {
            $data = $model->ajaxValidation();
            if (!empty($data)) {
                return $data;
            }

            if (!isset($model->iAgree)) {
                return [
                    'js' => Html::script('$("#secondary-form-agree-block").addClass("has-error")')
                ];
            }
        }

        $apartment = Apartment::find()->where(["id" => $model->apartment_id])->one();
        if(!empty($apartment)){
            $apartment_id = $apartment->label;
            $apartment_complex_id = $apartment->apartmentComplex->label;
        }
        else{
            $apartment_id = "Не определено";
            $apartment_complex_id = "Не определено";
        }

        $model->apartment_id = $apartment_id;
        $model->apartment_complex_id = $apartment_complex_id;

        $model->save();

        return [
            'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
            'js' => Html::script('showPopup(); $("#secondary-form").find("input:not(#presentation-form-ac-id)").val(""); $("#secondary-form-agree-block").removeClass("has-error")'),
            'replaces' => [['data' => $this->renderPartial('_thanks_popup', []), 'what' => '.popup-replace'],],
        ];
    }

    public function actionFormSecondaryValidate()
    {
        response()->format = Response::FORMAT_JSON;
        $model = new FormSecondary();
        if (request()->getIsAjax() && $model->load(request()->post())) {
            $errors = $model->ajaxValidation();
            if (!empty($errors)) {
                return $errors;
            }
            if (!isset($model->iAgree)) {
                return [
                    'js' => Html::script('$("#secondary-form-agree-block").addClass("has-error")')
                ];
            }
        }

        return [];
    }

    public function actionFormCallbackPopup($header = '_Call back')
    {
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax()) {

            $model = new FormCallback();
            if ($model->load(request()->post()) && $model->save()) {
                return [
                    'js' => Html::script('showPopup();'),
                    'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                    'replaces' => [
                        ['data' => $this->renderPartial('_thanks_popup'), 'what' => '.popup-replace'],
                    ],
                ];
            }

            return [
                'js' => Html::script('showPopup();'),
                'replaces' => [
                    ['data' => $this->renderPartial('_callback_popup', ['model' => $model, 'header' => $header]), 'what' => '.popup-replace'],
                ],
            ];
        }
    }

    public function actionAgentsShowPhonePopup(string $id)
    {
        $agent = AgentsMembers::find()->where(['published' => 1, 'id' => $id])->one();
        $agent->phone_shows++;
        $agent->save(false);

        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax()) {

            return [
                'js' => Html::script('showPopup();'),
                'replaces' => [
                    ['data' => $this->renderPartial('_show_phone_popup', ['agent' => $agent]), 'what' => '.popup-replace'],
                ],
            ];
        }
    }

    public function actionAgentsTakePartForm()
    {
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax()) {
            $model = new FormAgentsTakePartRequest();
            if ($model->load(request()->post()) && $model->save()) {
                return [
                    'js' => Html::script('showPopup();'),
                    'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                    'replaces' => [
                        ['data' => $this->renderPartial('_agents_thanks_popup', []), 'what' => '.popup-replace'],
                    ],
                ];
            }
            return [
                'js' => Html::script('showPopup();'),
                'replaces' => [
                    ['data' => $this->renderPartial('_show_agents_take_part_popup', ['model' => $model]), 'what' => '.popup-replace'],
                ],
            ];
        }
    }

    public function actionAgentsVideoPopup()
    {
        response()->format = Response::FORMAT_JSON;
        $video = \Yii::$app->request->get('video');
        return [
            'js' => Html::script('showPopup();'),
            'replaces' => [
                ['data' => $this->renderPartial('_show_agents_video_popup', ['video' => $video]), 'what' => '.popup-replace'],
            ],
        ];
    }

    public function actionFormPartnerPopup()
    {
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax()) {

            $model = new FormPartner();
            $pageModel = Earth::find()->andWhere(['city_id' => City::getUserCity()])->one();
            if ($model->load(request()->post()) && $model->save()) {
                return [
                    'js' => Html::script('showPopup();'),
                    'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                    'replaces' => [
                        ['data' => $this->renderPartial('_thanks_popup', []), 'what' => '.popup-replace'],
                    ],
                ];
            }

            return [
                'js' => Html::script('showPopup();'),
                'replaces' => [
                    ['data' => $this->renderPartial('_partner_popup', ['model' => $model, 'pageModel' => $pageModel, 'city' => City::getUserCity()]), 'what' => '.popup-replace'],
                ],
            ];
        }
    }

    public function actionFormCommercialPopup()
    {
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax()) {
            $model = new FormCommercial();
            if ($model->load(request()->post()) && $model->save()) {
                return [
                    'js' => Html::script('showPopup();'),
                    'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                    'replaces' => [
                        ['data' => $this->renderPartial('_thanks_popup', []), 'what' => '.popup-replace'],
                    ],
                ];
            }

            return [
                'js' => Html::script('showPopup();'),
                'replaces' => [
                    ['data' => $this->renderPartial('_commercial_popup', ['model' => $model]), 'what' => '.popup-replace'],
                ],
            ];
        }
    }

    public function actionFormExcursionPopup()
    {
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax()) {
            $model = new FormExcursion();
            if ($model->load(request()->post())) {
                //$model->save();
                $city = City::getUserCity();
                $apartment = Apartment::find()->where(["id" => $model->apartment_id])->one();

                if(!empty($apartment)){
                    $apartment_id = $apartment->label;
                    $apartment_complex_id = $apartment->apartmentComplex->label;
                }
                else{
                    $apartment_id = "Не определено";
                    $apartment_complex_id = "Не определено";
                }

                $constructor_request = new ConstructorRequest();
                $constructor_request->name = $model->name;
                $constructor_request->phone = $model->phone;
                $constructor_request->flat_id = $model->apartment_id;
                $constructor_request->complex_id = $apartment->apartment_complex_id;
                $constructor_request->city_id = $city->id ? $city->id : null;

                $constructor_request->apartment_id = $apartment_id;
                $constructor_request->apartment_complex_id = $apartment_complex_id;

                $constructor_request->bitrixSend();

                if ($constructor_request->save()) {

               /*     app()->trigger('bitrixData', new Event(['sender' => [
                            'title' => 'Запрос на ипотеку',
                            'source_description' => 'заявка с сайта - Запрос на ипотеку',
                            'form' => [
                                'name' => $constructor_request->name,
                                'phone' => $constructor_request->phone,
                                'city' => obtain('label', City::getUserCity()),
                                'comment' => $apartment_complex_id.", ".$apartment_id,
                            ]]])
                    );*/

                    return [
                        'js' => Html::script('showPopup();'),
                        'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                        'replaces' => [
                            [
                                'data' => $this->renderPartial('_thanks_popup', []),
                                'what' => '.popup-replace'
                            ],
                        ],
                    ];
                }
            }
            return [
                'js' => Html::script('showPopup();'),
                'replaces' => [
                    [
                        'data' => $this->renderPartial('_excursion_popup', ['model' => $model, 'apartmentId' => request()->get('apartmentId')]),
                        'what' => '.popup-replace'
                    ],
                ]
            ];
        }
    }

    public function actionSubscribeFormSubmit()
    {
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax()) {
            $model = new SubscribeForm();
            if ($model->load(request()->post()) && $model->save()) {
                return [
                    'js' => Html::script('showPopup();'),
                    'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                    'replaces' => [
                        ['data' => $this->renderPartial('_thanks_popup', []), 'what' => '.popup-replace'],
                    ],
                ];
            }

            return [
                'js' => Html::script('showPopup();'),
                'replaces' => [
                    [
                        'data' => FooterSubscribeFormWidget::widget(['subscribeModel' => $model]),
                        'what' => '.popup-replace'
                    ],
                ],
            ];
        }
    }

    public function actionSubscribeBottomFormSubmit()
    {
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax()) {
            $model = new BottomSubscribeForm();
            if ($model->load(request()->post()) && $model->save()) {
                return [
                    'js' => Html::script('showPopup();'),
                    'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                    'replaces' => [
                        [
                            'data' => $this->renderPartial('_thanks_popup', []),
                            'what' => '.popup-replace'
                        ],
                    ],
                ];
            }

            return [
                'js' => Html::script('showPopup();'),
                'replaces' => [
                    [
                        'data' => FooterSubscribeFormWidget::widget(['subscribeModel' => $model]),
                        'what' => '.popup-replace'
                    ],
                ],
            ];
        }
    }

    public function actionCustomPopupRequestSubmit()
    {
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax()) {
            $model = new CustomPopupForm();
            if ($model->load(request()->post()) && $model->save()) {
                return [
                    'js' => Html::script('showPopup();'),
                    'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                    'replaces' => [
                        [
                            'data' => $this->renderPartial('_thanks_popup', []),
                            'what' => '.popup-replace'
                        ],
                    ],
                ];
            }

            return [
                'js' => Html::script('showPopup();'),
                'replaces' => [
                    [
                        'data' => CustomPopupRequestWidget::widget(['requestModel' => $model]),
                        'what' => '.popup-replace'
                    ],
                ],
            ];
        }
    }

    public function actionFormInstalmentPlanPopup()
    {
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax()) {
            $model = new FormInstallmentPlan();
            $pageModel = InstallmentPlan::findByCityWithBenefitsAndStepsOrFail(City::getUserCity());
            if ($model->load(request()->post()) ) {
                $apartment = Apartment::find()->where(["id" => $model->apartment_id])->one();
                if(!empty($apartment)){
                    $apartment_id = $apartment->label;
                    $apartment_complex_id = $apartment->apartmentComplex->label;
                }
                else{
                    $apartment_id = "Не определено";
                    $apartment_complex_id = "Не определено";
                }
                $model->apartment_id = $apartment_id;
                $model->apartment_complex_id = $apartment_complex_id;
                $model->save();

                return [
                    'js' => Html::script('showPopup();'),
                    'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                    'replaces' => [
                        ['data' => $this->renderPartial('_thanks_popup', []), 'what' => '.popup-replace'],
                    ],
                ];
            }
            $url = $_SERVER["HTTP_REFERER"];
            $array = explode("/",$url);
            $last_item_index = count($array) - 1;
            $ap_id =  $array[$last_item_index];
            return [
                'js' => Html::script('showPopup();'),
                'replaces' => [
                    ['data' => $this->renderPartial('_instalment_plan_popup', ['model' => $model, 'pageModel' => $pageModel,'apartmentId' => $ap_id]), 'what' => '.popup-replace'],
                ],
            ];
        }
    }

    public function actionFormHypothecPopup()
    {
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax()) {
            $model = new FormHypothec();
            $pageModel = Hypothec::findByCityWithBenefitsAndStepsOrFail(City::getUserCity());
            if ($model->load(request()->post())) {

                $apartment = Apartment::find()->where(["id" => $model->apartment_id])->one();
                if(!empty($apartment)){
                    $apartment_id = $apartment->label;
                    $apartment_complex_id = $apartment->apartmentComplex->label;
                }
                else{
                    $apartment_id = "Не определено";
                    $apartment_complex_id = "Не определено";
                }
                $model->apartment_id = $apartment_id;
                $model->apartment_complex_id = $apartment_complex_id;
                $model->save();

                return [
                    'js' => Html::script('showPopup();'),
                    'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                    'replaces' => [
                        ['data' => $this->renderPartial('_thanks_popup', []), 'what' => '.popup-replace'],
                    ],
                ];
            }

            $url = $_SERVER["HTTP_REFERER"];
            $array = explode("/",$url);
            $last_item_index = count($array) - 1;
            $ap_id =  $array[$last_item_index];

            return [
                'js' => Html::script('showPopup();'),
                'replaces' => [
                    ['data' => $this->renderPartial('_hypothec_popup', ['model' => $model, 'pageModel' => $pageModel,'apartmentId' => $ap_id]), 'what' => '.popup-replace'],
                ],
            ];
        }
    }


    public function actionFormInvest()
    {
        $model = new FormInvest();
        $model->phone = request()->post('FormInvest')['phone'];
        $model->save();

        return true;
    }

    public function actionFormSumm()
    {
		response()->format = Response::FORMAT_JSON;
		if (request()->getIsAjax()) {

			$model = new RequestFullAmount();
			if ($model->load(request()->post()) ) {
                $apartment = Apartment::find()->where(["id" => $model->apartment_id])->one();
                if(!empty($apartment)){
                    $apartment_id = $apartment->label;
                    $apartment_complex_id = $apartment->apartmentComplex->label;
                }
                else{
                    $apartment_id = "Не определено";
                    $apartment_complex_id = "Не определено";
                }
                $model->apartment_id = $apartment_id;
                $model->apartment_complex_id = $apartment_complex_id;
                $model->saveAndSendEmailAndCrm();

				return [
					'code' => 1,
				];
			}
			return [
				'code' => 0,
				'error' => 1,
			];
		}
    }


    public
    function actionFormHypothecWithDoc()
    {
        $data = [];
        $model = new FormHypothecWithDoc();
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax() && $model->load(request()->post())) {
            $data = $model->ajaxValidation();
            if (empty($data)) {
                $model->save();
                $data = [
                    'js' => Html::script('showPopup(); $("#question-form").find("input").val(""); $("#question-form-agree-block").removeClass("has-error");$(".label_file").text($(".label_file").data("old-label"));'),
                    'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                    'replaces' => [
                        ['data' => $this->renderPartial('_thanks_popup'), 'what' => '.popup-replace'],
                    ],
                ];
            }
        }

        return $data;
    }

    public
    function actionFormJobWithDoc()
    {
        $data = [];
        $model = new FormJobWithDoc();
        response()->format = Response::FORMAT_JSON;
        $a = request()->post('FormJobWithDoc');
        $vac_id = $a['vacancy'] ?? null;
        //file_put_contents('/var/www/EGORVACS.txt', print_r($b, true));
        if (request()->getIsAjax() && $model->load(request()->post())) {
            $data = $model->ajaxValidation();
            if (empty($data)) {
                $model->save();

                if ($vac_id)
                {
                    \common\models\Vacancy::incrementResponse($vac_id);
                }

                $data = [
                    'js' => Html::script('showPopup(); $("#question-form").find("input").val(""); $("#question-form-agree-block").removeClass("has-error");$(".label_file").text($(".label_file").data("old-label"));'),
                    'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                    'replaces' => [
                        ['data' => $this->renderPartial('_thanks_popup'), 'what' => '.popup-replace'],
                    ],
                ];
            }
        }

        return $data;
    }

    public
    function actionFormHypothecWithDocValidate()
    {
        $data = [];
        response()->format = Response::FORMAT_JSON;
        $model = new FormHypothecWithDoc();
        if (request()->getIsAjax() && $model->load(request()->post())) {
            $data = $model->ajaxValidation();
            if (!isset($model->iAgree)) {
                $data['js'] = Html::script('$("#question-form-agree-block").addClass("has-error")');
            }
        }

        return $data;
    }

    public
    function actionFormJobWithDocValidate()
    {
        $data = [];
        response()->format = Response::FORMAT_JSON;
        $model = new FormJobWithDoc();
        if (request()->getIsAjax() && $model->load(request()->post())) {
            $data = $model->ajaxValidation();
            if (!isset($model->iAgree)) {
                $data['js'] = Html::script('$("#question-form-agree-block").addClass("has-error")');
            }
        }

        return $data;
    }

    public
    function actionFormCustomGuarantee()
    {
        $data = [];
        $model = new FormCustomGuarantee();
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax() && $model->load(request()->post())) {
            $data = $model->ajaxValidation();
            if (empty($data)) {
                $model->save();
                $data = [
                    'js' => Html::script('showPopup(); $("#question-form")[0].reset(); $("#question-form").find(".form__field").removeClass("form__field_has_content"); $("#question-form-agree-block").removeClass("has-error");$(".label_file").text($(".label_file").data("old-label"));'),
                    'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                    'replaces' => [
                        ['data' => $this->renderPartial('_thanks_popup'), 'what' => '.popup-replace'],
                    ],
                ];
            }
        }

        return $data;
    }

    public
    function actionFormCustomGuaranteeValidate()
    {
        $data = [];
        response()->format = Response::FORMAT_JSON;
        $model = new FormCustomGuarantee();
        if (request()->getIsAjax() && $model->load(request()->post())) {
            $data = $model->ajaxValidation();
        }

        return $data;
    }

    public
    function actionFormCooperation()
    {
        $data = [];
        $model = new FormCooperation();
        response()->format = Response::FORMAT_JSON;
        if (request()->getIsAjax() && $model->load(request()->post())) {
            $data = $model->ajaxValidation();
            if (empty($data)) {
                $model->save();
                $data = [
                    'js' => Html::script('showPopup(); $("#question-form").find("input").val(""); $("#question-form-agree-block").removeClass("has-error")'),
                    'onsubmit' => Html::script(request()->post('onsubmit') ?? false),
                    'replaces' => [
                        ['data' => $this->renderPartial('_thanks_popup'), 'what' => '.popup-replace'],
                    ],
                ];
            }
        }

        return $data;
    }

    public
    function actionFormCooperationValidate()
    {
        $data = [];
        response()->format = Response::FORMAT_JSON;
        $model = new FormCooperation();
        if (request()->getIsAjax() && $model->load(request()->post())) {
            $data = $model->ajaxValidation();
            if (!isset($model->iAgree)) {
                $data['js'] = Html::script('$("#question-form-agree-block").addClass("has-error")');
            }
        }

        return $data;
    }

    public
    function actionPublicDataDownloadFile($id)
    {
        $data = [];
        response()->format = Response::FORMAT_JSON;
        $model = new FormPublicDataFile(['fileId' => $id]);
        if (request()->getIsAjax() && $model->load(request()->post())) {
            $data = $model->ajaxValidation();
            if (empty($data)) {
                return [
                    'content' => [
                        ['data' => '', 'what' => ".public-data-file-$id p.help-block"],
                    ],
                    'redirect' => $model->getFile()->getFileSrc(),
                ];
            }
        }

        return [
            'content' => [
                ['data' => obtain('filePass', $data, ''), 'what' => ".public-data-file-$id p.help-block"],
            ]
        ];
    }

    public function actionFormAscAboutDiscount() {
		response()->format = Response::FORMAT_JSON;
		$returnData = [];

		if (request()->getIsAjax()) {
			$model = new FormAscAboutDiscount();

			// если был пост на этой форме
			if($model->load(request()->post())) {
				// аякс валидация
				$data = $model->ajaxValidation();
				// проверка валидации
				if(count($data) == 0 && $model->validate()) {

				    //Запросы экскурсии стали Запросы цен. Поэтому запросы цен добавляются в экскурсии request_excursion

                    $city = City::getUserCity();

                  /*  $url = $_SERVER["HTTP_REFERER"];
                    $array = explode("/",$url);
                    $last_item_index = count($array) - 1;
                    $ap_id =  $array[$last_item_index];

                    echo $ap_id;
                    die;*/


$apartment = Apartment::find()->where(["id" => $model->apartment_id])->one();
if(!empty($apartment)){
    $apartment_id = $apartment->label;
    $apartment_complex_id = $apartment->apartmentComplex->label;
}
else{
    $apartment_id = "Не определено";
    $apartment_complex_id = "Не определено";
}
                $request_excursion = new RequestExcursion();
                    $request_excursion->name = $model->username;
                    $request_excursion->phone = $model->phone;
                    $request_excursion->apartment_id = $model->apartment_id;
                    $request_excursion->city_id = $city->id ? $city->id : null;

                    if(!empty($city)) {
                        if ($city->label == "Хабаровск" || $city->label == "Уфа" || $city->label == "Пермь" || $city->label == "Тюмень") {
                            $request_excursion->request_type=2;
                        }
                        else{
                            $request_excursion->request_type=1;
                        }
                    }

                    $request_excursion->save();

                    $model->apartment_id = $apartment_id;
                    $model->apartment_complex_id = $apartment_complex_id;

					if($model->bitrixSend()) {
						$data = [
							'js' => Html::script('showPopup();'),
							'onsubmit' => Html::script(request()->post('onsubmit')),
							'replaces' => [
								[
									'data' => $this->renderPartial('_thanks_popup', []),
									'what' => '.popup-replace'
								],
							],
						];
						return $data;
					}
				} else {
					return $data;
				}
			}
            $url = $_SERVER["HTTP_REFERER"];
            $array = explode("/",$url);
            $last_item_index = count($array) - 1;
            $ap_id =  $array[$last_item_index];
			return [
				'js' => Html::script('showPopup();'),
				'replaces' => [
					[
						'data' => $this->renderPartial('_asc_about_discount', [
							'model' => $model, 'apartmentId' => $ap_id
						]),
						'what' => '.popup-replace'
					],
				],
			];
		}
	}
}
