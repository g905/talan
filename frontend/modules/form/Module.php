<?php

namespace frontend\modules\form;


class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\form\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
