<?php

namespace frontend\modules\form\models;

use yii\base\Event;
use yii\base\Model;
use yii\helpers\Html;
use common\models\City;
use common\models\Manager;
use common\helpers\MailerHelper;
use common\models\RequestComplexPresentation;

/**
 * This is the model class for table "request".
 *
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $acId
 * @property string $abId
 */
class FormComplexPresentation extends Model
{
    public $name;
    public $phone;
    public $email;
    public $acId;
    public $abId;
    public $managerId;
    public $iAgree;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['acId', 'abId', 'managerId'], 'integer'],
            [['email'], 'email'],
            [['name', 'phone', 'email', 'iAgree'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => t('_Your name', 'form-ask-question'),
            'phone' => t('_Phone', 'form-ask-question'),
            'email' => t('Email', 'form-ask-question'),
            'iAgree' => t('_I agree to the processing of personal data', 'form-ask-question'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {
        if ($this->validate()) {
            $newRequest = new RequestComplexPresentation();
            $newRequest->city_id = obtain('id', City::getUserCity());
            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->email = $this->email;
            $newRequest->ac_id = $this->acId;
            $newRequest->ab_id = $this->abId;
            $saveResult = $newRequest->save(false);

            if ($saveResult) {
                $notifyEmails = [];
                $manager = Manager::findOne($this->managerId);
                if ($manager) {
                    $notifyEmails = $manager->getNotificationEmailsList();
                }

                app()->trigger(
                    'bitrixData',
                    new Event([
                        'sender' => [
                            'title' => 'Запрос на полную презентацию комплекса',
                            'source_description' => 'заявка с сайта - Запрос на полную презентацию комплекса',
                            'form' => [
                                'name' => $newRequest->name,
                                'phone' => $newRequest->phone,
                                'emails' => $newRequest->email,
                                'city' => obtain('label', City::getUserCity())
                            ]
                        ]
                    ])
                );

                MailerHelper::sendNotificationEmail(bt('complex presentation request', 'menu'), [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                    'Email' => $newRequest->email,
                    'Квартирный комплекс' => $newRequest->apartmentComplex->label ?? 'Не определено',
                ], $notifyEmails);
            }

            return $saveResult;
        }

        return null;
    }
}
