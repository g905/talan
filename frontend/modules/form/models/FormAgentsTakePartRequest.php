<?php

namespace frontend\modules\form\models;

use common\models\RequestAgentsTakePart;
use yii\base\Event;
use yii\base\Model;
use yii\helpers\Html;
use common\models\City;
use common\helpers\MailerHelper;

/**
 * This is the model class for table "request".
 *
 * @property string $name
 * @property string $phone
 */
class FormAgentsTakePartRequest extends Model
{
    public $name;
    public $phone;
    public $email;
    public $agencyName;
    public $city_id;

    public function rules()
    {
        return [
            [['name', 'email', 'agencyName'], 'required'],
            [['name', 'phone', 'email', 'agencyName'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => t('_Your name', 'form-ask-question'),
            'phone' => t('_Phone', 'form-ask-question'),
            'email' => t('_Email', 'form-ask-question'),
            'agencyName' => t('_AgencyName', 'form-ask-question'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {
        if ($this->validate()) {
            $newRequest = new RequestAgentsTakePart();
            $newRequest->city_id = obtain('id', City::getUserCity());
            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->email = $this->email;
            $newRequest->agency_name = $this->agencyName;
            $saveResult = $newRequest->save(false);

          if ($saveResult) {
                MailerHelper::sendNotificationEmail('Новый агент', [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                    'email' => $newRequest->email,
                    'Агентство' => $newRequest->agency_name,
                    
                ]);
                //MailerHelper::sendClientEmail('test', $this->email, 'test', true, true);
            }
            app()->trigger('bitrixData', new Event(['sender' => ['title' => 'Новый агент', 'source_description' => 'Запрос на участие со страницы агентов', 'form' => $newRequest->toArray()]]));


            return $saveResult;
        }

        return null;
    }
}
