<?php

namespace frontend\modules\form\models;

use yii\base\Event;
use yii\base\Model;
use yii\helpers\Html;
use common\models\City;
use common\helpers\MailerHelper;
use common\models\RequestCallback;

/**
 * This is the model class for table "request".
 *
 * @property string $name
 * @property string $phone
 */
class FormCallback extends Model
{
    public $name;
    public $phone;
    public $acId;
    public $user_city;

    public function rules()
    {
        return [
            [['acId'], 'integer'],
            [['name', 'phone'], 'required'],
            [['name', 'phone', 'user_city'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => t('_Your name', 'form-ask-question'),
            'phone' => t('_Phone', 'form-ask-question'),
            'user_city' => t('_City', 'form-ask-question'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {
        if ($this->validate()) {
            $newRequest = new RequestCallback();
            $newRequest->city_id = obtain('id', City::getUserCity());

            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->ac_id = $this->acId;
            $saveResult = $newRequest->save(false); 

            if ($saveResult) {
                MailerHelper::sendNotificationEmail(bt('callback request', 'menu'), [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                    //'Город' => $newRequest->user_city
                ]);
            }
            $newRequest = $newRequest->toArray();
            if ($this->user_city)
            {
                $newRequest['user_city'] = $this->user_city;
            } else {
                $newRequest['user_city'] = City::getUserCity()->label;
            }


            app()->trigger('bitrixData', new Event(['sender' => ['title' => 'Обратный звонок', 'source_description' => 'заявка с сайта - Обратный звонок', 'form' => $newRequest]]));
            return $saveResult;
        }

        return null;
    }
}
