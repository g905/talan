<?php

namespace frontend\modules\form\models;

use common\models\RequestGuarantee;
use yii\base\Event;
use yii\base\Model;
use Yii;
use yii\helpers\Html;
use common\models\City;
use common\models\Manager;
use common\helpers\MailerHelper;
use common\models\RequestComplexPresentation;

/**
 * This is the model class for table "{{%request_guarantee}}".
 *
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $building_id
 * @property integer $problem
 * @property integer $problem_type
 */
class FormGuarantee extends Model
{
    public $name;
    public $phone;
    public $email;
    public $problem;
    public $problem_type;
    public $building_id;
    public $iAgree;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['building_id', 'problem_type'], 'integer'],
            [['email'], 'email'],
            [['name', 'phone', 'email', 'problem', 'iAgree'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => t('_Your name', 'form-ask-question'),
            'phone' => t('_Phone', 'form-ask-question'),
            'email' => t('Email', 'form-ask-question'),
            'problem' => t('Problem', 'form-ask-question'),
            'problem_type' => t('Problem type', 'form-ask-question'),
            'iAgree' => t('_I agree to the processing of personal data', 'form-ask-question'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {
        if ($this->validate()) {
            $newRequest = new RequestGuarantee();
            $newRequest->city_id = obtain('id', City::getUserCity());
            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->email = $this->email;
            $newRequest->problem = $this->problem;
            $newRequest->problem_type = $this->problem_type;
            $newRequest->building_id = $this->building_id;
            $saveResult = $newRequest->save(false);

            if ($saveResult) {
                $notifyEmails = [];

                app()->trigger(
                    'bitrixData',
                    new Event([
                        'sender' => [
                            'title' => 'Запрос с проблемой',
                            'source_description' => 'заявка с сайта - Запрос с проблемой',
                            'form' => [
                                'name' => $newRequest->name,
                                'phone' => $newRequest->phone,
                                'emails' => $newRequest->email,
                                'comment' => $newRequest->problem,
                                'city' => obtain('label', City::getUserCity())
                            ]
                        ]
                    ])
                );

                MailerHelper::sendNotificationEmail(bt('Guarantee request', 'menu'), [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                    'Email' => $newRequest->email,
                    'Здание' => $newRequest->apartmentBuilding->label ?? 'Не определено',
                    'Тип проблемы' => $newRequest->problem_type,
                    'Проблема' => $newRequest->problem
                ], $notifyEmails);
            }
            return $saveResult;


        }

        return null;
    }
}
