<?php

namespace frontend\modules\form\models;

use yii\base\Model;
use common\models\FpmFile;

/**
 * Class FormPublicDataFile
 *
 * @package frontend\modules\form\models
 */
class FormPublicDataFile extends Model
{
    public $fileId;
    public $filePass;

    private $file;

    public function rules()
    {
        return [
            [['fileId', 'filePass'], 'required'],
            [['fileId'], 'integer'],
            [
                ['filePass'],
                'compare',
                'operator' => '===',
                'compareValue' => $this->getRealPass(),
                'message' => 'Вы ввели неправильный пароль к файлу.'
            ],
        ];
    }

    public function getFile()
    {
        if ($this->file === null) {
            $this->file = FpmFile::find()->where(['id' => $this->fileId])->with(['metaData'])->one();
        }

        return $this->file;
    }

    private function getRealPass()
    {
        return obtain(['metaData', 'alt'], $this->getFile(), '');
    }

    public function attributeLabels()
    {
        return [
            'fileId' => '',
            'filePass' => 'Пароль',
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getFirstErrors() as $attribute => $errors) {
            $result[$attribute] = $errors;
        }

        return $result;
    }
}
