<?php

namespace frontend\modules\form\models;

use common\helpers\SiteUrlHelper;
use yii\base\Event;
use yii\base\Model;
use common\models\City;
use common\helpers\MailerHelper;
use common\models\SubscribeRequest;

class SubscribeForm extends Model
{
    public $email;
    public $agree;
    public $acId;

    /**
     * @var array
     */
    public $attributeLabelsData = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['acId'], 'integer'],
            [['email'], 'required'],
            [['agree'], 'boolean'],
            [['agree'], 'required', 'requiredValue' => 1],
            [['email'], 'email'],
            [['email'], 'string', 'max' => 255],
//            [['email'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
        ];
    }

    public function attributeLabels()
    {
        $city = City::getUserCity();
        return [
            'email' => t('Email', 'form-subscribe'),
            'agree' => empty($this->attributeLabelsData['agree'])
                ?'Даю
                    <a href="' . SiteUrlHelper::getAgreementUrl() . '">согласие</a> на обработку моих персональных данных, с условиями
                    <a href="' . SiteUrlHelper::getPrivatePolicyUrl() . '">Политики</a> ознакомлен'
                : $this->attributeLabelsData['agree'],
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }
        $model = new SubscribeRequest();
        $model->email = $this->email;
        $model->city_id = obtain('id', City::getUserCity());
        $model->ac_id = $this->acId;

        $saved = $model->save(false);
        if ($saved) {
            app()->trigger('bitrixData', new Event(['sender' => ['title' => 'Форма подписки', 'source_description' => 'заявка с сайта - Форма подписки', 'form' => ['emails' =>  $model->email, 'city' => obtain('label', City::getUserCity())]]]));
            MailerHelper::sendNotificationEmail(bt('subscribe request', 'menu'), ['Email' => $model->email]);
        }

        return $saved;
    }
}
