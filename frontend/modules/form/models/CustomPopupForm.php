<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 23.04.18
 * Time: 14:22
 */

namespace frontend\modules\form\models;

use common\helpers\MailerHelper;
use common\helpers\SiteUrlHelper;
use common\models\City;
use common\models\RequestCustom;
use yii\base\Event;
use yii\base\Model;
use Yii;

class CustomPopupForm extends Model
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $phone;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $popupId;
    /**
     * @var string
     */
    public $agree;
    public $acId;

    /**
     * @var array
     */
    public $attributeLabelsData = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['acId'], 'integer'],
            [['name'], 'required'],
            [['phone'], 'required'],
            [['email'], 'required'],

            [['name'], 'string', 'max' => 200],
            [['phone'], 'string', 'max' => 200],
            [['email'], 'string', 'max' => 200],

            [['email'], 'email'],

            [['agree'], 'boolean'],
            [['agree'], 'required', 'requiredValue' => 1],

            [['name', 'email'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],

            [['popupId'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $city = City::getUserCity();
        return [
            'email' => !empty($this->attributeLabelsData['email'])
                ? $this->attributeLabelsData['email']
                : Yii::t('front/form-subscribe', 'Email'),
            'phone' => !empty($this->attributeLabelsData['phone'])
                ? $this->attributeLabelsData['phone']
                : Yii::t('front/form-subscribe', 'Phone'),
            'name' => !empty($this->attributeLabelsData['name'])
                ? $this->attributeLabelsData['name']
                : Yii::t('front/form-subscribe', 'Name'),
            'agree' => !empty($this->attributeLabelsData['agree'])
                ? 'Даю
                    <a href="' . SiteUrlHelper::getAgreementUrl() .'">согласие</a> на обработку моих персональных данных, с условиями
                    <a href="' . SiteUrlHelper::getPrivatePolicyUrl() . '">Политики</a> ознакомлен'
                : strip_tags($this->attributeLabelsData['agree'], '<a>'),
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $model = new RequestCustom();
        $model->email = $this->email;
        $model->name = $this->name;
        $model->phone = $this->phone;
        $model->custom_popup_id = $this->popupId;
        $model->ac_id = $this->acId;
        $model->city_id = obtain('id', City::getUserCity());
        $saved = $model->save(false);

        if ($saved) {
            app()->trigger(
                'bitrixData',
                new Event([
                    'sender' => [
                        'title' => 'Всплывающее окно',
                        'source_description' => 'заявка с сайта - Всплывающее окно',
                        'form' => [
                            'name' => $model->name,
                            'phone' => $model->phone,
                            'emails' => $model->email,
                            'city' => obtain('label', City::getUserCity())
                        ]
                    ]
                ])
            );
            MailerHelper::sendNotificationEmail(bt('custom_popup_request', 'menu'), [
                'Name' => $model->name,
                'Phone' => $model->phone,
                'Email' => $model->email
            ]);
        }

        return $saved;
    }
}
