<?php

namespace frontend\modules\form\models;

use yii\base\Event;
use yii\base\Model;
use yii\helpers\Html;
use common\models\City;
use common\helpers\MailerHelper;
use common\models\RequestCooperation;

/**
 * This is the model class for table "request".
 */
class FormCooperation extends Model
{
    public $name;
    public $phone;
    public $email;
    public $company_name;
    public $chief_phone;

    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'company_name', 'chief_phone'], 'required'],
            [['name', 'phone', 'company_name', 'chief_phone'], 'string', 'max' => 255],
            [['email'], 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => t('Name', 'form-cooperation'),
            'phone' => t('Phone', 'form-cooperation'),
            'email' => t('Email', 'form-cooperation'),
            'company_name' => t('Company name', 'form-cooperation'),
            'chief_phone' => t('Chief phone', 'form-cooperation'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {
        if ($this->validate()) {
            $newRequest = new RequestCooperation();
            $newRequest->city_id = obtain('id', City::getUserCity());
            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->email = $this->email;
            $newRequest->company_name = $this->company_name;
            $newRequest->chief_phone = $this->chief_phone;
            $saveResult = $newRequest->save(false);

            if ($saveResult) {
                $data = [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                    'Почта' => $newRequest->email,
                    'Телефон руководителя' => $newRequest->chief_phone,
                    'Название компании' => $newRequest->company_name,
                ];

                app()->trigger('bitrixData', new Event(['sender' => [
                    'title' => 'Запрос на сотрудничество',
                    'source_description' => 'заявка с сайта - Запрос на сотрудничество',
                    'form' => [
                        'name' => $newRequest->name,
                        'phone' => $newRequest->phone,
                        'email' => $newRequest->email,
                        'city' => obtain('label', City::getUserCity())
                    ]]])
                );

                MailerHelper::sendNotificationEmail(bt('Cooperation request', 'menu'), $data);
            }

            return $saveResult;
        }

        return null;
    }
}
