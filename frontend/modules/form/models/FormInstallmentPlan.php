<?php

namespace frontend\modules\form\models;

use common\models\InstallmentPlan;
use yii\base\Event;
use yii\base\Model;
use yii\helpers\Html;
use common\models\City;
use common\helpers\MailerHelper;
use common\models\RequestInstallmentPlan;

/**
 * This is the model class for table "request".
 *
 * @property string $name
 * @property string $phone
 */
class FormInstallmentPlan extends Model
{
    public $name;
    public $phone;
    public $apartment_id;
    public $apartment_complex_id;

    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['apartment_id', 'apartment_complex_id'], 'safe'],
            [['name', 'phone'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => t('_Your name', 'form-instalment-plan'),
            'phone' => t('_Phone', 'form-instalment-plan'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {
        if ($this->validate()) {
            $newRequest = new RequestInstallmentPlan();
            $newRequest->city_id = obtain('id', City::getUserCity());
            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $saveResult = $newRequest->save(false);

            if ($saveResult) {
                $data = [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                ];

                app()->trigger('bitrixData', new Event(['sender' => [
                    'title' => 'Запрос на рассрочку',
                    'source_description' => 'заявка с сайта - Запрос на рассрочку',
                    'form' => [
                        'name' => $newRequest->name,
                        'phone' => $newRequest->phone,
                        'city' => obtain('label', City::getUserCity()),
                        'comment' => $this->apartment_complex_id.", ".$this->apartment_id,
                    ]]])
                );

                $pageModel = InstallmentPlan::findByCityWithBenefitsAndStepsOrFail(City::getUserCity());

                $email = [];

                if ($pageModel) {
                    if (!empty($pageModel->notification_email)) {
                        $email = explode("\n", $pageModel->notification_email);
                    }
                }

                MailerHelper::sendNotificationEmail(bt('Instalment plan request', 'menu'), $data,  $email);
            }

            return $saveResult;
        }

        return null;
    }
}
