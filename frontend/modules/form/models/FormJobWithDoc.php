<?php

namespace frontend\modules\form\models;

use yii\base\Event;
use yii\base\Model;
use yii\helpers\Html;
use yii\web\UploadedFile;
use metalguardian\fileProcessor\helpers\FPM;
use common\models\City;
use common\models\RequestJob;
use common\helpers\MailerHelper;

/**
 * This is the model class for table "request_job".
 */
class FormJobWithDoc extends Model
{
    public $name;
    public $phone;
    public $email;
    public $file;
    public $vacancy;

    public function rules()
    {
        return [
            [['name', 'phone', 'email'], 'required'],
            [['email'], 'email'],
            [['name', 'phone'], 'string', 'max' => 255],
            [['file'], 'file'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => t('_Your name', 'form-job-with-doc'),
            'phone' => t('_Phone', 'form-job-with-doc'),
            'email' => t('_Email', 'form-job-with-doc'),
            'file' => t('_File', 'form-job-with-doc'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {
        if ($this->validate()) {
            $newRequest = new RequestJob();
            $newRequest->city_id = obtain('id', City::getUserCity());
            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->email = $this->email;
            $newRequest->file_id = $this->upload();
            $saveResult = $newRequest->save(false);

            if ($saveResult) {
                $data = [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                    'Почта' => $newRequest->email,
                ];
                $form = [
                    'name' => $newRequest->name,
                    'phone' => $newRequest->phone,
                    'emails' => $newRequest->email,
                    'city' => obtain('label', City::getUserCity())
                ];
                if ($newRequest->file_id !== null) {
                    $data['CV'] = request()->getHostInfo() . FPM::originalSrc($newRequest->file_id);
                    $form['comment'] = request()->getHostInfo() . FPM::originalSrc($newRequest->file_id);
                }

                app()->trigger('bitrixData', new Event(['sender' => [
                    'title' => 'Запрос на работу',
                    'source_description' => 'заявка с сайта - Запрос на работу',
                    'form' => $form
                ]]));

                MailerHelper::sendNotificationEmail(bt('Job request', 'menu'), $data);
            }

            return $saveResult;
        }

        return null;
    }

    public function upload()
    {
        if ($this->validate(['file'])) {
            $file = UploadedFile::getInstance($this, 'file');
            if ($file) {
                return FPM::transfer()->saveUploadedFile($file);
            }
            return null;
        } else {
            return null;
        }
    }
}
