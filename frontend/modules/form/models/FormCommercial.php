<?php

namespace frontend\modules\form\models;

use yii\base\Event;
use yii\base\Model;
use yii\helpers\Html;
use common\models\City;
use common\helpers\MailerHelper;
use common\models\RequestCommercial;

/**
 * This is the model class for table "request".
 *
 * @property string $name
 * @property string $phone
 */
class FormCommercial extends Model
{
    public $name;
    public $phone;
    public $acId;

    public function rules()
    {
        return [
            [['acId'], 'integer'],
            [['name', 'phone'], 'required'],
            [['name', 'phone'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => t('_Your name', 'form-ask-question'),
            'phone' => t('_Phone', 'form-ask-question'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {
        if ($this->validate()) {
            $newRequest = new RequestCommercial();
            $newRequest->city_id = obtain('id', City::getUserCity());
            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->ac_id = $this->acId;
            $saveResult = $newRequest->save(false);

            if ($saveResult) {
                app()->trigger(
                    'bitrixData',
                    new Event([
                        'sender' => [
                            'title' => 'Запрос на коммерческую недвижимость',
                            'source_description' => 'заявка с сайта - Запрос на коммерческую недвижимость',
                            'form' => [
                                'name' => $newRequest->name,
                                'phone' => $newRequest->phone,
                                'city' => obtain('label', City::getUserCity())
                            ]
                        ]
                    ])
                );
                MailerHelper::sendNotificationEmail(bt('callback request', 'menu'), [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                ]);
            }

            return $saveResult;
        }

        return null;
    }
}
