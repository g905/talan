<?php

namespace frontend\modules\form\models;

use common\models\City;
use common\models\Earth;
use common\models\PartnerRequest;
use yii\base\Event;
use yii\base\Model;
use yii\helpers\Html;
use common\helpers\MailerHelper;

class FormPartner extends Model
{
    public $name;
    public $phone;
    public $city;

    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['name', 'phone', 'city'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => t('_Your name', 'form-ask-question'),
            'phone' => t('_Phone', 'form-ask-question'),
            'city' => t('_City', 'form-ask-question'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {
        if ($this->validate()) {
            $newRequest = new PartnerRequest();
            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->city = $this->city;
            $saveResult = $newRequest->save(false);

            if ($saveResult) {

                $pageModel = Earth::findByCity(City::getUserCity());

                $email = [];

                if ($pageModel) {
                    if (!empty($pageModel->notification_email)) {
                        $email = explode("\n", $pageModel->notification_email);
                    }
                }

                MailerHelper::sendNotificationEmail(bt('partner request', 'menu'), [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                ], $email);
            }
            app()->trigger('bitrixData', new Event(['sender' => ['title' => 'Партнер', 'source_description' => 'заявка с сайта - Партнер', 'form' => $newRequest->toArray()]]));

            return $saveResult;
        }

        return null;
    }
}
