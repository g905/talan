<?php

namespace frontend\modules\form\models;

use yii\base\Event;
use yii\base\Model;
use yii\helpers\Html;
use common\models\City;
use common\models\Manager;
use common\helpers\MailerHelper;
use common\models\RequestQuestion;

/**
 * This is the model class for table "request".
 *
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $question
 */
class FormAskQuestion extends Model
{
    public $name;
    public $phone;
    public $email;
    public $question;
    public $iAgree;
    public $acId;
    public $abId;
    public $managerId;
    public $apartment_id;
    public $apartment_complex_id;

    public function rules()
    {
        return [
            [['acId', 'abId', 'managerId'], 'integer'],
            [['name'], 'match', 'pattern' => '/^([а-яa-z\s]){1,40}$/iu'],
            [['name', 'phone'], 'required'],
            [['apartment_id', 'apartment_complex_id'], 'safe'],
            [['email'], 'email'],
            [['name', 'phone', 'email', 'question', 'iAgree', 'phone'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => t('_Your name', 'form-ask-question'),
            'phone' => t('_Phone', 'form-ask-question'),
            'email' => t('Email', 'form-ask-question'),
            'question' => t('_Your question', 'form-ask-question'),
            'iAgree' => t('_I agree to the processing of personal data', 'form-ask-question'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {

        if ($this->validate()) {
            $newRequest = new RequestQuestion();
            $newRequest->city_id = obtain('id', City::getUserCity());
            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->email = $this->email;
            $newRequest->question = $this->question;
            $newRequest->ac_id = $this->acId;
            $newRequest->ab_id = $this->abId;
            $saveResult = $newRequest->save(false);

            if ($saveResult) {
                $notifyEmails = [];
                $manager = Manager::findOne($this->managerId);
                if ($manager) {
                    $notifyEmails = $manager->getNotificationEmailsList();
                } 
                app()->trigger(
                    'bitrixData',
                    new Event([
                        'sender' => [
                            'title' => 'Задать вопрос',
                            'source_description' => 'заявка с сайта - Задать вопрос',
                            'form' => [
                                'name' => $newRequest->name,
                                'phone' => $newRequest->phone,
                                'emails' => $newRequest->email,
                                'comment' => $this->apartment_complex_id.", ".$this->apartment_id." , ".$newRequest->question,
                                'city' => obtain('label', City::getUserCity())
                            ]
                        ]
                    ])
                );

                MailerHelper::sendNotificationEmail(bt('question request', 'menu'), [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                    'Email' => $newRequest->email,
                    'Вопрос' => $newRequest->question
                ], $notifyEmails);
            }

            return $saveResult;
        }

        return null;
    }
}
