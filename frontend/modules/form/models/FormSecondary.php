<?php

namespace frontend\modules\form\models;

use common\models\RequestSecondary;
use yii\base\Event;
use yii\base\Model;
use Yii;
use yii\helpers\Html;
use common\models\City;
use common\helpers\MailerHelper;

/**
 * This is the model class for table "{{%request_secondary}}".
 *
 * @property string $name
 * @property string $phone
 * @property string $square
 * @property string $address
 * @property integer $age
 * @property integer $redevelopment
 */
class FormSecondary extends Model
{
    public $name;
    public $phone;
    public $square;
    public $address;
    public $age;
    public $redevelopment;
    public $iAgree;
    public $apartment_id;
    public $apartment_complex_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['age', 'redevelopment'], 'integer'],
            [['apartment_id', 'apartment_complex_id'], 'safe'],
            [['name', 'phone', 'address', 'square', 'iAgree'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => t('_Your name', 'form-ask-question'),
            'phone' => t('_Phone', 'form-ask-question'),
            'square' => t('Square', 'form-ask-question'),
            'address' => t('Address', 'form-ask-question'),
            'age' => t('Age', 'form-ask-question'),
            'redevelopment' => t('Redevelopment', 'form-ask-question'),
            'iAgree' => t('_I agree to the processing of personal data', 'form-ask-question'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {
        if ($this->validate()) {
            $newRequest = new RequestSecondary();
            $newRequest->city_id = obtain('id', City::getUserCity());
            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->square = $this->square;
            $newRequest->address = $this->address;
            $newRequest->age = $this->age;
            $newRequest->redevelopment = $this->redevelopment ? 1 : 0;
            $saveResult = $newRequest->save(false);

            if ($saveResult) {
                $notifyEmails = [];

                MailerHelper::sendNotificationEmail(bt('Secondary request', 'menu'), [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                    'Площадь квартиры' => $newRequest->square,
                    'Район' => $newRequest->address,
                    'Количество лет в собственности' => $newRequest->age,
                    'Наличие неузаконенной планировки' => $newRequest->redevelopment ? 'Присутствует' : 'Отсутствует'
                ], $notifyEmails);

                app()->trigger(
                    'bitrixData',
                    new Event([
                        'sender' => [
                            'title' => 'Оплата вторичным жильем',
                            'source_description' => 'заявка с сайта - Оплата вторичным жильем',
                            'form' => [
                                'name' => $newRequest->name,
                                'phone' => $newRequest->phone,
                                'city' => obtain('label', City::getUserCity()),
                                'comment' => $this->apartment_complex_id.", ".$this->apartment_id,
                            ]
                        ]
                    ])
                );
            }

            return $saveResult;
        }

        return null;
    }
}
