<?php

namespace frontend\modules\form\models;

use common\models\ApartmentComplex;
use common\models\City;
use common\models\GuaranteePage;
use Yii;
use yii\base\Event;
use yii\base\Model;
use inquid\pdf\FPDF;
use yii\helpers\Html;
use yii\web\UploadedFile;
use common\helpers\MailerHelper;
use common\models\RequestCustomGuarantee;
use metalguardian\fileProcessor\helpers\FPM;


/**
 * Class FormCustomGuarantee
 * @package frontend\modules\form\models
 */
class FormCustomGuarantee extends Model
{
    public $modelId;
    public $name;
    public $phone;
    public $email;
    public $file;
    public $city;
    public $complex;
    public $building_number;
    public $apartment_number;
    public $comment;
    public $iAgree;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'complex', 'building_number', 'apartment_number', 'comment'], 'required'],
            [['email'], 'email'],
            [['modelId'], 'integer'],
            [['name'], 'string', 'max' => 60],
            [['apartment_number'], 'string', 'max' => 4],
            [['comment', 'building_number'], 'string', 'max' => 500],
            [['file'], 'file'],
            [['city', 'iAgree'], 'safe']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => t('_Your name', 'form-custom-guarantee'),
            'phone' => t('_Phone', 'form-custom-guarantee'),
            'email' => t('_Email', 'form-custom-guarantee'),
            'city' => t('_City', 'form-custom-guarantee'),
            'complex' => t('_Complex', 'form-custom-guarantee'),
            'building_number' => t('_Building', 'form-custom-guarantee'),
            'apartment_number' => t('_Apartment', 'form-custom-guarantee'),
            'comment' => t('_Comment', 'form-custom-guarantee'),
            'file' => t('_File', 'form-custom-guarantee'),
        ];
    }

    /**
     * @return array
     */
    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }


    public function save()
    {
        if ($this->validate()) {
            $newRequest = new RequestCustomGuarantee();
            $newRequest->city = $this->city;
            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->email = $this->email;
            $newRequest->complex = $this->complex;
            $newRequest->building_number = $this->building_number;
            $newRequest->apartment_number = $this->apartment_number;
            $newRequest->comment = $this->comment;
            $newRequest->pdf_id = 'ГС-'.rand(1,1000000).$this->city.$this->complex;
            $newRequest->file_id = $this->upload();
            $saveResult = $newRequest->save(false);

            if ($saveResult) {
                $data = [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                    'Почта' => $newRequest->email,
                    'Город' => $newRequest->city,
                    'Комплекс' => $newRequest->complex,
                    'Номер дома' => $newRequest->building_number,
                    'Номер квартиры' => $newRequest->apartment_number,
                    'Комментарий' => $newRequest->comment,
                ];
                $form = [
                    'name' => $newRequest->name,
                    'phone' => $newRequest->phone,
                    'emails' => $newRequest->email,
                    'city' => obtain('label', City::getUserCity())
                ];
                if ($newRequest->file_id !== null) {
                    $data['CV'] = request()->getHostInfo() . FPM::originalSrc($newRequest->file_id);
                    $form['comment'] = request()->getHostInfo() . FPM::originalSrc($newRequest->file_id);
                }

                app()->trigger('bitrixData', new Event(['sender' => [
                    'title' => 'Запрос на гарантию',
                    'source_description' => 'заявка с сайта - Запрос на гарантию',
                    'form' => $form
                ]]));

                $this->savePDF($newRequest);

                $pageModel = GuaranteePage::getPageById($this->modelId);

                if ($pageModel){
                    $emails = [];
                    if ($pageModel->emails) {
                        $emails = explode("\n", $pageModel->emails);
                    }
                }

               // MailerHelper::sendClientEmail(bt('Guarantee request', 'mail'), $newRequest->email, $data, false, $newRequest->pdf_id);

                MailerHelper::sendNotificationEmailClient(bt('Guarantee request', 'mail'), $data, $newRequest->email, true, false, $newRequest->pdf_id);
                MailerHelper::sendNotificationEmail(bt('Guarantee request', 'menu'), $data, $emails, true, false, $newRequest->pdf_id);
            }

            return $saveResult;
        }

        return null;
    }

    /**
     * @return int|null
     * @throws \yii\base\InvalidConfigException
     */
    public function upload()
    {
        if ($this->validate(['file'])) {
            $file = UploadedFile::getInstance($this, 'file');
            if ($file) {
                return FPM::transfer()->saveUploadedFile($file);
            }
            return null;
        } else {
            return null;
        }
    }

    public function savePDF($newRequest)
    {
        if (!file_exists(getAlias('@webroot').'/uploads/pdf')) {
            mkdir(getAlias('@webroot').'/uploads/pdf', 0777, true);
        }

        $complex = ApartmentComplex::getComplexById($newRequest->complex);
        $city = City::getCityById($newRequest->city);

        $pdf = new FPDF('P', 'mm', 'Letter');
        $pdf->AddPage();
        $pdf->AddFont('arialcyr','','arialcyr.php');
        $pdf->SetFont('arialcyr','',12);
        $pdf->Ln(20);
        $pdf->Cell(100,0, '', 0, 0, 'L');
        $pdf->Cell(0,0, self::getCyrillic('Директору ООО "Специализированный Застройщик '), 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(100,0, '', 0, 0, 'L');
        $pdf->Cell(0,0, self::getCyrillic('"ТАЛАН-ИЖЕВСК"'), 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(100,0, '', 0, 0, 'L');
        $pdf->Cell(10,0, self::getCyrillic($complex->director), 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(100,0, '', 0, 0, 'L');
        $pdf->Cell(10,0, self::getCyrillic('Cобственника: '.$newRequest->name), 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(100,0, '', 0, 0, 'L');
        $pdf->Cell(10,0, self::getCyrillic('Почтовый адрес: г.'.$city->label), 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(100,0, '', 0, 0, 'L');
        //$pdf->Cell(10,0, self::getCyrillic('ул.'.$complex->address.', д.'.$newRequest->building_number.', кв.'.$newRequest->apartment_number), 0, 0, 'L');
        $pdf->Cell(10,0, self::getCyrillic($newRequest->building_number.', кв.'.$newRequest->apartment_number), 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(100,0, '', 0, 0, 'L');
        $pdf->Cell(10,0, self::getCyrillic('Мобильный телефон: '.$newRequest->phone), 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(100,0, '', 0, 0, 'L');
        $pdf->Cell(10,0, self::getCyrillic('e-mail: '.$newRequest->email), 0, 0, 'L');
        $pdf->Ln(20);
        $pdf->Cell(0,0, self::getCyrillic('Заявление.'), 0, 0, 'C');
        $pdf->Ln(20);
        $pdf->Write(6, self::getCyrillic('Прошу устранить неполадки в моей квартире в соответствии с гарантийными обязательствами.'));
        $pdf->Write(6, self::getCyrillic($newRequest->comment));
        $pdf->Ln(40);
        $pdf->Cell(0,0, self::getCyrillic('Заявка                   '.$newRequest->pdf_id), 0, 0, 'L');
        $pdf->Ln(6);

        $cl = htmlentities($complex->label);
        $cl = str_replace(array('&laquo;', '&raquo;'), "'", $cl);

        $pdf->Cell(0,0, self::getCyrillic('Жилой комплекс   '.$cl), 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(0,0, self::getCyrillic('Дом                        '.$newRequest->building_number), 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(0,0, self::getCyrillic('Квартира               '.$newRequest->apartment_number), 0, 0, 'L');
        $pdf->Ln(40);
        $pdf->Cell(150,0, '', 0, 0, 'L');
        $pdf->Cell(0,0, self::getCyrillic('Дата ').date('d.m.Y', $newRequest->created_at), 0, 0, 'L');
        $pdf->Ln(10);
        $pdf->Cell(150,0, '', 0, 0, 'L');
        $pdf->Cell(0,0, self::getCyrillic('Подпись ____________'), 0, 0, 'L');
        $pdf->Output('F', 'uploads/pdf/'.$newRequest->pdf_id.'.pdf', true);
    }

    public static function getCyrillic($text)
    {
        return iconv('UTF-8', 'ISO-8859-5', $text);
    }
}
