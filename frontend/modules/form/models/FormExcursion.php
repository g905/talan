<?php

namespace frontend\modules\form\models;

use yii\base\Event;
use yii\base\Model;
use yii\helpers\Html;
use common\models\City;
use common\helpers\MailerHelper;
use common\models\RequestExcursion;

/**
 * This is the model class for table "request".
 *
 * @property string $name
 * @property string $phone
 */
class FormExcursion extends Model
{
    public $name;
    public $phone;
    public $apartment_id;
    public $apartment_complex_id;

    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['apartment_complex_id'], 'safe'],
            [['name', 'phone', 'apartment_id'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => t('_Your name', 'form-ask-question'),
            'phone' => t('_Phone', 'form-ask-question'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {
        if ($this->validate()) {
            $newRequest = new RequestExcursion();
            $newRequest->city_id = obtain('id', City::getUserCity());
            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->apartment_id = $this->apartment_id;

            $apartment = Apartment::find()->where(["id" => $model->apartment_id])->one();
            if(!empty($apartment)){
                $apartment_id = $apartment->label;
                $apartment_complex_id = $apartment->apartmentComplex->label;
            }
            else{
                $apartment_id = "Не определено";
                $apartment_complex_id = "Не определено";
            }


            $saveResult = $newRequest->save(false);

            if ($saveResult) {
                 app()->trigger(
                     'bitrixData',
                     new Event([
                         'sender' => [
                             'title' => 'Записаться на экскурсию',
                             'source_description' => 'заявка с сайта - Записаться на экскурсию',
                             'form' => [
                                 'name' => $newRequest->name,
                                 'phone' => $newRequest->phone,
                                 'city' => obtain('label', City::getUserCity()),
                                 'comment' => $apartment_complex_id.", ".$apartment_id,
                             ]
                         ]
                     ])
                 );

                MailerHelper::sendNotificationEmail(bt('excursion request', 'menu'), [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                    'Квартира' => $newRequest->apartment->label ?? 'Не определено',
                ]);
            }

            return $saveResult;
        }

        return null;
    }
}
