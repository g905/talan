<?php

namespace frontend\modules\form\models;

use common\models\Hypothec;
use yii\base\Event;
use yii\base\Model;
use yii\helpers\Html;
use metalguardian\fileProcessor\helpers\FPM;
use common\models\City;
use common\helpers\MailerHelper;
use common\models\RequestHypothec;
use yii\web\UploadedFile;

/**
 * This is the model class for table "request".
 *
 * @property string $name
 * @property string $phone
 */
class FormHypothecWithDoc extends Model
{
    public $name;
    public $phone;
    public $email;
    public $file;

    public function rules()
    {
        return [
            [['name', 'phone', 'email'], 'required'],
            [['email'], 'email'],
            [['name', 'phone'], 'string', 'max' => 255],
            [['file'], 'file'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => t('_Your name', 'form-hypothec-with-doc'),
            'phone' => t('_Phone', 'form-hypothec-with-doc'),
            'email' => t('_Email', 'form-hypothec-with-doc'),
            'file' => t('_File', 'form-hypothec-with-doc'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {
        if ($this->validate()) {
            $newRequest = new RequestHypothec();
            $newRequest->city_id = obtain('id', City::getUserCity());
            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->email = $this->email;
            $newRequest->file_id = $this->upload();
            $saveResult = $newRequest->save(false);

            if ($saveResult) {
                $data = [
                    'Имя' => $newRequest->name,
                    'Телефон' => $newRequest->phone,
                    'Почта' => $newRequest->email,
                ];

                $form = [
                    'name' => $newRequest->name,
                    'phone' => $newRequest->phone,
                    'emails' => $newRequest->email,
                    'city' => obtain('label', City::getUserCity())
                ];
                if ($newRequest->file_id !== null) {
                    $data['Анкета'] = request()->getHostInfo() . FPM::originalSrc($newRequest->file_id);
                    $form['comment'] = request()->getHostInfo() . FPM::originalSrc($newRequest->file_id);
                }

                app()->trigger('bitrixData', new Event(['sender' => [
                    'title' => 'Запрос на ипотеку с бланком анкеты заемщика',
                    'source_description' => 'заявка с сайта - Запрос на ипотеку с бланком анкеты заемщика',
                    'form' => $form
                ]]));

                $pageModel = Hypothec::findByCityWithBenefitsAndStepsOrFail(City::getUserCity());

                $email = [];

                if ($pageModel) {
                    if (!empty($pageModel->notification_email)) {
                        $email = explode("\n", $pageModel->notification_email);
                    }
                }

                MailerHelper::sendNotificationEmail(bt('Hypothec request', 'menu'), $data,  $email);
            }

            return $saveResult;
        }

        return null;
    }

    public function upload()
    {
        if ($this->validate(['file'])) {
            $file = UploadedFile::getInstance($this, 'file');
            if ($file) {
                return FPM::transfer()->saveUploadedFile($file);
            }
            return null;
        } else {
            return null;
        }
    }
}
