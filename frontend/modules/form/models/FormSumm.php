<?php

namespace frontend\modules\form\models;

use common\models\Hypothec;
use common\models\RequestSumm;
use yii\base\Event;
use yii\base\Model;
use yii\helpers\Html;
use common\models\City;
use common\helpers\MailerHelper;
use common\models\RequestHypothec;

/**
 * This is the model class for table "request".
 *
 * @property string $name
 * @property string $phone
 */
class FormSumm extends Model
{
   public $phone;

    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['phone'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
          'phone' => t('_Phone', 'form-summ'),
        ];
    }

    public function ajaxValidation()
    {
        $result = [];
        $this->validate();
        foreach ($this->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this, $attribute)] = $errors;
        }

        return $result;
    }

    public function save()
    {
        if ($this->validate()) {
            $newRequest = new RequestSumm();
            $newRequest->city_id = obtain('id', City::getUserCity());
            $newRequest->phone = $this->phone;
            $saveResult = $newRequest->save(false);

            if ($saveResult) {

                app()->trigger('bitrixData', new Event(['sender' => [
                        'title' => 'Запрос на оплату всей суммой',
                        'source_description' => 'заявка с сайта - Запрос на оплату всей суммой',
                        'form' => [
                            'phone' => $newRequest->phone,
                            'city' => obtain('label', City::getUserCity())
                        ]]])
                );
            }
            return $saveResult;
        }

        return null;
    }
}
