<?php
namespace frontend\modules\form\models;

use common\models\Apartment;
use Yii;
use yii\base\Event;
use yii\base\Model;
use yii\helpers\Html;

class FormAscAboutDiscount extends Model {

	public $username;
	public $phone;
    public $apartment_id;
    public $apartment_complex_id;

	public function rules() {
		return [
			[['username', 'phone',], 'required'],
			[['username', 'phone','apartment_id'], 'string', 'max' => 255,],
		];
	}

	public function attributeLabels() {
		return [
			'username' => t('_Your name', 'form-ask-question'),
            'phone' => t('_Phone', 'form-ask-question'),
		];
	}

	public function ajaxValidation()
	{
		$result = [];
		$this->validate();
		foreach ($this->getErrors() as $attribute => $errors) {
			$result[Html::getInputId($this, $attribute)] = $errors;
		}

		return $result;
	}

	public function bitrixSend() {

		if($this->validate()) {
//var_dump($this->apartment_id);
			$formTitle = Yii::$app->config->get('formAscQuerstionTitle');
			//$apartment = Apartment::find()->where(["id"=>$this->apartment_id])->one();

			app()->trigger('bitrixData',
				new Event(
					[
						'sender' => [
							'title' => $formTitle,
							'source_description' => 'заявка с сайта - ' . $this->apartment_id,
							'form' => [
								'phone' => $this->phone,
								'name' => $this->username,
								'comment' => $this->apartment_complex_id.", ".$this->apartment_id,
								//'comment' => $this->apartment_id
							],
						],
					]
				)
			);

			return true;
		}
		return null;
	}
}