<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 7/1/18
 * Time: 6:24 PM
 */

namespace frontend\modules\resale\components;


use frontend\modules\apartment\components\ApartmentsListView as BaseListView;

class ApartmentsListView extends BaseListView
{
    /**
     * @return string|void
     */
    public function run()
    {
        if ($this->showOnEmpty || $this->dataProvider->getCount() > 0) {
            $content = preg_replace_callback('/{\\w+}/', function ($matches) {
                $content = $this->renderSection($matches[0]);

                return $content === false ? $matches[0] : $content;
            }, $this->layout);
        } else {
            $content = $this->renderEmpty();
        }

        echo $content;
    }
}