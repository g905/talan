<?php

namespace frontend\modules\resale;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\resale\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
