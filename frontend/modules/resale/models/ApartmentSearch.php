<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 6/29/18
 * Time: 10:24 PM
 */

namespace frontend\modules\resale\models;


use common\helpers\Property;
use common\models\Apartment;
use common\models\City;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class ApartmentSearch extends Model
{
    const PAGE_SIZE = 6;

    public $squareFrom;
    public $squareTo;
    public $priceFrom;
    public $priceTo;
    public $floor;

    /**
     * @var ActiveDataProvider
     */
    protected $_dataProvider;
    /**
     * Store internal data combined from DB
     *
     * @var []
     */
    protected $_data = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['squareFrom', 'squareTo', 'priceFrom', 'priceTo', 'floor'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // preset form values, load() will overwrite them
        $this->squareFrom = $this->getMinSquare();
        $this->squareTo = $this->getMaxSquare();
        $this->priceFrom = $this->getMinPrice();
        $this->priceTo = $this->getMaxPrice();
    }

    /**
     * @return ActiveDataProvider
     */
    public function getDataProvider()
    {
        if (empty($this->_dataProvider)) {
            $query = $this->getBaseSearchQuery();
                //->with(['floorPlanPhoto']);

            $this->_dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => self::PAGE_SIZE,
                ]
            ]);

            if ($this->validate()) {
                $query->andFilterWhere(['>=', 'total_area', $this->squareFrom])
                    ->andFilterWhere(['<=', 'total_area', (float)$this->squareTo + 1]) // todo: because markup js rounding to integer values
                    ->andFilterWhere(['>=', 'price_from', $this->priceFrom])
                    ->andFilterWhere(['<=', 'price_from', $this->priceTo])
                    ->andFilterWhere(['floor' => $this->floor]);
            }
        }

        return $this->_dataProvider;
    }

    /**
     * @return \common\components\model\DefaultQuery
     */
    protected function getBaseSearchQuery()
    {
        return Apartment::find()
            ->joinWith('apartmentComplex')
            ->andWhere([
                'apartment.published' => 1,
                'apartment.type' => Property::TYPE_SECONDARY,
                'apartment_complex.city_id' => City::getUserCity()->id,
            ])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return mixed
     */
    public function getMinSquare()
    {
        $keyName = __METHOD__;
        if (!array_key_exists($keyName, $this->_data)) {
            $this->_data[$keyName] = $this->getBaseSearchQuery()->min('total_area');
        }

        return $this->_data[$keyName];
    }

    /**
     * @return mixed
     */
    public function getMaxSquare()
    {
        $keyName = __METHOD__;
        if (!array_key_exists($keyName, $this->_data)) {
            $this->_data[$keyName] = $this->getBaseSearchQuery()->max('total_area');
        }

        return $this->_data[$keyName];
    }

    /**
     * @return mixed
     */
    public function getMinPrice()
    {
        $keyName = __METHOD__;
        if (!array_key_exists($keyName, $this->_data)) {
            $this->_data[$keyName] = $this->getBaseSearchQuery()->min('price_from');
        }

        return $this->_data[$keyName];
    }

    /**
     * @return mixed
     */
    public function getMaxPrice()
    {
        $keyName = __METHOD__;
        if (!array_key_exists($keyName, $this->_data)) {
            $this->_data[$keyName] = $this->getBaseSearchQuery()->max('price_from');
        }

        return $this->_data[$keyName];
    }

    /**
     * @return mixed
     */
    public function getFloorDictionary()
    {
        $keyName = __METHOD__;
        if (!array_key_exists($keyName, $this->_data)) {
            $data = $this->getBaseSearchQuery()
                ->select([
                    'apartment_complex_id',
                    'floor',
                    'key' => 'floor'
                ])
                ->orderBy(['floor' => SORT_ASC])
                ->asArray()
                ->all();
            $this->_data[$keyName] = ArrayHelper::map($data, 'key', 'floor');
        }

        return $this->_data[$keyName];
    }
}
