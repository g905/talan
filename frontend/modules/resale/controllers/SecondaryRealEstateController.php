<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 6/29/18
 * Time: 8:01 PM
 */

namespace frontend\modules\resale\controllers;

use yii\web\NotFoundHttpException;
use common\models\City;
use common\models\SecondaryRealEstate;
use frontend\components\MetaRegistrar;
use frontend\components\FrontendController;
use frontend\modules\resale\models\ApartmentSearch;

class SecondaryRealEstateController extends FrontendController
{
    public function actionIndex($type, $city = null)
    {
        $currentCity = City::getCurrentCity($city);
        $model = SecondaryRealEstate::getModelByCity($currentCity);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        MetaRegistrar::register($model, $model->label);

        $searchModel = new ApartmentSearch();
        $searchModel->load(request()->get(), '');

        return $this->render('index', [
            'model' => $model,
            'propertyType' => $type,
            'searchModel' => $searchModel,
        ]);
    }
}
