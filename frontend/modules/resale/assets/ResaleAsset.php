<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 7/1/18
 * Time: 4:01 PM
 */

namespace frontend\modules\resale\assets;


use frontend\assets\AppAsset;
use yii\web\AssetBundle;

class ResaleAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/resale/assets';
    public $js = [
        'js/resale.js',
    ];
    public $depends = [
        AppAsset::class,
    ];
}