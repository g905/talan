(function () {
    var initCounter = 0;
    var isInited = false;
    var isFinished = false;
    var timer;

    /* thanks to markup fucking JS events usage */
    var delayedExecution = function(callback) {
        if (!isFinished) {
            clearTimeout(timer);
        }
        timer = setTimeout(function() {
            callback();
        }, 500);
    };

    var sendFilterRequest = function() {
        if (isInited) {
            delayedExecution(function () {
                var form = $('#resale_filter_form');
                form.submit();
            });
        } else {
            initCounter++;
            if (initCounter > 3) {
                isInited = true;
            }
        }
    };

    $(document).on('submit', '#resale_filter_form', function(event) {
        var url = $(event.target).attr('action') + '?' + $(event.target).serialize();
        $.pjax({url: url, container: '#pjax_resale_apartments', scrollTo: false});
        return false;
    });

    $(document).on('nouisliderChange', '', function(e) {
        sendFilterRequest();
    });

    $(document).on('click', '#resale_filter_form [data-select-item-value]', function(e) {
        var dropdown = $('#apartmentsearch-floor');
        var val = $(this).data('select-item-value');
        dropdown.val(val);
        sendFilterRequest();
    });
})();