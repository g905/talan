<?php
use frontend\modules\page\widgets\PageHeaderWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\resale\components\ApartmentsListView;
use common\models\City;
use common\helpers\SiteUrlHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use frontend\modules\resale\assets\ResaleAsset;
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 6/29/18
 * Time: 8:12 PM
 *
 * @var $this \yii\web\View
 * @var $model \common\models\SecondaryRealEstate
 * @var $complexListModel \common\models\ComplexList
 * @var $propertyType integer
 * @var $searchModel \frontend\modules\resale\models\ApartmentSearch
 */
ResaleAsset::register($this);
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('front/resale', 'real_estate'),
    'url' => SiteUrlHelper::createComplexListUniversalUrl(['type' => $propertyType]),
];
$this->params['breadcrumbs'][] = $model->label;
$this->params['headerMenu'] = $model->menus;
$this->params['headerMenuType'] = $model;
?>
<section class="poster poster_padding">
    <?php if ($model->isDisplayableVideo()) : ?>
    <video class="poster__bg poster__bg_video" autoplay loop muted data-video-src="<?= $model->getPageBackgroundVideo() ?>" data-poster-video data-autoplay data-keepplaying></video>
    <?php endif; ?>
    <div class="poster__bg poster__bg_photo" data-bg-src="<?= $model->getPageBackgroundImage() ?>" data-bg-pos="center" data-bg-size="cover"></div>
    <div class="poster__cover"></div>
    <div class="poster__inner">
        <div class="poster__content">
            <h1 class="h1 poster__h1 wow fadeInUp"> <?= $model->label ?> </h1>
            <h5 class="h5 poster__h5 wow fadeInUp" data-wow-delay="0.3s"> <?= $model->description ?> </h5>
            <h5 class="h5 poster__h5 poster__mob-text wow fadeInUp" data-wow-delay="0.6s"> <?= $model->description_only_desktop ?> </h5>
            <a href="#!" class="button button_transparent poster__button wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s" data-scroll_to="apartments_list_block"> <?= $model->button_text ?>
                <div class="button__bg"></div>
                <div class="button__blip button__blip_hover"></div>
                <div class="button__blip button__blip_click"></div>
            </a>
        </div>
    </div>
    <div class="down poster__down wow down-to-top">
        <button class="down__button" data-scroll-down></button>
        <div class="down__label">вниз</div>
    </div>
</section>
<!-- /POSTER -->
<?= PageHeaderWidget::widget(['currentCity' => City::getUserCity()]) ?>
<!-- VALUES -->
<section class="values values_commercial">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 values__h2 wow fadeInLeft"> <?= $model->conditions_label ?> </h2>
        <div class="values__list">
            <?php foreach($model->conditions as $conditionIndex => $condition) : ?>
            <div class="card values__card wow fadeInUp">
                <div class="card__img" data-bg-src="<?= $condition->getEntityThumb('imageProp', 'resale', 'condition') ?>" data-bg-pos="center" data-bg-size="cover"></div>
                <h5 class="h5 card__h5"> <?= $condition->label ?> </h5>
                <div class="card__number"> <?= str_pad($conditionIndex + 1, 2, "0", STR_PAD_LEFT) ?> </div>
                <div class="divider card__divider"></div>
                <div class="card__text">
                    <p> <?= $condition->content ?> </p>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<!-- /VALUES -->
<section class="take_apartments">
    <div class="wrap wrap_mobile_full take_apartments__wrap">
        <h2 class="take_apartments__h2 wow fadeInLeft"> <?= $model->opportunity_black_label ?>
            <span> <?= $model->opportunity_white_label ?> </span>
        </h2>
        <div class="take_apartments__content">
            <p class="take_apartments__content-text wow fadeInRight"><?= $model->opportunity_description ?></p>
            <a href="#!" class="button form__button wow fadeInUp" data-scroll_to="apartments_list_block"> <?= $model->opportunity_button_text ?>
                <div class="button__blip button__blip_hover"></div>
                <div class="button__blip button__blip_click"></div>
            </a>
        </div>
    </div>
</section>
<div class="wrap wrap_mobile_full" id="apartments_list_block">
    <div class="vtor-details__text-block ">
        <h2 class="h2 vtor-details__apartments-h2"><?= $model->apartments_label ?> </h2>
    </div>
</div>
<div class="wrap temporary wrap_mobile_full">
    <div class="buffer vtor-details__buffer">
        <div class="buffer__overflow-x-mobile">
            <div class="buffer__overflow-mobile">
                <?php $form = ActiveForm::begin([
                    'method' => 'GET',
                    'action' => \yii\helpers\Url::current([
                        'squareFrom' => null,
                        'squareTo' => null,
                        'priceFrom' => null,
                        'priceTo' => null,
                        'floor' => null,
                    ]),
                    'options' => [
                        'class' => 'filters wow fadeInLeft',
                        'id' => 'resale_filter_form',
                    ]
                ]) ?>
                    <div class="filters__section">
                        <div class="filters__heading"><?= Yii::t('front/resale', 'filter_square') ?></div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-range-min="<?= $searchModel->getMinSquare() ?>" data-range-max="<?= $searchModel->getMaxSquare() ?>" data-nouislider-type='square'>
                                <div class="nouislider__slider"></div>
                                <div class="ranges nouislider__ranges">
                                    <span class="ranges__label">от</span>
                                    <?= Html::activeInput('text', $searchModel, 'squareFrom', [
                                        'class' => 'ranges__input',
                                        'data' => [
                                            'no-ui-input-min' => true,
                                        ],
                                        'name' => 'squareFrom'
                                    ]) ?>
                                    <span class="ranges__label">до</span>
                                    <?= Html::activeInput('text', $searchModel, 'squareTo', [
                                        'class' => 'ranges__input',
                                        'data' => [
                                            'no-ui-input-max' => true,
                                        ],
                                        'name' => 'squareTo'
                                    ]) ?>
                                    <span class="ranges__label">м²</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="filters__section">
                        <div class="filters__heading"><?= Yii::t('front/resale', 'filter_price') ?></div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1000}' data-range-min="<?= $searchModel->getMinPrice() ?>" data-range-max="<?= $searchModel->getMaxPrice() ?>" data-nouislider-type='price'>
                                <div class="nouislider__slider"></div>
                                <div class="ranges nouislider__ranges">
                                    <span class="ranges__label">от</span>
                                    <?= Html::activeInput('text', $searchModel, 'priceFrom', [
                                        'class' => 'ranges__input ranges__input_no_padding',
                                        'data' => [
                                            'no-ui-input-min' => true,
                                        ],
                                        'name' => 'priceFrom'
                                    ]) ?>
                                    <span class="ranges__label">до</span>
                                    <?= Html::activeInput('text', $searchModel, 'priceTo', [
                                        'class' => 'ranges__input ranges__input_no_padding',
                                        'data' => [
                                            'no-ui-input-max' => true,
                                        ],
                                        'name' => 'priceTo'
                                    ]) ?>
                                    <span class="ranges__label">руб</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="filters__section">
                        <div class="filters__heading filters__heading_side_left"><?= Yii::t('front/resale', 'filter_floor') ?></div>
                        <div class="filters__inner filters__inner_side_right">
                            <div class="custom-select filters__custom-select" data-custom-select>
                                <?= Html::activeDropDownList($searchModel, 'floor', $searchModel->getFloorDictionary(), [
                                    'prompt' => '',
                                    'name' => 'floor'
                                ]) ?>
                            </div>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <?php Pjax::begin([
        'id' => 'pjax_resale_apartments',
        'timeout' => 25000,
        'enablePushState' => true,
        'linkSelector' => '.pagination a',
        'scrollTo' => '200',
        'options' => [
            'class' => 'apartaments'
        ]
    ]) ?>
        <?= ApartmentsListView::widget(['dataProvider' => $searchModel->getDataProvider([
            'options' => [
                'tag' => false,
            ]
        ])]) ?>
    <?php Pjax::end(); ?>
</div>
<!-- ZAYAVKA -->
<section class="zayavka statistics ipot-zayavka vtor-steps">
    <div class="wrap wrap_mobile_full">
        <div class="wow fadeInLeft">
            <h2 class="h2 values__h2 vtor-steps__h2"> <?= $model->steps_label ?> </h2>
            <p class="vtor-steps__text"> <?= $model->steps_description ?> </p>
        </div>
        <div class="statistics__list">
            <?php foreach($model->steps as $stepIndex => $step) : ?>
            <div class="card statistics__card wow fadeIn" data-wow-delay="0s" style="visibility: visible; animation-delay: 0s; animation-name: fadeIn;">
                <h3 class="h3 card__h3"><span class="card_span"><?= $stepIndex + 1 ?></span></h3>
                <h5 class="h5 card__h5"> <?= $step->label ?> </h5>
                <div class="divider card__divider"></div>
                <div class="card__text">
                    <p> <?= $step->content ?> </p>
                </div>
            </div>
            <?php endforeach; ?>
            <a href="<?= $model->steps_link_url ?>" class="more view-all__more vtor-steps__link"> <?= $model->steps_link_text ?> </a>
        </div>
    </div>
</section>
<!-- BLOG ARTICLES -->
<?= $this->render('@frontend/modules/hr/widgets/views/articles', [
    'title' => $model->articles_label,
    'btnLabel' => Yii::t('front/resale', 'all_articles'),
    'btnLink' => SiteUrlHelper::getBlogAllUrl(),
    'articles' => $model->articles,
    'cssClass' => 'articles vtor-details__articles',
]) ?>

<?= PageFooterWidget::widget(['currentCity' => City::getUserCity()])?>
