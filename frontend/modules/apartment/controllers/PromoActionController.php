<?php

namespace frontend\modules\apartment\controllers;

use Yii;
use common\models\City;
use common\models\PromoAction;
use frontend\components\MetaRegistrar;
use frontend\components\FrontendController;
use frontend\modules\form\widgets\CustomPopupRequestWidget;

/**
 * Class PromoActionController
 *
 * @author art
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package frontend\modules\apartment\controllers
 */
class PromoActionController extends FrontendController
{
    /**
     * View promo page.
     *
     * @param int $id promo record identifier.
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $city = City::getCurrentCity(request()->get('city'));
        $model = PromoAction::findOnePublishedOrFail($id);
        $model->incrementViewsCount();
        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->label);

        Yii::$app->params['footerPopup'] = CustomPopupRequestWidget::widget([
            'delayBeforeOpen' => true,
            'customPopupModel' => $city->customPopup
        ]);

        return $this->render('view', compact('model'));
    }
}
