<?php

namespace frontend\modules\apartment\controllers;

use common\models\Apartment;
use common\models\ApartmentBuilding;
use common\models\ApartmentComplex;
use frontend\components\MetaRegistrar;
use frontend\components\FrontendController;
use frontend\components\ViewCountService;

/**
 * Class ApartmentController
 *
 * @author art
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package frontend\modules\apartment\controllers
 */
class ApartmentController extends FrontendController
{
    /**
     * View apartment.
     *
     * @param int $type apartment type (must match complex type).
     * @param int $id apartment identifier.
     * @return string
     * @see Property class to find out available property type constants.
     */
    public function actionView(int $type, $id)
    {

        $model = Apartment::findOnePublishedOrFail($id);
        $this->checkCity($model->city->alias);

        $building = $model->apartmentBuilding;
        $complex = $model->apartmentComplex;

        $count = new ViewCountService('apartment', $model->id);
        $count->run();
        \Yii::$app->session->set('viewCountObject', $count);

        $model->incrementViewsCount();
        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->label);
if (\Yii::$app->request->get('test')) return $this->render('view-test', compact('model', 'complex', 'building'));
        return $this->render('view', compact('model', 'complex', 'building'));
    }
}
