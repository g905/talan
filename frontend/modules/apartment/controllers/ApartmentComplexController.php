<?php

namespace frontend\modules\apartment\controllers;

use common\models\ApartmentBuilding;
use frontend\widgets\FilterWidget;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\models\City;
use common\models\Apartment;
use common\helpers\Property;
use common\models\ComplexList;
use common\models\ApartmentComplex;
use frontend\components\MetaRegistrar;
use frontend\components\FrontendController;
use frontend\modules\form\widgets\CustomPopupRequestWidget;
use frontend\modules\apartment\models\BuildHistoryApartmentComplex;
use frontend\modules\apartment\components\ApartmentComplexHistoryInfoGrabber;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\helpers\ConfigHelper;

/**
 * Class ApartmentComplexController
 *
 * @author art
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package frontend\modules\apartment\controllers
 */
class ApartmentComplexController extends FrontendController
{
    const COMPLEXES_SHOWN = 2;
    const COMPLETED_PROJECTS_SHOWN = 3;

    /**
     * Render complexes list page.
     *
     * @param int $type complex type.
     * @return string
     * @see Property class to find out available property type constants.
     * @throws NotFoundHttpException
     */
    public function actionList(int $type)
    {

        $city = City::getCurrentCity(request()->get('city'));

        $model = ComplexList::findByCityIdAndTypeWithComplexesOrFail($city->id, $type);
        $this->checkCity($model->city->alias);

        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->top_title);
        $complexes = $model->complexes;
        $completedProjectsShown = $model->completedProjects;
        $benefits = $model->benefits;
        // Split general complexes and completed projects lists into parts, one is shown by default and other is hidden.
        // list($complexesShown, $complexesHidden) = $this->splitIntoShownAndHidden($complexes, self::COMPLEXES_SHOWN);
        //list($completedProjectsShown, $completedProjectsHidden) = $this->splitIntoShownAndHidden($completedProjects, self::COMPLETED_PROJECTS_SHOWN);
        /** @var ApartmentComplex $complex */
        $complex = obtain(0, $complexes);
        $provider = $complex->isCommercial() ? $complex->filter(request()->getQueryParams()) : false;

        app()->params['footerPopup'] = CustomPopupRequestWidget::widget([
            'delayBeforeOpen' => true,
            'customPopupModel' => $city->customPopup,
            'complexId' => $complex->id
        ]);

        return $this->render('complex_list', compact(
            'city', 'model', 'provider', 'benefits', 'complexes',
            'completedProjectsShown', 'completedProjectsHidden'
        ));
    }

    /**
     * View apartment complex page.
     *
     * @param int $type complex type.
     * @param string $alias apartment complex alias.
     * @return string
     * @see Property class to find out available property type constants.
     * @throws \Exception
     */
    public function actionView(string $alias, int $type)
    {
        $model = ApartmentComplex::findOnePublishedByAliasAndTypeOrFail($alias, $type);

        $this->checkCity($model->city->alias);

        $model->incrementViewsCount();
        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->label);

        app()->params['footerPopup'] = CustomPopupRequestWidget::widget([
            'delayBeforeOpen' => true,
            'customPopupModel' => $model->customPopup,
            'complexId' => $model->id,
        ]);
        return $this->render('view', compact('model'));
    }
 
    /**
     * View apartments list page.
     *
     * @param int $type complex type.
     * @param string $alias apartment complex alias.
     * @return string
     * @see Property class to find out available property type constants.
     * @throws \Exception
     */
    public function actionApartmentsList(string $alias, int $type, $test = 0)
    {
        /** @var ApartmentComplex $model */
        $model = ApartmentComplex::findOnePublishedByAliasAndTypeOrFail($alias, $type);
        $this->checkCity($model->city->alias);

        Url::remember(Url::current(), 'filter_params');

        $provider = $model->filter(request()->getQueryParams());


        $this->attachLastModifiedHeader((new Query())->from(Apartment::tableName())->max('updated_at'));
        MetaRegistrar::register($model, $model->label);

        app()->params['footerPopup'] = CustomPopupRequestWidget::widget([
            'delayBeforeOpen' => true,
            'customPopupModel' => $model->customPopup,
            'complexId' => $model->id,
        ]);

        return $this->render('apartment_list', compact('model', 'provider'));
    }

    public function actionChatApartmentsList ()
    {
        $params = request()->getQueryParams();
        $provider = $this->chatFilter(request()->getQueryParams());

        return $this->render('chat_apartment_list', compact('provider', 'params'));
    }

    public function chatFilter ($params)
    {
        //$model = Apartment::findOnePublishedByAliasAndTypeOrFail($alias, $type);

        $city_id = obtain('city_id', $params, City::getUserCity());
        $complex = obtain('complex', $params, null);
        $complexes = explode(',', $complex);
        if (count($complexes) === 1)
        {
            $complexes = $complexes[0];
        }
        $type = obtain('type', $params, null);
        $rooms = obtain('rooms', $params, null);
        $floor = obtain('floor', $params, null);

        $floors = explode(',', $floor);

        foreach($floors as $fl)
        {
            if(strpos($fl, '-'))
            {
                $diapazone = explode('-', $fl);
                for($i = $diapazone[0]; $i<=$diapazone[1]; $i++)
                {
                    $floors_array[] = (string)$i;
                }
            } else {
                $floors_array[] = $fl;
            }
        }

        sort($floors_array);

        $for = obtain('for', $params);

        $query = Apartment::find()->alias('a')->where([
            'a.published' => Apartment::IS_PUBLISHED,
        ]);
        $city_id === null ?: $query->joinWith('apartmentComplex')->where(['apartment_complex.city_id' => $city_id]);
        $complex === null ?: $query->andFilterWhere(['IN', 'apartment_complex_id', $complexes]);
        $type === null ?: $query->andFilterWhere(['=', 'a.type', $type]);
        $rooms === null ?: $query->andFilterWhere(['=', 'a.rooms_number', $rooms]);
        $floor === null ?: $query->andFilterWhere(['IN', 'a.floor', $floors_array]);

        if ($for) {
            $forMap = [
                FilterWidget::FILTER_YOUNG_FAMILY => ['<', 'a.rooms_number', 3],
                FilterWidget::FILTER_PENSIONER => ['<', 'a.rooms_number', 2],
                FilterWidget::FILTER_SINGLES => ['<', 'a.rooms_number', 2],
                FilterWidget::FILTER_BIG_FAMILY => ['>=', 'a.rooms_number', 3],
            ];
            $condition = obtain($for, $forMap);
            !is_array($condition) ? : $query->andFilterWhere($condition);
        }

        $query->andFilterWhere(['a.published' => Apartment::IS_PUBLISHED]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'position' => SORT_ASC,
                ],
            ],
            'pagination' => [
                'pageSize' => ConfigHelper::getApartmentsPerPage(),
                'defaultPageSize' => ConfigHelper::getApartmentsPerPage(),
            ],
        ]);
        //dd($provider->query->all());
        //return json_encode($provider->query->all());
        return $provider;
    }

    /**
     * @param string $alias
     * @param int $type
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionApartmentsConfigurator(string $alias, int $type)
    {
        /** @var ApartmentComplex $model */
        $model = ApartmentComplex::findOnePublishedByAliasAndTypeOrFail($alias, $type);
        $this->checkCity($model->city->alias);
        $mapModel = $model->map;

        $this->attachLastModifiedHeader((new Query())->from(Apartment::tableName())->max('updated_at'));

        app()->params['footerPopup'] = CustomPopupRequestWidget::widget([
            'delayBeforeOpen' => true,
            'customPopupModel' => $model->customPopup,
            'complexId' => $model->id,
        ]);
 
        MetaRegistrar::register($model, $model->label);

        return $this->render('apartment_configurator', compact('model', 'mapModel'));
    }

    /**
     * @param string $alias
     * @param int $type
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionApartmentsConstructor(string $alias, int $type, int $id, int $flat)
    {
        /** @var ApartmentComplex $model */
        $complex = ApartmentComplex::findOnePublishedByAliasAndTypeOrFail($alias, $type);

        $building = ApartmentBuilding::findOnePublishedByIdOrFail($id);

        $apartment = Apartment::findOnePublishedOrFail($flat);

        $this->attachLastModifiedHeader((new Query())->from(Apartment::tableName())->max('updated_at'));

        app()->params['footerPopup'] = CustomPopupRequestWidget::widget([
            'delayBeforeOpen' => true,
            'customPopupModel' => $complex->customPopup,
            'complexId' => $complex->id,
        ]);

        MetaRegistrar::register($complex, $complex->label);

        return $this->render('apartment_constructor', compact('complex', 'building', 'apartment'));
    }

    public function actionHistory(string $alias, int $type)
    {
        $model = BuildHistoryApartmentComplex::findOnePublishedByAliasAndTypeOrFail($alias, $type);
        $this->attachLastModifiedHeader($model->updated_at);
        $city = City::getCurrentCity(request()->get('city'));
        $complexModel = ComplexList::findByCityIdAndTypeWithComplexesOrFail($city->id, $type);
        $completedProjects = $complexModel->completedProjects;
        list($completedProjectsShown, $completedProjectsHidden) = $this->splitIntoShownAndHidden($completedProjects, self::COMPLETED_PROJECTS_SHOWN);

        MetaRegistrar::register($model, $model->label);

        return $this->render('history', [
            'city' => $city,
            'model' => $model,
            'complexModel' => $complexModel,
            'completedProjectsShown' => $completedProjectsShown,
            'completedProjectsHidden' => $completedProjectsHidden,
        ]);
    }

    /**
     * @param string $alias
     * @return array
     */
    public function actionApartmentHistoryInfo(string $alias)
    {
        app()->response->format = Response::FORMAT_JSON;

        return ApartmentComplexHistoryInfoGrabber::getInfoByComplexAlias($alias);
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionStream()
    {
        $iframe = request()->post('iframe', false);
//        var_dump($iframe);die;
        response()->format = Response::FORMAT_JSON;
        if ($iframe && request()->getIsAjax()) {
            $data = [
                'js' => Html::script('showPopup();'),
                'replaces' => [
                    ['data' => $this->renderPartial('_stream', ['iframe' => $iframe]), 'what' => '.popup-replace'],
                ],
            ];

            return $data;
        }

        throw new NotFoundHttpException();
    }

    /**
     * Splits an array into two parts.
     * First array entry collects items that should be shown be default,
     * second - collects items that should be hidden.
     *
     * @param array $list
     * @param int $shownNumber
     * @return array
     */
    private function splitIntoShownAndHidden(array $list, int $shownNumber)
    {
        $output = [$list, []];
        if (count($list) > $shownNumber) {
            $output[0] = array_slice($list, 0, $shownNumber);
            $output[1] = array_slice($list, $shownNumber);
        }

        return $output;
    }
}
