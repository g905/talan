<?php

namespace frontend\modules\apartment\controllers;

use yii\web\Response;
use common\helpers\Property;
use common\models\ApartmentBuilding;
use frontend\components\MetaRegistrar;
use frontend\components\FrontendController;
use frontend\modules\apartment\components\ApartmentBuildingHistoryInfoGrabber;
use frontend\modules\form\widgets\CustomPopupRequestWidget;

/**
 * Class ApartmentBuildingController
 *
 * @author dimarychek
 * @package frontend\modules\apartment\controllers
 */
class ApartmentBuildingController extends FrontendController
{
    /**
     * View apartment building page.
     *
     * @param int $id apartment complex identifier.
     * @return string
     * @see Property class to find out available property type constants.
     * @throws \Exception
     */
    public function actionView($id)
    {
        $model = ApartmentBuilding::findOnePublishedOrFail($id);
        $this->checkCity($model->city->alias);

        $model->incrementViewsCount();
        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->label);

        app()->params['footerPopup'] = CustomPopupRequestWidget::widget([
            'delayBeforeOpen' => true,
            'customPopupModel' => $model->customPopup,
            'complexId' => $model->id,
        ]);

        return $this->render('view', compact('model'));
    }

    public function actionApartmentHistoryInfo($id)
    {
        app()->response->format = Response::FORMAT_JSON;

        return ApartmentBuildingHistoryInfoGrabber::getInfoByBuildingId($id);
    }
}
