<?php

namespace frontend\modules\apartment\controllers;

use common\models\HowBuyBlock;
use common\models\PromoAction;
use frontend\components\MetaRegistrar;
use frontend\components\FrontendController;

/**
 * Class HowBuyBlockController
 *
 * @author art
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package frontend\modules\apartment\controllers
 */
class HowBuyBlockController extends FrontendController
{
    /**
     * View how to buy page.
     *
     * @param int $id how buy record identifier.
     * @return string
     */
    public function actionView($id)
    {
        $model = HowBuyBlock::findOnePublishedOrFail($id);
        MetaRegistrar::register($model, $model->label);
        // some action
        $promoAction = PromoAction::find()->where(['published' => PromoAction::IS_PUBLISHED])->one();

        return $this->render('view', compact('model', 'promoAction'));
    }
}
