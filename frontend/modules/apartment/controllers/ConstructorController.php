<?php

namespace frontend\modules\apartment\controllers;

use common\helpers\MailerHelper;
use common\models\ApartmentConstructorOptionPrice;
use common\models\ApartmentConstructorPrice;
use common\helpers\SiteUrlHelper;
use common\models\Apartment;
use common\models\ApartmentConstructor;
use common\models\City;
use common\models\ConstructorRequest;
use Yii;
use frontend\components\FrontendController;
use yii\base\Event;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Class ConstructorController
 *
 * @author sanja
 * @package frontend\modules\apartment\controllers
 */
class ConstructorController extends FrontendController
{
    public $enableCsrfValidation = false;


    /**
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request->get();

        if ($request) {

            $data = [];

            $apartment = Apartment::findOnePublishedOrFail($request['id']);

            $data['global'] = $this->actionGlobalData($apartment);

            $data['items'] = $this->actionApartmentData();

            return Json::encode($data);
        }

        throw new NotFoundHttpException();
    }

    /**
     * @param $apartment
     * @return array
     */
    public function actionGlobalData($apartment)
    {
        $globalData = [
            'flat_id' => intval($apartment->id),
            'flat_price' => intval($apartment->price_from),
            'flat_square' => intval($apartment->total_area),
            'square_price' => ApartmentConstructorPrice::getPrice($apartment),
            'loadingSrc' => '/static/img/vue-constructor/loading.gif',
            'texts' => [
                'sum' => Yii::t('front/constructor', 'Итого'),
                'currency' => Yii::t('front/constructor', 'руб'),
                'btns' => Yii::t('front/constructor', 'Купить квартиру с отделкой')
            ],
            'form' => [
                'button' => Yii::t('front/constructor', 'Отправить'),
                'button_sending' => Yii::t('front/constructor', 'Отправка...'),
                'sended' => Yii::t('front/constructor', 'Отправлено'),
                'error' => Yii::t('front/constructor', 'Ошибка'),
                'postLinks' => [
                    'fullPrice' => SiteUrlHelper::getApartmentConstructorSubmitUrl()
                ],
                'fields' => [
                    [
                        'name' => Yii::t('front/constructor', 'Имя'),
                    ],
                    [
                        'name' => Yii::t('front/constructor', 'Телефон'),
                    ]
                ],
                'afterFilteForm' => [
                    'title' => Yii::t('front/constructor', 'Варианты покупки:'),
                    'forms' => [
                        [
                            'id' => ConstructorRequest::FULL_PRICE_FORM,
                            'name' => Yii::t('front/constructor', 'Вся сумма'),
                            'postLinks' => SiteUrlHelper::getApartmentConstructorSubmitUrl()
                        ],
                        [
                            'id' => ConstructorRequest::CREDIT_HOUSING_FORM,
                            'name' => Yii::t('front/constructor', 'В зачет жилья'),
                            'postLinks' => SiteUrlHelper::getApartmentConstructorSubmitUrl()
                        ]
                    ]
                ]
            ]
        ];

        return $globalData;
    }

    /**
     * @return array
     */
    public function actionApartmentData()
    {
        $city = City::getUserCity();

        $roomData = [
            [
                'name' => Yii::t('front/constructor', 'Комната'),
                'filters' => [
                    [
                        'name' => ApartmentConstructor::getTypeDictionary()[ApartmentConstructor::CONSTRUCTOR_WALLS],
                        'items' => ApartmentConstructor::getDataByType(ApartmentConstructor::CONSTRUCTOR_WALLS)
                    ],
                    [
                        'name' => ApartmentConstructor::getTypeDictionary()[ApartmentConstructor::CONSTRUCTOR_FLOORS],
                        'items' => ApartmentConstructor::getDataByType(ApartmentConstructor::CONSTRUCTOR_FLOORS)
                    ],
                    [
                        'name' => ApartmentConstructor::getTypeDictionary()[ApartmentConstructor::CONSTRUCTOR_DOORS],
                        'items' => ApartmentConstructor::getDataByType(ApartmentConstructor::CONSTRUCTOR_DOORS)
                    ],
                    [
                        'name' => ApartmentConstructor::getTypeDictionary()[ApartmentConstructor::CONSTRUCTOR_OPTIONS],
                        'items' => ApartmentConstructorOptionPrice::getDataOptions()
                    ],
                ],
            ],
            [
                'name' => Yii::t('front/constructor', 'Caнузел'),
                'filters' => [
                    [
                        'name' => ApartmentConstructor::getTypeDictionary()[ApartmentConstructor::CONSTRUCTOR_BATHROOM],
                        'items' => ApartmentConstructor::getDataByType(ApartmentConstructor::CONSTRUCTOR_BATHROOM)
                    ],
                ]
            ]
        ];

        $constructorOption = ApartmentConstructorOptionPrice::find()
            ->where(['city_id' => $city->id])
            ->isPublished()
            ->all();

        if (!$constructorOption) {
            array_pop($roomData[0]['filters']);
        }

        return $roomData;
    }

    /**
     * @return string
     */
    public function actionFormSubmit()
    {
        $request = Yii::$app->request->post();

        if ($request) {
            $city = City::getUserCity();

            $flat = Apartment::findOnePublishedOrFail($request['id_flat']);

            $complex = $flat->apartmentComplex;

            $model = new ConstructorRequest();

            $model->name = $request['name'];
            $model->phone = $request['tel'];
            $model->price = $request['fullPrice'];
            $model->city_id = $city->id;
            $model->complex_id = $complex->id;
            $model->flat_id = $request['id_flat'];
            $model->data = ApartmentConstructor::getData($request);
            $model->type = $request['nameOfForm'];

            if ($model->validate() && $model->save()) {
                $data = [
                    'Имя' => $model->name,
                    'Телефон' => $model->phone,
                    'Стоимость' => $model->price,
                    'Отделка' => $model->data,
                    'Тип' => ConstructorRequest::getTypeDictionary()[$model->type]
                ];

                app()->trigger('bitrixData', new Event(['sender' => [
                        'title' => 'Запрос c конструктора',
                        'source_description' => 'заявка с сайта - Запрос c конструктора',
                        'form' => [
                            'name' => $model->name,
                            'phone' => $model->phone,
                        ]]])
                );

                MailerHelper::sendNotificationEmail(bt('Constructor request', 'menu'), $data);

                return true;
            }

            return false;
        }

        throw new NotFoundHttpException();
    }
}
