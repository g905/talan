<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.05.18
 * Time: 18:05
 */

namespace frontend\modules\apartment\models;


class BuildHistoryApartmentComplexItemSlide extends \common\models\BuildHistoryApartmentComplexItemSlide
{
    public $year;
    public $month;
    public $date;
}