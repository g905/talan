<?php

namespace frontend\modules\apartment\models;

class BuildHistoryApartmentBuildingItemSlide extends \common\models\BuildHistoryApartmentBuildingItemSlide
{
    public $year;
    public $month;
    public $date;
}
