<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\apartment\widgets\apartmentsList;

use yii\base\Widget;

class ApartmentsListTopBlockWidget extends Widget
{

    public $page;
    public function run()
    {
        if (!isset($this->page)) {
            return null;
        }

        echo $this->render('apartmentsListTopBlockWidgetView',[
            'model' => $this->page
        ]);
    }
}
