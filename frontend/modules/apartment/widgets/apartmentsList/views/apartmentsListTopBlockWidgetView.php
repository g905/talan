<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 25.12.2017
 * Time: 16:43
 */

use frontend\helpers\FrontendHelper;
use common\helpers\SiteUrlHelper;

/** @var \common\models\ApartmentComplex $model */
?>

<div class="wrap wrap_mobile_full">
    <div class="apartment-head choose-ap">
        <h2 class="h2 apartment-head__h2 wow fadeInLeft">
            <?= $model->label?>
        </h2>
        <?php if ($model->buildingsForChess) : ?>
            <div class="choose-ap__switch wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                <?php if (count($model->buildingsForChess) > 1) : ?>
                    <a class="choose-ap__btn" href="<?= SiteUrlHelper::createApartmentComplexConfiguratorUrl(['alias' => $model->alias, 'type' => $model->type]) ?>"> Конфигуратор </a>
                <?php else : ?>
                    <a class="choose-ap__btn" href="<?= SiteUrlHelper::createApartmentComplexPlanUrl(['alias' => $model->alias, 'id' => $model->buildingsForChess[0]->id]) ?>"> Конфигуратор </a>
                <?php endif; ?>
                <span class="choose-ap__btn choose-ap__btn-a" target="_self"> Планировки </span>
            </div>
        <?php endif; ?>
        <?php if (!FrontendHelper::isNullOrEmpty($model->general_progress_percent)): ?>
        <div class="diagram apartment-head__diagram wow fadeInLeft" data-wow-delay="0.5s">

            <div class="circle-progress" data-circle-progress="<?=$model->general_progress_percent?>">
                <div class="circle-progress__outer">
                    <div class="circle-progress__progress circle-progress__progress_10"></div>
                    <div class="circle-progress__progress circle-progress__progress_20"></div>
                    <div class="circle-progress__progress circle-progress__progress_30"></div>
                    <div class="circle-progress__progress circle-progress__progress_40"></div>
                    <div class="circle-progress__progress circle-progress__progress_50"></div>
                    <div class="circle-progress__progress circle-progress__progress_60"></div>
                    <div class="circle-progress__progress circle-progress__progress_70"></div>
                    <div class="circle-progress__progress circle-progress__progress_80"></div>
                    <div class="circle-progress__progress circle-progress__progress_90"></div>
                    <div class="circle-progress__progress circle-progress__progress_100"></div>
                    <div class="circle-progress__value"><?=$model->general_progress_percent?></div>
                    <div class="circle-progress__percent">%</div>
                </div>
                <div class="circle-progress__label">
                    <?=$model->general_progress_title?>
                </div>
            </div>

        </div>
        <?php endif; ?>
    </div>
</div>
