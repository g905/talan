<?php
/**
 * @var $this yii\web\View
 * @var $videoMp4 string
 * @var $poster string
 * @var $title string
 * @var $presentationFile string
 */
?>

<section class="poster poster_padding">
    <video class="poster__bg poster__bg_video" autoplay loop muted data-video-src="<?= $videoMp4 ?>" data-poster-video data-autoplay data-keepplaying></video>
    <div class="poster__bg poster__bg_photo" data-bg-src="<?= $poster ?>" data-bg-pos="center" data-bg-size="cover"></div>
    <div class="poster__cover"></div>
    <div class="poster__inner">
        <div class="poster__content">
            <h1 class="h1 poster__h1 wow fadeInUp"><?= $title ?></h1>
            <?php if ($presentationFile) : ?>
            <a href="<?= $presentationFile ?>" class="button button_transparent poster__button wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                <?= t('Download presentation', 'complex-list') ?>
                <div class="button__bg"></div>
                <span class="button__blip button__blip_hover"></span>
                <span class="button__blip button__blip_click"></span>
            </a>
            <?php endif ?>
        </div>
    </div>
    <div class="down poster__down wow down-to-top">
        <button class="down__button" data-scroll-down></button>
        <div class="down__label"><?= t('Down', 'complex-list') ?></div>
    </div>
</section>
