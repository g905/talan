<?php

namespace frontend\modules\apartment\widgets\complexList;

use common\helpers\Property;
use common\models\ComplexList;
use yii\base\Widget;

/**
 * Class ComplexListPoster
 *
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package frontend\modules\apartment\widgets\complexList
 */
class ComplexListPoster extends Widget
{
    /** @var ComplexList */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model->type === Property::TYPE_COMMERCIAL) {
            $output = $this->render('complex-list-poster', [
                'title' => $this->model->top_title,
                'videoMp4' => $this->model->getVideoMp4Src(),
                'poster' => $this->model->getPosterSrc(),
                'presentationFile' => $this->model->getPresentationFileSrc(),
            ]);
        }

        return $output;
    }
}
