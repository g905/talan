<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\apartment\widgets\complex;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

class ComplexAdvantagesWidget extends Widget
{

    public $complexPage;

    public function run()
    {
        if (!isset($this->complexPage)) {
            return null;
        }

        if (count($this->complexPage->advantages) == 0) {
            return null;
        }
        
        echo $this->render('complexAdvantagesWidgetView', [
            'model' => $this->complexPage
        ]);
    }


}
