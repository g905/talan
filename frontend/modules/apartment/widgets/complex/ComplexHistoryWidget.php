<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.05.18
 * Time: 14:56
 */

namespace frontend\modules\apartment\widgets\complex;


use common\models\ApartmentComplex;
use yii\base\Widget;

class ComplexHistoryWidget extends Widget
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var ApartmentComplex
     */
    public $model;

    /**
     * @return string|null
     */
    public function run()
    {
        if (empty($this->model->build_start_date) || empty($this->model->build_end_date)) {
            return null;
        }

        return $this->render('complexHistoryWidgetView', [
            'title' => $this->title,
            'model' => $this->model,
        ]);
    }
}