<?php
namespace frontend\modules\apartment\widgets\complex;

use yii\base\Widget;

class TlNewComplexAdvantagesWidget extends Widget {

	public $complexPage;

	public function run()
	{
		if (!isset($this->complexPage)) {
			return null;
		}

		if (count($this->complexPage->advantages) == 0) {
			return null;
		}

		echo $this->render('tlNewComplexAdvantagesView', [
			'model' => $this->complexPage
		]);
	}
}