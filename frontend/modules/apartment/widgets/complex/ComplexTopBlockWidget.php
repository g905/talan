<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\apartment\widgets\complex;

use yii\base\Widget;

class ComplexTopBlockWidget extends Widget
{

    public $complexPage;

    public function run()
    {
        if (!isset($this->complexPage)) {
            return null;
        }

        echo $this->render('complexTopBlockWidgetView', [
            'model' => $this->complexPage
        ]);
    }
}
