<?php

namespace frontend\modules\apartment\widgets\complex;

use common\models\AcMarker;
use frontend\helpers\FrontendHelper;
use yii\base\Widget;
use yii\helpers\Json;

/**
 * Class ComplexMapWidget
 *
 * @author art
 * @package frontend\modules\apartment\widgets\complex
 */
class ComplexMapWidget extends Widget
{
    /**
     * @var string main pin icon path.
     */
    const MAIN_ICON_PATH = '/static/img/map/main_pin.png';
    /**
     * @var \common\models\ApartmentComplex complex model.
     */
    public $complexPage;
    /**
     * @var string marker template with replaceable tokens.
     */
    public $markerTemplate = <<<HTML
<div class="infowindow">
    <h5 class="h5 infowindow__h5">{label}</h5>
    <div class="infowindow__divider"></div>
    <a href="tel:{phoneFormatted}" class="infowindow__tel">{phone}</a>
    <div class="text infowindow__text">{address}<br>
        <a href="mailto:{email}">{email}</a>
    </div>
</div>
HTML;

    public function run()
    {
        if (!isset($this->complexPage)) {
            return null;
        }

        return $this->render('complexMapWidgetView', [
            'model' => $this->complexPage,
            'markerTypes' => $this->complexPage->getMarkerTypes(),
            'mapOptions' => $this->generateMapOptions(),
        ]);
    }

    private function generateMapOptions(): string
    {
        $complex = $this->complexPage;
        $markers = $complex->markers;
        $map = [
            'pos' => ['lat' => $complex->lat, 'lng' => $complex->lng],
            'zoom' => $complex->map_zoom,
            'mainMarker' => ['pos' => ['lat' => $complex->lat, 'lng' => $complex->lng], 'icon' => self::MAIN_ICON_PATH],
            'markers' => [],
        ];

        if ($markers) {
            foreach ($markers as $marker) {
                $map['markers'][] = $this->prepareMarkerOptions($marker);
            }
        }

        return Json::encode($map, JSON_NUMERIC_CHECK);
    }

    private function prepareMarkerOptions(AcMarker $marker): array
    {
        return [
            'type' => $marker->ac_marker_type_id,
            'pos' => ['lat' => $marker->lat, 'lng' => $marker->lng],
            'icon' => AcMarker::MARKER_TYPE_PATH . obtain($marker->ac_marker_type_id, AcMarker::getMarkerTypeMapIco()),
            'html' => strtr($this->markerTemplate, [
                '{label}' => $marker->label,
                '{email}' => $marker->email,
                '{phone}' => $marker->phone,
                '{address}' => $marker->address,
                '{phoneFormatted}' => FrontendHelper::formatPhone($marker->phone),
            ]),
        ];
    }
}
