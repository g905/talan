<?php

namespace frontend\modules\apartment\widgets\complex;

use yii\base\Widget;
use common\models\ApartmentComplex;

/**
 * Class ComplexBuildingWidget
 *
 * @package frontend\modules\apartment\widgets\complex
 */
class ComplexBuildingWidget extends Widget
{
    /** @var ApartmentComplex */
    public $complex;

    public function run()
    {
        $output = '';

        if ($this->complex) {
            $buildings = $this->complex->buildings;
            if ($buildings) {
                $output = $this->render('complexBuildingsWidgetView', [
                    'complex' => $this->complex,
                    'buildings' => $buildings
                ]);
            }
        }

        return $output;
    }
}
