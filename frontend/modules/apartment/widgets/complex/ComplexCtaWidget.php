<?php

namespace frontend\modules\apartment\widgets\complex;

use yii\base\Widget;
use common\models\ApartmentComplex;

/**
 * Class ComplexBuildingWidget
 *
 * @package frontend\modules\apartment\widgets\complex
 */
class ComplexCtaWidget extends Widget
{
    /** @var ApartmentComplex */
    public $complexPage;

    public function run()
    {
        if (isset($this->complexPage)) {
            if($this->complexPage->cta_published) {
                return $this->render('complexCtaWidgetView', [
                    'model' => $this->complexPage
                ]);
            }
        }

        return false;
    }
}
