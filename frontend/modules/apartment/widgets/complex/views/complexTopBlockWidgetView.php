<?php

use common\helpers\SiteUrlHelper;
use frontend\helpers\ImgHelper;
use frontend\modules\page\widgets\PageSliderWidget;

/**
 * @author art
 * @var $this yii\web\View
 * @var $model common\models\ApartmentComplex
 */
$link = $model->btn_link ?: SiteUrlHelper::createApartmentComplexApartmentsListUrl(['alias' => $model->alias, 'type' => $model->type]);
$linkLabel = $model->btn_label ?: t('_choose apartment', 'apartment-complex');
?>

<section class="poster poster_padding">
    <?= PageSliderWidget::widget([
        'imagePath' => ImgHelper::getFileSrc($model, 'topScreenBackgroundImage'),
        'videoPath' => ImgHelper::getFileSrc($model, 'topScreenBackgroundVideo'),
    ]) ?>
    <div class="poster__inner">
        <div class="poster__content">
            <?php if ($model->show_label) : ?>
            <h1 class="h1 poster__h1 wow fadeInUp"><?= $model->label ?></h1>
            <?php endif ?>
            <?php if(isset($model->btn_label) && ($model->btn_label !== "")):?>
                <?php if ($model->is_sale) : ?>
                    <div class="poster__sale-block wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                        <span class="poster__sale-text"> продажа завершена </span>
                    </div>
                <?php else : ?>
                    <a href="<?= $link ?>" onclick="<?= $model->choose_apartment_onclick ?>" class="button button_transparent poster__button wow fadeInUp" data-wow-delay="0.3s">
                        <?= $linkLabel ?>
                        <div class="button__blip button__blip_hover"></div>
                        <div class="button__blip button__blip_click"></div>
                    </a>
                <?php endif; ?>
            <?php endif;?>
        </div>
    </div>
    <div class="down poster__down wow down-to-top">
        <button class="down__button" data-scroll-down></button>
        <div class="down__label"><?= t('_down', 'home') ?></div>
    </div>
</section>
