<?php

use frontend\helpers\ImgHelper;
use yii\helpers\Html;

/** @var \common\models\ApartmentComplex $model */
?>

<div class="new-quality">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 new-quality__h2 wow fadeInUp"><?= $model->advantages_title ?></h2>
        <div class="new-quality__list">
            <div class="card card_large new-quality__card">
                <div class="card__content">
                    <h3 class="h3 card__h3 wow fadeInLeft">
                        <span class="card_span"><?= $model->advantages[0]->title ?> </span>
                    </h3>
                    <div class="card__number wow fadeInLeft" data-wow-delay="0.3s"><?=$model->advantages[0]->getPositionFormatted()?></div>
                    <div class="card__divider wow fadeInLeft" data-wow-delay="0.3s"></div>
                    <div class="card__text wow fadeInLeft" data-advantage-content data-wow-delay="0.6s">
                        <?= Html::decode($model->advantages[0]->description) ?>
                    </div>
                    <div class="card__blur wow fadeInLeft" data-wow-delay="1.2s" data-blur-advantage>
                        <button class="card__btn" data-btn-advantage>Развернуть</button>
                    </div>
                </div>
                <div class="card__img_desk wow fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                    <img class="card__img" src="<?= ImgHelper::getImageSrc($model->advantages[0], 'complex', 'advantagesTop', 'photo') ?>" alt="">
                </div>
            </div>
            <?php for ($i=1; $i<count($model->advantages); $i++): ?>
                <div class="card new-quality__card wow fadeInUp">
                    <div class="card__img_desk">
                        <img class="card__img" src="<?= ImgHelper::getImageSrc($model->advantages[$i], 'complex', 'advantages', 'photo') ?>" alt="">
                    </div>
                    <div class="card__content">
                        <h3 class="h3 card__h3"><span class="card_span"><?= $model->advantages[$i]->title ?></span></h3>
                        <div class="card__number"><?= $model->advantages[$i]->getPositionFormatted() ?></div>
                        <div class="card__divider"></div>
                        <div class="card__text" data-advantage-content><?= Html::decode($model->advantages[$i]->description) ?></div>
                        <div class="card__blur wow fadeInLeft" data-wow-delay="1.2s" data-blur-advantage>
                            <button class="card__btn" data-btn-advantage>Развернуть</button>
                        </div>
                    </div>
                    <div class="card__img card__img_mob" data-bg-src="<?= ImgHelper::getImageSrc($model->advantages[$i], 'complex', 'advantages', 'photo') ?>" data-bg-pos="center" data-bg-size="cover"></div>
                </div>
            <?php endfor; ?>
        </div>
    </div>
</div>
