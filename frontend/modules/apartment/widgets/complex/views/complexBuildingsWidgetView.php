<?php
/**
 * @var $this yii\web\View
 * @var $buildings common\models\ApartmentBuilding[]
 * @var $complex common\models\ApartmentComplex
 */
?>

<?php if ($buildings) : ?>
    <div class="wrap wrap_mobile_full">
        <div class="apartment-head catalogue-head">
            <h2 class="h2 apartment-head__h2 wow fadeInLeft"><?= t('Buildings', 'complex') ?></h2>
        </div>
    </div>
    <section class="complecs">
        <div class="wrap wrap_mobile_full">
            <div class="complecs__list">
                <?php foreach ($buildings as $building) : ?>
                    <a href="<?= $building->getViewUrl($complex->alias) ?>" class="card complecs__card wow fadeInUp">
                    <div class="card__img" data-bg-src="<?= $building->getGeneralPhotoSrc() ?>" data-bg-pos="center" data-bg-size="cover">
                        <?php if ($building->general_progress_percent) : ?>
                        <div class="diagram apartment-head__diagram">
                            <div class="circle-progress" data-circle-progress="<?= $building->general_progress_percent ?>">
                                <div class="circle-progress__outer">
                                    <div class="circle-progress__progress circle-progress__progress_10"></div>
                                    <div class="circle-progress__progress circle-progress__progress_20"></div>
                                    <div class="circle-progress__progress circle-progress__progress_30"></div>
                                    <div class="circle-progress__progress circle-progress__progress_40"></div>
                                    <div class="circle-progress__progress circle-progress__progress_50"></div>
                                    <div class="circle-progress__progress circle-progress__progress_60"></div>
                                    <div class="circle-progress__progress circle-progress__progress_70"></div>
                                    <div class="circle-progress__progress circle-progress__progress_80"></div>
                                    <div class="circle-progress__progress circle-progress__progress_90"></div>
                                    <div class="circle-progress__progress circle-progress__progress_100"></div>
                                    <div class="circle-progress__value"><?= $building->general_progress_percent ?></div>
                                    <div class="circle-progress__percent">%</div>
                                </div>
                                <div class="circle-progress__label"><?= $building->general_progress_title ?></div>
                            </div>
                        </div>
                        <?php endif ?>
                    </div>
                    <h5 class="h5 card__h5"><?= $building->label ?></h5>
                </a>
                <?php endforeach ?>
                <div class="divider_ajax">
                    <div class="line"></div>
                    <!--<button class="footer__button" data-list-show="gk">
                        <span class="footer__button-hide-text">все проекты</span>
                        <span class="footer__button-show-text">скрыть</span>
                    </button>-->
                </div>
            </div>
        </div>
    </section>
<?php endif ?>
