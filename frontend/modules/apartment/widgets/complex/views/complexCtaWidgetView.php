<section class="take_apartments">
    <div class="wrap wrap_mobile_full take_apartments__wrap">
        <h2 class="take_apartments__h2 wow fadeInLeft"> <?= $model->cta_black_label ?>
            <span> <?= $model->cta_white_label ?> </span>
        </h2>
        <div class="take_apartments__content">
            <p class="take_apartments__content-text wow fadeInRight"><?= $model->cta_description ?></p>
            <a href="<?= $model->cta_button_link ?>" class="button form__button wow fadeInUp"> <?= $model->cta_button_label ?>
                <div class="button__blip button__blip_hover"></div>
                <div class="button__blip button__blip_click"></div>
            </a>
        </div>
    </div>
</section>