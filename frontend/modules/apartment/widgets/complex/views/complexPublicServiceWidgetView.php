<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 24.12.2017
 * Time: 23:24
 */

use frontend\helpers\ImgHelper;

/** @var \common\models\ApartmentComplex $model */
/** @var \common\models\AcPublicService $service */
?>
<div class="accomplishment">

    <div class="wrap wrap_mobile_full">
        <h2 class="h2 accomplishment__h2 wow fadeInLeft">
            <?= $model->public_service_title?>
        </h2>
    </div>

    <div class="tabs accomplishment__tabs wow fadeInUp" data-tabs>
        <div class="tabs__head tabs__head_desk">
            <div class="wrap wrap_mobile_full">
                <?php foreach ($model->publicServices as $service): ?>
                <button class="tabs__btn" data-open-tab="<?=$service->id?>"><?=$service->label?></button>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="wrap wrap_mobile_full">
            <div class="custom-select custom-select_accomplishment tabs__custom-select" data-custom-select='{"tabs":true}'>
                <select name="subject">
                    <?php foreach ($model->publicServices as $service): ?>
                        <option value="<?=$service->id?>"><?=$service->label?></option>
                    <?php endforeach; ?>
                    </select>
            </div>
        </div>


        <div class="tabs__body">

            <?php foreach ($model->publicServices as $service): ?>
            <div class="tabs__tab" data-tab="<?=$service->id?>">
                <div class="accomplishment__content">
                    <div class="accomplishment__bg" data-bg-src="<?= ImgHelper::getImageSrc($service, 'complex', 'publicService', 'photo')?>" data-bg-size="cover" data-bg-pos="center"></div>
                    <div class="wrap wrap_mobile_full">
                        <div class="text accomplishment__text">
                            <p>
                                <?=$service->content?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>

</div>
