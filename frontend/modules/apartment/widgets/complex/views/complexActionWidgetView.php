<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 25.12.2017
 * Time: 0:11
 */

use common\helpers\SiteUrlHelper;
use frontend\helpers\ImgHelper;

/** @var \common\models\ApartmentComplex $model */
/** @var \common\models\PromoAction $action */
?>

<section class="shares">
    <div class="wrap wrap_mobile_full shares__wrap">
        <h2 class="h2 shares__h2 wow fadeInLeft">
            <?=Yii::t('front/apartment-complex', '_Actions')?>
        </h2>

        <div class="shares__list show_all_actions" id="about-good__block-desc_chars4">
<div class="props_phone" id="propsPhone4"  data-shares-slider='{"slidesToShow": 1, "slidesToScroll": 1, "dots": false, "prevArrow": ".shares .navigation__btn_prev", "nextArrow": ".shares .navigation__btn_next"}'>


                    <?php $delay=0; foreach ($model->promoActions as $action): ?>
                    <a class="card shares__card wow fadeIn"  data-wow-delay="<?=$delay?>s" href="<?= SiteUrlHelper::createPromoActionViewUrl(['id' => $action->id])?>">
                        <div class="card__img">
                            <div class="card__background" data-bg-src="<?= ImgHelper::getImageSrc($action, 'complex', 'action', 'image')?>" data-bg-size="contain" data-bg-pos="center" data-all-djs-photo="0"></div>
                        </div>
                        <h5 class="card__title">
                            <?=$action->label?>
                        </h5>
                    </a>
                    <?php $delay+=0.3; endforeach; ?>
            </div>

        </div>
<!--        <div class="card__blur wow fadeInLeft customSvBtn" data-wow-delay="1.2s" data-blur-advantageRR>-->
<!--            <button class="card__btn js-btn-more" data-btn-advantagerr>Смотреть все акции</button>-->
<!--        </div>-->


        <div class="navigation shares__navigation wow fadeInUp">
            <div class="wrap wrap_mobile_full">
                <div class="navigation__line"></div>
                <div class="navigation__content">
                    <button class="navigation__btn navigation__btn_prev" data-testimonials-arrow-prev>
                        <div class="arrow arrow_left navigation__arrow"></div>
                    </button>
                    <div class="navigation__status">
                        <span class="navigation__current" data-shares-status-current>..</span>\<span class="navigation__total" data-shares-status-total>..</span>
                    </div>
                    <button class="navigation__btn navigation__btn_next" data-testimonials-arrow-next>
                        <div class="arrow arrow_right navigation__arrow"></div>
                    </button>
                </div>
            </div>
        </div>


        <!--<div class="view-all">
            <div class="view-all__line"></div> 
            <a href="#" class="more view-all__more">
                все акции
            </a>
        </div>-->

        <div class="view-all view-all_full customSvBtnL">
            <div class="view-all__line"></div>
            <a href="#" class="more view-all__more">
                <?=Yii::t('front/apartment-complex', '_all promo actions')?>
            </a>
        </div>

    </div>
</section>
