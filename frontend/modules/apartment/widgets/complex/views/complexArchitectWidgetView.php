<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 24.12.2017
 * Time: 23:58
 */

use frontend\helpers\ImgHelper;

/** @var \common\models\ApartmentComplex $model */
?>
<div class="architect">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 architect__h2 wow fadeInLeft">
            <?=$model->architecture_title ?>
        </h2>

        <div class="card architect__card">
            <div class="card__content" data-architect-content>
                <h5 class="h5 card__h5 wow fadeInLeft">
                    <?=$model->architecture_fio ?>
                </h5>
                <div class="card__label fadeInLeft" data-wow-delay="0.3s"><?=$model->architecture_job_position ?></div>
                <div class="card__divider wow fadeInLeft" data-wow-delay="0.6s"></div>
                <div class="card__text wow fadeInLeft" data-wow-delay="0.9s">
                    <?=$model->architecture_description ?>
                </div>

                <?php if (mb_strlen($model->architecture_description) > 500): ?>
                <div class="card__blur wow fadeInLeft" data-wow-delay="1.2s" data-blur-architect>
                    <button class="card__btn" data-btn-architect> <?=Yii::t('front/apartment-complex', '_expand')?></button>
                </div>
                <?php endif; ?>
            </div>
            <div class="card__img wow fadeIn"  data-wow-delay="0.5s" data-wow-duration="2s" data-bg-src="<?= ImgHelper::getImageSrc($model,'complex', 'architecture', 'architecturePhoto')?>" data-bg-pos="center" data-bg-size="cover"></div>
        </div>

    </div>
</div>

<div class="image-block wow fadeInUp">
    <div class="wrap wrap_mobile_full">
        <img src="<?= ImgHelper::getImageSrc($model,'complex', 'architectureBottom', 'architectureBottomPhoto')?>" class="image-block__image" alt="">
    </div>
</div>