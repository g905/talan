<?php

use common\models\AcMarker;
use common\models\ApartmentComplex;
use frontend\helpers\ImgHelper;

/**
 * @var $model ApartmentComplex
 * @var $marker AcMarker
 * @var $markerTypes array
 * @var $mapOptions string
 */
/* 1424x550 */
?>

<?php $map_image=ImgHelper::getFileSrc($model, 'mapImage');?>

<div class="map">
    <div class="wrap">
        <h2 class="h2 map__h2 wow fadeInUp"><?= $model->map_title ?></h2>
    </div>
    <?php if(isset($map_image) && ($map_image != "")):?>
    	<div id="map" class="map__map wow fadeIn" data-map-options='<?= $mapOptions ?>' style="background-image: url('<?= $map_image; ?>');background-size:contain;background-repeat:no-repeat; background-color: white"></div>
    <?php else:?>
    	<div id="map" class="map__map wow fadeIn" data-map-options='<?= $mapOptions ?>'></div>
    <?php endif;?>
    <?php if (count($markerTypes) > 0) : ?>
        <div class="map__content">
            <div class="wrap wrap_mobile_full">
                <div class="infrastructure-objects map__infrastructure-objects wow fadeIn">
                    <h5 class="h5 infrastructure-objects__h5"><?= t('_Infrastructure objects', 'apartment-complex') ?></h5>
                    <div class="infrastructure-objects__list">
                        <?php foreach ($markerTypes as $markerType) : ?>
                            <button class="card infrastructure-objects__card card_active" data-infrastructure-object-type="<?= $markerType ?>">
                                <span class="card__icon">
                                    <svg class="card__svg">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= obtain($markerType, AcMarker::getMarkerTypeSvgClass()) ?>"></use>
                                    </svg>
                                </span>
                                <span class="card__text"><?= obtain($markerType, AcMarker::getMarkerTypeDictionary()) ?></span>
                            </button>
                        <?php endforeach ?>
                    </div>
                </div>
                <div class="map__description wow fadeIn" data-wow-delay="0.3s">
                    <h5 class="h5 map__h5"><?= $model->map_subtitle ?></h5>
                    <div class="map__content-text" style="overflow: hidden;" data-advantage-content>
                        <div class="text map__text"><?= $model->map_description ?></div>
                        <div class="card__blur wow fadeInLeft" data-wow-delay="1.2s" data-blur-advantage>
                            <button class="card__btn" data-btn-advantage>Развернуть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>
