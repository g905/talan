<?php

use common\helpers\SiteUrlHelper;
use frontend\helpers\ImgHelper;
use frontend\helpers\FrontendHelper;

/**
 * @author art
 * @var $this yii\web\View
 * @var $model common\models\ApartmentComplex
 */
?>

<section class="readiness">
    <div class="wrap wrap_mobile_full wrap_large">
        <?php if (count($model->generalPhotos) != 0): ?>
            <div class="readiness__list wow fadeIn" data-wow-duration="2s" data-wow-delay="0.5s" data-readiness-slider data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "dots": false, "prevArrow": ".readiness .navigation__btn_prev", "nextArrow": ".readiness .navigation__btn_next"}'>
                <?php foreach ($model->sliderVideos as $video): ?>
                    <div class="card readiness__card">
                        <div class="video">
                            <div data-youtube="<?= FrontendHelper::extractYoutubeVideoId($video->title) ?>">
                                <iframe src="https://www.youtube.com/embed/<?= FrontendHelper::extractYoutubeVideoId($video->title) ?>?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen=""></iframe>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php foreach ($model->generalPhotos as $photoEtF): ?>
                    <div class="card readiness__card"
                         data-bg-src="<?= ImgHelper::getImageSrcFromEtF($photoEtF, 'complex', 'gallery') ?>"
                         data-bg-size="cover" data-bg-pos="center"></div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <div class="navigation readiness__navigation readiness__navigation_tab wow fadeInUp">
            <div class="wrap wrap_mobile_full">
                <div class="navigation__content">
                    <button class="navigation__btn navigation__btn_prev" data-readiness-arrow-prev>
                        <div class="arrow arrow_left navigation__arrow"></div>
                    </button>
                    <div class="navigation__status">
                        <span class="navigation__current" data-readiness-status-current>..</span>\<span class="navigation__total" data-readiness-status-total>..</span>
                    </div>
                    <button class="navigation__btn navigation__btn_next" data-readiness-arrow-next>
                        <div class="arrow arrow_right navigation__arrow"></div>
                    </button>
                </div>
                <div class="navigation__line"></div>
            </div>
        </div>

        <div class="readiness__content">
            <div class="readiness__wrapp">
                <?php if (!FrontendHelper::isNullOrEmpty($model->general_progress_percent)): ?>
                    <div class="readiness__diagram  wow fadeInRight">
                        <div class="circle-progress" data-circle-progress="<?= $model->general_progress_percent ?>">
                            <div class="circle-progress__outer">
                                <div class="circle-progress__progress circle-progress__progress_10"></div>
                                <div class="circle-progress__progress circle-progress__progress_20"></div>
                                <div class="circle-progress__progress circle-progress__progress_30"></div>
                                <div class="circle-progress__progress circle-progress__progress_40"></div>
                                <div class="circle-progress__progress circle-progress__progress_50"></div>
                                <div class="circle-progress__progress circle-progress__progress_60"></div>
                                <div class="circle-progress__progress circle-progress__progress_70"></div>
                                <div class="circle-progress__progress circle-progress__progress_80"></div>
                                <div class="circle-progress__progress circle-progress__progress_90"></div>
                                <div class="circle-progress__progress circle-progress__progress_100"></div>
                                <div class="circle-progress__value"><?= $model->general_progress_percent ?></div>
                                <div class="circle-progress__percent">%</div>
                            </div>
                            <div class="circle-progress__label"><?= $model->general_progress_title ?></div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (!FrontendHelper::isNullOrEmpty($model->general_description)): ?>
                    <div class="readiness__text wow fadeInRight" data-advantage-content data-wow-delay="0.3s">
                        <p><?= $model->general_description ?></p>
                    </div>
                <?php endif; ?>
                <div class="card__blur wow fadeInLeft" data-wow-delay="1.2s" data-blur-advantage>
                    <button class="card__btn" data-btn-advantage>Развернуть</button>
                </div>
            </div>

            <a href="<?= SiteUrlHelper::createApartmentComplexApartmentsListUrl(['alias' => $model->alias, 'type' => $model->type]) ?>" class="button button_green readiness__button">
                <?= t('_choose apartment', 'apartment-complex') ?>
                <span class="button__blip button__blip_hover"></span>
                <span class="button__blip button__blip_click"></span>
            </a>

            <?php if (($model->general_docs_link_title != '') && ($model->general_docs_link != '')): ?>
                <a href="<?= $model->general_docs_link ?>" class="more readiness__more  wow fadeInRight" data-wow-delay="0.6s">
                    <?= $model->general_docs_link_title ?>
                </a>
            <?php endif; ?>

            <?php if (($model->general_fb_link != '') || ($model->general_vk_link != '')): ?>
                <div class="readiness__subscribe wow fadeInRight" data-wow-delay="0.9s">
                    <p><?= Yii::t('front/apartment-complex', '_Subscribe') ?>:</p>
                    <?php if ($model->general_vk_link != ''): ?>
                        <a href="<?= $model->general_vk_link ?>" class="readiness__icon socicon-vkontakte" target="_blank"></a>
                    <?php endif; ?>

                    <?php if ($model->general_fb_link != ''): ?>
                        <a href="<?= $model->general_fb_link ?>" class="readiness__icon socicon-facebook" target="_blank"></a>
                    <?php endif; ?>

                    <?php if ($model->general_ig_link != ''): ?>
                        <a href="<?= $model->general_ig_link ?>" class="readiness__icon socicon-instagram" target="_blank"></a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>

            <?php if (count($model->generalPhotos) != 0): ?>
                <div class="navigation readiness__navigation readiness__navigation_desk">
                    <div class="wrap wrap_mobile_full">
                        <div class="navigation__content">
                            <button class="navigation__btn navigation__btn_prev" data-readiness-arrow-prev>
                                <div class="arrow arrow_left navigation__arrow"></div>
                            </button>
                            <div class="navigation__status">
                                <span class="navigation__current" data-readiness-status-current>..</span>\<span class="navigation__total" data-readiness-status-total>..</span>
                            </div>
                            <button class="navigation__btn navigation__btn_next" data-readiness-arrow-next>
                                <div class="arrow arrow_right navigation__arrow"></div>
                            </button>
                        </div>
                        <div class="navigation__line"></div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
</section>
