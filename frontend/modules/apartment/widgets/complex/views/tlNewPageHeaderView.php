<?php
use frontend\helpers\ImgHelper;
use common\helpers\SiteUrlHelper;
use \frontend\modules\apartment\assets\TlNewComplexPartAsset;

$photoCount = count($model->generalPhotos);
$link = $model->btn_link ?: SiteUrlHelper::createApartmentComplexApartmentsListUrl(['alias' => $model->alias, 'type' => $model->type]);
$linkLabel = $model->btn_label ?: t('_choose apartment', 'apartment-complex');
$specOfferBtnText = (!empty($model->special_offer_text) ? $model->special_offer_text : 'Получить спец. предложение');
?>
<section class="poster poster_padding">

	<?php if(is_array($model->generalPhotos) && count($model->generalPhotos)) : ?>
		<div class="slider-wr">
		<?php foreach($model->generalPhotos as $photoEtF): ?>
			<div class="slider-wr-item">
				<div class="bg-sz" style="background-image: url('<?= ImgHelper::getImageSrcFromEtF($photoEtF, 'complex', 'gallery'); ?>')"></div>
				<div class="poster__cover"></div>

				<div class="slider-wr-content">
					<?php if ($model->show_label) : ?>
						<div class="slider-wr-row">
							<div class="slider-wr--ttl wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
								<?= $model->label; ?>
							</div>
						</div>
					<?php endif;?>
					<?php if(isset($model->btn_label) && ($model->btn_label !== "")):?>
						<div class="slider-wr-row">
							<div class="slider-wr--descr">
								<?php
								if(!empty($model->special_offer_description)) {
									echo $model->special_offer_description;
								}
								?>
							</div>
							<?php // if(!empty($model->special_offer_link)):?>
							<button data-href="<?=SiteUrlHelper::createFormCallbackPopupUrl(['header' => '_Get special offer']) ?>"
                               class="slider-wr--button button button_transparent poster__button wow fadeInUp ajax-link"
							   data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
                                <?php /*
                                <a href="<?= $model->special_offer_link; ?>" onclick="" class="slider-wr--button button button_transparent poster__button wow fadeInUp"
							    data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">

                                */?>
									<?= $specOfferBtnText; ?>
								<div class="button__bg"></div>
								<div class="button__blip button__blip_hover"></div>
								<div class="button__blip button__blip_click"></div>
							</button>
							<?php //endif; ?>
							<?php if ($model->is_sale) : ?>
								<span class="poster__sale-text"> продажа завершена </span>
							<?php else : ?>
								<a href="<?= $link;?>" class="more slider-wr--vub  wow fadeInRight" data-wow-delay="0.6s"
								    style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInRight;">
									<?= $linkLabel;?>
								</a>
							<?php endif; ?>
						</div>
					<?php endif;?>
				</div>
			</div>
		<?php endforeach; ?>
		</div>

		<div class="wrap-nav">
			<div class="slider-nav">
				<?php for($ind1 = 0; $ind1 < count($model->generalPhotos); $ind1++): ?>
				<div class=""><div class="slider-nav-item"><?= $ind1 + 1;?></div></div>
				<?php endfor; ?>
			</div>
		</div>
	<?php endif; ?>

    <div class="down poster__down wow down-to-top">
        <button class="down__button" data-scroll-down></button>
        <div class="down__label"><?= t('_down', 'home') ?></div>
	</div>
</section>
<?php
TlNewComplexPartAsset::register($this);