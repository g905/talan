<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\apartment\widgets\complex;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

class ComplexActionWidget extends Widget
{

    public $complexPage;

    public function run()
    {
        if (!isset($this->complexPage)) {
            return null;
        }

        if (count($this->complexPage->promoActions) == 0) {
            return null;
        }

        echo $this->render('complexActionWidgetView', [
            'model' => $this->complexPage
        ]);
    }


}
