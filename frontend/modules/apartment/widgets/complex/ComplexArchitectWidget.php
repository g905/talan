<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\apartment\widgets\complex;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

class ComplexArchitectWidget extends Widget
{

    public $complexPage;

    public function run()
    {
        if (!isset($this->complexPage)) {
            return null;
        }

        if ((FrontendHelper::isNullOrEmpty($this->complexPage->architecture_fio)) &&
            (FrontendHelper::isNullOrEmpty($this->complexPage->architecture_description))) {
            return null;
        }

        echo $this->render('complexArchitectWidgetView', [
            'model' => $this->complexPage
        ]);
    }


}
