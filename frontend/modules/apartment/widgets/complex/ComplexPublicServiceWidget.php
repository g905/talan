<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\apartment\widgets\complex;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

class ComplexPublicServiceWidget extends Widget
{

    public $complexPage;

    public function run()
    {
        if (!isset($this->complexPage)) {
            return null;
        }

        if (count($this->complexPage->publicServices) == 0) {
            return null;
        }

        echo $this->render('complexPublicServiceWidgetView', [
            'model' => $this->complexPage
        ]);
    }


}
