<?php
namespace frontend\modules\apartment\widgets\complex;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

class TlNewPageHeader extends Widget {

	public $complexPage;

	public function run()
	{
		if (!isset($this->complexPage)) {
			return null;
		}

		if ((count($this->complexPage->generalPhotos) == 0) &&
			(FrontendHelper::isNullOrEmpty($this->complexPage->general_progress_percent)) &&
			(FrontendHelper::isNullOrEmpty($this->complexPage->general_description))) {
			return null;
		}

		echo $this->render('tlNewPageHeaderView', [
			'model' => $this->complexPage
		]);
	}
}