<?php

namespace frontend\modules\apartment\widgets\apartment;

use common\models\Apartment;
use yii\base\Widget;

/**
 * Class ApartmentConfiguratorTabsWidget
 *
 * @package frontend\modules\apartment\widgets\apartment
 */
class ApartmentConfiguratorTabsWidget extends Widget
{
    /**
     * @var Apartment
     */
    public $model;

    public function run()
    {
        return $this->render('apartmentConfiguratorTabsWidgetView', [
            'model' => $this->model
        ]);
    }
}