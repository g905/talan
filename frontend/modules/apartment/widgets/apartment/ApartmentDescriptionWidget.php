<?php

namespace frontend\modules\apartment\widgets\apartment;

use yii\base\Widget;
use frontend\helpers\ImgHelper;
use frontend\helpers\FrontendHelper;

/**
 * Class ApartmentDescriptionWidget
 *
 * @package frontend\modules\apartment\widgets\apartment
 * @author art
 */
class ApartmentDescriptionWidget extends Widget
{
    public $apartmentPage;

    public function run()
    {
        if (!isset($this->apartmentPage)) {
            return null;
        }

        if (FrontendHelper::isNullOrEmpty( $this->apartmentPage->description_text)){
            return null;
        }

        $image = ImgHelper::getImageSrc($this->apartmentPage, 'apartment', 'description', 'descriptionPhoto');

        if ($image && $this->apartmentPage->description_text) {
            return $this->render('apartmentDescriptionWidgetView',[
                'model' => $this->apartmentPage,
                'image' => $image
            ]);
        }

    }
}
