<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\apartment\widgets\apartment;

use common\models\ApartmentConstructor;
use yii\base\Widget;

class ApartmentConstructorWidget extends Widget
{

    public $model;

    public $complex;

    public $building;

    public function run()
    {
        $validate = ApartmentConstructor::validateRender();

        if ($validate && $this->model->isResidential()) {
            return $this->render('apartmentConstructorWidgetView',[
                'model' => $this->model,
                'complex' => $this->complex,
                'building' => $this->building
            ]);
        }
    }
}
