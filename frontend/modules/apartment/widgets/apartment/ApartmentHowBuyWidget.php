<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\apartment\widgets\apartment;

use yii\base\Widget;

class ApartmentHowBuyWidget extends Widget
{

    public $apartmentPage;

    public function run()
    {
        if (!isset($this->apartmentPage)) {
            return null;
        }

        $howBuyBlocks = $this->apartmentPage->getHowBuyBlocks();

        if ((!isset($howBuyBlocks)) || (count($howBuyBlocks) == 0)){
            return null;
        }

        echo $this->render('apartmentHowBuyWidgetView',[
            'howBuyBlocks' => $howBuyBlocks
        ]);
    }
}
