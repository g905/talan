<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\apartment\widgets\apartment;

use yii\base\Widget;
use common\models\City;

class ApartmentTopBlockWidget extends Widget
{
    public $apartmentPage;
    public function run()
    {
        if (!isset($this->apartmentPage)) {
            return null;
        }

        $city = City::getUserCity();

if (\Yii::$app->request->get('test'))         return $this->render('apartmentTopBlockWidgetView-test',[
    'model' => $this->apartmentPage,
    'city' => $city
]);


        return $this->render('apartmentTopBlockWidgetView',[
            'model' => $this->apartmentPage,
            'city' => $city
        ]);
    }
}
