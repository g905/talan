<?php

namespace frontend\modules\apartment\widgets\apartment;

use common\models\Apartment;
use yii\base\Widget;

/**
 * Class ApartmentInvestCalculatorWidget
 *
 * @package frontend\modules\apartment\widgets\apartment
 */
class ApartmentInvestCalculatorWidget extends Widget
{
    /**
     * @var Apartment
     */
    public $model;

    public function run()
    {
        $output = '';

        $complex = $this->model->apartmentComplex;

        if ($this->model->isCommercial() && $complex->show_calc) {
            $output = $this->render('apartmentInvestCalculatorWidgetView', [
                'model' => $this->model
            ]);
        }

        return $output;
    }
}