<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\apartment\widgets\apartment;

use yii\base\Widget;

class ApartmentTourWidget extends Widget
{
    public $model;

    public function run()
    {
        if ($this->model) {
            if ($this->model->isResidential()) {
                $rooms = 'room_'.$this->model->rooms_number;

                $url = $this->model->apartmentComplex->$rooms;

                if ($url) {
                    return $this->render('apartmentTourWidgetView',[
                        'url'=> $url
                    ]);
                }

                return false;
            }
        }

        return false;
    }
}
