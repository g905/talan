<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\apartment\widgets\apartment;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

class ApartmentFloorPlanWidget extends Widget
{

    public $apartmentPage;
    public function run()
    {
        if (!isset($this->apartmentPage)) {
            return null;
        }

        if ($this->apartmentPage->floorPlanPhoto == null){
            return null;
        }

        echo $this->render('apartmentFloorPlanWidgetView',[
            'model' => $this->apartmentPage
        ]);
    }
}
