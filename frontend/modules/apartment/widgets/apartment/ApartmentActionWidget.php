<?php

namespace frontend\modules\apartment\widgets\apartment;

use common\models\Apartment;
use yii\base\Widget;

class ApartmentActionWidget extends Widget
{
    /** @var Apartment */
    public $model;

    public function run()
    {
        $output = '';

        if (!isset($this->model)) {
            return null;
        }

        $promoActions = optional($this->model->apartmentComplex)->promoActions;

        if ($promoActions) {
            $output = $this->render('apartmentActionWidgetView', [
                'promoActions' => $promoActions,
            ]);
        }

        return $output;
    }
}
