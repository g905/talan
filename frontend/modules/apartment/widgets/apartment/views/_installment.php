<?php
/**
 * Created by PhpStorm.
 * User: p-clerick
 * Date: 04.06.19
 * Time: 12:53
 */


use common\models\City;
use common\models\InstallmentPlan;
use common\helpers\SiteUrlHelper;

$city = City::getUserCity();
$model = InstallmentPlan::findByCityWithBenefitsAndStepsOrFail($city->id);

if ($model) {
    $formOnSubmit = $model->form_calc_onsubmit;
    $defaultTimeTerm = $model->default_time_term;
    $defaultInitialFee = $model->default_initial_fee;
    $defaultApartmentPrice = $model->default_apartment_price;
    $minTerm = $model->min_term;
    $maxTerm = $model->max_term;
} else {
    $formOnSubmit = '';
    $defaultTimeTerm = 0;
    $defaultInitialFee = 0;
    $defaultApartmentPrice = 0;
    $minTerm = 0;
    $maxTerm = 0;
}

?>

<section class="calc">
    <div class="title">
        <div class="wrap-tab">
            <h3 class="h3 " >Рассчитайте рассрочку</h3>
        </div>
    </div>
    <div class="calc__block ">
        <div class="wrap-tab">
            <form class="calc_form calc_form-installment clearfix">
                <fieldset class="calc__fieldset--left " >
                    <legend>Введите данные:</legend>
                    <div class="filters__section clearfix">
                        <div class="filters__heading">Стоимость квартиры:</div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-wnumb='{"decimals":"0", "thousand": " ", "postfix":" руб"}' data-range-min="500000" data-range-max="5000000" data-nouislider-type='price'>
                                <div class="ranges nouislider__ranges">
                                    <input class="ranges__input" placeholder="<?= $defaultApartmentPrice ?>" type="text" value="<?= $defaultApartmentPrice ?>" name="square-max" data-no-ui-input> </div>
                                <div class="nouislider__slider"></div>
                            </div>
                            <!-- /.filters__nouislider -->
                        </div>
                        <!-- /.filters__inner -->
                    </div>
                    <!-- /.filters__section -->
                    <div class="filters__section clearfix">
                        <div class="filters__heading">Сроки рассрочки:</div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-wnumb='{"decimals":"0", "postfix":" мес"}' data-range-min="<?= $minTerm ? $minTerm : '1' ?>" data-range-max="<?= $maxTerm ? $maxTerm : '12' ?>" data-nouislider-type='term'>
                                <div class="ranges nouislider__ranges">
                                    <input class="ranges__input" placeholder="6 мес" value="<?= $defaultTimeTerm ?>" type="text" name="square-max" data-no-ui-input> </div>
                                <div class="nouislider__slider"></div>
                            </div>
                            <!-- /.filters__nouislider -->
                        </div>
                        <!-- /.filters__inner -->
                    </div>
                    <!-- /.filters__section -->
                    <div class="filters__section clearfix">
                        <div class="filters__heading">
                            <span class="tabl-hidden">Первоначальный взнос:</span>
                            <span class="desk-hidden">Первонач. взнос:</span>
                        </div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-wnumb='{"decimals":"0", "postfix":"%"}' data-range-min="1" data-range-max="100" data-nouislider-type='first_value'>
                                <div class="ranges nouislider__ranges">
                                    <input class="ranges__input" placeholder="60%" value="<?= $defaultInitialFee ?>" type="text" name="square-max" data-no-ui-input> </div>
                                <div class="nouislider__slider"></div>
                            </div>
                            <!-- /.filters__nouislider -->
                        </div>
                        <!-- /.filters__inner -->
                    </div>
                    <!-- /.filters__section -->
                </fieldset>
                <!-- /.calc__fieldset-left -->
                <div class="calc__fieldset--right">
                    <div class="calc__calculated-block " >
                        <div class="calc__calculated--row">
                            <div class="calc__month-payment">Ежемесячный платеж:</div>
                            <span class="calc__month-payment--price2" data-wnumb='{"decimals": 0, "thousand":" ", "postfix":" р"}'>225 000 р</span>
                            <!-- <span class="calc__letter big-letter">&nbsp;р</span> -->
                        </div>
                        <!-- /.calc__month-payment -->
                        <!-- <div class="calc__calculated--row">
                      <div class="calc__last-payment">Последний платеж:</div>
                      <span class="last-payment--price">525 000</span>
                      <span class="calc__letter">&nbsp;р</span>
                    </div>-->
                        <!-- /.calc__month-payment -->
                        <button type="button" class="send-request ajax-link" data-href="<?= SiteUrlHelper::createFormInstalmentPlanPopupUrl()?>">
                            <span class="tabl-hidden">Отправить заявку на рассрочку</span>
                            <span class="desk-hidden">Отправить заявку</span>
                        </button>
                    </div>
                    <!-- /.calc__calculated-block -->
                </div>
                <!-- /.calc__fieldset--right -->
            </form>
            <!-- /.calc_form -->
        </div>
        <!-- /.wrap -->
    </div>
    <!-- /.calc__block -->
</section>
