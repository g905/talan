<?php

use common\components\CommonDataModel;
use frontend\helpers\FrontendHelper;
use common\helpers\SiteUrlHelper;
use frontend\helpers\ImgHelper;
use common\models\Apartment;

/**
 * @author dimarychek
 *
 * @var $this \yii\web\View
 * @var $model Apartment
 * @var $related object
 * @var $city object
 */

?>

<?php if ($related) : ?>
    <section class="apartaments apartaments_related">
        <div class="wrap wrap_mobile_full">
            <h2 class="h2 apartaments__h2 wow fadeInLeft"> Похожие квартиры </h2>
            <div class="apartaments__list">

                <?php foreach ($related[0] as $flat) : ?>
                    <a class="card apartaments__card wow fadeInUp" href="<?= $flat->id ?>">
                        <div class="card__img">
                            <div class="card__bg" data-bg-src="<?= ImgHelper::getImageSrc($flat, 'complex', 'apartmentList', 'photo', Apartment::NO_IMAGE_PATH) ?>" data-bg-pos="center" data-bg-size="contain"></div>
                        </div>
                        <h5 class="h5 card__h5"> <?= $flat->label ?> </h5>
                        <p> <?= Yii::$app->formatter->asDate($flat->delivery_date, 'php:d-m-Y') ?> ?> </p>
                        <div class="divider card__divider"></div>
                        <div class="card__text">
                            <?php if ($city->show_prices || FrontendHelper::isNullOrEmpty($flat->price_from)) : ?>
                                <p><?= t('_from', 'apartment') . ' ' . app()->formatter->asInteger($flat->price_from) . ' ' . CommonDataModel::getCurrencyText() ?></p>
                            <?php endif ?>

                            <p><?= $flat->total_area . ' ' . CommonDataModel::getAreaUnit() ?></p>
                        </div>
                    </a>
                <?php endforeach; ?>

            </div>
            <div class="view-all view-all_apartaments_related-apartments wow fadeInUp">
                <div class="view-all__line"></div>
                <a href="<?= SiteUrlHelper::createApartmentComplexApartmentsListUrl(['alias' => $model->apartmentComplex->alias, 'type' => $model->apartmentComplex->type]) ?>" class="more view-all__more"> все квартиры </a>
            </div>
        </div>
    </section>
<?php endif; ?>