<?php
/**
 * Created by PhpStorm.
 * User: p-clerick
 * Date: 04.06.19
 * Time: 14:40
 */


use common\models\City;
use common\models\BuyPage;
use common\helpers\SiteUrlHelper;
use frontend\modules\form\models\FormSecondary;
use yii\bootstrap\ActiveForm;

$city = City::getUserCity();

$model = BuyPage::findByCity($city);

$formModel = new FormSecondary();

$url = $_SERVER['REQUEST_URI'];
$array = explode("/",$url);
$last_item_index = count($array) - 1;
$ap_id =  $array[$last_item_index];

?>

<section class="calc vtor-calc">
    <div class="title">
        <div class="wrap wrap_mobile_full">
            <h3 class="h3"><?= $model->form_top_title ?></h3>
        </div>
    </div>
    <div class="calc__block calc__block_v2">
        <div class="wrap wrap_mobile_full">

            <?php $form = ActiveForm::begin([
                'action' => SiteUrlHelper::createFormSecondaryUrl(),
                'options' => [
                    'id' => 'secondary-form',
                    'class' => 'calc_form ajax-form clearfix',
                    'data-onsubmit' => $model->form_top_onsubmit ? : config()->get('request_excursion_onsubmit', null),
                ], // important
                'enableAjaxValidation' => true,
                'validationUrl' => SiteUrlHelper::createFormSecondaryValidateUrl(),
                'fieldConfig' => ['options' => ['class' => 'form__row']],
            ]) ?>

            <div class="calc__fieldset--left">
                <legend><?= Yii::t('forms', 'Enter the data') ?>:</legend>
                <div class="filters__section clearfix">
                    <div class="filters__heading"><?= Yii::t('forms', 'Square') ?>:</div>
                    <div class="filters__inner">
                        <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-wnumb='{"decimals":"0", "thousand": " ", "postfix":" м²"}' data-range-min="15" data-range-max="200" data-nouislider-type='area'>
                            <div class="ranges nouislider__ranges">
                                <input class="ranges__input" placeholder="100 м²" type="text" value="<?= $model->form_top_square ?>" name="FormSecondary[square]" data-no-ui-input> </div>
                            <div class="nouislider__slider"></div>
                        </div>
                        <!-- /.filters__nouislider -->
                    </div>
                    <!-- /.filters__inner -->
                </div>
                <!-- /.filters__section -->
                <div class="calc__form-row clearfix">
                    <label class="field form__field">
                        <span class="calc__form-span"><?= Yii::t('forms', 'Area') ?></span>
                        <input type="text" class="input form__input calc__form-input" name="FormSecondary[address]" placeholder="<?= Yii::t('forms', 'Address') ?>"> </label>
                </div>
                <div class="filters__section">
                    <div class="filters__heading filters__heading_side_left"><?= Yii::t('forms', 'Ownership') ?>:</div>
                    <div class="filters__inner filters__inner_side_right">
                        <div class="custom-select filters__custom-select" data-custom-select>
                            <select name="FormSecondary[age]">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="filters__section checkbox-section">
                    <div class="filters__inner filters__inner_side_left calc__checkbox-row">
                        <label class="checkbox form__checkbox form__checkbox--top">
                            <input class="checkbox__checkbox" type="checkbox" name="FormSecondary[redevelopment]" value="1">
                            <span class="checkbox__mask"></span>
                            <span class="checkbox__label"><?= Yii::t('forms', 'Redevelopment') ?></span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="calc__fieldset--right " >
                <legend><?= Yii::t('forms', 'Leave request') ?>:</legend>
                <div class="calc__calculated-block calc__right-block_form clearfix">
                    <div class="form__row form__row_split_2">
                        <?= $form->field($formModel, 'name', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['class' => 'input form__input']]) ?>
                    </div>
                    <div class="form__row form__row_split_2">
                        <?= $form->field($formModel, 'phone', ['template' => '<label class="field form__field">{input}<span class="form__label">{labelTitle}</span>{error}</label>', 'inputOptions' => ['type' => 'tel','class' => 'input form__input input_phone']])->textInput([
                            'type' => 'tel'
                        ]) ?>
                    </div>

                    <input type="hidden" value="<?= $ap_id ?>" class="form__input" name="FormSecondary[apartment_id]"  aria-required="true">


                    <div class="form__row">
                        <div id="secondary-form-agree-block" class="field form__field">
                            <label class="checkbox form__checkbox">
                                <input type="checkbox" class="checkbox__checkbox" name="FormSecondary[iAgree]" data-validation-name="agree">
                                <span class="checkbox__mask"></span>
                                <span class="checkbox__label"><?= obtain('agreementCheckboxText', app()->params, '') ?></span>
                            </label>
                        </div>
                    </div>
                    <div class="form__row">
                        <button type="submit" class="button form__button " >
                            <?php if ($model->form_top_btn) : ?>
                                <?= $model->form_top_btn ?>
                            <?php else : ?>
                                <?= Yii::t('forms', 'Send') ?>
                            <?php endif; ?>
                            <span class="button__blip button__blip_hover"></span>
                            <span class="button__blip button__blip_click"></span>
                        </button>
                    </div>
                </div>
            </div>

            <?php $form::end() ?>
        </div>
        <!-- /.wrap -->
    </div>
    <!-- /.calc__block -->
</section>
