<?php

use common\helpers\SiteUrlHelper;

?>

<section id="constructor_anchor" class="constructor_path wrap">
    <?php if ($complex->buildings && $building) : ?>
        <div class="choose-house__lists">
            <a href="<?= SiteUrlHelper::createApartmentComplexConfiguratorUrl(['alias' => $complex->alias, 'type' => $complex->type]) ?>" class="choose-house__items ">Выбор дома</a>
            <a href="<?= SiteUrlHelper::createApartmentComplexPlanUrl(['alias' => $complex->alias, 'id' => $building->id]) ?>" class="choose-house__items">Выбор квартиры</a>
            <a href="#" class="choose-house__items choose-house__items--active">Выбор отделки</a>
        </div>
    <?php endif; ?>
</section>

<section data-section="vue-constructor">
    <input id="apartment_id" type="hidden" value="<?= $model->id ?>">
    <div id="vueApp">
        <the-constructor/>
    </div>
</section>