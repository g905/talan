<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 25.12.2017
 * Time: 21:09
 */

/** @var \common\models\Apartment $model */
?>
<div class="floor-plan">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 floor-plan__h2 wow fadeInUp">
            <?= $model->floor_plan_title?>
        </h2>
    </div>
    <div class="wrap wrap_large wow fadeIn">
        <div class="floor-plan__img" data-bg-src="<?=\frontend\helpers\ImgHelper::getImageSrc($model, 'apartment', 'floorPlan', 'floorPlanPhoto')?>" data-bg-pos="center" data-bg-size="contain"></div>
    </div>
</div>
