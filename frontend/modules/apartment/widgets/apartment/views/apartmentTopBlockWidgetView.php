<?php

use common\models\Apartment;
use common\helpers\SiteUrlHelper;
use common\components\CommonDataModel;
use common\models\City;
use frontend\helpers\ImgHelper;
use frontend\helpers\FrontendHelper;
use frontend\components\ViewCountService;

/**
 * @author art
 * @var $this yii\web\View
 * @var $model Apartment
 * @var $city common\models\City
 */

$views = Yii::$app->session->get('viewCountObject') ? Yii::$app->session->get('viewCountObject')->getViews() : 0;
$city = City::getUserCity();
// уникальный идентификатор города, который сходится на разных серверах
$sxgeo_city_id = null;
$apComplex = $model->apartmentComplex;

if(!empty($apComplex)) {
	$apartmentCityObj = $apComplex->city;

	if(!empty($apartmentCityObj)) {
		$sxgeo_city_id = $apartmentCityObj->sxgeo_city_id;
	}
}

$url = $_SERVER['REQUEST_URI'];
$array = explode("/",$url);
$last_item_index = count($array) - 1;
$ap_id =  $array[$last_item_index];

?>

    <!-- NDC FRONT. TABS 1st -->

    <section class="tabs-top">
        <div class="wrap wrap_mobile_full">
            <!-- tab-panels -->
            <div class="tab-panels">
                <div class="tab-head-g">
                    <div data-panel-name="panel1" class="tab-item-g tab-item active">Описание</div>
					<?php
					// если это не хабаровск - будем показывать опцию
					if($sxgeo_city_id != 2022890):
					?>
						<div data-panel-name="panel2" class="tab-item-g tab-item">Выбрать опции</div>
					<?php endif; ?>
                    <div data-panel-name="panel3" class="tab-item-g tab-item">3D тур</div>
                </div>

                <!-- gallery -->
                <section class="gallery panel active" id="panel1">
                    <div class="wrap wrap_mobile_full">

                        <div class="gallery__content gallery__content_mob">
                            <div class="gallery__diagram gallery__diagram_desk"></div>
                            <h3 class="h3 gallery__h3 wow fadeInRight" data-wow-delay="0.3s">
                                <?= $model->label ?>
                            </h3>
                            <div class="gallery__text wow fadeInRight" data-wow-delay="0.6s">
                                <?php if (!FrontendHelper::isNullOrEmpty($model->apartmentComplex->general_progress_percent)): ?>
                                    <p>
                                        <span><?= t('_Ready', 'apartment') ?>:</span>
                                        <strong><?= $model->apartmentComplex->general_progress_percent ?> %</strong>
                                    </p>
                                <?php endif; ?>
                                <?php if (!FrontendHelper::isNullOrEmpty($model->subtype) && $model->isCommercial()) : ?>
                                    <p>
                                        <span><?= t('_Type', 'apartment') ?>:</span>
                                        <strong><?= $model->getSubtypeLabel() ?></strong>
                                    </p>
                                <?php endif; ?>
                                <?php if (!FrontendHelper::isNullOrEmpty($model->profile)) : ?>
                                    <p>
                                        <span><?= t('_Profile', 'apartment') ?>:</span>
                                        <strong><?= $model->profile ?></strong>
                                    </p>
                                <?php endif; ?>
                                <?php if (!FrontendHelper::isNullOrEmpty($model->income)) : ?>
                                    <p>
                                        <span><?= t('_Income', 'apartment') ?>:</span>
                                        <strong><?= $model->income ?></strong>
                                    </p>
                                <?php endif; ?>
                                <?php if (!FrontendHelper::isNullOrEmpty($model->recoupment)) : ?>
                                    <p>
                                        <span><?= t('_Recoupment', 'apartment') ?>:</span>
                                        <strong><?= $model->recoupment ?></strong>
                                    </p>
                                <?php endif; ?>
                                <?php if (!FrontendHelper::isNullOrEmpty($model->entrance)) : ?>
                                    <p>
                                        <span><?= t('_Entrance', 'apartment') ?>:</span>
                                        <strong><?= $model->entrance ?></strong>
                                    </p>
                                <?php endif; ?>
                                <?php if (!FrontendHelper::isNullOrEmpty($model->rooms_number)) : ?>
                                    <p>
                                        <span><?= t('_Rooms count', 'apartment') ?>:</span>
                                        <strong><?php echo $model->rooms_number;
                                            // echo t('{n, plural, =0{_no_rooms} =1{# _1_room} one{# _one_room} few{# _few_rooms} many{# _many_rooms} other{# _other_rooms}}', 'apartment', ['n' => intval($model->rooms_number)])
                                            ?></strong>
                                    </p>
                                <?php endif; ?>

                                <?php if (!FrontendHelper::isNullOrEmpty($model->total_area)) : ?>
                                    <p>
                                        <span><?= t('_Total area', 'apartment') ?>:</span>
                                        <strong><?= $model->total_area . ' ' . CommonDataModel::getAreaUnit() ?></strong>
                                    </p>
                                <?php endif; ?>

                                <?php if (!FrontendHelper::isNullOrEmpty($model->floor)): ?>
                                    <p>
                                        <span><?= t('_Floor', 'apartment') ?>:</span>
                                        <strong><?= $model->floor ?></strong>
                                    </p>
                                <?php endif; ?>

                                <?php if (!FrontendHelper::isNullOrEmpty($model->delivery_date)) : ?>
                                    <p>
                                        <span><?= t('_Delivery date', 'apartment') ?>:</span>
                                        <strong><?= Yii::$app->formatter->asDate($model->delivery_date,
                                                'php:d-m-Y') ?></strong>
                                    </p>
                                <?php endif; ?>
                                <?php if ($model->canShowPrice()) : ?>

                                <?php if ($city->label != "Хабаровск") { ?>
                                    <p>
                                        <span><?= t('_Price from', 'apartment') ?>:</span>
                                        <strong><?= $model->priceFormatted() ?></strong>
                                    </p>
                                <?php } else { ?>
                                        <p>
                                            <span>Стоимость от:</span>
                                            <strong><?= $model->priceFormatted() ?></strong>
                                        </p>
                                <?php } ?>
                                    <p>
                                        <span><?= t('_Price installment', 'apartment') ?>:</span>
                                        <strong><?= $model->installmentPriceFormatted() ?></strong>
                                    </p>
                                <?php endif; ?>
                            </div>
                            <div class="int"> Этой квартирой интересуются <br/><?= $views ?> человек</div>
                            <div class="links-bl">
                                <a href="#"
                                   class="button button_green gallery__button wow fadeInRight ajax-link ascAboutStock"
                                   data-href="<?= SiteUrlHelper::createFormAscAboutDiscPopupUrl(); ?>"
                                   data-wow-delay="0.9s">
                                    Узнать цену
                                    <div class="button__blip button__blip_hover"></div>
                                    <div class="button__blip button__blip_click"></div>
                                </a>
                                <!--                                <a href="" class="add-favorite">Добавить в избранное</a>-->
                            </div>
                        </div>

                        <div class="gallery__content gallery__content_desk">

                            <h3 class="h3 gallery__h3 wow fadeInRight" data-wow-delay="0.3s">
                                <?= $model->label ?>
                            </h3>
                            <div class="gallery__text wow fadeInRight" data-wow-delay="0.6s">
                                <?php if (!FrontendHelper::isNullOrEmpty($model->apartmentComplex->general_progress_percent)): ?>
                                    <p>
                                        <span><?= t('_Ready', 'apartment') ?>:</span>
                                        <strong><?= $model->apartmentComplex->general_progress_percent ?> %</strong>
                                    </p>
                                <?php endif; ?>
                                <?php if (!FrontendHelper::isNullOrEmpty($model->subtype) && $model->isCommercial()) : ?>
                                    <p>
                                        <span><?= t('_Type', 'apartment') ?>:</span>
                                        <strong><?= $model->getSubtypeLabel() ?></strong>
                                    </p>
                                <?php endif; ?>
                                <?php if (!FrontendHelper::isNullOrEmpty($model->profile)) : ?>
                                    <p>
                                        <span><?= t('_Profile', 'apartment') ?>:</span>
                                        <strong><?= $model->profile ?></strong>
                                    </p>
                                <?php endif; ?>
                                <?php if (!FrontendHelper::isNullOrEmpty($model->income)) : ?>
                                    <p>
                                        <span><?= t('_Income', 'apartment') ?>:</span>
                                        <strong><?= $model->income ?></strong>
                                    </p>
                                <?php endif; ?>
                                <?php if (!FrontendHelper::isNullOrEmpty($model->recoupment)) : ?>
                                    <p>
                                        <span><?= t('_Recoupment', 'apartment') ?>:</span>
                                        <strong><?= $model->recoupment ?></strong>
                                    </p>
                                <?php endif; ?>
                                <?php if (!FrontendHelper::isNullOrEmpty($model->entrance)) : ?>
                                    <p>
                                        <span><?= t('_Entrance', 'apartment') ?>:</span>
                                        <strong><?= $model->entrance ?></strong>
                                    </p>
                                <?php endif; ?>
                                <?php if (!FrontendHelper::isNullOrEmpty($model->rooms_number)): ?>
                                    <p>
                                        <span><?= t('_Rooms count', 'apartment') ?>:</span>
                                        <strong><?php echo $model->rooms_number;
                                            //t('{n, plural, =0{_no_rooms} =1{# _1_room} one{# _one_room} few{# _few_rooms} many{# _many_rooms} other{# _other_rooms}}', 'apartment', ['n' => intval($model->rooms_number)])
                                            ?></strong>
                                    </p>
                                <?php endif; ?>

                                <?php if (!FrontendHelper::isNullOrEmpty($model->total_area)): ?>
                                    <p>
                                        <span><?= t('_Total area', 'apartment') ?>:</span>
                                        <strong><?= $model->total_area . ' ' . CommonDataModel::getAreaUnit() ?></strong>
                                    </p>
                                <?php endif; ?>

                                <?php if (!FrontendHelper::isNullOrEmpty($model->floor)): ?>
                                    <p>
                                        <span><?= t('_Floor', 'apartment') ?>:</span>
                                        <strong><?= $model->floor ?></strong>
                                    </p>
                                <?php endif; ?>

                                <?php if (!FrontendHelper::isNullOrEmpty($model->delivery_date)): ?>
                                    <p>
                                        <span><?= t('_Delivery date', 'apartment') ?>:</span>
                                        <strong><?= Yii::$app->formatter->asDate($model->delivery_date,
                                                'php:d-m-Y') ?></strong>
                                    </p>
                                <?php endif; ?>

                                <?php if ($model->canShowPrice()): ?>
                                <?php if ($city->label != "Хабаровск") { ?>
                                    <p>
                                        <span><?= t('_Price from', 'apartment') ?>:</span>
                                        <strong><?= $model->priceFormatted() ?></strong>
                                    </p>
                                    <?php } else { ?>
                                        <p>
                                            <span>Стоимость от:</span>
                                            <strong><?= $model->priceFormatted() ?></strong>
                                        </p>
                                    <?php } ?>
                                    <p>
                                        <span><?= t('_Price installment', 'apartment') ?>:</span>
                                        <strong><?= $model->installmentPriceFormatted() ?></strong>
                                    </p>
                                <?php endif; ?>
                            </div>
                            <div class="int"> Этой квартирой интересуются <?= $views ?> человек</div>
                            <div class="links-bl">
                                <a href="#"
                                   class="button button_green gallery__button wow fadeInRight ajax-link ascAboutStock"
                                   data-href="<?= SiteUrlHelper::createFormAscAboutDiscPopupUrl(); ?>"
                                   data-wow-delay="0.9s">
                                    <?php if ($city->label == "Хабаровск" || $city->label == "Уфа" || $city->label == "Пермь" || $city->label == "Тюмень") { ?>
                                        Забронировать
                                    <?php } else { ?>
                                    Узнать цену
                                    <?php } ?>
                                    <div class="button__blip button__blip_hover"></div>
                                    <div class="button__blip button__blip_click"></div>
                                </a>
                                <!--                                <a href="" class="add-favorite">Добавить в избранное</a>-->
                            </div>

                        </div>
                        <!--                        <div class="gallery__wrap">-->
                        <!--                            <div class="gallery__slider wow fadeIn" data-wow-delay="0.5s" data-wow-duration="2s"-->
                        <!--                                 data-gallery-slider-->
                        <!--                                 data-slick='{"slidesToShow": 1, "infinite": false, "slidesToScroll": 1, "dots": false, "arrows": false, "asNavFor": "[data-gallery-nav]"}'>-->
                        <!--                                <div data-fancybox="gallery" data-bg-size="contain" data-bg-pos="center"-->
                        <!--                                     href=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-1.png">-->
                        <!--                                    <img src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-1.png" alt="">-->
                        <!--                                </div>-->
                        <!--                                <!--<div class="gallery__photo" data-bg-src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-1.png" data-bg-size="contain" data-bg-pos="center"></div>-->
                        -->
                        <!--                                <div data-fancybox="gallery" data-bg-size="contain" data-bg-pos="center"-->
                        <!--                                     href=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-2.jpg">-->
                        <!--                                    <img src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-2.jpg" alt="">-->
                        <!--                                </div>-->
                        <!--                                <!--<div class="gallery__photo" data-bg-src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-2.jpg" data-bg-size="contain" data-bg-pos="center"></div>-->
                        -->
                        <!--                                <div data-fancybox="gallery" data-bg-size="contain" data-bg-pos="center"-->
                        <!--                                     href=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-3.jpg">-->
                        <!--                                    <img src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-3.jpg" alt="">-->
                        <!--                                </div>-->
                        <!--                                <!--<div class="gallery__photo" data-bg-src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-3.jpg" data-bg-size="contain" data-bg-pos="center"></div>-->
                        -->
                        <!--                                <div data-fancybox="gallery" data-bg-size="contain" data-bg-pos="center"-->
                        <!--                                     href=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-4.png">-->
                        <!--                                    <img src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-4.png" alt="">-->
                        <!--                                </div>-->
                        <!--                                <!--<div class="gallery__photo" data-bg-src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-4.png" data-bg-size="contain" data-bg-pos="center"></div>-->
                        -->
                        <!--                                <div data-fancybox="gallery" data-bg-size="contain" data-bg-pos="center"-->
                        <!--                                     href=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-5.jpg">-->
                        <!--                                    <img src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-5.jpg" alt="">-->
                        <!--                                </div>-->
                        <!--                                <!--<div class="gallery__photo" data-bg-src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-5.jpg" data-bg-size="contain" data-bg-pos="center"></div>-->
                        -->
                        <!--                            </div>-->
                        <!--                            <div class="gallery__nav" data-gallery-nav-->
                        <!--                                 data-slick='{"slidesToShow": 4, "slidesToScroll": 1, "dots": false, "asNavFor": "[data-gallery-slider]", "focusOnSelect": true, "infinite": false, "arrows": false, "responsive": [{"breakpoint": 1201, "settings": {"slidesToShow": 3}}, {"breakpoint": 480, "settings": {"slidesToShow": 2}}]}'>-->
                        <!--                                <div class="gallery__item">-->
                        <!--                                    <div class="gallery__img "-->
                        <!--                                         data-bg-src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-1.png"-->
                        <!--                                         data-bg-size="cover" data-bg-pos="center"></div>-->
                        <!--                                </div>-->
                        <!--                                <div class="gallery__item">-->
                        <!--                                    <div class="gallery__img "-->
                        <!--                                         data-bg-src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-2.jpg"-->
                        <!--                                         data-bg-size="cover" data-bg-pos="center"></div>-->
                        <!--                                </div>-->
                        <!--                                <div class="gallery__item">-->
                        <!--                                    <div class="gallery__img "-->
                        <!--                                         data-bg-src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-3.jpg"-->
                        <!--                                         data-bg-size="cover" data-bg-pos="center"></div>-->
                        <!--                                </div>-->
                        <!--                                <div class="gallery__item">-->
                        <!--                                    <div class="gallery__img "-->
                        <!--                                         data-bg-src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-4.png"-->
                        <!--                                         data-bg-size="cover" data-bg-pos="center"></div>-->
                        <!--                                </div>-->
                        <!--                                <div class="gallery__item">-->
                        <!--                                    <div class="gallery__img "-->
                        <!--                                         data-bg-src=".&#x2F;static&#x2F;img&#x2F;apartment&#x2F;gallery-img-5.jpg"-->
                        <!--                                         data-bg-size="cover" data-bg-pos="center"></div>-->
                        <!--                                </div>-->
                        <!--                            </div>-->
                        <!--                            <button class="gallery__zoom">-->
                        <!--                                <svg>-->
                        <!--                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search"/>-->
                        <!--                                </svg>-->
                        <!--                            </button>-->
                        <!--                        </div>-->

                        <div class="gallery__wrap">
                            <div class="gallery__slider wow fadeIn <?= count($model->photos) > 1 ?: 'clear_border' ?>"
                                 data-wow-delay="0.5s" data-wow-duration="2s" data-gallery-slider
                                 data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "dots": false, "arrows": false, "asNavFor": "[data-gallery-nav]"}'>
                                <?php foreach ($model->photos as $photoEtF): ?>
                                    <div data-fancybox="gallery" data-bg-size="contain" data-bg-pos="center"
                                         href="<?= ImgHelper::getImageSrcFromEtF($photoEtF, 'apartment', 'gallery') ?>">
                                        <div class="gallery__photo"
                                             data-bg-src="<?= ImgHelper::getImageSrcFromEtF($photoEtF, 'apartment',
                                                 'gallery') ?>" data-bg-size="contain" data-bg-pos="center"></div>
                                    </div>
                                <?php endforeach; ?>
                                <?php if (count($model->photos) == 0): ?>
                                    <div class="gallery__photo" data-bg-src="<?= Apartment::NO_IMAGE_PATH ?>"
                                         data-bg-size="contain" data-bg-pos="center"></div>
                                <?php endif; ?>
                            </div>

                            <?php if (count($model->photos) > 1) : ?>
                                <div class="gallery__nav" data-gallery-nav
                                     data-slick='{"slidesToShow": 4, "slidesToScroll": 1, "dots": false, "asNavFor": "[data-gallery-slider]", "focusOnSelect": true, "infinite": false, "arrows": false, "responsive": [{"breakpoint": 1201, "settings": {"slidesToShow": 3}}, {"breakpoint": 480, "settings": {"slidesToShow": 2}}]}'>

                                    <?php foreach ($model->photos as $photoEtF): ?>
                                        <div class="gallery__item">
                                            <div class="gallery__img"
                                                 data-bg-src="<?= ImgHelper::getImageSrcFromEtF($photoEtF, 'apartment',
                                                     'galleryThumb') ?>" data-bg-size="cover"
                                                 data-bg-pos="center"></div>
                                        </div>
                                    <?php endforeach; ?>

                                    <?php if (count($model->photos) == 0): ?>
                                        <div class="gallery__item">
                                            <div class="gallery__img" data-bg-src="<?= Apartment::NO_IMAGE_PATH ?>"
                                                 data-bg-size="cover" data-bg-pos="center"></div>
                                        </div>
                                    <?php endif; ?>

                                </div>
                                <button class="gallery__zoom">
                                    <svg>
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search"/>
                                    </svg>
                                </button>
                            <?php endif; ?>
                        </div>

                    </div>
                </section>
                <!-- /gallery -->
                <section class="panel" id="panel2">
                    <div data-section="vue-constructor">
                        <input id="apartment_id" type="hidden" value="<?= $model->id ?>">
                        <div id="vueApp">
                            <the-constructor/>
                        </div>
                    </div>
                </section>
                <!-- tour -->
                <section class="tour panel" id="panel3">
                    <div class="wrap wrap_mobile_full">
                        <div class="tour__body">
                            <iframe class="tour__iframe"
                                    src="https://s3.eu-west-2.amazonaws.com/tour.talan2/site/index.html"></iframe>
                        </div>
                    </div>
                </section>
                <!-- /tour -->
            </div>
            <!-- /tab-panels -->
        </div>
    </section>
    <!-- END NDC FRONT. END TABS 1st -->

    <!-- NDC FRONT How to buy-->

    <section class="how-to-buy-c">
        <div class="wrap wrap_mobile_full">

            <div class="tabs-but tab-panels">
                <div class="tabs-left">
                    <h2 class="h2 how-to-buy__h2 wow fadeInLeft"
                        style="visibility: visible; animation-name: fadeInLeft;">
                        Как купить: </h2>
                    <div class="tabs-mob-slide">
                        <div data-panel-name="panelC1" class="tab-item tab-item-h active"><span>Ипотека</span></div>
                        <div data-panel-name="panelC2" class="tab-item tab-item-h"><span>Рассрочка</span></div>
                        <?php if ($city->label != "Хабаровск") { ?>
                            <div data-panel-name="panelC3" class="tab-item tab-item-h">
                                <span>Оплата вторичным  жильем</span>
                            </div>
                        <?php } ?>
                        <!--                        <div data-panel-name="panelC4" class="tab-item tab-item-h"><span>Инвестирование</span></div>-->
                        <a href="https://инвест.талан.рф" target="_blank" class="tab-item-h"><span>Инвестирование</span></a>
                        <div data-panel-name="panelC5" class="tab-item tab-item-h"><span>Вся сумма</span></div>
                    </div>
                </div>
                <div class="tabs-right">
                    <div id="result-form">

                        <div class="simple-thanks" style="display: none" id="simple-thanks">
                            <div class="outer">
                                <div class="modal__overlay simple-thanks-close"></div>
                                <div class="modal__inner">
                                    <div class="modal__content">

                                        <div class="popup">
                                            <h5 class="h5 popup__h5">
                                                <?= Yii::t('front/form', '_Thanks') ?>
                                            </h5>

                                            <div class="popup__text">
                                                <p>
                                                    <?= Yii::t('front/form',
                                                        '_Your application has been successfully submitted. Our manager will contact you soon.') ?>
                                                </p>
                                            </div>

                                            <button class="button button_green popup__button simple-thanks-close">
                                                <?= Yii::t('front/form', '_Close') ?>
                                                <span class="button__blip button__blip_hover"></span>
                                                <span class="button__blip button__blip_click"></span>
                                            </button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="panelC1" class="tabs-but-item panel active">
                        <?= $this->render('_hypotec'); ?>
                    </div>

                    <div id="panelC2" class="tabs-but-item panel">
                        <!-- CALC -->
                        <?= $this->render('_installment'); ?>
                    </div>
                    <div id="panelC3" class="tabs-but-item panel">
                        <!-- CALC -->
                        <?= $this->render('_buy'); ?>
                    </div>
                    <?php
                    echo \yii\helpers\Html::beginForm('',
                        'post', [
                            'id' => 'simpleForm',
                            'class' => 'call-form',
                        ]
                    );
                    ?>
                    <div id="panelC4" class="tabs-but-item panel">
                        <div class="call-me__inner">

                            <div class="row call-items">

                                <div class="call-title col-xs-12 col-md">
                                    <div class="heading">
                                        <div class="h1">Поможем подобрать продукт</div>
                                    </div>
                                </div>
                                <div class="call-phone col-xs-12 col-md">
                                    <div class="form__row ">
                                        <label class="field form__field">
                                            <input type="text" class="input form__input" name="_name"
                                                   data-validation-name="name">
                                            <span class="form__label"> Ваше имя </span>
                                            <span class="field__error"> Введите Ваше имя </span>
                                        </label>
                                    </div>
                                    <div class="form__row field-formsecondary-phone required"><label
                                                class="field form__field"><input type="tel" id="formsecondary-phone"
                                                                                 class="input form__input input_phone"
                                                                                 name="FormSecondary[phone]"
                                                                                 aria-required="true"><span
                                                    class="form__label">Телефон</span>
                                            <p class="help-block help-block-error"></p></label></div>
                                </div>

                                <div class="agr-wr col-xs-12 col-md" style="">
                                    <div id="secondary-form-agree-block" class="field form__field">
                                        <label class="checkbox form__checkbox">
                                            <input type="checkbox" class="checkbox__checkbox"
                                                   name="FormSecondary[iAgree]" data-validation-name="agree">
                                            <span class="checkbox__mask"></span>
                                            <span class="checkbox__label"><?php echo obtain('agreementCheckboxText', app()->params, '') ?></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="call-button col-xs-12 col-md">

                                    <button type="button"
                                            class="button btn-callme btn-simple-form"
                                            data-input-id="i17"
                                            data-form-id="FormInvest"
                                            data-form-action="/form/form/form-invest">
                                        Позвоните мне<span class="button-blip button-blip_hover"></span>
                                        <span class="button-blip button-blip_click"></span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div id="panelC5" class="tabs-but-item panel">
                        <div class="call-me__inner">


                            <div class="row call-items">

                                <div class="call-title col-xs-12 col-md">
                                    <div class="heading">
                                        <div class="h1">Оплачивая всю стоимость квартиры сразу, <br/>вы получаете
                                            скидку
                                            2%
                                        </div>
                                    </div>
                                </div>
                                <div class="call-phone col-xs-12 col-md">
                                    <div class="form__row ">
                                        <label class="field form__field">
                                            <input type="text" class="input form__input" name="RequestFullAmount[name]"
                                                   data-validation-name="name" id="fullAmountName">
                                            <span class="form__label"> Ваше имя </span>
                                            <span class="field__error"> Введите Ваше имя </span>
                                        </label>
                                    </div>
                                    <div class="form__row field-formsecondary-phone required">
										<label class="field form__field">
											<input type="tel" id="fullAmountPhone" class="input form__input input_phone"
                                            	name="RequestFullAmount[phone]" aria-required="true">
											<span class="form__label">Телефон</span>
                                            <p class="help-block help-block-error"></p>
										</label>
									</div>
                                    <input type="hidden" value="<?= $ap_id ?>" class="form__input" name="RequestFullAmount[apartment_id]"  aria-required="true">

                                </div>
                                <div class="agr-wr col-xs-12 col-md" style="">
                                    <div id="secondary-form-agree-block" class="field form__field">
                                        <label class="checkbox form__checkbox">
                                            <input type="checkbox" class="checkbox__checkbox" id="fullAmountAgreement"
                                                   name="RequestFullAmount[iAgree]" data-validation-name="agree" required="required">
                                            <span class="checkbox__mask"></span>
                                            <span class="checkbox__label"><?= obtain('agreementCheckboxText',
                                                    app()->params, '') ?></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="call-button col-xs-12 col-md">

                                    <button type="button"
                                            class="button btn-callme btn-simple-form"
                                            data-form-id="FormSumm"
                                            data-input-id="i18"
                                            data-form-action="/form/form/form-summ">

                                        Позвоните мне<span class="button-blip button-blip_hover"></span>
                                        <span class="button-blip button-blip_click"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                 echo   \yii\helpers\Html::endForm();
                    ?>
                </div>
            </div>

        </div>
    </section>

    <style>
        #result-form {
            position: relative;
        }

        #simple-thanks {
            position: absolute;
            z-index: 10;
            width: 100%;
            padding: 49px;
            background-color: #2aa662;
        }

        .form-error {
            font-weight: normal;
            font-size: 13px;
            color: red;
            background-color: orange;
            border-radius: 5px;
            position: absolute;
            top: 45px;
            left: 0;
            padding: 0 9px;
            z-index: 2;
            transform: translateY(28px);
            transform-origin: bottom left;
            transition-property: transform, color;
            transition-duration: 0.4s;
            transition-timing-function: cubic-bezier(0.23, 1, 0.32, 1);
            letter-spacing: .2px;
        }

		.tlnModal {
			position: fixed;
			background: rgba(0,0,0, 0.3);
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
			z-index: 100;
			display: flex;
			justify-content: center;
			align-items: center;
		}
		.tlnMdlWnd {
			background: #fff;
			padding: 45px 20px 20px;
			border: 1px solid #999;
			position: relative;
			max-width: 600px;
		}
		@media (max-width: 650px) {
			.tlnMdlWnd {
				max-width: 95%;
			}
		}
		.tlnDisplNone {
			display: none !important;
		}
		.tlnMdlWnd .tlnFormInput {
			display: block;
			width: 100%;
			height: 40px;
			border-top: 0;
			border-right: 0;
			border-bottom: 1px solid #1e9b57;
			border-left: 0;
			outline: 0;
			font-size: 14px;
			line-height: 2;
			padding: 0 9px;
			color: #343434;
		}
		.tlnMdlWnd .form__button {
			margin: 40px auto 0;
			width: 150px;
		}
		.tlnMdlWnd .form__button[disabled] {
			color: #555;
			background: #aaa;
			border: 1px solid #555;
		}
		.tlnMdlWnd .form__button[disabled] .button__blip_hover {
			background: #aaa;
		}

		.tlnMdlWnd .h5 {
			text-align: center;
		}
    </style>

    <!-- END NDC FRONT -->
<?php
$this->registerJs(<<<JSS
    $( document ).ready(function() {
        $('.simple-thanks-close').on('click', function(){
            $('#simple-thanks').hide();
        });
        $('.input.phone-formatted.form-input').on('focus', function(){
            $('.form-error').hide();
        });
        
        $(".btn-simple-form").on('click',
            function(e){
                e.preventDefault();
                
				var formFieldName = $('#fullAmountName').val().trim()
				,	formPhone = $('#fullAmountPhone').val()
				,	agreementChb = $('#fullAmountAgreement:checked').length
				,	errorForm = $('#anyFormEntry')
				,	msgText = errorForm.find('.tlnModalText')
				;
				if(!formFieldName.length) {
					msgText.html('Необходимо ввести имя');
					errorForm.removeClass('tlnDisplNone');
				} else if(!formPhone.length) {
					msgText.html('Необходимо ввести телефон');
					errorForm.removeClass('tlnDisplNone');
				} else if(!agreementChb) {
					msgText.html('Необходимо согласится с пользовательским соглашением');
					errorForm.removeClass('tlnDisplNone');
				}
				if(formFieldName.length && formPhone.length && agreementChb) {
					sendAjaxForm($(this).data('form-action'));
				}
                return false;
            }
        );
        
        $('.tlnModal').on('click', function(event) {
			var eventTarget = $(event.target)
			;
			if(eventTarget.hasClass('tlnModal') || !event || !event.target || !eventTarget.length) {
				eventTarget.addClass('tlnDisplNone');
			}
			return false;
		});
		
		$('.tlnMdlWnd .modal__close, .tlnMdlWnd .tlnModalClose').on('click', function(event) {
			var parentTlnModal = $(this).closest('.tlnModal');
			if(parentTlnModal.length) {
				parentTlnModal.addClass('tlnDisplNone');
			}
			return false;
		});
    });

function sendAjaxForm(url) {
    $.ajax({
        url:     url, 
        type:     "POST",
        dataType: "html", 
        data: $("#simpleForm").serialize(),  // Сериaлизуем объект
        success: function(response) { //Данные отправлены успешно
            $('#simple-thanks').show();
            $('#i17').val('');
            $('#i18').val('');
    },
    	error: function(response) { // Данные не отправлены
        $('#result-form').html('Ошибка. Данные не отправлены.');
    }
 	});
}
JSS
);
?>
<div class="tlnModal tlnDisplNone" id="anyFormEntry">
	<div class="tlnMdlWnd">
		<button class="modal__close"></button>

		<div class="modal__content">
			<h5 class="h5 popup__h5">Внимание!</h5>

			<div class="popup__text">
				<p class="tlnModalText"></p>
			</div>

			<button class="button button_green popup__button tlnModalClose">
				<?= Yii::t('front/form', '_Close') ?>
				<span class="button__blip button__blip_hover"></span>
				<span class="button__blip button__blip_click"></span>
			</button>
		</div>
	</div>
</div>