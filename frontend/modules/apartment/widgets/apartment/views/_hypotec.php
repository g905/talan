<?php
/**
 * Created by PhpStorm.
 * User: p-clerick
 * Date: 04.06.19
 * Time: 11:51
 */

use frontend\modules\page\widgets\PageIntroWidget;
use frontend\modules\page\widgets\HypothecCalcWidget;
use frontend\modules\page\widgets\HypothecStepsWidget;
use frontend\modules\page\widgets\HypothecBenefitWidget;
use frontend\modules\page\widgets\HypothecBenefitsWidget;

use common\models\City;
use common\models\Hypothec;
use common\helpers\SiteUrlHelper;


$city = $city = City::getUserCity();

$model = Hypothec::findByCityWithBenefitsAndStepsOrFail($city->id);

if ($model !== null) {
        $formOnSubmit = $model->form_calc_onsubmit;
        $defaultTimeTerm = $model->default_time_term;
        $defaultInitialFee = $model->default_initial_fee;
        $defaultApartmentPrice = $model->default_apartment_price;
        $minTerm = $model->min_term;
        $maxTerm = $model->max_term;
        $banks = $model->banks;
}
else {
    $formOnSubmit = '';
    $defaultTimeTerm = 0;
    $defaultInitialFee = 0;
    $defaultApartmentPrice = 0;
    $minTerm = 0;
    $maxTerm = 0;
    $banks = [];
}
?>
<section class="calc stavka">
    <div class="title">
        <div class="wrap-tab">
            <h3 class="h3 ">Узнайте свою ставку</h3>
        </div>
    </div>
    <div class="calc__block ">
        <div class="wrap-tab">
            <form class="calc_form calc_form-ipoteka clearfix">
                <div class="calc__fieldset--left " >
                    <legend>1) Введите данные:</legend>
                    <div class="filters__section clearfix">
                        <div class="filters__heading">Стоимость <br />квартиры:</div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1000}' data-wnumb='{"decimals":"0", "thousand": " ", "postfix":" руб"}'  data-range-min="50000" data-range-max="10000000" data-nouislider-type='priceHypo'>
                                <div class="ranges nouislider__ranges">
                                    <input class="ranges__input" placeholder="<?= $defaultApartmentPrice ?>" type="text" value="<?= $defaultApartmentPrice ?>" name="square-max" data-no-ui-input> </div>
                                <div class="nouislider__slider"></div>
                            </div>
                            <!-- /.filters__nouislider -->
                        </div>
                        <!-- /.filters__inner -->
                    </div>
                    <!-- /.filters__section -->
                    <div class="filters__section clearfix">
                        <div class="filters__heading">Сроки:</div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-wnumb='{"decimals":"0", "postfix":" мес"}' data-range-min="<?= $minTerm ? : '1' ?>" data-range-max="<?= $maxTerm ? : '240' ?>" data-nouislider-type='termHypo'>
                                <div class="ranges nouislider__ranges">
                                    <input class="ranges__input" placeholder="" value="<?= $defaultTimeTerm ?>" type="text" name="square-max" data-no-ui-input> </div>
                                <div class="nouislider__slider"></div>
                            </div>
                            <!-- /.filters__nouislider -->
                        </div>
                        <!-- /.filters__inner -->
                    </div>
                    <!-- /.filters__section -->
                    <div class="filters__section clearfix">
                        <div class="filters__heading">
                            <span class="tabl-hidden">Первоначальный <br />взнос:</span>
                            <span class="desk-hidden">Первонач. взнос:</span>
                        </div>
                        <div class="filters__inner">
                            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-wnumb='{"decimals":"0", "postfix":"%"}' data-range-min="1" data-range-max="100" data-nouislider-type='first_valueHypo'>
                                <div class="ranges nouislider__ranges">
                                    <input class="ranges__input" placeholder="" value="<?= $defaultInitialFee ?>" type="text" name="square-max" data-no-ui-input> </div>
                                <div class="nouislider__slider"></div>
                            </div>
                            <!-- /.filters__nouislider -->
                        </div>
                        <!-- /.filters__inner -->
                    </div>
                    <!-- /.filters__section -->
                </div>
                <!-- /.calc__fieldset-left -->
                <div class="calc__fieldset--right ">
                    <legend>2) Выберите банк:</legend>
                    <div class="bank-section" data-stavka-slider data-slick='{ "rows": 3,
                                          "slidesPerRow": 3, "dots": false, "prevArrow": ".stavka .navigation__btn_prev", "nextArrow": ".stavka .navigation__btn_next","responsive": [{
                                            "breakpoint": 1200,
                                            "settings": {
                                              "rows": 3,
                                              "slidesPerRow": 3
                                            }
                                          }]}'>
                        <!-- <div>
                            <div class="bank-section__item active" data-monthBid="10" data-bankname="РоссельхозБанка">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 554 217.9">
                                    <path fill="#e52713" d="M85.2 42.6c9 0 11.9 6.5 14.3 13l25.2 69c.2.5.1 1.4-.8 1.7l-13.7 5.3c-.7.3-1.4-.1-1.7-.8l-5.9-16.3H67.8l-5.9 16.3c-.2.6-1 1.1-1.7.8l-13.7-5.3c-.7-.3-1-1.1-.8-1.7l25.2-69c2.3-6.4 5.3-13 14.3-13zM43.9 157.7c-.7 0-1.3.6-1.3 1.4v15c0 .7.6 1.3 1.3 1.3h82.6c.7 0 1.3-.6 1.3-1.3v-15c0-.7-.6-1.4-1.3-1.4H43.9m41.2-94.1L73.4 98.9h23.7L85.3 63.6h-.2zm307.4 51.9c0 4-2.2 6.6-6.3 6.6-1.6 0-2.9 0-5-.6v-12.1c.9-.2 2.8-.5 5-.4 4.2.2 6.3 2.1 6.3 6.5zm8.4-.6c0-7.4-4.3-12.5-13.5-12.5a23 23 0 0 0-6.2.8V92.5a72.3 72.3 0 0 1 16 .1l.2-6.2a56.3 56.3 0 0 0-24.5.5v39.4c4.3 1.6 8.6 2.3 12.9 2.3 9.9 0 15.1-5.3 15.1-13.7zm-94.9-2.2c0 5.5-2.2 9-5.8 9-1.5 0-2.3-.1-4-.6v-16.5c1.9-.9 2.9-.9 4.4-.9 3.6 0 5.4 3 5.4 9m-27.5.2c0-6 1.8-9.2 5.4-9.2 1.5 0 2.5 0 4.4.9v16.5c-1.7.6-2.5.6-4 .6-4.1.1-5.8-3.3-5.8-8.8m17.7 24.7v-10.7c1 .6 3 1.5 6.3 1.5 6.7 0 11.6-5.5 11.6-15.6 0-10.9-4.9-15.6-12.9-15.6-2 0-3.4.2-5 1V85.3h-7.8v12.8c-1.6-.8-3-1-5-1-8 0-12.9 4.9-12.9 15.7 0 10.1 4.4 15.5 11.6 15.5 3.4 0 5.3-.9 6.3-1.5v10.7h7.8m-59.5-9.7V99a41 41 0 0 0-23.9 0v14.8c0 4-.1 5.2-.5 6.5-.4 1.1-1 2.5-2.6 3.6l2.1 4.6c6.3-1.8 8.7-4.5 8.7-13.3v-10.9c2.1-.3 2.5-.4 4.2-.4 1.7 0 2 .1 4.1.4v23.5h7.9zm96.8-6.3c-1.7.6-2.9.7-4.2.7-2.8 0-4.4-1.1-4.4-4 0-3 2-4 5.9-4 .9 0 1.9.1 2.3.3v2.2c0 2.3.1 3.8.4 4.8zm10 3.5c-2.1-3.2-2.6-5-2.6-9v-8.1c0-4.4-.5-6-1.7-7.5-1.6-2.2-4.3-3.2-9.2-3.2-3.5 0-6.9.4-10.1 1.2l1.2 5.8c2.2-.3 4.5-.6 6.2-.6 5.2 0 5.8.8 5.8 4.7v1.5c-1.2-.3-2.5-.5-4-.5-7.9 0-12 3.8-12 10.2 0 5.9 4.4 9.2 9.8 9.2 2 0 4.1-.4 5.8-1 1-.4 1.4-.7 2.7-1.5a7.2 7.2 0 0 0 3 2.5l5.1-3.7zm76.8-3.5c-1.7.6-2.9.7-4.2.7-2.8 0-4.4-1.1-4.4-4 0-3 2-4 5.9-4 .9 0 1.9.1 2.3.3v2.2c-.1 2.3.1 3.8.4 4.8zm10 3.5c-2.1-3.2-2.6-5-2.6-9v-8.1c0-4.4-.5-6-1.6-7.5-1.6-2.2-4.3-3.2-9.2-3.2-3.5 0-6.9.4-10.1 1.2l1.2 5.8c2.2-.3 4.5-.6 6.2-.6 5.2 0 5.8.8 5.8 4.7v1.5c-1.2-.3-2.5-.5-4-.5-7.9 0-12 3.8-12 10.2 0 5.9 4.4 9.2 9.8 9.2 2 0 4.1-.4 5.8-1 1-.4 1.4-.7 2.7-1.5a7.9 7.9 0 0 0 3 2.5l5-3.7zM259 117.5c0 3-1 5-4.8 5-1.3 0-1.8 0-3.4-.5v-8.6c2-.4 2.2-.5 3.4-.5 3.6 0 4.8 1.8 4.8 4.6zm-16.1-19.8v28.6c3.7 1.6 7.8 2.3 11.5 2.3 8.3 0 12.8-4.3 12.8-11.4 0-6.4-4-10.4-11.3-10.4-1.9 0-3 .2-5 .7v-9.8h-8zM459 127.8V97.9h-7.9v11.3h-8.9V97.9h-7.9v29.9h7.9v-12.2h8.9v12.2h7.9zm32.3 0l-5.3-11.2c-1.2-2.3-2.3-4-4.7-4.5v-.1c1.6-.3 2.9-1.3 4.3-4.3 1.3-2.9 1.7-3.6 2.5-4.2.8-.7 1.8-.4 1.8-.4l.4-5.4c-1.1-.1-2.4-.2-3.7-.1-2.1.1-3.2.7-4.3 1.8-.8.8-1.4 2-2.1 3.4a166 166 0 0 1-2.1 4.3c-.8 1.6-1.1 2.3-3.2 2.3h-1.3V97.9h-7.9v29.9h7.9v-12.2h1.1c2.2 0 2.6.6 3.8 2.9 1.4 2.8 4.1 9.3 4.1 9.3h8.7zm-293.1 0h9.1L193.6 86c-1.4-.5-3.2-.8-5.1-.8-2.1 0-2.8.3-4.4.8l-14.5 41.8h8.7l2.6-8.2h15l2.3 8.2zm-15.3-15.2l2.9-9.7 2.5-8.8h.2a322 322 0 0 0 5 18.4h-10.6zm174-4.7a5 5 0 0 0-5 5 5 5 0 0 0 5 5 5 5 0 0 0 5-5 5 5 0 0 0-5-5z"
                                    /> </svg>
                            </div>
                        </div> -->
                        <?php foreach ($banks as $i => $bank) : ?>
                            <?php $svg = $bank->getSvgImagePath() ?>
                            <div>
                                <div class="bank-section__item <?= $i === 0 ? 'active' : '' ?>" data-monthBid="<?= $bank->mortgage_rate ?>" data-bankname="<?= $bank->label ?>">
                                    <?= $svg ? @file_get_contents($svg) : '' ?>
                                </div>
                            </div>
                        <?php endforeach ?>
                        <!-- <div>
                            <div class="bank-section__item" data-monthBid="12" data-bankname="Тинькоф Банка">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 554 217.9">
                                    <path fill="#e52713" d="M85.2 42.6c9 0 11.9 6.5 14.3 13l25.2 69c.2.5.1 1.4-.8 1.7l-13.7 5.3c-.7.3-1.4-.1-1.7-.8l-5.9-16.3H67.8l-5.9 16.3c-.2.6-1 1.1-1.7.8l-13.7-5.3c-.7-.3-1-1.1-.8-1.7l25.2-69c2.3-6.4 5.3-13 14.3-13zM43.9 157.7c-.7 0-1.3.6-1.3 1.4v15c0 .7.6 1.3 1.3 1.3h82.6c.7 0 1.3-.6 1.3-1.3v-15c0-.7-.6-1.4-1.3-1.4H43.9m41.2-94.1L73.4 98.9h23.7L85.3 63.6h-.2zm307.4 51.9c0 4-2.2 6.6-6.3 6.6-1.6 0-2.9 0-5-.6v-12.1c.9-.2 2.8-.5 5-.4 4.2.2 6.3 2.1 6.3 6.5zm8.4-.6c0-7.4-4.3-12.5-13.5-12.5a23 23 0 0 0-6.2.8V92.5a72.3 72.3 0 0 1 16 .1l.2-6.2a56.3 56.3 0 0 0-24.5.5v39.4c4.3 1.6 8.6 2.3 12.9 2.3 9.9 0 15.1-5.3 15.1-13.7zm-94.9-2.2c0 5.5-2.2 9-5.8 9-1.5 0-2.3-.1-4-.6v-16.5c1.9-.9 2.9-.9 4.4-.9 3.6 0 5.4 3 5.4 9m-27.5.2c0-6 1.8-9.2 5.4-9.2 1.5 0 2.5 0 4.4.9v16.5c-1.7.6-2.5.6-4 .6-4.1.1-5.8-3.3-5.8-8.8m17.7 24.7v-10.7c1 .6 3 1.5 6.3 1.5 6.7 0 11.6-5.5 11.6-15.6 0-10.9-4.9-15.6-12.9-15.6-2 0-3.4.2-5 1V85.3h-7.8v12.8c-1.6-.8-3-1-5-1-8 0-12.9 4.9-12.9 15.7 0 10.1 4.4 15.5 11.6 15.5 3.4 0 5.3-.9 6.3-1.5v10.7h7.8m-59.5-9.7V99a41 41 0 0 0-23.9 0v14.8c0 4-.1 5.2-.5 6.5-.4 1.1-1 2.5-2.6 3.6l2.1 4.6c6.3-1.8 8.7-4.5 8.7-13.3v-10.9c2.1-.3 2.5-.4 4.2-.4 1.7 0 2 .1 4.1.4v23.5h7.9zm96.8-6.3c-1.7.6-2.9.7-4.2.7-2.8 0-4.4-1.1-4.4-4 0-3 2-4 5.9-4 .9 0 1.9.1 2.3.3v2.2c0 2.3.1 3.8.4 4.8zm10 3.5c-2.1-3.2-2.6-5-2.6-9v-8.1c0-4.4-.5-6-1.7-7.5-1.6-2.2-4.3-3.2-9.2-3.2-3.5 0-6.9.4-10.1 1.2l1.2 5.8c2.2-.3 4.5-.6 6.2-.6 5.2 0 5.8.8 5.8 4.7v1.5c-1.2-.3-2.5-.5-4-.5-7.9 0-12 3.8-12 10.2 0 5.9 4.4 9.2 9.8 9.2 2 0 4.1-.4 5.8-1 1-.4 1.4-.7 2.7-1.5a7.2 7.2 0 0 0 3 2.5l5.1-3.7zm76.8-3.5c-1.7.6-2.9.7-4.2.7-2.8 0-4.4-1.1-4.4-4 0-3 2-4 5.9-4 .9 0 1.9.1 2.3.3v2.2c-.1 2.3.1 3.8.4 4.8zm10 3.5c-2.1-3.2-2.6-5-2.6-9v-8.1c0-4.4-.5-6-1.6-7.5-1.6-2.2-4.3-3.2-9.2-3.2-3.5 0-6.9.4-10.1 1.2l1.2 5.8c2.2-.3 4.5-.6 6.2-.6 5.2 0 5.8.8 5.8 4.7v1.5c-1.2-.3-2.5-.5-4-.5-7.9 0-12 3.8-12 10.2 0 5.9 4.4 9.2 9.8 9.2 2 0 4.1-.4 5.8-1 1-.4 1.4-.7 2.7-1.5a7.9 7.9 0 0 0 3 2.5l5-3.7zM259 117.5c0 3-1 5-4.8 5-1.3 0-1.8 0-3.4-.5v-8.6c2-.4 2.2-.5 3.4-.5 3.6 0 4.8 1.8 4.8 4.6zm-16.1-19.8v28.6c3.7 1.6 7.8 2.3 11.5 2.3 8.3 0 12.8-4.3 12.8-11.4 0-6.4-4-10.4-11.3-10.4-1.9 0-3 .2-5 .7v-9.8h-8zM459 127.8V97.9h-7.9v11.3h-8.9V97.9h-7.9v29.9h7.9v-12.2h8.9v12.2h7.9zm32.3 0l-5.3-11.2c-1.2-2.3-2.3-4-4.7-4.5v-.1c1.6-.3 2.9-1.3 4.3-4.3 1.3-2.9 1.7-3.6 2.5-4.2.8-.7 1.8-.4 1.8-.4l.4-5.4c-1.1-.1-2.4-.2-3.7-.1-2.1.1-3.2.7-4.3 1.8-.8.8-1.4 2-2.1 3.4a166 166 0 0 1-2.1 4.3c-.8 1.6-1.1 2.3-3.2 2.3h-1.3V97.9h-7.9v29.9h7.9v-12.2h1.1c2.2 0 2.6.6 3.8 2.9 1.4 2.8 4.1 9.3 4.1 9.3h8.7zm-293.1 0h9.1L193.6 86c-1.4-.5-3.2-.8-5.1-.8-2.1 0-2.8.3-4.4.8l-14.5 41.8h8.7l2.6-8.2h15l2.3 8.2zm-15.3-15.2l2.9-9.7 2.5-8.8h.2a322 322 0 0 0 5 18.4h-10.6zm174-4.7a5 5 0 0 0-5 5 5 5 0 0 0 5 5 5 5 0 0 0 5-5 5 5 0 0 0-5-5z"
                                    /> </svg>
                            </div>
                        </div> -->

                    </div>
                    <!-- /.bank-section -->
                    <div class="navigation__content">
                        <button type="button" class="navigation__btn navigation__btn_prev slick-arrow" data-stavka-arrow-prev="" style="display: inline-block;">
                            <span class="arrow arrow_left navigation__arrow"></span>
                        </button>
                        <div class="navigation__status">
                            <span class="navigation__current" data-stavka-status-current>01</span> \
                            <span class="navigation__total" data-stavka-status-total>02</span>
                        </div>
                        <button type="button" class="navigation__btn navigation__btn_next slick-arrow" data-stavka-arrow-next="" style="display: inline-block;">
                            <span class="arrow arrow_right navigation__arrow"></span>
                        </button>
                    </div>
                </div>
                <!-- /.calc__fieldset-left -->
            </form>
            <!-- /.calc_form -->
        </div>
        <!-- /.wrap -->
        <form class="calc_form-bottom clearfix">
            <div class="wrap wrap_mobile_full">
                <fieldset class="calc__fieldset--bottom">
                    <div class="calc__calculated-block wbg   ">
                        <div class="ls-calc">
                            <div class="bank-names">Ставка
                                <span class="bank-name"></span>
                            </div>
                            <div class="calc__calculated--row">
                                <div class="calc__month-payment">Ежемесячный платеж:</div>
                                <span class="calc__month-payment--price" data-wnumb='{"decimals": 0, "thousand":" ", "postfix":" р"}'>225 000 р</span>
                                <!-- <span class="calc__letter big-letter">&nbsp;р</span> -->
                            </div>
                        </div>
                        <!-- /.calc__month-payment -->
                        <!-- <div class="calc__calculated--row">
                      <div class="calc__last-payment">Последний платеж:</div>
                      <span class="last-payment--price">525 000</span>
                      <span class="calc__letter">&nbsp;р</span>
                    </div>-->
                        <!-- /.calc__month-payment -->
                        <button type="button"  class="send-request ajax-link" data-href="<?= SiteUrlHelper::createFormHypothecPopupUrl()?>">
                            <span class="tabl-hidden">Отправить заявку на ипотеку</span>
                            <span class="desk-hidden">Отправить заявку</span>
                        </button>
                    </div>
                    <!-- /.calc__calculated-block -->
                </fieldset>
                <!-- /.calc__fieldset--bottom -->
            </div>
        </form>
    </div>
    <!-- /.calc__block -->
</section>

