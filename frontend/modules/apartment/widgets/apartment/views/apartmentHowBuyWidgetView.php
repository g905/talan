<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 25.12.2017
 * Time: 21:26
 */

use frontend\helpers\FrontendHelper;

/** @var \common\models\HowBuyBlock[] $howBuyBlocks */
?>


<div class="how-to-buy">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 how-to-buy__h2 wow fadeInLeft">
            <?= $howBuyBlocks[0]->howBuy->label ?>
        </h2>
        <div class="how-to-buy__list">

            <?php foreach ($howBuyBlocks as $howBuyBlock):
                $link = $howBuyBlock->link;
                if (FrontendHelper::isNullOrEmpty($link)) {
                    $link = \common\helpers\SiteUrlHelper::createHowBuyBlockViewUrl(['id' => $howBuyBlock->id]);
                }
                ?>
                <a href="<?= $link ?>" class="card how-to-buy__card wow fadeInUp">
                    <h3 class="h3 card__h3">
                        <span class="card_span"><?= $howBuyBlock->label ?></span>
                    </h3>
                    <div class="card__text">
                            <?= $howBuyBlock->short_desc ?>
                    </div>
                    <div onclick="window.location.href = '<?= $link ?>'" class="card__more">
                        <?= $howBuyBlock->link_label ?>
                    </div>
                </a>
            <?php endforeach; ?>

        </div>
    </div>
</div>
