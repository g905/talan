<section class="tour">
    <div class="wrap wrap_mobile_full">
        <div class="tour__body">
            <div class="tour__body-overlay">
                <span><?= Yii::t('app', '_Click_to_view') ?></span>
            </div>
            <iframe class="tour__iframe" src="<?= $url ?>"></iframe>
        </div>
    </div>
</section>