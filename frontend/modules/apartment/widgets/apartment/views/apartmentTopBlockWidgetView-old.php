<?php

use common\models\Apartment;
use common\helpers\SiteUrlHelper;
use common\components\CommonDataModel;
use frontend\helpers\ImgHelper;
use frontend\helpers\FrontendHelper;

/**
 * @author art
 * @var $this yii\web\View
 * @var $model Apartment
 * @var $city common\models\City
 */
?>

<section class="gallery">
    <div class="wrap wrap_mobile_full">
        <?php if (!FrontendHelper::isNullOrEmpty($model->apartmentComplex->general_progress_percent)): ?>
            <div class="gallery__diagram gallery__diagram_tab wow fadeInRight">
                <div class="circle-progress" data-circle-progress="<?= $model->apartmentComplex->general_progress_percent ?>">
                    <div class="circle-progress__outer">
                        <div class="circle-progress__progress circle-progress__progress_10"></div>
                        <div class="circle-progress__progress circle-progress__progress_20"></div>
                        <div class="circle-progress__progress circle-progress__progress_30"></div>
                        <div class="circle-progress__progress circle-progress__progress_40"></div>
                        <div class="circle-progress__progress circle-progress__progress_50"></div>
                        <div class="circle-progress__progress circle-progress__progress_60"></div>
                        <div class="circle-progress__progress circle-progress__progress_70"></div>
                        <div class="circle-progress__progress circle-progress__progress_80"></div>
                        <div class="circle-progress__progress circle-progress__progress_90"></div>
                        <div class="circle-progress__progress circle-progress__progress_100"></div>
                        <div class="circle-progress__value"><?= $model->apartmentComplex->general_progress_percent ?></div>
                        <div class="circle-progress__percent">%</div>
                    </div>
                    <div class="circle-progress__label">
                        <?= $model->apartmentComplex->general_progress_title ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="gallery__content gallery__content_mob">
            <div class="gallery__diagram gallery__diagram_desk"></div>
            <h3 class="h3 gallery__h3 wow fadeInRight" data-wow-delay="0.3s">
                <?= $model->label ?>
            </h3>
            <div class="gallery__text wow fadeInRight" data-wow-delay="0.6s">
                <?php if (!FrontendHelper::isNullOrEmpty($model->subtype) && $model->isCommercial()) : ?>
                    <p>
                        <span><?= t('_Type', 'apartment') ?>:</span>
                        <strong><?= $model->getSubtypeLabel() ?></strong>
                    </p>
                <?php endif; ?>
                <?php if (!FrontendHelper::isNullOrEmpty($model->profile)) : ?>
                    <p>
                        <span><?= t('_Profile', 'apartment') ?>:</span>
                        <strong><?= $model->profile ?></strong>
                    </p>
                <?php endif; ?>
                <?php if (!FrontendHelper::isNullOrEmpty($model->income)) : ?>
                    <p>
                        <span><?= t('_Income', 'apartment') ?>:</span>
                        <strong><?= $model->income ?></strong>
                    </p>
                <?php endif; ?>
                <?php if (!FrontendHelper::isNullOrEmpty($model->recoupment)) : ?>
                    <p>
                        <span><?= t('_Recoupment', 'apartment') ?>:</span>
                        <strong><?= $model->recoupment ?></strong>
                    </p>
                <?php endif; ?>
                <?php if (!FrontendHelper::isNullOrEmpty($model->entrance)) : ?>
                    <p>
                        <span><?= t('_Entrance', 'apartment') ?>:</span>
                        <strong><?= $model->entrance ?></strong>
                    </p>
                <?php endif; ?>
                <?php if (!FrontendHelper::isNullOrEmpty($model->rooms_number)) : ?>
                    <p>
                        <span><?= t('_Rooms count', 'apartment') ?>:</span>
                        <strong><?= t('{n, plural, =0{_no_rooms} =1{# _1_room} one{# _one_room} few{# _few_rooms} many{# _many_rooms} other{# _other_rooms}}', 'apartment', ['n' => intval($model->rooms_number)]) ?></strong>
                    </p>
                <?php endif; ?>

                <?php if (!FrontendHelper::isNullOrEmpty($model->total_area)) : ?>
                    <p>
                        <span><?= t('_Total area', 'apartment') ?>:</span>
                        <strong><?= $model->total_area . ' ' . CommonDataModel::getAreaUnit() ?></strong>
                    </p>
                <?php endif; ?>

                <?php if (!FrontendHelper::isNullOrEmpty($model->floor)): ?>
                    <p>
                        <span><?= t('_Floor', 'apartment') ?>:</span>
                        <strong><?= $model->floor ?></strong>
                    </p>
                <?php endif; ?>

                <?php if (!FrontendHelper::isNullOrEmpty($model->delivery_date)) : ?>
                    <p>
                        <span><?= t('_Delivery date', 'apartment') ?>:</span>
                        <strong><?= Yii::$app->formatter->asDate($model->delivery_date, 'php:d-m-Y') ?></strong>
                    </p>
                <?php endif; ?>
                <?php if ($model->canShowPrice()) : ?>
                    <p>
                        <span><?= t('_Price from', 'apartment') ?>:</span>
                        <strong><?= $model->priceFormatted() ?></strong>
                    </p>
                    <p>
                        <span><?= t('_Price installment', 'apartment') ?>:</span>
                        <strong><?= $model->installmentPriceFormatted() ?></strong>
                    </p>
                <?php endif; ?>
            </div>
            <a href="#" data-href="<?= SiteUrlHelper::createFormExcursionPopupUrl(['apartmentId' => $model->id]) ?>" class="button button_green gallery__button ajax-link wow fadeInRight" data-wow-delay="0.9s">
                <?= t('_Schedule an excursion', 'apartment') ?>
                <span class="button__blip button__blip_hover"></span>
                <span class="button__blip button__blip_click"></span>
            </a>
        </div>

        <div class="gallery__wrap">
            <div class="gallery__slider wow fadeIn <?= count($model->photos) > 1 ? : 'clear_border' ?>" data-wow-delay="0.5s" data-wow-duration="2s" data-gallery-slider data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "dots": false, "arrows": false, "asNavFor": "[data-gallery-nav]"}'>
                <?php foreach ($model->photos as $photoEtF): ?>
                    <div data-fancybox="gallery" data-bg-size="contain" data-bg-pos="center" href="<?= ImgHelper::getImageSrcFromEtF($photoEtF, 'apartment', 'gallery') ?>">
                        <div class="gallery__photo" data-bg-src="<?= ImgHelper::getImageSrcFromEtF($photoEtF, 'apartment', 'gallery') ?>" data-bg-size="contain" data-bg-pos="center"></div>
                    </div>
                <?php endforeach; ?>
                <?php if (count($model->photos) == 0): ?>
                    <div class="gallery__photo" data-bg-src="<?= Apartment::NO_IMAGE_PATH ?>" data-bg-size="contain" data-bg-pos="center"></div>
                <?php endif; ?>
            </div>

            <?php if (count($model->photos) > 1) : ?>
                <div class="gallery__nav" data-gallery-nav data-slick='{"slidesToShow": 4, "slidesToScroll": 1, "dots": false, "asNavFor": "[data-gallery-slider]", "focusOnSelect": true, "infinite": false, "arrows": false, "responsive": [{"breakpoint": 1201, "settings": {"slidesToShow": 3}}, {"breakpoint": 480, "settings": {"slidesToShow": 2}}]}'>

                    <?php foreach ($model->photos as $photoEtF): ?>
                        <div class="gallery__item">
                            <div class="gallery__img" data-bg-src="<?= ImgHelper::getImageSrcFromEtF($photoEtF, 'apartment', 'galleryThumb') ?>" data-bg-size="cover" data-bg-pos="center"></div>
                        </div>
                    <?php endforeach; ?>

                    <?php if (count($model->photos) == 0): ?>
                        <div class="gallery__item">
                            <div class="gallery__img" data-bg-src="<?= Apartment::NO_IMAGE_PATH ?>" data-bg-size="cover" data-bg-pos="center"></div>
                        </div>
                    <?php endif; ?>

                </div>
                <button class="gallery__zoom">
                    <svg>
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search" /> </svg>
                </button>
            <?php endif; ?>
        </div>

        <div class="gallery__content gallery__content_desk">
            <?php if (!FrontendHelper::isNullOrEmpty($model->apartmentComplex->general_progress_percent)): ?>
                <div class="gallery__diagram gallery__diagram_desk wow fadeInRight">
                    <div class="circle-progress" data-circle-progress="<?= $model->apartmentComplex->general_progress_percent ?>">
                        <div class="circle-progress__outer">
                            <div class="circle-progress__progress circle-progress__progress_10"></div>
                            <div class="circle-progress__progress circle-progress__progress_20"></div>
                            <div class="circle-progress__progress circle-progress__progress_30"></div>
                            <div class="circle-progress__progress circle-progress__progress_40"></div>
                            <div class="circle-progress__progress circle-progress__progress_50"></div>
                            <div class="circle-progress__progress circle-progress__progress_60"></div>
                            <div class="circle-progress__progress circle-progress__progress_70"></div>
                            <div class="circle-progress__progress circle-progress__progress_80"></div>
                            <div class="circle-progress__progress circle-progress__progress_90"></div>
                            <div class="circle-progress__progress circle-progress__progress_100"></div>
                            <div class="circle-progress__value"><?= $model->apartmentComplex->general_progress_percent ?></div>
                            <div class="circle-progress__percent">%</div>
                        </div>
                        <div class="circle-progress__label">
                            <?= $model->apartmentComplex->general_progress_title ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <h3 class="h3 gallery__h3 wow fadeInRight" data-wow-delay="0.3s">
                <?= $model->label ?>
            </h3>
            <div class="gallery__text wow fadeInRight" data-wow-delay="0.6s">
                <?php if (!FrontendHelper::isNullOrEmpty($model->subtype) && $model->isCommercial()) : ?>
                    <p>
                        <span><?= t('_Type', 'apartment') ?>:</span>
                        <strong><?= $model->getSubtypeLabel() ?></strong>
                    </p>
                <?php endif; ?>
                <?php if (!FrontendHelper::isNullOrEmpty($model->profile)) : ?>
                    <p>
                        <span><?= t('_Profile', 'apartment') ?>:</span>
                        <strong><?= $model->profile ?></strong>
                    </p>
                <?php endif; ?>
                <?php if (!FrontendHelper::isNullOrEmpty($model->income)) : ?>
                    <p>
                        <span><?= t('_Income', 'apartment') ?>:</span>
                        <strong><?= $model->income ?></strong>
                    </p>
                <?php endif; ?>
                <?php if (!FrontendHelper::isNullOrEmpty($model->recoupment)) : ?>
                    <p>
                        <span><?= t('_Recoupment', 'apartment') ?>:</span>
                        <strong><?= $model->recoupment ?></strong>
                    </p>
                <?php endif; ?>
                <?php if (!FrontendHelper::isNullOrEmpty($model->entrance)) : ?>
                    <p>
                        <span><?= t('_Entrance', 'apartment') ?>:</span>
                        <strong><?= $model->entrance ?></strong>
                    </p>
                <?php endif; ?>
                <?php if (!FrontendHelper::isNullOrEmpty($model->rooms_number)): ?>
                    <p>
                        <span><?= t('_Rooms count', 'apartment') ?>:</span>
                        <strong>
                            <?= t('{n, plural, =0{_no_rooms} =1{# _1_room} one{# _one_room} few{# _few_rooms} many{# _many_rooms} other{# _other_rooms}}', 'apartment', ['n' => intval($model->rooms_number)]) ?>
                        </strong>
                    </p>
                <?php endif; ?>

                <?php if (!FrontendHelper::isNullOrEmpty($model->total_area)): ?>
                    <p>
                        <span><?= t('_Total area', 'apartment') ?>:</span>
                        <strong><?= $model->total_area . ' ' . CommonDataModel::getAreaUnit() ?></strong>
                    </p>
                <?php endif; ?>

                <?php if (!FrontendHelper::isNullOrEmpty($model->floor)): ?>
                    <p>
                        <span><?= t('_Floor', 'apartment') ?>:</span>
                        <strong><?= $model->floor ?></strong>
                    </p>
                <?php endif; ?>

                <?php if (!FrontendHelper::isNullOrEmpty($model->delivery_date)): ?>
                    <p>
                        <span><?= t('_Delivery date', 'apartment') ?>:</span>
                        <strong><?= Yii::$app->formatter->asDate($model->delivery_date, 'php:d-m-Y') ?></strong>
                    </p>
                <?php endif; ?>

                <?php if ($model->canShowPrice()): ?>
                    <p>
                        <span><?= t('_Price from', 'apartment') ?>:</span>
                        <strong><?= $model->priceFormatted() ?></strong>
                    </p>
                    <p>
                        <span><?= t('_Price installment', 'apartment') ?>:</span>
                        <strong><?= $model->installmentPriceFormatted() ?></strong>
                    </p>
                <?php endif; ?>
            </div>
            <a href="#" data-href="<?= SiteUrlHelper::createFormExcursionPopupUrl(['apartmentId' => $model->id]) ?>" class="ajax-link button button_green gallery__button wow fadeInRight" data-wow-delay="0.9s">
                <?= t('_Schedule an excursion', 'apartment') ?>
                <span class="button__blip button__blip_hover"></span>
                <span class="button__blip button__blip_click"></span>
            </a>
        </div>

    </div>

</section>
