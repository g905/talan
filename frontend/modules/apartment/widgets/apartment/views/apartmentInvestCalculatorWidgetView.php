<?php

/**
 * @var $this yii\web\View
 * @var $model common\models\Apartment
 */
?>

<!-- invest calculator -->
<section class="invest-calculator">
  <div class="wrap wrap_mobile_full">
    <div class="invest-calculator__columns">
      <div class="invest-calculator__column">
        <h2 class="h2 invest-calculator__h2 wow fadeInLeft">
          <?= t('Calculator', 'apartment/invest-block') ?>
            <strong><?= t('investment attractiveness', 'apartment/invest-block') ?></strong>
        </h2>
      </div>
      <div class="invest-calculator__column">
        <form action="/" method="POST" class="form invest-calculator__form wow fadeInUp" data-form="">
          <div class="form__row form__row_split_2">
            <div class="field form__field">
              <span class="label form__label">Стоимость:</span>
            </div>
            <div class="field form__field">
              <strong class="label form__label calculator_input__price">
              <span><?= $model->canShowPrice() ? (int)$model->price_from : 0 ?></span> р</strong>
            </div>
          </div>
          <div class="form__row form__row_split_2">
            <div class="field form__field">
              <span class="label form__label">Арендная ставка в месяц:</span>
            </div>
            <div class="field form__field">
              <input type="text" class="input form__input calculator_input__bid" name="" value="2060000">
              <span class="invest-calculator__form_currency">руб</span>
            </div>
          </div>
          <div class="form__row form__row_split_2 last">
            <div class="field form__field">
              <span class="label form__label">Возврат инвестиций:</span>
            </div>
            <div class="field form__field">
              <strong class="label form__label calculator_input__years">
                <span>5</span> лет</strong>
            </div>
            <div class="field form__field">
              <span class="label form__label">Доход в год:</span>
            </div>
            <div class="field form__field">
              <strong class="label form__label calculator_input__profit">
                <span>515 995</span> р</strong>
            </div>
          </div>
          <div class="form__row">
            <button type="button" class="button form__button  wow fadeInUp ajax-link" data-href="/form-commercial-popup">
              <?= t('Send request', 'apartment/invest-block') ?>
              <span class="button__blip button__blip_hover"></span>
              <span class="button__blip button__blip_click"></span>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<!-- / invest calculator -->
