<?php
/**
 * @author art
 * @var \common\models\Apartment $model
 * @var string $image
 */
?>

<div class="apartment-description apartment-description_left">
    <div class="wrap wrap_mobile_full">
        <div class="apartment-description__inner">
            <div class="apartment-description__content">
                <?php if ($model->description_title) : ?>
                    <h3 class="h3 apartment-description__h3 wow fadeInLeft"><?= $model->description_title ?></h3>
                <?php endif ?>
                <?php if ($model->description_subtitle) : ?>
                    <div class="apartment-description__number wow fadeInLeft" data-wow-delay="0.3s"><?= $model->description_subtitle ?></div>
                    <div class="apartment-description__divider wow fadeInLeft" data-wow-delay="0.3s"></div>
                <?php endif ?>
                <div class="apartment-description__text wow fadeInLeft" data-wow-delay="0.6s">
                    <p><?= $model->description_text ?></p>
                </div>
            </div>
        </div>
        <?php if ($image) : ?>
            <div class="apartment-description__img wow fadeIn" data-wow-delay="0.5s" data-wow-duration="2s" data-bg-src="<?= $image ?>" data-bg-pos="center bottom" data-bg-size="cover"></div>
        <?php endif ?>
    </div>
</div>
