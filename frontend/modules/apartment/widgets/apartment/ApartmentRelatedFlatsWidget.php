<?php

namespace frontend\modules\apartment\widgets\apartment;

use function Sodium\library_version_minor;
use yii\base\Widget;
use common\models\City;

/**
 * Class ApartmentRelatedFlatsWidget
 * @package frontend\modules\apartment\widgets\apartment
 *
 * @author dimarychek
 */
class ApartmentRelatedFlatsWidget extends Widget
{
    public $apartmentPage;

    public function run()
    {
        if (!isset($this->apartmentPage)) {
            return null;
        }

        $city = City::getCurrentCity(request()->get('city'));

        $related = $this->apartmentPage->getRelatedFlats();
        $related = array_chunk($related, 3);

        echo $this->render(
            'apartmentRelatedFlatsWidgetView',
            [
                'model' => $this->apartmentPage,
                'related' => $related,
                'city' => $city
            ]
        );
    }
}
