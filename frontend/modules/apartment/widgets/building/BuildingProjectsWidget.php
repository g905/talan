<?php
/**
 * Created by PhpStorm.
 * User: dimarychek
 */

namespace frontend\modules\apartment\widgets\building;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

class BuildingProjectsWidget extends Widget
{

    public $buildingPage;

    public function run()
    {
        if (!isset($this->buildingPage)) {
            return null;
        }

        if (count($this->buildingPage->projects) == 0) {
            return null;
        }

        $projects = $this->buildingPage->projects;

        echo $this->render('buildingProjectsWidgetView', [
            'model' => $this->buildingPage,
            'projects' => $projects
        ]);
    }
}
