<?php
/**
 * Created by PhpStorm.
 * User: dimarychek
 */

namespace frontend\modules\apartment\widgets\building;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

class BuildingActionWidget extends Widget
{
    public $buildingPage;

    public function run()
    {
        if (!isset($this->buildingPage)) {
            return null;
        }

        if (count($this->buildingPage->promoActions) == 0) {
            return null;
        }

        echo $this->render('buildingActionWidgetView', [
            'model' => $this->buildingPage
        ]);
    }


}
