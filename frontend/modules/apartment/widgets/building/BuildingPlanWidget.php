<?php

namespace frontend\modules\apartment\widgets\building;

use common\helpers\Property;
use common\models\ApartmentEntrance;
use common\models\Entrance;
use yii\base\Widget;
use yii\db\ActiveQuery;
use metalguardian\fileProcessor\helpers\FPM;
use common\models\Apartment;
use common\models\EntityToFile;
use common\helpers\SiteUrlHelper;
use common\models\ApartmentComplex;
use common\models\ApartmentBuilding;
use yii\db\Query;

/**
 * Class BuildingPlanWidget
 *
 * @package frontend\modules\apartment\widgets\building
 */
class BuildingPlanWidget extends Widget
{
    public $entrance;
    /** @var ApartmentBuilding */
    public $building;
    /** @var ApartmentComplex */
    private $complex;

    public function run()
    {
        $output = '';

        if ($this->building === null) {
            return $output;
        }

        $this->complex = $complex = $this->building->buildingComplex;
        $building = $this->building;
        $sections = $this->getPreparedData();

        $mapModel = \common\models\ApartmentMap::getByComplexId($complex->id);

        $entranceQuery = ApartmentEntrance::find()
            ->where(['building_id' => $this->building->id])
            ->andWhere(['published' => 1]);
        if ($this->entrance) {
            $entranceQuery->andWhere(['entrance' => $this->entrance]);
        }
        $entrance = $entranceQuery->all();

        if ($sections) {
            $output = $this->render('buildingPlanWidgetView', compact('sections', 'building', 'complex', 'mapModel', 'entrance'));
        }

        return $output;
    }

    private function getPreparedData()
    {
    	//dd(ApartmentBuilding::find()->where(['id' => $this->building->id])->asArray()->all());
        $building = ApartmentBuilding::find()
            ->alias('b')
            ->select(['b.id', 'b.entrance', 'b.floors'])
            ->andWhere(['b.id' => $this->building->id])
            ->innerJoinWith(['apartments' => function (ActiveQuery $q) {
                $q->select([
                    'id', 'type', 'apartment_building_id', 'label', 'price_from', 'installment_price',
                    'total_area', 'floor', 'apartment_number_on_floor', 'entrance', 'status', 'rooms_number'
                ])->andWhere(['deleted' => Apartment::NOT_DELETED])
                  //->andWhere(['=', 'type', Property::TYPE_COMMERCIAL])
                ->orderBy(['entrance' => SORT_ASC, 'floor' => SORT_ASC, 'apartment_number_on_floor' => SORT_ASC]);
            }])
            ->asArray();

            if ($this->entrance) {
                //dd($this->entrance);
                $building->innerJoinWith(['apartments' => function (ActiveQuery $q) {
                    $q->andFilterWhere(['apartment.entrance' => $this->entrance])
                        ->orderBy(['entrance' => SORT_ASC, 'floor' => SORT_ASC, 'apartment_number_on_floor' => SORT_ASC]);
                }]);
            }

        return $this->prepareSections($building->one());
    }

    private function prepareSections($building)
    {
        $output = [];
        if ($building) {
//            $section = $building['id'];
//            $floors = $building['floors'];

//            for ($i = 1; $i <= $floors; $i++) {
//                $output[$section]['floorsCount'] = $floors;
//                $output[$section]['floors'][$i]['apartments'] = [];
//            }

            foreach ($building['apartments'] as $apartment) {
                $section = $apartment['entrance'];
                $floor = $apartment['floor'];
                $numberOnFloor = $apartment['apartment_number_on_floor'];
                $apartment['url'] = $this->obtainApartmentUrl($apartment['id'], $apartment['type']);
                $apartment['image'] = $this->obtainApartmentImage($apartment['id']);
                $output[$section]['floorsCount'] = $building['floors'];
                $output[$section]['floors'][$floor]['apartments'][$numberOnFloor] = $apartment;
            }
        }
//dd($output);
        return $output;
    }

    private function obtainApartmentUrl($apartmentId, $apartmentType)
    {
        $url = '#';
        if ($apartmentId && $apartmentType) {
            $url = SiteUrlHelper::createApartmentUrl([
                'id' => $apartmentId,
                'alias' => $this->complex->alias,
                'type' => $apartmentType
            ]);
        }

        return $url;
    }

    private function obtainApartmentImage($apartmentId)
    {
        $path = '/static/img/apartments/apartaments-hover.png';
        $model = EntityToFile::find()->where([
            'entity_model_id' => $apartmentId,
            'entity_model_name' => 'Apartment',
            'attribute' => Apartment::SAVE_ATTRIBUTE_PHOTOS
        ])->asArray()->orderBy('position ASC')->one();

        if ($model !== null) {
            $path = FPM::src(obtain('file_id', $model), 'apartment', 'plan');
        }

        return $path;
    }
}
