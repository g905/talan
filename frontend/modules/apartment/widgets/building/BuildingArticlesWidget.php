<?php

namespace frontend\modules\apartment\widgets\building;

use common\models\ApartmentBuilding;
use yii\base\Widget;

/**
 * Class ArticlesWidget
 *
 * @package frontend\modules\hr\widgets
 */
class BuildingArticlesWidget extends Widget
{
    /**
     * @var ApartmentBuilding
     */
    public $buildingPage;

    public function run()
    {
        $articles = $this->buildingPage->articles;

        if (count($this->buildingPage->articles) == 0) {
            return null;
        }

        echo $this->render('buildingArticlesWidgetView', [
            'model' => $this->buildingPage,
            'articles' => $articles
        ]);
    }
}
