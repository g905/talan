<?php
/**
 * Created by PhpStorm.
 * User: dimarychek
 */

namespace frontend\modules\apartment\widgets\building;

use yii\base\Widget;

class BuildingTopBlockWidget extends Widget
{

    public $buildingPage;

    public function run()
    {
        if (!isset($this->buildingPage)) {
            return null;
        }

        echo $this->render('buildingTopBlockWidgetView', [
            'model' => $this->buildingPage
        ]);
    }
}
