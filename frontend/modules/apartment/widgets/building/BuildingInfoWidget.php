<?php
/**
 * Created by PhpStorm.
 * User: dimarychek
 */

namespace frontend\modules\apartment\widgets\building;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

class BuildingInfoWidget extends Widget
{
    public $buildingPage;

    public function run()
    {
        if (!isset($this->buildingPage)) {
            return null;
        }

        if ((count($this->buildingPage->generalPhotos) == 0) &&
            (FrontendHelper::isNullOrEmpty($this->buildingPage->general_progress_percent)) &&
            (FrontendHelper::isNullOrEmpty($this->buildingPage->general_description))) {
            return null;
        }

        echo $this->render('buildingInfoWidgetView', [
            'model' => $this->buildingPage
        ]);
    }
}
