<?php
namespace frontend\modules\apartment\widgets\building;

use yii\base\Widget;

class TlNewBuildingAdvantagesWidget extends Widget {

	public $complexPage;

	public function run()
	{
		if (!isset($this->complexPage)) {
			return null;
		}

		if (count($this->complexPage->advantages) == 0) {
			return null;
		}

		echo $this->render('tlNewBuildingAdvantagesView', [
			'model' => $this->complexPage
		]);
	}
}