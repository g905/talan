<?php

namespace frontend\modules\apartment\widgets\building;

use yii\base\Widget;
use common\models\ApartmentBuilding;

class BuildingHistoryWidget extends Widget
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var ApartmentBuilding
     */
    public $model;

    /**
     * @return string|null
     */
    public function run()
    {
        if (empty($this->model->build_start_date) || empty($this->model->build_end_date)) {
            return null;
        }

        return $this->render('buildingHistoryWidgetView', [
            'title' => $this->title,
            'model' => $this->model,
        ]);
    }
}
