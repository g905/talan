<?php
/**
 * Created by PhpStorm.
 * User: dimarychek
 */

namespace frontend\modules\apartment\widgets\building;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

class BuildingInvestmentWidget extends Widget
{
    public $buildingPage;

    public function run()
    {
        if (!isset($this->buildingPage)) {
            return null;
        }

        echo $this->render('buildingInvestmentWidgetView', [
            'model' => $this->buildingPage
        ]);
    }


}
