<?php

use frontend\helpers\ImgHelper;
use common\models\ApartmentBuilding;
use common\models\CompletedProject;

/**
 * @author dimarychek
 * @var ApartmentBuilding $model
 * @var CompletedProject $projects
 */
?>

<section class="values done_projects tal-section__done-projects">
    <div class="wrap wrap_mobile_full">
        <div class="done_projects__title">
            <h2 class="h2 values__h2 wow fadeInLeft"><?= $model->projects_label ?></h2>
            <p class="p values__p wow fadeInLeft"><?= $model->projects_description ?></p>
        </div>
        <div class="values__list">
            <?php for ($i = 0; $i < count($projects) && $i < 3; $i++) : ?>
                <div class="card values__card wow fadeInUp">
                    <div class="card__img" data-bg-src="<?= $projects[$i]->getPhotoPreviewSrc() ?>" data-bg-pos="center" data-bg-size="cover"></div>
                    <h5 class="h5 card__h5"><?= $projects[$i]->title ?></h5>
                    <div class="divider card__divider"></div>
                    <div class="card__text">
                        <?= $projects[$i]->description ?>
                    </div>
                </div>
            <?php endfor; ?>
            <?php if (count($projects) > 3) : ?>
                <div class="values__list" style="display: none" data-list-items="done-projects">
                    <?php for ($i = 3; $i < count($projects); $i++) : ?>
                        <div class="card values__card wow fadeInUp">
                            <div class="card__img" data-bg-src="<?= $projects[$i]->getPhotoPreviewSrc() ?>" data-bg-pos="center" data-bg-size="cover"></div>
                            <h5 class="h5 card__h5"><?= $projects[$i]->title ?></h5>
                            <div class="divider card__divider"></div>
                            <div class="card__text">
                                <?= $projects[$i]->description ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
                <div class="divider_ajax">
                    <div class="line"></div>
                    <button class="footer__button" data-list-show="done-projects">
                        <span class="footer__button-hide-text">все проекты</span>
                        <span class="footer__button-show-text">скрыть</span>
                    </button>
                </div>
            <?php else : ?>
                <div class="divider_ajax">
                    <div class="line"></div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>