<?php

use common\models\Apartment;
use common\helpers\SiteUrlHelper;
use metalguardian\fileProcessor\helpers\FPM;

/**
 * @var $this yii\web\View
 * @var $building common\models\ApartmentBuilding
 * @var $complex common\models\ApartmentComplex
 * @var $sections array
 */
?>

<?php $map_img = FPM::originalSrc($mapModel->image->file_id ?? '') ?>

<style>
    .choose-house__back-link_wrap {
        width: 100%;
        /*background: rgba(47,19,255,0.24);*/
        background-image: url("<?= $map_img?>");
        background-size: cover;
        height: 50px;
        margin-bottom: 30px;
        justify-content: center;
        display: inline-flex;
    }
    .choose-house__back-link {
        height: 40px;
        line-height: 40px;
        text-align: center;
        width: fit-content;
        padding: 0 30px;
        background: #FFC66D;
        margin: auto;
        border-radius: 20px;
        transition: background .15s ease-in-out;
    }

    .choose-house__back-link:hover {
        height: 40px;
        line-height: 40px;
        text-align: center;
        width: fit-content;
        padding: 0 30px;
        background: #FFF;
        margin: auto;
        border-radius: 20px;
    }

    .plan-wrap {
        width: 60%;
        display: inline-block;
        /*position: absolute;
        right: 10%;*/
        float: right;
    }

    .chess-wrap {
        width: 100%;
    }

    .floor-plans {
        width: 100%;
        border-left: 1px solid gray;
        padding-bottom: 57px;
    }

    .floor-plan-images-wrap {
        position: relative;
    }

    .floor-plan-image {
        display: none;
        text-align: center;
        width: 100%;
    }

    .floor-plan-image.active {
        display: block;
    }
    .floor-plan-image.active img {
        max-height: 400px;
        width: auto;
    }
    .floor-plan-titles {
        padding: 10px;
    }
    .plan-button {
        width: fit-content;
        padding: 0 10px;
        margin: 0 10px;
    }
    .plan-button.btn-active {
        background: #22a662;
        color: #fff;
    }
    .entrance {
        width: 100%;
        position: relative;
    }
    .choose-aphouse__section {
        padding: 19px 21px 0!important;
    }
    @media screen and (max-width: 768px) {
        .plans-wrap {
            display: block;
        }
        .plan-wrap {
            display: none;
        }
        .chess-wrap {
             width: 100%;
         }
    }
</style>

<section class="choose-aphouse tal-section tal-section__chess">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 new-quality__h2 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">Планировки</h2>
        <div class="choose-house__lists">
            <a href="<?= SiteUrlHelper::createApartmentComplexConfiguratorUrl(['alias' => $complex->alias, 'type' => $complex->type]) ?>" class="choose-house__items ">Выбор дома</a>
            <a href="#" class="choose-house__items choose-house__items--active">Выбор квартиры</a>
            <a href="#" class="choose-house__items ">Выбор отделки</a>
        </div>

        <div class="choose-house__back-link_wrap">
            <div class="choose-house__back-link">
                <a href="<?= SiteUrlHelper::createApartmentComplexConfiguratorUrl(['alias' => $complex->alias, 'type' => $complex->type]) ?>">
                    Вернуться к выбору секции
                </a>
            </div>
        </div>


        <div class="plans-wrap" style="display: flex;">

            <div class="chess-wrap">
                <div class="choose-aphouse__legend">
                    <div class="choose-aphouse__info">
                        <div class="choose-aphouse__flat-box">
                            <span class="choose-aphouse__flat choose-aphouse__flat"></span>
                            <span class="choose-aphouse__info-text">свободна</span>
                        </div>
                        <div class="choose-aphouse__flat-box">
                            <span class="choose-aphouse__flat choose-aphouse__flat-booked"></span>
                            <span class="choose-aphouse__info-text">забронирована</span>
                        </div>
                        <div class="choose-aphouse__flat-box">
                            <span class="choose-aphouse__flat choose-aphouse__flat-sold"></span>
                            <span class="choose-aphouse__info-text">продана</span>
                        </div>
                    </div>
                    <?php $sectionImage = $building->getSectionsImageSrc() ?>
                    <?php if ($sectionImage) : ?>
                        <div class="choose-aphouse__section-img">
                            <span>секции</span>
                            <img src="<?= $sectionImage ?> " alt="img-section">
                        </div>
                    <?php endif ?>
                </div>

                <div class="choose-aphouse__complex">
                    <?php $i = 0; ?>
                    <?php foreach ($sections as $sectionNumber => $section) : ?>

                    <div class="entrance">
                        <div class="choose-aphouse__section">
                            <?php foreach ($section['floors'] as $floorNumber => $floor) : ?>
                                <div class="choose-aphouse__row">
                                    <span class="choose-aphouse__numbers"><?= $floorNumber ?></span>
                                    <?php foreach ($floor['apartments'] as $apartment) : ?>
                                        <?php $status = obtain('status', $apartment) ?>
                                        <?php if ($status == Apartment::STATUS_AVAILABLE) : ?>
                                            <div class="choose-aphouse__link" onclick="window.location.href='<?= SiteUrlHelper::getApartmentUrl(['type' => $complex->type, 'alias' => $complex->alias, 'id' => $apartment['id']/*, '#' => 'constructor_anchor'*/]) ?>'">
                                                <?= obtain('rooms_number', $apartment) ?>
                                                <div class="choose-aphouse__flats">
                                                    <div class="choose-aphouse__close-btn"></div>
                                                    <a href="<?= SiteUrlHelper::getApartmentUrl(['type' => $complex->type, 'alias' => $complex->alias, 'id' => $apartment['id']/*, '#' => 'constructor_anchor'*/]) ?>" class="choose-aphouse__flats-link">
                                                        <div class="choose-aphouse__flats-img">
                                                            <img src="<?= obtain('image', $apartment, '') ?>" alt="flat">
                                                        </div>
                                                    </a>
                                                    <a href="<?= SiteUrlHelper::getApartmentUrl(['type' => $complex->type, 'alias' => $complex->alias, 'id' => $apartment['id']/*, '#' => 'constructor_anchor'*/]) ?>">
                                                        <div class="choose-aphouse__flats-title"><?= obtain('label', $apartment, 'Квартира') ?></div>
                                                    </a>
                                                    <!--
                                                    <div class="choose-aphouse__flats-price">
                                                        от <span class="prices-box"><?= obtain('price_from', $apartment, 0) ?></span> руб
                                                    </div>
                                                    <div class="choose-aphouse__flats-price">
                                                        <?= Yii::t('apartment/chess', '_installment') ?> <span class="prices-box"><?= obtain('installment_price', $apartment, 0) ?></span> руб
                                                    </div>
                                                    -->
                                                    <div class="choose-aphouse__flats-price">
                                                        <?php if($complex->apartment_chess_text):?>
                                                            <?= $complex->apartment_chess_text?>
                                                        <?php else:?>
                                                            <div class="choose-aphouse__flats-price">
                                                                от <span class="prices-box"><?= obtain('price_from', $apartment, 0) ?></span> руб
                                                            </div>
                                                            <div class="choose-aphouse__flats-price">
                                                                <?= Yii::t('apartment/chess', '_installment') ?> <span class="prices-box"><?= obtain('installment_price', $apartment, 0) ?></span> руб
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                    <span class="choose-aphouse__flats-area"><?= obtain('total_area', $apartment, 0) ?> м²</span>
                                                    <span class="choose-aphouse__flats-status">Свободна</span>
                                                </div>
                                            </div>
                                        <?php elseif ($status == Apartment::STATUS_SOLD) : ?>
                                            <div class="choose-aphouse__link choose-aphouse__link-sold"></div>
                                        <?php else : ?>
                                            <div class="choose-aphouse__link choose-aphouse__link-booked"></div>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </div>
                            <?php endforeach ?>
                            <div class="choose-aphouse__section-title">Секция <?= $sectionNumber ?></div>
                        </div>

                        <div class="plan-wrap">
                            <div class="floor-plans">


                                    <div class="floor-plan-titles" style="display: flex;">
                                        <?php foreach($entrance[$i]->floorPlans as $index=>$floorPlan):?>
                                            <button style="border: 1px solid #1e9b57" id="plan-<?=$i?>-<?=$index?>" type="button" class="button plan-button wow fadeInUp <?= $index == 0 ? 'btn-active' : '' ?>" plan-index="<?= $index?>" style="visibility: visible; animation-name: fadeInUp;">
                                                <?= $floorPlan->title?>
                                            </button>
                                        <?php endforeach;?>
                                    </div>
                                    <div class="floor-plan-images-wrap">
                                        <?php foreach($entrance[$i]->floorPlans as $index=>$floorPlan):?>
                                            <?php //dd($floorPlan)?>
                                            <div class="floor-plan-image <?= $index == 0 ? 'active' : '' ?>" id="plan-image-<?=$i?>-<?= $index?>">
												<?php $imgUrl = FPM::originalSrc($floorPlan->getImage()->one()->file_id); ?>
												<a href="<?= $imgUrl; ?>" class="tlnLightBox" data-lightbox="<?= $index . '-' . $i;?>">
													<img style="" src="<?= $imgUrl; ?>">
												</a>
                                            </div>
                                        <?php endforeach;?>
                                    </div>



                            </div>
                        </div>


                        </div>
                    <?php $i++?>
                    <?php endforeach ?>
                </div>
            </div>

        </div>
        </div>
    </div>
</section>
<?php
$this->registerJs(
/*"$('.plan-button').on('click', function() {
        $('.floor-plan-image').removeClass('active');
        $('.plan-button').removeClass('btn-active');
        id = this.id.substr(this.id.length-1,1);
        img = $('#plan-image-'+id);
        btn = $('#plan-'+id);
        btn.addClass('btn-active');
        img.addClass('active');
        
    });"*/
"
    let entrances = $('.entrance');
    entrances.each(function(index, entrance) {
        $(entrance).find('.plan-button').on('click', function(e){
            $(entrance).find('.plan-button').removeClass('btn-active');
            $(e.target).addClass('btn-active');
            let ind = e.target.id;
            ind = ind.substr(ind.length - 3);
            let img = $('#plan-image-'+ind);
            $(entrance).find('.floor-plan-image').removeClass('active');
            img.addClass('active');
        })
    })
    ",
\yii\web\View::POS_END,
'floor-plan-button-handler'
);

\frontend\assets\LightBoxAsset::register($this);