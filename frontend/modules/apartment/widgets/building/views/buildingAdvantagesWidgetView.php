<?php

use common\models\ApartmentBuilding;
use common\models\blocks\BuildingAdvantage;

/**
 * @author dimarychek
 * @var ApartmentBuilding $model
 * @var BuildingAdvantage $advantages
 */
?>

<div class="new-quality">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 new-quality__h2 wow fadeInUp"> <?= $model->advantages_title_first_part ?>
            <br>
            <span><?= $model->advantages_title_second_part ?></span>
        </h2>
        <div class="new-quality__list">
            <div class="card card_large new-quality__card">
                <div class="card__content">
                    <h3 class="h3 card__h3 wow fadeInLeft">
                        <span class="card_span"><?= $advantages[0]->title ?></span>
                    </h3>
                    <div class="card__number wow fadeInLeft" data-wow-delay="0.3s"><?= $model->getPositionFormatted(0) ?></div>
                    <div class="card__divider wow fadeInLeft" data-wow-delay="0.3s"></div>
                    <div class="card__text wow fadeInLeft" data-advantage-content data-wow-delay="0.6s">
                        <?= $advantages[0]->description ?>
                    </div>
                    <div class="card__blur wow fadeInLeft" data-wow-delay="1.2s" data-blur-advantage>
                        <button class="card__btn" data-btn-advantage>Развернуть</button>
                    </div>
                </div>
                <div class="card__img_desk wow fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                    <img class="card__img" src="<?= $advantages[0]->getImageSrc() ?>" alt="">
                </div>
            </div>
            <?php for ($i = 1; $i < count($advantages); $i++) : ?>
                <div class="card new-quality__card wow fadeInUp">
                    <div class="card__img_desk">
                        <img class="card__img" src="<?= $advantages[$i]->getImageSrc() ?>" alt="">
                    </div>
                    <div class="card__content">
                        <h3 class="h3 card__h3">
                            <span class="card_span"><?= $advantages[$i]->title ?></span>
                        </h3>
                        <div class="card__number"><?= $model->getPositionFormatted($i) ?></div>
                        <div class="card__divider"></div>
                        <div class="card__text" data-advantage-content><?= $advantages[$i]->description ?></div>
                        <div class="card__blur wow fadeInLeft" data-wow-delay="1.2s" data-blur-advantage>
                            <button class="card__btn" data-btn-advantage>Развернуть</button>
                        </div>
                    </div>
                    <div class="card__img card__img_mob" data-bg-src="<?= $advantages[$i]->getImageSrc() ?>" data-bg-pos="center" data-bg-size="cover"></div>
                </div>
            <?php endfor; ?>
        </div>
    </div>
</div>
