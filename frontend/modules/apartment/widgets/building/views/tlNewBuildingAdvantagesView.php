<?php
use yii\helpers\Html;
use frontend\helpers\ImgHelper;
use \frontend\modules\apartment\assets\TlNewComplexPartAsset;

if(is_array($model->advantages) && count($model->advantages)):
	$imgDocumentationLink = ImgHelper::getFileSrc($model, 'viewDocumentation');
	$documentLinkName = (!empty($model->general_docs_link_title) ? $model->general_docs_link_title : 'Посмотреть документацию');
?>
<section class="comfortt">
	<div class="wrap wrap_large wrap_mobile_full">
		<h2 class="h2_tln_ncav">
            Преимущества
			<span class="h2span_tln_ncav"><?=$model->label?></span>
		</h2>
		<div class="slider-comfort" data-comfort-slider>
			<?php for ($ind2 = 0; $ind2 < count($model->advantages); $ind2++): ?>
				<div>
					<div class="slider-comfort-item">
						<div class="slider-comfort-item-img">
							<img class="card__img" src="<?= ImgHelper::getImageSrc($model->advantages[$ind2], 'complex', 'advantagesTop', 'image') ?>" alt="<?= $ind2 ?>"/>
						</div>
						<div class="card__content">
							<h3 class="h3 card__h3">
								<span class="card_span"><?= $model->advantages[$ind2]->title ?></span>
							</h3>
							<div class="card__number" data-comfort-status-current></div>
							<div class="card__divider"></div>
							<div class="card__text" data-comfort-content style="height: 140px; overflow: hidden;">
								<?= Html::decode($model->advantages[$ind2]->description) ?>
							</div>
							<div class="card__blur wow fadeInLeft card__blur_show" data-wow-delay="1.2s" data-blur-comfort="" style="visibility: visible; animation-delay: 1.2s; animation-name: fadeInLeft;">
								<button class="card__btn" data-btn-comfort="">Развернуть</button>
							</div>

						</div>
					</div>
				</div>
			<?php endfor; ?>

		</div>

		<div class="slider-comfort-nav">
			<?php for ($ind3 = 0; $ind3 < count($model->advantages); $ind3++): ?>
                <?php
                if($ind3==0){
                    $first='first';
                }
                else{
                    $first='';
                }
                ?>
			<div class="myadv">
				<div class="slider-comfort-nav-item <?= $first ?>" data-slide="<?= $ind3 + 1; ?>"><?= $model->advantages[$ind3]->title ?></div>
			</div>
			<?php endfor; ?>
			<div class="readiness__subscribe wow fadeInRight" data-wow-delay="0.9s" style="visibility: visible; animation-delay: 0.9s; animation-name: fadeInRight;">
				<p>Подписаться:</p>
				<a href="https://vk.com/talanizh/" class="readiness__icon socicon-vkontakte" target="_blank"></a>
				<a href="https://www.facebook.com/talan.izh/" class="readiness__icon socicon-facebook" target="_blank"></a>
				<a href="https://www.instagram.com/talan_izhevsk/" class="readiness__icon socicon-instagram" target="_blank"></a>
			</div>
		</div>

		<div class="comfort-arrows">
			<div class="comfort-arrows__wrap">
				<button class="navigation__btn comfort-arrows__btn_prev">
					<div class="arrow arrow_left navigation__arrow"></div>
				</button>
				<div class="navigation__status">
					<span class="navigation__current" data-comfort-status-current></span> \
					<span class="navigation__total" data-comfort-status-total></span>
				</div>
				<button class="navigation__btn comfort-arrows__btn_next">
					<div class="arrow arrow_right navigation__arrow"></div>
				</button>
			</div>
		</div>

		<div class="links-complex">
			<?php if(!empty($model->general_docs_link)):?>
				<a href="<?= $model->general_docs_link; ?>" class="more readiness__more wow fadeInRight"
					data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInRight;"
					target="_blank"><?= $documentLinkName; ?></a>
			<?php endif;?>
			<?php if(!empty($imgDocumentationLink)): ?>
				<a href="<?= $imgDocumentationLink; ?>"
					class="more readiness__more wow fadeInRight" data-wow-delay="0.6s"
					style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInRight;" target="_blank" download>
					Скачать презентацию
				</a>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php
endif;

TlNewComplexPartAsset::register($this);