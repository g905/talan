<?php

use common\helpers\SiteUrlHelper;
use frontend\helpers\ImgHelper;
use frontend\modules\page\widgets\PageSliderWidget;

/**
 * @author dimarychek
 * @var $this yii\web\View
 * @var $model common\models\ApartmentBuilding
 */
$link = $model->top_screen_btn_link ?: SiteUrlHelper::createApartmentBuildingApartmentsListUrl(['alias' => $model->alias]);
$linkLabel = $model->top_screen_btn_label ?: t('_choose apartment', 'apartment-complex');
?>

<section class="poster poster_padding">
    <?= PageSliderWidget::widget([
        'imagePath' => ImgHelper::getFileSrc($model, 'topScreenBackgroundImage'),
        'videoPath' => ImgHelper::getFileSrc($model, 'topScreenBackgroundVideo'),
    ]) ?>
    <div class="poster__inner">
        <div class="poster__content">
            <?php if ($model->show_label) : ?>
                <h1 class="h1 poster__h1 wow fadeInUp"><?= $model->label ?></h1>
            <?php endif ?>
            <?php if ($model->top_screen_description) : ?>
                <h5 class="h5 poster__h5 wow fadeInUp" data-wow-delay="0.3s"> <?= $model->top_screen_description ?> </h5>
            <?php endif ?>
            <a href="<?= $link ?>" class="button button_transparent poster__button wow fadeInUp" data-wow-delay="0.3s">
                <?= $linkLabel ?>
                <div class="button__blip button__blip_hover"></div>
                <div class="button__blip button__blip_click"></div>
            </a>
        </div>
    </div>
    <div class="down poster__down wow down-to-top">
        <button class="down__button" data-scroll-down></button>
        <div class="down__label"><?= t('_down', 'home') ?></div>
    </div>
</section>
