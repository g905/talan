<?php

use common\helpers\SiteUrlHelper;

/**
 * @var $title string
 * @var $model common\models\ApartmentBuilding
 */
?>
<section class="course_of_construction" data-url="<?= SiteUrlHelper::getApartmentBuildingHistoryInfo(['id' => $model->id]) ?>">
    <!-- data-method="post" for POST request -->
    <div class="wrap wrap_mobile_full course_of_construction__wrap">
        <h2 class="h2 course_of_construction__h2 wow fadeInLeft"> <?= $title ?> </h2>
		<div class="course_of_construction__legend_container">
			<div class="course_of_construction__legend">
				<div class="course_of_construction__legend-item">
					<span></span> <?= Yii::t('front/history', 'plan_history') ?> </div><br/>
				<div class="course_of_construction__legend-item">
					<span></span> <?= Yii::t('front/history', 'fact_history') ?> </div>
			</div>
		</div>
        <div class="construction_wrap wow fadeInUp">
            <ul class="construction-nav-years"> </ul>
            <div class="construction-timeline-wrap">
                <div class="construction-timeline-overflow">
                    <ul class="construction-timeline"> </ul>
                    <span class="construction-timeline-actual"></span>
                    <span class="construction-timeline-planned"></span>
                </div>
            </div>
            <div class="construction-content">
                <div class="construction-content__date" data-advantage-content>
                    <h3 class="construction-content__date-title wow fadeInUp" data-wow-delay="0.2s"></h3>
                    <p class="construction-content__date-desc wow fadeInUp" data-wow-delay="0.3s"></p>
                    <div class="card__blur wow fadeInLeft" data-wow-delay="1.2s" data-blur-advantage>
                        <button class="card__btn" data-btn-advantage>Развернуть</button>
                    </div>
                    <!--<a href="#" class="button button_green construction-content__date-btn wow fadeInUp" data-wow-delay="0.6s"> смотреть онлайн
                        <span class="button__bg"></span>
                        <span class="button__blip button__blip_hover"></span>
                        <span class="button__blip button__blip_click"></span>
                    </a>-->
                </div>
                <div class="construction-slider-wrap">
                    <div class="main-slider-wrap">
                        <ul class="construction-slider wow fadeInUp" data-wow-delay="0.2s"> </ul>
                        <button class="construction_zoom wow zoomIn" data-wow-delay="0.3s">
                            <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search" /></svg>
                        </button>
                    </div>
                    <ul class="construction-slider-nav wow fadeInUp" data-wow-delay="0.4s"> </ul>
                </div>
                <div class="construction-arrows wow fadeInUp" data-wow-delay="0.5s">
                    <div class="construction-arrows__line"></div>
                    <div class="construction-arrows__wrap">
                        <button class="navigation__btn construction-arrows__btn_prev">
                            <div class="arrow arrow_left navigation__arrow"></div>
                        </button>
                        <div class="navigation__status">
                            <span class="navigation__current" data-construction-status-current></span> \
                            <span class="navigation__total" data-construction-status-total></span>
                        </div>
                        <button class="navigation__btn construction-arrows__btn_next">
                            <div class="arrow arrow_right navigation__arrow"></div>
                        </button>
                    </div>
                </div>
                <div class="construction-content__date construction-content__date_mobile">
                    <h3 class="construction-content__date-title wow fadeInUp" data-wow-delay="0.2s"></h3>
                    <p class="construction-content__date-desc wow fadeInUp" data-wow-delay="0.3s"></p>
                    <!--<a href="#" class="button button_green construction-content__date-btn wow fadeInUp" data-wow-delay="0.6s"> смотреть онлайн
                        <span class="button__bg"></span>
                        <span class="button__blip button__blip_hover"></span>
                        <span class="button__blip button__blip_click"></span>
                    </a>-->
                </div>
            </div>
        </div>
    </div>
</section>
