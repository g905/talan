<?php
/**
 * Created by PhpStorm.
 * User: dimarychek
 */

use common\helpers\SiteUrlHelper;
use frontend\helpers\ImgHelper;
use common\models\ApartmentBuilding;

/**
 * @var ApartmentBuilding $model
 */
?>

<section class="rules tal-section__rules">
    <div class="wrap wrap_mobile_full">
        <h2 class="rules__h2 wow fadeInUp"><?= $model->investment_title_first ?>
            <br>
            <span class="tal-section__span"> <?= $model->investment_title_second ?></span>
        </h2>
    </div>
    <div class="rules__list">
        <div class="card card_left rules__card">
            <div class="wrap wrap_mobile_full">
                <div class="card__inner">
                    <div class="card__content tal-section__content">
                        <div class="card__text wow fadeInLeft" data-wow-delay="0.8s">
                            <?= $model->investment_description ?>
                            <?php if ($model->investment_btn_label && $model->investment_btn_link) : ?>
                                <a href="<?= $model->investment_btn_link ?>" class="button button_green tal-section__rules-btn gallery__button">
                                    <?= $model->investment_btn_label ?>
                                    <div class="button__blip button__blip_hover"></div>
                                    <div class="button__blip button__blip_click"></div>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="card__img wow fadeIn" data-bg-src="<?= ImgHelper::getFileSrc($model, 'investmentPhoto') ?>" data-bg-pos="center bottom" data-bg-size="cover" data-wow-duration="2s" data-wow-delay="0.5s"></div>
            </div>
        </div>
    </div>
</section>

