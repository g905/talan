<?php

use common\helpers\SiteUrlHelper;
use common\models\ApartmentBuilding;

/**
 * @author dimarychek
 * @var ApartmentBuilding $model
 * @var $this yii\web\View
 * @var $title string
 * @var $articles common\models\Article[]
 * @var $cssClass string
 */
?>

<section class="articles tal-section__journal">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 tal-section__journal-title wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;"><?= $model->articles_label ?></h2>
        <div class="articles__list">
            <div class="card articles__card articles__card_left wow fadeInUp">
                <div class="card__img" data-bg-src="<?= $articles[0]->getPreviewImageSrc() ?>" data-bg-pos="center" data-bg-size="cover">
                    <a href="<?= $articles[0]->viewUrl() ?>"></a>
                </div>
                <div class="card__content">
                    <h5 class="h5 card__h5">
                        <a href="<?= $articles[0]->viewUrl() ?>"><?= $articles[0]->label ?></a>
                    </h5>
                    <span class="card__date"><?= $articles[0]->getDisplayableListDate() ?></span>
                    <span class="divider card__divider"></span>
                    <ul class="card__tags">
                        <?php foreach ($articles[0]->blogThemes as $theme) : ?>
                            <li>
                                <a href="<?= SiteUrlHelper::getBlogAllUrl(['theme' => $theme->alias]) ?>"><?= $theme->label ?></a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                    <div class="card__text">
                        <?= $articles[0]->short_description ?>
                    </div>
                </div>
            </div>

            <?php for ($i = 1; $i < count($articles); $i++) : ?>
                <div class="card articles__card  wow fadeInUp">
                    <div class="card__img" data-bg-src="<?= $articles[$i]->getPreviewImageSrc() ?>" data-bg-pos="center" data-bg-size="cover">
                        <a href="<?= $articles[$i]->viewUrl() ?>"></a>
                    </div>
                    <div class="card__content">
                        <h5 class="h5 card__h5">
                            <a href="<?= $articles[$i]->viewUrl() ?>"><?= $articles[$i]->label ?></a>
                        </h5>
                        <span class="card__date"><?= $articles[$i]->getDisplayableListDate() ?></span>
                        <span class="divider card__divider"></span>
                        <ul class="card__tags">
                            <?php foreach ($articles[$i]->blogThemes as $theme) : ?>
                                <li>
                                    <a href="<?= SiteUrlHelper::getBlogAllUrl(['theme' => $theme->alias]) ?>"><?= $theme->label ?></a>
                                </li>
                            <?php endforeach ?>
                        </ul>
                        <div class="card__text">
                            <?= $articles[$i]->short_description ?>
                        </div>
                    </div>
                </div>
            <?php endfor ?>
        </div>
        <div class="view-all">
            <div class="view-all__line"></div>
            <a href="<?= SiteUrlHelper::getBlogAllUrl() ?>" class="more view-all__more"> все статьи </a>
        </div>
    </div>
</section>
