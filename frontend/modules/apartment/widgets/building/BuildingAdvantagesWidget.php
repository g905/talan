<?php
/**
 * Created by PhpStorm.
 * User: dimarychek
 */

namespace frontend\modules\apartment\widgets\building;

use frontend\helpers\FrontendHelper;
use yii\base\Widget;

class BuildingAdvantagesWidget extends Widget
{

    public $buildingPage;

    public function run()
    {
        if (!isset($this->buildingPage)) {
            return null;
        }

        if (count($this->buildingPage->advantages) == 0) {
            return null;
        }

        $advantages = $this->buildingPage->advantages;

        echo $this->render('buildingAdvantagesWidgetView', [
            'model' => $this->buildingPage,
            'advantages' => $advantages
        ]);
    }
}
