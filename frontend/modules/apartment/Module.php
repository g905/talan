<?php

namespace frontend\modules\apartment;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\apartment\controllers';
}
