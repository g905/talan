<?php
namespace frontend\modules\apartment\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;

class TlNewComplexPartAsset extends AssetBundle {
	public $fileVersion = '1';

	public $css = [
		'/css/complex-page-top.css?v=6',
	];

	public $js = [
		'/js/complex-page-top.js?v=3',
	];

	public $depends = [
		AppAsset::class,
	];
}