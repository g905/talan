<?php

namespace frontend\modules\apartment\components;

use common\models\ApartmentBuilding;
use common\models\BuildHistoryApartmentBuildingItem;
use frontend\modules\apartment\models\BuildHistoryApartmentBuildingItemSlide;
use metalguardian\fileProcessor\helpers\FPM;
use yii\db\Expression;

class ApartmentBuildingHistoryInfoGrabber
{
    public static function getInfoByBuildingId($id)
    {
        $model = ApartmentBuilding::find()->where(['id' => $id, 'published' => 1])->one();

        $result = [];
        if (!$model) {
            return $result;
        }

        $result = self::getYearData($model);

        return $result;
    }

    /**
     * @param ApartmentBuilding $model
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected static function getYearData(ApartmentBuilding $model)
    {
        $startYear = (integer)app()->formatter->asDate($model->build_start_date, 'YYYY');
        $endYear = (integer)app()->formatter->asDate($model->build_end_date, 'YYYY');
        $startMonth = (integer)app()->formatter->asDate($model->build_start_date, 'MM');
        $endMonth = (integer)app()->formatter->asDate($model->build_end_date, 'MM');
        $currentYear = (integer)app()->formatter->asDate((new \DateTime()), 'YYYY');
        $currentMonth = (integer)app()->formatter->asDate((new \DateTime()), 'MM');

        $result = [];
        if ($startYear > $endYear) {
            return [];
        }

        $datesData = static::getDatesData($model);
        $currentPercent = self::getCurrentPercentage($model);

        for ($y = $startYear; $y <= $endYear; $y++) { // year
            $monthData = [];
            foreach (static::getMonthDictionary() as $monthIndex => $month) { // month
                $monthIndex += 1;
                // skip all months before start and after end date
                if ($y == $startYear && $monthIndex < $startMonth) {
                    $monthData[] = [
                        'title' => $month,
                    ];
                    continue;
                }
                if ($y == $endYear && $monthIndex > $endMonth) {
                    $monthData[] = [
                        'title' => $month,
                    ];
                    continue;
                }

                // fill end date item
                if ($y == $endYear && $monthIndex == $endMonth) {
                    $monthData[] = [
                        'title' => app()->formatter->asDate($model->build_end_date, 'LLLL, YYYY'),
                        'subtitle' => $model->build_history_end_text
                    ];
                    continue;
                }

                // fill month with slides
                //dump($datesData[$y][$monthIndex]);die();
                if (!empty($datesData[$y][$monthIndex])) {

                    $data = [
                        'title' => $month,
                        'days' => $datesData[$y][$monthIndex],
                        'caption_actual' => $datesData[$y][$monthIndex][0]['percent_plan'] . '%',
                        'caption_planned' => $datesData[$y][$monthIndex][0]['percent'] . '%',
                    ];
                    // current day - add percents
                    if ($y == $currentYear && $monthIndex == $currentMonth) {
                        $data['subtitle'] = $model->build_history_current_text;
                    }
                    $monthData[] = $data;
                    continue;
                }
                // fill current day if there are no slides
                if ($y == $currentYear && $monthIndex == $currentMonth) {
                    $monthData[] = [
                        'title' => $month,
                    ];
                    continue;
                }

                // fill null values for future months
                if ($y >= $currentYear && $monthIndex > $currentMonth) {
                    $monthData[] = [
                        'title' => $month,
                    ];
                    continue;
                }

                // fill month without anything
                $monthData[] = ['title' => $month];
            }
            $result[] = [
                'year' => $y,
                'month' => $monthData,
            ];
        }

        return $result;
    }

    /**
     * @param ApartmentBuilding $model
     * @return array|\yii\db\ActiveRecord[]
     */
    protected static function getDatesData(ApartmentBuilding $model)
    {
        $result = BuildHistoryApartmentBuildingItemSlide::find()
            ->alias('slide')
            ->select([
                'slide.*',
                'month' => 'MONTH(item.date)',
                'year' => 'YEAR(item.date)',
                'date' => 'item.date',
                'item.percent'
            ])
            ->orderBy(new Expression('CAST(slide.day AS UNSIGNED) ASC'))
            ->joinWith([
                'buildHistoryApartmentBuildingItem' => function(\yii\db\ActiveQuery $query) {
                    return $query->alias('item')->asArray(true);
                }
            ], false)
            ->where(['item.apartment_building_id' => $model->id])
            ->all();

        $comfortResult = [];

        foreach ($result as $item) {
            foreach ($item->image as $img) {
                $year = $item['year'];
                $month = $item['month'];
                $comfortResult[$year][$month][] = [
                    'title' => $item->day . ' ' . app()->formatter->asDate($item['date'], 'MMMM'),
                    'text' => $item->content,
                    //'img' => FPM::src($img->file_id ?? null, 'complex', 'historyslide'),
                    'img' => FPM::originalSrc($img->file_id ?? null),
                    'percent' => $item->buildHistoryApartmentBuildingItem->percent,
                    'percent_plan' => $item->buildHistoryApartmentBuildingItem->percent_plan
                ];
            }
        }
        return $comfortResult;
    }

    /**
     * @param ApartmentBuilding $model
     * @return int
     */
    protected static function getCurrentPercentage(ApartmentBuilding $model)
    {
        $data = BuildHistoryApartmentBuildingItem::find()
            ->select(['percent'])
            ->where(['apartment_building_id' => $model->id])
            ->orderBy(['date' => SORT_DESC])
            ->limit(1)
            ->column();

        return $data[0] ?? 0;
    }

    /**
     * @return array
     */
    protected static function getMonthDictionary()
    {
        return [
            'янв',
            'фев',
            'март',
            'апр',
            'май',
            'июнь',
            'июль',
            'авг',
            'сент',
            'окт',
            'нояб',
            'дек',
        ];
    }
}
