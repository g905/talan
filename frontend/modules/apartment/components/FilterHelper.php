<?php

namespace frontend\modules\apartment\components;

use common\models\ProductAttribute;
use yii\helpers\Json;

/**
 * Created by PhpStorm.
 * User: art
 * Date: 24.07.2017
 * Time: 13:44
 */
class FilterHelper
{
    /**
     * @param $postData
     * @return array
     */
    public static function generateGetFromPost($postData)
    {
        $session = \Yii::$app->session;

        //price
        if ((isset($postData['BaseFiltersForm']['isUserSetPriceFrom'])) && ($postData['BaseFiltersForm']['isUserSetPriceFrom'])) {
            $session->set('userMinPrice', $postData['BaseFiltersForm']['priceFrom']);
        }

        if  ((isset($postData['BaseFiltersForm']['isUserSetPriceTo'])) && ($postData['BaseFiltersForm']['isUserSetPriceTo'])) {
            $session->set('userMaxPrice', $postData['BaseFiltersForm']['priceTo']);
        }

        $userMinPrice = $session->get('userMinPrice');
        $userMaxPrice = $session->get('userMaxPrice');

        //area
        if ((isset($postData['BaseFiltersForm']['isUserSetAreaFrom'])) && ($postData['BaseFiltersForm']['isUserSetAreaFrom'])) {
            $session->set('userMinArea', $postData['BaseFiltersForm']['areaFrom']);
        }

        if ((isset($postData['BaseFiltersForm']['isUserSetAreaTo'])) && ($postData['BaseFiltersForm']['isUserSetAreaTo'])) {
            $session->set('userMaxArea', $postData['BaseFiltersForm']['areaTo']);
        }

        $userMinArea = $session->get('userMinArea');
        $userMaxArea = $session->get('userMaxArea');

        $getData = [];


        if (isset($userMinPrice)) {
            $getData['pricefrom'] = $userMinPrice;
        }

        if (isset($userMaxPrice)) {
            $getData['priceto'] = $userMaxPrice;
        }

        if (isset($userMinArea)) {
            $getData['areafrom'] = $userMinArea;
        }

        if (isset($userMaxArea)) {
            $getData['areato'] = $userMaxArea;
        }

        if (isset($postData['page'])) {
            $getData['page'] = $postData['page'];
        }

        if (isset($postData['type'])) {
            $getData['type'] = $postData['type'];
        }

        if (isset($postData['BaseFiltersForm']['floor'])) {
            $getData['floor'] = $postData['BaseFiltersForm']['floor'];
        }


        if (isset($postData['BaseFiltersForm']['rooms'][0])) {
            $getData['rooms'] = $postData['BaseFiltersForm']['rooms'][0];
        }

        if (isset($postData['BaseFiltersForm']['other'][0])) {
            $getData['other'] = $postData['BaseFiltersForm']['other'][0];
        }

        if (isset($postData['BaseFiltersForm']['complex'][0])) {
            $getData['complex'] = $postData['BaseFiltersForm']['complex'][0];
        }

        return $getData;
    }

    /**
     * @param $getData
     * @return mixed
     */
    public static function generatePostFromGet($getData)
    {
        $session = \Yii::$app->session;
        $session->remove('userMinPrice');
        $session->remove('userMaxPrice');

        $session->remove('userMinArea');
        $session->remove('userMaxArea');

        $ignoredParams = [
            'alias', //because category alias has same name
        ];

        $serializeData['BaseFiltersForm']['priceFrom'] = [];
        $serializeData['BaseFiltersForm']['priceTo'] = [];
        $serializeData['BaseFiltersForm']['areaFrom'] = [];
        $serializeData['BaseFiltersForm']['areaTo'] = [];
        $serializeData['BaseFiltersForm']['rooms'] = [];
        $serializeData['BaseFiltersForm']['other'] = [];
        $serializeData['BaseFiltersForm']['complex'] = [];
        $serializeData['BaseFiltersForm']['type'] = '';
        $serializeData['BaseFiltersForm']['floor'] = '';

        foreach ($getData as $getParamAlias => $getParamValueStr) {
            $paramValue = [];

            if (in_array($getParamAlias, $ignoredParams)) {
                continue;
            }

            switch ($getParamAlias) {

                case 'pricefrom':
                    $serializeData['BaseFiltersForm']['priceFrom'] = $getParamValueStr;
                    $session->set('userMinPrice', $getParamValueStr);
                    break;

                case 'priceto':
                    $serializeData['BaseFiltersForm']['priceTo'] = $getParamValueStr;
                    $session->set('userMaxPrice', $getParamValueStr);
                    break;
                case 'areafrom':
                    $serializeData['BaseFiltersForm']['areaFrom'] = $getParamValueStr;
                    $session->set('userMinArea', $getParamValueStr);
                    break;

                case 'areato':
                    $serializeData['BaseFiltersForm']['areaTo'] = $getParamValueStr;
                    $session->set('userMaxArea', $getParamValueStr);
                    break;
                case 'rooms':
                    $serializeData['BaseFiltersForm']['rooms'][0] = $getParamValueStr;
                    break;
                case 'other':
                    $serializeData['BaseFiltersForm']['other'][0] = $getParamValueStr;
                    break;
                case 'complex':
                    $serializeData['BaseFiltersForm']['complex'][0] = $getParamValueStr;
                    break;
                case 'floor':
                    $serializeData['BaseFiltersForm']['floor'] = $getParamValueStr;
                    break;
                case 'type':
                    $serializeData['BaseFiltersForm']['type'] = $getParamValueStr;
                    break;
                case 'page':
                    continue;
                    break;
            }
        }

        return $serializeData;
    }
}
