<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.05.18
 * Time: 15:10
 */

namespace frontend\modules\apartment\components;


use common\models\ApartmentComplex;
use common\models\BuildHistoryApartmentComplexItem;
use frontend\modules\apartment\models\BuildHistoryApartmentComplexItemSlide;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use yii\db\Expression;

class ApartmentComplexHistoryInfoGrabber
{
    /**
     * @param string $alias
     * @return array
     */
    public static function getInfoByComplexAlias(string $alias)
    {
        $model = ApartmentComplex::find()->where([
            'alias' => $alias,
            'published' => 1,
        ])->one();

        $result = [];
        if (!$model) {
            return $result;
        }

        $result = self::getYearData($model);

        return $result;
    }

    /**
     * @param ApartmentComplex $model
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected static function getYearData(ApartmentComplex $model)
    {
        $startYear = (integer)app()->formatter->asDate($model->build_start_date, 'YYYY');
        $endYear = (integer)app()->formatter->asDate($model->build_end_date, 'YYYY');
        $startMonth = (integer)app()->formatter->asDate($model->build_start_date, 'MM');
        $endMonth = (integer)app()->formatter->asDate($model->build_end_date, 'MM');
        $currentYear = (integer)app()->formatter->asDate((new \DateTime()), 'YYYY');
        $currentMonth = (integer)app()->formatter->asDate((new \DateTime()), 'MM');

        $result = [];
        if ($startYear > $endYear) {
            return [];
        }

        $datesData = static::getDatesData($model);
        $currentPercent = self::getCurrentPercentage($model);

        for ($y = $startYear; $y <= $endYear; $y++) { // year
            $monthData = [];
            foreach (static::getMonthDictionary() as $monthIndex => $month) { // month
                $monthIndex += 1;
                // skip all months before start and after end date
                if ($y == $startYear && $monthIndex < $startMonth) {
                    $monthData[] = [
                        'title' => $month,
                    ];
                    continue;
                }
                if ($y == $endYear && $monthIndex > $endMonth) {
                    $monthData[] = [
                        'title' => $month,
                    ];
                    continue;
                }

                // fill end date item
                if ($y == $endYear && $monthIndex == $endMonth) {
                    $monthData[] = [
                        'title' => app()->formatter->asDate($model->build_end_date, 'LLLL, YYYY'),
                        'subtitle' => $model->build_history_end_text
                    ];
                    continue;
                }

                // fill month with slides
                //dump($datesData[$y][$monthIndex]);die();
                if (!empty($datesData[$y][$monthIndex])) {

                    $data = [
                        'title' => $month,
                        'days' => $datesData[$y][$monthIndex],
                        'caption_actual' => $datesData[$y][$monthIndex][0]['percent_plan'] . '%',
                        'caption_planned' => $datesData[$y][$monthIndex][0]['percent'] . '%',
                    ];
                    // current day - add percents
                    if ($y == $currentYear && $monthIndex == $currentMonth) {
                        $data['subtitle'] = $model->build_history_current_text;
                    }
                    $monthData[] = $data;
                    continue;
                }
                // fill current day if there are no slides
                if ($y == $currentYear && $monthIndex == $currentMonth) {
                    $monthData[] = [
                        'title' => $month,
                    ];
                    continue;
                }

                // fill null values for future months
                if ($y >= $currentYear && $monthIndex > $currentMonth) {
                    $monthData[] = [
                        'title' => $month,
                    ];
                    continue;
                }

                // fill month without anything
                $monthData[] = ['title' => $month];
            }
            $result[] = [
                'year' => $y,
                'month' => $monthData,
            ];
        }

        return $result;
    }

    /**
     * @param ApartmentComplex $model
     * @return array|\yii\db\ActiveRecord[]
     */
    protected static function getDatesData(ApartmentComplex $model)
    {
        $result = BuildHistoryApartmentComplexItemSlide::find()
            ->alias('slide')
            ->select([
                'slide.*',
                'month' => 'MONTH(item.date)',
                'year' => 'YEAR(item.date)',
                'date' => 'item.date',
                'item.percent'
            ])
            ->orderBy(new Expression('CAST(slide.day AS UNSIGNED) ASC'))
            ->joinWith([
                'buildHistoryApartmentComplexItem' => function(\yii\db\ActiveQuery $query) {
                    return $query->alias('item')->asArray(true);
                }
            ], false)
            ->where(['item.apartment_complex_id' => $model->id])
            ->all();

        $comfortResult = [];

        foreach ($result as $item) {
            foreach ($item->image as $img) {
                $year = $item['year'];
                $month = $item['month'];
                $comfortResult[$year][$month][] = [
                    'title' => $item->day . ' ' . app()->formatter->asDate($item['date'], 'MMMM'),
                    'text' => $item->content,
//                    'img' => FPM::src($img->file_id ?? null, 'complex', 'historyslide'),
                    'img' => FPM::originalSrc($img->file_id ?? null),
                    'percent' => $item->buildHistoryApartmentComplexItem->percent,
                    'percent_plan' => $item->buildHistoryApartmentComplexItem->percent_plan
                ];
            }
        }
        return $comfortResult;
    }

    /**
     * @param ApartmentComplex $model
     * @return int
     */
    protected static function getCurrentPercentage(ApartmentComplex $model)
    {
        $data = BuildHistoryApartmentComplexItem::find()
            ->select(['percent'])
            ->where([
                'apartment_complex_id' => $model->id,
            ])->orderBy([
                'date' => SORT_DESC
            ])->limit(1)
            ->column();

        return $data[0] ?? 0;
    }

    /**
     * @return array
     */
    protected static function getMonthDictionary()
    {
        return [
            'янв',
            'фев',
            'март',
            'апр',
            'май',
            'июнь',
            'июль',
            'авг',
            'сент',
            'окт',
            'нояб',
            'дек',
        ];
    }
}
