<?php

namespace frontend\modules\apartment\components;

use frontend\widgets\FilterWidget;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use common\models\Apartment;

/**
 * Apartments ListView widget.
 *
 * @author art
 * @author delagics
 * @package frontend\modules\catalog\components
 */
class ApartmentsListView extends ListView
{
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->emptyText = $this->getEmptyBlock();
    }

    public $model;
    public $itemView = '@app/modules/apartment/views/apartment-complex/_listItem';
    public $stubView = '@app/modules/apartment/views/apartment-complex/_blankItem';
    public $options = ['class' => 'apartaments'];
    public $itemOptions = ['tag' => false];
    public $pager = [
        'maxButtonCount' => 5,
        'firstPageLabel' => '...',
        'lastPageLabel' => '...',
        'pageCssClass' => 'pagination__item',
        'firstPageCssClass' => 'pagination__first',
        'lastPageCssClass' => 'pagination__last',
    ];
    public $emptyText;
    public $emptyTextOptions = [
        'tag' => 'div class="apartaments__list"'
    ];
    public $layout = <<<HTML
<div class="apartaments__list">{items}</div>
<div class="apartaments__bottom wow fadeIn">
    <div class="apartaments__cell"><div class="apartaments__line"></div></div>
    <div class="apartaments__cell">{pager}</div>
</div>
HTML;

    /**
     * Overridden to add ability to generate stubs for not full rows.
     *
     * @inheritdoc
     */
    protected function renderAfterItem($model, $key, $index)
    {
        $neededStubs = $this->getStubsNeeded();

        if ($neededStubs > 0) {
            $keys = $this->dataProvider->getKeys();
            $lastKey = end($keys);
            if ($lastKey === $key) {
                return $this->renderStubs($neededStubs);
            }
        }

        return null;
    }

    /**
     * Get the number of needed stubs.
     *
     * @param int $perRowCount
     * @return int
     */
    private function getStubsNeeded($perRowCount = Apartment::ITEM_COUNT_PER_ROW)
    {
        $count = $this->dataProvider->getCount();
        $needed = $perRowCount - ($count % $perRowCount);

        return $needed !== $perRowCount ? $needed : 0;
    }

    /**
     * Render needed number of stubs (empty items).
     *
     * @param int $count the number of needed stubs.
     * @return string
     */
    private function renderStubs(int $count)
    {
        $output = '';
        for ($i = 0; $i < $count; $i++) {
            $output .= $this->render($this->stubView);
        }

        return $output;
    }

    public function getEmptyBlock()
    {
        if (empty($this->model)) {
            return '<div class="apartaments__recommended-desc"> К сожалению, по вашему запросу квартиры не найдены.</div>';
        }
        $apartments = $this->recommendedFilter(request()->getQueryParams(), $this->model);
        $render = '<div class="apartaments__recommended-desc"> К сожалению, по вашему запросу квартиры не найдены. Попробуйте изменить параметры. </div><div class="apartaments__recommended-title"> Рекомендуем посмотреть: </div>';

        if ($apartments->getModels()) {
            foreach ($apartments->getModels() as $item) {
                $render .= $this->render('@app/modules/apartment/views/apartment-complex/_listItem', ['model' => $item]);
            }
        } else {
            $apartments = Apartment::find()
                ->isPublished()
                ->andWhere(['apartment_complex_id' => $this->model->id, 'type' => $this->model->type])
                ->limit(3)
                ->all();
            foreach ($apartments as $item) {
                $render .= $this->render('@app/modules/apartment/views/apartment-complex/_listItem', ['model' => $item]);
            }
        }

        return $render;
    }

    public function recommendedFilter($params, $model)
    {
        $complexes = obtain('complex', $params, [$model->id]);
        $squareMin = $this->getPercents(obtain('square-min', $params), false);
        $squareMax = $this->getPercents(obtain('square-max', $params), true);
        $priceMin = $this->getPercents(obtain('price-min', $params), false);
        $priceMax = $this->getPercents(obtain('price-max', $params), true);
        //$floor = obtain('floor', $params);
        $floorMin = obtain('floor-min', $params);
        $floorMax = obtain('floor-max', $params);
        $rooms = obtain('rooms', $params);
        $for = obtain('for', $params);

        $query = Apartment::find()->alias('a')->where([
            'a.published' => Apartment::IS_PUBLISHED,
            'a.type' => $model->type,
            'a.apartment_complex_id' => $complexes,
        ]);

        // Filter by square
        $squareMin === null ?: $query->andFilterWhere(['>=', 'total_area', $squareMin]);
        $squareMax === null ?: $query->andFilterWhere(['<=', 'total_area', $squareMax]);
        // Filter by price
        $priceMin === null ?: $query->andFilterWhere(['>=', 'price_from', $priceMin]);
        $priceMax === null ?: $query->andFilterWhere(['<=', 'price_from', $priceMax]);
        // Filter by floor number
        //$floor === null || $floor == -1 ?: $query->andFilterWhere(['floor' => $floor]);
        $floorMin === null ?: $query->andFilterWhere(['>=', 'floor', $floorMin]);
        $floorMax === null ?: $query->andFilterWhere(['<=', 'floor', $floorMax]);
        // Filter by rooms count
        $rooms === null ?: $query->andFilterWhere(['rooms_number' => $rooms]);
        // Filter by 'for whom' param
        if ($for) {
            $forMap = [
                FilterWidget::FILTER_YOUNG_FAMILY => ['<', 'a.rooms_number', 3],
                FilterWidget::FILTER_PENSIONER => ['<', 'a.rooms_number', 2],
                FilterWidget::FILTER_SINGLES => ['<', 'a.rooms_number', 2],
                FilterWidget::FILTER_BIG_FAMILY => ['>=', 'a.rooms_number', 3],
            ];
            $condition = obtain($for, $forMap);
            !is_array($condition) ? : $query->andFilterWhere($condition);
        }

        return new ActiveDataProvider([
            'query' => $query->limit(3),
            'sort' => [
                'defaultOrder' => [
                    'position' => SORT_ASC,
                ],
            ],
            'pagination' => false
        ]);
    }

    public function getPercents($int = false, $forward)
    {
        if ($int) {
            if ($forward) {
                $int = $int + (($int / 100) * 25);
            } else {
                $int = $int - (($int / 100) * 25);
            }

        }

        return strval($int);
    }
}
