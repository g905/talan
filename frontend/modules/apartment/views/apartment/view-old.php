<?php

use yii\widgets\Breadcrumbs;
use common\helpers\SiteUrlHelper;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\general\widgets\CommonComplexScriptsWidget;
use frontend\modules\apartment\widgets\apartment\ApartmentActionWidget;
use frontend\modules\apartment\widgets\apartment\ApartmentHowBuyWidget;
use frontend\modules\apartment\widgets\apartment\ApartmentTopBlockWidget;
use frontend\modules\apartment\widgets\apartment\ApartmentFloorPlanWidget;
use frontend\modules\apartment\widgets\apartment\ApartmentDescriptionWidget;
use frontend\modules\apartment\widgets\apartment\ApartmentInvestCalculatorWidget;
use frontend\modules\apartment\widgets\apartment\ApartmentRelatedFlatsWidget;
use frontend\modules\apartment\widgets\apartment\ApartmentTourWidget;
use frontend\modules\apartment\widgets\apartment\ApartmentConstructorWidget;
use yii\helpers\Url;

/**
 * @author art
 * @var $this yii\web\View
 * @var $model common\models\Apartment
 */


//if comes from outer internet (no referer), link to catalog
Yii::$app->request->referrer ? $this->params['back_url'] = Yii::$app->request->referrer : $this->params['back_url'] = ['/apartment/apartment-complex/apartments-list', 'type' => $model->apartmentComplex->type, 'alias' => $model->apartmentComplex->alias];

//Choose label for breadcrumbs, residential or commercial
switch ($model->apartmentComplex->type)
{
    case (\common\helpers\Property::TYPE_RESIDENTIAL):
        $this->params['breadcrumb_label'] = Yii::t('front/apartment', 'Choose_Apartment');
        break;
    case (\common\helpers\Property::TYPE_COMMERCIAL):
        $this->params['breadcrumb_label'] = Yii::t('front/apartment', 'Choose_Premises');
        break;
    default:
        $this->params['breadcrumb_label'] = Yii::t('front/apartment', 'Choose_Premises');
}

$this->params['headerMenu'] = $model->apartmentComplex->apartmentComplexMenu;
$this->params['headerMenuType'] = $model;
$this->params['breadcrumbs'][] = ['label' => 'Недвижимость', 'url' => ['/page/page/catalog']];
$this->params['breadcrumbs'][] = [
    'label' => $model->apartmentComplex->label,
    'url' => SiteUrlHelper::createApartmentComplexUrl(['alias' => $model->apartmentComplex->alias, 'type' => $model->apartmentComplex->type]),
];

$this->params['breadcrumbs'][] = [
    'label' => $this->params['breadcrumb_label'],
    'url' => $this->params['back_url']
];
$this->params['breadcrumbs'][] = $model->label;
?>

<?= CommonComplexScriptsWidget::widget(['currentComplex' => $model->apartmentComplex]) ?>

<div class="header__row header__row_apartment">
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'homeLink' => [
                'label' => t('_Breadcrumbs home label', 'header'),
                'url' => Yii::$app->homeUrl,
                'class' => 'bread-crumbs__link',
            ],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'options' => [
                'class' => 'bread-crumbs bread-crumbs_grey header__bread-crumbs',
            ]
        ]) ?>
    </div>
</div>

<div class="wrap wrap_mobile_full">
    <div class="apartment-head apartment-head_single">
        <h2 class="h2 apartment-head__h2  wow fadeInLeft">
            <?= $model->apartmentComplex->label ?>
        </h2>
        <a href="<?= Yii::$app->request->referrer ?>" data-wow-delay="0.3s" class="apartment-head__back wow fadeInLeft">
            <?= t('_Back to choose apartment', 'apartment') ?>
        </a>
    </div>
</div>


<?= ApartmentTopBlockWidget::widget(['apartmentPage' => $model]) ?>

<?= ApartmentDescriptionWidget::widget(['apartmentPage' => $model]) ?>

<?= ApartmentFloorPlanWidget::widget(['apartmentPage' => $model]) ?>

<?= ApartmentInvestCalculatorWidget::widget(compact('model')) ?>

<?= ApartmentTourWidget::widget(compact('model')) ?>

<?= !$model->is_secondary ? ApartmentConstructorWidget::widget(compact('model','complex', 'building')) : ''?>

<?= ApartmentHowBuyWidget::widget(['apartmentPage' => $model]) ?>

<?= ApartmentActionWidget::widget(compact('model')) ?>

<?= ApartmentRelatedFlatsWidget::widget(['apartmentPage' => $model]) ?>

<?= PageFormBottomBlockWidget::widget([
    'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
    'manager' => $model->apartmentComplex->manager,
    'formTitle' => $model->apartmentComplex->form_bottom_title,
    'formDescription' => $model->apartmentComplex->form_bottom_description,
    'blockClass' => 'questions_article catalogue_question commercial-question',
    'onsubmitJS' => empty($model->form_onsubmit) ? $model->apartmentComplex->submit_apartment_script : $model->form_onsubmit,
]) ?>

<?= PageFooterWidget::widget(['currentCity' => $model->apartmentComplex->city]) ?>
