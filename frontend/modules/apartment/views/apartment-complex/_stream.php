<?php

?>
<div class="modal modals__modal popup-replace" data-modal="thanks">
    <div class="modal__outer">
        <div class="modal__overlay" data-modal-close></div>
        <div class="modal__inner">
            <div class="modal__content">
                <?= $iframe; ?>
                <button class="button button_green popup__button" data-modal-close>
                    <?=Yii::t('front/form', '_Close')?>
                    <span class="button__blip button__blip_hover"></span>
                    <span class="button__blip button__blip_click"></span>
                </button>
            </div>
        </div>
    </div>
</div>
