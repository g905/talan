<?php

use frontend\modules\apartment\widgets\apartment\ApartmentConfiguratorTabsWidget;
use metalguardian\fileProcessor\helpers\FPM;

?>

<?php if ($model->map) : ?>

<?php if (isset($model->choose_house_title) && ($model->choose_house_title !== "")) :?>
    <div class="wrap">
        <h2 class="h2 map__h2 wow fadeInUp"><?= $model->choose_house_title ?></h2>
        <h5 class="h5 wow fadeInUp" style="text-align: center"><?= $model->choose_house_subtitle ?? ""?></h5>
    </div>
<?php else :?>
    <div class="wrap">
        <h2 class="h2 map__h2 wow fadeInUp"><?= Yii::t('apartment-complex', 'choose_house'); ?></h2>
        <h4><?= $model->choose_house_subtitle ?></h4>
        <h5 class="h5 wow fadeInUp" style="text-align: center"><?= $model->choose_house_subtitle ?? ""?></h5>
    </div>
<?php endif; ?>
<?php $model->map->content = preg_replace('/%complex_alias%/', $model->alias, $model->map->content);?>
    <section class="choose-house <?= $model->map->class ?>">
        <div class="wrap wrap_mobile_full">
            <!--<?/*= ApartmentConfiguratorTabsWidget::widget(['model' => $model->map]) */?>-->
            <div class="choose-house__box">
                <div class="choose-house__img-box">
                    <img src="<?= FPM::originalSrc($model->map->image->file_id ?? '') ?>" alt="choose_house" class="choose-house__img-bg"> </div>
                <?= $model->map->content ?>
            </div>
            <ul class="choose-house__lists-desc">
                <?= $model->map->mob_content ?>
            </ul>
        </div>
    </section>
<?php endif; ?>

<?php
$this->registerCss('
     .map__h2 {
        margin-top: -20px!important;
     }
     .choose-house {
        padding-top: 0;
     }
');
?>
