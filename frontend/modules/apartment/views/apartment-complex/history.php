<?php
/**
 * Created by PhpStorm.
 * User: dvixi
 * Date: 04.05.2018
 * Time: 17:30
 */

use common\models\ApartmentComplex;
use frontend\modules\apartment\widgets\complex\ComplexHistoryWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use yii\widgets\Breadcrumbs;
use common\helpers\SiteUrlHelper;
use common\helpers\Property;
use frontend\modules\page\widgets\SelectCityWidget;
use frontend\modules\page\widgets\GetHeaderMenuWidget;
use frontend\modules\page\widgets\GetHeaderComplexListMenuWidget;
use frontend\helpers\FrontendHelper;

/**
 * @var ApartmentComplex $model
 * @var $complexModel \common\models\ComplexList
 * @var \common\models\CompletedProject[] $completedProjectsShown
 * @var \common\models\CompletedProject[] $completedProjectsHidden
 */

$this->params['breadcrumbs'][] = [
    'label' => $model->label,
    'url' => SiteUrlHelper::createApartmentComplexUrl(['alias' => $model->alias, 'type' => $model->type]),
];
$this->params['breadcrumbs'][] = $model->build_history_title;
$this->params['headerMenu'] = $model->apartmentComplexMenu;
$this->params['headerMenuType'] = $model;
?>

<?php if ($model->type == Property::TYPE_COMMERCIAL) : ?>
    <header class="header">
        <div class="wrap header__wrap wrap_mobile_full">
            <a href="<?= SiteUrlHelper::createHomePageUrl()?>" class="logo header__logo">
                <svg class="logo__svg">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo_header_white"></use>
                </svg>
            </a>
            <?= SelectCityWidget::widget([
                'currentCity' => $this->params['currentCity'] ?? City::getUserCity(),
                'position' => SelectCityWidget::POSITION_HEADER_FIXED
            ])?>
            <?= GetHeaderMenuWidget::widget([
                'currentCity' => $this->params['currentCity'] ?? City::getUserCity(),
                'position' => GetHeaderMenuWidget::POSITION_TOP
            ])?>
            <button class="btn-menu header__btn-menu" data-open-menu>
                <span class="btn-menu__element"></span>
                <span class="btn-menu__element"></span>
                <span class="btn-menu__element"></span>
            </button>
            <button class="call header__call ajax-link" data-href="<?=SiteUrlHelper::createFormCallbackPopupUrl()?>">
                <svg class="call__svg">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-phone"></use>
                </svg>
            </button>
            <a href="tel:<?= ConfigHelper::getHeaderPhoneFormated($this->params['currentCity'] ?? City::getUserCity())?>" class="header__phone">
                <?= ConfigHelper::getHeaderPhone($this->params['currentCity'] ?? City::getUserCity())?>
            </a>
        </div>
        <!-- SUBMENU -->
        <?= GetHeaderComplexListMenuWidget::widget(['currentCity' => $city]) ?>
        <!-- /SUBMENU -->
        <div class="wrap wrap_mobile_full">
            <?= Breadcrumbs::widget([
                'tag' => 'div',
                'itemTemplate' => "{link}\n",
                'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
                'homeLink' => [
                    'label' => Yii::t('front/header', '_Breadcrumbs home label'),
                    'url' => Yii::$app->homeUrl,
                    'class' => 'bread-crumbs__link',
                ],
                'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
                'options' => [
                    'class' => 'bread-crumbs header__bread-crumbs',
                ]
            ]) ?>
        </div>
    </header>
<?php else: ?>
    <div class="header__row header__row_apartment">
        <div class="wrap wrap_mobile_full">
            <?= Breadcrumbs::widget([
                'tag' => 'div',
                'itemTemplate' => "{link}\n",
                'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
                'homeLink' => [
                    'label' => Yii::t('front/header', '_Breadcrumbs home label'),
                    'url' => Yii::$app->homeUrl,
                    'class' => 'bread-crumbs__link',
                ],
                'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
                'options' => [
                    'class' => 'bread-crumbs bread-crumbs_many bread-crumbs_grey header__bread-crumbs',
                ]
            ]) ?>
        </div>
    </div>
<?php endif ?>

<?= ComplexHistoryWidget::widget([
    'title' => $model->build_history_title,
    'model' => $model,
]) ?>

<?= $this->render('_completedProjects', [
    'completedProjectsShown' => $completedProjectsShown,
    'completedProjectsHidden' => $completedProjectsHidden,
    'title' => $complexModel->completed_projects_title,
    'description' => $complexModel->completed_projects_description,
    'btnText' => $complexModel->completed_projects_more_btn_label,
]) ?>

<?= PageFooterWidget::widget(['currentCity' => $model->city]) ?>
