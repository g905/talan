<?php
/**
 * @var $this yii\web\View
 */
?>

<a href="#" class="card card_stub apartaments__card wow fadeInUp">
    <div class="card__img"  style="background: url('/static/img/apartments/apartaments-stub.png') center center / auto no-repeat;"></div>
</a>
