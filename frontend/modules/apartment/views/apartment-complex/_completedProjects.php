<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 04.05.18
 * Time: 18:32
 *
 * @var $completedProjectsShown \common\models\CompletedProject[]
 * @var $completedProjectsHidden \common\models\CompletedProject[]
 * @var $title string
 * @var $description string
 * @var $btnText string
 */
?>
<?php if ($completedProjectsShown) : ?>
    <section class="values done_projects">
        <div class="wrap wrap_mobile_full">
            <div class="done_projects__title">
                <h2 class="h2 values__h2 wow fadeInLeft"><?= $title ?></h2>
                <p class="p values__p wow fadeInLeft"><?= strip_tags($description) ?></p>
            </div>
            <div class="values__list">
                <?php foreach ($completedProjectsShown as $project) : ?>
                    <a <?= $project->url ? 'href="'.$project->url.'"' : '' ?> class="card values__card wow fadeInUp">
                        <div class="card__img" data-bg-src="<?= $project->getPhotoPreviewSrc() ?>" data-bg-pos="center" data-bg-size="cover"></div>
                        <h5 class="h5 card__h5"><?= $project->title ?></h5>
                        <div class="divider card__divider"></div>
                        <div class="card__text">
                            <p><?= strip_tags($project->description) ?></p>
                        </div>
                    </a>
                <?php endforeach ?>
                <?php// if ($completedProjectsHidden) : ?>
<!--                    <div class="values__list" style="display: none" data-list-items="done-projects">-->
<!--                        --><?php ////foreach ($completedProjectsHidden as $project) : ?>
<!--                            <div class="card values__card wow fadeInUp">-->
<!--                                <div class="card__img" data-bg-src="--><?php//// $project->getPhotoPreviewSrc() ?><!--" data-bg-pos="center" data-bg-size="cover"></div>-->
<!--                                <h5 class="h5 card__h5">--><?php//// $project->title ?><!--</h5>-->
<!--                                <div class="divider card__divider"></div>-->
<!--                                <div class="card__text">-->
<!--                                    <p>--><?php //// strip_tags($project->description) ?><!--</p>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        --><?php ////endforeach ?>
<!--                    </div>-->
                <?php// endif ?>
<!--                <div class="divider_ajax">-->
<!--                    <div class="line"></div>-->
<!--                    --><?php ////if ($completedProjectsHidden) : ?>
<!--                        <button class="footer__button" data-list-show="done-projects">-->
<!--                            <span class="footer__button-hide-text">--><?php// $btnText ?><!--</span>-->
<!--                            <span class="footer__button-show-text">скрыть</span>-->
<!--                        </button>-->
<!--                    --><?php ////endif ?>
<!--                </div>-->
            </div>
        </div>
    </section>
<?php endif ?>
