<?php

use frontend\modules\apartment\widgets\apartment\ApartmentConfiguratorTabsWidget;
use metalguardian\fileProcessor\helpers\FPM;

?>

<?php if ($mapModel) : ?>

<?php $mapModel->content = preg_replace('/%complex_alias%\/+/', "", $mapModel->content);?>

    <section class="choose-house <?= $mapModel->class ?>">
        <div class="wrap wrap_mobile_full">
            <!--<?//= ApartmentConfiguratorTabsWidget::widget(['model' => $mapModel]) ?>-->
            <div class="choose-house__box">
                <div class="choose-house__img-box">
                    <img src="<?= FPM::originalSrc($mapModel->image->file_id ?? '') ?>" alt="choose_house" class="choose-house__img-bg"> </div>
                <?= $mapModel->content ?>
            </div>
            <ul class="choose-house__lists-desc">
                <?= $mapModel->mob_content ?>
            </ul>
        </div>
    </section>
<?php endif; ?>