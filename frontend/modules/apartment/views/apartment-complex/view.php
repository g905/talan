<?php

use common\models\ApartmentComplex;
use frontend\modules\apartment\widgets\complex\ComplexActionWidget;
use frontend\modules\apartment\widgets\complex\ComplexArchitectWidget;
use frontend\modules\apartment\widgets\complex\ComplexMapWidget;
use frontend\modules\apartment\widgets\complex\ComplexPublicServiceWidget;
use frontend\modules\apartment\widgets\complex\ComplexHistoryWidget;
use frontend\modules\general\widgets\CommonComplexScriptsWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\page\widgets\PageHeaderWidget;
use frontend\modules\apartment\widgets\complex\ComplexBuildingWidget;
use frontend\modules\apartment\widgets\complex\ComplexCtaWidget;
use frontend\modules\page\widgets\ApartmentComplexWidget;

//use \frontend\modules\apartment\widgets\complex\TlNewPageHeader;
use frontend\modules\page\widgets\TlNewAllPageHeader;

use \frontend\modules\apartment\widgets\complex\TlNewComplexAdvantagesWidget;

/** @var ApartmentComplex $model */
$this->params['currentCity'] = $model->city;
$this->params['breadcrumbs'][] = ['label' => 'Недвижимость', 'url' => ['/page/page/catalog']];
$this->params['breadcrumbs'][] = $model->label;
$this->params['headerMenu'] = $model->apartmentComplexMenu;
$this->params['headerMenuType'] = $model;
?>
 
<?= CommonComplexScriptsWidget::widget(['currentComplex' => $model]) ?>

<?= TlNewAllPageHeader::widget(['complexPage' => $model]); ?>

<?= PageHeaderWidget::widget(['currentCity' => $model->city]) ?>

<?php echo TlNewComplexAdvantagesWidget::widget(['complexPage' => $model]);?>

<?php if($model->show_choose_house_block): ?>

<?= $this->render('_complexMap_for_complex_page', compact(['model'])) ?>

<?php endif; ?>

<?php
if (!empty($model->form_top_title_first) || !empty($model->form_top_title_second) || !empty($model->form_top_description) || !empty($model->form_top_emails)) {
    ?>
    <?= PageFormBottomBlockWidget::widget([
        'formTitle' => $model->form_top_title,
        'formDescription' => $model->form_top_description,
        'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_RIGHT,
        'blockClass' => 'questions_request',
        'formType' => PageFormBottomBlockWidget::FORM_TYPE_COMPLEX_PRESENTATION,
        'complexId' => $model->id,
        'onsubmitJS' => $model->form_top_onsubmit,
    ]) ?>
    <?php
}
?>

<?= ComplexBuildingWidget::widget(['complex' => $model]) ?>

<?= ComplexMapWidget::widget(['complexPage' => $model]) ?>

<?= ComplexCtaWidget::widget(['complexPage' => $model]) ?>

<?= ComplexActionWidget::widget(['complexPage' => $model]) ?>

<?= ComplexPublicServiceWidget::widget(['complexPage' => $model]) ?>

<?= ComplexArchitectWidget::widget(['complexPage' => $model]) ?>

<?= ComplexHistoryWidget::widget(['title' => $model->build_history_title, 'model' => $model]) ?>

<?= PageFormBottomBlockWidget::widget([
    'formTitle' => $model->form_bottom_title,
    'formDescription' => $model->form_bottom_description,
    'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
    'manager' => $model->manager,
    //'agreementText' => 'afdsfdsf',
    'blockClass' => 'questions_article catalogue_question commercial-question',
    'onsubmitJS' => $model->form_bottom_onsubmit,
]) ?>

<?= ApartmentComplexWidget::widget(['model' => $model]) ?>

<?= PageFooterWidget::widget(['currentCity' => $model->city]) ?>


