<?php

use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
use common\models\City;
use common\helpers\Property;
use common\helpers\ConfigHelper;
use common\helpers\SiteUrlHelper;
use frontend\widgets\FilterWidget;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\SelectCityWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\GetHeaderMenuWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\apartment\components\ApartmentsListView;
use frontend\modules\page\widgets\GetHeaderComplexListMenuWidget;
use frontend\modules\apartment\widgets\complexList\ComplexListPoster;

/**
 * @var $this yii\web\View
 * @var $city common\models\City
 * @var $model common\models\ComplexList
 * @var $complexes common\models\ApartmentComplex[]
 * @var $completedProjectsShown common\models\CompletedProject[]
 * @var $completedProjectsHidden common\models\CompletedProject[]
 * @var $benefits common\models\Benefit[]
 * @var $provider yii\data\ActiveDataProvider|false
 */
$this->params['currentCity'] = $city;
// $this->params['breadcrumbs'][] = ['label' => 'Недвижимость', 'url' => ['/apartment/apartment-complex/list', 'type' => Property::TYPE_RESIDENTIAL]];
$this->params['breadcrumbs'][] = $model->menu_title;
$this->params['headerComplexList'] = GetHeaderComplexListMenuWidget::widget(['currentCity' => $city]);
?>

<?= ComplexListPoster::widget(compact('model')) ?>

<?php if ($model->type == Property::TYPE_COMMERCIAL) : ?>
<header class="header">
    <div class="wrap header__wrap wrap_mobile_full">
        <a href="<?= SiteUrlHelper::createHomePageUrl()?>" class="logo header__logo">
            <svg class="logo__svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo_header_white"></use>
            </svg>
        </a>
        <?= SelectCityWidget::widget([
            'currentCity' => $this->params['currentCity'] ?? City::getUserCity(),
            'position' => SelectCityWidget::POSITION_HEADER_FIXED
        ])?>
        <?= GetHeaderMenuWidget::widget([
            'currentCity' => $this->params['currentCity'] ?? City::getUserCity(),
            'position' => GetHeaderMenuWidget::POSITION_TOP
        ])?>
        <button class="btn-menu header__btn-menu" data-open-menu>
            <span class="btn-menu__element"></span>
            <span class="btn-menu__element"></span>
            <span class="btn-menu__element"></span>
        </button>
        <button class="call header__call ajax-link" data-href="<?=SiteUrlHelper::createFormCallbackPopupUrl()?>">
            <svg class="call__svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-phone"></use>
            </svg>
        </button>
        <a href="tel:<?= ConfigHelper::getHeaderPhoneFormatted($this->params['currentCity'] ?? City::getUserCity())?>" class="header__phone">
            <?= ConfigHelper::getHeaderPhone($this->params['currentCity'] ?? City::getUserCity())?>
        </a>
    </div>
    <!-- SUBMENU -->
    <?= GetHeaderComplexListMenuWidget::widget(['currentCity' => $city]) ?>
    <!-- /SUBMENU -->
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'homeLink' => [
                'label' => Yii::t('front/header', '_Breadcrumbs home label'),
                'url' => Yii::$app->homeUrl,
                'class' => 'bread-crumbs__link',
            ],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'options' => [
                'class' => 'bread-crumbs header__bread-crumbs',
            ]
        ]) ?>
    </div>
</header>
<?php else: ?>
    <div class="header__row header__row_apartment">
        <div class="wrap wrap_mobile_full">
            <?= Breadcrumbs::widget([
                'tag' => 'div',
                'itemTemplate' => "{link}\n",
                'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
                'homeLink' => [
                    'label' => t('_Breadcrumbs home label', 'header'),
                    'url' => app()->homeUrl,
                    'class' => 'bread-crumbs__link',
                ],
                'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
                'options' => ['class' => 'bread-crumbs bread-crumbs_many bread-crumbs_grey header__bread-crumbs']
            ]) ?>
        </div>
    </div>
<?php endif ?>

<div class="wrap wrap_mobile_full">
    <div class="apartment-head catalogue-head">
        <h2 class="h2 apartment-head__h2 wow fadeInLeft"><?= $model->top_title ?></h2>
    </div>
</div>

<!-- COMPLECS -->
<section class="complecs">
    <div class="wrap wrap_mobile_full">
        <div class="complecs__list">
            <?php if ($complexes) : ?>
                <?php foreach ($complexes as $complex) : ?>
                    <div class="card complecs__card wow fadeInUp">
                        <a href="<?= $complex->getViewUrl() ?>" class="card__img-link"></a>
                        <div class="card__img" data-bg-src="<?= $complex->getPreviewPhotoSrc() ?>" data-bg-pos="center" data-bg-size="cover">
                            <div class="diagram apartment-head__diagram">
                                <?php if ($complex->general_progress_percent) : ?>
                                    <div class="circle-progress" data-circle-progress="<?= trim($complex->general_progress_percent) ?>">
                                        <div class="circle-progress__outer">
                                            <div class="circle-progress__progress circle-progress__progress_10"></div>
                                            <div class="circle-progress__progress circle-progress__progress_20"></div>
                                            <div class="circle-progress__progress circle-progress__progress_30"></div>
                                            <div class="circle-progress__progress circle-progress__progress_40"></div>
                                            <div class="circle-progress__progress circle-progress__progress_50"></div>
                                            <div class="circle-progress__progress circle-progress__progress_60"></div>
                                            <div class="circle-progress__progress circle-progress__progress_70"></div>
                                            <div class="circle-progress__progress circle-progress__progress_80"></div>
                                            <div class="circle-progress__progress circle-progress__progress_90"></div>
                                            <div class="circle-progress__progress circle-progress__progress_100"></div>
                                            <div class="circle-progress__value"><?= $complex->general_progress_percent ?></div>
                                            <div class="circle-progress__percent">%</div>
                                        </div>
                                        <div class="circle-progress__label"> <?= $complex->general_progress_title ?> </div>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                        <h5 class="h5 card__h5"> <?= $complex->label ?> </h5>
                        <div class="complecs__card-links">
                            <a href="<?= $complex->getViewUrl() ?>" class="button button_green" data-wow-delay="1.3s"> подробнее о ЖК
                                <span class="button__bg"></span>
                                <div class="button__blip button__blip_hover"></div>
                                <div class="button__blip button__blip_click"></div>
                            </a>
                            <?php if (!$complex->is_sale) : ?>
                                <a href="<?= \common\helpers\SiteUrlHelper::createApartmentComplexApartmentsListUrl(['alias' => $complex->alias, 'type' => $complex->type]) ?>" class="more view-all__more">
                                    <?php if ($complex->type == \common\helpers\Property::TYPE_RESIDENTIAL) : ?>
                                        <?php if ($complex->custom_choose_label) : ?>
                                            <?= $complex->custom_choose_label ?>
                                        <?php else : ?>
                                            <?= Yii::t('front/complex', 'choose_residential_apartment') ?>
                                        <?php endif; ?>
                                    <?php else : ?>
                                        <?php if ($complex->custom_choose_label) : ?>
                                            <?= $complex->custom_choose_label ?>
                                        <?php else : ?>
                                            <?= Yii::t('front/complex', 'choose_commercial_apartment') ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach ?>
            <?php endif ?>

            <div class="divider_ajax">
                <div class="line"></div>
            </div>
        </div>
    </div>
</section>
<!-- /COMPLECS -->
<!-- VALUES -->
<?php if ($complex->isResidential()) : ?>
<?= $this->render('_completedProjects', [
    'completedProjectsShown' => $completedProjectsShown,
    //'completedProjectsHidden' => $completedProjectsHidden,
    'title' => $model->completed_projects_title,
    'description' => $model->completed_projects_description,
    'btnText' => $model->completed_projects_more_btn_label,
]) ?>
<?php endif ?>

<?php if ($complex->isCommercial() && $benefits) : ?>
    <section class="values profit">
        <div class="wrap wrap_mobile_full">
            <h2 class="h2 values__h2 wow fadeInLeft text-align-center"><?= $model->benefits_title ?></h2>
            <div class="values__list">
                <?php foreach ($benefits as $key => $benefit) : ?>
                    <div class="card values__card wow fadeInUp">
                    <div class="card__img" data-bg-src="<?= $benefit->getPhotoPreviewSrc() ?>" data-bg-pos="center" data-bg-size="cover"></div>
                    <h5 class="h5 card__h5"><?= $benefit->title ?></h5>
                    <div class="divider card__divider">
                        <span>0 <?= $key + 1 ?></span>
                    </div>
                    <div class="card__text">
                        <p><?= $benefit->description ?></p>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </section>
<?php endif ?>

<?php if ($provider) : ?>
<?php $pjax = Pjax::begin([
    'id' => 'pjax-catalog-list',
    'timeout' => 25000,
    'enablePushState' => true,
    //'linkSelector' => '.pjax_link, .pagination__item a',
]) ?>

<div class="wrap temporary wrap_mobile_full filter_commercial">
    <h2 class="h2 values__h2 wow fadeInLeft text-align-center"><?= t('Choose room by parameters', 'complex-list') ?></h2>
    <div class="buffer">
        <div class="buffer__overflow-x-mobile">
            <div class="buffer__overflow-mobile">
                <?= FilterWidget::widget([
                    'formBaseUrl' => '/apartment/apartment-complex/list',
                    'complex' => $complex, 'pjaxContainerID' => $pjax->getId()
                ]) ?>
            </div>
        </div>
    </div>
    <?= ApartmentsListView::widget(['dataProvider' => $provider]) ?>
</div>
<?php $pjax::end() ?>
<?php endif ?>

<!-- /VALUES -->
<?= PageFormBottomBlockWidget::widget([
    'formTitle' => $model->form_title,
    'formDescription' => $model->form_description,
    'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
    'manager' => $model->manager,
    'agreementText' => $model->form_agreement,
    'blockClass' => 'questions_article catalogue_question commercial-question',
    'onsubmitJS' => null,
]) ?>

<?= PageFooterWidget::widget(['currentCity' => $city]) ?>
