<?php

use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
use frontend\widgets\FilterWidget;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\apartment\components\ApartmentsListView;
use frontend\modules\general\widgets\CommonComplexScriptsWidget;
use frontend\modules\apartment\widgets\apartmentsList\ApartmentsListTopBlockWidget;
use frontend\modules\page\widgets\ApartmentTabsWidget;

/**
 * @var $model common\models\ApartmentComplex
 * @var $complexes common\models\ApartmentComplex[]
 * @var $provider yii\data\ActiveDataProvider
 */

$this->params['currentCity'] = $model->city;
$this->params['headerMenu'] = $model->apartmentComplexMenu;
$this->params['headerMenuType'] = $model;
$this->params['breadcrumbs'][] = $model->label;
$this->registerJs('window.preloader.done();'); // TODO: get rid of temporary preloader bug fix.

echo CommonComplexScriptsWidget::widget(['currentComplex' => $model]);
?>

<div class="header__row header__row_apartment">
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'homeLink' => ['label' => t('_Breadcrumbs home label', 'header'), 'url' => app()->homeUrl, 'class' => 'bread-crumbs__link'],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'options' => ['class' => 'bread-crumbs bread-crumbs_grey header__bread-crumbs']
        ]) ?>
    </div>
</div>

<?= ApartmentTabsWidget::widget(['complex' => $model]) ?>

<?= $this->render('_complexMap', compact('mapModel')) ?>

<?= PageFormBottomBlockWidget::widget([
    'formTitle' => $model->form_bottom_title,
    'formDescription' => $model->form_bottom_description,
    'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
    'manager' => $model->manager,
    'blockClass' => 'questions_article catalogue_question commercial-question',
]) ?>


<?= PageFooterWidget::widget(['currentCity' => $model->city]) ?>