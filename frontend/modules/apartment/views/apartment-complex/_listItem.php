<?php

use frontend\helpers\ImgHelper;
use frontend\helpers\FrontendHelper;
use common\models\Apartment;
use common\components\CommonDataModel;

/**
 * @author art
 * @var $this yii\web\View
 * @var $model Apartment
 */
?>

<a href="<?= $model->getViewUrl() ?>" class="card apartaments__card wow fadeInUp">
    <div class="card__img">
        <div class="card__bg" data-bg-pos="center" data-bg-size="auto 90%" style="background: url('<?= ImgHelper::getImageSrc($model, 'complex', 'apartmentList', 'photo', Apartment::NO_IMAGE_PATH) ?>') center center / auto 90% no-repeat;"></div>
    </div>
    <div class="card__content">
        <h5 class="h5 card__h5"><?= /*$model->apartmentComplex->label . ', ' . */$model->label ?></h5>
        <p> Срок сдачи: <?= Yii::$app->formatter->asDate($model->delivery_date, 'php:d-m-Y') ?>  </p>
        <div class="divider card__divider"></div>
        <div class="card__text">
            <?php if ($model->canShowPrice()) : ?>
                <p><?= t('_from', 'apartment') . ' ' . $model->priceFormatted() ?></p>
                <?php if ($model->isResidential()) : ?>
                    <p><?= t('_installment', 'apartment') . ' ' . $model->installmentPriceFormatted() ?></p>
                <?php endif; ?>
            <?php endif ?>
            <?php if (!FrontendHelper::isNullOrEmpty($model->total_area)) : ?>
                <p><?= $model->total_area . ' ' . CommonDataModel::getAreaUnit() ?></p>
            <?php endif ?>
        </div>
    </div>
</a>
