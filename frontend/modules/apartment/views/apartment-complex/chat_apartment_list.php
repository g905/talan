<?php

use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
use frontend\widgets\FilterWidget;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\apartment\components\ApartmentsChatListView;
use frontend\modules\general\widgets\CommonComplexScriptsWidget;
use frontend\modules\apartment\widgets\apartmentsList\ApartmentsListTopBlockWidget;

/**
 * @var $model common\models\ApartmentComplex
 * @var $complexes common\models\ApartmentComplex[]
 * @var $provider yii\data\ActiveDataProvider
 */

//$this->params['currentCity'] = $model->city;
//$this->params['headerMenu'] = $model->apartmentComplexMenu;
//$this->params['headerMenuType'] = $model;
//$this->params['breadcrumbs'][] = ['label' => 'Недвижимость', 'url' => ['/page/page/catalog']];
//$this->params['breadcrumbs'][] = ['label' => $model->label, 'url' => ['/apartment/apartment-complex/view', 'type' => $model->type, 'alias' => $model->alias]];
$this->params['breadcrumbs'][] = Yii::t('front/apartment', 'Choose_Apartment');
$this->registerJs('window.preloader.done();'); // TODO: get rid of temporary preloader bug fix.
//echo CommonComplexScriptsWidget::widget(['currentComplex' => $model]);
?>

<div class="header__row header__row_apartment">
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([ 
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'homeLink' => ['label' => t('_Breadcrumbs home label', 'header'), 'url' => app()->homeUrl, 'class' => 'bread-crumbs__link'],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'options' => ['class' => 'bread-crumbs bread-crumbs_grey header__bread-crumbs']
        ]) ?>
    </div>
</div>

<!--
<?//= ApartmentsListTopBlockWidget::widget(['page' => $model]) ?>

-->

<?php $pjax = Pjax::begin([
    'id' => 'pjax-catalog-list',
    'timeout' => 25000,
    'enablePushState' => true,
    'scrollTo' => 0
    //'linkSelector' => '.pjax_link, .pagination__item a',
]) ?>
<style>
    .test2 {
        display: none;
    }
    .show_filter_btn {
        background: #1e9b57;
        margin-left: 5px;
        border: 1px solid #1e9b57;
        color: white;
        width: 100%;
    }
    .show_filter_btn a {
        margin: calc(50% - 25px);
        border: none;
        color: white;
    }
    .buffer__overflow-x-mobile {
        height: 100%;
    }
    .act {
        background: white;
    }
    .show_filter_btn.act a {
        color: black;
    }
    @media screen and (min-width: 767px) {
        .show_filter_btn {
            display: none;
        }
        .buffer__overflow-x-mobile {
            display: block;
        }
    }
</style>
<?php
$this->registerJs(
    "
    filter_flag = (localStorage.getItem('show_filter') === 'true');
    if (!filter_flag) {
        $('.buffer__overflow-x-mobile').addClass('test2');
        $('.show_filter_btn').removeClass('act');
    } else {
        $('.buffer__overflow-x-mobile').removeClass('test2');
        $('.show_filter_btn').addClass('act');
    } 
    $('.show_filter_btn').on('click', function () {
        filter_flag = (localStorage.getItem('show_filter') === 'true');
            $('.buffer__overflow-x-mobile').fadeToggle('fast', function () {
                //$(this).toggleClass('test2');
                console.log(filter_flag);
                if (!filter_flag) {
                    $('.buffer__overflow-x-mobile').addClass('test2');
                    $('.show_filter_btn').addClass('act');
                } else {
                    $('.buffer__overflow-x-mobile').removeClass('test2');
                    $('.show_filter_btn').removeClass('act');
                } 
            });
            //$('.show_filter_btn').toggleClass('act');
            
            localStorage.setItem('show_filter', !filter_flag);
            console.log(localStorage.getItem('show_filter') === 'true');
        });",
    \yii\web\View::POS_END,
    'show-filter-btn'
);
?>
    <div class="wrap temporary wrap_mobile_full">
        <div class="buffer">
            <div class="show_filter_btn wow fadeInUp">
                <a class="choose-ap__btn" href="#"> Фильтры </a>
            </div>
            <div class="buffer__overflow-x-mobile">
                <div class="buffer__overflow-mobile">
                    <?//= FilterWidget::widget(['complex' => $model, 'pjaxContainerID' => $pjax->getId()]) ?>
                </div>
            </div>
        </div>
        <?= ApartmentsChatListView::widget(['dataProvider' => $provider, 'params2' => $params]) ?>
    </div>
<?php $pjax::end() ?>

<!--
<?/*= PageFormBottomBlockWidget::widget([
    'formTitle' => $model->form_bottom_title,
    'formDescription' => $model->form_bottom_description,
    'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
    'manager' => $model->manager,
    'blockClass' => 'questions_article catalogue_question commercial-question',
]) */?>
-->

<?= PageFooterWidget::widget(['currentCity' => \common\models\City::getUserCity()]); ?>
