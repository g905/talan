<?php

use common\models\ApartmentBuilding;
use frontend\modules\apartment\widgets\building\BuildingHistoryWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;

//use frontend\modules\page\widgets\PageHeaderWidget;
use frontend\modules\page\widgets\TlNewAllPageHeader;

use frontend\modules\apartment\widgets\building\BuildingTopBlockWidget;
use frontend\modules\apartment\widgets\building\BuildingInfoWidget;

use frontend\modules\apartment\widgets\building\TlNewBuildingAdvantagesWidget;


use frontend\modules\apartment\widgets\building\BuildingActionWidget;
use frontend\modules\apartment\widgets\building\BuildingInvestmentWidget;
use frontend\modules\apartment\widgets\building\BuildingArticlesWidget;
use frontend\modules\apartment\widgets\building\BuildingProjectsWidget;
use frontend\modules\apartment\widgets\building\BuildingPlanWidget;
use frontend\modules\apartment\widgets\complex\ComplexCtaWidget;


use frontend\modules\page\widgets\PageHeaderWidget;

/**
 * @author dimarychek
 * @var ApartmentBuilding $model
 *
 *
 */
$this->params['currentCity'] = $model->city;

$this->params['breadcrumbs'][] = $model->label;
$this->params['headerMenu'] = $model->apartmentBuildingMenu;
$this->params['headerMenuType'] = $model;
?>

<?= TlNewAllPageHeader::widget(['complexPage' => $model]) ?>
<?= PageHeaderWidget::widget(['currentCity' => $model->city]) ?>

<?php // echo  BuildingInfoWidget::widget(['buildingPage' => $model]) ?>

<?= TlNewBuildingAdvantagesWidget::widget(['complexPage' => $model]) ?>

<?php
if (!empty($model->form_top_title_first) || !empty($model->form_top_title_second) || !empty($model->form_top_description) || !empty($model->form_top_emails)) {
    ?>
    <?= PageFormBottomBlockWidget::widget([
        'formTitle' => $model->form_top_title_first,
        'formTitleDark' => $model->form_top_title_second,
        'formDescription' => $model->form_top_description,
        'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_RIGHT,
        'blockClass' => 'questions_request',
        'formType' => PageFormBottomBlockWidget::FORM_TYPE_COMPLEX_PRESENTATION,
        'buildingId' => $model->id,
        'onsubmitJS' => $model->form_top_onsubmit,
    ]) ?>
    <?php
}
?>

<?php echo BuildingPlanWidget::widget(['building' => $model]) ?>

<?php echo BuildingActionWidget::widget(['buildingPage' => $model]) ?>

<?php echo ComplexCtaWidget::widget(['complexPage' => $model]) ?>

<?php
if (!empty($model->investment_title_first) || !empty($model->investment_title_second) || !empty($model->investment_btn_label) || !empty($model->investment_btn_link) || !empty($model->investment_description)) {
    ?>
    <?php echo BuildingInvestmentWidget::widget(['buildingPage' => $model]) ?>
    <?php
}
?>

<?php
if (!empty($model->form_middle_title_first) || !empty($model->form_middle_title_second) || !empty($model->form_middle_description) || !empty($model->form_middle_emails)) {
    ?>
    <?= PageFormBottomBlockWidget::widget([
        'formTitle' => $model->form_middle_title_first,
        'formTitleDark' => $model->form_middle_title_second,
        'formDescription' => $model->form_middle_description,
        'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_RIGHT,
        'blockClass' => 'questions_request',
        'formType' => PageFormBottomBlockWidget::FORM_TYPE_GUARANTEE,
        'buildingId' => $model->id,
        'problems' => $model->problems,
        'onsubmitJS' => $model->form_middle_onsubmit,
    ]) ?>
    <?php
}
?>
<?= BuildingArticlesWidget::widget(['buildingPage' => $model]) ?>

<?= BuildingHistoryWidget::widget(['title' => $model->build_history_title, 'model' => $model]) ?>

<?= BuildingProjectsWidget::widget(['buildingPage' => $model]) ?>

<?= PageFormBottomBlockWidget::widget([
    'formTitle' => $model->form_bottom_title,
    'formDescription' => $model->form_bottom_description,
    'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
    'manager' => $model->manager,
    'buildingId' => $model->id,
    'blockClass' => 'questions_article catalogue_question commercial-question',
    'onsubmitJS' => $model->form_bottom_onsubmit,
]) ?>

<?= PageFooterWidget::widget(['currentCity' => $model->city]) ?>
