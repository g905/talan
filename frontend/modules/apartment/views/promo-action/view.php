<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 25.12.2017
 * Time: 13:51
 */

use common\models\City;
use frontend\helpers\FrontendHelper;
use frontend\helpers\ImgHelper;
use frontend\modules\apartment\widgets\promoAction\ActionBottomFormWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\page\widgets\PageShareWidget;

/** @var \common\models\PromoAction $model */
$this->params['breadcrumbs'][] = $model->label;
?>

<div class="header__row header__row_article">
    <div class="wrap wrap_mobile_full">
        <?= \yii\widgets\Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'homeLink' => [
                'label' => Yii::t('front/header', '_Breadcrumbs home label'),
                'url' => Yii::$app->homeUrl,
                'class' => 'bread-crumbs__link',
            ],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'options' => ['class' => 'bread-crumbs bread-crumbs_grey header__bread-crumbs']
        ]) ?>
    </div>
</div>

<?= PageShareWidget::widget(['orientation' => PageShareWidget::ORIENTATION_VERTICAL]) ?>

<section class="article">
    <div class="wrap wrap_small wrap_mobile_full">
        <h3 class="article__title  wow fadeInUp"><?= $model->label ?></h3>
        <div class="article__date wow fadeInUp" data-wow-delay="0.3s"><?= date('d.m.Y', $model->created_at) ?></div>
        <div class="article__content wow fadeInUp" data-wow-delay="0.5s">
            <img src="<?= ImgHelper::getImageSrc($model, 'promoAction', 'viewMain', 'image') ?>" alt="">
            <?= $model->content ?>
        </div>
    </div>
</section>

<div class="wrap wrap_small wrap_mobile_full">
    <?= PageShareWidget::widget(['orientation' => PageShareWidget::ORIENTATION_HORIZONTAL]) ?>
</div>

<?= PageFormBottomBlockWidget::widget([
    'formTitle' => Yii::t('front/promo-action', '_We are always ready to answer any of your questions'),
    'formDescription' => Yii::t('front/promo-action', '_Leave your data and write the question you are interested in. Our managers will contact you as soon as possible, and they will consult you in a qualified manner'),
    'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
    'manager' => $model->manager,
    'blockClass' => 'questions_article catalogue_question commercial-question',
    'onsubmitJS' => !empty($model->form_onsubmit) ? $model->form_onsubmit : config()->get('defaultPromoOnSubmit', ''),
]) ?>


<?= PageFooterWidget::widget(['currentCity' => City::getUserCity()]) ?>
