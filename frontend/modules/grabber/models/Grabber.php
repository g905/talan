<?php

namespace frontend\modules\grabber\models;

use yii\base\Model;
use common\models\FpmFile;
use yii\web\NotFoundHttpException;
use metalguardian\fileProcessor\helpers\FPM;

/**
 * Class Grabber
 * @package frontend\modules\grabber\models
 */
class Grabber extends Model
{
    /**
     * @param $name
     * @return mixed
     * @throws NotFoundHttpException
     */
    public static function getGrubberImage($name)
    {
        $name = preg_replace('/\\.[^.\\s]{3,4}$/', '', $name);

        $image = FpmFile::find()->where(['base_name' => $name])->one();

        if ($image) {
            $root = getAlias('@webroot');

            $src = FPM::originalSrc($image->id);

            if (file_exists($root . $src)) {
                header('Content-Type:image/'.$image->extension);

                return require $root . $src;
            }

            throw new NotFoundHttpException();
        }

        throw new NotFoundHttpException();
    }
}
