<?php

namespace frontend\modules\grabber;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\grabber\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
