<?php

namespace frontend\modules\grabber\controllers;

use frontend\components\FrontendController;
use frontend\modules\grabber\models\Grabber;

/**
 * Class GrabberController
 * @package frontend\modules\grabber\controllers
 */
class GrabberController extends FrontendController
{

    /**
     * @param $name
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex($name)
    {
        return Grabber::getGrubberImage($name);
    }
}
