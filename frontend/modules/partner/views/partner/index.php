<?php
use frontend\modules\page\widgets\PageHeaderWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use yii\widgets\Breadcrumbs;
use frontend\helpers\FrontendHelper;
use common\models\City;
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 6/17/18
 * Time: 11:11 PM
 *
 * @var $partnerModel \common\models\PartnerPage
 */
$this->params['breadcrumbs'][] = $partnerModel->label;
$this->params['headerMenu'] = $partnerModel->menus;
$this->params['headerMenuType'] = $partnerModel;
?>
<div class="header__row header__row_apartment">
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'homeLink' => [
                'label' => t('_Breadcrumbs home label', 'header'),
                'url' => app()->homeUrl,
                'class' => 'bread-crumbs__link',
            ],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'options' => ['class' => 'bread-crumbs bread-crumbs_many bread-crumbs_grey header__bread-crumbs']
        ]) ?>
    </div>
</div>

<div class="how-to-buy partners">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 how-to-buy__h2 wow fadeInLeft"> <?= $partnerModel->label ?> </h2>
        <div class="how-to-buy__list">
            <?php foreach($partnerModel->cards as $card) : ?>
            <?= $this->render('_card', ['model' => $card]) ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?= PageFormBottomBlockWidget::widget([
    'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
    'manager' => $partnerModel->manager,
    'formTitle' => $partnerModel->form_bottom_title,
    'formDescription' => $partnerModel->form_bottom_description,
    'blockClass' => 'questions_article commercial-question',
    'onsubmitJS' => $partnerModel->form_onsubmit,
]) ?>

<?= PageFooterWidget::widget(['currentCity' => City::getUserCity()])?>