<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 6/17/18
 * Time: 11:54 PM
 *
 * @var $model \common\models\PartnerPageCard
 */
?>
<a href="<?= $model->link_url ?>" style="background-color: <?= $model->color ?>" class="card how-to-buy__card wow fadeInUp">
    <div class=" card__title"> <?= $model->label ?> </div>
    <div class="card__text">
        <p> <?= $model->content ?> </p>
    </div>
    <div class="card__more"> <?= $model->link_text ?> </div>
</a>
