<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 6/17/18
 * Time: 11:08 PM
 */

namespace frontend\modules\partner\controllers;

use yii\web\NotFoundHttpException;
use common\models\City;
use common\models\PartnerPage;
use frontend\components\MetaRegistrar;
use frontend\components\FrontendController;

class PartnerController extends FrontendController
{
    public function actionIndex($city = null)
    {
        $currentCity = City::getCurrentCity($city);
        $partnerModel = PartnerPage::getModelByCity($currentCity);
        if (!$partnerModel) {
            throw new NotFoundHttpException();
        }
        MetaRegistrar::register($partnerModel, $partnerModel->label);

        return $this->render('index', [
            'partnerModel' => $partnerModel,
        ]);
    }
}
