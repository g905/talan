<?php

namespace frontend\modules\partner;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\partner\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
