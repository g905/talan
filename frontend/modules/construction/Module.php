<?php

namespace frontend\modules\construction;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\construction\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
