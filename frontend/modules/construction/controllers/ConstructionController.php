<?php

namespace frontend\modules\construction\controllers;

use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use frontend\components\FrontendController;
use frontend\modules\construction\models\Construction;

/**
 * Class ConstructionController
 * @package frontend\modules\construction\controllers
 */
class ConstructionController extends FrontendController
{
    /**
     * @param $timestamp
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($timestamp)
    {
        $guid = '';
        if(isset($_GET['complex']))
        {
            $guid = $_GET['complex'];
        }
        if ($timestamp >= 0) {
            return Json::encode(new Construction($timestamp, $guid));
        }

        throw new NotFoundHttpException();
    }
}
