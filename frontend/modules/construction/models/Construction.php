<?php

namespace frontend\modules\construction\models;

use yii\base\Model;
use yii\helpers\Url;
use metalguardian\fileProcessor\helpers\FPM;
use common\models\BuildHistoryApartmentComplexItemSlide;

/**
 * Class Construction
 * @package frontend\modules\construction\models
 */
class Construction extends Model
{
    public $construction;

    /**
     * Construction constructor.
     * @param $timestamp
     * @throws \Exception
     */
    public function __construct($timestamp, $guid)
    {
        parent::__construct();

        $this->construction = $this->getConstruction($timestamp, $guid);
    }

    /**
     * @param $timestamp
     * @return array
     * @throws \Exception
     */
    public function getConstruction($timestamp, $cid)
    {
        $slides = BuildHistoryApartmentComplexItemSlide::getBuildHistory($cid);
//dd($slides[456]->buildHistoryApartmentComplexItem->apartmentComplex->guid);
        $construction = [];
        //$construction['item_id'] = $slides[0]->buildHistoryApartmentComplexItem->id;

        foreach ($slides as $slide) {

            if(!$slide->buildHistoryApartmentComplexItem) {
                echo ($slide->buildHistoryApartmentComplexItem);
                continue;
            }

            $time = $this->getTime($slide);
            $creation_time = $this->getCreationTime($slide);

            if (($time >= $timestamp) OR ($slide->updated_at >= $timestamp)) {

                $construction[] = [
                    //'тест_таймМесяцХодаСтроительства' => $time,
                    //'тест_таймСлайдаХодаСтроительства' => $slide->updated_at,
                    'item_id' => $slide->id,
                    'id' => $slide->buildHistoryApartmentComplexItem->apartmentComplex->guid,
                    'date' => $time,
                    'creation_date' => $creation_time,
                    'content' => $slide->content,
                    'images' => $this->getSliderImages($slide),
                ];
            }
        }

        return $construction;
    }

    /**
     * @param $slide
     * @return array|null
     * @throws \Exception
     */
    public function getSliderImages($slide)
    {
        $basePath = Url::base(true);
        $images = [];

        foreach ($slide->image as $image) {
            $images[] = [
                'src' => $basePath.FPM::originalSrc($image->file_id ?? '')
            ];
        }

        if (!empty($images)) {
            return $images;
        }

        return null;
    }

    /**
     * @param $slide
     * @return false|int
     */
    public function getTime($slide)
    {
        $historyItem = $slide->buildHistoryApartmentComplexItem;

        if ($historyItem) {
            $date = explode('-', $slide->buildHistoryApartmentComplexItem->date);

            $date[2] = str_pad($slide->day, 2, 0, STR_PAD_LEFT);

            //return strtotime(implode('-', $date));
            //return $historyItem->updated_at;
	    return $slide->updated_at;
        }

        return false;
    }

    public function getCreationTime($slide)
    {
        $historyItem = $slide->buildHistoryApartmentComplexItem;

        if ($historyItem) {
            $date = explode('-', $slide->buildHistoryApartmentComplexItem->date);

            $date[2] = str_pad($slide->day, 2, 0, STR_PAD_LEFT);

            return strtotime(implode('-', $date));
            //return $historyItem->updated_at;
        }

        return false;
    }
}
