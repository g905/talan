<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 6/15/18
 * Time: 12:06 AM
 */

namespace frontend\modules\agents\controllers;

use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use common\models\City;
use common\models\Agents;
use common\models\AboutCompany;
use common\models\TeamPageDefaultCityTab;
use frontend\components\MetaRegistrar;
use frontend\components\FrontendController;

class AgentsController extends FrontendController
{
    /**
     * @param null $city
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex($city = null)
    {
        $currentCity = City::getCurrentCity($city);

        $teamPage = Agents::getModelByCity($currentCity);
        $aboutModel = AboutCompany::findByCity($currentCity);
        if (!$teamPage || !$aboutModel) {
            throw new NotFoundHttpException();
        }
        //$defaultTabDataModel->label = "test";// = (new TeamPageDefaultCityTab())->get();
        MetaRegistrar::register($teamPage, $teamPage->label);

        return $this->render('index', [
            'teamPage' => $teamPage,
            'active' => 'deals'
        ]);
    }

    public function actionCityItems($city = null)
    {
        $currentCity = City::getCurrentCity($city);

        $sort = 'service_evaluation';

        $defaultTabDataModel = Agents::getModelByCity($currentCity)->getAgentsMembers($sort)->all();

        $result = '';
        foreach ($defaultTabDataModel as $member) {
            $result .= $this->renderPartial('_agentsMembers', [
                'model' => $member, 'active' => 'service'
            ]);
        }
        $result = Html::tag('div', $result, ['class' => 'branch_team__players']);

        return $result;
    }
}
