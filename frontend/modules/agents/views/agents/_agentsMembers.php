<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 6/15/18
 * Time: 1:06 AM
 *
 * @var $model \common\models\TeamMember
 */
?>
<div class="branch_team__players-player wow fadeInUp">
    <div class="branch_team__players-player__img agent-video-img">
        <?php if($model->youtube_link !== ""):?>
<div class="agent-video-wrap">
    <button class="footer__button agent-video-button ajax-link" style="width: 150px" data-href="<?= \common\helpers\SiteUrlHelper::agentsVideoPopup(['video' => $model->youtube_link]) ?>"><?= Yii::t('front/agents', 'watch_agent_video')?></button>
</div>
        <?php endif;?>
        <img src="<?= $model->getBackgroundImage() ?>" >
    </div>
    <h6 class="branch_team__players-player__name wow fadeInUp" data-wow-delay="0.05s"> <?= $model->fio ?> </h6>
    <hr class="branch_team__players-player__line wow fadeInUp" data-wow-delay="0.1s">
    <p class="branch_team__players-player__position wow fadeInUp" data-wow-delay="0.15s">
        Рейтинг агента:
        <span class="<?= $active == 'deals' ? 'agent_active' : '' ?>"><?= $model->deals_amount ?></span>/<span class="<?= $active == 'service' ? 'agent_active' : '' ?>"><?= $model->service_evaluation ?></span>
    </p>
    <p class="branch_team__players-player__position wow fadeInUp" data-wow-delay="0.2s"><?= $model->description ?></p>

    <a class="button button_green branch_team__players-player__btn wow fadeInUp ajax-link" data-wow-delay="0.25s" data-href="<?= '/agents-show-phone-popup/'.$model->id ?>" style="width: 100%!important">
        <?= Yii::t('front/agents', 'show_phone') ?>
        <span class="button__bg"></span>
        <div class="button__blip button__blip_hover"></div>
        <div class="button__blip button__blip_click"></div>
    </a>
</div>

<style>

    .agent-video-wrap {
        z-index: 99;
        width: 100%;
        height: 100px;
        position: absolute;
        bottom: 0;
        background: rgba(0,0,0,.2);
        line-height: 100px;
        text-align: center;
        opacity: 0;
        transition: opacity .5s ease-in-out;
    }

    .agent-video-img:hover .agent-video-wrap {
        opacity: 1;
    }

    .agent-video-button {
        width: 150px;
        z-index: 199;
        color: white;
    }

    .agent-video-img {
        position: relative;
    }

</style>
