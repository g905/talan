<?php
use frontend\modules\page\widgets\PageHeaderWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use common\helpers\SiteUrlHelper;
use common\models\City;
use metalguardian\fileProcessor\helpers\FPM;
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 6/15/18
 * Time: 12:08 AM
 *
 * @var $this \yii\web\View
 * @var $teamPage \common\models\Team
 * @var $defaultTabDataModel \common\models\TeamPageDefaultCityTab
 * @var $aboutModel \common\models\AboutCompany
 */

$this->params['data-pages'] = 'team-page';
/*$this->params['breadcrumbs'][] = [
    'label' => Yii::t('front/team', 'about_company'),
    'url' => SiteUrlHelper::createAboutCompanyPageUrl(),
];*/
$this->params['breadcrumbs'][] = $teamPage->label;
//$this->params['headerMenu'] = $aboutModel->aboutCompanyMenu;
//$this->params['headerMenuType'] = $aboutModel;
?>
<?= PageHeaderWidget::widget(['currentCity' => City::getUserCity()])?>

<section class="poster poster_padding">

    <video class="poster__bg poster__bg_video" autoplay loop muted data-video-src="" data-poster-video data-autoplay data-keepplaying></video>
    <div class="poster__bg poster__bg_photo" data-bg-src="<?= $teamPage->getPageBackgroundImage() ?>" data-bg-pos="center" data-bg-size="cover"></div>
    <div class="poster__cover"></div>
    <div class="poster__inner">
        <div class="poster__content">
            <h1 class="h1 poster__h1 wow fadeInUp"> <?= $teamPage->label ?> </h1>
            <h5 class="h5 poster__h5 wow fadeInUp" data-wow-delay="0.3s"> <?= $teamPage->description ?> </h5>
        </div>
    </div>
    <div class="down poster__down wow down-to-top">
        <button class="down__button" data-scroll-down></button>
        <div class="down__label"><?= Yii::t('front/team', 'down') ?></div>
    </div>
</section>
<!-- /POSTER -->
<section class="branch_team">
    <div class="branch_team__wrap wrap wrap_mobile_full">
        <h2 class="h2 branch_team__title wow fadeInUp"> <?= $teamPage->agents_title ?> </h2>
        <div class="branch_team__wrap-btn wow fadeInUp" style="justify-content: space-between;">
            <div class="branch_team__wrap-btn wow fadeInUp">
                <a href=# class="branch_team__btn branch_team__btn-a"> <?= $teamPage->getTabsLabels('deals') ?> </a>
                <a href=# class="branch_team__btn " data-url="<?= SiteUrlHelper::getAgentsCityItemsUrl() ?>"> <?= $teamPage->getTabsLabels('service') ?> </a>
            </div>
            <div class="branch_team__wrap-btn wow fadeInUp">
                <?php if($teamPage->youtube_link !== ""):?>
                    <button class="footer__button ajax-link" style="width: 150px" data-href="<?= SiteUrlHelper::agentsVideoPopup(['video' => $teamPage->youtube_link]) ?>"><?= Yii::t('front/agents', 'watch_video')?></button>
                <?php endif;?>
                <button class="footer__button ajax-link" style="width: 150px" data-href="<?= SiteUrlHelper::agentsTakePartPopupForm() ?>"><?= Yii::t('front/agents', 'take_part') ?></button>
            </div>
        </div>
        <div id="players_wrap" class=" branch_team__tabs">
            <div class="branch_team__tab branch_team__tab-active has-content">
                <div class="branch_team__players">
                    <?php foreach($teamPage->agentsMembers as $member) : ?>
                    <?= $this->render('_agentsMembers', ['model' => $member, 'active' => $active]) ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="branch_team__tab "> </div>
        </div>
    </div>
</section>

    <style>
        .agent_active {
           font-weight: bold;
        }
    </style>

<?= PageFooterWidget::widget(['currentCity' => City::getUserCity()])?>