<?php

namespace frontend\modules\agents;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\agents\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
