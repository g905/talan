<?php

namespace frontend\modules\blog\models;

use common\models\City;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\data\ActiveDataProvider;
use common\models\Article;
use common\models\BlogTheme;
use common\helpers\SiteUrlHelper;
use frontend\modules\blog\helpers\BlogDataProviderHelper;
use yii\db\Expression;

/**
 * Class BlogSearch
 *
 * @author maxim
 * @package frontend\modules\blog\models
 */
class BlogSearch extends Model
{
    public $theme;

    public $year;

    private $cityId;

    public function __construct(array $config = [])
    {
        $this->cityId = obtain('id', City::getUserCity());
        parent::__construct($config);
    }

    /**
     * @var self
     */
    protected static $_staticModel;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['theme', 'year'], 'safe'],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function getDataProvider()
    {
        /*
        $query = $this->getBaseQuery();
        if ($this->validate()) {
            $query
                ->joinWith(['blogThemes' => function(ActiveQuery $query) {
                    $query->alias('theme');
                }], false)
                ->andFilterWhere(['theme.alias' => $this->theme])
                ->andFilterHaving(['year' => $this->year]);
        }*/

        $query = Article::find()
            ->alias('a')
            //->select(new Expression('DATE_FORMAT(date_published, "%Y") as year'))
            ->joinWith('blogThemes b', false)
            ->joinWith('cities c', false )
            ->joinWith('citiesToArticle cta')
            ->where(['cta.city_id' => $this->cityId])
            ->orWhere(['cta.city_id' => null])
            ->andWhere(['a.published' => true])
            ->orderBy('date_published desc');
        if($this->cityId)
        {
            //
        }
        if($this->theme)
        {
            $query->where(['b.alias' => $this->theme]);
        }
        if($this->year)
        {
            $query->andWhere(['=', new Expression('DATE_FORMAT(date_published, "%Y")'), $this->year]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
/*            'pagination' => [
                'defaultPageSize' => BlogDataProviderHelper::getStaticModel()->getItemsPerPageCount(),
                'forcePageParam' => true,
                //'pageSize' => 12,
                //'page' => 0
            ]*/
        ]);

        return $dataProvider;
    }

    /**
     * @param string $alias
     * @return Article|null|\yii\db\ActiveRecord
     */
    public function getArticleByAlias($alias)
    {
        //return $this->getBaseQuery()->andWhere(['a.alias' => $alias])->one();
        return Article::find()->alias('a')->where(['a.alias' => $alias])->one();
    }

    public function getBaseQuery()
    {
        return Article::find()
            ->alias('a')
            ->select(['a.*', 'DATE_FORMAT(date_published, "%Y") as year'])
            ->where(['a.published' => 1])
            ->joinWith(['citiesToArticle' => function (ActiveQuery $query) {
                $query->alias('cta');
                $query->where(['cta.city_id' => $this->cityId]);
                $query->orWhere(['cta.city_id' => null]);
            }])
            ->orderBy(['date_published' => SORT_DESC]);
    }

    /**
     * @return array
     */
    public function getThemesList()
    {
        $allThemes = BlogTheme::find()
            ->select([
                'label',
                'alias',
            ])
            ->asArray()
            ->all();

        $result = [
            'active' => null,
            'list' => [
                SiteUrlHelper::getBlogAllUrl() => Yii::t('front/blog', 'all_themes'),
            ]
        ];

        foreach ($allThemes as $theme) {
            if ($theme['alias'] == $this->theme) {
                $result['active'] = $theme['label'];
            } else {
                $url = SiteUrlHelper::getBlogAllUrl(['theme' => $theme['alias']]);
                $result['list'][$url] = $theme['label'];
            }
        }

        if (is_null($result['active'])) {
            $result['active'] = Yii::t('front/blog', 'all_themes');
            unset($result['list'][SiteUrlHelper::getBlogAllUrl()]);
        }

        return $result;
    }

    /**
     * @param bool $loadFromRequest
     * @return BlogSearch
     */
    public static function getStaticModel($loadFromRequest = true)
    {
        if (!isset(static::$_staticModel)) {
            $m = new BlogSearch();
            if ($loadFromRequest) {
                $m->load(request()->get(), '');
            }
            static::$_staticModel = $m;
        }

        return static::$_staticModel;
    }
}
