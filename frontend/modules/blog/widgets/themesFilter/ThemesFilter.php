<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.04.18
 * Time: 15:17
 */

namespace frontend\modules\blog\widgets\themesFilter;


use frontend\modules\blog\models\BlogSearch;
use yii\base\Widget;

class ThemesFilter extends Widget
{
    const SELECT_CLASS_FIX = 'select select_submenu-fix select_always-visible header__select';
    const SELECT_CLASS_TOP = 'select select_submenu-top select_always-visible header__select';

    public $selectClass = self::SELECT_CLASS_FIX;

    /***
     * @return string
     */
    public function run()
    {
        $items = BlogSearch::getStaticModel()->getThemesList();

        return $this->render('themesFilterView', [
            'items' => $items,
            'selectClass' => $this->selectClass
        ]);
    }
}