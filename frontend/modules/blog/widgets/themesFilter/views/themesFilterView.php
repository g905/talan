<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.04.18
 * Time: 15:18
 *
 * @var $selectClass string
 */
?>


<div class="header__row">
    <div class="wrap wrap_mobile_full">
        <div class="header__line"></div>
        <div class="submenu header__submenu">
            <?php foreach ($items['list'] as $url => $item) : ?>
                <a href="<?= $url ?>" class="submenu__item">
                    <?= $item ?>
                </a>
            <?php endforeach ?>
            <a class="submenu__item submenu__item_active">
                <?= $items['active'] ?>
            </a>
        </div>
        <div class="select select_submenu-fix header__select">
            <button class="select__btn" data-dropdown-cities-btn="about-submenu-fix">
                <?= $items['active'] ?>
            </button>
            <div class="select__options" data-dropdown-cities="about-submenu-fix">
                <?php foreach ($items['list'] as $url => $item): ?>
                    <a href="<?= $url ?>" class="select__option"><?= $item ?></a>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
