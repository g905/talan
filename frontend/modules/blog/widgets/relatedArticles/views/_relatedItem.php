<?php
/**
 * @var $this yii\web\View
 * @var $bg string
 * @var $link string
 * @var $title string
 * @var $date string
 */
?>

<div class="card articles__card  wow fadeInUp">
    <div class="card__img" data-bg-src="<?= $bg ?>" data-bg-pos="center" data-bg-size="cover">
        <a href="<?= $link ?>"></a>
    </div>
    <div class="card__content">
        <h5 class="h5 card__h5">
            <a href="<?= $link ?>"><?= $title ?></a>
        </h5>
        <span class="card__date"><?= $date ?></span>
        <span class="divider card__divider"></span>
        <ul class="card__tags"></ul>
        <div class="card__text"><p></p></div>
    </div>
</div>
