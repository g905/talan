<?php
/**
 * @var $this yii\web\View
 * @var $related string
 */
?>

<section class="articles articles_journal">
    <div class="wrap wrap_small wrap_mobile_full">
        <h2 class="h2 articles_journal__title"><?= t('_related articles', 'blog') ?></h2>
        <div class="articles__list"><?= $related ?></div>
    </div>
</section>
