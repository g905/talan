<?php

namespace frontend\modules\blog\widgets\relatedArticles;

use common\models\City;
use yii\base\Widget;
use yii\db\ActiveQuery;
use common\models\Article;
use common\helpers\SiteUrlHelper;
use metalguardian\fileProcessor\helpers\FPM;

/**
 * Class RelatedArticles
 *
 * @author Bogdan Fedun <delagics@gmail.com>
 */
class RelatedArticles extends Widget
{
    /**
     * @var Article associated article model.
     */
    public $model;
    /**
     * @var int related articles fetch limit.
     */
    public $limit = 4;

    public function run()
    {
        $related = $this->renderRelated();

        if ($related) {
            $related = $this->render('relatedArticles', compact('related'));
        }

        return $related;
    }

    private function renderRelated()
    {
        $output = '';
        $related = $this->fetchRelated();

        if ($related) {
            foreach ($related as $article) {
                $bg = FPM::src($article->miniImageAttr->file_id ?? null, 'article', 'itemfull');
                $link = SiteUrlHelper::getBlogViewUrl(['alias' => $article->alias]);
                $title = $article->label;
                $date = $article->getDisplayableViewDate();
                $output .= $this->render('_relatedItem', compact('bg', 'link', 'title', 'date'));
            }
        }

        return $output;
    }

    /**
     * Fetch related articles.
     *
     * @return array|Article[]|\yii\db\ActiveRecord[]
     */
    private function fetchRelated()
    {
        $ids = [];
        $related = [];
        $city = City::getUserCity();
        $themes = $this->model->blogThemes;
        if ($themes) {
            foreach ($themes as $theme) {
                $ids[] = $theme->id;
            }
        }

        if ($ids) {
            $articles = Article::find()
                ->alias('article')
                ->isPublished()
                ->innerJoinWith(['themesToArticle' => function (ActiveQuery $query) use ($ids) {
                    $query->andWhere(['blog_theme_id' => $ids]);
                }])
                ->andWhere(['!=', 'article.id', $this->model->id])
                ->orderBy(['article.date_published' => SORT_DESC])
                ->limit($this->limit)
                ->all();

            foreach ($articles as $article) {
                if(!$article->cities) {
                    $related[] = $article;
                } else {
                    foreach ($article->cities as $item) {
                        if ($city->id == $item->id) {
                            $related[] = $article;
                        }
                    }
                }
            }
        }

        return $related;
    }
}
