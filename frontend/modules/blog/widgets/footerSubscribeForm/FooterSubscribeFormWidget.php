<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.04.18
 * Time: 13:52
 */

namespace frontend\modules\blog\widgets\footerSubscribeForm;


use frontend\modules\form\models\BottomSubscribeForm;
use yii\base\Widget;

class FooterSubscribeFormWidget extends Widget
{
    public $titleBlack;
    public $titleWhite;
    public $agreement;
    public $btnText;
    public $onsubmit;

    /**
     * @var BottomSubscribeForm
     */
    public $subscribeModel;

    /**
     * @return string
     */
    public function run()
    {
        if (!$this->subscribeModel) {
            $this->subscribeModel = new BottomSubscribeForm([
                'attributeLabelsData' => [
                    'agree' => $this->agreement,
                ]
            ]);
        }

        return $this->render('footerSubscribeFormWidgetView', [
            'titleBlack' => $this->titleBlack,
            'titleWhite' => $this->titleWhite,
            'btnText' => $this->btnText,
            'model' => $this->subscribeModel,
            'onsubmit' => $this->onsubmit,
        ]);
    }
}
