<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.04.18
 * Time: 13:53
 *
 * @var $titleBlack string
 * @var $titleWhite string
 * @var $btnText string
 * @var $onsubmit string
 * @var $model \frontend\modules\form\models\BottomSubscribeForm
 */
?>
<div class="questions journal_questions" data-form-outer>
    <div class="form-content questions__form-content">
        <div class="wrap wrap_mobile_full">
            <div class="questions__columns">
                <div class="questions__column">
                    <h2 class="h2 questions__h2 wow fadeInLeft">
                        <strong><?= $titleBlack ?></strong> <?= $titleWhite ?> </h2>
                </div>
                <div class="questions__column">
                    <!-- SUBSCRIBE FORM -->
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'action' => \common\helpers\SiteUrlHelper::getSubscribeBottomFormSubmitUrl(),
                        'method' => 'post',
                        'options' => [
                            'class' => 'form subscribe__form wow fadeInUp ajax-form',
                            'data-onsubmit' => $onsubmit ?: config()->get('request_subscribe_onsubmit', null)
                        ]
                    ]) ?>
                        <?= $form->field($model, 'email', [
                            'template' => '<label class="field form__field">{input}<span class="form__label">{label}</span>{error}</label>',
                            'inputOptions' => [
                                'class' => 'input form__input',
                                'type' => 'email',
                            ],
                            'options' => [
                                'class' => 'form__row',
                            ],
                            'errorOptions' => [
                                'class' => 'field__error',
                                'tag' => 'span'
                            ]
                        ]) ?>
                        <div class="form__row">
                            <?= $form->field($model, 'agree', [
                                'template' => '<label class="checkbox form__checkbox">{input}<span class="checkbox__mask"></span>{label}{error}</label>',
                                'options' => [
                                    'class' => 'field form__field',
                                ],
                                'errorOptions' => [
                                    'class' => 'field__error',
                                    'tag' => 'span'
                                ]
                            ])->checkbox([
                                'class' => 'checkbox__checkbox'
                            ], false)->label($model->getAttributeLabel('agree'), [
                                'class' => 'checkbox__label',
                            ]) ?>
                        </div>
                        <div class="form__row">
                            <button type="submit" class="button form__button wow fadeInUp"> <?= $btnText ?>
                                <span class="button__blip button__blip_hover"></span>
                                <span class="button__blip button__blip_click"></span>
                            </button>
                        </div>
                    <?php \yii\widgets\ActiveForm::end(); ?>
                    <!-- /SUBSCRIBE FORM -->
                </div>
            </div>
        </div>
    </div>
</div>
