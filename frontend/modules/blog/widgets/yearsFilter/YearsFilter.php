<?php

namespace frontend\modules\blog\widgets\yearsFilter;

use yii\base\Widget;
use common\models\Article;

class YearsFilter extends Widget
{
    public function run()
    {
        $years = Article::getYears();

        $checked = request()->get('year') ?? false;

        if ($years) {
            return $this->render('view', [
                'years' => $years,
                'checked' => $checked,
            ]);
        }
    }
}
