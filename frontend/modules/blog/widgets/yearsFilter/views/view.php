<div class="vacancy__cities years-select first-select wow fadeInRight" data-wow-delay="1s" data-wow-duration="1s">
    <div class="years-select__title"><?= Yii::t('front/blog', 'Years') ?></div>
    <div class="filters__section">
        <div class="filters__inner filters__inner_side_right">
            <div class="custom-select filters__custom-select" data-custom-select>
                <select name="year" class="year_filter">
                    <option value="false"><?= Yii::t('front/blog', 'All') ?></option>
                    <?php foreach ($years as $year) : ?>
                        <option <?= $checked == $year ? 'selected' : '' ?> value="<?= $year ?>"><?= $year ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>
</div>
