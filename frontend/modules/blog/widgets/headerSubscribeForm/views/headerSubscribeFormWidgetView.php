<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.04.18
 * Time: 15:53
 *
 * @var $model \frontend\modules\form\models\SubscribeForm
 * @var $title string
 * @var $text string
 * @var $btnText string
 * @var $imagePath string
 * @var $videoPath string
 * @var $onsubmit string
 */
?>
<section class="poster poster_padding">
    <video class="poster__bg poster__bg_video" autoplay loop muted data-video-src="<?= $videoPath ?>" data-poster-video data-autoplay data-keepplaying></video>
    <div class="poster__bg poster__bg_photo" data-bg-src="<?= $imagePath ?>" data-bg-pos="center" data-bg-size="cover"></div>
    <div class="poster__cover"></div>
    <div class="poster__inner">
        <div class="poster__content">
            <h1 class="h1 poster__h1 wow fadeInUp"> <?= $title ?> </h1>
            <h5 class="h5 poster__h5 wow fadeInUp" data-wow-delay="0.3s"> <?= $text ?> </h5>
            <!-- SUBSCRIBE FORM -->
            <?php $form = \yii\widgets\ActiveForm::begin([
                'action' => \common\helpers\SiteUrlHelper::getSubscribeFormSubmitUrl(),
                'method' => 'post',
                'options' => [
                    'class' => 'form subscribe__form wow fadeInUp ajax-form',
                    'data-onsubmit' => $onsubmit,
                ],
            ]) ?>
                <?= $form->field($model, 'email', [
                    'template' => '<label class="field form__field">{input}<span class="form__label">{label}</span>{error}</label>',
                    'inputOptions' => [
                        'class' => 'input form__input',
                        'type' => 'email',
                    ],
                    'options' => [
                        'class' => 'form__row',
                    ],
                    'errorOptions' => [
                        'class' => 'field__error',
                        'tag' => 'span'
                    ]
                ]) ?>
                <div class="form__row">
                    <?= $form->field($model, 'agree', [
                        'template' => '<label class="checkbox form__checkbox">{input}<span class="checkbox__mask"></span>{label}{error}</label>',
                        'options' => [
                            'class' => 'field form__field',
                        ],
                        'errorOptions' => [
                            'class' => 'field__error',
                            'tag' => 'span'
                        ]
                    ])->checkbox([
                        'class' => 'checkbox__checkbox'
                    ], false)->label($model->getAttributeLabel('agree'), [
                        'class' => 'checkbox__label',
                    ]) ?>
                </div>
                <div class="form__row">
                    <button type="submit" class="button button_transparent wow fadeInUp"> <?= $btnText ?>
                        <span class="button__blip button__blip_hover"></span>
                        <span class="button__blip button__blip_click"></span>
                    </button>
                </div>
            <?php \yii\widgets\ActiveForm::end(); ?>
            <!-- /SUBSCRIBE FORM -->
        </div>
    </div>
    <div class="down poster__down wow down-to-top">
        <button class="down__button" data-scroll-down></button>
        <div class="down__label">вниз</div>
    </div>
</section>
