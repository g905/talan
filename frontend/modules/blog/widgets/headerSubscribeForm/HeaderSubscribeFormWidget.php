<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.04.18
 * Time: 15:52
 */

namespace frontend\modules\blog\widgets\headerSubscribeForm;


use frontend\modules\form\models\SubscribeForm;
use yii\base\Widget;

class HeaderSubscribeFormWidget extends Widget
{
    public $title;
    public $text;
    public $btnText;
    public $agreement;
    public $imagePath;
    public $videoPath;
    public $onsubmit;

    /**
     * @var SubscribeForm
     */
    public $subscribeModel;

    /**
     * @return string
     */
    public function run()
    {
        if (!$this->subscribeModel) {
            $this->subscribeModel = new SubscribeForm([
                'attributeLabelsData' => [
                    'agree' => $this->agreement,
                ]
            ]);
        }

        return $this->render('headerSubscribeFormWidgetView', [
            'title' => $this->title,
            'text' => $this->text,
            'btnText' => $this->btnText,
            'imagePath' => $this->imagePath,
            'videoPath' => $this->videoPath,
            'model' => $this->subscribeModel,
            'onsubmit' => $this->onsubmit,
        ]);
    }
}
