<?php

namespace frontend\modules\blog\controllers;

use yii\web\NotFoundHttpException;
use common\models\VisitCount;
use common\models\BlogListPage;
use frontend\components\MetaRegistrar;
use frontend\modules\blog\models\BlogSearch;
use frontend\components\FrontendController;

/**
 * Class BlogController
 *
 * @author maxim
 * @package frontend\modules\blog\controllers
 */
class BlogController extends FrontendController
{
    public function actionIndex()
    {
        $pageModel = (new BlogListPage())->get();
        $searchModel = new BlogSearch();
        $searchModel->load(request()->get(), '');
        MetaRegistrar::register($pageModel, $pageModel->label);

        return $this->render('index', compact('pageModel', 'searchModel'));
    }

    public function actionView($alias)
    {
        $pageModel = (new BlogListPage())->get();
        $model = (new BlogSearch())->getArticleByAlias($alias);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        $this->attachLastModifiedHeader($model->updated_at);

        VisitCount::addVisit($model, $model->id);
        MetaRegistrar::register($model, $model->label);

        return $this->render('view', compact('model', 'pageModel'));
    }
}
