<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.04.18
 * Time: 16:01
 */

namespace frontend\modules\blog\components;


use common\models\BlogListPage;
use frontend\modules\apartment\components\ApartmentsListView;
use frontend\modules\blog\helpers\BlogDataProviderHelper;
use frontend\modules\blog\models\DummyModel;

class ListView extends \yii\widgets\ListView
{
    /**
     * @param $itemTypeId integer
     * @return string
     */
    public function getItemViewName($itemTypeId)
    {
        switch ($itemTypeId) {
            case BlogListPage::LAYOUT_FULL_ROW:
                return '@frontend/modules/blog/views/blog/_itemDiv3.php';
            case BlogListPage::LAYOUT_RIGHT:
                return '@frontend/modules/blog/views/blog/_itemRight.php';
            case BlogListPage::LAYOUT_LEFT:
                return '@frontend/modules/blog/views/blog/_itemLeft.php';
            default:
                return '@frontend/modules/blog/views/blog/_itemDiv3.php';
        }
    }

    /**
     * Renders all data models.
     * @return string the rendering result
     */
    public function renderItems()
    {
        $models = $this->dataProvider->getModels();
        if (empty($models)) {
            $list = new ApartmentsListView();
            $list->getEmptyBlock();
        }
        $keys = $this->dataProvider->getKeys();

        /*  */
        $modelsCount = count($models);
        $modelsToAdd = 0;
        $helper = BlogDataProviderHelper::getStaticModel();
        $friendlyItemsData = $helper->getFriendlyItemsData();
        $itemTypeId = $helper->getItemTypeIdByIndex($modelsCount - 1);
        if ($itemTypeId == BlogListPage::LAYOUT_FULL_ROW) {
            $friendlyItemsData = array_slice($friendlyItemsData, ($modelsCount - 0));
            $itemsCount = count($friendlyItemsData);
            for ($n = 0; $n < $itemsCount; $n++) {
                if (isset($friendlyItemsData[$n]) && $friendlyItemsData[$n] == $itemTypeId && ($modelsToAdd < 2 && $itemTypeId == $friendlyItemsData[$n])) {
                    $modelsToAdd += 1;
                } else {
                    break;
                }
            }
        }
        /*for ($i = 0; $i < $modelsToAdd; $i++) {
            $models[] = new DummyModel();
            $keys[] = 'b' . $i;
        }*/
        /*  */

        $rows = [];
        foreach (array_values($models) as $index => $model) {
            $key = $keys[$index];
            if (($before = $this->renderBeforeItem($model, $key, $index)) !== null) {
                $rows[] = $before;
            }

            $rows[] = $this->renderItem($model, $key, $index);

            if (($after = $this->renderAfterItem($model, $key, $index)) !== null) {
                $rows[] = $after;
            }
        }

        return implode($this->separator, $rows);
    }
}
