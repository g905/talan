<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.04.18
 * Time: 16:23
 */

namespace frontend\modules\blog\helpers;

use common\models\BlogListPage;
use common\models\BlogListViewTemplate;
use Yii;

class BlogDataProviderHelper
{
    /**
     * @var $this
     */
    protected static $_model;
    /**
     * @var int
     */
    protected $_itemsPerPage;
    /**
     * @var []
     */
    protected $_listTemplateData;
    /**
     * @var []
     */
    protected $_listFriendlyTemplateData;


    /**
     * @return int
     */
    public function getItemsPerPageCount()
    {
        if (!isset($this->_itemsPerPage)) {
            $itemsCount = 0;
            $items = $this->getListItemsData();
            foreach ($items as $item) {
                $add = 0;
                switch ($item) {
                    case BlogListPage::LAYOUT_LEFT:
                    case BlogListPage::LAYOUT_RIGHT:
                        $add = 1;
                        break;
                    case BlogListPage::LAYOUT_FULL_ROW:
                        $add = 3;
                        break;
                    default:
                        $add = 0;
                }
                $itemsCount += $add;
                $this->_itemsPerPage = $itemsCount;
            }
        }

        return $this->_itemsPerPage;
    }

    /**
     * @param $index
     * @return int|mixed
     */
    public function getItemTypeIdByIndex($index)
    {
        $items = $this->getFriendlyItemsData();

        return $items[$index] ?? BlogListPage::LAYOUT_FULL_ROW;
    }

    /**
     * @return $this
     */
    public static function getStaticModel()
    {
        if (!isset(static::$_model)) {
            static::$_model = (new BlogDataProviderHelper());
        }

        return static::$_model;
    }

    /**
     * @return array
     */
    public function getListItemsData()
    {
        if (!isset($this->_listTemplateData)) {
            $this->_listTemplateData = BlogListViewTemplate::find()
                ->select(['item_type_id'])
                ->orderBy(['id' => SORT_ASC])
                ->column();
        }

        return $this->_listTemplateData;
    }

    /**
     * @return array
     */
    public function getFriendlyItemsData()
    {
        if (!isset($this->_listFriendlyTemplateData)) {
            $dbItems = $this->getListItemsData();
            $result = [];
            foreach ($dbItems as $dbItem) {
                switch ($dbItem) {
                    case BlogListPage::LAYOUT_LEFT:
                    case BlogListPage::LAYOUT_RIGHT:
                        $offset = 1;
                        break;
                    case BlogListPage::LAYOUT_FULL_ROW:
                        $offset = 3;
                        break;
                    default:
                        $offset = 1;
                }
                if ($offset == 1) {
                    $result[] = $dbItem;
                } else {
                    for ($i = 0; $i < $offset; $i++) {
                        $result[] = $dbItem;
                    }
                }
            }
            $this->_listFriendlyTemplateData = $result;
        }

        return $this->_listFriendlyTemplateData;
    }
}