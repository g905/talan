<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.04.18
 * Time: 16:06
 *
 * @var $model \common\models\Article
 */

use metalguardian\fileProcessor\helpers\FPM;
?>
<div class="card articles__card articles__card_left wow fadeInUp">
    <div class="card__img" data-bg-src="<?= FPM::originalSrc($model->miniImageAttr->file_id ?? null) ?? '/static/img/apartments/apartaments-hover.png' ?>" data-bg-pos="center" data-bg-size="cover">
        <a href="<?= \common\helpers\SiteUrlHelper::getBlogViewUrl(['alias' =>$model->alias]) ?>"></a>
    </div>
    <div class="card__content">
        <h5 class="h5 card__h5">
            <a href="<?= \common\helpers\SiteUrlHelper::getBlogViewUrl(['alias' =>$model->alias]) ?>"><?= $model->label ?></a>
        </h5>
        <span class="card__date"><?= $model->getDisplayableListDate() ?></span>
        <span class="divider card__divider"></span>
        <ul class="card__tags">
            <?php foreach($model->blogThemes as $theme) : /* @var $theme \common\models\BlogTheme */ ?>
            <li>
                <a href="<?= \common\helpers\SiteUrlHelper::getBlogAllUrl(['theme' => $theme->alias]) ?>"><?= $theme->label ?></a>
            </li>
            <?php endforeach; ?>
        </ul>
        <div class="card__text">
            <p> <?= $model->short_description ?> </p>
        </div>
    </div>
</div>
