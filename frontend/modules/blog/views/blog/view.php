<?php

use yii\widgets\Breadcrumbs;
use metalguardian\fileProcessor\helpers\FPM;
use common\models\City;
use common\helpers\SiteUrlHelper;
use frontend\helpers\FrontendHelper;
use frontend\helpers\SocialShareHelper;
use frontend\widgets\BuilderContentWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;
use frontend\modules\blog\widgets\relatedArticles\RelatedArticles;

/**
 * @author maxim
 * @var $model \common\models\Article
 * @var $pageModel \common\models\BlogListPage
 */
$this->params['breadcrumbs'][] = [
    'label' => $pageModel->label,
    'url' => SiteUrlHelper::getBlogAllUrl(),
];
$this->params['breadcrumbs'][] = $model->label;
?>

<div class="header__row header__row_article">
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'homeLink' => [
                'label' => Yii::t('front/header', '_Breadcrumbs home label'),
                'url' => Yii::$app->homeUrl,
                'class' => 'bread-crumbs__link',
            ],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'options' => [
                'class' => 'bread-crumbs bread-crumbs_grey header__bread-crumbs',
            ]
        ]) ?>
    </div>
</div>

<section class="share share_vertical wow fadeIn">
    <div class="share__text"> <?= Yii::t('front/blog', 'share_text') ?> </div>
    <div class="social share__social">
        <a href="<?= SocialShareHelper::getOdnoklasnikiShareUrl() ?>" class="social__item socicon-odnoklassniki" target="_blank"></a>
        <a href="<?= SocialShareHelper::getVkShareUrl(FPM::src($model->coverImageAttr->file_id ?? null, 'article', 'view'), $model->label) ?>" class="social__item socicon-vkontakte" target="_blank"></a>
        <a href="<?= SocialShareHelper::getFacebookShareUrl() ?>" class="social__item socicon-facebook" target="_blank"></a>
    </div>
</section>
<!-- /SHARE -->
<!-- ARTICLE -->
<section class="article">
    <div class="wrap wrap_small wrap_mobile_full">
        <h1 class="article__title wow fadeInUp"> <?= $model->label ?> </h1>
        <div class="article__date wow fadeInUp" data-wow-delay="0.3s"> <?= $model->getDisplayableViewDate() ?> </div>
        <br>
        <ul class="tags article__tegs wow fadeInUp" data-wow-delay="0.6s">
            <?php foreach($model->blogThemes as $theme) : /* @var $theme \common\models\BlogTheme */ ?>
                <li class="item tags__item">
                    <a class="item__link" href="<?= SiteUrlHelper::getBlogAllUrl(['theme' => $theme->alias]) ?>"><?= $theme->label ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="views article__views">
            <div class="views__wrap"> <?= $model->getVisitsCount() ?> </div>
        </div>
        <div class="article__content wow fadeInUp" data-wow-delay="0.5s">
            <?php if (isset($model->coverImageAttr->file_id)) : ?>
            <p>
                <img src="<?= FPM::originalSrc($model->coverImageAttr->file_id ?? null) ?>" alt="" style="margin:20px 0">
            </p>
            <?php endif; ?>
        </div>
        <?= BuilderContentWidget::widget(['content' => $model->content]) ?>
    </div>
</section>
<!-- /ARTICLE -->
<!-- SHARE -->
<div class="wrap wrap_small wrap_mobile_full">
    <section class="share share_horizontal wow fadeIn">
        <div class="share__line"></div>
        <div class="social share__social">
            <a href="<?= SocialShareHelper::getOdnoklasnikiShareUrl() ?>" class="social__item socicon-odnoklassniki" target="_blank"></a>
            <a href="<?= SocialShareHelper::getVkShareUrl(FPM::src($model->coverImageAttr->file_id ?? null, 'article', 'view'), $model->label) ?>" class="social__item socicon-vkontakte" target="_blank"></a>
            <a href="<?= SocialShareHelper::getFacebookShareUrl() ?>" class="social__item socicon-facebook" target="_blank"></a>
        </div>
        <div class="share__text"> <?= Yii::t('front/blog', 'share_text') ?> </div>
    </section>
</div>

<?= RelatedArticles::widget(compact('model')) ?>

<!-- /SHARE -->
<?= PageFormBottomBlockWidget::widget([
    'formTitle' => $model->form_title,
    'formDescription' => $model->form_description,
    'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
    'manager' => $model->manager,
    'blockClass' => 'questions_article catalogue_question commercial-question',
    'agreementText' => strip_tags($model->form_agreement, '<a>'),
    'onsubmitJS' => $model->form_onsubmit,
]) ?>

<?= PageFooterWidget::widget(['currentCity' => City::getUserCity()]) ?>
