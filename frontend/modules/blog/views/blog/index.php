<?php

use frontend\modules\blog\components\ListView;
use frontend\modules\blog\helpers\BlogDataProviderHelper;
use common\models\City;
use frontend\modules\blog\widgets\yearsFilter\YearsFilter;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\blog\widgets\footerSubscribeForm\FooterSubscribeFormWidget;
use frontend\modules\blog\widgets\themesFilter\ThemesFilter;

$this->params['breadcrumbs'][] = $pageModel->label;
$this->params['headerArticle'] = ThemesFilter::widget([]);
$dataProvider = $searchModel->getDataProvider();
?>

<section class="articles">
    <div class="wrap wrap_mobile_full">
        <?= YearsFilter::widget() ?>

        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'options' => [
                'class' => 'articles__list',
            ],
            'itemView' => function($model, $key, $index, $widget) {
                /* @var $widget ListView */
                $view = '@frontend/modules/blog/views/blog/_itemDummy.php';
                if (!($model instanceof \frontend\modules\blog\models\DummyModel)) {
                    $helper = BlogDataProviderHelper::getStaticModel();
                    $itemTypeId = $helper->getItemTypeIdByIndex($index);
                    $view = $widget->getItemViewName($itemTypeId);
                }

                return $widget->render($view, [
                    'model' => $model,
                    'index' => $index,
                    'widget' => $widget
                ]);
            },
            'layout' => "{items}",
            'itemOptions' => [
                'tag' => false,
            ]
        ]) ?>
        <div class="apartaments__bottom wow fadeIn">
            <div class="apartaments__cell">
                <div class="apartaments__line"></div>
            </div>
            <div class="apartaments__cell">
                <?= \yii\widgets\LinkPager::widget([
                    'pagination' => $dataProvider->getPagination(),
                    'options' => [
                        'class' => 'pagination apartaments__pagination',
                    ]
                ]) ?>
            </div>
        </div>
    </div>
</section>
<!-- /BLOG ARTICLES -->

<?= FooterSubscribeFormWidget::widget([
    'titleBlack' => $pageModel->bottomFormBlackTitle,
    'titleWhite' => $pageModel->bottomFormWhiteTitle,
    'agreement' => obtain('agreementCheckboxText', app()->params, ''), // strip_tags($pageModel->bottomFormAgreement, '<a>'),
    'btnText' => $pageModel->bottomFormButtonText,
    'onsubmit' => $pageModel->bottomFormOnSubmit
]) ?>


<?= PageFooterWidget::widget(['currentCity' => City::getUserCity()])?>
