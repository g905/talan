<?php

namespace frontend\modules\general;

use yii\helpers\Url;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\general\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
