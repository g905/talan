<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\general\widgets;

use yii\base\Widget;

class CommonCityScriptsWidget extends Widget
{
    public $currentCity;

    public function run()
    {
        if (!isset($this->currentCity)) {
            return null;
        }

       $commonScripts = $this->currentCity->common_script;

        if (!isset($commonScripts)){
            return null;
        }

        echo $commonScripts;

    }

}
