<?php

namespace frontend\modules\general\widgets;

use yii\base\Widget;

/**
 * Class CommonComplexScriptsWidget
 *
 * @author art
 * @package frontend\modules\general\widgets
 */
class CommonComplexScriptsWidget extends Widget
{
    public $currentComplex;

    public function run()
    {
        if (!isset($this->currentComplex)) {
            return null;
        }

        $commonScripts = $this->currentComplex->common_script;

        if (!isset($commonScripts)) {
            return null;
        }

        echo $commonScripts;
    }
}
