<?php
namespace frontend\modules\api;

class Module extends \yii\base\Module
{
	public $controllerNamespace = 'frontend\modules\api\v1\controllers';

	public function init()
	{
		parent::init();

		// custom initialization code goes here
	}
}
