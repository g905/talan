<?php
namespace frontend\modules\api\v1\widgets;

class ApartmentsListView extends \frontend\modules\apartment\components\ApartmentsListView {
	public $agency_token;

	public function getEmptyBlock()
	{
		if (empty($this->model)) {
			return '<div class="apartaments__recommended-desc"> К сожалению, по вашему запросу квартиры не найдены.</div>';
		}
		$render = '<div class="apartaments__recommended-desc"> К сожалению, по вашему запросу квартиры не найдены. Попробуйте изменить параметры. </div>';

		return $render;
	}

}