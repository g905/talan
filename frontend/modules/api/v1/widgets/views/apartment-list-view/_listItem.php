<?php

use frontend\helpers\ImgHelper;
use frontend\helpers\FrontendHelper;
use common\models\Apartment;
use common\components\CommonDataModel;

$baseApiUrl = '/api/excursion/step3?token=' . Yii::$app->params['api_agency_token']
	. '&id=' . $model->id . '&building_num=' . Yii::$app->params['api_building_num'];
?>

<a href="<?= $baseApiUrl ?>" class="card apartaments__card wow fadeInUp">
    <div class="card__img">
        <div class="card__bg" data-bg-pos="center" data-bg-size="auto 90%" style="background: url('<?= ImgHelper::getImageSrc($model, 'complex', 'apartmentList', 'photo', Apartment::NO_IMAGE_PATH) ?>') center center / auto 90% no-repeat;"></div>
    </div>
    <div class="card__content">
        <h5 class="h5 card__h5"><?= /*$model->apartmentComplex->label . ', ' . */$model->label ?></h5>
        <div class="divider card__divider"></div>
        <div class="card__text">
            <?php if ($model->canShowPrice()) : ?>
                <p><?= t('_from', 'apartment') . ' ' . $model->priceFormatted() ?></p>
                <?php if ($model->isResidential()) : ?>
                    <p><?= t('_installment', 'apartment') . ' ' . $model->installmentPriceFormatted() ?></p>
                <?php endif; ?>
            <?php endif ?>
            <?php if (!FrontendHelper::isNullOrEmpty($model->total_area)) : ?>
                <p><?= $model->total_area . ' ' . CommonDataModel::getAreaUnit() ?></p>
				<?php
				if(!empty($model->price_from) && Yii::$app->params['api_show_price']) {
					echo '<div class="choose-aphouse__flats-price">'
						. 'от <span class="prices-box">' . obtain('price_from', $model, 0) . '</span> руб</div>';
				}
				?>
            <?php endif ?>
        </div>
    </div>
</a>
