<?php

/**
 * @var $this yii\web\View
 * @var $formID string Form identifier.
 * @var $formAction string Current form action.
 * @var $selected array Form data GET.
 * @var $complexList array List of options for complex filter.
 * @var $roomList array List of options for room filter.
 * @var $floorList array List of options for floor filter.
 * @var $forList array List of options for for filter.
 * @var $minBoundSquare int Minimum bound for square property.
 * @var $maxBoundSquare int Maximum bound for square property.
 * @var $minBoundPrice int Minimum bound for price property.
 * @var $maxBoundPrice int Maximum bound for price property.
 * @var $isResidential bool Whether complex is residential.
 * @var $isCommercial bool Whether complex is commercial.
 * @var $showPrices bool Whether to show prices range input.
 */
?>

<form class="filters wow fadeInLeft" id="<?= $formID ?>" action="<?= $formAction ?>" method="get" data-pjax="1" style="margin-top: 120px">
    <?php if ($isCommercial) : ?>
    	<?php if(count($complexList) > 1): ?>
        <div class="filters__section complecs_filter complex">
<!--            <div class="filters__heading">ЖК:</div>-->
            <div class="filters__inner">
            	
                <?php foreach ($complexList as $id => $label) : ?>
                    <?php $checked = in_array($id, obtain('complex', $selected, [])) ? 'checked' : '' ?>
                    <label class="checkbox form__checkbox">
                        <input class="checkbox__checkbox" name="complex[]" value="<?= $id ?>" <?= $checked ?> type="checkbox">
                        <span class="checkbox__mask"></span>
                        <span class="checkbox__label"><?= $label ?></span>
                    </label>
                <?php endforeach ?>
            
            </div>
        </div>
        <?php endif; ?>
    <?php endif ?>

    <?php if (count($queueList) > 0) : ?>
        <div class="filters__section queue">
            <div class="filters__heading">Очередь:</div>
            <div class="filters__inner">
                <?php foreach ($queueList as $queue) : ?>
                    <?php $checked = obtain('queue', $selected, 0) == $queue ? 'checked' : '' ?>
                    <label class="radio filters__radio">
                        <input type="radio" name="queue" class="radio__input" <?= $checked ?> value="<?= $queue ?>">
                        <span class="radio__mask"><?php
					    	switch ($queue) {
					    		case 0:
					    			//echo "I";
					    			break;
					    		case 1:
					    			echo "I";
					    			break;
					    		case 2:
					    			echo "II";
					    			break;
					    		case 3:
					    			echo "III";
					    			break;
					    		case 4:
					    			echo "IV";
					    			break;
					    	}
                         ?></span>
                    </label>
                <?php endforeach ?>
            </div>
        </div>
    <?php endif ?>

    <?php if ($roomList) : ?>
        <div class="filters__section rooms">
            <div class="filters__heading">Комнаты:</div>
            <div class="filters__inner">
                <?php foreach ($roomList as $room) : ?>
                    <?php $checked = obtain('rooms', $selected, 0) == $room ? 'checked' : '' ?>
                    <label class="radio filters__radio">
                        <input type="radio" name="rooms" class="radio__input" <?= $checked ?> value="<?= $room ?>">
                        <span class="radio__mask"><?= $room ?></span>
                    </label>
                <?php endforeach ?>
            </div>
        </div>
    <?php endif ?>

    <div class="filters__section square">
        <div class="filters__heading">Площадь:</div>
        <div class="filters__inner">
            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-range-min="<?= $minBoundSquare ?>" data-range-max="<?= $maxBoundSquare ?>" data-nouislider-type='square'>
                <div class="nouislider__slider"></div>
                <div class="ranges nouislider__ranges">
                    <span class="ranges__label">от</span>
                    <input class="ranges__input" value="<?= obtain('square-min', $selected, $minBoundSquare) ?>" type="number" name="square-min" data-no-ui-input-min>
                    <span class="ranges__label">до</span>
                    <input class="ranges__input" value="<?= obtain('square-max', $selected, $maxBoundSquare) ?>" type="number" name="square-max" data-no-ui-input-max>
                    <span class="ranges__label">м²</span>
                </div>
            </div>
        </div>
    </div>


    <?php if ($showPrices) : ?>
        <div class="filters__section price">
            <div class="filters__heading">Цена:</div>
            <div class="filters__inner">
                <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-range-min="<?= $minBoundPrice ?>" data-range-max="<?= $maxBoundPrice ?>" data-nouislider-type='price'>
                    <div class="nouislider__slider"></div>
                    <div class="ranges nouislider__ranges">
                        <span class="ranges__label">от</span>
                        <input class="ranges__input ranges__input_no_padding" value="<?= obtain('price-min', $selected, $minBoundPrice) ?>" type="text" name="price-min" data-no-ui-input-min>
                        <span class="ranges__label">до</span>
                        <input class="ranges__input ranges__input_no_padding" value="<?= obtain('price-max', $selected, $maxBoundPrice) ?>" type="text" name="price-max" data-no-ui-input-max>
                        <span class="ranges__label">руб</span>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>

    <?php if ($isResidential) : ?>
    <div class="filters__section floor">
        <div class="filters__heading">Этаж:</div>
        <div class="filters__inner">
            <div class="nouislider filters__nouislider" data-no-ui-slider='{"connect": true, "step": 1}' data-range-min="<?= $minBoundFloor ?>" data-range-max="<?= $maxBoundFloor ?>" data-nouislider-type='floor'>
                <div class="nouislider__slider"></div>
                <div class="ranges nouislider__ranges">
                    <span class="ranges__label">от</span>
                    <input class="ranges__input ranges__input_no_padding" value="<?= obtain('floor-min', $selected, $minBoundFloor) ?>" type="text" name="floor-min" data-no-ui-input-min>
                    <span class="ranges__label">до</span>
                    <input class="ranges__input ranges__input_no_padding" value="<?= obtain('floor-max', $selected, $maxBoundFloor) ?>" type="text" name="floor-max" data-no-ui-input-max>
                    <span class="ranges__label">этажа</span>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>

    <?php if (false) : ?>
        <div class="filters__section floor">
            <div class="filters__heading filters__heading_side_left">Этаж:</div>
            <div class="filters__inner filters__inner_side_right">
                <div class="custom-select filters__custom-select" data-custom-select>
                    <select name="floor">
                        <option value="-1">Любой</option>
                        <?php foreach ($floorList as $floor) : ?>
                            <?php $checked = obtain('floor', $selected) == $floor ? 'selected' : '' ?>
                            <option <?= $checked ?> value="<?= $floor ?>"><?= $floor ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
        </div>
    <?php endif ?>

    <?php if (false/*$isResidential*/) : ?>
        <div class="filters__section for">
            <div class="filters__heading">Для кого:</div>
            <div class="filters__inner">
                <div class="filters__radio-column">
                    <?php foreach ($forList as $value => $for) : ?>
                        <?php $checked = obtain('for', $selected) == $value ? 'checked' : '' ?>
                        <label class="radio filters__radio">
                            <input type="radio" name="for" class="radio__input" <?= $checked ?> value="<?= $value ?>">
                            <span class="radio__mask">
                                <svg class="radio__svg">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= obtain('icon', $for, '') ?>"></use>
                                </svg>
                            </span>
                            <span class="radio__label"><?= obtain('label', $for, '') ?></span>
                        </label>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    <?php endif ?>
</form>
