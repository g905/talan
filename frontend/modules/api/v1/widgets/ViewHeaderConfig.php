<?php
namespace frontend\modules\api\v1\widgets;

use yii\base\Widget;

class ViewHeaderConfig extends Widget {

	public $step = 11;
	public $complex = null;

	public function run() {
		if (!isset($this->complex)) {
			return null;
		}

		return $this->render('view-header-config/select-config', [
			'step' => $this->step,
			'complex' => $this->complex,
		]);
	}
}