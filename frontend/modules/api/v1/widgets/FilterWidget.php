<?php
namespace frontend\modules\api\v1\widgets;

class FilterWidget extends \frontend\widgets\FilterWidget {

	private function buildFormAction() {
		return $this->formBaseUrl;
	}

	public function run(): string
	{
		$this->registerClientScripts();
		$selected = request()->get();

		$this->city = $this->complex->city;
		$complexList = $this->makeComplexOptions();
		$complexIDs = array_keys($complexList);
		$roomList = $this->makeRoomsOptions($complexIDs);
		$queueList = $this->makeQueueOptions($complexIDs);
		$floorList = $this->makeFloorOptions($complexIDs);
		$forList = $this->forWhomOptions();
		$minBoundSquare = $this->makeMinSquareBound($complexIDs);
		$maxBoundSquare = $this->makeMaxSquareBound($complexIDs);
		$minBoundPrice = $this->makeMinPriceBound($complexIDs);
		$maxBoundPrice = $this->makeMaxPriceBound($complexIDs);
		$minBoundFloor = $this->makeMinFloorBound($complexIDs);
		$maxBoundFloor = $this->makeMaxFloorBound($complexIDs);
		//dd($queueList);
		return $this->render('filter-widget/_filters', [
			'formID' => $this->formID,
			'formAction' => $this->buildFormAction(),
			'selected' => $selected,
			'complexList' => $complexList,
			'roomList' => $roomList,
			'queueList' => $queueList,
			'floorList' => $floorList,
			'forList' => $forList,
			'isCommercial' => $this->complex->isCommercial(),
			'isResidential' => $this->complex->isResidential(),
			'minBoundSquare' => $minBoundSquare,
			'maxBoundSquare' => $maxBoundSquare,
			'minBoundPrice' => $minBoundPrice,
			'maxBoundPrice' => $maxBoundPrice,
			'minBoundFloor' => $minBoundFloor,
			'maxBoundFloor' => $maxBoundFloor,
			'showPrices' => (bool)$this->city->show_prices,
		]);
	}
}