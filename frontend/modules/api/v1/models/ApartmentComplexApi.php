<?php
namespace frontend\modules\api\v1\models;

use \common\models\ApartmentComplex;

class ApartmentComplexApi extends ApartmentComplex {

	public static function findOnePublishedById($id)
	{
		return static::checkFail(
			static::find()->where(['id' => $id, 'published' => self::IS_PUBLISHED])->one()
		);
	}
}