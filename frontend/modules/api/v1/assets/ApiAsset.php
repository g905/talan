<?php

namespace frontend\modules\api\v1\assets;

use yii\web\YiiAsset;
use yii\web\AssetBundle;

class ApiAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'/static/css/index.css',
		//'css/frontend.css',
	];

	public $js = [
		'js/filter.js',
		'static/js/index.js',
/*

		'js/frontend.js',
		'static/js/map.js',

		'js/electron.js'
/**/
	];

	public $depends = [
		YiiAsset::class,
	];
}
