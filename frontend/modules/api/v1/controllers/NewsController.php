<?php
/**
 * Created by PhpStorm.
 * User: p-clerick
 * Date: 16.05.19
 * Time: 9:32
 */

namespace frontend\modules\api\v1\controllers;


use common\models\Article;
use common\models\FpmFile;
use metalguardian\fileProcessor\helpers\FPM;
use yii\rest\Controller;

class NewsController extends Controller
{
    public function actionIndex($page = 1, $limit = 20)
    {
        \Yii::$app->response->format =  \yii\web\Response::FORMAT_JSON;

        $offset = ($page - 1) * $limit;
        $art_query = Article::find();
        $count = $art_query->count();

        $articles =  $art_query->offset($offset)->limit($limit)->all();
        if ($articles) {
            $res = [];
            foreach ($articles as $key => $item) {
                $res[$key] = [
                    'id' => $item->id,
                    'label' => $item->label,
                    'alias' => $item->alias,
                    'article_content' => (isset($item->content[0]) )? $item->content[0]->text : '',
                    'publish_date' => $item->date_published,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->updated_at,
                    'published' => $item->published,
                    'position' => $item->position,
                    'cities' => $item->getCities()->select(['label', 'alias', 'id', 'sxgeo_city_id'])->all(),
                    'wet_content' => '',
                    'coverImage' => FPM::originalSrc($item->coverImageAttr->file_id ?? null),
                    'miniImage' => FPM::originalSrc($item->miniImageAttr->file_id ?? null)

                ];
            }
        }
        else $res = ['error' => 'Запрос вернул пустой результат'];

        return array_merge(['page' => $page, 'limit' => $limit, 'total' => $count], $res);

    }
}