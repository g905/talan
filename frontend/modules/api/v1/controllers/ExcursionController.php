<?php
namespace frontend\modules\api\v1\controllers;

use backend\modules\api\models\ApiAgencyStatVisits;
use backend\modules\api\models\ApiAgencyToApartmentBuilding;

use backend\modules\api\models\ApiCustomer;
use common\helpers\MailerHelper;
use common\models\Apartment;
use common\models\ApartmentBuilding;
use frontend\components\MetaRegistrar;
use frontend\modules\form\widgets\CustomPopupRequestWidget;
use frontend\modules\api\v1\models\ApartmentComplexApi;
use rmrevin\yii\postman\ViewLetter;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ExcursionController extends BaseController {

	public function afterAction($action, $result) {
		if(in_array($action->id, ['step11', 'step12', 'step2', 'step3']) && $this->agency) {
			$aasv = new ApiAgencyStatVisits();
			$aasv->api_agency_id = $this->agency->id;
			$aasv->save();
		}

		$parentRes = parent::afterAction($action, $result);
		return $parentRes;
	}

	public function actionStep11($alias = null, $type = null) {

		if($this->agency) {
			Yii::$app->params['api_agency_token'] = $this->agency->token;
			$apartmentIds = ApiAgencyToApartmentBuilding::getComplexIdsBy($this->agency->id);

			if(is_array($apartmentIds) && count($apartmentIds)) {
				$arrKeys = array_keys($apartmentIds);
				$model = ApartmentComplexApi::findOnePublishedById($arrKeys[0]);

				// в том случае, если отсутсвует параметр buildingsForChess - не дадим просматривать конфигуратор
				// (т.к. в нем всё равно ничего нельзя будет выбирать
				$buildsForChess = $model->buildingsForChess;
				if(is_array($buildsForChess) && count($buildsForChess)) {
					$mapModel = $model->map;

					return $this->render('@frontend/modules/api/v1/views/excursion/step-1-1', [
						'model' => $model,
						'mapModel' => $mapModel,
						'agency' => $this->agency,
					]);
				}
				return $this->redirect('/api/excursion/step12?token=' . $this->agency->token . '&complex_id=' . $arrKeys[0]);
			}
		}
		throw new NotFoundHttpException();
	}

	public function actionStep12() {
		if($this->agency) {
			Yii::$app->params['api_agency_token'] = $this->agency->token;
			Yii::$app->params['api_building_num'] = 1;
			Yii::$app->params['api_show_price'] = $this->agency->show_price;
			$apartmentIds = ApiAgencyToApartmentBuilding::getComplexIdsBy($this->agency->id);

			if(is_array($apartmentIds) && count($apartmentIds)) {
				$arrKeys = array_keys($apartmentIds);
				$model = ApartmentComplexApi::findOnePublishedById($arrKeys[0]);

				$provider = $model->filter(request()->getQueryParams());

				// Url::remember(Url::current(), 'filter_params');
				return $this->render('@frontend/modules/api/v1/views/excursion/step-1-2', [
					'model' => $model,
					'provider' => $provider,
					'agency' => $this->agency,
				]);
			}
		}
		throw new NotFoundHttpException();
	}

	public function actionStep2($complex_id = null, $apartment_id = null, $building_id = null) {

		if($this->agency) {

			Yii::$app->params['api_agency_token'] = $this->agency->token;
			Yii::$app->params['api_building_num'] = $building_id;
			Yii::$app->params['api_show_price'] = $this->agency->show_price;
			$entrance = $building_id;
			$complex = ApartmentComplexApi::findOnePublishedById($complex_id);
			$building = ApartmentBuilding::findOneWithChessByIdOrFail($apartment_id);

			if (!isset($complex) || !isset($building)) {
				throw new NotFoundHttpException();
			}

			return $this->render('@frontend/modules/api/v1/views/excursion/step-2', [
				'complex' => $complex,
				'building' => $building,
				'entrance' => $entrance,
				'agency' => $this->agency,
			]);
		}
		throw new NotFoundHttpException();
	}

	public function actionStep3($id, $building_num = null) {
		
		if($this->agency) {
			Yii::$app->params['api_agency_token'] = $this->agency->token;
			$model = Apartment::findOnePublishedOrFail($id);
			$building = $model->apartmentBuilding;
			$complex = $model->apartmentComplex;

			return $this->render('@frontend/modules/api/v1/views/excursion/step-3', [
				'model' => $model,
				'complex' => $complex,
				'building' => $building,
				'building_num' => $building_num,
			]);
		}
		throw new NotFoundHttpException();
	}

	public function actionStep4() {
		if($this->agency && Yii::$app->request->isAjax) {
			$retArr = ['code' => 0];

			$customerModel = new ApiCustomer();
			$customerModel->scenario = 'create';
			$customerModel->load(Yii::$app->request->post());
			$customerModel->agency_id = $this->agency->id;

			if($customerModel->validate() && $customerModel->save()) {
				$retArr['code'] = 1;

				$this->sendExcursionEmail( $customerModel);
			} else {
				$tmpArr = array_shift($customerModel->errors);
				$retArr['error'] = array_shift($tmpArr);
			}

			Yii::$app->response->format = Response::FORMAT_JSON;
			return $retArr;
		}
		throw new NotFoundHttpException();
	}

	public function sendExcursionEmail($customerModel) {

		$apartmentObj = $customerModel->apartament;
		$flatLbl = '-';
		if(!empty($apartmentObj->label)) {
			$flatLbl = $apartmentObj->label;
		}
		$address = '-';
		if(!empty($apartmentObj->address)) {
			$address = $apartmentObj->address;
		}

		$complexObj = $apartmentObj->apartmentComplex;
		$complexName = '-';
		if(!empty($complexObj->label)) {
			$complexName = $complexObj->label;
		}

		$content = '<p>У Вас новая заявка</p>'
			. '<h3>Контактные данные: </h3>'
			. '<p>Имя: ' . $customerModel->name . '</p>'
			. '<p>Телефон: ' . $customerModel->phone . '</p>'
			. '<p>Email: ' . $customerModel->email . '</p>'
			. '<br/>'
			. '<h3>Данные экскурсии:' . '</h3>'
			. '<p>Комплекс: ' . $complexName . '</p>'
			. '<p>Апартаменты: ' . $flatLbl . '</p>'
		;

		$letter = (new ViewLetter())
			->setSubject('Заявка на квартиру')
			->setBody($content);

		$letter->addAddress($this->agency->email);

		if (!$letter->send(true)) {
			return false;
		}

		return true;
	}
}