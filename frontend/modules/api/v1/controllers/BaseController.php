<?php
namespace frontend\modules\api\v1\controllers;

use backend\modules\api\models\ApiAgency;
use Yii;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BaseController extends Controller {

	public $layout = '@frontend/modules/api/v1/views/layout/main.php';
	public $agency = null;

	public function beforeAction($action) {
		$pbaRes = parent::beforeAction($action);

		$token = Yii::$app->request->get('token');
		// agency exists?
		$this->agency = ApiAgency::find()
			->select([
				'id',
				'token',
				'email',
				'show_price',
				'callback_url',
				'active',
			])
			->where([
				'token' => $token,
			])->one();

		Yii::$app->response->format = Response::FORMAT_HTML;

		if(empty($this->agency) || !$this->agency->active) {
			throw new NotFoundHttpException();
		}

		return $pbaRes;
	}

}