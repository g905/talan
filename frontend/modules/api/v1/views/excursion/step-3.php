<?php
use frontend\modules\general\widgets\CommonComplexScriptsWidget;
use frontend\modules\apartment\widgets\apartment\ApartmentTopBlockWidget;
use common\models\City;
use common\models\Apartment;
use common\helpers\SiteUrlHelper;
use common\components\CommonDataModel;
use frontend\helpers\ImgHelper;
use frontend\helpers\FrontendHelper;
use \yii\widgets\ActiveForm;
use \backend\modules\api\models\ApiCustomer;

//if comes from outer internet (no referer), link to catalog
if(Yii::$app->request->referrer) {
	$this->params['back_url'] = Yii::$app->request->referrer;
} else {
	$this->params['back_url'] = $flatUrl =  Yii::$app->params['baseApiUrl']
		. '/api/excursion/step2?token=' . Yii::$app->params['api_agency_token']
		. '&complex_id=' . $complex->id . '&apartment_id=' . $building->id . '&building_id=' . $building_num;;
	;
}

$step4Url = Yii::$app->params['baseApiUrl'] . '/api/excursion/step4?token=' . Yii::$app->params['api_agency_token'];
$this->params['headerMenu'] = $model->apartmentComplex->apartmentComplexMenu;
$this->params['headerMenuType'] = $model;

$this->title = 'step-3';
$city = City::getUserCity();
?>

<?= CommonComplexScriptsWidget::widget(['currentComplex' => $model->apartmentComplex]) ?>
<div class="wrap wrap_mobile_full">
	<div class="apartment-head apartment-head_single">
		<h2 class="h2 apartment-head__h2  wow fadeInLeft">
			<?= $model->apartmentComplex->label ?>
		</h2>
		<a href="<?= Yii::$app->request->referrer; ?>" data-wow-delay="0.3s" class="apartment-head__back wow fadeInLeft">
			<?= t('_Back to choose apartment', 'apartment') ?>
		</a>
	</div>
</div>

<section class="gallery">
	<div class="wrap wrap_mobile_full">
		<?php if (!FrontendHelper::isNullOrEmpty($model->apartmentComplex->general_progress_percent)): ?>
			<div class="gallery__diagram gallery__diagram_tab wow fadeInRight">
				<div class="circle-progress" data-circle-progress="<?= $model->apartmentComplex->general_progress_percent ?>">
					<div class="circle-progress__outer">
						<div class="circle-progress__progress circle-progress__progress_10"></div>
						<div class="circle-progress__progress circle-progress__progress_20"></div>
						<div class="circle-progress__progress circle-progress__progress_30"></div>
						<div class="circle-progress__progress circle-progress__progress_40"></div>
						<div class="circle-progress__progress circle-progress__progress_50"></div>
						<div class="circle-progress__progress circle-progress__progress_60"></div>
						<div class="circle-progress__progress circle-progress__progress_70"></div>
						<div class="circle-progress__progress circle-progress__progress_80"></div>
						<div class="circle-progress__progress circle-progress__progress_90"></div>
						<div class="circle-progress__progress circle-progress__progress_100"></div>
						<div class="circle-progress__value"><?= $model->apartmentComplex->general_progress_percent ?></div>
						<div class="circle-progress__percent">%</div>
					</div>
					<div class="circle-progress__label">
						<?= $model->apartmentComplex->general_progress_title ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="gallery__content gallery__content_mob">
			<div class="gallery__diagram gallery__diagram_desk"></div>
			<h3 class="h3 gallery__h3 wow fadeInRight" data-wow-delay="0.3s">
				<?= $model->label ?>
			</h3>
			<div class="gallery__text wow fadeInRight" data-wow-delay="0.6s">
				<?php if (!FrontendHelper::isNullOrEmpty($model->subtype) && $model->isCommercial()) : ?>
					<p>
						<span><?= t('_Type', 'apartment') ?>:</span>
						<strong><?= $model->getSubtypeLabel() ?></strong>
					</p>
				<?php endif; ?>
				<?php if (!FrontendHelper::isNullOrEmpty($model->profile)) : ?>
					<p>
						<span><?= t('_Profile', 'apartment') ?>:</span>
						<strong><?= $model->profile ?></strong>
					</p>
				<?php endif; ?>
				<?php if (!FrontendHelper::isNullOrEmpty($model->income)) : ?>
					<p>
						<span><?= t('_Income', 'apartment') ?>:</span>
						<strong><?= $model->income ?></strong>
					</p>
				<?php endif; ?>
				<?php if (!FrontendHelper::isNullOrEmpty($model->recoupment)) : ?>
					<p>
						<span><?= t('_Recoupment', 'apartment') ?>:</span>
						<strong><?= $model->recoupment ?></strong>
					</p>
				<?php endif; ?>
				<?php if (!FrontendHelper::isNullOrEmpty($model->entrance)) : ?>
					<p>
						<span><?= t('_Entrance', 'apartment') ?>:</span>
						<strong><?= $model->entrance ?></strong>
					</p>
				<?php endif; ?>
				<?php if (!FrontendHelper::isNullOrEmpty($model->rooms_number)) : ?>
					<p>
						<span><?= t('_Rooms count', 'apartment') ?>:</span>
						<strong><?= t('{n, plural, =0{_no_rooms} =1{# _1_room} one{# _one_room} few{# _few_rooms} many{# _many_rooms} other{# _other_rooms}}', 'apartment', ['n' => intval($model->rooms_number)]) ?></strong>
					</p>
				<?php endif; ?>

				<?php if (!FrontendHelper::isNullOrEmpty($model->total_area)) : ?>
					<p>
						<span><?= t('_Total area', 'apartment') ?>:</span>
						<strong><?= $model->total_area . ' ' . CommonDataModel::getAreaUnit() ?></strong>
					</p>
				<?php endif; ?>

				<?php if (!FrontendHelper::isNullOrEmpty($model->floor)): ?>
					<p>
						<span><?= t('_Floor', 'apartment') ?>:</span>
						<strong><?= $model->floor ?></strong>
					</p>
				<?php endif; ?>

				<?php if (!FrontendHelper::isNullOrEmpty($model->delivery_date)) : ?>
					<p>
						<span><?= t('_Delivery date', 'apartment') ?>:</span>
						<strong><?= Yii::$app->formatter->asDate($model->delivery_date, 'dd-MM-YYYY') ?></strong>
					</p>
				<?php endif; ?>
				<?php if ($model->canShowPrice()) : ?>
					<p>
						<span><?= t('_Price from', 'apartment') ?>:</span>
						<strong><?= $model->priceFormatted() ?></strong>
					</p>
					<p>
						<span><?= t('_Price installment', 'apartment') ?>:</span>
						<strong><?= $model->installmentPriceFormatted() ?></strong>
					</p>
				<?php endif; ?>
			</div>
			<a href="#" data-href="<?= SiteUrlHelper::createFormExcursionPopupUrl(['apartmentId' => $model->id]) ?>" class="button button_green gallery__button ajax-link wow fadeInRight mdlExcursion" data-wow-delay="0.9s">
				<?= 'Оставить заявку'; ?>
				<span class="button__blip button__blip_hover"></span>
				<span class="button__blip button__blip_click"></span>
			</a>
		</div>

		<div class="gallery__wrap">
			<div class="gallery__slider wow fadeIn <?= count($model->photos) > 1 ? : 'clear_border' ?>" data-wow-delay="0.5s" data-wow-duration="2s" data-gallery-slider data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "dots": false, "arrows": false, "asNavFor": "[data-gallery-nav]"}'>
				<?php foreach ($model->photos as $photoEtF): ?>
					<div data-fancybox="gallery" data-bg-size="contain" data-bg-pos="center" href="<?= ImgHelper::getImageSrcFromEtF($photoEtF, 'apartment', 'gallery') ?>">
						<div class="gallery__photo" data-bg-src="<?= ImgHelper::getImageSrcFromEtF($photoEtF, 'apartment', 'gallery') ?>" data-bg-size="contain" data-bg-pos="center"></div>
					</div>
				<?php endforeach; ?>
				<?php if (count($model->photos) == 0): ?>
					<div class="gallery__photo" data-bg-src="<?= Apartment::NO_IMAGE_PATH ?>" data-bg-size="contain" data-bg-pos="center"></div>
				<?php endif; ?>
			</div>

			<?php if (count($model->photos) > 1) : ?>
				<div class="gallery__nav" data-gallery-nav data-slick='{"slidesToShow": 4, "slidesToScroll": 1, "dots": false, "asNavFor": "[data-gallery-slider]", "focusOnSelect": true, "infinite": false, "arrows": false, "responsive": [{"breakpoint": 1201, "settings": {"slidesToShow": 3}}, {"breakpoint": 480, "settings": {"slidesToShow": 2}}]}'>

					<?php foreach ($model->photos as $photoEtF): ?>
						<div class="gallery__item">
							<div class="gallery__img" data-bg-src="<?= ImgHelper::getImageSrcFromEtF($photoEtF, 'apartment', 'galleryThumb') ?>" data-bg-size="cover" data-bg-pos="center"></div>
						</div>
					<?php endforeach; ?>

					<?php if (count($model->photos) == 0): ?>
						<div class="gallery__item">
							<div class="gallery__img" data-bg-src="<?= Apartment::NO_IMAGE_PATH ?>" data-bg-size="cover" data-bg-pos="center"></div>
						</div>
					<?php endif; ?>

				</div>
				<button class="gallery__zoom">
					<svg>
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search" /> </svg>
				</button>
			<?php endif; ?>
		</div>

		<div class="gallery__content gallery__content_desk">
			<?php if (!FrontendHelper::isNullOrEmpty($model->apartmentComplex->general_progress_percent)): ?>
				<div class="gallery__diagram gallery__diagram_desk wow fadeInRight">
					<div class="circle-progress" data-circle-progress="<?= $model->apartmentComplex->general_progress_percent ?>">
						<div class="circle-progress__outer">
							<div class="circle-progress__progress circle-progress__progress_10"></div>
							<div class="circle-progress__progress circle-progress__progress_20"></div>
							<div class="circle-progress__progress circle-progress__progress_30"></div>
							<div class="circle-progress__progress circle-progress__progress_40"></div>
							<div class="circle-progress__progress circle-progress__progress_50"></div>
							<div class="circle-progress__progress circle-progress__progress_60"></div>
							<div class="circle-progress__progress circle-progress__progress_70"></div>
							<div class="circle-progress__progress circle-progress__progress_80"></div>
							<div class="circle-progress__progress circle-progress__progress_90"></div>
							<div class="circle-progress__progress circle-progress__progress_100"></div>
							<div class="circle-progress__value"><?= $model->apartmentComplex->general_progress_percent ?></div>
							<div class="circle-progress__percent">%</div>
						</div>
						<div class="circle-progress__label">
							<?= $model->apartmentComplex->general_progress_title ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<h3 class="h3 gallery__h3 wow fadeInRight" data-wow-delay="0.3s">
				<?= $model->label ?>
			</h3>
			<div class="gallery__text wow fadeInRight" data-wow-delay="0.6s">
				<?php if (!FrontendHelper::isNullOrEmpty($model->subtype) && $model->isCommercial()) : ?>
					<p>
						<span><?= t('_Type', 'apartment') ?>:</span>
						<strong><?= $model->getSubtypeLabel() ?></strong>
					</p>
				<?php endif; ?>
				<?php if (!FrontendHelper::isNullOrEmpty($model->profile)) : ?>
					<p>
						<span><?= t('_Profile', 'apartment') ?>:</span>
						<strong><?= $model->profile ?></strong>
					</p>
				<?php endif; ?>
				<?php if (!FrontendHelper::isNullOrEmpty($model->income)) : ?>
					<p>
						<span><?= t('_Income', 'apartment') ?>:</span>
						<strong><?= $model->income ?></strong>
					</p>
				<?php endif; ?>
				<?php if (!FrontendHelper::isNullOrEmpty($model->recoupment)) : ?>
					<p>
						<span><?= t('_Recoupment', 'apartment') ?>:</span>
						<strong><?= $model->recoupment ?></strong>
					</p>
				<?php endif; ?>
				<?php if (!FrontendHelper::isNullOrEmpty($model->entrance)) : ?>
					<p>
						<span><?= t('_Entrance', 'apartment') ?>:</span>
						<strong><?= $model->entrance ?></strong>
					</p>
				<?php endif; ?>
				<?php if (!FrontendHelper::isNullOrEmpty($model->rooms_number)): ?>
					<p>
						<span><?= t('_Rooms count', 'apartment') ?>:</span>
						<strong>
							<?= t('{n, plural, =0{_no_rooms} =1{# _1_room} one{# _one_room} few{# _few_rooms} many{# _many_rooms} other{# _other_rooms}}', 'apartment', ['n' => intval($model->rooms_number)]) ?>
						</strong>
					</p>
				<?php endif; ?>

				<?php if (!FrontendHelper::isNullOrEmpty($model->total_area)): ?>
					<p>
						<span><?= t('_Total area', 'apartment') ?>:</span>
						<strong><?= $model->total_area . ' ' . CommonDataModel::getAreaUnit() ?></strong>
					</p>
				<?php endif; ?>

				<?php if (!FrontendHelper::isNullOrEmpty($model->floor)): ?>
					<p>
						<span><?= t('_Floor', 'apartment') ?>:</span>
						<strong><?= $model->floor ?></strong>
					</p>
				<?php endif; ?>

				<?php if (!FrontendHelper::isNullOrEmpty($model->delivery_date)): ?>
					<p>
						<span><?= t('_Delivery date', 'apartment') ?>:</span>
						<strong><?= Yii::$app->formatter->asDate($model->delivery_date, 'dd.MM.YYYY') ?></strong>
					</p>
				<?php endif; ?>

				<?php if ($model->canShowPrice()): ?>
					<p>
						<span><?= t('_Price from', 'apartment') ?>:</span>
						<strong><?= $model->priceFormatted() ?></strong>
					</p>
					<p>
						<span><?= t('_Price installment', 'apartment') ?>:</span>
						<strong><?= $model->installmentPriceFormatted() ?></strong>
					</p>
				<?php endif; ?>
			</div>

			<a href="#" data-href="<?= SiteUrlHelper::createFormExcursionPopupUrl(['apartmentId' => $model->id]) ?>" class="ajax-link button button_green gallery__button wow fadeInRight mdlExcursion" data-wow-delay="0.9s">
				<?= 'Оставить заявку'; ?>
				<span class="button__blip button__blip_hover"></span>
				<span class="button__blip button__blip_click"></span>
			</a>
		</div>

	</div>

</section>

<div class="tlnModal tlnDisplNone" id="tlnSuccessModal">
	<div class="tlnMdlWnd">
	<button class="modal__close"></button>
		<div class="">
			<h5 class="h5 popup__h5 ">Ваша заявка сохранена</h5>
		</div>
	</div>
</div>

<div class="tlnModal tlnDisplNone" id="tlnExcursionModal">
	<div class="tlnMdlWnd">
		<button class="modal__close"></button>
		<div class="">
			<h5 class="h5 popup__h5 ">Оставить заявку</h5>
			<div class="">
				<?php
				$customerModel = new ApiCustomer();
				$customerModel->scenario = 'create';
				$customerModel->apartment_id = $model->id;

				$form1 = ActiveForm::begin([
					'id' => 'excursionForm',
					'action' => '/',
				]);

				echo $form1->field($customerModel, 'name', ['options' => ['class' => 'form__row']])
					->textInput([
						'placeholder' => 'Ваше имя',
						'class' => 'tlnFormInput',
					])->label(false);

				echo $form1->field($customerModel, 'phone', ['options' => ['class' => 'form__row']])
					->textInput([
						'placeholder' => 'Телефон',
						'class' => 'tlnFormInput',
                        'type' => 'tel'
					])->label(false);

				echo $form1->field($customerModel, 'email', ['options' => ['class' => 'form__row']])
					->textInput([
						'placeholder' => 'Email',
						'class' => 'tlnFormInput',
					])->label(false);

				?>
				<div class="form__row required field-apicustomer-confirmAgreement">
					<label class="checkbox form__checkbox">
						<input type="checkbox" class="checkbox__checkbox" name="ApiCustomer[confirmAgreement]" id="agreementInp"/>
						<span class="checkbox__mask"></span>
						<span class="checkbox__label">Я <a href="/files/agreement.docx" target="_blank">согласен</a> на обработку моих персональных данных.<br/>
			С <a href="/files/privacy-policy.docx" target="_blank">Политикой</a> в отношении обработки персональных данных ознакомлен и согласен.</span>
						<div class="help-block"></div>
					</label>
				</div>
				<?php echo $form1->field($customerModel, 'apartment_id')->hiddenInput()->label(false); ?>

				<div id="form1Error" class="tlnDisplNone"></div>

				<button type="submit" class="button button_green form__button" id="excursionSaveBtn">
					Отправить
					<span class="button__blip button__blip_hover"></span>
					<span class="button__blip button__blip_click"></span>
				</button>

				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>

<style>
	#tlnSuccessModal .modal__close {
		right: 0;
		top: 0;
		width: 30px;
		height: 30px;
	}
	#tlnSuccessModal .modal__close::before,
	#tlnSuccessModal .modal__close::after {
		width: 15px;
		top: 10px;
	}
	#tlnSuccessModal .modal__close::before {
		left: 10px;
	}
	#tlnSuccessModal .modal__close::after {
		right: 10px;
	}
	#form1Error {
		color: #f00;
		text-align: center;
	}
	.tlnModal .help-block {
		color: #f00;
		font-size: 0.8rem;
	}
	.tlnModal .checkbox__label {
		color: #343434;
	}
	.tlnModal .checkbox__mask {
		color: #fff;
		border-color: #343434;
	}
	.tlnModal .checkbox__checkbox:checked + .checkbox__mask {
		background-color: rgba(52,52,52,.75);
	}
	.tlnModal {
		position: fixed;
		background: rgba(0,0,0, 0.3);
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		z-index: 100;
		display: flex;
		justify-content: center;
		align-items: center;
	}
	.tlnMdlWnd {
		background: #fff;
		padding: 45px 20px 20px;
		border: 1px solid #999;
		position: relative;
		max-width: 600px;
	}
	@media (max-width: 650px) {
		.tlnMdlWnd {
			max-width: 95%;
		}
	}
	.tlnDisplNone {
		display: none !important;
	}
	.tlnMdlWnd .tlnFormInput {
		display: block;
		width: 100%;
		height: 40px;
		border-top: 0;
		border-right: 0;
		border-bottom: 1px solid #1e9b57;
		border-left: 0;
		outline: 0;
		font-size: 14px;
		line-height: 2;
		padding: 0 9px;
		color: #343434;
	}
	.tlnMdlWnd .form__button {
		margin: 40px auto 0;
		width: 150px;
	}
	.tlnMdlWnd .form__button[disabled] {
		color: #555;
		background: #aaa;
		border: 1px solid #555;
	}
	.tlnMdlWnd .form__button[disabled] .button__blip_hover {
		background: #aaa;
	}

	.tlnMdlWnd .h5 {
		text-align: center;
	}

	::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
		color: #6a6a6a !important;
		opacity: 1; /* Firefox */
	}

	:-ms-input-placeholder { /* Internet Explorer 10-11 */
		color: #6a6a6a !important;
	}

	::-ms-input-placeholder { /* Microsoft Edge */
		color: #6a6a6a !important;
	}
</style>
<?php
$jsCode = <<<rjs
	var tlnModalCont = $('#tlnExcursionModal')
	;
	$('.mdlExcursion').on('click', function(event) {
		tlnModalCont.removeClass('tlnDisplNone');
	});
	
	$('.tlnModal').on('click', function(event) {
		var eventTarget = $(event.target)
		;
		if(eventTarget.hasClass('tlnModal') || !event || !event.target || !eventTarget.length) {
			eventTarget.addClass('tlnDisplNone');
		}
	});
	
	$('.tlnMdlWnd .modal__close').on('click', function() {
		var parentTlnModal = $(this).closest('.tlnModal');
		if(parentTlnModal.length) {
			parentTlnModal.addClass('tlnDisplNone');
		}
	});
	
	var excursionBtn = $('#excursionSaveBtn')
	,	form1 = $('#excursionForm')
	;
	$('#apicustomer-phone').mask("+7(999) 999-99-99");
	
	function form1ShowError(text) {
		var lblBlock = $('#form1Error')
		;		
		if(!text) {
			lblBlock.text('');
			lblBlock.addClass('tlnDisplNone');
		} else {
			lblBlock.text(text);
			lblBlock.removeClass('tlnDisplNone');
		}
	}
	
	form1.on('beforeSubmit', function(event) {
		event.preventDefault();
		
		form1.yiiActiveForm('validate');
		if($('#excursionForm').yiiActiveForm('data').validated) {
			var	ajaxParams = form1.serialize()
			,	nameInp = $('#apicustomer-name')
			,	phoneInp = $('#apicustomer-phone')
			,	emailInp = $('#apicustomer-email')
			,	agreementInp = $('#agreementInp')
			;
			form1ShowError('');
			if(!excursionBtn.prop('disabled')) {
			
				excursionBtn.attr('disabled', 'disabled'); 
				$.ajax({
					'url': '$step4Url',
					'method': 'post',
					'data': ajaxParams,
				}).done(function(resp) {

					if(resp) {
						if(resp.code == 1) {
							nameInp.val('');
							phoneInp.val('');
							emailInp.val('');
							agreementInp.prop('checked', false);
							tlnModalCont.trigger('click');
							$('#tlnSuccessModal').removeClass('tlnDisplNone');
						}  else {
							if(resp.error) {
								form1ShowError(resp.error);
							} else {
								form1ShowError('Error occurred');
							}
						}
					} else {
						form1ShowError('Error occurred');
					}
				}).fail(function(r1, r2, r3) {
					form1ShowError(r3);
				}).always(function() {
					excursionBtn.prop('disabled', false);
				});
			}
		}
		
		event.preventDefault();
		return false;
	});
	
rjs;

$this->registerJs($jsCode);
//echo ApartmentTopBlockWidget::widget(['apartmentPage' => $model]);
/*
	echo ApartmentDescriptionWidget::widget(['apartmentPage' => $model]);
	echo ApartmentFloorPlanWidget::widget(['apartmentPage' => $model]);
	echo ApartmentInvestCalculatorWidget::widget(compact('model'));
	echo ApartmentTourWidget::widget(compact('model'))
	if(!$model->is_secondary) {
		echo ApartmentConstructorWidget::widget(compact('model','complex', 'building'));
	}
/**/
