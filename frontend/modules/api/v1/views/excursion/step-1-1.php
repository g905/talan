<?php

use frontend\modules\general\widgets\CommonComplexScriptsWidget;
use \frontend\modules\api\v1\widgets\ViewHeaderConfig;
use metalguardian\fileProcessor\helpers\FPM;

$this->params['currentCity'] = $model->city;
$this->params['headerMenu'] = $model->apartmentComplexMenu;
$this->params['headerMenuType'] = $model;
$this->params['breadcrumbs'][] = $model->label;
$this->registerJs('window.preloader.done();'); // TODO: get rid of temporary preloader bug fix.

$siteUrl = Yii::$app->params['baseApiUrl'];
$flatUrl = $siteUrl . '/api/excursion/step2?token=' . $agency->token;
$this->title = 'step-1-1';
echo CommonComplexScriptsWidget::widget(['currentComplex' => $model]);
?>

<?php
echo ViewHeaderConfig::widget([
		'complex' => $model,
		'step' => 11,
	]);

if ($mapModel) : ?>

	<?php $mapModel->content = preg_replace('/%complex_alias%\/+/', "", $mapModel->content);?>

	<section class="choose-house <?= $mapModel->class ?>">
		<div class="wrap wrap_mobile_full">
			<div class="choose-house__lists">
				<a href="#" class="choose-house__items choose-house__items--active">Выбор дома</a>
				<a href="#" class="choose-house__items ">Выбор квартиры</a>
				<a href="#" class="choose-house__items ">Выбор отделки</a>
			</div>
			<!--<?//= ApartmentConfiguratorTabsWidget::widget(['model' => $mapModel]) ?>-->
			<div class="choose-house__box">
				<div class="choose-house__img-box">
					<img src="<?= FPM::originalSrc($mapModel->image->file_id ?? '') ?>" alt="choose_house" class="choose-house__img-bg"> </div>
				<?php
				// заменим Url
				$contentStr = preg_replace([
					'`\"([\\d\.]+)\\/apartments\\/([\\d\.]+)\"`',
					'`\"([\\d\.]+)\\/apartments\"`',
				], [
					'"' . $flatUrl . '&complex_id=' . $model->id . '&apartment_id=$1&building_id=$2"',
					'"' . $flatUrl . '&complex_id=' . $model->id . '&apartment_id=$1"',
				], $mapModel->content);
				echo $contentStr ?>
			</div>
			<ul class="choose-house__lists-desc">
				<?= $mapModel->mob_content ?>
			</ul>
		</div>
	</section>
<?php endif; ?>
<style>
	.ajax-popup {
		background-color: white;
		border-radius: 10px;
		padding: 10px;
		position: absolute;
		top: 0;
		width: fit-content;
		border: 1px solid black;
	}
	.hr {
		width: 100px;
		float: left;
		background-color: gray;
		border: none;
		height: 2px
	}
	.ajax-popup-header-wrap {
		margin-bottom: 10px;
		padding-bottom: 10px;
		width: fit-content;
	}
	.ajax-popup-header {
		color: green;
		text-transform: uppercase;
	}
	.ajax-popup-date {
		margin-bottom: 10px;
		padding-bottom: 10px;
	}
	.ajax-popup-date .date {
		display: inline;
	}
	.ajax-popup-left-col {
		float: left;
		width: 80px;
		white-space: nowrap;
	}
	.ajax-popup-right-col {
		margin-left: 180px;
		white-space: nowrap;
	}
</style>