<?php
use yii\widgets\Pjax;
use frontend\modules\api\v1\widgets\FilterWidget;
use frontend\modules\api\v1\widgets\ApartmentsListView;
use \frontend\modules\api\v1\widgets\ViewHeaderConfig;

use \frontend\modules\api\v1\assets\ApiAsset;

ApiAsset::register($this);

$siteUrl = Yii::$app->params['baseApiUrl'];
$currUrl = $siteUrl . '/api/excursion/step12?token=' . $agency->token;
$this->title = 'step-1-2';
?>

<?php
echo ViewHeaderConfig::widget([
	'complex' => $model,
	'step' => 12,
]);
?>

<?php $pjax = Pjax::begin([
	'id' => 'pjax-catalog-list',
	'timeout' => 25000,
	'enablePushState' => true,
	'scrollTo' => 0
	//'linkSelector' => '.pjax_link, .pagination__item a',
]) ?>
<style>
	.test2 {
		display: none;
	}
	.show_filter_btn {
		background: #1e9b57;
		margin-left: 5px;
		border: 1px solid #1e9b57;
		color: white;
		width: 100%;
	}
	.show_filter_btn a {
		margin: calc(50% - 25px);
		border: none;
		color: white;
	}
	.buffer__overflow-x-mobile {
		height: 100%;
	}
	.act {
		background: white;
	}
	.show_filter_btn.act a {
		color: black;
	}
	@media screen and (min-width: 767px) {
		.show_filter_btn {
			display: none;
		}
		.buffer__overflow-x-mobile {
			display: block;
		}
	}
</style>
<?php
$this->registerJs(
	"
    filter_flag = (localStorage.getItem('show_filter') === 'true');
    if (!filter_flag) {
        $('.buffer__overflow-x-mobile').addClass('test2');
        $('.show_filter_btn').removeClass('act');
    } else {
        $('.buffer__overflow-x-mobile').removeClass('test2');
        $('.show_filter_btn').addClass('act');
    } 
    $('.show_filter_btn').on('click', function () {
        filter_flag = (localStorage.getItem('show_filter') === 'true');
            $('.buffer__overflow-x-mobile').fadeToggle('fast', function () {
                console.log(filter_flag);
                if (!filter_flag) {
                    $('.buffer__overflow-x-mobile').addClass('test2');
                    $('.show_filter_btn').addClass('act');
                } else {
                    $('.buffer__overflow-x-mobile').removeClass('test2');
                    $('.show_filter_btn').removeClass('act');
                } 
            });
            
            localStorage.setItem('show_filter', !filter_flag);
            console.log(localStorage.getItem('show_filter') === 'true');
        });",
	\yii\web\View::POS_END,
	'show-filter-btn'
);
?>

<!-- -->
<div class="wrap temporary wrap_mobile_full">

	<div class="buffer">
		<div class="show_filter_btn wow fadeInUp">
			<a class="choose-ap__btn" href="#"> Фильтры </a>
		</div>
		<div class="buffer__overflow-x-mobile">
			<div class="buffer__overflow-mobile">
				<?php
				echo FilterWidget::widget([
					'formBaseUrl' => $currUrl,
					'complex' => $model,
					'pjaxContainerID' => $pjax->getId(),
				]);
				?>
			</div>
		</div>
	</div>

	<?= ApartmentsListView::widget([
		'dataProvider' => $provider,
		'model' => $model,
		'itemView' => '@frontend/modules/api/v1/widgets/views/apartment-list-view/_listItem.php',
	]) ?>
</div>
<?php $pjax::end() ?>
