<?php
use frontend\modules\api\v1\widgets\BuildingPlanWidget;
use \frontend\modules\api\v1\widgets\ViewHeaderConfig;

$siteUrl = Yii::$app->params['baseApiUrl'];
$this->title = 'step-2';

$this->params['currentCity'] = $complex->city;
$this->params['headerMenu'] = $complex->apartmentComplexMenu;
$this->params['headerMenuType'] = $complex;
?>

<?php echo ViewHeaderConfig::widget([
	'complex' => $complex,
	'step' => 11,
]);

echo BuildingPlanWidget::widget([
	'building' => $building,
	'entrance' => $entrance,
]);
