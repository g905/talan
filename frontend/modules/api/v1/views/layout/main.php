<?php
use \yii\helpers\Html;
use \frontend\modules\api\v1\assets\ApiAsset;

ApiAsset::register($this);

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php // echo Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
	<?php $this->beginBody() ?>

	<?= $content; ?>

	<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>