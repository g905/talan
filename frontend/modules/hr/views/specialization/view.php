<?php

use frontend\modules\hr\widgets\PosterWidget;
use frontend\modules\hr\widgets\MissionWidget;
use frontend\modules\hr\widgets\GalleryWidget;
use frontend\modules\hr\widgets\ReviewsWidget;
use frontend\modules\hr\widgets\BenefitsWidget;
use frontend\modules\hr\widgets\ArticlesWidget;
use frontend\modules\hr\widgets\VacanciesWidget;
use frontend\modules\hr\widgets\PrinciplesWidget;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageHeaderWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;

/**
 * @var $this yii\web\View
 * @var $model common\models\Career
 * @var $city common\models\City
 */

$this->params['breadcrumbs'][] = $model->top_label;
$this->params['headerMenu'] = $model->subMenus;
$this->params['headerMenuType'] = $model;
?>

<?= PosterWidget::widget(compact('model')) ?>

<?= PageHeaderWidget::widget(['currentCity' => $city]) ?>

<?= BenefitsWidget::widget(compact('model')) ?>

<?= MissionWidget::widget(compact('model')) ?>

<?= PrinciplesWidget::widget(compact('model')) ?>

<?= GalleryWidget::widget(compact('model')) ?>

<?= ArticlesWidget::widget(compact('model')) ?>

<?= ReviewsWidget::widget(compact('model')) ?>

<?= VacanciesWidget::widget(compact('model')) ?>

<?php if ($model->manager): ?>
    <?= PageFormBottomBlockWidget::widget([
        'formType' => PageFormBottomBlockWidget::FORM_TYPE_JOB_WITH_DOC,
        'formWrapperClass' => 'form-content questions__form-content ipot-form-content',
        'formTitle' => $model->form_label,
        'formDescription' => $model->form_description,
        'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
        'manager' => $model->manager,
        'blockClass' => 'questions questions_article commercial-question',
        'onsubmitJS' => $model->form_onsubmit,
    ]) ?>
<?php else: ?>
    <div data-bg-src=""></div>
<?php endif; ?>

<?= PageFooterWidget::widget(['currentCity' => $city]) ?>
