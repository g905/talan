<?php

use yii\widgets\Breadcrumbs;
use common\helpers\SiteUrlHelper;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;

/**
 * @var $this yii\web\View
 * @var $model common\models\Vacancy
 * @var $city common\models\City
 */

$this->params['breadcrumbs'][] = [
    'label' => 'Вакансии',
    'url' => SiteUrlHelper::getVacanciesUrl(),
];
$this->params['breadcrumbs'][] = $model->label;
$this->params['headerMenu'] = $model->subMenus;
$this->params['headerMenuType'] = $model;
?>

<div class="header__row header__row_apartment">
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'homeLink' => [
                'label' => Yii::t('front/header', '_Breadcrumbs home label'),
                'url' => Yii::$app->homeUrl,
                'class' => 'bread-crumbs__link',
            ],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'options' => [
                'class' => 'bread-crumbs bread-crumbs_many bread-crumbs_grey header__bread-crumbs',
            ]
        ]) ?>
    </div>
</div>

<section class="vac-about">
    <div class="wrap wrap_mobile_full vac-about__wrap">
        <h3 class="h3 vac-about__h3 wow fadeInRight"><?= $model->label ?></h3>
        <p class="vac-about__description wow fadeInRight" data-wow-delay="0.7s" data-wow-duration="1s">
            <?= $model->description ?>
        </p>

        <?php if ($model->responsibilities_label) : ?>
            <div class="vac-about__block wow fadeInLeft" data-wow-delay="1s" data-wow-duration="1s">
                <strong><?= $model->responsibilities_label ?></strong>
                <?= $model->responsibilities_content ?>
            </div>
        <?php endif ?>

        <?php if ($model->requirements_label) : ?>
            <div class="vac-about__block wow fadeInRight" data-wow-delay="1s" data-wow-duration="1s">
                <strong><?= $model->requirements_label ?></strong>
                <?= $model->requirements_content ?>
            </div>
        <?php endif ?>

        <?php if ($model->benefits_label) : ?>
            <div class="vac-about__block wow fadeInLeft" data-wow-delay="1s" data-wow-duration="1s">
                <strong><?= $model->benefits_label ?></strong>
                <?= $model->benefits_content ?>
            </div>
        <?php endif ?>

        <section class="share share_horizontal wow fadeIn">
            <div class="share__line"></div>
            <div class="social share__social">
             <!--   <a href="https://www.instagram.com/talan_tyumen" class="social__item socicon-instagram" target="_blank"></a>-->
             <!--   <a href="https://ok.ru/profile/588185280058" class="social__item socicon-odnoklassniki" target="_blank"></a>-->
             <!--   <a href="https://vk.com/talan_tyumen" class="social__item socicon-vkontakte" target="_blank"></a>-->
             <!--   <a href="https://www.facebook.com/profile.php?id=100022990113201&ref=bookmarks" class="social__item socicon-facebook" target="_blank"></a>-->
                <a href="https://connect.ok.ru/offer?url=http:<?= yii\helpers\Url::current() ?>" class="social__item socicon-odnoklassniki" target="_blank"></a>
                <a href="http://vkontakte.ru/share.php?url=http:<?= yii\helpers\Url::current()?>>&noparse=true" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false" class="social__item socicon-vkontakte" target="_blank"></a>
                <a id="share" href="http://www.facebook.com/sharer.php?u=http:<?= yii\helpers\Url::current() ?>" class="social__item socicon-facebook" target="_blank"></a>
            </div>
            <div class="share__text"> поделиться </div>
        </section>

    </div>
</section>

<?php $faqBlocks = $faq;/*$model->faqBlocks;*/ ?>
<?php if ($faqBlocks) : ?>
<section class="vac-faq">
    <div class="wrap wrap_mobile_full">
        <h3 class="h3 vac-faq__h3 wow fadeInRight"><?= $model->faq_label ?></h3>
        <div class="vac-faq__block-questions">
            <?php foreach ($faqBlocks as $faqBlock) : ?>
                <div class="vac-faq__question wow fadeInUp" data-wow-delay="1s" data-wow-duration="1s">
                    <a href="#" class="vac-faq__link"><?= $faqBlock->title ?></a>
                    <div class="vac-faq__textblock"><?= $faqBlock->description ?></div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
<?php endif ?>

<?php if ($model->manager): ?>
    <?= PageFormBottomBlockWidget::widget([
        'formType' => PageFormBottomBlockWidget::FORM_TYPE_JOB_WITH_DOC,
        'formWrapperClass' => 'form-content questions__form-content ipot-form-content',
        'formTitle' => $model->form_label,
        'formDescription' => $model->form_description,
        'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
        'manager' => $model->manager,
        'blockClass' => 'questions questions_article commercial-question',
        'onsubmitJS' => $model->form_onsubmit,
        'vacancy' => $model,
    ]) ?>
<?php endif ?>

<?php $related = $model->related; ?>
<?php if ($related) : ?>
    <section class="vacancy">
        <div class="wrap wrap_mobile_full">
            <div class="vacancy__top-row clearfix">
                <h2 class="h2 vacancy__h2 wow fadeInLeft" data-wow-delay="0.8s" data-wow-duration="0.8s"><?= $model->related_label ?></h2>
            </div>
            <div class="vacancy__card-block clearfix">
                <?php foreach ($related as $relatedVacancy) : ?>
                    <a href="<?= $relatedVacancy->viewUrl() ?>" class="vacancy__card clearfix">
                    <div class="vacancy__left-block wow fadeInLeft" data-wow-delay="1s" data-wow-duration="1s">
                        <div class="vacancy__card-title vacancy__card-title--similar"><?= $relatedVacancy->label ?></div>
                    </div>
                    <div class="vacancy__right-block clearfix wow fadeInRight" data-wow-delay="1.5s" data-wow-duration="1.5s">
                        <div class="vacancy__card-link arrow arrow_right">arrow</div>
                        <div class="vacancy__card-city"><?= $city->label ?></div>
                        <div class="vacancy__card-department"><?= $model->specialization->top_label ?></div>
                    </div>
                </a>
                <?php endforeach ?>
            </div>
        </div>
    </section>
<?php endif ?>

<?php $principles = $model->principleBlocks ?>
<?php if ($principles) : ?>
    <section class="values vac-values">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 values__h2 wow fadeInLeft"><?= $model->principles_label ?></h2>
        <div class="values__list">
            <?php foreach ($principles as $principle) : ?>
                <div class="card values__card wow fadeInUp">
                <div class="card__img" data-bg-src="<?= $principle->getImageSrc() ?>" data-bg-pos="center" data-bg-size="cover"></div>
                <h5 class="h5 card__h5"><?= $principle->title ?></h5>
                <div class="divider card__divider"></div>
                <div class="card__text">
                    <?= $principle->description ?>
                </div>
            </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
<?php endif ?>

<?= PageFooterWidget::widget(['currentCity' => $city]) ?>

<?php
$this->registerJs('

    let block = $(".questions").position().top;
    function show_callback_popup()
    {
        if($(window).scrollTop() >= (block-200))
        {
            $(".footer__button").click();
            $(window).off("scroll", show_callback_popup);
        }
    }
    $(window).on("scroll", show_callback_popup);

', \yii\web\View::POS_END);
?>