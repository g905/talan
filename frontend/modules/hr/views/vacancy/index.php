<?php

use yii\widgets\Breadcrumbs;
use frontend\helpers\FrontendHelper;
use frontend\modules\page\widgets\PageFooterWidget;
use frontend\modules\hr\widgets\VacanciesFilterList;
use frontend\modules\page\widgets\PageFormBottomBlockWidget;

/**
 * @var $this yii\web\View
 * @var $city common\models\City
 * @var $model common\models\VacanciesPage
 */

$this->params['breadcrumbs'][] = $model->label;
$this->params['headerMenu'] = $model->subMenus;
$this->params['headerMenuType'] = $model;
?>

<div class="header__row header__row_apartment">
    <div class="wrap wrap_mobile_full">
        <?= Breadcrumbs::widget([
            'tag' => 'div',
            'itemTemplate' => "{link}\n",
            'activeItemTemplate' => " <span class='bread-crumbs__link'>{link}</span>\n",
            'homeLink' => [
                'label' => bt('_Breadcrumbs home label', 'header'),
                'url' => app()->homeUrl,
                'class' => 'bread-crumbs__link',
            ],
            'links' => isset($this->params['breadcrumbs']) ? FrontendHelper::formatBreadcrumbs($this->params['breadcrumbs']) : [],
            'options' => [
                'class' => 'bread-crumbs bread-crumbs_many bread-crumbs_grey header__bread-crumbs',
            ]
        ]) ?>
    </div>
</div>

<?= VacanciesFilterList::widget(compact('model')) ?>

<?php if ($model->manager): ?>
    <?= PageFormBottomBlockWidget::widget([
        'formType' => PageFormBottomBlockWidget::FORM_TYPE_JOB_WITH_DOC,
        'formWrapperClass' => 'form-content questions__form-content ipot-form-content',
        'formTitle' => $model->form_label,
        'formDescription' => $model->form_description,
        'formPosition' => PageFormBottomBlockWidget::FORM_POSITION_LEFT,
        'manager' => $model->manager,
        'blockClass' => 'questions questions_article commercial-question',
        'onsubmitJS' => $model->form_onsubmit,
    ]) ?>
<?php else: ?>
    <div data-bg-src=""></div>
<?php endif; ?>

<?= PageFooterWidget::widget(['currentCity' => $city]) ?>

<?php
$this->registerJs('

    let block = $(".questions").position().top;
    function show_callback_popup()
    {
        if($(window).scrollTop() >= (block-200))
        {
            $(".footer__button").click();
            $(window).off("scroll", show_callback_popup);
        }
    }
    $(window).on("scroll", show_callback_popup);

', \yii\web\View::POS_END);
?>