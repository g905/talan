<?php

namespace frontend\modules\hr\widgets;

use yii\base\Widget;
use common\models\Career;
use common\models\Specialization;

/**
 * Class ReviewsWidget
 *
 * @package frontend\modules\hr\widgets
 */
class ReviewsWidget extends Widget
{
    /**
     * @var Career|Specialization
     */
    public $model;

    public function run()
    {
        $output = '';
        $reviews = $this->model->reviewBlocks;

        if ($reviews) {
            $title = $this->model->reviews_label;
            $output = $this->render('reviews', compact('title', 'reviews'));
        }

        return $output;
    }
}
