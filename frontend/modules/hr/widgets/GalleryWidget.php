<?php

namespace frontend\modules\hr\widgets;

use common\models\EntityToFile;
use metalguardian\fileProcessor\helpers\FPM;
use yii\base\Widget;
use common\models\Specialization;

/**
 * Class GalleryWidget
 *
 * @package frontend\modules\hr\widgets
 */
class GalleryWidget extends Widget
{
    /**
     * @var Specialization
     */
    public $model;

    public function run()
    {
        $output = '';
        $images = $this->model->galleryImages;

        if ($images) {
            $output = $this->render('gallery', [
                'title' => $this->model->gallery_label,
                'description' => $this->model->gallery_description,
                'images' => $this->prepareImages($images),
            ]);
        }

        return $output;
    }

    /**
     * @param EntityToFile[] $images
     * @return array
     * @throws \Exception
     */
    private function prepareImages($images)
    {
        $output = [];
        foreach ($images as $image) {
            $output[] = FPM::src($image->file_id, 'specialization', 'gallery');
        }

        return $output;
    }
}
