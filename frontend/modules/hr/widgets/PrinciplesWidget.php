<?php

namespace frontend\modules\hr\widgets;

use yii\base\Widget;
use common\models\Specialization;

/**
 * Class PrinciplesWidget
 *
 * @package frontend\modules\hr\widgets
 */
class PrinciplesWidget extends Widget
{
    /**
     * @var Specialization
     */
    public $model;

    public function run()
    {
        $output = '';
        $principles = $this->model->principleBlocks;

        if ($principles) {
            $title = $this->model->principles_label;
            $output = $this->render('principles', compact('title', 'principles'));
        }

        return $output;
    }
}
