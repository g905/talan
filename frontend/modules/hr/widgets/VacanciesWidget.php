<?php

namespace frontend\modules\hr\widgets;

use common\helpers\SiteUrlHelper;
use yii\base\Widget;
use common\models\Specialization;

/**
 * Class VacanciesWidget
 *
 * @package frontend\modules\hr\widgets
 */
class VacanciesWidget extends Widget
{
    /**
     * @var Specialization
     */
    public $model;

    public function run()
    {
        $output = '';
        $vacancies = $this->model->vacancies;

        if ($vacancies) {
            $title = $this->model->vacancies_label;
            $btnLabel = $this->model->vacancies_btn_label;
            $btnLink = SiteUrlHelper::getVacanciesUrl();
            $output = $this->render('vacancies', compact('title', 'btnLabel', 'btnLink', 'vacancies'));
        }

        return $output;
    }
}
