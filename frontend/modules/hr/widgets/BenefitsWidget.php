<?php

namespace frontend\modules\hr\widgets;

use yii\base\Widget;
use common\models\Specialization;

/**
 * Class BenefitsWidget
 *
 * @package frontend\modules\hr\widgets
 */
class BenefitsWidget extends Widget
{
    /**
     * @var Specialization
     */
    public $model;

    public function run()
    {
        $output = '';
        $benefits = $this->model->benefitBlocks;

        if ($benefits) {
            $title = $this->model->details_label;
            $description = strip_tags($this->model->details_description);
            $output = $this->render('benefits', compact('title', 'description', 'benefits'));
        }

        return $output;
    }
}
