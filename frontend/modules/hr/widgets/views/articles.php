<?php

use common\helpers\SiteUrlHelper;

/**
 * @var $this yii\web\View
 * @var $title string
 * @var $btnLink string
 * @var $btnLabel string
 * @var $articles common\models\Article[]
 * @var $cssClass string
 */
?>

<section class="<?= $cssClass ?>">
    <div class="wrap wrap_mobile_full">
        <div class="vtor-details__text-block ">
            <h2 class="h2"><?= $title ?></h2>
        </div>
        <div class="articles__list">
            <?php foreach ($articles as $article) : ?>
                <div class="card articles__card  wow fadeInUp">
                <div class="card__img" data-bg-src="<?= $article->getPreviewImageSrc() ?>" data-bg-pos="center" data-bg-size="cover">
                    <a href="<?= $article->viewUrl() ?>"></a>
                </div>
                <div class="card__content">
                    <h5 class="h5 card__h5">
                        <a href="<?= $article->viewUrl() ?>"><?= $article->label ?></a>
                    </h5>
                    <span class="card__date"><?= $article->getDisplayableListDate() ?></span>
                    <span class="divider card__divider"></span>
                    <ul class="card__tags">
                        <?php foreach($article->blogThemes as $theme) : ?>
                            <li><a href="<?= SiteUrlHelper::getBlogAllUrl(['theme' => $theme->alias]) ?>"><?= $theme->label ?></a></li>
                        <?php endforeach ?>
                    </ul>
                    <div class="card__text">
                        <p><?= $article->short_description ?></p>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
        </div>
        <div class="view-all career__view-all">
            <div class="view-all__line"></div>
            <a href="<?= $btnLink ?>" class="more view-all__more"><?= $btnLabel ?></a>
        </div>
    </div>
</section>
