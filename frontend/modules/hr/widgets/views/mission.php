<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $description string
 */
?>

<section class="mission">
    <h2 class="h2 mission__h2 wow fadeIn">
        <?= $title ?>
        <?php if ($description) : ?>
        <span><?= $description ?></span>
        <?php endif ?>
    </h2>
</section>
