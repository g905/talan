<?php
/**
 * @var $this yii\web\View
 * @var $name string
 * @var $post string
 * @var $text string
 * @var $photo string
 */
?>

<section class="director car-details__director">
    <div class="wrap wrap_mobile_full clearfix">
        <div class="director__img director__img_desk car-details__director-img wow fadeIn" data-bg-src="<?= $photo ?>" data-bg-pos="center" data-bg-size="cover" data-wow-delay="0.5s" data-wow-duration="2s"></div>
        <div class="director__content car-details__director-content">
            <h5 class="h5 director__h5 wow fadeInRight"><?= $name ?></h5>
            <h6 class="h6 director__h6 wow fadeInRight" data-wow-delay="0.3s"><?= $post ?></h6>
            <div class="divider director__divider wow fadeInRight" data-wow-delay="0.6s"></div>
            <div class="director__text wow fadeInRight" data-wow-delay="0.9s"><?= $text ?></div>
        </div>
    </div>
</section>
