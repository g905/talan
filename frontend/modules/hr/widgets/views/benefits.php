<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $description string
 * @var $benefits common\models\blocks\SpecializationBenefit[]
 */
?>

<section class="values values_commercial">
    <div class="wrap wrap_mobile_full">
        <div class="career__top-row clearfix">
            <h2 class="h2 career__h2 wow fadeInLeft"><?= $title ?></h2>
            <div class="career__block-text wow fadeInRight">
                <p class="career__title-description"><?= $description ?></p>
            </div>
        </div>
        <div class="values__list">
            <?php foreach ($benefits as $benefit) : ?>
                <div class="card values__card wow fadeInUp">
                    <div class="card__img" data-bg-src="<?= $benefit->getImageSrc() ?>" data-bg-pos="center" data-bg-size="cover"></div>
                    <h5 class="h5 card__h5"><?= $benefit->title ?></h5>
                    <div class="divider card__divider"></div>
                    <div class="card__text">
                        <?= $benefit->description ?>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
