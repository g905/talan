<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $title string
 * @var $reviews common\models\blocks\CareerReview[]|common\models\blocks\SpecializationReview[]
 */
?>

<section class="testimonials employeers">
    <div class="wrap wrap_mobile_full">
        <h2 class="testimonials__h2 wow fadeInUp"><?= $title ?></h2>
        <div class="testimonials__list employeers__list" data-testimonials-slider data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "dots": false, "prevArrow": ".testimonials .navigation__btn_prev", "nextArrow": ".testimonials .navigation__btn_next", "responsive": [{"breakpoint": 1200, "settings": {"adaptiveHeight": true, "fade": true}}]}'>
            <?php foreach ($reviews as $review) : ?>
                <div class="card testimonials__card">
                    <?php if ($review->getYoutubeCode() !== null) : ?>
                        <div class="card__img wow fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                            <div class="video">
                                <div data-bg-size="cover" data-bg-pos="center">
                                    <iframe src="<?= $review->getYoutubeCode(); ?>" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="card__img wow fadeIn" data-bg-src="<?= $review->getImageSrc() ?>" data-bg-size="cover" data-bg-pos="center" data-wow-delay="0.5s" data-wow-duration="2s"></div>
                    <?php endif; ?>
                <div class="card__inner">
                    <div class="card__content">
                        <h5 class="h5 card__h5 wow fadeInLeft"><?= $review->title ?></h5>
                        <div class="card__excerpt wow fadeInLeft" data-wow-delay="0.3s"><?= $review->sub_title ?></div>
                        <div class="card__divider wow fadeInLeft" data-wow-delay="0.6s"></div>
                        <div class="card__text wow fadeInLeft" data-wow-delay="0.9s"><?= $review->description ?></div>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
        </div>
        <div class="navigation testimonials__navigation career__navigation wow fadeInUp">
            <div class="navigation__content">
                <button class="navigation__btn navigation__btn_prev" data-testimonials-arrow-prev>
                    <div class="arrow arrow_left navigation__arrow"></div>
                </button>
                <div class="navigation__status">
                    <span class="navigation__current" data-testimonials-status-current>..</span>\
                    <span class="navigation__total" data-testimonials-status-total>..</span>
                </div>
                <button class="navigation__btn navigation__btn_next" data-testimonials-arrow-next>
                    <div class="arrow arrow_right navigation__arrow"></div>
                </button>
            </div>
            <div class="navigation__line"></div>
        </div>
    </div>
</section>
