<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $description string
 * @var $images array
 */
?>

<section class="gallery gallery_commercial career__gallery">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 career__h2 wow fadeInRight" data-wow-delay="0.3s"><?= $title ?></h2>
    </div>
    <div class="wrap wrap_mobile_full">
        <div class="gallery__wrap career__gallery-wrap">
            <div class="gallery__slider wow fadeIn" data-wow-delay="0.5s" data-wow-duration="2s" data-gallery-slider data-slick='{"slidesToShow": 1, "prevArrow": " .navigation__build-btn_prev", "nextArrow": " .navigation__build-btn_next", "infinite": true, "slidesToScroll": 1, "dots": false, "fade":true, "asNavFor": "[data-gallery-nav]"}'>
                <?php foreach ($images as $image) : ?>
                    <div data-fancybox="gallery" class="career__card" href="<?= $image ?>">
                        <img src="<?= $image ?>" alt="">
                    </div>
                <?php endforeach ?>
            </div>
            <div class="gallery__nav career__nav" data-gallery-nav data-slick='{"slidesToShow": 7, "slidesToScroll": 1, "dots": false, "focusOnSelect": true, "asNavFor": "[data-gallery-slider]", "infinite": true, "responsive": [{"breakpoint": 1201, "settings": {"slidesToShow": 5}}]}'>
                <?php foreach ($images as $image) : ?>
                    <div class="gallery__item">
                        <div class="gallery__img" data-bg-src="<?= $image ?>" data-bg-size="cover" data-bg-pos="center"></div>
                    </div>
                <?php endforeach ?>
            </div>
            <button class="gallery__zoom career__zoom">
                <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search" /></svg>
            </button>
        </div>
        <div class="gallery__content gallery__content_desk career__gallery-content">
            <p class="career__descr wow fadeInRight" data-wow-delay="0.3s"><?= $description ?></p>
        </div>
    </div>
    <div class="wrap wrap_mobile_full">
        <div class="navigation testimonials__navigation career__build-navigation wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
            <div class="navigation__content">
                <button class="navigation__btn navigation__build-btn_prev slick-arrow" data-gallery-slider-arrow-prev="" style="display: inline-block;">
                    <div class="arrow arrow_left navigation__arrow"></div>
                </button>
                <div class="navigation__status">
                    <span class="navigation__current" data-gallery-slider-status-current="">1</span> \
                    <span class="navigation__total" data-gallery-slider-status-total=""><?= count($images) ?></span>
                </div>
                <button class="navigation__btn navigation__build-btn_next slick-arrow" data-gallery-slider-arrow-next="" style="display: inline-block;">
                    <div class="arrow arrow_right navigation__arrow"></div>
                </button>
            </div>
            <div class="navigation__line"></div>
        </div>
    </div>
</section>
