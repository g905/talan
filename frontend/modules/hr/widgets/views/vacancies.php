<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $btnLabel string
 * @var $btnLink string
 * @var $vacancies common\models\Vacancy[]
 */
?>

<section class="vacancy career__vacancy">
    <div class="wrap wrap_mobile_full">
        <div class="vacancy__top-row clearfix">
            <h2 class="h2 vacancy__h2 wow fadeInLeft" data-wow-delay="0.8s" data-wow-duration="0.8s"><?= $title ?></h2>
            <div class="share share_horizontal career__social wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                <div class="social share__social">
                    <a href="https://www.instagram.com/talan_tyumen" class="social__item socicon-instagram" target="_blank"></a>
                    <a href="https://ok.ru/profile/588185280058" class="social__item socicon-odnoklassniki" target="_blank"></a>
                    <a href="https://vk.com/talan_tyumen" class="social__item socicon-vkontakte" target="_blank"></a>
                    <a href="https://www.facebook.com/profile.php?id=100022990113201&amp;ref=bookmarks" class="social__item socicon-facebook" target="_blank"></a>
                </div>
                <div class="share__text"> поделиться </div>
            </div>
        </div>
        <div class="vacancy__card-block clearfix">
            <?php foreach ($vacancies as $vacancy) : ?>
                <a href="<?= $vacancy->viewUrl() ?>" class="vacancy__card clearfix">
                    <div class="vacancy__left-block wow fadeInLeft" data-wow-delay="1s" data-wow-duration="1s">
                        <div class="vacancy__card-title"><?= $vacancy->label ?></div>
                    </div>
                    <div class="vacancy__right-block clearfix wow fadeInRight" data-wow-delay="1.5s" data-wow-duration="1.5s">
                        <div class="vacancy__card-link arrow arrow_right">arrow</div>
                        <div class="vacancy__card-city"><?= $vacancy->city->label ?></div>
                        <div class="vacancy__card-department"><?= $vacancy->specialization->top_label ?></div>
                    </div>
                </a>
            <?php endforeach ?>
        </div>
        <?php if ($btnLabel && $btnLink) : ?>
            <div class="view-all">
                <div class="view-all__line"></div>
                <a href="<?= $btnLink ?>" class="more view-all__more"><?= $btnLabel ?></a>
            </div>
        <?php endif; ?>
    </div>
</section>
