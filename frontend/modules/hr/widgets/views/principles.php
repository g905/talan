<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $principles common\models\blocks\SpecializationPrinciple[]
 */
?>

<section class="values">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 values__h2 wow fadeInLeft"><?= $title ?></h2>
        <div class="values__list">
            <?php foreach ($principles as $principle) : ?>
                <div class="card values__card wow fadeInUp">
                    <div class="card__img" data-bg-src="<?= $principle->getImageSrc() ?>" data-bg-pos="center" data-bg-size="cover"></div>
                    <h5 class="h5 card__h5"><?= $principle->title ?></h5>
                    <div class="divider card__divider"></div>
                    <div class="card__text">
                        <?= $principle->description ?>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
