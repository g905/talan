<?php
/**
 * @var $this yii\web\View
 * @var $title string
 * @var $specializations common\models\Specialization[]
 */
?>

<div class="specialization">
    <div class="wrap wrap_mobile_full">
        <h2 class="h2 specialization__h2 wow fadeInLeft"><?= $title ?></h2>
        <div class="specialization__list">
            <?php foreach ($specializations as $specialization) : ?>
                <a href="<?= $specialization->viewUrl() ?>" class="specialization__link">
                    <div class="card specialization__card wow fadeInUp" style="background-color: <?= $specialization->color ?> ">
                        <div class="specialization__bg" data-bg-src="<?= $specialization->getPreviewImageSrc() ?>" data-bg-pos="center" data-wow-delay="0.5s" data-wow-duration="2s"></div>
                        <div class=" specialization__title"><?= $specialization->top_label ?></div>
                    </div>
                </a>
            <?php endforeach ?>
        </div>
    </div>
</div>
