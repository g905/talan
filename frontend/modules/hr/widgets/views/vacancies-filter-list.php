<?php

use yii\widgets\Pjax;
use yii\widgets\ListView;
use common\models\Vacancy;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $title string
 * @var $provider yii\data\ActiveDataProvider
 * @var $cityList array
 * @var $specializationList array
 */
?>

<section class="vacancy">
    <div class="wrap wrap_mobile_full">
        <div class="vacancy__top-row clearfix">
            <h2 class="h2 vacancy__h2 wow fadeInLeft" data-wow-delay="0.8s" data-wow-duration="0.8s"><?= $title ?></h2>

            <?php $form = ActiveForm::begin(['id' => 'vacancy-filter-form']) ?>
                <div class="vacancy__cities wow fadeInRight" data-wow-delay="1s" data-wow-duration="1s">
                    <div class="filters__section">
                        <div class="filters__inner filters__inner_side_right">
                            <div class="custom-select filters__custom-select" data-custom-select>
                                <select name="vacancy-city" id="vacancy-city-select">
                                    <?php foreach ($cityList as $cityId => $cityName) : ?>
                                        <option value="<?= $cityId ?>"><?= $cityName ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vacancy__prof wow fadeInRight" data-wow-delay="1.5s" data-wow-duration="1.5s">
                    <div class="filters__section">
                        <div class="filters__inner filters__inner_side_right">
                            <div class="custom-select filters__custom-select" data-custom-select>
                                <select name="specialization" id="vacancy-specialization-select">
                                    <?php foreach ($specializationList as $specId => $specName) : ?>
                                        <option value="<?= $specName ?>"><?= $specName ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            <?php $form::end() ?>
        </div>

        <?php Pjax::begin(['id' => 'vacancies-list-pjax']) ?>
            <?= ListView::widget([
                'dataProvider' => $provider,
                'itemView' => '_vacancies-filter-list-item',
                'options' => ['tag' => false],
                'itemOptions' => ['tag' => false],
                'layout' => Vacancy::getVacancyLayout($provider),
                'emptyTextOptions' => ['style' => 'margin: 30px 0 60px'],
                'pager' => [
                    'options' => ['class' => 'pagination apartaments__pagination'],
                    'maxButtonCount' => 5,
                    'firstPageLabel' => '...',
                    'lastPageLabel' => '...',
                    'pageCssClass' => '',
                    'firstPageCssClass' => 'first',
                    'lastPageCssClass' => 'last',
                    'prevPageCssClass' => 'prev',
                    'nextPageCssClass' => 'next',
                    'activePageCssClass' => 'active'
                ]
            ]) ?>
        <?php Pjax::end() ?>
    </div>
</section>
