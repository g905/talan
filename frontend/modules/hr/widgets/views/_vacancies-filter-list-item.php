<?php

use yii\widgets\ListView;

/**
 * @var $this yii\web\View
 * @var $model common\models\Vacancy the data model
 * @var $key mixed the key value associated with the data item
 * @var $index integer the zero-based index of the data item in the items array returned by [[dataProvider]].
 * @var $widget ListView this widget instance
 */
?>

<a href="<?= $model->viewUrl() ?>" class="vacancy__card clearfix">
    <div class="vacancy__left-block wow fadeInLeft" data-wow-delay="1s" data-wow-duration="1s">
        <div class="vacancy__card-title"><?= $model->label ?></div>
        <div class="vacancy__card-description"><?= $model->description ?></div>
    </div>
    <div class="vacancy__right-block clearfix wow fadeInRight" data-wow-delay="1.5s" data-wow-duration="1.5s">
        <div class="vacancy__card-link arrow arrow_right">arrow</div>
        <div class="vacancy__card-city"><?= $model->city->label ?></div>
        <div class="vacancy__card-department"><?= $model->specialization->top_label ?></div>
    </div>
</a>
