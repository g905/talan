<?php
/**
 * @var $this yii\web\View
 * @var $topTitle string
 * @var $topDescription string
 * @var $topBtnLabel string
 * @var $topBtnLink string
 * @var $topImage string
 * @var $topVideo string
 * @var $innerClass string
 */
?>

<section class="poster poster_padding">
    <video class="poster__bg poster__bg_video" autoplay loop muted data-video-src="<?= $topVideo ?>" data-poster-video data-autoplay data-keepplaying></video>
    <div class="poster__bg poster__bg_photo" data-bg-src="<?= $topImage ?>" data-bg-pos="center" data-bg-size="cover"></div>
    <div class="poster__cover"></div>
    <div class="<?= $innerClass ?>">
        <div class="poster__content">
            <h1 class="h1 poster__h1 wow fadeInUp"><?= $topTitle ?></h1>
            <h5 class="h5 poster__h5 wow fadeInUp" data-wow-delay="0.3s"><?= $topDescription ?></h5>
            <a href="<?= $topBtnLink ?>" class="button button_transparent poster__button wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                <?= $topBtnLabel ?>
                <div class="button__bg"></div>
                <div class="button__blip button__blip_hover"></div>
                <div class="button__blip button__blip_click"></div>
            </a>
        </div>
    </div>
    <div class="down poster__down wow down-to-top">
        <button class="down__button" data-scroll-down></button>
        <div class="down__label">вниз</div>
    </div>
</section>
