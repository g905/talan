<?php

namespace frontend\modules\hr\widgets;

use yii\base\Widget;
use common\models\Specialization;

/**
 * Class MissionWidget
 *
 * @package frontend\modules\hr\widgets
 */
class MissionWidget extends Widget
{
    /**
     * @var Specialization
     */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model->mission_label) {
            $title = $this->model->mission_label;
            $description = $this->model->mission_sub_label;
            $output = $this->render('mission', compact('title', 'description'));
        }

        return $output;
    }
}
