<?php

namespace frontend\modules\hr\widgets;

use yii\base\Widget;
use common\models\City;
use common\models\Specialization;
use common\models\VacanciesPage;

/**
 * Class VacanciesFilterList
 *
 * @package frontend\modules\hr\widgets
 */
class VacanciesFilterList extends Widget
{
    /**
     * @var VacanciesPage
     */
    public $model;
    /**
     * @var int limit output.
     */
    public $limit = 10;

    public function run()
    {
        $output = '';

        if ($this->model) {
            $provider = $this->model->getDataProvider(request()->getQueryParams(), $this->limit);
            $title = $this->model->label;
            $cityList = $this->cityList();
            $specializationList = $this->specializationList();
            $output = $this->render('vacancies-filter-list', compact('title', 'provider', 'cityList', 'specializationList'));
        }

        return $output;
    }

    private function cityList()
    {
        return City::getItems('id', 'label');
    }

    private function specializationList()
    {
        //return Specialization::getItems('id', 'top_label');
        return Specialization::getUniqueLabels();
    }
}
