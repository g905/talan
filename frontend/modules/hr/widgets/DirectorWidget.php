<?php

namespace frontend\modules\hr\widgets;

use yii\base\Widget;
use common\models\Career;

/**
 * Class DirectorWidget
 *
 * @package frontend\modules\hr\widgets
 */
class DirectorWidget extends Widget
{
    /**
     * @var Career
     */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model->director_name && $this->model->director_text) {
            $output = $this->render('director', [
                'name' => $this->model->director_name,
                'post' => $this->model->director_post,
                'text' => $this->model->director_text,
                'photo' => $this->model->getDirectorPhotoSrc()
            ]);
        }

        return $output;
    }
}
