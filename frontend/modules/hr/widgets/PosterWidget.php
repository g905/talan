<?php

namespace frontend\modules\hr\widgets;

use common\models\Specialization;
use yii\base\Widget;
use common\models\Career;

/**
 * Class PosterWidget
 *
 * @package frontend\modules\hr\widgets
 */
class PosterWidget extends Widget
{
    /**
     * @var Career|Specialization
     */
    public $model;

    public function run()
    {
        $output = '';

        if ($this->model) {
            $output = $this->render('poster', [
                'topTitle' => $this->model->top_label,
                'topDescription' => $this->model->top_description,
                'topBtnLabel' => $this->model->top_btn_label,
                'topBtnLink' => $this->model->top_btn_link,
                'topImage' => $this->model->getTopImageSrc(),
                'topVideo' => $this->model->getTopVideoSrc(),
                'innerClass' => $this->model instanceof Career ? 'poster__inner' : 'poster__inner career__inner'
            ]);
        }

        return $output;
    }
}
