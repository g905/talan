<?php

namespace frontend\modules\hr\widgets;

use yii\base\Widget;
use common\models\Career;
use common\models\Specialization;
use common\helpers\SiteUrlHelper;

/**
 * Class ArticlesWidget
 *
 * @package frontend\modules\hr\widgets
 */
class ArticlesWidget extends Widget
{
    /**
     * @var Career|Specialization
     */
    public $model;

    public function run()
    {
        $output = '';
        $articles = $this->model->articles;

        if ($articles) {
            $title = $this->model->articles_label;
            $btnLink = SiteUrlHelper::getBlogAllUrl();
            $btnLabel = $this->model->articles_btn_label ?? 'Все статьи';
            $cssClass = $this->model instanceof Career
                ? 'articles vtor-details__articles career__articles car-details__journal'
                : 'articles vtor-details__articles career__articles'; // ¯\_(ツ)_/¯
            $output = $this->render('articles', compact('title', 'btnLabel', 'btnLink', 'articles', 'cssClass'));
        }

        return $output;
    }
}
