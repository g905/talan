<?php

namespace frontend\modules\hr\widgets;

use yii\base\Widget;
use common\models\Career;

/**
 * Class SpecializationWidget
 *
 * @package frontend\modules\hr\widgets
 */
class SpecializationWidget extends Widget
{
    /**
     * @var Career
     */
    public $model;

    public function run()
    {
        $output = '';
        $specializations = $this->model->specializations;

        if ($specializations) {
            $title = $this->model->specializations_label;
            $output = $this->render('specialization', compact('title', 'specializations'));
        }

        return $output;
    }
}
