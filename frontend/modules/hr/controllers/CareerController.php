<?php

namespace frontend\modules\hr\controllers;

use common\models\City;
use common\models\Career;
use frontend\components\MetaRegistrar;
use frontend\components\FrontendController;

/**
 * Class CareerController
 *
 * @package frontend\modules\hr\controllers
 */
class CareerController extends FrontendController
{
    public function actionView()
    {
        $city = City::getCurrentCity(request()->get('city'));
        $model = Career::findOnePublishedByCityOrFail($city->id);
        $model->incrementViewsCount();
        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->top_label);

        return $this->render('view', compact('model', 'city'));
    }
}
