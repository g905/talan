<?php

namespace frontend\modules\hr\controllers;

use common\models\City;
use common\models\Vacancy;
use common\models\VacanciesPage;
use frontend\components\MetaRegistrar;
use frontend\components\FrontendController;

/**
 * Class VacancyController
 *
 * @package frontend\modules\hr\controllers
 */
class VacancyController extends FrontendController
{
    public function actionIndex()
    {
        $city = City::getCurrentCity(request()->get('city'));
        $model = VacanciesPage::findOnePublishedByCityOrFail($city->id);
        $model->incrementViewsCount();
        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->label);

        return $this->render('index', compact('model', 'city'));
    }

    public function actionView($alias)
    {
        $city = City::getCurrentCity(request()->get('city'));
        $model = Vacancy::findOnePublishedByAliasAndCityOrFail($alias, $city->id);
        $faq = VacanciesPage::findOnePublishedByCityOrFail($city->id)->faqBlocks;
        $model->incrementViewsCount();
        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->label);

        return $this->render('view', compact('model', 'city', 'faq'));
    }
}
