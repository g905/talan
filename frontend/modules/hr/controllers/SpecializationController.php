<?php

namespace frontend\modules\hr\controllers;

use common\models\City;
use common\models\Specialization;
use frontend\components\MetaRegistrar;
use frontend\components\FrontendController;

/**
 * Class SpecializationController
 *
 * @package frontend\modules\hr\controllers
 */
class SpecializationController extends FrontendController
{
    public function actionView($id)
    {
        $city = City::getCurrentCity(request()->get('city'));
        $model = Specialization::findOnePublishedByCityAndIdOrFail($id, $city->id);
        $model->incrementViewsCount();
        $this->attachLastModifiedHeader($model->updated_at);
        MetaRegistrar::register($model, $model->top_label);

        return $this->render('view', compact('model', 'city'));
    }
}
