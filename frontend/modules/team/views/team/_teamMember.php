<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 6/15/18
 * Time: 1:06 AM
 *
 * @var $model \common\models\TeamMember
 */
?>
<div class="branch_team__players-player wow fadeInUp">
    <div class="branch_team__players-player__img">
        <img src="<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($model->manager->photo->file_id ?? '') ?>" > </div>
    <h6 class="branch_team__players-player__name wow fadeInUp" data-wow-delay="0.05s"> <?= $model->manager->fio ?> </h6>
    <hr class="branch_team__players-player__line wow fadeInUp" data-wow-delay="0.1s">
    <p class="branch_team__players-player__position wow fadeInUp" data-wow-delay="0.15s"> <?= $model->manager->job_position ?> </p>
    <a href="mailto:<?= $model->manager->contact_email ?>" class="button button_green branch_team__players-player__btn wow fadeInUp" data-wow-delay="0.2s">
        <?= Yii::t('front/team', 'write_to_manager') ?>
        <span class="button__bg"></span>
        <div class="button__blip button__blip_hover"></div>
        <div class="button__blip button__blip_click"></div>
    </a>
</div>
