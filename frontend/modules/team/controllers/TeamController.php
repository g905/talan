<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 6/15/18
 * Time: 12:06 AM
 */

namespace frontend\modules\team\controllers;

use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use common\models\City;
use common\models\Team;
use common\models\AboutCompany;
use common\models\TeamPageDefaultCityTab;
use frontend\components\MetaRegistrar;
use frontend\components\FrontendController;

class TeamController extends FrontendController
{
    /**
     * @param null $city
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex($city = null)
    {
        $currentCity = City::getCurrentCity($city);
        $teamPage = Team::getModelByCity($currentCity);
        $aboutModel = AboutCompany::findByCity($currentCity);
        if (!$teamPage || !$aboutModel) {
            throw new NotFoundHttpException();
        }
        $defaultTabDataModel = (new TeamPageDefaultCityTab())->get();
        MetaRegistrar::register($teamPage, $teamPage->label);

        return $this->render('index', [
            'teamPage' => $teamPage,
            'defaultTabDataModel' => $defaultTabDataModel,
            'aboutModel' => $aboutModel
        ]);
    }

    public function actionCityItems()
    {
        $defaultTabDataModel = (new TeamPageDefaultCityTab())->get();
        $result = '';
        foreach ($defaultTabDataModel->getTeamMembers() as $member) {
            $result .= $this->renderPartial('_teamMember', [
                'model' => $member,
            ]);
        }
        $result = Html::tag('div', $result, ['class' => 'branch_team__players']);

        return $result;
    }
}
