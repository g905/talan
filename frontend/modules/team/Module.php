<?php

namespace frontend\modules\team;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\team\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
