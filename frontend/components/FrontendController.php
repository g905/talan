<?php

namespace frontend\components;

use common\helpers\SiteUrlHelper;
use common\models\City;
use Yii;
use yii\helpers\Url;
use yii\web\Cookie;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * Class FrontendController
 *
 * @author walter
 * @package frontend\components
 */
class FrontendController extends Controller
{
    public function beforeAction($action)
    {
        foreach (app()->getRequest()->get() as $key => $value) {
            if (strpos($key, 'utm_') !== false || strpos($key, 'ref') !== false) {
                $cookies = Yii::$app->response->cookies;

                $cookies->add(new Cookie([
                    'name' => $key,
                    'value' => $value,
                    'expire' => time() + 86400 * 365,
                ]));
            }
        }

        if (parent::beforeAction($action)) {
            $baseUrl = request()->getAbsoluteUrl();
            preg_match("/(http|https):\/\/(www.)*/", $baseUrl, $match);
            if ($match) {
                if (ArrayHelper::getValue($match, 2) == 'www.') {
                    $url = str_replace('www.', '', $baseUrl);

                    return $this->redirect($url, 301);
                }
            }

            if ($pos = strpos($baseUrl, '/index.php')) {
                return $this->redirect(app()->getHomeUrl());
            }


            return true;
        }

        return false;
    }

    protected function attachLastModifiedHeader($modifiedTime): void
    {
        if (YII_ENV_DEV) {
            return;
        }

        $LastModified_unix = $modifiedTime;
        $LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
        $IfModifiedSince = false;
        if (isset($_ENV['HTTP_IF_MODIFIED_SINCE']))
            $IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));
        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
            $IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
        if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
            exit;
        }
        header('Last-Modified: '. $LastModified);
        // todo: check this part, there is an exception on local machine http://joxi.ru/ZrJ4LVoH9wvVa2
    }

    public function checkCity($city){
        $host = $_SERVER['HTTP_HOST'];
        $pCity = explode('.', $host)[0];
        $rCity = City::convertCityAlias($pCity);
        if ( $city != $rCity){
            throw new HttpException(404, "Для города ". $pCity.  " данных не найдено");
            /*  // Переадресация на адрес с правильным городом
            $req = $_SERVER['REQUEST_URI'];
            $proto = explode(':', request()->getAbsoluteUrl())[0];
            $newUrl = $proto . '://' . $city . '.' . app()->params['baseUrl'] . $req;
            $this->redirect($newUrl, 301);
            */
        };
    }
}
