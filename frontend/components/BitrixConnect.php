<?php

namespace frontend\components;

use yii\base\Component;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use frontend\components\bitrix\exceptions\BitrixLeadSendException;

/**
 * BitrixConnect component is responsible for sending data to bitrix endpoint.
 * To use it, you should create an incoming web-hook in bitrix panel,
 * and pass needed credentials to BitrixConnect component.
 *
 * @author Bogdan K. Fedun <delagics@gmail.com>
 * @package frontend\components
 */
class BitrixConnect extends Component
{
    /** @var string $bitrixUrl Bitrix base URL. */
    public $baseUrl;
    /** @var string|int bitrix user ID which created the hook, will be also used as responsible user. */
    public $syncUserId;
    /** @var string incoming web-hook code. */
    public $hookAuthCode;
    /** @var float|int request timeout. */
    public $requestTimeout = 10.0;
    /** @var bool whether to enable SSL certificate verification. */
    public $sslVerification = false;
    /** @var Client instance. */
    private $client;

    public function getClient(): Client
    {
        if ($this->client === null) {
            $this->client = new Client([
                'base_uri' => $this->baseUrl,
                'timeout' => $this->requestTimeout,
                'verify' => $this->sslVerification,
            ]);
        }

        return $this->client;
    }

    public function addLead($params)
    {
       // var_dump($params);
      //  die;
        $utmCookies = [];
        foreach ($_COOKIE as $cName => $cValue) {
            if (substr($cName, 0, 3) == 'utm') $utmCookies[$cName] = $cValue;
        }
        $params['FIELDS']['UTM'] = $utmCookies;
        try {

            $this->getClient()->post($this->apiUrl('crm.lead.add'), ['form_params' => $params]);

        } catch (TransferException $e) {
            throw new BitrixLeadSendException($e->getMessage(), $e->getCode());
        }
    }

    private function apiUrl($method)
    {
        return "rest/{$this->syncUserId}/{$this->hookAuthCode}/{$method}";
    }
}
