<?php

namespace frontend\components;

use common\models\base\StaticPage;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;

/**
 * Class MetaRegistrar
 *
 * @package frontend\components
 */
class MetaRegistrar
{
    public static function register($model, $defaultTitle = null, $metaTagsToFetch = null)
    {
        view()->title = $defaultTitle;

        if ($model instanceof StaticPage) {
            ConfigurationMetaTagRegister::register($model, null, $metaTagsToFetch);
        } else {
            MetaTagRegister::register($model, null, $metaTagsToFetch);
        }
    }
}
