<?php

namespace frontend\components;

use yii\base\Event;
use yii\base\Component;
use frontend\components\bitrix\IBitrixLeadSender;
use frontend\components\bitrix\IBitrixLeadCreator;

/**
 * Class BitrixIntegration
 *
 * @author Andrew Kontseba <andjey.skinwalker@gmail.com>
 * @package frontend\components
 */
class BitrixIntegration extends Component
{
    private $bitrixLeadCreator;
    private $bitrixLeadSender;

    /**
     * BitrixIntegration constructor.
     *
     * @param IBitrixLeadCreator $bitrixLeadCreator
     * @param IBitrixLeadSender $bitrixLeadSender
     * @param array $config
     */
    public function __construct(
        IBitrixLeadCreator $bitrixLeadCreator,
        IBitrixLeadSender $bitrixLeadSender,
        array $config = []
    ) {
        $this->bitrixLeadCreator = $bitrixLeadCreator;
        $this->bitrixLeadSender = $bitrixLeadSender;

        app()->on('bitrixData', function (Event $event) {

            $leadEntity = $this->bitrixLeadCreator->create($event->sender);
            
            $this->bitrixLeadSender->send($leadEntity);
        });

        parent::__construct($config);
    }
}
