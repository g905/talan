<?php

namespace frontend\components;

use common\models\AnalyticsSummary;
use common\models\City;
use yii\base\BootstrapInterface;

class SiteAnalytics implements BootstrapInterface
{
    const NOT_UNIQUE_TIMESTAMP = 5*60; //5 minutes

    public function bootstrap($app)
    {
        $cookiesRead = \Yii::$app->request->cookies;
        $cookiesWrite = \Yii::$app->response->cookies;

        $uniqueToday = $cookiesRead->getValue('uniqueToday', null);
        $notUniqueToday = $cookiesRead->getValue('notUniqueToday', null);


        if ((!isset($uniqueToday)) && (!isset($notUniqueToday))){
            $forTodayTimestamp = strtotime('tomorrow midnight');
            $notUniqueTimestamp = time() + self::NOT_UNIQUE_TIMESTAMP;

            if ($notUniqueTimestamp > $forTodayTimestamp){
                $notUniqueTimestamp = $forTodayTimestamp;
            }
            

           $cookiesWrite->add(new \yii\web\Cookie([
                'name' => 'uniqueToday',
                'value' => '1',
                'expire' => $forTodayTimestamp,
                'domain' => \Yii::$app->params['baseUrl'],
            ]));



            $cookiesWrite->add(new \yii\web\Cookie([
                'name' => 'notUniqueToday',
                'value' => '1',
                'expire' => $notUniqueTimestamp,
                'domain' => \Yii::$app->params['baseUrl'],
            ]));

            $todayAnalytics = AnalyticsSummary::find()
                ->where([
                    'date' => date("Y-m-d")
                ])
                ->one();

            if (!isset($todayAnalytics)){
                $todayAnalytics = new AnalyticsSummary();
                $todayAnalytics->date = date("Y-m-d");
                $todayAnalytics->save();

            }

            $todayAnalytics->visit_count = $todayAnalytics->visit_count + 1;
            $todayAnalytics->unique_visit_count = $todayAnalytics->unique_visit_count + 1;
            $todayAnalytics->save();

            //update city stat
            $currentCity = City::getUserCity();
            if (isset($currentCity)){
                $currentCity->updateAttributes([
                    'view_count' => $currentCity->view_count+1,
                ]);
            }

        }

        if ((isset($uniqueToday))  && (!isset($notUniqueToday))){

            $forTodayTimestamp = strtotime('today midnight') - time();
            $notUniqueTimestamp = time() + self::NOT_UNIQUE_TIMESTAMP;

            if ($notUniqueTimestamp > $forTodayTimestamp){
                $notUniqueTimestamp = $forTodayTimestamp;
            }

            $cookiesWrite->add(new \yii\web\Cookie([
                'name' => 'notUniqueToday',
                'value' => '1',
                'expire' => $notUniqueTimestamp,
                'domain' => \Yii::$app->params['baseUrl'],
            ]));

            $todayAnalytics = AnalyticsSummary::find()
                ->where([
                    'date' => date("Y-m-d")
                ])
                ->one();

            if (!isset($todayAnalytics)){
                $todayAnalytics = new AnalyticsSummary();
                $todayAnalytics->date = date("Y-m-d");
                $todayAnalytics->save();
            }

            $todayAnalytics->visit_count = $todayAnalytics->visit_count + 1;
            $todayAnalytics->save();
        }

        if ((!isset($uniqueToday))  && (isset($notUniqueToday))){
            $cookiesWrite->remove('notUniqueToday');
        }

    }
}
