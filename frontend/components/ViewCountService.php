<?php
/**
 * Created by PhpStorm.
 * User: p-clerick
 * Date: 13.06.19
 * Time: 13:51
 *
 * Сервис посчёта просмотров страницы за определённый период
 * Для работы - создать экземпляр класса
 * с параметрами («категория элемента», «ID экземпляра элемента», [«период хранения»])
 * Период хранения  используется только при создании новой записи. По-умолчанию - 30 дней.
 * Если элемента ещё нет в базе - он добавится.
 * Например, для страницы конкретных апартоаментов - new ViewCountService('apartaments',$model->id)
 * «категория элемента» задаётся произвольно для отличия разных элементов
 * «ID экземпляра элемента» - должен отличать страницы с разными экземплярами элемента.
 * Для внесения IP, проверки базы на актуальность и очистки от старых элементов - вызываем метов ->run()
 * Для получения актуального числа просмотров - метод ->getViews()
 *
 */

namespace frontend\components;

use common\models\ViewCount;
use common\models\ViewItem;


class ViewCountService
{
    const DEFAULT_PERIOD = 30;

    public $item;

    /**
     * @param string $item_name
     * @param integer $item_index
     */
    public function __construct($item_name, $item_index, $period = self::DEFAULT_PERIOD)
    {
        /** @var ViewItem $record */
        $record = ViewItem::findOne(['item' => $item_name, 'item_index' => $item_index]);

        //Если записи ещё нет - создаём с параметрами по-умолчанию
        if (! $record) {
            $record = new ViewItem();
            $record->item = $item_name;
            $record->item_index = $item_index;
            $record->period = $period;
            $record->save();
        }
        $this->item = $record;
    }


    //создание или обновлении записи о посещении

    /**
     * @return bool
     *
     */
    public function setItem()
    {
        $ip = ip2long($_SERVER['REMOTE_ADDR']);

        /** @var ViewItem $item */
        $item = $this->item;

        /** @var ViewCount $row */
        $row = $item->getViewCounts()->andWhere(['ip' => $ip])->one();

        if ($row) {
            $row->updated_at = time();
            $row->save(false);
            return true;
        } else {
            $row = new ViewCount();
            $row->ip = $ip;
            $row->item_id = $item->id;
            $row->save(false);
            return false;
        }
    }

    //удаление старых записей  раз в сутки, период берём из свойств объёкта
    public function clearOld()
    {
        /** @var ViewItem $item */
        $item = $this->item;

        $today = strtotime(date('d-m-Y'));
        if ($item->updated_at < $today) {
            $countList = $item->viewCounts;

            $last = $today - (24 * 60 * 60) * $item->period;
            foreach ($countList as $record) {
                if ($record->created_at < $last) {
                    $record->delete();
                }
            }
            $item->updated_at = time();
            $item->save(true);
        }
        return false;
    }

    public function run()
    {
        $this->clearOld();
        $this->setItem();
        return true;
    }

    public function getViews()
    {
        return $this->item->getViewCounts()->count();
    }
}