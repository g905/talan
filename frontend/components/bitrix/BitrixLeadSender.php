<?php

namespace frontend\components\bitrix;

use common\helpers\MailerHelper;
use frontend\components\BitrixConnect;
use frontend\components\bitrix\entities\ILeadEntity;
use frontend\components\bitrix\exceptions\BitrixLeadSendException;
use yii\web\NotFoundHttpException;



/**
 * Class BitrixLeadSender
 *
 * @author Andrew Kontseba <andjey.skinwalker@gmail.com>
 * @package frontend\components\bitrix
 */
class BitrixLeadSender implements IBitrixLeadSender
{
    /**/
    /**
     * Send LeadEntity to Bitrix24.
     *
     * @param ILeadEntity $leadEntity LeadEntity to send.
     */
    public function send(ILeadEntity $leadEntity): void
    {
        /** @var BitrixConnect $client */
        $client = app()->get('bitrixConnect');
        try {
            $client->addLead(['FIELDS' => $leadEntity->toArray()]);

        } catch (BitrixLeadSendException $e) {
            $this->sendErrorEmail($leadEntity, $e);
            logError($e->getMessage() . ' code: ' . $e->getCode(), 'bitrix-lead-send');
        }
    }

    public function sendErrorEmail(ILeadEntity $leadEntity, $e)
    {
        //file_put_contents("/var/www/LEAD_SEND_ERRORS.txt", print_r($leadEntity->toArray(), true), FILE_APPEND);
        $lead = [];
        $a = $leadEntity->toArray();
        array_walk_recursive($a, function ($item, $key) use (&$lead) {
            $lead[$key] = $item;
        });
        $data = [
            'Ошибка' => 'Ошибка отправки лида в CRM. Нужна проверка.',
            'Детали' => $e->getMessage()
        ];
        $data = array_merge($data, $lead);
        MailerHelper::sendNotificationEmail('Ошибка отправки лида', $data, [], true, true, false);
    }
}
