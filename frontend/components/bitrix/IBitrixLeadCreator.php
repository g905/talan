<?php

namespace frontend\components\bitrix;

use frontend\components\bitrix\entities\ILeadEntity;
use frontend\components\bitrix\exceptions\IInvalidLeadEntityException;

/**
 * Interface IBitrixLeadCreator
 *
 * @package frontend\components\bitrix
 */
interface IBitrixLeadCreator
{
    /**
     * Create new LeadEntity from given data array.
     *
     * @param array $data data array to be populated to produced entity.
     * @return ILeadEntity
     * @throws IInvalidLeadEntityException when passed data is not valid for LeadEntity creation.
     */
    public function create(array $data): ILeadEntity;
}
