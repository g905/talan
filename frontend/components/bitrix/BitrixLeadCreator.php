<?php
/**
 * Created by PhpStorm.
 * User: skinwalker
 * Date: 31.05.18
 * Time: 15:20
 */

namespace frontend\components\bitrix;

use frontend\components\bitrix\entities\ILeadEntity;
use frontend\components\bitrix\entities\LeadEntity;
use frontend\components\bitrix\exceptions\IInvalidLeadEntityException;

class BitrixLeadCreator implements IBitrixLeadCreator
{

    /**
     * Create new LeadEntity from given data array.
     *
     * @param array $data data array to be populated to produced entity.
     * @return ILeadEntity
     * @throws IInvalidLeadEntityException when passed data is not valid for LeadEntity creation.
     */
    public function create(array $data): ILeadEntity
    {
        return new LeadEntity($data);
    }
}
