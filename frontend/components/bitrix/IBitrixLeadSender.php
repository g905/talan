<?php

namespace frontend\components\bitrix;

use frontend\components\bitrix\entities\ILeadEntity;
use frontend\components\bitrix\exceptions\IBitrixLeadSendException;

/**
 * Interface IBitrixService
 *
 * @package frontend\components\bitrix
 */
interface IBitrixLeadSender
{
    /**
     * Send LeadEntity to Bitrix24.
     *
     * @param ILeadEntity $leadEntity LeadEntity to send.
     * @throws IBitrixLeadSendException
     */
    public function send(ILeadEntity $leadEntity): void;
}
