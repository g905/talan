<?php

namespace frontend\components\bitrix\entities;

use yii\base\Arrayable;

/**
 * Interface ILeadEntity
 *
 * @package frontend\components\bitrix
 * @see \yii\base\Arrayable
 */
interface ILeadEntity extends Arrayable
{
    /**
     * @var string supported "Lead" keys.
     */
    const KEY_TITLE = 'TITLE';
    const KEY_NAME = 'NAME';
    const KEY_SECOND_NAME = 'SECOND_NAME';
    const KEY_LAST_NAME = 'LAST_NAME';
    const KEY_COMMENT = 'COMMENTS';
    const KEY_PHONE = 'PHONE';
    const KEY_EMAIL = 'EMAIL';
    const KEY_ADDRESS_CITY = 'ADDRESS_CITY';

    public function getTitle(): string;
    public function getName(): ?string;
    public function getSecondName(): ?string;
    public function getLastName(): ?string;
    public function getComment(): ?string;

    /**
     * @return array
     * @example of returned value [["VALUE": "555888", "VALUE_TYPE": "WORK"]]
     */
    public function getPhones(): array;

    /**
     * @return array
     * @example of returned value [["VALUE": "example@example.com", "VALUE_TYPE": "WORK"]]
     */
    public function getEmails(): array;
}
