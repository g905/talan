<?php

namespace frontend\components\bitrix\entities;

use common\models\City;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class LeadEntity
 *
 * @author Andrew Kontseba <andjey.skinwalker@gmail.com>
 * @package frontend\components\bitrix\entities
 */
class LeadEntity implements ILeadEntity
{
    private $title;
    private $name;
    private $secondName;
    private $lastName;
    private $comment;
    private $phones;
    private $emails;
    private $city;

    /**
     * LeadEntity constructor.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->title = obtain('title', $data, '');
        $this->name = obtain('form.name', $data);
        $this->secondName = obtain('form.secondName', $data);
        $this->lastName = obtain('form.lastName', $data);
        $this->comment = obtain('form.comment', $data);
        $this->phones = obtain('form.phone', $data);
        $this->emails = obtain('form.emails', $data);
        $this->city = obtain('form.user_city', $data);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getSecondName(): ?string
    {
        return $this->secondName;
    }

    /**
     * @return null|string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return null|string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return array
     * @example of returned value [["VALUE": "555888", "VALUE_TYPE": "WORK"]]
     */
    public function getPhones(): array
    {
        return [['VALUE' => $this->phones, 'VALUE_TYPE' => 'WORK']];
    }

    /**
     * @return array
     * @example of returned value [["VALUE": "example@example.com", "VALUE_TYPE": "WORK"]]
     */
    public function getEmails(): array
    {
        return [['VALUE' => $this->emails, 'VALUE_TYPE' => 'HOME']];
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            self::KEY_TITLE => $this->title,
            self::KEY_NAME => $this->name,
        ];
    }

    /**
     * @return array
     */
    public function extraFields()
    {
        $fields = [];

        if ($this->secondName) {
            $fields[self::KEY_SECOND_NAME] = $this->secondName;
        }
        if ($this->lastName) {
            $fields[self::KEY_COMMENT] = $this->lastName;
        }
        if ($this->comment) {
            $fields[self::KEY_COMMENT] = $this->comment;
        }
        if ($this->phones) {
            $fields[self::KEY_PHONE] = $this->getPhones();
        }
        if ($this->emails) {
            $fields[self::KEY_EMAIL] = $this->getEmails();
        }
        if ($this->city) {
        /*    if (City::getCityById($this->city))
            {
                $fields[self::KEY_ADDRESS_CITY] = City::getCityById($this->city)->label;
            } else {
                $fields[self::KEY_ADDRESS_CITY] = $this->city;
            }*/
        $fields[self::KEY_ADDRESS_CITY] = $this->city;

        }

        $cookies = request()->getCookies();
        $fields['UF_CRM_1501758944'] = $this->parseReferer(request()->getReferrer());
        $fields['UF_CRM_1501758959'] = $this->title;
        $bitrixMap = [
            'UTM_MEDIUM' => 'utm_medium',
            'UTM_SOURCE' => 'utm_source',
            'UTM_CAMPAIGN' => 'utm_campaign',
            'UTM_TERM' => 'utm_term',
            'UTM_CONTENT' => 'utm_content',
            'UF_CRM_1501758981' => 'ref',
            'UF_CRM_1501759028' => 'roistat_visit'
        ];

        foreach ($bitrixMap as $bitrix => $utm) {
            if ($cookies->has($utm)) {
                $fields[$bitrix] = $cookies->getValue($utm);
            }
        }

        return $fields;
    }

    /**
     * Convert object props to array.
     *
     * @param array $fields
     * @param array $expand
     * @param bool $recursive
     * @return array
     */
    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        return ArrayHelper::merge($this->fields(), $this->extraFields());
    }

    public function parseReferer($url)
    {
        $parsedUrl = parse_url($url);
        $decodedHost = idn_to_utf8($parsedUrl['host'], 0, INTL_IDNA_VARIANT_UTS46);

        return $parsedUrl['scheme'].'//'.$decodedHost.$parsedUrl['path'];
    }
}
