<?php

namespace frontend\components\bitrix\exceptions;

/**
 * Interface IInvalidLeadEntityException
 *
 * @package frontend\components\bitrix\exceptions
 */
interface IInvalidLeadEntityException
{

}
