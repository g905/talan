<?php

namespace frontend\components\bitrix\exceptions;

use Exception;

/**
 * Interface IBitrixLeadSendException
 *
 * @package frontend\components\bitrix\exceptions
 */
class BitrixLeadSendException extends Exception implements IBitrixLeadSendException
{

}
