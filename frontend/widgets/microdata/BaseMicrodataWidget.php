<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 22.05.17
 * Time: 13:07
 */

namespace frontend\widgets\microdata;


use yii\helpers\Html;
use yii\helpers\Json;

class BaseMicrodataWidget extends \yii\base\Widget
{
    /**
     * @var array
     */
    public $rawMicrodataData = [];

    /**
     * @return null|string
     */
    public function run()
    {
        $data = $this->rawMicrodataData;
        if (empty($data)) {
            return null;
        }

        return $this->prepareData($data);
    }

    /**
     * @param array $data
     * @return string
     */
    protected function prepareData(array $data)
    {
        $data = Json::encode($data);

        return Html::script($data, ['type' => 'application/ld+json']);
    }
}