<?php

namespace frontend\widgets;

use common\models\City;
use yii\base\Widget;
use jisoft\sypexgeo\Sypexgeo;

/**
 * Class LocationPopup
 * @package frontend\widgets
 */
class LocationPopup extends Widget
{
    /**
     * @var
     */
    private $location;

    public function init()
    {
        $this->location = City::findBySxgeoCity();
    }

    /**
     * @return string
     */
    public function run()
    {
        $geolocation = \Yii::$app->request->cookies->getValue('geolocation');

        $data = [
            'render' => false
        ];

        if ($this->location) {
            $data = [
                'city' => $this->location,
                'render' => true
            ];
        }

        if (!$geolocation && self::isHomePage()) {
            return $this->render('_locationPopup', $data);
        }

        return false;
    }

    /**
     * Little Hardcode
     */
    public static function isHomePage()
    {
        $currentRoute = \Yii::$app->controller->getRoute();

        return $currentRoute == 'page/page/home';
    }
}