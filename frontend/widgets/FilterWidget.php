<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Url;
use yii\helpers\Json;
use common\models\City;
use common\models\Apartment;
use common\models\ApartmentComplex;
/**
 * Class FilterWidget
 *
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package frontend\widgets
 */
class FilterWidget extends Widget
{
    const FILTER_YOUNG_FAMILY = 1;
    const FILTER_PENSIONER = 2;
    const FILTER_SINGLES = 3;
    const FILTER_BIG_FAMILY = 4;

    /** @var ApartmentComplex complex model. */
    public $complex;
    /** @var string pjax container identifier. */
    public $pjaxContainerID;
    /** @var string form identifier. */
    public $formID = 'filters-form';
    /** @var string form action base url. */
    public $formBaseUrl = '/apartment/apartment-complex/apartments-list';
    /** @var array options passed to Filter javascript plugin. */
    public $clientOptions = [];
    /** @var City current city. */
    protected $city;

    public function run(): string
    { 
        $this->registerClientScripts();
        $selected = request()->get();

        $this->city = $this->complex->city;
        $complexList = $this->makeComplexOptions();
        $complexIDs = array_keys($complexList);
        $roomList = $this->makeRoomsOptions($complexIDs);
        $queueList = $this->makeQueueOptions($complexIDs);
        $floorList = $this->makeFloorOptions($complexIDs);
        $forList = $this->forWhomOptions();
        $minBoundSquare = $this->makeMinSquareBound($complexIDs);
        $maxBoundSquare = $this->makeMaxSquareBound($complexIDs);
        $minBoundPrice = $this->makeMinPriceBound($complexIDs);
        $maxBoundPrice = $this->makeMaxPriceBound($complexIDs);
        $minBoundFloor = $this->makeMinFloorBound($complexIDs);
        $maxBoundFloor = $this->makeMaxFloorBound($complexIDs);
 //dd($queueList);
        return $this->render('_filters', [
            'formID' => $this->formID,
            'formAction' => $this->buildFormAction(),
            'selected' => $selected,
            'complexList' => $complexList,
            'roomList' => $roomList,
            'queueList' => $queueList,
            'floorList' => $floorList,
            'forList' => $forList,
            'isCommercial' => $this->complex->isCommercial(),
            'isResidential' => $this->complex->isResidential(),
            'minBoundSquare' => $minBoundSquare,
            'maxBoundSquare' => $maxBoundSquare,
            'minBoundPrice' => $minBoundPrice,
            'maxBoundPrice' => $maxBoundPrice,
            'minBoundFloor' => $minBoundFloor,
            'maxBoundFloor' => $maxBoundFloor,
            'showPrices' => (bool)$this->city->show_prices,
        ]);
    }

    private function buildFormAction(): string
    {
        return Url::to([$this->formBaseUrl, 'alias' => $this->complex->alias, 'type' => $this->complex->type]);
    }

    protected function registerClientScripts()
    {
        $id = $this->getId();
        $clientOptions = $this->prepareClientOptions();
        $this->getView()->registerJs("var filter_$id = new Filter($clientOptions);filter_$id.init();");
    }

    private function prepareClientOptions(): string
    {
        return Json::encode(merge($this->clientOptions, [
            'form' => '#' . $this->formID,
            'pjaxContainer' => '#' . $this->pjaxContainerID
        ]));
    }

    protected function makeComplexOptions(): array
    {
        return ApartmentComplex::find()
            ->alias('ac')
            ->select(['ac.label'])
            ->andWhere([
                'ac.published' => ApartmentComplex::IS_PUBLISHED,
                'ac.type' => $this->complex->type,
                'ac.city_id' => $this->complex->city_id,
            ])
            ->innerJoinWith('apartments')
            ->indexBy('id')->column();
    }

    protected function makeRoomsOptions(array $complexes): array
    {
        return Apartment::find()
            ->alias('a')
            ->select(['a.rooms_number'])
            ->where([
                'a.published' => Apartment::IS_PUBLISHED,
                'a.type' => $this->complex->type,
                'a.apartment_complex_id' => empty($complexes) ? (array) $this->complex->id : $complexes,
            ])
            ->andWhere(['not', ['a.rooms_number' => null]])
            ->andWhere(['!=', 'a.rooms_number', 0])
            ->distinct()
            ->orderBy(['a.rooms_number' => SORT_ASC])
            ->column();
    }

    protected function makeQueueOptions(array $complexes): array
    {
    	
        return Apartment::find()
            ->alias('a')
            ->select(['a.queue'])
            ->where([
                'a.published' => Apartment::IS_PUBLISHED,
                'a.type' => $this->complex->type,
                'a.apartment_complex_id' => empty($complexes) ? (array) $this->complex->id : $complexes,
            ])
            ->andWhere(['not', ['a.queue' => 0]])
            //->andWhere(['!=', 'a.queue', 0])
            ->distinct()
            ->orderBy(['a.queue' => SORT_ASC])
            ->column();
    }

    protected function makeFloorOptions(array $complexes): array
    {
        return Apartment::find()
            ->alias('a')
            ->select(['a.floor'])
            ->where([
                'a.published' => Apartment::IS_PUBLISHED,
                'a.type' => $this->complex->type,
                'a.apartment_complex_id' => empty($complexes) ? (array) $this->complex->id : $complexes,
            ])
            ->andWhere(['not', ['a.floor' => null]])
            ->distinct()
            ->orderBy(['a.floor' => SORT_ASC])
            ->column();
    }

    protected function makeMinSquareBound(array $complexes): int
    {
        return floor((float)Apartment::find()
            ->alias('a')
            ->select(['a.rooms_number'])
            ->where([
                'a.published' => Apartment::IS_PUBLISHED,
                'a.type' => $this->complex->type,
                'a.apartment_complex_id' => empty($complexes) ? (array) $this->complex->id : $complexes,
            ])
            ->min('a.total_area'));
    }

    protected function makeMaxSquareBound(array $complexes): int
    {
        return ceil((float)Apartment::find()
            ->alias('a')
            ->select(['a.rooms_number'])
            ->where([
                'a.published' => Apartment::IS_PUBLISHED,
                'a.type' => $this->complex->type,
                'a.apartment_complex_id' => empty($complexes) ? (array) $this->complex->id : $complexes,
            ])
            ->max('a.total_area'));
    }

    protected function makeMinPriceBound(array $complexes): int
    {
        return (int)Apartment::find()
            ->alias('a')
            ->select(['a.price_from'])
            ->where([
                'a.published' => Apartment::IS_PUBLISHED,
                'a.type' => $this->complex->type,
                'a.apartment_complex_id' => empty($complexes) ? (array) $this->complex->id : $complexes,
            ])
            ->min('a.price_from');
    }

    protected function makeMaxPriceBound(array $complexes): int
    {
        return (int)Apartment::find()
            ->alias('a')
            ->select(['a.price_from'])
            ->where([
                'a.published' => Apartment::IS_PUBLISHED,
                'a.type' => $this->complex->type,
                'a.apartment_complex_id' => empty($complexes) ? (array) $this->complex->id : $complexes,
            ])
            ->max('a.price_from');
    }

    protected function makeMinFloorBound(array $complexes): int
    {
        return (int)Apartment::find()
            ->alias('a')
            ->select(['a.floor'])
            ->where([
                'a.published' => Apartment::IS_PUBLISHED,
                'a.type' => $this->complex->type,
                'a.apartment_complex_id' => empty($complexes) ? (array) $this->complex->id : $complexes,
            ])
            ->min('a.floor');
    }

    protected function makeMaxFloorBound(array $complexes): int
    {
        return (int)Apartment::find()
            ->alias('a')
            ->select(['a.floor'])
            ->where([
                'a.published' => Apartment::IS_PUBLISHED,
                'a.type' => $this->complex->type,
                'a.apartment_complex_id' => empty($complexes) ? (array) $this->complex->id : $complexes,
            ])
            ->max('a.floor');
    }

    protected function forWhomOptions()
    {
        return [
            self::FILTER_YOUNG_FAMILY => [
                'icon' => '#icon-young-family',
                'label' => t('_Young family', 'apartment-list'),
                'condition' => ['<', 'a.rooms_number', 3],
            ],
            self::FILTER_PENSIONER => [
                'icon' => '#icon-old',
                'label' => t('_Pensioner', 'apartment-list'),
                'condition' => ['<', 'a.rooms_number', 2],
            ],
            self::FILTER_SINGLES => [
                'icon' => '#icon-man',
                'label' => t('_Singles', 'apartment-list'),
                'condition' => ['<', 'a.rooms_number', 2],
            ],
            self::FILTER_BIG_FAMILY => [
                'icon' => '#icon-big-family',
                'label' => t('_Big family', 'apartment-list'),
                'condition' => ['>=', 'a.rooms_number', 3],
            ],
        ];
    }

    
}
