<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.04.18
 * Time: 13:26
 */

namespace frontend\widgets;


use common\components\BuilderModel;
use common\models\builder\BlogDoubleImageBuilderModel;
use common\models\builder\BlogHtmlBuilderModel;
use common\models\builder\BlogImageWithTextBuilderModel;
use common\models\builder\BlogSliderBuilderModel;
use common\models\builder\BlogYoutubeVideoBuilderModel;
use yii\base\Widget;

class BuilderContentWidget extends Widget
{
    /** @var BuilderModel[] */
    public $content = [];

    public function run()
    {
        $output = [];
        foreach ($this->content as $widget) {
            $widget->proccessFiles();
            $widget->proccessTranslation();
            $class = get_class($widget);
            switch ($class) {
                case BlogDoubleImageBuilderModel::class:
                    $output[] = $this->render('_doubleImage', ['model' => $widget]);
                    break;
                case BlogHtmlBuilderModel::class:
                    $output[] = $this->render('_html', ['model' => $widget]);
                    break;
                case BlogImageWithTextBuilderModel::class:
                    $output[] = $this->render('_imageWithText', ['model' => $widget]);
                    break;
                case BlogSliderBuilderModel::class:
                    $output[] = $this->render('_slider', ['model' => $widget]);
                    break;
                case BlogYoutubeVideoBuilderModel::class:
                    if (!empty($widget->getYoutubeId())) {
                        $output[] = $this->render('_youtube', ['model' => $widget]);
                    }
                    break;
                default:
                    break;
            }
        }

        return implode("\n", $output);
    }
}