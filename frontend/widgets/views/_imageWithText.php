<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.04.18
 * Time: 13:30
 *
 * @var $model \common\models\builder\BlogImageWithTextBuilderModel
 */
//\metalguardian\fileProcessor\helpers\FPM::src($model->getFirstFile('image')->id ?? null, 'article', 'view')
?>
<div class="article__content wow fadeInUp" data-wow-delay="0.5s">
    <!-- IMG DESCRIPTION -->
    <div class="img-description">
        <img alt="<?= $model->text ?>" src="<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($model->getFirstFile('image')->id) ?>">
        <p> <?= $model->text ?> </p>
    </div>
    <!-- /IMG DESCRIPTION -->
</div>
