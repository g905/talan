<?php

use common\helpers\SiteUrlHelper;
use common\models\SxgeoCities;

?>
<div class="popup header__popup geolocation_popup" data-popup-city data-geolocation-url="<?= SiteUrlHelper::createGeolocationUrl() ?>">
    <div class="popup__inner">
        <?php if ($render) : ?>
            <div class="popup__question">
                <?= Yii::t('app', '_Geo_Your_City') ?> -
                <span class="popup__city"><?= $city->label ?></span>?
            </div>
        <?php else : ?>
            <div class="popup__question">
                <?= Yii::t('app', '_Geo_Not_Found_City') ?>
            </div>
        <?php endif; ?>
        <div class="popup__answers">
            <?php if ($render) : ?>
                <div class="view-all">
                    <button class="more view-all__more ajax_geolocation" data-type="<?= SxgeoCities::TYPE_LOCATION ?>" data-popup-city-close><?= Yii::t('app', '_Geo_Yes') ?></button>
                </div>
                <div class="view-all">
                    <button class="more view-all__more ajax_geolocation" data-type="<?= SxgeoCities::TYPE_PRELOAD ?>" data-popup-city-more><?= Yii::t('app', '_Geo_No') ?></button>
                </div>
            <?php else : ?>
                <div class="view-all">
                    <button class="more view-all__more ajax_geolocation" data-type="<?= SxgeoCities::TYPE_PRELOAD ?>" data-popup-city-more><?= Yii::t('app', '_Geo_Show_List') ?></button>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="popup__overlay" data-popup-city-overlay></div>
</div>
