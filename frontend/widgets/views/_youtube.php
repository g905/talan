<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.04.18
 * Time: 13:30
 *
 * @var $model \common\models\builder\BlogYoutubeVideoBuilderModel
 */
?>
<div class="article__content wow fadeInUp" data-wow-delay="0.5s">
    <!-- VIDEO -->
    <div class="video">
        <div data-youtube="<?= $model->getYoutubeId() ?>" data-bg-src="<?= \metalguardian\fileProcessor\helpers\FPM::src($model->getFirstFile('image')->id ?? null, 'builder', 'youtube') ?>" data-bg-size="cover" data-bg-pos="center">
            <div class="play video__play">
                <div class="play__blip play__blip_hover"></div>
                <div class="play__blip play__blip_click"></div>
            </div>
            <iframe src="" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <!-- /VIDEO -->
</div>
