<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.04.18
 * Time: 13:30
 *
 * @var $model \common\models\builder\BlogSliderBuilderModel
 */

use frontend\helpers\ImgHelper;

?>
<div class="article__content wow fadeInUp" data-wow-delay="0.5s">
    <!-- SLIDER -->
    <div class="sliders">
        <div class="slider-main">
            <div class="slider-main__wrap" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "arrows": false, "asNavFor": ".slider-nav, .slider-description", "fade": true}'>
                <?php foreach($model->images as $image) : ?>

                    <?php
                    // $src = \metalguardian\fileProcessor\helpers\FPM::src($image->id ?? null ?? null, 'builder', 'slider');
                    $src = \metalguardian\fileProcessor\helpers\FPM::originalSrc($image->id );
                    ?>
                <div data-bg-src="<?= $src ?>" data-bg-pos="center" data-bg-size="cover"></div>
                <?php endforeach; ?>
            </div>
            <img class="slider-main__zoom-icon" src="/st/img/zoom_icon.png" alt=""> </div>
        <div class="slider-nav" data-slick='{"slidesToShow": 5, "slidesToScroll": 1, "arrows": false, "asNavFor": ".slider-main__wrap, .slider-description", "focusOnSelect": true,	"responsive":	[{"breakpoint": 1201, "settings": {"slidesToShow": 4}}, {"breakpoint": 768, "settings": {"slidesToShow": 2}}]}'>
            <?php foreach($model->images as $imageFull) : ?>
                <?php
                $src = \metalguardian\fileProcessor\helpers\FPM::originalSrc($imageFull->id );
                ?>
                <div>
                <div data-bg-src="<?= $src ?>" data-bg-pos="center" data-bg-size="cover"></div>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="slider-description" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "arrows": false, "asNavFor": ".slider-nav, .slider-main__wrap", "fade": true}'>
            <?php foreach($model->images as $imageAlt) : ?>
            <?php if (isset($imageAlt->metaData)) : ?>
            <p> <?= $imageAlt->metaData->alt ?> </p>
            <?php else: ?>
            <p></p>
            <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
    <!-- /SLIDER -->
</div>
