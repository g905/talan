<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.04.18
 * Time: 13:30
 *
 * @var $model \common\models\builder\BlogDoubleImageBuilderModel
 */

//\metalguardian\fileProcessor\helpers\FPM::src($model->getFirstFile('image')->id ?? null ?? null, 'builder', 'doubleimage')
//\metalguardian\fileProcessor\helpers\FPM::src($model->getFirstFile('image2')->id ?? null ?? null, 'builder', 'doubleimage')
?>
<div class="article__content wow fadeInUp" data-wow-delay="0.5s">
    <!-- IMG-SPLIT -->
    <div class="imgs">
        <table>
            <tr>
                <td>
                    <img src="<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($model->getFirstFile('image')->id) ?>">
                <td>
                    <img src="<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($model->getFirstFile('image2')->id) ?>">
            </tr>
        </table>
    </div>
    <!-- /IMG-SPLIT -->
</div>
