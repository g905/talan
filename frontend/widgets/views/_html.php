<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.04.18
 * Time: 13:30
 *
 * @var $model \common\models\builder\BlogHtmlBuilderModel
 */
?>
<div class="article__content wow fadeInUp" data-wow-delay="0.5s">
    <?= $model->text ?>
</div>
