<?php

namespace frontend\controllers;

use common\components\Sypexgeo;
use common\helpers\SiteUrlHelper;
use common\models\City;
use common\models\SxgeoCities;
use Yii;
use yii\helpers\Url;
use yii\web\Cookie;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use common\models\Robots;
use frontend\components\FrontendController;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * Action for testing purposes.
     *
     * @todo remove on last release.
     * @return string|array
     */
    public function actionTest()
    {
        $this->layout = 'test';

        return $this->render('test');
    }

    /**
     * Renders index page
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Renders maintenance page
     *
     * @return string
     */
    public function actionMaintenance()
    {
        $this->layout = false;

        return $this->render('maintenance');
    }

    /**
     * Generates robots.txt file
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionRobots()
    {
        /* @var Robots $robots */
        $robots = Robots::find()->one();

        if (!$robots) {
            throw new NotFoundHttpException();
        }

        $this->layout = false;

        $response = Yii::$app->getResponse();
        $response->format = Response::FORMAT_RAW;
        $headers = $response->headers;
        $headers->add('Content-Type', 'text/plain');

        $text = $robots->text;
        $text .= "\nSitemap: " . Url::to(['/sitemap/default/index'], true);

        return $this->renderContent($text);
    }

    public function actionGeolocation()
    {
        $request = Yii::$app->request->post();

        if ($request) {
            $city = City::findBySxgeoCity();
            $currentCity = City::getUserCity();

            $geolocation = Yii::$app->request->cookies->getValue('geolocation');

            if (empty($geolocation)) {
                $cookie = new Cookie([
                    'name' => 'geolocation',
                    'value' => $city ? $city->alias : SxgeoCities::TYPE_PRELOAD,
                    'domain' => Yii::$app->params['cookieDomain'] ?? '',
                ]);
                Yii::$app->response->cookies->add($cookie);
            }

            if ($request['type'] == SxgeoCities::TYPE_LOCATION) {
                if ($city->id != $currentCity->id) {
                    return $this->redirect(SiteUrlHelper::createHomePageUrl(['city' => $city->alias]));
                }
            }
        }
    }
	/**
	 * Метод сохраняет в "куку" значение о том, что баннер был закрыт
	 */
	public function actionCloseBanner() {

		if(Yii::$app->request->isAjax && Yii::$app->request->isPost) {

			Yii::$app->response->cookies->add(new Cookie([
				'name' => 'tlnMobileBanner',
				'value' => '1',
			]));

			Yii::$app->response->format = Response::FORMAT_JSON;
			return [
				'code' => 1,
			];
		}
		return null;
	}
}
