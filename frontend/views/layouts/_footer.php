<?php

use common\helpers\ConfigHelper;

/**
 * @var $this \yii\web\View
 */
$footerModals = obtain('footerModals', $this->params);
?>

<!-- MODALS -->
<div class="modals" data-modals>
    <?php if (isset(Yii::$app->params['footerPopup'])) : ?>
        <?= Yii::$app->params['footerPopup'] ?>
    <?php else : ?>
        <div class="modal modals__modal popup-replace" data-modal="call-back">
            <div class="modal__outer">
                <div class="modal__overlay" data-modal-close></div>
                <div class="modal__inner">
                    <div class="modal__content">
                        <div class="popup">
                            <button class="modal__close" data-modal-close></button>
                            <h5 class="h5 popup__h5 ">Обратный звонок</h5>
                            <form class="form popup__form" onsubmit="<?= ConfigHelper::getGlobalCallbackOnSubmit() ?>">
                                <input type="text" class="form__input" placeholder="Ваше имя">
                                <input type="tel" class="form__input m_phone" placeholder="Телефон">
                                <button class="button button_green form__button" data-modal-close data-modal-open="thanks">
                                    Отправить
                                    <span class="button__blip button__blip_hover"></span>
                                    <span class="button__blip button__blip_click"></span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($footerModals) : ?>
        <?php foreach ($footerModals as $footerModal) : ?>
            <?= $footerModal ?>
        <?php endforeach ?>
    <?php endif ?>

    <!--<div class="modal modals__modal" data-modal="thanks">
        <div class="modal__outer">
            <div class="modal__overlay" data-modal-close></div>
            <div class="modal__inner">
                <div class="modal__content">
                    <div class="popup">
                        <h5 class="h5 popup__h5">Спасибо</h5>
                        <div class="popup__text">
                            <p>Ваша заявка успешно отправлена. Наш менеджер свяжется с Вами в ближайшее время.</p>
                        </div>
                        <button class="button button_green popup__button" data-modal-close>
                            Закрыть
                            <span class="button__blip button__blip_hover"></span>
                            <span class="button__blip button__blip_click"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
</div>
<!-- /MODALS -->





