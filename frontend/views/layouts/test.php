<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $content string
 */
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= app()->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=<?= app()->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
    <link rel="shortcut icon" href="<?= request()->baseUrl ?>/favicon.ico?v=2.0" type="image/x-icon">
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <?= $content ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
