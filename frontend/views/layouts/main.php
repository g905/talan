<?php

use yii\helpers\Html;
use yii\widgets\Spaceless;
use common\models\City;
use frontend\assets\AppAsset;
use frontend\helpers\MetaHelper;
use frontend\assets\CdnAsset;

/**
 * @var $this \yii\web\View
 * @var $content string
 */
AppAsset::register($this);
CdnAsset::register($this);
MetaHelper::registerYandexValidation(City::getUserCity()->id);

?>

<?php $this->beginPage() ?>
<?php Spaceless::begin() ?>
<!DOCTYPE html>
<html lang="<?= app()->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=<?= app()->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="google-play-app" content="app-id=ru.napoleonit.talan_client">
    <meta name="apple-itunes-app" content="app-id=ru.napoleonit.talan_client">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
    <link rel="shortcut icon" href="<?= request()->baseUrl ?>/favicon.ico?v=2.0" type="image/x-icon">
    <meta name="geo.placename" content="ул. Революции, 21, Пермь, Пермский край, Россия, 614007" />
    <meta name="geo.position" content="58.0059260;56.2552470" />
    <meta name="geo.region" content="RU-Пермский край" />
    <meta name="ICBM" content="58.0059260, 56.2552470" />

    <link rel="stylesheet" type="text/css" href="/css/electron.css">

    <?php $this->head() ?>

    <?= City::getUserCity()->head_script; ?>

    <?= \common\models\ApartmentComplex::getHeadScript()?>

    <?= config()->get('endHead') ?>

</head>
<body data-page="<?= obtain('data-page', $this->params) ?>">
    <?php $this->beginBody() ?>
    <?= $this->render('_svg') ?>
    <?= $this->render('_preloader') ?>
    <?php if (!empty($this->params['data-pages'])) : ?>
    <main data-pages="<?= $this->params['data-pages'] ?>">
    <?php endif; ?>
    <?= $this->render('_header') ?>
    <?= $content ?>
    <?php if (!empty($this->params['data-pages'])) : ?>
    </main>
    <?php endif; ?>
    <?= $this->render('_footer') ?>
    <?php $this->endBody() ?>
    <?= config()->get('endBody') ?>
    <?php
	$hideMobiTopBanner = false;
	$bannerCookie = Yii::$app->request->cookies->get('tlnMobileBanner');
	// нужно ли отображать баннер для моб. устройства
	if(!empty($bannerCookie) && $bannerCookie->value == '1') {
		$hideMobiTopBanner = true;
	}
	echo Html::hiddenInput('tlnHideMobiTopBanner', ($hideMobiTopBanner ? '1' : '0'), ['id' => 'tlnHideMobiTopBanner']);
	echo Html::hiddenInput('tlnmdIsMobile', (Yii::$app->mobileDetect->isMobile() ? '1': '0'), ['id' => 'tlnmdIsMobile']);
	echo Html::hiddenInput('tlnmdIsAndroid', (Yii::$app->mobileDetect->isAndroidOS() ? '1': '0'), ['id' => 'tlnmdIsAndroid']);
	echo Html::hiddenInput('tlnmdIsIos', (Yii::$app->mobileDetect->isiOS() ? '1': '0'), ['id' => 'tlnmdIsIos']);
    $this->registerJsFile('/js/frontend.js');
    $this->registerJsFile('/js/electron.js');
    ?>
</body>
</html>
<?php Spaceless::end() ?>
<?php $this->endPage(); ?>

