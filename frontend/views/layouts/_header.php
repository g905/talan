<?php

use common\models\City;
use common\helpers\ConfigHelper;
use common\helpers\SiteUrlHelper;
use frontend\modules\general\widgets\CommonCityScriptsWidget;
use frontend\modules\page\widgets\GetHeaderMenuWidget;
use frontend\modules\page\widgets\HeaderPageMenuWidget;
use frontend\modules\page\widgets\SelectCityWidget;
use frontend\widgets\LocationPopup;

/**
 * @var $this \yii\web\View
 */
?>

<div class="panel-menu" data-panel-menu>
    <div class="panel-menu__overlay"></div>
    <div class="panel-menu__inner">
        <button class="panel-menu__close" data-close-menu></button>
        <?= SelectCityWidget::widget([
            'currentCity' => $this->params['currentCity'] ?? City::getUserCity(),
            'position' => SelectCityWidget::POSITION_HEADER_PANEL
        ]) ?>
        <?= GetHeaderMenuWidget::widget([
            'currentCity' => $this->params['currentCity'] ?? City::getUserCity(),
            'position' => GetHeaderMenuWidget::POSITION_FIXED_PANEL
        ]) ?>
    </div>
</div>

<!-- fixed top header -->
<header class="header header_black" id="header-black">
    <div class="wrap header__wrap wrap_mobile_full">
        <a href="<?= SiteUrlHelper::createHomePageUrl()?>" class="logo header__logo">
            <svg class="logo__svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo_new"></use>
            </svg>
        </a>
        <?= SelectCityWidget::widget([
            'currentCity' => $this->params['currentCity'] ?? City::getUserCity(),
            'position' => SelectCityWidget::POSITION_HEADER_FIXED
        ])?>
        <?= GetHeaderMenuWidget::widget([
            'currentCity' => $this->params['currentCity'] ?? City::getUserCity(),
            'position' => GetHeaderMenuWidget::POSITION_TOP
        ])?>
        <button class="btn-menu header__btn-menu" data-open-menu>
            <span class="btn-menu__element"></span>
            <span class="btn-menu__element"></span>
            <span class="btn-menu__element"></span>``
        </button>
        <button class="call header__call ajax-link" data-href="<?=SiteUrlHelper::createFormCallbackPopupUrl() ?>">
            <svg class="call__svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-phone"></use>
            </svg>
        </button>
        <a href="tel:<?= ConfigHelper::getHeaderPhoneFormatted($this->params['currentCity'] ?? City::getUserCity())?>" class="header__phone">
            <?= ConfigHelper::getHeaderPhone($this->params['currentCity'] ?? City::getUserCity())?>
        </a>
        <?= LocationPopup::widget() ?>
    </div>
    <?php if (isset($this->params['headerMenu'])) : ?>
        <?= HeaderPageMenuWidget::widget(['menu' => $this->params['headerMenu'], 'menuType' => $this->params['headerMenuType']]) ?>
    <?php endif ?>
    <?= isset($this->params['headerArticle']) ? $this->params['headerArticle'] : '' ?>
    <?= isset($this->params['headerComplexList']) ? $this->params['headerComplexList'] : '' ?>


    <?php if (isset($this->params['breadcrumbsContent'])) : ?>
        <div class="header__line"></div>
        <div class="header__row">
            <div class="wrap wrap_mobile_full">
                <?= $this->params['breadcrumbsContent'] ?>
            </div>
        </div>
    <?php else: ?>
        <div class="wrap header__wrap wrap_mobile_full">
            <div class="header__line"></div>
        </div>
    <?php endif ?>
</header>
<?= CommonCityScriptsWidget::widget(['currentCity' => $this->params['currentCity'] ?? City::getUserCity()]) ?>
