<?php

use yii\helpers\Html;
use yii\widgets\Spaceless;
use common\models\City;
use frontend\assets\AppAsset;
use frontend\helpers\MetaHelper;

/**
 * @var $this \yii\web\View
 * @var $content string
 */
AppAsset::register($this);
//MetaHelper::registerYandexValidation(City::getUserCity()->id);
?>

<?php $this->beginPage() ?>
<?php Spaceless::begin() ?>
<!DOCTYPE html>
<html lang="<?= app()->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=<?= app()->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
    <link rel="shortcut icon" href="<?= request()->baseUrl ?>/favicon.ico?v=2.0" type="image/x-icon">

    <meta name="geo.placename" content="ул. Революции, 21, Пермь, Пермский край, Россия, 614007" />
    <meta name="geo.position" content="58.0059260;56.2552470" />
    <meta name="geo.region" content="RU-Пермский край" />
    <meta name="ICBM" content="58.0059260, 56.2552470" />

    <?php $this->head() ?>
    <?= config()->get('endHead') ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <?= $this->render('_svg') ?>
    <?= $this->render('_preloader') ?>
    <?= $content ?>
    <?php $this->endBody() ?>
    <?= config()->get('endBody') ?>
</body>
</html>
<?php Spaceless::end() ?>
<?php $this->endPage() ?>
