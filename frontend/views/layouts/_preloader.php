<?php
/**
 * @var $this yii\web\View
 */
?>

<!--BLOCK PRELOADER-->
<div class="preloader" id="preloader">
    <svg class="preloader__logo icon-logo_talan_short">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo_talan_short"></use>
    </svg>
</div>
<!--/BLOCK PRELOADER-->
