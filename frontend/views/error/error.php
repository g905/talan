<?php

/**
 * @var $this yii\web\View
 * @var $name string
 * @var $message string
 * @var $exception Exception
 */
$this->title =  "Ошибка: {$exception->statusCode}";
?>

<section class="about">
    <div class="wrap">
        <div class="about__inner">
            <div class="about__content">
                <h1>Ошибка <?= $exception->statusCode ?></h1>
                <?php
                if (! empty($exception->getMessage())) echo $exception->getMessage();
                ?>
                <p>Этой страницы не существует.</p>
                <p>Скорее всего, вы перешли по неверной ссылке.</p>
                <p><a href="/">Вернуться на главную</a></p>
            </div>
        </div>
        <div class="about__img" data-bg-src="/img/about-img.jpg" data-bg-pos="center bottom" data-bg-size="cover"></div>
    </div>
</section>
