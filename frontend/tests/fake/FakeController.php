<?php
namespace frontend\tests\fake;

use yii\web\Controller;

/**
 * Fake controller for mock in tests
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class FakeController extends Controller
{

}
