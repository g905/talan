<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.04.18
 * Time: 17:45
 */

namespace frontend\helpers;


use common\models\FpmFile;

trait StaticPageRelationTrait
{
    /**
     * @var array
     */
    protected $_relationFileModels = [];

    /**
     * @param $constantName
     * @param bool $isMultiple
     * @return mixed
     */
    public function getFileRelation($constantName, $isMultiple = false)
    {
        if (array_key_exists($constantName, $this->_relationFileModels)) {
            return $this->_relationFileModels[$constantName];
        }

        $result = FpmFile::find()
            ->alias('file')
            ->joinWith([
                'entityToFiles' => function(\yii\db\ActiveQuery $query) {
                    return $query->alias('entity');
                }
            ])
            ->where([
                'entity.entity_model_name' => static::formName(),
                'entity.attribute' => $constantName
            ])
            ->orderBy(['entity.position' => SORT_DESC]);
        if ($isMultiple) {
            $this->_relationFileModels[$constantName] = $result->all();
        } else {
            $this->_relationFileModels[$constantName] = $result->one();
        }

        return $this->_relationFileModels[$constantName];
    }

    /**
     * @param $constantName
     * @param bool $isMultiple
     * @return array|null
     */
    public function getFileRelationFileId($constantName, $isMultiple = false)
    {
        $models = $this->getFileRelation($constantName, $isMultiple);

        if ($isMultiple) {
            $ids = [];
            foreach ($models as $model) {
                $ids[] = $model->id;
            }
            return $ids;
        } else {
            return $models->id ?? null;
        }
    }
}