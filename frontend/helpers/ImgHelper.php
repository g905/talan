<?php
namespace frontend\helpers;

use metalguardian\fileProcessor\helpers\FPM;
use yii\helpers\Html;

/**
 * Class ImgHelper
 * @package backend\components
 */
class ImgHelper
{

    public static function getImageSrc($model, $imageModule = false, $imageSize = false, $relation = 'mainImage', $emptyImagePath = '')
    {
//        return (isset($model->{$relation}) && $model->{$relation}->file_id)
//            ? FPM::src($model->{$relation}->file_id, $imageModule, $imageSize)
//            : $emptyImagePath;

        return (isset($model->{$relation}) && $model->{$relation}->file_id)
            ? FPM::originalSrc($model->{$relation}->file_id)
            : $emptyImagePath;

    }

    public static function getFileSrc($model, $relation = 'mainImage')
    {
        return (isset($model->{$relation}) && $model->{$relation}->file_id)
            ? FPM::originalSrc($model->{$relation}->file_id)
            : '';
    }

    public static function getImageSrcFromEtF($entityToFile, $imageModule = false, $imageSize = false)
    {
//        return (isset($entityToFile) && $entityToFile->file_id)
//            ? FPM::src($entityToFile->file_id, $imageModule, $imageSize)
//            : '';
        return (isset($entityToFile) && $entityToFile->file_id)
            ? FPM::originalSrc($entityToFile->file_id)
            : '';

    }

    /**
     * @param $imageId
     * @return bool
     */
    public static function isImageExistInFpm($imageId)
    {
        return (boolean) \metalguardian\fileProcessor\models\File::findOne((int) $imageId);
    }

    /**
     * @param $model
     * @param $relation
     * @return bool
     */
    public static function isImageExist($model, $relation)
    {
        return (isset($model->{$relation}) && $model->{$relation}->file_id);
    }

    /**
     * @param $model
     * @param $relation
     * @param $size
     * @param array $htmlOptions
     * @return null|string
     */
    public static function getImageSvg($model, $relation, $size, $htmlOptions = [])
    {
        return static::isImageExist($model, $relation)
            ? Html::tag('image', '', array_merge(['xlink:href' => FPM::src($model->{$relation}->file_id, $size[0] , $size[1])], $htmlOptions))
            : null;

    }

}
