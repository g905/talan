<?php
/**
 * @author walter
 */

namespace frontend\helpers;

use yii\helpers\BaseHtml;

/**
 * Class ExtendedHtml
 * @package frontend\helpers
 */
class ExtendedHtml extends BaseHtml
{
    /**
     * Generates ajax hyperlink tag
     *
     * @param string $text
     * @param array|string|null $url
     * @param array $options
     * @return string
     */
    public static function ajaxLink($text, $url = null, $options = [])
    {
        self::addCssClass($options, 'ajax-link');
        $options['rel'] = 'nofollow';
        $link = parent::a($text, $url, $options);
        return self::wrapInNoindexTag($link);
    }

    /**
     * Generates a hyperlink tag to another sites
     *
     * @param string $text
     * @param array|string|null $url
     * @param array $options
     * @return string
     */
    public static function externalLink($text, $url = null, $options = [])
    {
        $options['rel'] = 'nofollow';
        $link = parent::a($text, $url, $options);
        return self::wrapInNoindexTag($link);
    }

    /**
     * Returns HTML wrapped in to no index block
     *
     * @param string $html
     * @return string
     */
    public static function wrapInNoIndexTag($html)
    {
        return '<!--noindex-->' . $html . '<!--/noindex-->';
    }

    /**
     * Generates a callto hyperlink
     *
     * @param string $tel
     * @param null|string $text
     * @param array $options
     * @return string
     */
    public static function callto($tel, $text = null, $options = [])
    {
        return self::a(
            ($text === null ? $tel : $text),
            "tel:$tel",
            $options
        );
    }

}