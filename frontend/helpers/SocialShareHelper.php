<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.04.18
 * Time: 13:07
 */

namespace frontend\helpers;


use yii\helpers\Url;
use Yii;

class SocialShareHelper
{
    /**
     * @return string
     */
    public static function getOdnoklasnikiShareUrl()
    {
        $resultUrl = Url::canonical();

        return 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=' . $resultUrl;
    }

    /**
     * @return string
     */
    public static function getVkShareUrl($imagePath, $title)
    {
        $resultUrl = Url::canonical();

        return 'http://vk.com/share.php?url=' . $resultUrl . '&title=' . $title . '&image' . $imagePath;
    }

    /**
     * @return string
     */
    public static function getFacebookShareUrl()
    {
        $resultUrl = Url::canonical();

        return 'https://www.facebook.com/sharer/sharer.php?u=' . $resultUrl;
    }
}