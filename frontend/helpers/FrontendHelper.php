<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 20.12.2017
 * Time: 14:37
 */

namespace frontend\helpers;


class FrontendHelper
{
    public static function formatBreadcrumbs($breadcrumbs)
    {
        for ($i = 0; $i < count($breadcrumbs); $i++) {
            if (is_array($breadcrumbs[$i])) {
                $breadcrumbs[$i]['class'] = 'bread-crumbs__link';
            }
        }

        return $breadcrumbs;
    }

    public static function isNullOrEmpty($val)
    {
        if ((!isset($val)) || ($val == '')) {
            return true;
        }
        return false;
    }

    public static function formatPhone($phone)
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);
        return $phone;
    }

    /**
     * Extract video ID from valid Youtube's video URL
     * Return null if URL have invalid format (format valid at 30.09.2015)
     *
     * @return mixed
     * */
    public static function extractYoutubeVideoId($url)
    {
        $re = "/^(?:http(?:s)?:\\/\\/)?(?:www\\.)?(?:m\\.)?(?:youtu\\.be\\/|youtube\\.com\\/(?:(?:watch)?\\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\\/))([^\\?&\\\"'>]+)/";
        $str = $url;
        preg_match($re, $str, $matches);
        return isset($matches[1]) ? $matches[1] : null;
    }
}
