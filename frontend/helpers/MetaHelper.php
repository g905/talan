<?php

namespace frontend\helpers;

/**
 * Class MetaHelper
 *
 * @package frontend\helpers
 */
class MetaHelper
{
    public static function registerYandexValidation($cityId)
    {
        $contents = [
            1 => '76d9baf35b8c090e', // Ижевск
            3 => '0b0e324efc13fee8', // Перьм
            4 => 'aaa04a291da79a72', // Наб. челны
            5 => '6854d48532c5fdd7', // Тюмень
            6 => '4f26c669774f29ca', // Тверь
        ];

        app()->getView()->registerMetaTag([
            'name' => 'yandex-verification',
            'content' => obtain($cityId, $contents, '30d6e1bc709e38af')
        ]);
    }
}
