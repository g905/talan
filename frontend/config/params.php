<?php
return [
    'adminEmail' => 'admin@example.com',
//    'notification_emails' => 'biobox@yandex.ru',
    'notification_emails' => 'vlook.reg@gmail.com',
    'agreementCheckboxText' => 'Я <a href="/files/agreement.docx" target="_blank">согласен</a> на обработку моих персональных данных.
                                С <a href="/files/privacy-policy.docx" target="_blank">Политикой</a> в отношении обработки персональных данных ознакомлен и согласен.',
];
