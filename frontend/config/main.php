<?php

use frontend\components\BitrixConnect;
use frontend\components\BitrixIntegration;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$config =  [
    'id' => 'app-frontend',
    'name' => 'Project',
    'language' => 'ru',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'fileProcessor', 'config', 'siteAnalytics', 'bitrixIntegration'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => \yii\helpers\ArrayHelper::merge([
        'sitemap' => [
            'class' => 'himiklab\sitemap\Sitemap',
            'models' => [
                //your models
                /*
                'app\modules\news\models\News',
                //or configuration for creating a behavior
                [
                    'class' => 'app\modules\news\models\News',
                    'behaviors' => [
                        'sitemap' => [
                            'class' => SitemapBehavior::className(),
                            'scope' => function ($model) {
                                    $model->select(['url', 'lastmod']);
                                    $model->andWhere(['is_deleted' => 0]);
                                },
                            'dataClosure' => function ($model) {
                                    return [
                                        'loc' => Url::to($model->url, true),
                                        'lastmod' => strtotime($model->lastmod),
                                        'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                                        'priority' => 0.8
                                    ];
                                }
                        ],
                    ],
                ],*/
            ],
            'urls' => [
                [
                    'loc' => '/',
                    'lastmod' => time(),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_DAILY,
                    'priority' => 0.8
                ],
                // your additional urls
                /*[
                    'loc' => '/news/index',
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_DAILY,
                    'priority' => 0.8,
                    'news' => [
                        'publication'   => [
                            'name'          => 'Example Blog',
                            'language'      => 'en',
                        ],
                        'access'            => 'Subscription',
                        'genres'            => 'Blog, UserGenerated',
                        'publication_date'  => 'YYYY-MM-DDThh:mm:ssTZD',
                        'title'             => 'Example Title',
                        'keywords'          => 'example, keywords, comma-separated',
                        'stock_tickers'     => 'NASDAQ:A, NASDAQ:B',
                    ],
                    'images' => [
                        [
                            'loc'           => 'http://example.com/image.jpg',
                            'caption'       => 'This is an example of a caption of an image',
                            'geo_location'  => 'City, State',
                            'title'         => 'Example image',
                            'license'       => 'http://example.com/license',
                        ],
                    ],
                ],*/
            ],
            'cacheKey' => 'sitemapCacheKey',
            'enableGzip' => false, // default is false
        ],
        'page' => [
            'class' => 'frontend\modules\page\Module',
        ],
        'form' => [
            'class' => 'frontend\modules\form\Module',
        ],
        'apartment' => [
            'class' => 'frontend\modules\apartment\Module',
        ],
        'general' => [
            'class' => 'frontend\modules\general\Module',
        ],
        'blog' => [
            'class' => 'frontend\modules\blog\Module',
        ],
        'team' => [
            'class' => 'frontend\modules\team\Module',
        ],
        'agents' => [
            'class' => 'frontend\modules\agents\Module',
        ],
        'partner' => [
            'class' => 'frontend\modules\partner\Module',
        ],
        'hr' => [
            'class' => 'frontend\modules\hr\Module',
        ],
        'resale' => [
            'class' => 'frontend\modules\resale\Module',
        ],
        'grabber' => [
            'class' => 'frontend\modules\grabber\Module',
        ],
        'construction' => [
            'class' => 'frontend\modules\construction\Module',
        ],
		'api' => [
			'class' => 'frontend\modules\api\Module',
		],
    ],
        require(__DIR__ . '/core_modules/modules.php')
    ),
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'config' => [
            'class' => '\common\components\ConfigurationComponent',
        ],
        'user' => [
            'loginUrl' => ['/user/login'],
        ],
        'siteAnalytics'=> [
            'class'=>'frontend\components\SiteAnalytics'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'formatter' => [
            'thousandSeparator' => ' ',
        ],
        'urlManager' => [
            'enableLanguageDetection' => false,
            //'languages' => ['ru'],
            'rules' => \yii\helpers\ArrayHelper::merge(
                require(__DIR__ . '/../../common/config/url-rules.php'),
                require(__DIR__ . '/core_modules/url-rules.php')
            ),
        ],
        'errorHandler' => [
            'errorAction' => 'error/error',
        ],
//        'view' => [
//            'theme' => [
//                'basePath' => '@app/themes/basic',
//                'pathMap' => [
//                    '@app/views' => ['@app/themes/basic'],
//                    '@app/modules' => ['@app/themes/basic/modules'],
//                    '@app/widgets' => ['@app/themes/basic/widgets']
//                ],
//                'baseUrl' => '@web/themes/basic',
//            ],
//        ],
        'assetManager' => [
            'linkAssets' => true,
            'appendTimestamp' => true,
            'bundles' => YII_ENV_DEV ? [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        '/static/js/index.jquery.js',
                    ],
                    'jsOptions' => ['position' => \yii\web\View::POS_HEAD]
                ],
            ] : require(__DIR__ . '/' . 'assets-prod.php'),
        ],
        'bitrixIntegration' => [
            'class' => BitrixIntegration::class,
        ],
        'bitrixConnect' => [
            'class' => BitrixConnect::class,
        ],
		'mobileDetect' => [
			'class' => \Detection\MobileDetect::class,
		]
    ],
    'params' => $params,
];

return $config;