<?php

namespace backend\modules\currency\models;

use Yii;
use common\components\model\ActiveRecord;

/**
* This is the model class for table "{{%currency_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class CurrencyTranslation extends ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%currency_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/currency', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
         ];
    }
}
