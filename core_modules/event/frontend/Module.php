<?php

namespace frontend\modules\event;

use yii\base\Module as BaseModule;
use common\components\interfaces\CoreModuleFrontendInterface;

/**
 * Class Module
 *
 * @package frontend\modules\event
 * @author Bogdan Fedun <delagics@gmail.com>
 */
class Module extends BaseModule implements CoreModuleFrontendInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\event\controllers';
}
