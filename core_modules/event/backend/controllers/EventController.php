<?php

namespace backend\modules\event\controllers;

use Yii;
use yii\base\Exception;
use yii\db\StaleObjectException;
use common\components\model\ActiveRecord;
use backend\components\BackendController;
use backend\modules\event\models\Event;

/**
 * EventController implements the CRUD actions for Event model.
 *
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package backend\modules\event\controllers
 */
class EventController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Event::className();
    }

    /**
     * Creates a new model.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $class = $this->getModelClass();
        /** @var ActiveRecord $model */
        $model = new $class();
        $model->loadDefaultValues();
        $this->saveEvent($model, Yii::t('app', 'Record successfully created!'));

        return $this->render('//templates/create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /** @var ActiveRecord $model */
        $model = $this->findModel($id);
        $this->saveEvent($model, Yii::t('app', 'Record successfully updated!'));

        return $this->render('//templates/update', [
            'model' => $model,
        ]);
    }

    /**
     * Save model into the database.
     *
     * @param ActiveRecord $model associated model.
     * @param string $successMsg success message.
     * @return \yii\web\Response|void
     */
    public function saveEvent($model, $successMsg)
    {
        try {
            if ($this->loadModels($model) && $model->save()) {
                Yii::$app->getSession()->setFlash('info', $successMsg);

                return $this->redirect(['view', 'id' => $model->id]);
            }
        } catch (Exception $e) {
            if ($e instanceof StaleObjectException) {
                Yii::$app->session->addFlash(
                    'error',
                    Yii::t('back/event', 'Update conflict, another user has already updated the record.')
                );
            } else {
                Yii::$app->session->addFlash('error', $e->getMessage());
            }
        }
    }
}
