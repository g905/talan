<?php

use console\components\Migration;

/**
 * Class m170415_113521_create_social_share_content_table migration
 */
class m170415_113521_create_social_share_content_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%social_share_content}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'model_name' => $this->string(50)->notNull()->comment('Model name'),
                'model_id' => $this->integer()->notNull()->comment('Model ID'),
                'title' => $this->string()->null()->comment('Title'),
                'description' => $this->text()->null()->comment('Description'),
                'image_id' => $this->text()->null()->comment('Image'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
