<?php

namespace backend\modules\blog\models;

use Yii;
use yii\helpers\ArrayHelper;
use creocoder\translateable\TranslateableBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\models\Configuration;
use common\models\BlogPageSettings as CommonBlogPageSettings;
use backend\modules\configuration\components\ConfigurationModel;

/**
 * Class BlogPageSettings
 *
 * @package backend\modules\blog\models
 */
class BlogPageSettings extends ConfigurationModel
{
    /**
     * @inheritdoc
     */
    public $showAsConfig = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'translateable' => [
                    'class' => TranslateableBehavior::className(),
                    'translationAttributes' => static::getTranslationAttributes(),
                ],
                'seo' => [
                    'class' => MetaTagBehavior::className()
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        return Yii::t('back/blog/page', 'Blog Page Settings');
    }

    /**
     * @inheritdoc
     */
    public function getFormRules()
    {
        return [
            [CommonBlogPageSettings::BLOG_INDEX_PAGE_TITLE, 'required'],
            [CommonBlogPageSettings::BLOG_INDEX_PAGE_SIZE, 'required'],
            [CommonBlogPageSettings::BLOG_INDEX_PAGE_FIXED_POST, 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFormTypes()
    {
        return [
            CommonBlogPageSettings::BLOG_INDEX_PAGE_TITLE => Configuration::TYPE_STRING,
            CommonBlogPageSettings::BLOG_INDEX_PAGE_SIZE => Configuration::TYPE_INTEGER,
            CommonBlogPageSettings::BLOG_INDEX_PAGE_FIXED_POST => Configuration::TYPE_INTEGER,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFormDescriptions()
    {
        return [
            CommonBlogPageSettings::BLOG_INDEX_PAGE_TITLE => Yii::t('back/blog/page', 'Page title'),
            CommonBlogPageSettings::BLOG_INDEX_PAGE_SIZE => Yii::t('back/blog/page', 'List page size'),
            CommonBlogPageSettings::BLOG_INDEX_PAGE_FIXED_POST => Yii::t('back/blog/page', 'Fixed post'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFormHints()
    {
        return [
            CommonBlogPageSettings::BLOG_INDEX_PAGE_TITLE => Yii::t(
                'back/blog/page',
                'Title on the Blog list page'
            ),
            CommonBlogPageSettings::BLOG_INDEX_PAGE_SIZE => Yii::t(
                'back/blog/page',
                'Size of the Blog list page'
            ),
            CommonBlogPageSettings::BLOG_INDEX_PAGE_FIXED_POST => Yii::t(
                'back/blog/page',
                'The ID of the fixed post on the Blog list page'
            ),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getTranslationAttributes()
    {
        return [
            CommonBlogPageSettings::BLOG_INDEX_PAGE_TITLE
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Main' => [
                    CommonBlogPageSettings::BLOG_INDEX_PAGE_TITLE,
                    CommonBlogPageSettings::BLOG_INDEX_PAGE_SIZE,
                    CommonBlogPageSettings::BLOG_INDEX_PAGE_FIXED_POST,
                ],
            ]
        ];

        return $config;
    }
}
