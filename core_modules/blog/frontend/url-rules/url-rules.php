<?php

return [
    'blog/<page:\d+>' => 'blog/blog/index',
    'blog/category/<category:[a-z0-9-]+>' => 'blog/blog/index',
    'blog' => 'blog/blog/index',
    'blog/view/<alias:[a-z0-9-]+>' => 'blog/blog/view',
];
