<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%blog_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 * @property string $content
 * @property string $description
 * @property EntityToFile $image
 */
class BlogTranslation extends ActiveRecord
{
    const SAVE_ATTRIBUTE_IMAGE = 'BlogTranslationImage';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_translation}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition([
                'image.entity_model_name' => static::formName(),
                'image.attribute' => static::SAVE_ATTRIBUTE_IMAGE
            ])
            ->alias('image')
            ->orderBy('image.position DESC');
    }
}
