<?php

return [
    'vacancy/all' => 'vacancy/default/index',
    'vacancy/<id:\d+>' => 'vacancy/default/vacancy-page',
    'vacancy/vacancy-request' => 'vacancy/default/vacancy-request',
];
