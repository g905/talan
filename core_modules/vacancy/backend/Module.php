<?php

namespace backend\modules\vacancy;

use common\components\interfaces\CoreModuleBackendInterface;
use Yii;

class Module extends \yii\base\Module implements CoreModuleBackendInterface
{
    public $controllerNamespace = 'backend\modules\vacancy\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}
