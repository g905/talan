<?php

namespace backend\modules\comment\models;

use backend\modules\admin\models\User;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii2mod\moderation\enums\Status;

/**
 * This is the model class for table "{{%comment}}".
 *
 * @property integer $id
 * @property string $entity
 * @property integer $entityId
 * @property string $content
 * @property integer $parentId
 * @property integer $level
 * @property integer $createdBy
 * @property integer $updatedBy
 * @property string $relatedTo
 * @property string $url
 * @property integer $status
 * @property integer $createdAt
 * @property integer $updatedAt
 *
 * @property User $author
 */
class Comment extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity', 'entityId', 'content'], 'required'],
            [['entityId', 'parentId', 'level', 'createdBy', 'updatedBy', 'status', 'createdAt', 'updatedAt'], 'integer'],
            [['content', 'url'], 'string'],
            [['entity'], 'string', 'max' => 10],
            [['relatedTo'], 'string', 'max' => 500],
            [['level'], 'default', 'value' => 1],
            [['status'], 'default', 'value' => 1],
         ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/comment', 'ID'),
            'entity' => Yii::t('back/comment', 'Entity'),
            'entityId' => Yii::t('back/comment', 'Entity ID'),
            'content' => Yii::t('back/comment', 'Content'),
            'parentId' => Yii::t('back/comment', 'Parent ID'),
            'level' => Yii::t('back/comment', 'Level'),
            'createdBy' => Yii::t('back/comment', 'Created By'),
            'updatedBy' => Yii::t('back/comment', 'Updated By'),
            'relatedTo' => Yii::t('back/comment', 'Related To'),
            'url' => Yii::t('back/comment', 'Url'),
            'status' => Yii::t('back/comment', 'Status'),
            'createdAt' => Yii::t('back/comment', 'Created At'),
            'updatedAt' => Yii::t('back/comment', 'Updated At'),
        ];
    }

    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Comment');
    }

    /**
     * Author relation
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'createdBy']);
    }

    /**
     * @return mixed
     */
    public function getAuthorName()
    {
        if ($this->author->hasMethod('getUsername')) {
            return $this->author->getUsername();
        }

        return $this->author->username;
    }

    /**
     * Get list of all authors
     *
     * @return array
     */
    public static function getAuthors()
    {
        $query = static::find()
            ->alias('c')
            ->select(['c.createdBy', 'a.username'])
            ->joinWith('author a')
            ->groupBy(['c.createdBy', 'a.username'])
            ->orderBy('a.username')
            ->asArray()
            ->all();

        return ArrayHelper::map($query, 'createdBy', 'author.username');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    //'entity',
                    //'entityId',
                    [
                        'attribute' => 'content',
                        'contentOptions' => ['style' => 'max-width: 350px;'],
                        'value' => function ($model) {
                            return StringHelper::truncate($model->content, 100);
                        },
                    ],
                    //'parentId',
                    //'level',
                    [
                        'attribute' => 'createdBy',
                        'value' => function ($model) {
                            return $model->getAuthorName();
                        },
                        'filter' => static::getAuthors(),
                        'filterInputOptions' => ['prompt' => Yii::t('back/comment', 'Select Author'), 'class' => 'form-control'],
                    ],
                    //'updatedBy',
                    'relatedTo',
                    // 'url:url',
                    [
                        'attribute' => 'status',
                        'value' => function ($model) {
                            return Status::getLabel($model->status);
                        },
                        'filter' => Status::listData(),
                        'filterInputOptions' => ['prompt' => Yii::t('back/comment', 'Select Status'), 'class' => 'form-control'],
                    ],
                    [
                        'attribute' => 'createdAt',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDatetime($model->createdAt);
                        },
                        'filter' => false,
                    ],
                    // 'updatedAt',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'entity',
                    'entityId',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'parentId',
                    'level',
                    'createdBy',
                    'updatedBy',
                    'relatedTo',
                    'url:url',
                    'status',
                    'createdAt',
                    'updatedAt',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new CommentSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            /*'entity' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'entityId' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],*/
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'content',
                ]
            ],
            /*'parentId' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'level' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'createdBy' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'updatedBy' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'relatedTo' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'url' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'url',
                ]
            ],*/
            'status' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => Status::listData(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            /*'createdAt' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'updatedAt' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],*/
        ];

        return $config;
    }


}
