<?php

use console\components\Migration;

/**
 * Class m170407_075235_create_video_table_translation migration
 */
class m170407_075235_create_video_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%video_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%video}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label'       => $this->string()->defaultValue(null)->comment('Label'),
                'description' => $this->text()->defaultValue(null)->comment('Description'),
                'content'     => $this->text()->defaultValue(null)->comment('Content'),
                'video_link'  => $this->text()->defaultValue(null)->comment('Video link'),
            ],
            $this->tableOptions
        );


        $this->addPrimaryKey('pk-video_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-video_translation-model_id-video-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

