<?php

namespace frontend\modules\videoGallery\helpers;

use common\components\model\Helper;

class VideoAlbumUrlHelper
{
    use Helper;

    /**
     * Method returns video album view page route
     *
     * @return string
     */
    public static function getVideoAlbumViewRoute(): string
    {
        return '/videoGallery/video-album/view';
    }

    /**
     * Method returns video albums list page route
     *
     * @return string
     */
    public static function getVideoAlbumListRoute(): string
    {
        return '/videoGallery/video-album/list';
    }

    /**
     * Method returns video albums list page url
     *
     * @return string
     */
    public static function getVideoAlbumListUrl(): string
    {
        return static::createUrl(static::getVideoAlbumListRoute(), []);
    }

    /**
     * Method returns video album view page rout
     *
     * @param array $params
     *
     * @return string
     */
    public static function getVideoAlbumViewUrl(array $params): string
    {
        return static::createUrl(static::getVideoAlbumViewRoute(), $params);
    }

}