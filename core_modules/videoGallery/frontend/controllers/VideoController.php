<?php

namespace frontend\modules\videoGallery\controllers;

use frontend\components\FrontendController;
use frontend\modules\videoGallery\models\Video;
use Yii;
use yii\web\NotFoundHttpException;

class VideoController extends FrontendController
{
    /**
     * Method renders list of videos
     *
     * @return string
     */
    public function actionList()
    {
        $dataProvider = Video::getActiveDateProvider();

        //TODO: render this list
    }

    /**
     * Method renders video page for given alias
     *
     * @param string $videoAlias
     *
     *
     * @return string
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($videoAlias)
    {
        if (!($model = Video::getVideoByAlias($videoAlias))) {
            throw new NotFoundHttpException(Yii::t('front/video', 'Video_not_found.'));
        }

        //TODO: render this video
    }
}