<?php

return [
    'label' => Yii::t('back/photo', 'Photo albums'),
    'url' => ['/photoGallery/photo-gallery-album/index'],
];
