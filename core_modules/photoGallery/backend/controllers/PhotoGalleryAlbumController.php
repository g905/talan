<?php

namespace backend\modules\photoGallery\controllers;

use backend\components\BackendController;
use backend\modules\photoGallery\models\PhotoGalleryAlbum;

/**
 * PhotoGalleryController implements the CRUD actions for PhotoGalleryAlbum model.
 */
class PhotoGalleryAlbumController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PhotoGalleryAlbum::className();
    }
}
