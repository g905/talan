<?php

namespace backend\modules\photoGallery\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PhotoGalleryAlbumSearch represents the model behind the search form about `PhotoGalleryAlbum`.
 */
class PhotoGalleryAlbumSearch extends PhotoGalleryAlbum
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'published', 'position'], 'integer'],
            [['label', 'alias'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PhotoGalleryAlbumSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'parent_id' => $this->parent_id,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'alias', $this->alias]);

        return $dataProvider;
    }
}
