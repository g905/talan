<?php

namespace backend\modules\photoGallery\models;

use backend\components\ImperaviContent;
use backend\components\TranslateableTrait;
use backend\modules\photoGallery\helpers\PhotoGalleryUrlHelper;
use common\components\model\Translateable;
use common\models\FpmFile;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

class PhotoGalleryEntity extends \common\models\PhotoGalleryEntity implements Translateable
{
    use TranslateableTrait;

    /**
     * @param int $id
     *
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getEntityByFileId(int $id)
    {
        return PhotoGalleryEntity::find()
            ->andWhere(['file_id' => $id])
            ->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['file_id'],
                'required',
                'message' => Yii::t('back/photo', 'You need to upload image first.'),
            ],
            [
                ['file_id'],
                'exist',
                'targetClass'     => FpmFile::className(),
                'targetAttribute' => 'id',
                'message'         => Yii::t('back/photo', 'Cant find requested file.'),
            ],
            [['file_id', 'published', 'position'], 'integer'],
            [['description'], 'string'],
            [['label', 'alt'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label'       => Yii::t('back/photo', 'Label'),
            'alt'         => Yii::t('back/photo', 'Alt'),
            'description' => Yii::t('back/photo', 'Description'),
            'published'   => Yii::t('back/photo', 'Published'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            ['timestamp' => ['class' => TimestampBehavior::className()]]
        );
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        return [
            'label'       => [
                'type'    => ActiveFormBuilder::INPUT_TEXT,
                'options' => ['maxlength' => true],
            ],
            'alt'         => [
                'type'    => ActiveFormBuilder::INPUT_TEXT,
                'options' => ['maxlength' => true],
            ],
            'description' => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
            ],
        ];
    }

    public function getMetaDataSaveUrl(): string
    {
        return PhotoGalleryUrlHelper::getSaveMetaDataUrl(['id' => $this->file_id]);
    }
}
