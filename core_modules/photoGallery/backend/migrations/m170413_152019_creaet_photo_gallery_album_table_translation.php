<?php

use console\components\Migration;

/**
 * Class m170413_152019_creaet_photo_gallery_album_table_translation migration
 */
class m170413_152019_creaet_photo_gallery_album_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%photo_gallery_album_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%photo_gallery_album}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label'       => $this->string()->defaultValue(null)->comment('Label'),
                'description' => $this->text()->defaultValue(null)->comment('Description'),
                'content'     => $this->text()->defaultValue(null)->comment('Content'),
                'author'      => $this->string()->defaultValue(null)->comment('Author'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-photo_gallery_album_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-photo_gallery_album_trans-model_id-photo_gallery_album-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

