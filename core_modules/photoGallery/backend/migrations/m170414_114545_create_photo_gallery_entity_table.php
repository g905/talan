<?php

use console\components\Migration;

/**
 * Class m170414_114545_create_photo_gallery_entity_table migration
 */
class m170414_114545_create_photo_gallery_entity_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%photo_gallery_entity}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'file_id' => $this->integer()->unsigned()->notNull(),

                'label'       => $this->string()->defaultValue(null)->comment('Label'),
                'alt'         => $this->string()->defaultValue(null)->comment('Alt'),
                'description' => $this->text()->defaultValue(null)->comment('Description'),

                'published' => $this->boolean()->unsigned()->notNull()->defaultValue(true)->comment('Published'),
                'position'  => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->createIndex('key-file_id', $this->tableName, 'file_id', true);

        $this->addForeignKey(
            'fk-photo_gallery_entity-file_id-to-fpm_file-id',
            $this->tableName,
            'file_id',
            '{{%fpm_file}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
