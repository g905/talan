<?php

return [
    'photo-gallery/albums/<albumAlias:([a-z0-9\-]+)>' => 'photoGallery/photo-gallery/list',
    'photo-gallery/albums' => 'photoGallery/photo-gallery/list',
    'photo-gallery/photos/<albumAlias:([a-z0-9\-]+)>' => 'photoGallery/photo-gallery/view',
];
