<?php

use console\components\Migration;

/**
 * Class m170312_183535_create_product_set_table migration
 */
class m170312_183535_create_product_set_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_set}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'product_id' => $this->integer()->notNull()->comment('Main product in set'),
                'product_set_id' => $this->integer()->notNull()->comment('Second product in set'),
                'discount' => $this->float()->null()->comment('set discount'),
                'price' => $this->decimal(10, 2)->null()->comment('set price'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-product_set-product_id-product-id',
            $this->tableName,
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-product_set-product_set_id-product-id',
            $this->tableName,
            'product_set_id',
            '{{%product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_set-product_id-product-id', $this->tableName);
        $this->dropForeignKey('fk-product_set-product_set_id-product-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
