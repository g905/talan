<?php

use console\components\Migration;

/**
 * Class m170319_150506_create_product_attribute_to_attribute_group_table migration
 */
class m170319_150506_create_product_attribute_to_attribute_group_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_attribute_to_attribute_group}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'attribute_id' => $this->integer()->notNull()->comment('Attribute'),
                'attribute_group_id' => $this->integer()->notNull()->comment('Attribute Group'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-attribute_id-product_attribute-id',
            $this->tableName,
            'attribute_id',
            '{{%product_attribute}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-attribute_group_id-product_attribute_group-id',
            $this->tableName,
            'attribute_group_id',
            '{{%product_attribute_group}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-attribute_id-product_attribute-id', $this->tableName);
        $this->dropForeignKey('fk-attribute_group_id-product_attribute_group-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
