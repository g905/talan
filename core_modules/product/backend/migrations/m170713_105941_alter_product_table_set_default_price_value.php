<?php

use console\components\Migration;

/**
 * Class m170713_105941_alter_product_table_set_default_price_value migration
 */
class m170713_105941_alter_product_table_set_default_price_value extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //update old price value
        \common\models\Product::updateAll(['price' => 0], ['price' => null]);
        \common\models\Product::updateAll(['old_price' => 0], ['old_price' => null]);
        $this->alterColumn($this->tableName, 'price', $this->decimal(10, 2)->notNull()->defaultValue(0)->comment('Price'));
        $this->alterColumn($this->tableName, 'old_price', $this->decimal(10, 2)->notNull()->defaultValue(0)->comment('Old price'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \common\models\Product::updateAll(['price' => null], ['price' => 0]);
        \common\models\Product::updateAll(['old_price' => null], ['old_price' => 0]);
        $this->alterColumn($this->tableName, 'price', $this->decimal(10, 2)->null()->comment('Price'));
        $this->alterColumn($this->tableName, 'old_price', $this->decimal(10, 2)->null()->comment('Old price'));
    }
}
