<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductAttribute;
use backend\modules\product\models\ProductAttributeGroup;
use backend\modules\product\models\ProductAttributeOption;
use backend\modules\product\models\ProductEav;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ProductAttributeController implements the CRUD actions for ProductAttribute model.
 */
class ProductAttributeController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductAttribute::className();
    }

    public function actionGetSelectItems($search = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($search)) {
            $search = '';
        }

        $query = new Query();
        $query->select('id, label AS text')
            ->from(ProductAttribute::tableName())
            ->where(['like', 'label', $search])
            ->limit(5);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => ProductAttribute::find($id)->name];
        }
        return $out;
    }

    public function actionGetByGroup()
    {
        $postData = \Yii::$app->request->post();
        if (!isset($postData['groupIds'])) {
            throw new NotFoundHttpException();
        }
        $groupIds = $postData['groupIds'];

        if (!is_array($groupIds)) {
            return json_encode([]);
        }

        $returnArray = [];
        foreach ($groupIds as $groupId) {
            $group = ProductAttributeGroup::findOne($groupId);
            if (!isset($group)) {
                throw new NotFoundHttpException();
            }

            $attributesArray = ArrayHelper::map(ProductAttribute::find()
                ->select(['product_attribute.id', 'label'])
                ->innerJoin(
                    'product_attribute_to_attribute_group',
                    'product_attribute_to_attribute_group.attribute_id = product_attribute.id'
                )
                ->where([
                    'product_attribute_to_attribute_group.attribute_group_id' => $groupId,
                    'product_attribute.published' => 1,
                ])
                ->asArray()
                ->all(), 'id', 'label');
            $returnArray += $attributesArray;
        }

        return json_encode($returnArray);
    }

    public function actionGetById()
    {
        $postData = \Yii::$app->request->post();
        if (!isset($postData['attributesIds'])) {
            throw new NotFoundHttpException();
        }
        $attributesIds = $postData['attributesIds'];

        if (!is_array($attributesIds)) {
            return json_encode([]);
        }

        $returnArray = [];
        foreach ($attributesIds as $attributeId) {
            $attributesArray = ProductAttribute::find()
                ->select(['id', 'label', 'attribute_type'])
                ->with('options')
                ->where([
                    'id' => $attributeId,
                    'published' => 1,
                ])
                ->asArray()
                ->one();


            $returnArray[] = $attributesArray;
        }

        return json_encode($returnArray);
    }

    public function actionGetAttributeValue()
    {
        $postData = \Yii::$app->request->post();

        if (!isset($postData['attributeId'])) {
            throw new NotFoundHttpException();
        }

        if (!isset($postData['attributeType'])) {
            throw new NotFoundHttpException();
        }

        if (!isset($postData['attributeLabel'])) {
            throw new NotFoundHttpException();
        }

        if (!isset($postData['tableId'])) {
            throw new NotFoundHttpException();
        }

        if (!isset($postData['className'])) {
            throw new NotFoundHttpException();
        }

        $attributeId = $postData['attributeId'];
        $productId = $postData['productId'] ?? null;
        $attributeType = $postData['attributeType'];
        $attributeLabel = $postData['attributeLabel'];
        $tableId = $postData['tableId'];
        $select2id = $postData['select2id'] ?? null;
        $className = $postData['className'];

        $eav = ProductEav::find()
            ->select('value')
            ->where([
                'product_id' => $productId,
                'attribute_id' => $attributeId
            ])
            ->asArray()
            ->all();


        $data = [];
        switch ($attributeType) {
            case \common\models\ProductAttribute::TYPE_TEXT_FIELD:

                if ((!isset($eav)) || (count($eav) == 0)) {
                    $value = '';
                } else {
                    $value = $eav[0]['value'];
                }

                $data = [
                    'append' => [
                        [
                            'data' => $this->generateTextTr($tableId, $attributeId, $attributeLabel, $value, $className),
                            'what' => '#' . $tableId
                        ],
                    ],
                ];

                break;

            case \common\models\ProductAttribute::TYPE_DROP_DOWN_FIELD:

                $isMultiple = false;
                if (!isset($select2id)) {
                    return null;
                }

                if ((!isset($eav)) || (count($eav) == 0)) {
                    $value = null;
                } else {
                    $value = $eav[0]['value'];
                }

                $data = [
                    'append' => [
                        [
                            'data' => $this->generateDropDownTr($tableId, $attributeId, $attributeLabel, $value,
                                $select2id, $isMultiple, $className),
                            'what' => '#' . $tableId
                        ],
                    ],
                ];
                break;

            case \common\models\ProductAttribute::TYPE_MULTI_SELECT_FIELD:

                $isMultiple = true;
                if (!isset($select2id)) {
                    return null;
                }

                if ((!isset($eav)) || (count($eav) == 0)) {
                    $value = [];
                } else {
                    $value = ArrayHelper::getColumn($eav, 'value');
                }

                $data = [
                    'append' => [
                        [
                            'data' => $this->generateDropDownTr($tableId, $attributeId, $attributeLabel, $value,
                                $select2id, $isMultiple, $className),
                            'what' => '#' . $tableId
                        ],
                    ],
                ];
                break;
        }


        return Json::encode($data);
    }

    protected function generateTextTr($tableId, $attributeId, $attributeLabel, $attributeValue, $className)
    {
        $returnData = Html::beginTag('tr', [
            'id' => $attributeId
        ]);
        $returnData .= Html::tag('td', $attributeLabel);

        $returnData .= Html::beginTag('td');
        $returnData .= Html::input(
            'text',
            $className."[attributeTable][" . $attributeId . "]",
            $attributeValue,
            [
                'class' => 'attribute-table-field'
            ]
        );
        $returnData .= Html::endTag('td');

        $returnData .= Html::endTag('tr');
        return $returnData;
    }

    protected function generateDropDownTr(
        $tableId,
        $attributeId,
        $attributeLabel,
        $attributeValue,
        $select2id,
        $isMultiple,
        $className
    ) {

        $dropDownOptions = ArrayHelper::map(ProductAttributeOption::find()
            ->select(['id', 'label'])
            ->where([
                'attribute_id' => $attributeId
            ])
            ->orderBy([
                'position' => SORT_ASC
            ])
            ->all(), 'id', 'label');


        $returnData = Html::beginTag('tr', [
            'id' => $attributeId
        ]);
        $returnData .= Html::tag('td', $attributeLabel);

        $returnData .= Html::beginTag('td');
        $returnData .= Html::dropDownList(
            $className."[attributeTable][" . $attributeId . "]",
            $attributeValue,
            $dropDownOptions,
            [
                'class' => 'attribute-table-field',
                'id' => $select2id,
                'multiple' => $isMultiple,
                'style' => [
                    'width' => '100%' //Style via css class only not work in related form
                ]
            ]
        );
        $returnData .= Html::endTag('td');

        $returnData .= Html::endTag('tr');
        return $returnData;
    }

}
