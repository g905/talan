<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductAttributeGroup;
use yii\db\Query;
use yii\web\Response;

/**
 * ProductAttributeGroupController implements the CRUD actions for ProductAttributeGroup model.
 */
class ProductAttributeGroupController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductAttributeGroup::className();
    }

    public function actionGetSelectItems($search = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($search)) {
            $search = '';
        }

        $query = new Query();
        $query->select('id, label AS text')
            ->from(ProductAttributeGroup::tableName())
            ->where(['like', 'label', $search])
            ->limit(5);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => ProductAttributeGroup::find($id)->name];
        }
        return $out;
    }
}
