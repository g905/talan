<?php

namespace backend\modules\product\models;

use backend\components\BackendModel;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;

/**
 * This is the model class for table "{{%product_delivery}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $short_desc
 * @property string $content
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductDeliveryTranslation[] $translations
 */
class ProductDelivery extends ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_delivery}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['content'], 'string'],
            [['published', 'position'], 'integer'],
            [['label', 'short_desc'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-delivery', 'ID'),
            'label' => Yii::t('back/product-delivery', 'Label'),
            'short_desc' => Yii::t('back/product-delivery', 'Short description'),
            'content' => Yii::t('back/product-delivery', 'Content'),
            'published' => Yii::t('back/product-delivery', 'Published'),
            'position' => Yii::t('back/product-delivery', 'Position'),
            'created_at' => Yii::t('back/product-delivery', 'Created At'),
            'updated_at' => Yii::t('back/product-delivery', 'Updated At'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'short_desc',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductDeliveryTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product-delivery', 'Product Delivery');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    'short_desc',
                    // 'content:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    'short_desc',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductDeliverySearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'short_desc' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'content',
                ]
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }
}
