<?php

namespace backend\modules\product\models;

use Yii;

/**
* This is the model class for table "{{%product_eav_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $value
*/
class ProductEavTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%product_eav_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/product-eav', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/product-eav', 'Language') . ' [' . $this->language . ']',
            'value' => Yii::t('back/product-eav', 'Value') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['value'], 'string', 'max' => 255],
         ];
    }
}
