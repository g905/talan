<?php
/**
 * Author: Pavel Naumenko
 */

namespace backend\modules\product\helpers;

use common\components\model\ActiveRecord;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use yii\helpers\StringHelper;

/**
 * Class ImgHelper
 * @package backend\components
 */
class ImgHelper
{
    /**
     * @param $model
     * @param string $relation
     * @return null|string
     */
    public static function getEntityImage($model, $relation = 'mainImage')
    {
        return (isset($model->{$relation}) && $model->{$relation}->file_id)
            ? FPM::image($model->{$relation}->file_id, 'admin', 'file')
            : Yii::t('back/' . StringHelper::basename($model::className()), 'Image not set');
    }
}
