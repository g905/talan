<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_manufacturer_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $short_desc
 * @property string $content
 */
class ProductManufacturerTranslation extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_manufacturer_translation}}';
    }
}
