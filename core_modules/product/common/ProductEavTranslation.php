<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_eav_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $value
 */
class ProductEavTranslation extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_eav_translation}}';
    }
}
