<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_attribute_to_attribute_group}}".
 *
 * @property integer $id
 * @property integer $attribute_id
 * @property integer $attribute_group_id
 */
class ProductAttributeToAttributeGroup extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_attribute_to_attribute_group}}';
    }
}
