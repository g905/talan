<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 13.03.2017
 * Time: 01:12
 */
namespace frontend\modules\product\helpers;

use common\models\ProductCategory;

class NestedSetHelper
{
    public static function buildCategoryArray(array $elements)
    {
        $result = [];

        foreach ($elements as $element) {
            $node = [
                'id' => $element->id,
                'label' => $element->label,
                'url' => ProductCategory::getSingleUrl(['alias' => $element->alias]),
            ];

            if ($element->children()->count() > 0) {
                $node['childrens'] = self::buildCategoryArray($element->children(1)->andWhere(['published' => 1])->all());
            }

            $result[] = $node;
        }
        return $result;
    }

    public static function buildNavXConfig(array $elements)
    {
        $result = [];

        foreach ($elements as $element) {
            $node = [
                'label' => $element->label,
                'url' => ProductCategory::getSingleUrl(['alias' => $element->alias]),
            ];

            if ($element->children()->count() > 0) {
                $node['items'] = self::buildNavXConfig($element->children(1)->andWhere(['published' => 1])->all());
            }

            $result[] = $node;
        }
        return $result;
    }
}
