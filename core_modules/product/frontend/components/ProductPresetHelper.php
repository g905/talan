<?php

namespace frontend\modules\product\components;
use common\models\ProductPresetPage;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;

/**
 * Created by PhpStorm.
 * User: art
 * Date: 24.07.2017
 * Time: 13:44
 */
class ProductPresetHelper
{
    /**
     * @return mixed
     */
    public static function getProductPresetPages()
    {
        $cache = \Yii::$app->cache;
        $dependency = new \yii\caching\DbDependency(['sql' => 'SELECT MAX(created_at) FROM {{%product_preset_page}}']);
        $pages = $cache->getOrSet('productPresetDynaRoute', function () {
            return ProductPresetPage::find()
                ->where([
                    'published' => 1
                ])
                ->all();
        }, null, $dependency);

        return $pages;
    }


    public static function checkPresetPage()
    {
        //check redirect from origin to alias
        $currentFullUrl = \Yii::$app->request->getAbsoluteUrl();
        $currentUrl = strstr($currentFullUrl, \Yii::$app->getRequest()->serverName);
        $currentUrl = str_replace(\Yii::$app->getRequest()->serverName.'/', '',  $currentUrl );


        /** @var ProductPresetPage $existPresetPage */
        $existPresetPage = ProductPresetPage::find()
            ->where(['origin_url' => $currentFullUrl])
            ->one();


        if (isset($existPresetPage)) {
            return \Yii::$app->getResponse()->redirect('/' . $existPresetPage->alias);
        }

        $presetPages = self::getProductPresetPages();

        foreach ($presetPages as $presetPage) {

            /** @var ProductPresetPage $presetPage */
            if ($currentUrl == $presetPage->alias) {


                $pageLabel = $presetPage->label;

                $getString = $presetPage->origin_url;
                $getString = basename($getString);

                $getStringArray = explode('?', $getString);

                $alias = $getStringArray[0];
                parse_str($getStringArray[1], $getParams);

                //set meta data for categories
                MetaTagRegister::register($presetPage);

                $returnData = [];
                $returnData['get_data'] = $getParams;
                $returnData['page_label'] = $pageLabel;
                $returnData['alias'] = $alias;

                return $returnData;
            }
        }
        return false;
    }
}
