<h1><?= $model->label ?></h1>
<hr/>
<?= $model->price ?>
<hr/>
<ul>
    <?php foreach ($model->getAttributesValue() as $attributeName => $attributeValue): ?>
        <li>
            <b><?= $attributeName ?></b>

            <ul>
                <?php foreach ($attributeValue['values'] as $value => $displayValue): ?>

                    <li><?= $displayValue ?></li>

                <?php endforeach; ?>
            </ul>
        </li>
    <?php endforeach; ?>
</ul>