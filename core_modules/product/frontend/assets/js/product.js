/**
 * Filters change event
 */
$(document).on('change', 'input[data-class="base-filter"]', function () {
    runFilter();
});

$(document).on('change', 'input[data-class="base-filter-price-from"]', function () {
    //apply price filter
    $("#user-set-filter-from").val(1);
    runFilter();
});

$(document).on('change', 'input[data-class="base-filter-price-to"]', function () {
    //apply price filter
    $("#user-set-filter-to").val(1);
    runFilter();
});

/**
 * Main filters apply functions
 */
function runFilter() {
    var container = $('#pjax-catalog-list');
    var baseFiltersForm = $('#base-filters-form');
    var pjaxUrl = baseFiltersForm.attr('action');

    var browserUrl = pjaxUrl + generateFilterUrl(baseFiltersForm.serialize());
    window.history.pushState(null, null, browserUrl);

    $.pjax({
        url: pjaxUrl,
        container: container,
        data: baseFiltersForm.serialize(),
        scrollTo: false,
        timeout: 15000,
        type: 'POST',
        push: false
    });
}

/**
 * Pretty url for filters
 */
function generateFilterUrl(serialize) {
    var FILTER_FORM_NAME = 'BaseFiltersForm';
    var USER_NOT_SET_PRICE = '';
    var result = decodeURIComponent(serialize);
    result = result.split("&");
    result.shift();


    var resultUrlArray = [];
    for (var i = 0; i < result.length; i++) {
        result[i] = result[i].replace(FILTER_FORM_NAME+'[', '').replace(']', '').replace('[]', '');

        //get option value
        var value = result[i].split("=");

        if (value[1] !== ''){
            valueJson = JSON.parse(value[1]);
        }
        var currentFilterName = '';
        var currentFilterValue = '';
        if (valueJson && typeof valueJson === "object") {

            //if attribute filter
             currentFilterName = valueJson.alias;
             currentFilterValue = valueJson.value;
        }
        else{
            // if brand/price filter
             currentFilterName = value[0].toLowerCase();
             currentFilterValue = value[1].toLowerCase();

             //don't add initial price to url
             if (currentFilterName === 'pricefrom'){
                 var initPriceFromElement = $('[init-price-from]');

                 if ($('#user-min-price').val() !== USER_NOT_SET_PRICE){
                     currentFilterValue = $('#user-min-price').val();
                 }
                 else{
                     if (initPriceFromElement.val() === initPriceFromElement.attr('init-price-from')){
                         continue;
                     }
                 }
             }
            if (currentFilterName === 'priceto'){
                var initPriceToElement = $('[init-price-to]');

                if ($('#user-max-price').val() !== USER_NOT_SET_PRICE){
                    currentFilterValue = $('#user-max-price').val();
                }
                else{
                    if (initPriceToElement.val() === initPriceToElement.attr('init-price-to')){
                        continue;
                    }
                }
            }
            if (currentFilterName === 'usersetfilterfrom'){
                continue;
            }
            if (currentFilterName === 'usersetfilterto'){
                continue;
            }
            if (currentFilterName === 'userminprice'){
                continue;
            }
            if (currentFilterName === 'usermaxprice'){
                continue;
            }

        }

        if (resultUrlArray[currentFilterName] === undefined){
            resultUrlArray[currentFilterName] = '';
        }
        else{
            resultUrlArray[currentFilterName] += ',';
        }
        resultUrlArray[currentFilterName] += encodeURIComponent(currentFilterValue);

    }

    var resultUrl = '';
    for (var option in resultUrlArray) {
        resultUrl+='&';
        if (resultUrlArray.hasOwnProperty(option)) {

            var urlKey = option;
            var urlValue = resultUrlArray[urlKey].replace(/,\s*$/, "");
            resultUrl+=urlKey+'='+urlValue;
        }
    }

    resultUrl = resultUrl.replace('&', '?');
    return resultUrl;

}
