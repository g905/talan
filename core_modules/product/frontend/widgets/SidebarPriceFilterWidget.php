<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 13.03.2017
 * Time: 01:02
 */

namespace frontend\modules\product\widgets;

use yii\base\Widget;

class SidebarPriceFilterWidget extends Widget
{
     public $categoryBoundaryMinPrice;
     public $categoryBoundaryMaxPrice;
     public $baseFiltersForm;
     public $categoryMinPrice;
     public $categoryMaxPrice;
     public $form;
    /**
     * @return string
     */
    public function run()
    {
        $session = \Yii::$app->session;
        $getData = \Yii::$app->request->get();
        return $this->render('_sidebarPriceFilterWidgetView', [
            'categoryBoundaryMinPrice' => $this->categoryBoundaryMinPrice,
            'categoryBoundaryMaxPrice' => $this->categoryBoundaryMaxPrice,
            'baseFiltersForm' => $this->baseFiltersForm,
            'categoryMinPrice' => $this->categoryMinPrice,
            'categoryMaxPrice' => $this->categoryMaxPrice,
            'form' => $this->form,
            'getData' => $getData,
            'session' => $session,
        ]);
    }
}
