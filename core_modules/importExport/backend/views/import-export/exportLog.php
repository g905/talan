<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 29.06.2017
 * Time: 1:22
 */

use backend\modules\importExport\config\ImportExportConfig;
use backend\modules\importExport\models\ImportExportMessage;

echo \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($model, $index, $widget, $grid) {

        if ($model->type == ImportExportMessage::MESSAGE_TYPE_SUCCESS) {
            return ['style' => 'background-color:' . ImportExportConfig::MESSAGE_LOG_SUCCESS_COLOR];
        } elseif (($model->type == ImportExportMessage::MESSAGE_TYPE_ERROR)) {
            return ['style' => 'background-color:' . ImportExportConfig::MESSAGE_LOG_ERROR_COLOR];
        }
    },
    'columns' => [
        [
            'attribute' => 'id',
            'label' => \Yii::t('back/import-export', 'id'),
        ],
        [
            'attribute' => 'created_at',
            'format' => ['date', 'YYYY-MM-dd HH:mm:ss'],
            'label' => \Yii::t('back/import-export', 'date'),
        ],
        [
            'attribute' => 'message',
            'label' => \Yii::t('back/import-export', 'message'),
            'content' => function ($model) {
                if ($model->type == ImportExportMessage::MESSAGE_TYPE_SUCCESS) {
                    return \Yii::t('back/import-export', 'Export completed successfully');
                } elseif (($model->type == ImportExportMessage::MESSAGE_TYPE_ERROR)) {
                    return $model->message;
                }
            }

        ],
        [
            'label' => \Yii::t('back/import-export', 'File'),
            'format' => 'raw',
            'value' => function ($model) {
                if ($model->type == ImportExportMessage::MESSAGE_TYPE_SUCCESS) {
                    return \yii\helpers\Html::a(
                        \Yii::t('back/import-export', 'Download'),
                        $model->message,
                        [
                            'target' => '_blank'
                        ]
                    );
                } else {
                    return '';
                }
            }
        ],
    ]
]);
