<?php

/*
 *
 */

namespace backend\modules\importExport\components;

use backend\modules\importExport\models\ImportExportMessage;
use yii\base\Component;

class CheckImportExportMessage extends Component
{
    public function init()
    {
        //find message
        /** @var ImportExportMessage $newMessage */
        $newMessage = ImportExportMessage::find()
            ->where([
                'is_show' => 1,
                'is_shown' => 0,
            ])
            ->orderBy([
                'id' => SORT_ASC
            ])
            ->one();

        if ($newMessage) {
            \Yii::$app->session->setFlash($newMessage->type, $newMessage->message);

            $newMessage->is_shown = 1;
            $newMessage->save();
        }
        parent::init();
    }
}
