<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%faq_question_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 * @property string $question
 * @property string $answer
 */
class FaqQuestionTranslation extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%faq_question_translation}}';
    }
}
