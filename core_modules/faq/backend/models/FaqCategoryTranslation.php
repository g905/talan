<?php

namespace backend\modules\faq\models;

use Yii;

/**
* This is the model class for table "{{%faq_category_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class FaqCategoryTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%faq_category_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/faq', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/faq', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/faq', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
         ];
    }
}
