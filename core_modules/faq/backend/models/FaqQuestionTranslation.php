<?php

namespace backend\modules\faq\models;

use Yii;

/**
* This is the model class for table "{{%faq_question_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $question
* @property string $answer
*/
class FaqQuestionTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%faq_question_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/faq', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/faq', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/faq', 'Label') . ' [' . $this->language . ']',
            'question' => Yii::t('back/faq', 'Question') . ' [' . $this->language . ']',
            'answer' => Yii::t('back/faq', 'Answer') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['question', 'answer'], 'string'],
            [['label'], 'string', 'max' => 255],
         ];
    }
}
