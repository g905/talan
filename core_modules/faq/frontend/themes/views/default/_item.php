<?php
/**
 * @var \yii\web\View $this
 * @var int $index
 * @var int $key
 * @var \frontend\modules\faq\models\FaqQuestion $model
 * @var \yii\widgets\ListView $widget
 */
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $model->label ?></h3>
    </div>
    <div class="panel-body">
        <?= $model->question ?>
        <hr/>
        <?= $model->answer ?>
    </div>
</div>
