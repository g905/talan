<?php

return [
    'faq/page/<page>' => 'faq/default/index',
    'faq' => 'faq/default/index',
    'faq/category/<alias>/page/<page>' => 'faq/default/category',
    'faq/category/<alias>' => 'faq/default/category',
    'faq/request' => 'faq/default/request',
];
