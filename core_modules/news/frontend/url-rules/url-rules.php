<?php
return [
    'news/view/<alias>' => 'news/news/view',
    'news/category/<category>/page-<page>' => 'news/news/index',
    'news/category/<category>' => 'news/news/index',
    'news/page-<page>' => 'news/news/index',
    'news' => 'news/news/index',
];