<?php

namespace backend\modules\offer\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OfferSearch represents the model behind the search form about `Offer`.
 */
class OfferSearch extends Offer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['published', 'position', 'categoriesIds'], 'integer'],
            [['label', 'alias', 'begin_date_string', 'end_date_string', 'created_at_string'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OfferSearch::find()->alias('t');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            't.published' => $this->published,
            't.position'  => $this->position,
        ]);

        $query->andFilterWhere(['like', 't.label', $this->label])
            ->andFilterWhere(['categories.id' => $this->categoriesIds]);

        if ($this->createdAtString) {
            $createdDate = new \DateTime($this->createdAtString);

            $query->andFilterWhere(['>', 't.created_at', $createdDate->getTimestamp()]);

            $createdDate->modify('+ 1 day');
            $query->andFilterWhere(['<', 't.created_at', $createdDate->getTimestamp()]);
        }

        if ($this->beginDateString) {
            $beginDate = new \DateTime($this->beginDateString);

            $query->andFilterWhere(['>', 't.begin_date', $beginDate->getTimestamp()]);

            $beginDate->modify('+ 1 day');
            $query->andFilterWhere(['<', 't.begin_date', $beginDate->getTimestamp()]);
        }

        if ($this->endDateString) {
            $endDate = new \DateTime($this->endDateString);

            $query->andFilterWhere(['>', 't.end_date', $endDate->getTimestamp()]);

            $endDate->modify('+ 1 day');
            $query->andFilterWhere(['<', 't.end_date', $endDate->getTimestamp()]);
        }

        $query->joinWith(['categories']);
        $query->distinct();

        return $dataProvider;
    }
}
