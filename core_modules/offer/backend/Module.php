<?php

namespace backend\modules\offer;

use common\components\interfaces\CoreModuleBackendInterface;

class Module extends \yii\base\Module implements CoreModuleBackendInterface
{
    public $controllerNamespace = 'backend\modules\offer\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
