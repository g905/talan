<?php
namespace backend\modules\contact;

use common\components\interfaces\CoreModuleBackendInterface;

/**
 * Class Module
 *
 * @package backend\modules\contact
 */
class Module extends \yii\base\Module implements CoreModuleBackendInterface
{
    /**
     * @var string
     */
    public $controllerNamespace = 'backend\modules\contact\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
