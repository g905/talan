<?php

namespace backend\modules\contact\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use backend\components\TranslateableTrait;
use backend\components\BackendModel;
use creocoder\translateable\TranslateableBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%contact}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property integer $position
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ContactTranslation[] $translations
 */
class Contact extends ActiveRecord implements BackendModel, Translateable
{
    use TranslateableTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contact}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['position', 'published'], 'integer'],
            [['label', 'email', 'phone'], 'string', 'max' => 255],
            [['address'], 'string'],
            [['email'], 'email'],
            [['position'], 'default', 'value' => 0],
            [['published'], 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/contact', 'ID'),
            'label' => Yii::t('back/contact', 'Label'),
            'address' => Yii::t('back/contact', 'Address'),
            'phone' => Yii::t('back/contact', 'Phone'),
            'position' => Yii::t('back/contact', 'Position'),
            'published' => Yii::t('back/contact', 'Published'),
            'created_at' => Yii::t('back/contact', 'Created At'),
            'updated_at' => Yii::t('back/contact', 'Updated At'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'address',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContactTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/contact', 'Contact');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
//                    'address',
                    'phone',
                    'email',
                    'position',
                    'published:boolean',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    'address',
                    'email',
                    'phone',
                    'position',
                    'published:boolean',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ContactSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'address' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA,
            ],
            'phone' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'email' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }
}
