<?php
use kartik\datecontrol\Module;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'name' => 'Talan',
    'language' => 'ru',
    'layout' => 'oneUi',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'config', 'fileProcessor','checkFtpMessage'],
    'modules' => \yii\helpers\ArrayHelper::merge([
		'api' => [
			'class' => '\backend\modules\api\Module',
		],
        'admin' => [
            'class' => '\backend\modules\admin\Module',
        ],
        'agents' => [
            'class' => '\backend\modules\agents\Module',
        ],
        'menu' => [
            'class' => 'backend\modules\menu\Module',
        ],
        'configuration' => [
            'class' => 'backend\modules\configuration\Module',
        ],
        'i18n' => [
            'class' => \vintage\i18n\Module::className(),
            'controllerMap' => [
                'default' => 'backend\modules\language\controllers\TranslationController',
            ]
        ],
        'language' => [
            'class' => 'backend\modules\language\Module',
        ],
        'meta' => [
            'class' => 'notgosu\yii2\modules\metaTag\Module',
            'controllerMap' => [
                'tag' => 'backend\modules\seo\controllers\TagController',
            ]
        ],
        'redirects' => [
            'class' => 'backend\modules\redirects\Module',
        ],
        'seo' => [
            'class' => 'backend\modules\seo\Module',
        ],
        'imagesUpload' => [
            'class' => 'backend\modules\imagesUpload\ImagesUploadModule',
        ],
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module',
            'displaySettings' => [
                Module::FORMAT_DATE => 'dd-MM-yyyy',
                Module::FORMAT_DATETIME => 'dd-MM-yyyy HH:mm',
            ],
        ],
        'builder' => [
            'class' => 'backend\modules\builder\Module',
            'models' => [
                'common\models\builder\BlogHtmlBuilderModel',
                'common\models\builder\BlogDoubleImageBuilderModel',
                'common\models\builder\BlogYoutubeVideoBuilderModel',
                'common\models\builder\BlogSliderBuilderModel',
                'common\models\builder\BlogImageWithTextBuilderModel',
                // Public data building blocks
                'common\models\builder\publicData\ProudFigure',
                // 'common\models\builder\publicData\DiagramTable', // unused
                // 'common\models\builder\publicData\DiagramSection', // unused
                'common\models\builder\publicData\ColumnTable',
                'common\models\builder\publicData\ColumnSection',
                'common\models\builder\publicData\QuantityDdu',
                'common\models\builder\publicData\ContentSection',
                'common\models\builder\publicData\Schedule',
                'common\models\builder\publicData\ServiceLevel',
                'common\models\builder\publicData\RatingSection',
                'common\models\builder\publicData\FilesSection',
                'common\models\builder\publicData\CustomDduSection',
                // 'common\models\builder\StringBuilderModel',
                // 'common\models\builder\TextBuilderModel',
                // 'common\models\builder\ImageBuilderModel',
            ],
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'page' => [
            'class' => 'backend\modules\page\Module',
        ],
        'contacts' => [
            'class' => 'backend\modules\contacts\Module',
        ],
        'request' => [
            'class' => 'backend\modules\request\Module',
        ],
        'apartment' => [
            'class' => 'backend\modules\apartment\Module',
        ],
        'analytics' => [
            'class' => 'backend\modules\analytics\Module',
        ],
        'ftpModel' => [
            'class' => 'backend\modules\ftpModel\Module',
        ],
        'blog' => [
            'class' => 'backend\modules\blog\Module',
        ],
        'bank' => [
            'class' => 'backend\modules\bank\Module',
        ],
        'team' => [
            'class' => 'backend\modules\team\Module',
        ],
        'partner' => [
            'class' => 'backend\modules\partner\Module',
        ],
        'hr' => [
            'class' => 'backend\modules\hr\Module',
        ],
        'resale' => [
            'class' => 'backend\modules\resale\Module',
        ],
    ],
        require(__DIR__ . '/core_modules/modules.php')
    ),
    'components' => [
        'config' => [
            'class' => '\common\components\ConfigurationComponent',
        ],
        'user' => [
            'loginUrl' => ['/admin/default/login'],
            'as authLog' => [
                'class' => 'common\components\IpAuthLogWebUserBehavior'
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'checkFtpMessage' => [
            'class' => '\backend\modules\ftpModel\components\CheckFtpMessage'
        ],
        'errorHandler' => [
            'errorAction' => 'error/error',
        ],
        'urlManager' => [
            'enableLocaleUrls' => false,
            //'languages' => ['ru'],
            'rules' => [
                '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                '<controller>/<action>' => '<controller>/<action>',
                '' => 'site/index',
            ],
        ],
        'assetManager' => [
            'linkAssets' => true,
            'appendTimestamp' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => ['https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js']
                ],
                'metalguardian\dateTimePicker\Asset' => [
                    'sourcePath' => '@bower/datetimepicker'
                ],
                'dosamigos\selectize\SelectizeAsset' => [
                    'css' => ['css/selectize.default.css'],
                    'depends' => ['yii\web\JqueryAsset']
                ],
            ],
        ],
        'formatter' => [
            'class' => '\backend\components\Formatter',
            'datetimeFormat' => 'php:d.m.y H:i:s',
        ],
    ],
    'params' => $params,
];
