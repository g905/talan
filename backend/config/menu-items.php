<?php

use common\models\PublicDataWidget;

return [
    require(__DIR__ . '/core_modules/menu-items.php'),
    [
        'label' => Yii::t('back/menu', 'property'),
        'icon' => '<i class="fa fa-building-o"></i>',
        'items' => [
            [
                'label' => Yii::t('back/menu', 'constructor'),
                'items' => [
                    [
                        'label' => Yii::t('back/menu', 'constructor items'),
                        'url' => ['/apartment/apartment-constructor/index'],
                    ],
                    [
                        'label' => Yii::t('back/menu', 'constructor price'),
                        'url' => ['/apartment/apartment-constructor-price/index'],
                    ],
                    [
                        'label' => Yii::t('back/menu', 'constructor option price'),
                        'url' => ['/apartment/apartment-constructor-option-price/index'],
                    ],
                ],
            ],
            [
                'label' => Yii::t('back/menu', 'property complex'),
                'url' => ['/apartment/apartment-complex/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'complex map'),
                'url' => ['/apartment/apartment-map/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'property building'),
                'url' => ['/apartment/apartment-building/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'property entrance'),
                'url' => ['/apartment/apartment-entrance/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'property objects'),
                'url' => ['/apartment/apartment/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'how buy'),
                'url' => ['/apartment/how-buy/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'promo action'),
                'url' => ['/apartment/promo-action/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'Completed projects'),
                'url' => ['/apartment/completed-project/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'Benefits'),
                'url' => ['/apartment/benefit/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'Secondary real estate'),
                'url' => ['/resale/secondary-real-estate/index'],
            ],
        ],
    ],
//    [
//        'label' => Yii::t('back/menu', 'apartment'),
//        'icon' => '<i class="si si-home"></i>',
//        'items' => [
//            [
//                'label' => Yii::t('back/menu', 'apartment complex'),
//                'url' => ['/apartment/apartment-complex/index'],
//            ],
//            [
//                'label' => Yii::t('back/menu', 'apartment'),
//                'url' => ['/apartment/apartment/index'],
//            ],
//            [
//                'label' => Yii::t('back/menu', 'how buy'),
//                'url' => ['/apartment/how-buy/index'],
//            ],
//            [
//                'label' => Yii::t('back/menu', 'promo action'),
//                'url' => ['/apartment/promo-action/index'],
//            ],
//        ],
//    ],
    [
        'label' => Yii::t('back/menu', 'pages'),
        'icon' => '<i class="si si-notebook"></i>',
        'items' => [
        	[
        		'label' => Yii::t('back/menu', 'Agents'),
        		'url' => ['/agents/agents/index'],
        	],
            [
                'label' => Yii::t('back/menu', 'Catalog'),
                'url' => ['/page/catalog/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'property complex list'),
                'url' => ['/apartment/complex-list/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'home page'),
                'url' => ['/page/home/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'about company page'),
                'url' => ['/page/about-company/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'Contact page'),
                'url' => ['/page/contact-page/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'custom page'),
                'url' => ['/page/custom-page/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'Installment plan page'),
                'url' => ['/page/installment-plan/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'Hypothec'),
                'items' => [
                    [
                        'label' => Yii::t('back/menu', 'Hypothec page'),
                        'url' => ['/page/hypothec/index'],
                    ],
                    [
                        'label' => Yii::t('back/menu', 'Banks'),
                        'url' => ['/bank/bank/index'],
                    ],
                ],
            ],
            [
                'label' => Yii::t('back/menu', 'Public data'),
                'items' => [
                    [
                        'label' => Yii::t('back/menu', 'Public data menu'),
                        'url' => ['/page/public-data-menu/index'],
                    ],
                    [
                        'label' => Yii::t('back/menu', 'Public data pages'),
                        'url' => ['/page/public-data/index'],
                    ],
                    [
                        'label' => Yii::t('back/menu', 'Public data widgets'),
                        'items' => [
                            [
                                'label' => Yii::t('back/menu', 'Column row'),
                                'url' => ['/page/public-data-column-row/index'],
                            ],
                            /*[
                                'label' => Yii::t('back/menu', 'Diagram row'),
                                'url' => ['/page/public-data-diagram-row/index'],
                            ],*/
                            [
                                'label' => Yii::t('back/menu', 'Diagram chart'),
                                'url' => ['/page/public-data-diagram-chart/index'],
                            ],
                            [
                                'label' => Yii::t('back/menu', 'Diagram schedule'),
                                'url' => ['/page/public-data-diagram-schedule/index'],
                            ],
                            [
                                'label' => Yii::t('back/menu', 'Service chart row'),
                                'url' => ['/page/public-data-service-chart-row/index'],
                            ],
                            /*[
                                'label' => Yii::t('back/menu', 'Diagram circle'),
                                'url' => ['/page/public-data-diagram-circle/index'],
                            ],*/
                            [
                                'label' => Yii::t('back/menu', 'Rating table'),
                                'url' => ['/page/public-data-rating-table/index'],
                            ],
                            [
                                'label' => Yii::t('back/menu', 'Custom ddu'),
                                'url' => ['/page/public-data-custom-ddu/index'],
                            ],
                        ],
                    ],
                ],
            ],
            [
                'label' => Yii::t('back/menu', 'Build history'),
                'items' => [
                    [
                        'label' => Yii::t('back/menu', 'Build history complex'),
                        'items' => [
                            [
                                'label' => Yii::t('back/menu', 'Build history apartment complex'),
                                'url' => ['/apartment/build-history-apartment-complex/index'],
                            ],
                            [
                                'label' => Yii::t('back/menu', 'Build history apartment complex month'),
                                'url' => ['/apartment/build-history-apartment-complex-item/index'],
                            ],
                            [
                                'label' => Yii::t('back/menu', 'Build history apartment complex slide'),
                                'url' => ['/apartment/build-history-apartment-complex-item-slide/index'],
                            ],
                        ],
                    ],
                    [
                        'label' => Yii::t('back/menu', 'Build history building'),
                        'items' => [
                            [
                                'label' => Yii::t('back/menu', 'Build history apartment building'),
                                'url' => ['/apartment/build-history-apartment-building/index'],
                            ],
                            [
                                'label' => Yii::t('back/menu', 'Build history apartment building month'),
                                'url' => ['/apartment/build-history-apartment-building-item/index'],
                            ],
                            [
                                'label' => Yii::t('back/menu', 'Build history apartment building slide'),
                                'url' => ['/apartment/build-history-apartment-building-item-slide/index'],
                            ],
                        ],
                    ],
                ]
            ],
            [
                'label' => Yii::t('back/menu', 'Team page'),
                'items' => [
                    [
                        'label' => Yii::t('back/menu', 'Team'),
                        'url' => ['/team/team/index']
                    ],
                    [
                        'label' => Yii::t('back/menu', 'Default city settings'),
                        'url' => ['/team/team-page-default-city-tab/update']
                    ]
                ],
            ],
            [
                'label' => Yii::t('back/menu', 'Partners'),
                'url' => ['/partner/partner-page/index']
            ],
            [
                'label' => Yii::t('back/menu', 'Guarantee'),
                'url' => ['/page/guarantee-page/index']
            ],
            [
                'label' => Yii::t('back/menu', 'Land purchase'),
                'url' => ['/page/earth/index']
            ],
            [
                'label' => Yii::t('back/menu', 'Secondary page'),
                'url' => ['/page/buy-page/index']
            ],
            [
                'label' => Yii::t('back/menu', 'Realtors page'),
                'url' => ['/page/realtor/index']
            ],
            [
                'label' => Yii::t('back/menu', 'How to buy page'),
                'url' => ['/page/how-buy-page/index']
            ],
        ],
    ],
    [
        'label' => Yii::t('back/menu', 'Blog'),
        'icon' => '<i class="fa fa-newspaper-o"></i>',
        'items' => [
            [
                'label' => Yii::t('back/menu', 'Themes'),
                'url' => ['/blog/blog-theme/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'Articles'),
                'url' => ['/blog/article/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'Blog list page'),
                'url' => ['/blog/blog-list-page/update'],
            ],
        ]
    ],
    [
        'label' => Yii::t('back/menu', 'HR'),
        'icon' => '<i class="fa fa-address-card-o"></i>',
        'items' => [
            [
                'label' => Yii::t('back/menu', 'career page'),
                'url' => ['/hr/career/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'specialization page'),
                'url' => ['/hr/specialization/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'vacancies page'),
                'url' => ['/hr/vacancies/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'vacancy'),
                'url' => ['/hr/vacancy/index'],
            ],
        ]
    ],
    [
        'label' => Yii::t('back/menu', 'menu'),
        'icon' => '<i class="si si-list"></i>',
        'items' => [
            [
                'label' => Yii::t('back/menu', 'site menu'),
                'url' => ['/menu/site-menu/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('back/menu', 'contacts'),
        'icon' => '<i class="si si-call-end"></i>',
        'items' => [
            [
                'label' => Yii::t('back/menu', 'city'),
                'url' => ['/contacts/city/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'managers'),
                'url' => ['/contacts/manager/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('back/menu', 'requests'),
        'icon' => '<i class="si si-envelope-letter"></i>',
        'items' => [
            [
                'label' => Yii::t('back/menu', 'Request prices'),
                'url' => ['/request/request-excursion/index?type=1'],
            ],
            [
                'label' => "Запросы на бронь",
                'url' => ['/request/request-excursion/index?type=2'],
            ],
            [
                'label' => Yii::t('back/menu', 'question request'),
                'url' => ['/request/request-question/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'callback request'),
                'url' => ['/request/request-callback/index'],
            ],
			[
				'label' => Yii::t('back/menu', 'Request for full amount'),
				'url' => ['/request/request-full-amount/index'],
			],
            [
                'label' => Yii::t('back/menu', 'Hypothec request'),
                'url' => ['/request/request-hypothec/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'Instalment plan request'),
                'url' => ['/request/request-instalment-plan/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'commercial request'),
                'url' => ['/request/request-commercial/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'complex presentation request'),
                'url' => ['/request/request-complex-presentation/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'Job request'),
                'url' => ['/request/request-job/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'subscribe request'),
                'url' => ['/request/subscribe-request/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'guarantee request'),
                'url' => ['/request/request-guarantee/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'secondary request'),
                'url' => ['/request/request-secondary/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'request cooperation'),
                'url' => ['/request/request-cooperation/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'Custom popup'),
                'items' => [
                    [
                        'label' => Yii::t('back/menu', 'custom popup request'),
                        'url' => ['/request/request-custom/index'],
                    ],
                    [
                        'label' => Yii::t('back/menu', 'custom popups'),
                        'url' => ['/request/custom-popup/index'],
                    ],
                ]
            ],
            [
                'label' => Yii::t('back/menu', 'Setup'),
                'items' => [
                    [
                        'label' => Yii::t('back/menu', 'Problem Type'),
                        'url' => ['/request/request-problem/index'],
                    ],
                ]
            ],
            [
                'label' => Yii::t('back/menu', 'constructor request'),
                'url' => ['/request/constructor-request/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'partner request'),
                'url' => ['/request/request-partner/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'Custom guarantee'),
                'url' => ['/request/request-custom-guarantee/index'],
            ],
        ]
    ],
    [
        'label' => Yii::t('back/menu', '1C synchronize'),
        'icon' => '<i class="si si-refresh"></i>',
        'items' => [
            [
                'label' => Yii::t('back/menu', 'log'),
                'url' => ['/ftpModel/ftp-log/log'],
            ],
            [
                'label' => Yii::t('back/menu', 'synchronization'),
                'url' => ['/ftpModel/ftp/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('back/menu', 'User'),
        'icon' => '<i class="si si-user"></i>',
        'items' => [
            [
                'label' => Yii::t('back/menu', 'user'),
                'url' => ['/admin/user/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'IP log'),
                'url' => ['/admin/ip-auth-log/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'IP block'),
                'url' => ['/admin/ip-block/index'],
            ],
        ]
    ],
	[
		'label' => Yii::t('back/menu', 'Export from site'),
		'icon' => '<i class="fa fa-tasks"></i>',
		'items' => [
			[
				'label' => Yii::t('back/menu', 'Agencies'),
				'url' => ['/api/api-agency/index'],
			],
			[
				'label' => Yii::t('back/menu', 'Statistic'),
				'items' => [
					[
						'label' => Yii::t('back/api', 'Lead list'),
						'url' => ['/api/stat/lead'],
					],
					[
						'label' => Yii::t('back/menu', 'Widget Visits'),
						'url' => ['/api/stat/widget-visits'],
					],
					[
						'label' => Yii::t('back/menu', 'Conversion'),
						'url' => ['/api/stat/conversion'],
					],
				],
			],
		],
	],
    [
        'label' => Yii::t('back/menu', 'settings'),
        'icon' => '<i class="si si-settings"></i>',
        'items' => [
            [
                'label' => Yii::t('back/menu', 'configurations'),
                'url' => ['/configuration/default/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'page seo'),
                'url' => ['/seo/page-seo/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'internationalization'),
                'items' => [
                    [
                        'label' => Yii::t('back/menu', 'translations'),
                        'url' => ['/i18n/default/index'],
                    ],
                    [
                        'label' => Yii::t('back/menu', 'languages'),
                        'url' => ['/language/language/index'],
                    ],
                ],
            ],
            [
                'label' => Yii::t('back/menu', 'refresh side menu'),
                'url' => ['/menu/menu/refresh-side-menu'],
            ],
        ],
    ],
];
