<?php

namespace backend\controllers;

use metalguardian\fileProcessor\helpers\FPM;
use wapmorgan\UnifiedArchive\UnifiedArchive;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\captcha\CaptchaAction;
use common\models\User;
use common\components\model\ActiveRecord;
use backend\modules\page\models\PublicDataWidget;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['captcha'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'ajax-checkbox', 'delete-file', 'test', 'list-model-select2', 'list-model-selectize', 'list-public-data-widget'],
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'captcha' => [
                'class' => CaptchaAction::class
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTest()
    {

        $file = getAlias('@webroot') . '/uploads/1.zip';
        $path = getAlias('@webroot') . '/uploads/data';
        $archive = UnifiedArchive::open($file);
        $archive->extractNode($path);
        dd($archive->getFileNames(), 10, true);


        $path = getAlias('@webroot') . '/uploads';


        $zip = new \ZipArchive();
        $res = $zip->open($file);
        if ($res === true) {
            // extract it to the path we determined above
            $zip->extractTo($path);
            $zip->close();
            echo "WOOT! $file extracted to $path";
        } else {
            echo "Doh! I couldn't open $file";
        }

        die;
    }

    public function actionAjaxCheckbox()
    {
        $modelID = request()->post('modelId');
        $modelName = request()->post('modelName');
        $attribute = request()->post('attribute');

        if (request()->getIsAjax() && $modelID && $modelName && $attribute) {
            $model = $modelName::findOne($modelID);
            if ($model) {
                $model->$attribute = $model->$attribute ? 0 : 1;
                $model->save(false);
            }
        }
    }

    public function actionDeleteFile()
    {
        $modelID = request()->post('modelId');
        $modelName = request()->post('modelName');
        $attribute = request()->post('attribute');
        $language = request()->post('language');

        if (request()->isAjax && $modelID && $modelName && $attribute) {
            $error = true;
            /** @var $model ActiveRecord */
            if ($language) {
                $model = $modelName::find()->where(['model_id' => $modelID, 'language' => $language])->one();
            } else {
                $model = $modelName::findOne($modelID);
            }
            if ($model) {
                $fileId = $model->$attribute;
                $model->$attribute = null;
                if ($model->save(false)) {
                    FPM::deleteFile($fileId);
                    $error = false;
                }
            }

            return Json::encode(['error' => $error]);
        }

        return false;
    }

    /**
     * Universal action to fetch data, prepared for select2 plugin.
     *
     * @param string $type model type (full namespace).
     * @param string $key key attribute name.
     * @param string $label label attribute name.
     * @param string $search search query.
     * @return array json formatted.
     */
    public function actionListModelSelect2($type, $key = 'id', $label = 'label', $search = '')
    {
        response()->format = Response::FORMAT_JSON;
        /** @var ActiveRecord $type */
        $query = $type::find()->select(["$key id", "$label text"])->isPublished()->asArray();

        if ($search) {
            $query->andWhere(['like', $label, $search]);
        }

        return [
            'results' => $query->all()
        ];
    }

    /**
     * Universal action to fetch data, prepared for select2 plugin.
     *
     * @param string $type model type (full namespace).
     * @param string $key key attribute name.
     * @param string $label label attribute name.
     * @param string $search search query.
     * @return array json formatted.
     */
    public function actionListModelSelectize($type, $key = 'id', $label = 'label', $search = '')
    {
        response()->format = Response::FORMAT_JSON;
        /** @var ActiveRecord $type */
        $query = $type::find()->select([$key, $label])->isPublished()->asArray();

        if ($search) {
            $query->andWhere(['like', $label, $search]);
        }

        return $query->all();
    }

    /**
     * @param string $class
     * @param int $type
     * @param string $key
     * @param string $label
     * @param string $search
     * @param bool $city
     * @param bool $complex
     * @return \backend\modules\page\models\PublicDataWidget[]
     */
    public function actionListPublicDataWidget(string $class, int $type, $key = 'id', $label = 'label', $city = false, $complex = false, $search = '')
    {
        response()->format = Response::FORMAT_JSON;
        /** @var PublicDataWidget $class */
        $query = $class::find()->alias('widget')->andWhere(['widget.type' => $type, 'widget.published' => 1]);

        if ($city) {
            $query->joinWith(['city city']);
            $query->select(["widget.$key", "concat(city.label, ': ', widget.$label) as $label", 'city_id']);
        }

        if ($complex) {
            $query->joinWith(['complex complex']);
            $query->select(["widget.$key", "concat(complex.label, ': ', widget.$label) as $label", 'complex_id']);
        }

        if (!$city && !$complex) {
            $query->select(["widget.$key", $label]);
        }

        if ($search) {
            $query->andWhere(['like', $label, $search]);
        }

        return $query->all();
    }
}
