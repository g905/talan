<?php
/**
 * Created by PhpStorm.
 * User: anatolii
 * Date: 02.04.16
 * Time: 23:45
 */

namespace backend\components;

use backend\modules\configuration\components\ConfigurationModel;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use metalguardian\fileProcessor\behaviors\UploadBehavior;
use Yii;
use yii\base\ErrorException;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class RelatedFormTrait
 *
 * @package backend\components
 */
trait RelatedFormTrait
{
    /**
     * Action for RelatedFormWidget
     *
     * @param $model ActiveRecord
     * @param $config array
     *
     * @return array|string|Response
     * @throws Exception
     */
    public function relatedFormAction($model, $config)
    {
        /** @var ActiveRecord[][] $relModelsArray */
        $relModelsArray = $model->relModels;
        $model->relModels = [];
        $valid = true;
        $flag = false;
        $isLoadModels = false;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // validate and save main model
            if ($isLoadModels = $this->loadModels($model)) {
                $valid = $model->validate() && $valid;
                $flag = $model->save(false);
            }
            foreach ($relModelsArray as $key => $relModels) {
                $relatedModel = $relModels[0];
                $relatedModelClassName = $relatedModel::className();
                $deletedIDs = [];
                if ($isLoadModels) {
                    $oldIDs = ArrayHelper::map($relModels, 'id', 'id');
                    $relModels = Model::createMultiple($relatedModelClassName, Yii::$app->request->post(), $relModels);
                    $this->loadMultipleModels($relModels);
                    $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($relModels, 'id', 'id')));
                    $this->attachUploadBehavior($config, $key, $relModels);
                    // ajax validation
                    if (Yii::$app->request->isAjax) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return ArrayHelper::merge(
                            ActiveForm::validateMultiple($relModels),
                            ActiveForm::validate($model)
                        );
                    }
                    try {
                        // validate all related model models with main model
                        $valid = Model::validateMultiple($relModels) && $valid;
                    } catch (ErrorException $e) {
                        \common\helpers\Dump::dump($e);
                    }

                    if ($valid) {
                        Model::saveRelModels($model, $relModels, $config, $key, $deletedIDs, $relatedModel, $flag);
                    }
                    if (!empty($deletedIDs)) {
                        $relModels = ArrayHelper::merge($relModels, [new $relatedModelClassName]);
                    }
                }
                if (empty($deletedIDs) && !$relatedModel->isNewRecord) {
                    $relModels = ArrayHelper::merge($relModels, [new $relatedModelClassName]);
                }

                $model->relModels[$key] = $relModels;
                foreach ($relModels as $index => $item) {
                    $item->relModelIndex = $index;
                }
            }

            if ($flag && $valid) {
                $transaction->commit();
            } elseif ($isLoadModels) {
                throw new Exception('Models can not be saved');
            } else {
                $transaction->rollBack();
            }
        } catch (Exception $e) {
            logError($e->getMessage());
            $transaction->rollBack();
            if (Yii::$app->controller->action->id == 'create') {
                $this->setToNewRecordState($model);
            }
        }
        $view = '//templates/update';
        if ($model->isNewRecord) {
            $view = '//templates/create';
        }
        if ($model instanceof ConfigurationModel) {
            $view = '@app/modules/configuration/views/configuration/templates/update';
        }

        if ($flag && $valid) {
            $message = 'Record successfully updated!';
            if (Yii::$app->controller->action->id == 'create') {
                $message = 'Record successfully created!';
            }
            \Yii::$app->getSession()->setFlash('info', Yii::t('app', $message));

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render($view, [
            'model' => $model
        ]);
    }

    /**
     * @param $model ActiveRecord
     */
    public function setToNewRecordState($model)
    {
        $model->isNewRecord = true;
        /** @var ActiveRecord[] $relModels */
        foreach ($model->relModels as $relModels) {
            foreach ($relModels as $modelRel) {
                $modelRel->isNewRecord = true;
            }
        }
    }

    /**
     * @param $config array
     * @param $key string
     * @param $relModels ActiveRecord[]
     */
    public function attachUploadBehavior($config, $key, &$relModels)
    {
        if (isset($config[$key]['uploadBehavior'])) {
            foreach ($config[$key]['uploadBehavior'] as $behavior) {
                foreach ($relModels as $index => $modelRel) {
                    $modelRel->attachBehaviors([
                        $behavior['attribute'] => [
                            'class' => UploadBehavior::className(),
                            'attribute' => "[$index]" . $behavior['attribute'],
                            'validator' => [
                                'extensions' => $behavior['extensions'],
                            ],
                            'required' => $behavior['required'],
                        ],
                    ]);
                    if ($modelRel instanceof Translateable && $modelRel->isTranslateAttribute($behavior['attribute'])) {
                        foreach ($modelRel->translations as $langModel) {
                            $langModel->attachBehaviors([
                                $behavior['attribute'] => [
                                    'class' => UploadBehavior::className(),
                                    'attribute' => "[$index][$langModel->language]" . $behavior['attribute'],
                                    'validator' => [
                                        'extensions' => $behavior['extensions'],
                                    ],
                                    'required' => false,
                                ],
                            ]);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $model ActiveRecord
     *
     * @return array
     */
    public function getRelatedFormActionConfig($model)
    {
        $formConfig = $model->getRelatedFormConfig();
        $return = [];
        foreach ($formConfig as $config) {
            $relation = $config['relation'];
            $className = $model->getRelation($relation)->modelClass;
            $relatedModels = $model->$relation;
            if ($relatedModels && !is_array($relatedModels)) {
                $relatedModels = $relatedModels->all();
            }
            $model->relModels[$relation] = $relatedModels ? $relatedModels : [new $className];
            $return[$relation] = $config;
        }

        return $return;
    }

    /**
     * Action for RelatedFormWidget, generates new form row
     *
     * @return string
     */
    public function actionGetNewRow()
    {
        $request = Yii::$app->request;
        $className = $request->post('className');
        $container = $request->post('container');
        $index = $request->post('index');
        $relModels[$index] = new $className;
        $relModels[$index]->relModelIndex = $index;
        $showTabs = \Yii::$app->config->get('languageTabs');
        $showTabs ? $rowView = '//templates/_related_form_fields_lang' : $rowView = '//templates/_related_form_fields';
        return Json::encode([
            'replaces' => [
                [
                    'what' => ".$container .content-append:last-child",
                    'data' => $this->renderAjax($rowView, ['relModels' => $relModels])
                ]
            ]
        ]);
    }
}
