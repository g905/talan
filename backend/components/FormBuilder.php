<?php

namespace backend\components;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;
use metalguardian\fileProcessor\helpers\FPM;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\builder\models\BuilderConfig;
use backend\modules\configuration\models\Configuration;
use backend\modules\builder\widgets\builderWidget\BuilderFieldWidget;

/**
 * Class FormBuilder
 *
 * @author anatolii
 * @package backend\components
 */
class FormBuilder extends ActiveFormBuilder
{
    /**
     * @var string additional input types.
     */
    const INPUT_TEXT_WITH_ICON = 'textInputWithIcon';

    public function renderUploadedFile($model, $attribute, $element, $language = null)
    {
        $content = '';
        if ($element['type'] == static::INPUT_FILE && isset($model->$attribute) && $model->$attribute) {
            $file = FPM::transfer()->getData($model->$attribute);
            $content .= Html::beginTag('div', ['class' => 'file-name']);
            $content .= Html::button(Yii::t('app', 'Delete file'), [
                'class' => 'delete-file',
                'data' => [
                    'modelName' => $model->className(),
                    'modelId' => $language ? $model->model_id : $model->id,
                    'attribute' => $attribute,
                    'language' => $language
                ]
            ]);
            $content .= Formatter::getFileLink($file);
            $content .= Html::endTag('div');
        }
        return $content;
    }

    public function prepareRows($model, $formConfig, $translationModels, $tabs = false)
    {
        $content = null;
        $showTabs = Yii::$app->config->get('languageTabs');
        $showTabs ? $rowView = '_form_row_lang' : $rowView = '_form_row';

        foreach ($formConfig as $attribute => $element) {
            if ($element instanceof BuilderConfig) {
                $content = BuilderFieldWidget::widget([
                    'model' => $model,
                    'attribute' => $attribute,
                    'element' => $element,
                    'form' => $this
                ]);
            } elseif (isset($element['relation']) && $model->relModels) {
                $limitMax = $element['limitMax'] ?? 0;
                $limitMin = $element['limitMin'] ?? 0;
                if ($tabs) {
                    $content .= $this->render('//templates/_related_form', [
                        'relModels' => $model->relModels[$element['relation']],
                        'form' => $this,
                        'showTabs' => $showTabs,
                        'limitMax' => $limitMax,
                        'limitMin' => $limitMin,
                    ]);
                } else {
                    foreach ($model->relModels as $relModels) {
                        $content .= $this->render('//templates/_related_form', [
                            'relModels' => $relModels,
                            'form' => $this,
                            'showTabs' => $showTabs,
                            'limitMax' => $limitMax,
                            'limitMin' => $limitMin,
                        ]);
                    }
                }
            } else {

                $view = !is_array($element)
                    ? '@app/modules/configuration/views/configuration/templates/' . $rowView
                    : '//templates/' . $rowView;

                $content .= $this->render($view, [
                    'model' => $model,
                    'attribute' => $attribute,
                    'element' => $element,
                    'translationModels' => $translationModels,
                    'form' => $this
                ]);
            }
        }

        return $content;
    }

    /**
     * @inheritdoc
     */
    public function renderField(\yii\base\Model $model, $attribute, array $settings = [])
    {
        $label = ArrayHelper::getValue($settings, 'label');
        if ($model instanceof Configuration && is_array($model->type)) {
            $settings = $model->type;
            if ($label !== null) {
                $settings['label'] = $label;
            }
        }
        $fieldOptions = ArrayHelper::getValue($settings, 'fieldOptions', []);
        $field = $this->field($model, $attribute, $fieldOptions);

        if ($label !== null) {
            $field->label($label, ArrayHelper::getValue($settings, 'labelOptions', []));
        }
        if (($hint = ArrayHelper::getValue($settings, 'hint')) !== null) {
            $field->hint($hint, ArrayHelper::getValue($settings, 'hintOptions', []));
        }

        $type = ArrayHelper::getValue($settings, 'type', static::INPUT_TEXT);
        $this->prepareField($field, $type, $settings);

        return $field;
    }

    protected function prepareField($field, $type, array $settings)
    {
        $options = ArrayHelper::getValue($settings, 'options', []);
        switch ($type) {
            case static::INPUT_HIDDEN:
            case static::INPUT_TEXT:
            case static::INPUT_TEXTAREA:
            case static::INPUT_PASSWORD:
            case static::INPUT_FILE:
                $field->$type($options);
                break;

            case static::INPUT_TEXT_WITH_ICON:
                $customType = static::INPUT_TEXT;
                $field->template = '';
                if (isset($options['iconLeftContent'])){
                    $field->template .= '{label}';
                }
                $field->template = '<div class="input-group">';

                if (!isset($options['iconLeftContent'])){
                    $field->template .= '<span class="input-group-addon">{label}</span>';
                }
                else{
                    if ((isset($options['iconLeftContent'])) && ($options['iconLeftContent'] != '')){
                        $field->template .= '<span class="input-group-addon">'.$options['iconLeftContent'].'</span>';
                    }
                }

                $field->template .= '{input}';
                if ((isset($options['iconRightContent'])) && ($options['iconRightContent'] != '')){
                    $field->template .= '<span class="input-group-addon">'.$options['iconRightContent'].'</span>';
                };

                $field->template .= '</div>{hint}{error}';
                $field->$customType($options);
                break;

            case static::INPUT_DROPDOWN_LIST:
            case static::INPUT_LIST_BOX:
            case static::INPUT_CHECKBOX_LIST:
            case static::INPUT_RADIO_LIST:
                $items = ArrayHelper::getValue($settings, 'items', []);
                $field->$type($items, $options);
                break;

            case static::INPUT_RADIO:
                $enclosedByLabel = ArrayHelper::getValue($settings, 'enclosedByLabel', true);
                $field->$type($options, $enclosedByLabel);
                break;

            case static::INPUT_CHECKBOX:
                $field->template = '{label}  <label class="css-input switch switch-sm switch-primary switch-label">{input}<span></span></label>{hint}{error}';
                $field->$type($options, false);
                break;

            case static::INPUT_HTML5:
                $html5type = ArrayHelper::getValue($settings, 'html5type', 'text');
                $field->$type($html5type, $options);
                break;

            case static::INPUT_WIDGET:
                $widgetClass = $this->getWidgetClass($settings);
                $field->$type($widgetClass, $options);
                break;

            case static::INPUT_RAW:
                $field->parts['{input}'] = $this->getValue($settings);
                break;

            default:
                throw new InvalidConfigException("Invalid input type '{$type}' configured for the attribute.");
        }
    }
}
