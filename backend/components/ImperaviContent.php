<?php
namespace backend\components;


use backend\assets\AdvRedactorAsset;
use vova07\imperavi\Widget;
use yii\helpers\Url;

class ImperaviContent extends Widget
{
    public function init()
    {
        parent::init();
        $settings = array_merge($this->getDefaultSettings(), $this->settings);
        $this->settings = $settings;
        $this->plugins = ['advLink' => AdvRedactorAsset::class];
    }

    public function getDefaultSettings()
    {
        return [
            'lang' => 'ru',
            'buttons' => [
                'html',
                'formatting',
                'bold',
                'italic',
                'underline',
                'unorderedlist',
                'orderedlist',
                'image',
                'file',
                'link',
                'alignment',
                'horizontalrule'
            ],
            'pastePlainText' => true,
            'buttonSource' => true,
            'replaceDivs' => false,
            'paragraphize' => true,
            'imageManagerJson' => Url::to(['/imagesUpload/default/images-get']),
            'imageUpload' => Url::to(['/imagesUpload/default/image-upload']),
            'plugins' => [
                'video',
                'table',
                'clips',
                'imagemanager',
                'fullscreen',
            ],
            'minHeight' => 150
        ];
    }
}
