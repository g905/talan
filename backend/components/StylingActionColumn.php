<?php

namespace backend\components;

use Yii;
use yii\helpers\Html;
use yii\grid\ActionColumn;

/**
 * Class ActionColumn
 */
class StylingActionColumn extends ActionColumn
{
    public $template = '{update} {delete}';

    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        $this->initDefaultButton('view', 'eye');
        $this->initDefaultButton('update', 'pencil');
        $this->initDefaultButton('delete', 'times', [
            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method' => 'post',
        ], true);
    }

    /**
     * Initializes the default button rendering callback for single button
     * @param string $name Button name as it's written in template
     * @param string $iconName The part of Bootstrap glyphicon class that makes it unique
     * @param array $additionalOptions Array of additional options
     * @since 2.0.11
     */
    protected function initDefaultButton($name, $iconName, $additionalOptions = [], $isDanger = false)
    {
        if (!isset($this->buttons[$name]) && strpos($this->template, '{' . $name . '}') !== false) {
            $this->buttons[$name] = function ($url, $model, $key) use ($name, $iconName, $additionalOptions, $isDanger) {
                switch ($name) {
                    case 'view':
                        $title = Yii::t('yii', 'View');
                        break;
                    case 'update':
                        $title = Yii::t('yii', 'Update');
                        break;
                    case 'delete':
                        $title = Yii::t('yii', 'Delete');
                        break;
                    default:
                        $title = ucfirst($name);
                }
                $options = array_merge([
                    'title' => $title,
                    'aria-label' => $title,
                    'data-pjax' => '0',
                    'class' => 'btn btn-xs btn-default'
                ], $additionalOptions, $this->buttonOptions);

                if ($isDanger){
                    $icon = Html::tag('span', '', ['class' => "text-danger fa fa-$iconName"]);
                }
                else{
                    $icon = Html::tag('span', '', ['class' => "fa fa-$iconName"]);
                }

                return Html::a($icon, $url, $options);
            };
        }
    }

    public static function createPreviewUrl($url, $model, $rule, $enableAlias = true)
    {

        if (isset($model->city_id)) {
            $url = $url.'&city='.$model->city_id;
        }

        if (isset($model->alias) && $enableAlias) {
            $url = $url.'&alias='.$model->alias;
        }

        if ($rule) {
            $url = $url.'&rule='.$rule;
        }

        return Html::a(Html::tag('span', '',['class' => 'si si-action-redo']), $url, ['class' => 'btn btn-xs btn-default']);
    }
}
