<?php
/**
 * This is the template for generating the model class of a specified table.
 */
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator backend\components\gii\crud\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */
/* @var $multiLanguageModel */
/* @var $translationModel boolean */
/* @var $behaviors string[] list of behaviors */
/* @var $translationAttributes string[] list of translated attributes */

echo "<?php\n";
?>

namespace <?= $generator->ns ?>;

use Yii;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;
<?php if ($multiLanguageModel) { ?>
use common\components\model\Translateable;
<?php } ?>
<?php if ($generator->imageUploaders) : ?>
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\base\EntityToFile;
<?php endif; ?>
<?php if ($generator->relationsForRelatedFormWidget): ?>
use yii\db\ActiveQueryInterface;
<?php endif; ?>

/**
 * This is the model class for table "<?= $generator->generateTableName($tableName) ?>".
 *
<?php foreach ($tableSchema->columns as $column): ?>
 * @property <?= "{$column->phpType} \${$column->name}\n" ?>
<?php endforeach; ?>
<?php if (!empty($relations)): ?>
 *
<?php foreach ($relations as $name => $relation): ?>
 * @property <?= $relation[1] . ($relation[2] ? '[]' : '') . ' $' . lcfirst($name) . "\n" ?>
<?php endforeach; ?>
<?php endif; ?>
 */
class <?= $className ?> extends ActiveRecord implements BackendModel<?= $multiLanguageModel ? ', Translateable' : null ?><?= "\n" ?>
{
<?php if ($multiLanguageModel) : ?>
    use \backend\components\TranslateableTrait;
<?php endif; ?>

<?php if ($generator->imageUploaders) : ?>
<?php foreach ($generator->imageUploaders as $uploader): ?>
    public $<?= $uploader['attribute'] ?>;

<?php endforeach; ?>
    public $sign;

    public function init()
    {
        parent::init();
        $this->sign = $this->sign ?: security()->generateRandomString();
    }

<?php endif; ?>
    public static function tableName()
    {
        return '<?= $generator->generateTableName($tableName) ?>';
    }
<?php if ($generator->db !== 'db'): ?>

    public static function getDb()
    {
        return Yii::$app->get('<?= $generator->db ?>');
    }
<?php endif; ?>

    public function rules()
    {
        return [<?= "\n            " . implode(",\n            ", $rules) . ",\n        " ?>
<?php if ($generator->imageUploaders): ?>
    [['sign'], 'string', 'max' => 255],<?= "\n         " ?>
<?php endif; ?>
];
    }

    public function attributeLabels()
    {
        return [
<?php foreach ($labels as $name => $label): ?>
            <?= "'$name' => " . $generator->generateStringWithTable($tableSchema, $label) . ",\n" ?>
<?php endforeach; ?>
<?php foreach ($generator->imageUploaders as $uploader): ?>
            <?= "'{$uploader['attribute']}' => " . $generator->generateString($uploader['attributeLabel']) . ",\n" ?>
<?php endforeach; ?>
        ];
    }

<?php if ($multiLanguageModel) : ?>
    public static function getTranslationAttributes()
    {
        return [
      <?php foreach ($translationAttributes as $attribute): ?>
      <?= "'$attribute',\n" ?>
      <?php endforeach; ?>
  ];
    }

<?php endif; ?>
<?php if (is_array($behaviors) && !empty($behaviors)) : ?>
    public function behaviors()
    {
        return [<?= "\n            " . implode(",\n            ", $behaviors) . ",\n        " ?>];
    }
<?php endif; ?>
<?php foreach ($relations as $name => $relation): ?>

    public function get<?= $name ?>()
    {
        <?= $relation[0] . "\n" ?>
    }
<?php endforeach; ?>

    public function getTitle()
    {
        return bt('<?= Inflector::camel2words(StringHelper::basename($generator->modelClass)) ?>', 'app');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    <?= implode(",\n                    ", $indexColumns) . ",\n" ?>
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    <?= implode(",\n                    ", $viewColumns) . ",\n" ?>
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new <?= StringHelper::basename($generator->modelClass) ?>Search();
    }

    public function getFormConfig()
    {
        $config = [
<?php foreach ($formColumns as $attribute => $config) : ?>
            '<?= $attribute ?>' => <?= $config ?>,
<?php endforeach; ?>
<?php if ($generator->imageUploaders) : ?>
<?php foreach ($generator->imageUploaders as $uploader): ?>
            '<?= $uploader['attribute'] ?>' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => '<?= $uploader['attribute'] ?>',
                    'saveAttribute' => \common\models\<?= $className ?>::<?= \backend\components\gii\migration\Generator::getSaveAttributeConstantName($uploader['attribute']) ?>,
                    'aspectRatio' => <?= $uploader['aspectRatio'] ?: 'false' ?>, //Пропорция для кропа
                    'multiple' => <?= $uploader['multiple'] ? 'true' : 'false' ?>, //Вкл/выкл множественную загрузку
                ])
            ],
<?php endforeach; ?>
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
<?php endif; ?>
        ];
<?php if ($generator->relationsForRelatedFormWidget): ?>

        $config = [
            'form-set' => [
                'Main' => $config,
<?php foreach ($generator->relationsForRelatedFormWidget as $relation): ?>
                '<?= $relation['tabName'] ?>' => [
                    $this->getRelatedFormConfig()['<?= $relation['relationName'] ?>']
                ],
<?php endforeach; ?>
            ]
        ];
<?php endif; ?>

        return $config;
    }

<?php if ($generator->relationsForRelatedFormWidget): ?>
    public function getRelatedFormConfig()
    {
        $config = [
<?php foreach ($generator->relationsForRelatedFormWidget as $relation): ?>
            '<?= $relation['relationName'] ?>' => [
                'relation' => '<?= $relation['relationName'] ?>'
            ],
<?php endforeach; ?>
        ];

        return $config;
    }
<?php endif; ?>

<?php if ($generator->imageUploaders) { ?>
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
<?php } ?>
}
