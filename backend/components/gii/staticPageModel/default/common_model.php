<?php
/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this yii\web\View */
/* @var $generator backend\components\gii\staticPageModel\Generator */

echo "<?php\n";
$modelClassName = $generator->modelClassName;
?>

namespace common\models;

use common\components\ConfigurationComponent;
use common\models\base\StaticPage;
use Yii;

/**
 * This is class for static page model.
 */
class <?= $modelClassName ?> extends StaticPage
{
<?php foreach ($generator->keys as $key): ?>
    const <?= $generator->formatToConstant($key['id']) ?> = '<?= $generator->generateKeyName($key['id']) ?>';
<?php endforeach; ?>

<?php foreach ($generator->imageUploaders as $uploader): ?>
	const <?= backend\components\gii\migration\Generator::getSaveAttributeConstantName($uploader['attribute']) ?> = '<?= $modelClassName . ucfirst($uploader['attribute']) ?>';
<?php endforeach; ?>

<?php foreach ($generator->keys as $key): ?>
    public $<?= $generator->camelCase($key['id']) ?>;
<?php endforeach; ?>

    /**
    * @return array
    */
    public function getKeys()
    {
        return [
<?php foreach ($generator->keys as $key): ?>
            self::<?= $generator->formatToConstant($key['id']) ?>,
<?php endforeach; ?>
        ];
    }

    /**
    * @return $this
    */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
<?php foreach ($generator->keys as $key): ?>
        $this-><?= $generator->camelCase($key['id']) ?> = $config->get(self::<?= $generator->formatToConstant($key['id']) ?>);
<?php endforeach; ?>

        return $this;
    }
}
