<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules;

use backend\components\gii\exportModules\parts\ModulePartManager;
use Symfony\Component\Finder\Finder;
use Yii;
use yii\base\BaseObject;


/**
 * Class BaseModule
 *
 * @package backend\components\gii\exportModules
 */
abstract class BaseModule extends BaseObject
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var array
     */
    public $log = [];

    /**
     * @var ModulePartManager[]
     */
    public $parts = [];

    /**
     * BaseModule constructor.
     *
     * @param $moduleId
     */
    public function __construct($moduleId)
    {
        parent::__construct();

        $this->id = $moduleId;
        $this->setParts();
    }

    protected function setParts()
    {
        $finder = new Finder();
        $path = Yii::getAlias('@backend') . '/components/gii/exportModules/parts';
        $finder->files()->name('*.php')->in($path);
        foreach ($finder as $file) {
            $ns = 'backend\components\gii\exportModules\parts';
            if ($relativePath = $file->getRelativePath()) {
                $ns .= '\\'.strtr($relativePath, '/', '\\');
            }
            $class = $ns . '\\' . $file->getBasename('.php');

            $r = new \ReflectionClass($class);

            if ($r->isSubclassOf(ModulePartManager::class) && !$r->isAbstract()) {
                $this->parts[$class] = $r->newInstance($this);
            }
        }
    }
}
