<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules\parts;


use backend\components\gii\exportModules\parts\fileGetters\FilesGetter;
use backend\components\gii\exportModules\parts\fileGetters\ModuleDirectoryFilesGetter;
use Yii;

/**
 * Class FrontendPartManager
 *
 * @package backend\components\gii\exportModules\parts\fileGetters
 */
class FrontendPartManager extends ModulePartManager
{
    /**
     * @return string
     */
    public function getAppModulePartPath()
    {
        return Yii::getAlias('@frontend') . '/modules/' . $this->module->id;
    }

    /**
     * @return string
     */
    public function getCoreModulePartPath()
    {
        return 'frontend';
    }

    /**
     * @return FilesGetter
     */
    public function getFilesGetter(): FilesGetter
    {
        return new ModuleDirectoryFilesGetter($this);
    }
}
