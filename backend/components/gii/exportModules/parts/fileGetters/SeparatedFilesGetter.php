<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules\parts\fileGetters;

use yii\helpers\Inflector;


/**
 * Class SeparatedFilesGetter
 *
 * @package backend\components\gii\exportModules\parts\fileGetters
 */
class SeparatedFilesGetter extends FilesGetter
{
    /**
     * @return array
     */
    public function getFilesForExport()
    {
        $modulePartManager = $this->modulePartManager;
        $files = [];
        foreach ($this->getDirectoryFilePaths() as $fileName => $filePath) {
            $files[$filePath] = $modulePartManager->getCoreModulePartFullPath() . '/' . $fileName;
        }

        return $files;
    }

    /**
     * @return array
     */
    protected function getDirectoryFilePaths()
    {
        $modulePartManager = $this->modulePartManager;
        $files = glob($modulePartManager->getAppModulePartPath() . '/' . Inflector::id2camel($modulePartManager->module->id) . '*');
        $paths = [];
        foreach ($files as $file) {
            $fileName = $this->getPhpFileName($file);
            if ($fileName) {
                $paths[$fileName] = $file;
            }
        }

        return $paths;
    }

    /**
     * @param string $file
     *
     * @return string|null
     */
    protected function getPhpFileName($file)
    {
        $array = explode('/', $file);
        $count = count($array);
        if (substr_count($array[$count - 1], '.php')) {
            return $array[$count - 1];
        }

        return null;
    }
}
