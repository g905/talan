<?php
/**
 * Author: metal
 * Email: metal
 */

namespace backend\components;

use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\helpers\LanguageHelper;
use common\models\City;
use common\models\EntityToFile;
use metalguardian\fileProcessor\helpers\FPM;
use vova07\imperavi\actions\GetAction;
use vova07\imperavi\actions\UploadAction;
use Yii;
use yii\base\NotSupportedException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class BackendController
 * @property $modelClass
 * @package backend\components
 */
abstract class BackendController extends Controller
{
    use RelatedFormTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [\common\models\User::ROLE_ADMIN],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @throws NotSupportedException
     * @return string
     */
    abstract public function getModelClass();

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'images-get' => [
                'class' => GetAction::className(),
                'url' => '/uploads/redactor/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/redactor',
                'type' => GetAction::TYPE_IMAGES,
                'options' => [
                    'basePath' => Yii::getAlias('@webroot/uploads/redactor'),
                    'except' => ['.gitkeep']
                ]

            ],
            'image-upload' => [
                'class' => UploadAction::className(),
                'url' => '/uploads/redactor/', // Directory URL address, where files are stored.
                'path' => 'uploads/redactor' // Absolute path to directory where files are stored.
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $class = $this->getModelClass();
        /** @var BackendModel $searchModel */
        $searchModel = (new $class)->getSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('//templates/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('//templates/view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $class = $this->getModelClass();
        /** @var ActiveRecord $model */
        $model = new $class();
        $model->loadDefaultValues();
        $config = $this->getRelatedFormActionConfig($model);
        if (!empty($config)) {
            return $this->relatedFormAction($model, $config);
        }

        if ($this->loadModels($model) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Record successfully created!'));

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('//templates/create', [
            'model' => $model,
        ]);
    }

    /**
     * @param \yii\db\ActiveRecord $model
     *
     * @return bool
     */
    public function loadModels($model)
    {
        $loaded = true;
        $this->loadLangModels($model, $loaded);

        $loaded = $model->load(Yii::$app->request->post()) && $loaded;

        return $loaded;
    }

    /**
     * @param $model \yii\db\ActiveRecord
     * @param $loaded bool
     */
    private function loadLangModels($model, &$loaded, $index = null)
    {
        if ($model instanceof Translateable) {
            $languages = LanguageHelper::getLanguageModels();

            $models = [];
            foreach ($languages as $language) {
                if ($language->locale === LanguageHelper::getDefaultLanguage()->locale) {
                    continue;
                }
                $models[$language->locale]= $model->getTranslation($language->locale);
            }

            if (!empty($models)) {
                $loaded &= Model::loadMultiple($models, Yii::$app->request->post(), null, $index);
            }
        }
    }

    /**
     * @param \yii\db\ActiveRecord[] $models
     *
     * @return bool
     */
    public function loadMultipleModels($models)
    {
        $loaded = true;
        foreach ($models as $index => $model) {
            $this->loadLangModels($model, $loaded, $index);

            $loaded = $model->load(Yii::$app->request->post($model->formName())[$index], '') && $loaded;
        }

        return $loaded;
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /** @var ActiveRecord $model */
        $model = $this->findModel($id);

        $config = $this->getRelatedFormActionConfig($model);
        if (!empty($config)) {
            return $this->relatedFormAction($model, $config);
        }

        if ($this->loadModels($model) && $model->save()) {
            \Yii::$app->getSession()->setFlash('info', Yii::t('app', 'Record successfully updated!'));

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('//templates/update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        \Yii::$app->getSession()->setFlash('warning', Yii::t('app', 'Record successfully deleted!'));

        return $this->redirect(['index']);
    }

    /**
     * Clone records. Clone record with only main model property
     */

    public function actionClone($id)
    {
        $currentModel = $this->findModel($id);
        $currentModel->id = null;
        $currentModel->isNewRecord = true;

        if($currentModel->save(false)) {
            \Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Record successfully duplicated!'));
        }else{
            \Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Sorry something wrong! Please try to duplicate record later!'));
        }

        return $this->redirect(['index']);
    }


    public function actionRelatedClone($id)
    {
        $model = $this->getModelClass();
        $oldModel = $this->findModel($id);
        $relations = $oldModel->getRelatedFormConfig();
        $newModel = new $model;
        $newModel->attributes = $oldModel->attributes;
        $newModel->published = false;
        if($newModel->save(false)) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Record successfully duplicated!'));
        }else{
            Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Sorry something wrong! Please try to duplicate record later!'));
        }

        self::cloneImages($oldModel->id, $newModel);

        self::saveRelations($relations, $oldModel, $newModel);

        return $this->redirect(['index']);
    }

    public static function saveRelations($relations, $oldModel, $newModel)
    {
        foreach ($relations as $config) {
            $relation = $config['relation'];
            $className = $oldModel->getRelation($relation)->modelClass;
            $key = key($oldModel->getRelation($relation)->link);
            $relatedModels = $oldModel->$relation;
            $clone = $oldModel->relModels[$relation] = $relatedModels ? $relatedModels : [new $className];

            foreach ($clone as $item) {
                if ($item->$key) {
                    $oldItemId = $item->id;
                    $item->id = null;
                    $item->isNewRecord = true;
                    $item->$key = $newModel->id;
                    $item->save(false);

                    self::cloneImages($oldItemId, $item);
                }
            }
        }
    }

    public static function cloneImages($oldModelId, $newModel)
    {
        $modelName = StringHelper::basename(get_class($newModel));

        $images = EntityToFile::find()->where([
            'entity_model_id' => $oldModelId,
            'entity_model_name' => $modelName,
        ])->all();

        if ($images) {
            foreach ($images as $imgage) {
                $oldPath = FPM::getOriginalDirectory($imgage->file_id);
                $file = $imgage->file;
                $oldName = $file->id . '-' . $file->base_name . '.' . $file->extension;
                $oldSrc = $oldPath . '/' . $oldName;
                $file->id = null;
                $file->isNewRecord = true;
                $file->save(false);
                $imgage->id = null;
                $imgage->file_id = $file->id;
                $imgage->entity_model_id = $newModel->id;
                $imgage->isNewRecord = true;
                $imgage->save(false);
                $path = FPM::getOriginalDirectory($imgage->file_id);
                $src = $path . '/' . $imgage->file_id . '-' . $file->base_name . '.' . $file->extension;
                if (file_exists($oldSrc)) {
                    copy($oldSrc, $src);
                }
            }
        }
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \yii\db\ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /** @var \yii\db\ActiveRecord $class */
        $class = $this->getModelClass();
        if (($model = $class::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPreview($city = false, $alias = false, $rule = false)
    {
        $baseFrontUrl = Yii::$app->params['baseFrontUrl'];

        $cityAlias = '';
        $aliasUrl = '';
        $ruleUrl = '';


        if ($city) {
            $cityAlias = idn_to_ascii(City::getCityById($city)->alias).'.';
        }

        if ($alias) {
            $aliasUrl = '/'.$alias;
        }

        if ($rule) {
            $ruleUrl = '/'.$rule;
        }

        return $this->redirect('//'.$cityAlias.$baseFrontUrl.$ruleUrl.$aliasUrl);
    }


}
