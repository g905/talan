<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 19.04.2017
 * Time: 8:47
 */

namespace backend\helpers;


use kartik\widgets\Select2;
use yii\web\JsExpression;

/**
 * Class Select2Helper
 * @package backend\modules\product\helpers
 */
class Select2Helper
{
    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     * @param string $dataUrl
     * @param array $selectedData
     * @param array $options
     * @return string
     */
    public static function getAjaxSelect2Widget(
        \yii\base\Model $model,
        string $attribute,
        string $dataUrl,
        $selectedData = [],
        $options = [],
        $allowClear = true
    ) {
        return Select2::widget(
            [
                'model' => $model,
                'attribute' => $attribute,
                'data' => $selectedData,
                'showToggleAll' => false,
                'pluginOptions' => [
                    'allowClear' => $allowClear,
                    'minimumInputLength' => 0,
                    'ajax' => [
                        'url' => $dataUrl,
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new JsExpression('function(params) { return {search:params.term}; }'),
                        'cache' => false
                    ],
                ],
                'options' => $options
            ]
        );
    }
}