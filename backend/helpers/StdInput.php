<?php

namespace backend\helpers;

use conquer\codemirror\CodemirrorAsset;
use conquer\codemirror\CodemirrorWidget;
use kartik\date\DatePicker;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\color\ColorInput;
use dosamigos\selectize\SelectizeTextInput;
use common\models\City;
use common\models\Manager;
use common\models\SiteMenu;
use common\models\Specialization;
use common\models\PublicDataMenu;
use common\helpers\SiteUrlHelper;
use backend\components\FormBuilder;
use backend\components\ImperaviContent;
use backend\modules\page\models\PublicDataWidget;
use backend\modules\apartment\models\ApartmentComplex;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;

/**
 * StdInput contains configurations for different field types.
 *
 * @package backend\helpers
 */
class StdInput
{
    public static function dropdown($items)
    {
        return [
            'type' => FormBuilder::INPUT_DROPDOWN_LIST,
            'items' => $items,
        ];
    }

    public static function label(ActiveRecord $model)
    {
        return [
            'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
            'options' => ['maxlength' => true, 'class' => $model->getIsNewRecord() ? 's_name form-control' : 'form-control'],
        ];
    }

    public static function alias(ActiveRecord $model)
    {
        return [
            'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
            'options' => ['maxlength' => true, 'class' => $model->getIsNewRecord() ? 's_alias form-control' : 'form-control'],
        ];
    }

    public static function text()
    {
        return [
            'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
            'options' => ['maxlength' => true],
        ];
    }

    public static function textArea()
    {
        return [
            'type' => FormBuilder::INPUT_TEXTAREA,
            'options' => ['rows' => 6],
        ];
    }

    public static function splittedTextArea()
    {
        return [
            'type' => FormBuilder::INPUT_TEXTAREA,
            'hint' => 'Для ввода нескольких значений, разделите их переводом каретки'
        ];
    }

    public static function imageUpload($attribute, $multiple = false, $ratio = false)
    {
        return [
            'type' => FormBuilder::INPUT_WIDGET,
            'widgetClass' => ImageUpload::class,
            'options' => [
                'saveAttribute' => $attribute,
                'aspectRatio' => $ratio,
                'multiple' => $multiple,
                'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
            ],
        ];
    }

    public static function videoUpload($attribute, $multiple = false)
    {
        return [
            'type' => FormBuilder::INPUT_WIDGET,
            'widgetClass' => ImageUpload::class,
            'options' => [
                'saveAttribute' => $attribute,
                'aspectRatio' => false,
                'multiple' => $multiple,
                'allowedFileExtensions' => ['mp4'],
            ],
        ];
    }

    public static function fileUpload($attribute, $multiple = false, $extensions = [], $meta = false)
    {
        return [
            'type' => FormBuilder::INPUT_WIDGET,
            'widgetClass' => ImageUpload::class,
            'options' => [
                'saveAttribute' => $attribute,
                'aspectRatio' => false,
                'multiple' => $multiple,
                'allowedFileExtensions' => $extensions,
                'showMetaDataBtn' => $meta,
            ],
        ];
    }

    public static function editor()
    {
        return [
            'type' => FormBuilder::INPUT_WIDGET,
            'widgetClass' => ImperaviContent::class,
        ];
    }

    public static function editorTableOnly()
    {
        return [
            'type' => FormBuilder::INPUT_WIDGET,
            'widgetClass' => ImperaviContent::class,
            'options' => [
                'settings' => [
                    'lang' => 'ru',
                    'buttons' => [
                        'html',
                        'bold',
                        'italic',
                        'underline',
                    ],
                    'pastePlainText' => true,
                    'buttonSource' => true,
                    'replaceDivs' => false,
                    'paragraphize' => true,
                    'plugins' => [
                        'table',
                        'fullscreen',
                    ],
                    'minHeight' => 150
                ],
            ],
        ];
    }

    public static function checkbox()
    {
        return [
            'type' => FormBuilder::INPUT_CHECKBOX,
        ];
    }

    public static function citySelect()
    {
        return [
            'type' => FormBuilder::INPUT_DROPDOWN_LIST,
            'items' => City::getItems(),
            'options' => ['prompt' => ''],
        ];
    }

    public static function complexSelect()
    {
        return [
            'type' => FormBuilder::INPUT_DROPDOWN_LIST,
            'items' => ApartmentComplex::getItems(),
            'options' => ['prompt' => ''],
        ];
    }

    public static function complexDiagramTypeSelect($type)
    {
        return [
            'type' => FormBuilder::INPUT_DROPDOWN_LIST,
            'items' => ApartmentComplex::getItems(),
            'options' => ['prompt' => ''],
        ];
    }

    public static function publicDataWidgetSelect(string $class, int $type, $label = 'label', $id = 'id', $max = null, $city = false, $complex = false)
    {
        return [
            'type' => FormBuilder::INPUT_WIDGET,
            'widgetClass' => SelectizeTextInput::class,
            'options' => [
                // 'loadUrl' => SiteUrlHelper::listPublicDataWidgetSelectizeUrl($class, $type, $id, $label, $city, $complex),
                'options' => [
                    'placeholder' => bt('Select ...'),
                ],
                'clientOptions' => [
                    'options' => self::getPublicDataWidgetData($class, $type, $id, $label, $city, $complex),
                    'valueField' => $id,
                    'labelField' => $label,
                    'searchField' => $label,
                    'create' => false,
                    'preload' => true,
                    'maxItems' => $max,
                    'inputClass' => 'selectize-input',
                    'plugins' => ['remove_button', 'drag_drop'],
                    'render' => [
                        'option' => new JsExpression(<<<JS
                            function(data, escape) {
                                return '<div class="option">' +
                                    '<span class="title">' + escape(data.$label) + '</span>' +
                                '</div>';
                            }
JS
                        ),
                        'item' => new JsExpression(<<<JS
                            function(data, escape) {
                                return '<div class="select-item">' + escape(data.$label) + '</div>';
                            }
JS
                        ),
                    ],
                ],
            ],
        ];
    }
 
    public static function publicDataWidgetSelectList(int $type, $label = 'label', $id = 'id', $city = false, $complex = false)
    {
        /** @var PublicDataWidget $class */
        $query = PublicDataWidget::find()->alias('widget')->andWhere(['widget.type' => $type]);

        if ($city) {
            $query->joinWith(['city city']);
            $query->select(["widget.$id", "concat(city.label, ': ', widget.$label) as $label", 'city_id']);
        }

        if ($complex) {
            $query->joinWith(['complex complex']);
            $query->select(["widget.$id", "concat(complex.label, ': ', widget.$label) as $label", 'complex_id']);
        }

        if (!$city && !$complex) {
            $query->select(["widget.$id", $label]);
        }

        $data = $query->asArray()->all();
        $data = map($data, $id, $label);

        return [
            'type' => FormBuilder::INPUT_DROPDOWN_LIST,
            'items' => $data,
            'options' => ['prompt' => '', 'multiple' => false],
        ];
    }

    public static function publicDataWidgetTypes()
    {
        return [
            'type' => FormBuilder::INPUT_DROPDOWN_LIST,
            'items' => PublicDataWidget::types(),
            'options' => ['prompt' => ''],
        ];
    }

    public static function publicDataMenuSelect()
    {
        return [
            'type' => FormBuilder::INPUT_DROPDOWN_LIST,
            'items' => PublicDataMenu::getItems(),
            'options' => ['prompt' => '-- Без меню --'],
        ];
    }

    public static function specializationSelect()
    {
        return [
            'type' => FormBuilder::INPUT_DROPDOWN_LIST,
            'items' => Specialization::getItems('id', 'top_label'),
            'options' => ['prompt' => ''],
        ];
    }

    public static function managerSelect()
    {
        return [
            'type' => FormBuilder::INPUT_DROPDOWN_LIST,
            'items' => Manager::getItems('id', 'fio'),
            'options' => ['prompt' => ''],
        ];
    }

    public static function hidden()
    {
        return [
            'type' => FormBuilder::INPUT_HIDDEN,
            'label' => false,
        ];
    }

    public static function viewCount()
    {
        return [
            'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
            'options' => ['maxlength' => true, 'readonly' => true],
        ];
    }

    public static function relationInput($type, $max = null, $idField = 'id', $textField = 'label')
    {
        return [
            'type' => FormBuilder::INPUT_WIDGET,
            'widgetClass' => SelectizeTextInput::class,
            'options' => [
                'loadUrl' => SiteUrlHelper::listModelSelectizeUrl($type, $idField, $textField),
                'options' => [
                    'placeholder' => bt('Select ...'),
                ],
                'clientOptions' => [
                    'options' => self::getRelationInputData($type, $idField, $textField),
                    'valueField' => $idField,
                    'labelField' => $textField,
                    'searchField' => $textField,
                    'create' => false,
                    'preload' => true,
                    'maxItems' => $max,
                    'inputClass' => 'selectize-input',
                    'plugins' => ['remove_button', 'drag_drop'],
                    'render' => [
                        'option' => new JsExpression(<<<JS
                            function(data, escape) {
                                return '<div class="option">' +
                                    '<span class="title">' + escape(data.$textField) + '</span>' +
                                '</div>';
                            }
JS
                        ),
                        'item' => new JsExpression(<<<JS
                            function(data, escape) {
                                return '<div class="select-item">' + escape(data.$textField) + '</div>';
                            }
JS
                        ),
                    ],
                ],
            ],
        ];
    }

    public static function relationInputSelect2($type, $multiple = true, $idField = 'id', $textField = 'label')
    {
        return [
            'type' => FormBuilder::INPUT_WIDGET,
            'widgetClass' => Select2::class,
            'options' => [
                'options' => ['multiple' => $multiple],
                'showToggleAll' => false,
                'pluginOptions' => [
                    'data' => self::getRelationInputData($type, $idField, $textField),
                    'allowClear' => true,
                    'minimumInputLength' => 0,
                    'maintainOrder' => true,
                    // 'ajax' => [
                    //     'url' => SiteUrlHelper::listModelUrl($type),
                    //     'dataType' => 'json',
                    //     'delay' => 250,
                    //     'data' => new JsExpression('function (params) {return {search: params.term};}'),
                    //     'cache' => false
                    // ],
                ],
            ],
        ];
    }

    private static function getRelationInputData($type, $idField, $textField)
    {
        /** @var ActiveRecord $type */
        $query = $type::find()->select([$idField, $textField])->asArray();

        return $query->all();
    }

    private static function getPublicDataWidgetData($class, int $type, $idField = 'id', $textField = 'label', $city, $complex)
    {
        /** @var PublicDataWidget $class */
        $query = $class::find()->alias('widget')->andWhere(['widget.type' => $type]);

        if ($city) {
            $query->joinWith(['city city' => function (ActiveQuery $q) {
                $q->select(['city.id', 'city.label']);
            }]);
            $query->select(["widget.$idField", "concat(city.label, ': ', widget.$textField) as $textField", 'city_id']);
        }

        if ($complex) {
            $query->joinWith(['complex complex' => function (ActiveQuery $q) {
                $q->select(['complex.id', 'complex.label']);
            }]);
            $query->select(["widget.$idField", "concat(complex.label, ': ', widget.$textField) as $textField", 'complex_id']);
        }

        if (!$city && !$complex) {
            $query->select(["widget.$idField", $textField]);
        }

        return $query->asArray()->all();
    }

    public static function colorSelect($format = 'rgb')
    {
        return [
            'type' => FormBuilder::INPUT_WIDGET,
            'widgetClass' => ColorInput::class,
            'options' => ['pluginOptions'=> ['preferredFormat' => $format]],
        ];
    }

    public static function map($uniqueId)
    {
        return [
            'label' => false,
            'type' => FormBuilder::INPUT_RAW,
            'value' => '<div id="product_map_' . $uniqueId . '" class="product_map"></div>'
        ];
    }

    public static function mapLat(ActiveRecord $model, $uniqueId, $attr = 'lat')
    {
        return [
            'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
            'options' => [
                'maxlength' => true,
                'readonly' => true,
                'id' => 'product_map_lat_' . $uniqueId,
                'class' => 'form-control',
                'value' => $model->getIsNewRecord() ? '56.8497600' : $model->$attr
            ],
        ];
    }

    public static function mapLng(ActiveRecord $model, $uniqueId, $attr = 'lng')
    {
        return [
            'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
            'options' => [
                'maxlength' => true,
                'readonly' => true,
                'id' => 'product_map_lng_' . $uniqueId,
                'class' => 'form-control',
                'value' => $model->getIsNewRecord() ? '53.2044800' : $model->$attr
            ],
        ];
    }

    public static function preparedPage()
    {
        return [
            'type' => FormBuilder::INPUT_DROPDOWN_LIST,
            'items' => SiteMenu::getPreparedPageDictionary(),
            'options' => ['prompt' => ''],
        ];
    }

    /**
     * @param $model
     * @param string $attr
     * @param string $preset javascript|php
     * @return array
     * @throws \Exception
     */
    public static function codeMirror($model, $attr, $preset = 'javascript')
    {
        return [
            'type' => FormBuilder::INPUT_RAW,
            'value' => CodemirrorWidget::widget([
                'model' => $model,
                'attribute' => $attr,
                'preset'=> $preset,
                'assets' => [
                    CodemirrorAsset::THEME_SOLARIZED
                ],
                'settings' => [
                    'theme' => 'solarized',
                ],
                'options' => ['rows' => 20],
            ]),
        ];
    }

    public static function separator($title = 'Блоки', $icon = 'grid')
    {
        return [
            'label' => false,
            'type' => FormBuilder::INPUT_RAW,
            'value' => "<div class='form-config-separator'><i class='si si-$icon'></i> $title</div>",
            'fieldOptions' => [
                'errorOptions' => ['tag' => null],
                'options' => ['tag' => null]
            ],
        ];
    }

    public static function dropdownEnumMonth()
    {
        return [
            'type' => FormBuilder::INPUT_DROPDOWN_LIST,
            'items' => [
                'ЯНВ' => 'ЯНВ',
                'ФЕВ' => 'ФЕВ',
                'МАР' => 'МАР',
                'АПР' => 'АПР',
                'МАЙ' => 'МАЙ',
                'ИЮНЬ' => 'ИЮНЬ',
                'ИЮЛЬ' => 'ИЮЛЬ',
                'АВГ' => 'АВГ',
                'СЕН' => 'СЕН',
                'ОКТ' => 'ОКТ',
                'НОЯ' => 'НОЯ',
                'ДЕК' => 'ДЕК',
            ],
        ];
    }

    public static function monthPicker()
    {
        return [
            'type' => FormBuilder::INPUT_WIDGET,
            'widgetClass' => DatePicker::class,
            'options' => [
                'options' => [
                    'placeholder' => 'Выберете месяц'
                ],
                'pluginOptions' => [
                    'format' => 'mm-yyyy',
                    'todayHighlight' => true,
                    'maxViewMode' => 'years', // days, month, months, year, years, decade, decades, century, centuries
                    'minViewMode' => 'months',
                    'language' => 'ru'
                ]
            ],
        ];
    }
}
