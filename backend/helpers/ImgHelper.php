<?php
/**
 * Author: Pavel Naumenko
 */

namespace backend\helpers;

use common\components\model\ActiveRecord;
use common\models\Product;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use yii\helpers\StringHelper;

/**
 * Class ImgHelper
 * @package backend\components
 */
class ImgHelper
{
    /**
     * @param $model
     * @param string $relation
     * @return null|string
     */
    public static function getEntityImage($model, $relation = 'mainImage')
    {
        return (isset($model->{$relation}) && $model->{$relation}->file_id)
            ? FPM::image($model->{$relation}->file_id, 'admin', 'fileAdaptive',['class' => 'img-index'] )
            : ' <div class="no-photo-div">
                        <div class="no-photo-line"></div>
                        <span class="glyphicon glyphicon-camera no-photo-icon"></span>
                    </div>';
    }


}
