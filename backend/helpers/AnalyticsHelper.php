<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 23.08.2017
 * Time: 13:40
 */

namespace backend\helpers;

use backend\modules\apartment\models\Apartment;
use backend\modules\apartment\models\ApartmentComplex;
use backend\modules\contacts\models\City;


class AnalyticsHelper
{
    /**
     * @return mixed
     */
    public static function getHeaderAnalytics()
    {
        $cityCount = City::find()->count();
        $apartmentComplexCount = ApartmentComplex::find()->count();
        $apartmentCount = Apartment::find()->count();

        if (strlen($cityCount) == 1){
            $cityCount = '0'.$cityCount;
        }
        if (strlen($apartmentComplexCount) == 1){
            $apartmentComplexCount = '0'.$apartmentComplexCount;
        }
        if (strlen($apartmentCount) == 1){
            $apartmentCount = '0'.$apartmentCount;
        }


        return [
            'cityCount' =>  $cityCount,
            'apartmentComplexCount' => $apartmentComplexCount,
            'apartmentCount' => $apartmentCount,
        ];
    }

}