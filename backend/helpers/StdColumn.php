<?php

namespace backend\helpers;

use yii\grid\SerialColumn;
use kartik\grid\GridView;
use common\models\City;
use common\models\Manager;
use common\helpers\Property;
use common\models\PublicDataMenu;
use common\models\Specialization;
use common\models\ApartmentComplex;
use backend\components\StylingActionColumn;

/**
 * StdColumn contains configurations for different column types.
 *
 * @package backend\helpers
 */
class StdColumn
{
    public static function serialColumn()
    {
        return ['class' => SerialColumn::class];
    }

    public static function actionColumn()
    {
        return ['class' => StylingActionColumn::class];
    }

    public static function published()
    {
        return 'published:boolean';
    }

    public static function city()
    {
        return [
            'format' => 'raw',
            'attribute' => 'city_id',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => map(City::find()->select(['id', 'label'])->orderBy('label')->asArray()->all(), 'id', 'label'),
            'filterInputOptions' => ['placeholder' => bt('Any city', 'site-menu')],
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true]],
            'value' => function ($model) {
                return '<strong>' . optional($model->city)->label . '</strong>';
            },
        ];
    }

    public static function arrowStatus()
    {
        return [
            'format' => 'raw',
            'attribute' => 'status',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => [0 => 'Вниз', 1 => 'Вверх'],
            'filterInputOptions' => ['placeholder' => 'Не указано'],
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true]],
            'value' => function ($model) {
                return '<strong>' . ($model->status == 1 ? '&uarr;' : '&darr;') . '</strong>';
            }
        ];
    }

    public static function complex()
    {
        return [
            'format' => 'raw',
            'attribute' => 'complex_id',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => map(ApartmentComplex::find()->select(['id', 'label'])->orderBy('label')->asArray()->all(), 'id', 'label'),
            'filterInputOptions' => ['placeholder' => bt('Any complex', 'site-menu')],
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true]],
            'value' => function ($model) {
                return '<strong>' . optional($model->complex)->label . '</strong>';
            },
        ];
    }

    public static function publicDataMenu()
    {
        return [
            'format' => 'raw',
            'attribute' => 'menu_id',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => map(PublicDataMenu::find()->select(['id', 'label'])->orderBy('label')->asArray()->all(), 'id', 'label'),
            'filterInputOptions' => ['placeholder' => bt('Any menu', 'site-menu')],
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true]],
            'value' => function ($model) {
                return '<strong>' . optional($model->menu)->label . '</strong>';
            },
        ];
    }

    public static function manager()
    {
        return [
            'format' => 'raw',
            'attribute' => 'manager_id',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => map(Manager::find()->select(['id', 'fio'])->orderBy('fio')->asArray()->all(), 'id', 'fio'),
            'filterInputOptions' => ['placeholder' => bt('Any manager', 'site-menu')],
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true]],
            'value' => function ($model) {
                return '<strong>' . optional($model->manager)->fio . '</strong>';
            },
        ];
    }

    public static function viewCount()
    {
        return [
            'attribute' => 'view_count',
            'format' => 'raw',
        ];
    }

    public static function color($model)
    {
        $items = map($model::find()->select(['color'])->distinct()->orderBy('color')->asArray()->all(), 'color', 'color');

        return [
            'attribute' => 'color',
            'format' => 'raw',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $items,
            'filterInputOptions' => ['placeholder' => bt('Any color', 'site-menu')],
            'filterWidgetOptions' => [
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ],
            'value' => function ($model) {
                return '';
            },
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => ['background-color' => $model->color]];
            }
        ];
    }

    public static function propertyType($attr = 'type')
    {
        return [
            'attribute' => $attr,
            'format' => 'raw',
            'filter' => Property::types(),
            'value' => function ($model) use ($attr) {
                return Property::typeLabel($model->{$attr});
            }
        ];
    }

    public static function position()
    {
        return ['attribute' => 'position', 'width' => '30px'];
    }

    public static function image($attr, $imageRelation)
    {
        return [
            'attribute' => $attr,
            'format' => 'raw',
            'value' => function ($data) use ($imageRelation) {
                return ImgHelper::getEntityImage($data, $imageRelation);
            }
        ];
    }

    public static function boolean($attr)
    {
        return "$attr:boolean";
    }

    public static function percentage($attr)
    {
        return [
            'attribute' => $attr,
            'value' => function ($model) use ($attr) {
                return $model->$attr == '' ? '-' : $model->$attr . '%';
            }
        ];
    }

    public static function specialization()
    {
        return [
            'format' => 'raw',
            'attribute' => 'specialization_id',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => map(Specialization::find()->select(['id', 'top_label'])->orderBy('position')->asArray()->all(), 'id', 'top_label'),
            'filterInputOptions' => ['placeholder' => bt('Any specialization', 'site-menu')],
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true]],
            'value' => function ($model) {
                return '<strong>' . optional($model->specialization)->top_label . '</strong>';
            },
        ];
    }

    public static function nl2br($attr)
    {
        return [
            'format' => 'raw',
            'attribute' => $attr,
            'value' => function ($model) use ($attr) {
                return nl2br($model->$attr);
            },
        ];
    }
}
