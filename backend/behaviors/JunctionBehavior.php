<?php

namespace backend\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;

/**
 * Class JunctionBehavior
 *
 * @property ActiveRecord $owner
 * @package backend\behaviors
 */
class JunctionBehavior extends Behavior
{
    /**
     * @var string field name.
     */
    public $field;
    /**
     * @var string relation name. e.g. articles
     */
    public $relation;
    /**
     * @var array additional columns values.
     */
    public $extraColumns = [];
    /**
     * @var bool whether to enable row order.
     */
    public $maintainOrder = true;
    /**
     * @var string the name of attribute which stores order.
     */
    public $orderAttribute = 'position';

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    public function afterFind()
    {
        $attached = $this->owner->getRelation($this->relation)
            ->select(['id'])
            ->asArray()
            ->column();
        $this->owner->{$this->field} = implode(',', $attached);
    }

    public function afterSave()
    {
        $values = $this->owner->{$this->field};
        $relInfo = $this->owner->getRelation($this->relation);
        $models = $this->createModels($relInfo->modelClass, $values);
        $this->linkAll($this->relation, $models, $this->extraColumns);
    }

    /**
     * Create a set of models based on comma separated identifiers.
     *
     * @param string $class related model class name.
     * @param string $identifiers comma separated model identifiers.
     * @return ActiveRecord[] instantiated models.
     */
    private function createModels($class, $identifiers)
    {
        $models = [];
        /** @var ActiveRecord $class */
        $identifiers = explode(',', $identifiers);
        foreach ($identifiers as $i => $id) {
            $models[] = tap(createObject(compact('class', 'id')))->setIsNewRecord(false);
        }

        return $models; // $class::findAll(explode(',', $identifiers));
    }

    /**
     * Manages the relationships between models.
     *
     * @param string $name the case sensitive name of the relationship.
     * @param BaseActiveRecord[] $models the related models to be linked.
     * @param array $extraColumns additional column values to be saved into the junction table.
     * This parameter is only meaningful for a relationship involving a junction table
     * (i.e., a relation set with `ActiveRelationTrait::via()` or `ActiveQuery::viaTable()`.)
     * @param bool $unlink whether to unlink models that are not in the $models array.
     * @param bool $delete whether to delete the models that are not in the $models array.
     * If $unlink is false then this is ignored.
     * If false, the model's foreign key will be set null and saved.
     * If true, the model containing the foreign key will be deleted.
     */
    public function linkAll($name, $models, $extraColumns, $unlink = true, $delete = true)
    {
        $modelPk = key(call_user_func([$this->owner, 'get' . $name])->link);
        $newModelPks = getColumn($models, $modelPk);
        $oldModels = $this->owner->{$name};
        $oldModelPks = getColumn($oldModels, $modelPk);
        if ($unlink) {
            $this->unlink($name, $modelPk, $oldModels, $newModelPks, $delete);
        }
        $this->link($name, $modelPk, $models, $oldModelPks, $extraColumns);
    }

    /**
     * Remove old relational links.
     *
     * @param string $name relation name.
     * @param int|string $modelPk model primary key.
     * @param BaseActiveRecord[] $oldModels old models.
     * @param array $newModelPks new model primary keys.
     * @param bool $delete whether to delete.
     */
    protected function unlink($name, $modelPk, $oldModels, $newModelPks, $delete)
    {
        foreach ($oldModels as $order => $oldModel) {
            if ($this->maintainOrder && $oldModel->{$modelPk} != obtain($order, $newModelPks)) {
                $this->owner->unlink($name, $oldModel, $delete);
                continue;
            }
            if (!in_array($oldModel->{$modelPk}, $newModelPks)) {
                $this->owner->unlink($name, $oldModel, $delete);
            }
        }
    }

    /**
     * Add new relational links.
     *
     * @param string $name relation name.
     * @param int|string $modelPk model primary key.
     * @param BaseActiveRecord[] $models models to be linked.
     * @param array $oldModelPks old models primary keys.
     * @param array $extraColumns extra columns.
     */
    protected function link($name, $modelPk, $models, $oldModelPks, $extraColumns)
    {
        foreach ($models as $order => $newModel) {
            $orderCase = $this->maintainOrder && $newModel->{$modelPk} != obtain($order, $oldModelPks);
            if ($orderCase || !in_array($newModel->{$modelPk}, $oldModelPks)) {
                $this->owner->link($name, $newModel, $this->prepareExtraColumns($order, $extraColumns));
            }
        }
    }

    /**
     * Prepare extra columns array.
     *
     * @param int $key model index (position).
     * @param array $extraColumns an array of general extra columns, that is added to each new row.
     * @return array
     */
    private function prepareExtraColumns($key, $extraColumns)
    {
        $prepared = $this->maintainOrder ? ['position' => $key] : [];

        return merge($extraColumns, $prepared);
    }
}
