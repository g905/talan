<?php
namespace backend\tests\unit\components\gii\model;

use Yii;
use common\tests\base\unit\TestCase;
use backend\components\gii\migration\Field;
use backend\modules\configuration\models\Configurator;
use backend\components\gii\model\Generator as CommonModelGenerator;
use backend\components\gii\migration\Generator as MigrationGenerator;
use backend\components\gii\staticPageModel\Generator as StaticPageModelGenerator;
use backend\components\gii\crud\Generator as CrudGenerator;
use backend\components\gii\module\Generator as ModuleGenerator;

/**
 * Test case for Gii code generators
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class GeneratorsTest extends TestCase
{
    public function testMigrationGenerator()
    {
        Yii::setAlias('@console/migrations', '@tests/_output');
        $generator = new MigrationGenerator();
        $generator->template = 'default';

        $generator->moduleId = 'testModule';
        $generator->controllerClass = 'tests\controllers\TestController';

        $generator->ns = 'tests\models';
        $generator->modelClass = 'TestModel';

        $generator->tableName = 'test_migration_generator';
        $generator->migrationName = 'create_test_migration_generator_table';
        $generator->useTablePrefix = true;
        $generator->fields = [
            [
                'name' => 'id',
                'type' => Field::TYPE_PRIMARY_KEY,
            ],
            [
                'name' => 'test_field',
                'type' => Field::TYPE_STRING,
                'isNotNull' => true,
            ],
        ];

        // validate generators fields
        $this->assertModelValid($generator);

        // first step - generates migration file
        $files = $generator->generate();
        $migrationCode = $files[0]->content;

        $this->assertStringContains(
            $migrationCode,
            'public $tableName = \'{{%test_migration_generator}}\';',
            "Wrong table name in generated model:\n" . $migrationCode
        );
        $this->assertStringContains(
            $migrationCode,
            '\'id\' => $this->primaryKey(),',
            "Generated migration not contains all fields:\n" . $migrationCode
        );
        $this->assertStringContains(
            $migrationCode,
            '\'test_field\' => $this->string()->notNull(),',
            "Generated migration not contains all fields:\n" . $migrationCode
        );

        // second step
        $generator->isSecondStep = true;

        $files = $generator->generate();
        $controllerCode     = $files[0]->content;
        $commonModelCode    = $files[1]->content;
        $modelCode          = $files[2]->content;
        $searchModelCode    = $files[3]->content;

        $this->assertStringContains(
            $controllerCode,
            'class TestController extends BackendController',
            'Controller class should be generated'
        );
        $this->assertStringContains(
            $commonModelCode,
            [
                'namespace common\models;',
                'class TestMigrationGenerator extends ActiveRecord',
                '{{%test_migration_generator}}',
            ],
            'Common model class should be generated'
        );
        $this->assertStringContains(
            $modelCode,
            [
                'namespace tests\models;',
                'class TestMigrationGenerator extends ActiveRecord implements BackendModel',
                '{{%test_migration_generator}}'
            ],
            'Model class should be generated'
        );
        $this->assertStringContains(
            $searchModelCode,
            [
                'namespace tests\models;',
                'class TestModelSearch extends TestModel',
            ],
            'Search model class should be generated'
        );
    }

    public function testCommonModelGenerator()
    {
        $generator = new CommonModelGenerator();
        $generator->template = 'default';
        $generator->ns = 'common\models';
        $generator->baseClass = 'common\components\model\ActiveRecord';
        $generator->tableName = 'test_table';
        $generator->modelClass = 'TestTable';
        $generator->enableI18N = true;

        $this->assertModelValid($generator);

        $files = $generator->generate();
        $modelCode = $files[0]->content;

        $this->assertStringContains(
            $modelCode,
            'namespace common\models;',
            'Generated model should be under "common\models" namespace.'
            . "\nGenerated code:\n" . $modelCode
        );
        $this->assertStringContains(
            $modelCode,
            'class TestTable extends \common\components\model\ActiveRecord',
            'Generated model should be inherited from "common\components\model\ActiveRecord"'
            . "\nGenerated code:\n" . $modelCode
        );
        $this->assertStringContains(
            $modelCode,
            '\'test_field\' => Yii::t(\'app\', \'Test Field\'),',
            "Generated model not contains fields from table:\n" . $modelCode
        );
    }

    public function testStaticPageModelGenerator()
    {
        $generator = new StaticPageModelGenerator();
        $generator->template = 'default';
        $generator->ns = 'tests\models\static';
        $generator->modelClassName = 'TestStaticPage';
        $generator->moduleId = 'test';
        $generator->controllerClass = 'tests\controllers\TestStaticController';
        $generator->title = 'Static page test';
        $generator->keys = [
            [
                'id'                => 'test-key',
                'type'              => Configurator::TYPE_STRING,
                'description'       => 'This is test key',
                'hint'              => 'This is test key',
                'rule'              => null,
                'isRequired'        => true,
                'isTranslatable'    => false
            ]
        ];

        $this->assertModelValid($generator);

        $files = $generator->generate();
        $controllerCode     = $files[0]->content;
        $backendModelCode   = $files[1]->content;
        $commonModel        = $files[2]->content;

        $this->assertStringContains(
            $controllerCode,
            [
                'namespace tests\controllers;',
                'class TestStaticController extends ConfigurationController',
            ],
            'Controller class should be generated'
        );
        $this->assertStringContains(
            $backendModelCode,
            [
                'namespace tests\models\static;',
                'class TestStaticPage extends ConfigurationModel'
            ],
            'Backend model class should be generated'
        );
        $this->assertStringContains(
            $commonModel,
            [
                'namespace common\models;',
                'class TestStaticPage extends StaticPage',
                'const TEST_KEY = \'testStaticPageTestKey\';',
                'public $testKey;'
            ],
            'Common model class should be generated'
        );
    }

    public function testCrudGenerator()
    {
        $generator = new CrudGenerator();
        $generator->template = 'default';
        $generator->tableName = 'test_table';
        $generator->ns = 'tests\models';
        $generator->modelClass = 'TestTable';
        $generator->controllerClass = 'tests\controllers\TestController';

        $this->assertModelValid($generator);

        $files = $generator->generate();
        $controllerCode     = $files[0]->content;
        $modelCode          = $files[1]->content;
        $searchModelCode    = $files[2]->content;

        $this->assertStringContains(
            $controllerCode,
            [
                'namespace tests\controllers;',
                'class TestController extends BackendController',
                'return TestTable::className();'
            ],
            'Controller class should be generated'
        );
        $this->assertStringContains(
            $modelCode,
            [
                'namespace tests\models;',
                'class TestTable extends ActiveRecord implements BackendModel',
                'return \'{{%test_table}}\';'
            ],
            'Active record model should be generated'
        );
        $this->assertStringContains(
            $searchModelCode,
            [
                'namespace tests\models;',
                'class TestTableSearch extends TestTable'
            ],
            'Search model class should be generated'
        );
    }

    public function testModuleGenerator()
    {
        $generator = new ModuleGenerator();
        $generator->template = 'default';
        $generator->moduleClass = 'tests\module\Test';
        $generator->moduleID = 'test';

        $this->assertModelValid($generator);

        $files = $generator->generate();
        $moduleCode = $files[0]->content;

        $this->assertStringContains(
            $moduleCode,
            [
                'namespace tests\module;',
                'class Test extends \yii\base\Module',
                'public $controllerNamespace = \'tests\module\controllers\';'
            ],
            'Module class should be generated'
        );
        $this->assertEquals(
            $files[1]->path,
            Yii::getAlias('@tests/module/controllers/.gitkeep'),
            'Catalog for controllers should be created'
        );
        $this->assertEquals(
            $files[2]->path,
            Yii::getAlias('@tests/module/views/.gitkeep'),
            'Catalog for views should be created'
        );
        $this->assertEquals(
            $files[3]->path,
            Yii::getAlias('@tests/module/models/.gitkeep'),
            'Catalog for models should be created'
        );
    }
}
