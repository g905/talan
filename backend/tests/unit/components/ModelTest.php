<?php
namespace backend\tests\unit\components;

use common\tests\base\unit\TestCase;
use backend\components\Model;
use backend\tests\fake\models\TestModel;
use backend\tests\fake\models\TestRelationTable;
use backend\tests\fake\models\TestTable;

/**
 * Test case for [[backend\components\Model]]
 * @see \backend\components\Model
 *
 * @property \backend\tests\UnitTester $tester
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class ModelTest extends TestCase
{
    /**
     * Data for mock in POST
     *
     * @return array
     */
    private function getData()
    {
        return [
            'TestModel' => [
                [
                    'id' => 1,
                    'title' => 'record 1',
                    'description' => 'record 1'
                ],
                [
                    'id' => 2,
                    'title' => 'record 2',
                    'description' => 'record 2'
                ],
                [
                    'id' => 3,
                    'title' => 'record 2',
                    'description' => 'record 2'
                ],
            ],
        ];
    }

    public function testInstanceOf()
    {
        $this->assertInstanceOf(\yii\base\Model::className(), new TestModel());
    }

    public function testCreateMultiple()
    {
        $expected = [
            new TestModel(),
            new TestModel(),
            new TestModel(),
        ];
        $actual = Model::createMultiple(TestModel::className(), $this->getData());

        $this->assertEquals($expected, $actual);
    }

    public function testLoadMultiple()
    {
        $models = [
            new TestModel(),
            new TestModel(),
            new TestModel(),
        ];

        $expected = [
            new TestModel([
                'id' => 1,
                'title' => 'record 1',
                'description' => 'record 1'
            ]),
            new TestModel([
                'id' => 2,
                'title' => 'record 2',
                'description' => 'record 2'
            ]),
            new TestModel([
                'id' => 3,
                'title' => 'record 2',
                'description' => 'record 2'
            ]),
        ];

        $this->specify(
            'Without index',
            function () use ($models, $expected) {
                $actual = Model::loadMultiple($models, $this->getData());

                $this->assertTrue($actual);
                $this->assertEquals($expected[0]->id,           $models[0]->id);
                $this->assertEquals($expected[1]->title,        $models[1]->title);
                $this->assertEquals($expected[2]->description,  $models[1]->description);
            }
        );

        $this->specify(
            'With index',
            function () use ($models, $expected) {
                $index = 777;
                $dataWithIndex['TestModel'][$index] = $this->getData()['TestModel'];

                $actual = Model::loadMultiple($models, $dataWithIndex, null, $index);

                $this->assertTrue($actual);
                $this->assertEquals($expected[0]->id,           $models[0]->id);
                $this->assertEquals($expected[1]->title,        $models[1]->title);
                $this->assertEquals($expected[2]->description,  $models[1]->description);
            }
        );
    }

    public function testSaveRelModels()
    {
        $model = new TestTable(['id' => 7]);
        $config = $this->tester->readRelatedFormConfig($model);
        $flag = true;

        /* @var TestRelationTable[] $relModels */
        $key = key($model->relModels);
        $relModels = $model->relModels[$key];
        $className = TestRelationTable::className();
        $data = [
            'TestRelationTable' => [
                [
                    'test_data_field' => 'test 1'
                ],
                [
                    'test_data_field' => 'test 2'
                ],
                [
                    'test_data_field' => 'test 3'
                ],
            ]
        ];
        $relModels = Model::createMultiple($className, $data);
        Model::loadMultiple($relModels, $data);
        Model::saveRelModels($model, $relModels, $config, $key, [], $className, $flag);

        $this->assertTrue($flag);
        $this->tester->seeInDb(TestRelationTable::tableName(), [
            'rel_table_id' => 7,
            'test_data_field' => 'test 1'
        ]);
        $this->tester->seeInDb(TestRelationTable::tableName(), [
            'rel_table_id' => 7,
            'test_data_field' => 'test 2'
        ]);
        $this->tester->seeInDb(TestRelationTable::tableName(), [
            'rel_table_id' => 7,
            'test_data_field' => 'test 3'
        ]);
    }
}
