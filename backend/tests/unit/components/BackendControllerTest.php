<?php
namespace backend\tests\unit\components;

use Yii;
use yii\web\Controller;
use common\tests\base\unit\DbTestCase;
use common\tests\fixtures\LanguageFixture;
use backend\tests\fake\models\TestTable;
use backend\tests\fake\controllers\BackendController;
use backend\tests\fixtures\TestTableFixture;
use backend\tests\fixtures\TestRelationTableFixture;

/**
 * Test case for [[backend\components\BackendController]]
 * @see \backend\components\BackendController
 *
 * @property \backend\tests\UnitTester $tester
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class BackendControllerTest extends DbTestCase
{
    /**
     * @var BackendController
     */
    private $controller;


    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'testTable' => ['class' => TestTableFixture::className()],
            'testRelationTable' => ['class' => TestRelationTableFixture::className()],
            'language' => ['class' => LanguageFixture::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();
        $this->controller = new BackendController(null, null);
    }

    public function testInstanceOf()
    {
        $this->assertInstanceOf(Controller::className(), $this->controller);
    }

    public function testFindModel()
    {
        $this->specify(
            'existing model',
            function () {
                $id = 1;
                $expected = TestTable::findOne($id);
                $actual = $this->controller->findModel($id);

                $this->assertEquals($expected, $actual);
            }
        );

        $this->specify(
            'not existing model',
            function () {
                $this->expectException('\yii\web\NotFoundHttpException');
                $this->controller->findModel(null);
            }
        );
    }

    public function testLoadModels()
    {
        Yii::$app->getRequest()->setData([
            'TestTable' => [
                'id' => 1,
                'test_table' => 'test'
            ],
            'TestRelationTable' => [
                [
                    'id' => 1,
                    'rel_table_id' => 1,
                    'language' => 'en-US',
                    'test_data_field' => 'test english translation'
                ],
                [
                    'id' => 2,
                    'rel_table_id' => 1,
                    'language' => 'uk-UA',
                    'test_data_field' => 'test ukrainian translation'
                ],
                [
                    'id' => 3,
                    'rel_table_id' => 1,
                    'language' => 'ru-RU',
                    'test_data_field' => 'test russian translation'
                ],
            ],
        ]);
        $model = TestTable::findOne(1);

        $this->assertTrue($this->controller->loadModels($model));
    }

    public function testLoadMultipleModels()
    {
        Yii::$app->getRequest()->setData([
            'TestTable' => [
                'first-model' => [
                    'id' => 1,
                    'test_field' => 'test'
                ],
                'second-model' => [
                    'id' => 2,
                    'test_field' => 'test2'
                ],
            ],
            'TestRelationTable' => [
                'first-model' => [
                    [
                        'id' => 1,
                        'rel_table_id' => 1,
                        'language' => 'en-US',
                        'test_data_field' => 'test english translation'
                    ],
                    [
                        'id' => 2,
                        'rel_table_id' => 1,
                        'language' => 'uk-UA',
                        'test_data_field' => 'test ukrainian translation'
                    ],
                    [
                        'id' => 3,
                        'rel_table_id' => 1,
                        'language' => 'ru-RU',
                        'test_data_field' => 'test russian translation'
                    ],
                ],
                'second-model' => [
                    [
                        'id' => 4,
                        'rel_table_id' => 2,
                        'language' => 'en-US',
                        'test_data_field' => 'test 2 english translation'
                    ],
                    [
                        'id' => 5,
                        'rel_table_id' => 2,
                        'language' => 'uk-UA',
                        'test_data_field' => 'test 2 ukrainian translation'
                    ],
                    [
                        'id' => 6,
                        'rel_table_id' => 2,
                        'language' => 'ru-RU',
                        'test_data_field' => 'test 2 russian translation'
                    ],
                ],
            ],
        ]);
        $models = TestTable::find()->all();
        $actual = $this->controller->loadMultipleModels([
            'first-model' => $models[0],
            'second-model' => $models[1]
        ]);

        $this->assertTrue($actual);
    }
}
