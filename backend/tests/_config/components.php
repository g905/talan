<?php
return [
    'db' => require(__DIR__ . '/db.php'),
    'request' => [
        'class' => \backend\tests\fake\yii\Request::className(),
    ],
    'user' => [
        'identityClass' => \common\models\User::className(),
        'returnUrl' => '/test/return/url',
    ],
    'authManager' => [
        'class' => yii\rbac\PhpManager::className(),
        'defaultRoles' => ['admin'],
        'itemFile' => '@tests/fake/rbac/items.php',
        'assignmentFile' => '@tests/fake/rbac/assigments.php',
    ],
];
