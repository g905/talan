<?php

/* @var $this yii\web\View */

use backend\modules\analytics\widgets\funnelAnalyticsWidget\FunnelAnalyticsWidget;
use backend\modules\analytics\widgets\orderSplineAnalyticsWidget\OrderSplineAnalyticsWidget;
use backend\modules\analytics\widgets\topStatisticAnalyticsWidget\TopStatisticAnalyticsWidget;

$this->title = 'Talan Admin Panel';
?>
<div class="site-index">
    <div class="row">

        <div class="col-sm-3">
            <?= FunnelAnalyticsWidget::widget() ?>
        </div>

        <div class="col-sm-9">
            <?= OrderSplineAnalyticsWidget::widget() ?>
        </div>
    </div>

    <div class="row">
        <?= TopStatisticAnalyticsWidget::widget() ?>
    </div>
</div>
