<?php
/**
 * @var $relModels ActiveRecord[]
 * @var int $limitMax
 * @var int $limitMin
 * @var $showTabs integer 0|1
 * @var $defaultValues
 */
use common\components\model\ActiveRecord;
use tolik505\relatedForm\RelatedFormWidget;

$formName = $relModels[0]->formName();
?>
<div class="form-group template-builder <?= ($limitMax > 0 || $limitMin > 0) ? 'limited-widget' : ''; ?>" data-limit-max-related-forms="<?= $limitMax; ?>" data-limit-min-related-forms="<?= $limitMin; ?>">
    <?php if ($limitMax > 0 || $limitMin > 0) : ?>
        <h4 class="limit-warning">
            <?php switch (true) {
                case $limitMax === $limitMin:
                    echo bt('You need to add exactly {count} forms.', 'related', ['count' => $limitMax]);
                    break;
                case $limitMax === 0:
                    echo bt('You need to add {count} forms or more.', 'related', ['count' => $limitMin]);
                    break;
                case $limitMin === 0:
                    echo bt('You need to add not more than {count} forms.', 'related', ['count' => $limitMax]);
                    break;
                default:
                    echo bt('Limit max: {limitMax}, limit min: {limitMin}.', 'related', ['limitMax' => $limitMax, 'limitMin' => $limitMin]);
            } ?>
        </h4>
    <?php endif; ?>
    <?php RelatedFormWidget::begin([
        'widgetContainer' => 'related_form_wrapper_' . $formName, // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
        'widgetBody' => '.template-builder', // required: css class selector
        'widgetItem' => '.content-append', // required: css class
        //'limit' => 4, // the maximum times, an element can be cloned (default 999)
        'min' => 0, // 0 or 1 (default 1)
        'insertButton' => '.add-item', // css class
        'deleteButton' => '.btn-template-delete', // css class
        'model' => $relModels[0],
        'formId' => 'main-form',
        'formFields' => ['dummy'],
    ]); ?>
    <div class="template-list related-model-panel">
        <div class="template-builder"><!-- widgetContainer -->
            <?php if (!$showTabs) : ?>
                <?= $this->render('_related_form_fields', ['form' => $form, 'relModels' => $relModels]) ?>
            <?php else : ?>
                <?= $this->render('_related_form_fields_lang', ['form' => $form, 'relModels' => $relModels]) ?>
            <?php endif; ?>
        </div>
        <div class="button-add">
            <button type="button" data-className="<?= $relModels[0]->className() ?>" class="btn btn-minw btn-square btn-success btn-template-builder add-item">
                <i class="si si-plus"></i> <?= Yii::t('back/related', 'Add'); ?>
            </button>
        </div>
    </div>
    <?php RelatedFormWidget::end(); ?>
</div>
