<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\menu\models\Menu */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => $model->getTitle(),
]);
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block block-themed">
    <div class="block-header bg-primary-dark">
        <h1 class="block-title"><i class="fa fa-cogs"></i> <?= Html::encode($this->title) ?></h1>
    </div>

    <div class="block-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
