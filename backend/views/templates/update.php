<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\components\BackendModel|\yii\db\ActiveRecord */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => $model->getTitle(),
    ]) . ' ' . ($model->hasAttribute('label') && $model->label ? $model->label : $model->id);
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => $model->hasAttribute('label') && $model->label ? $model->label : $model->id,
    'url' => ['view', 'id' => $model->id]
];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="block block-themed">
    <div class="block-header bg-primary-dark">
        <h1 class="block-title"><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
    </div>

    <div class="block-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
