<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use \backend\components\ModifiedDataColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\components\BackendModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $searchModel->getTitle();
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-sm-6 col-md-3">
        <?php if($searchModel->showCreateButton()): ?>
            <a class="block block-link-hover3 text-center" href="<?= \yii\helpers\Url::to(['create'])?>">
                <div class="block-content block-content-full block-sm-button black-stat-left" >
                    <div class="h1 font-w700 text-success"><i class="fa fa-plus"></i></div>
                </div>
                <div class="black-stat-right block-content block-content-full block-content-mini bg-gray-lighter text-success font-w600">
                    <?=Yii::t('app', 'Add new') ?>
                </div>
            </a>
        <?php endif; ?>
    </div>

    <?php
    foreach ($searchModel::getAdditionalHeaderConfig() as $headerConfig):
        ?>
        <div class="col-sm-6 col-md-3">
            <div class="block block-link-hover3 text-center" href="#">
                <div class="block-content block-content-full block-sm-button black-stat-left">
                    <div class="h1 font-w700 text-<?=$headerConfig['class']?>" data-toggle="countTo" data-to="<?=$headerConfig['leftValue']?>"><?=$headerConfig['leftValue']?></div>
                </div>
                <div class="black-stat-right block-content block-content-full block-content-mini bg-gray-lighter text-<?=$headerConfig['class']?> font-w600"><?=$headerConfig['rightValue']?></div>
            </div>
        </div>
    <?php
    endforeach;
    ?>


</div>



<?= \kartik\grid\GridView::widget([
    'options' => [
        'class' => 'grid-view table-responsive',
    ],
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $searchModel->getColumns('index'),
    'bordered' => false,
    'pjax'=>true,
    'pjaxSettings'=>[
        'neverTimeout'=>true,
    ],
    'resizableColumns'=>true,
    'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
    'panelTemplate' => '<div class="block block-themed">
        {panelHeading}
        {items}
        {panelFooter}
    </div>',
    'panelBeforeTemplate' => '',
    'panel' => [
        'heading'=>' <h1 class="block-title">'. Html::encode($this->title) .' </h1>',
        'headingOptions' => ['class' => 'block-header bg-primary-dark'],
    ],

    'tableOptions' => ['class' => 'table table-striped table-hover table-filtered'],
    'dataColumnClass' => ModifiedDataColumn::className()
]); ?>
