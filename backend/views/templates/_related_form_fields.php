<?php
/**
 * @var $relModels ActiveRecord[]
 */
use yii\helpers\Html;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use backend\components\FormBuilder;

$isAjax = false;
?>

<?php if (!isset($form)) : ?>
    <?php $form = FormBuilder::begin(['id' => 'dummy-form']) ?>
    <?php $isAjax = true ?>
<?php endif ?>

<?php foreach ($relModels as $index => $elModel) : ?>
    <div class="block block-themed block-rounded content-append filled item-<?= $index ?>" data-index="<?= $index ?>">
        <div class="block-header bg-primary-light">
            <ul class="block-options">
                <li><button type="button" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button></li>
                <li><button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button></li>
                <li><button type="button" data-toggle="block-option" class="btn-template-delete"><i class="si si-trash"></i></button></li>
                <li><button type="button" data-toggle="block-option" class="btn-template-mover ui-sortable-handle"><i class="si si-cursor-move"></i></button></li>
            </ul>
            <h3 class="block-title"><?= $elModel->getTitle() ?></h3>
        </div>
        <div class="block-content">
            <?php if (!$elModel->isNewRecord) : ?>
                <?= Html::activeHiddenInput($elModel, "[{$index}]id") ?>
            <?php endif ?>
            <?php foreach ($elModel->getFormConfig($index) as $attr => $el) : ?>
                <?= $form->renderField($elModel, "[$index]$attr", $el);  ?>
                <?php if (isset($elModel->errors["[$index]$attr"][0])) : ?>
                    <div class="help-block has-error"><?= $elModel->errors["[$index]$attr"][0] ?></div>
                <?php endif ?>
                <?= $form->renderUploadedFile($elModel,  "[$index]$attr", $el); ?>
                <?php if ($elModel instanceof Translateable && $elModel->isTranslateAttribute($attr)) : ?>
                    <?php foreach ($elModel->getTranslationModels() as $languageModel) : ?>
                        <?= $form->renderField($languageModel, "[$index][$languageModel->language]$attr", $el) ?>
                        <?php if (isset($languageModel->errors["[$index][$languageModel->language]$attr"][0])) : ?>
                            <div class="help-block has-error"><?= $languageModel->errors["[$index][$languageModel->language]$attr"][0] ?></div>
                        <?php endif ?>
                        <?= $form->renderUploadedFile($languageModel, $attr, $el, $languageModel->language) ?>
                    <?php endforeach ?>
                <?php endif ?>
            <?php endforeach ?>
        </div>
    </div>
<?php endforeach ?>

<?php if ($isAjax) : ?>
    <?php FormBuilder::end() ?>
<?php endif ?>
