<?php
/**
 * @var $relModels ActiveRecord[]
 */
use backend\components\FormBuilder;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use yii\helpers\Html;

$isAjax = false;
if (!isset($form)) {
    $form = FormBuilder::begin([
        'id' => 'dummy-form'
    ]);
    $isAjax = true;
}
foreach ($relModels as $index => $elModel) { ?>
    <div class="form-group content-append filled item-<?= $index ?>" data-index="<?= $index ?>"><!-- widgetBody -->
        <button type="button" class="btn btn-info btn-template-mover ui-sortable-handle"><i
                class="glyphicon glyphicon-move"></i></button>
        <button type="button" class="btn btn-danger btn-template-delete"><i class="glyphicon glyphicon-trash"></i>
        </button>
        <?php
        // necessary for update action.
        if (!$elModel->isNewRecord) {
            echo Html::activeHiddenInput($elModel, "[{$index}]id");
        }
        foreach ($elModel->getFormConfig($index) as $attr => $el) {
            if ($elModel instanceof Translateable && $elModel->isTranslateAttribute($attr)) { ?>
                <ul class="nav nav-tabs language_tabs">
                    <label class="switch">
                        <input type="checkbox">
                        <div class="swiper"></div>
                    </label>
                    <li class="active">
                        <a href="#tab-lang-content_<?= $index . '_' . $attr ?>"
                           data-toggle="tab"><?= $attr ?></a>
                    </li>
                    <?php foreach ($elModel->getTranslationModels() as $languageModel) { ?>
                        <li>
                            <a href="#tab-lang-content_<?= $index . '_' . $languageModel->language . '_' . $attr ?>"
                               data-toggle="tab"><?= $languageModel->language ?></a>
                        </li>
                    <?php } ?>
                </ul>
                <div class="tab-content">
                    <div id="tab-lang-content_<?= $index . '_' . $attr ?>" class="tab-pane active">
                        <?= $form->renderField($elModel, "[{$index}]$attr", $el)->label($elModel->getAttributeLabel($attr), ['class' => 'hidden_label control-label']);; ?>
                        <?php if (isset($elModel->errors["[{$index}]$attr"][0])) { ?>
                            <div class="help-block has-error"><?= $elModel->errors["[{$index}]$attr"][0] ?></div>
                        <?php } ?>
                        <?= $form->renderUploadedFile($elModel, "[{$index}]$attr", $el); ?>
                    </div>
                    <?php foreach ($elModel->getTranslationModels() as $languageModel) { ?>
                        <div id="tab-lang-content_<?= $index . '_' . $languageModel->language . '_' . $attr ?>"
                             class="tab-pane">
                            <?= $form->renderField($languageModel, "[{$index}][" . $languageModel->language . ']' . $attr, $el)->label($languageModel->getAttributeLabel($attr), ['class' => 'hidden_label control-label']); ?>
                            <?php if (isset($elModel->errors["[{$index}]$attr"][0])) { ?>
                                <div class="help-block has-error"><?= $elModel->errors["[{$index}]$attr"][0] ?></div>
                            <?php } ?>
                            <?= $form->renderUploadedFile($languageModel, $attr, $el, $languageModel->language); ?>
                        </div>
                    <?php } ?>
                </div>
                <?php

            } else {
                echo $form->renderField($elModel, "[{$index}]$attr", $el); ?>
                <?php if (isset($elModel->errors["[{$index}]$attr"][0])) { ?>
                    <div class="help-block has-error"><?= $elModel->errors["[{$index}]$attr"][0] ?></div>
                <?php }
                echo $form->renderUploadedFile($elModel, "[{$index}]$attr", $el);
            }
        }
        ?>
    </div>
    <?php
}
if ($isAjax) {
    FormBuilder::end();
}
