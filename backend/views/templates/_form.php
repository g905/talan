<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\components\model\Translateable;
use backend\components\FormBuilder;

/**
 * @var $this yii\web\View
 * @var $model \common\components\model\ActiveRecord
 * @var $form yii\bootstrap\ActiveForm
 */

$translationModels = [];
if ($model instanceof Translateable) {
    $translationModels = $model->getTranslationModels();
}
$action = isset($action) ? $action : '';
?>

<div class="menu-form">
    <?= Html::errorSummary(merge([$model], $translationModels), ['class' => 'alert alert-danger']); ?>
    <?php /** @var FormBuilder $form */
    $form = FormBuilder::begin([
        'action' => $action,
        'enableClientValidation' => true,
        'options' => ['id' => 'main-form', 'enctype' => 'multipart/form-data', 'class' => 'block']
    ]); ?>

    <?php
    $items = [];
    $formConfig = $model->getFormConfig();
//var_dump($formConfig);
//die;
    if (isset($formConfig['form-set'])) {
        $i = 0;
        foreach ($formConfig['form-set'] as $tabName => $tabConfig) {
            $class = 'tab_content_' . ++$i;
            $items[] = [
                'label' => $tabName,
                'content' => $form->prepareRows($model, $tabConfig, $translationModels, true),
                'options' => ['class' => $class],
                'linkOptions' => ['class' => $class],
            ];
        }
    } else {
        $items[] = [
            'label' => 'Контент',
            'content' => $form->prepareRows($model, $formConfig, $translationModels),
            'active' => true,
            'options' => ['class' => 'tab_content_content'],
            'linkOptions' => ['class' => 'tab_content'],
        ];
    }

    $seo = $model->getBehavior('seo');
    if ($seo && $seo instanceof MetaTagBehavior) {
        $seo = \notgosu\yii2\modules\metaTag\widgets\metaTagForm\Widget::widget(['model' => $model,]);
        $items[] = [
            'label' => 'SEO',
            'content' => $seo,
            'active' => false,
            'options' => ['class' => 'tab_seo_content'],
            'linkOptions' => ['class' => 'tab_seo'],
        ];
    }

    $socialShareContent = $model->getBehavior('socialShareContent');
    if ($socialShareContent instanceof \backend\modules\socialShareContent\behavior\SocialShareContentBehavior) {
        $items[] = [
            'label' => 'Social Share Content',
            'content' => \backend\modules\socialShareContent\widgets\metaTagForm\Widget::widget(compact('model', 'form')),
        ];
    }

    ?>

    <?= Tabs::widget(['items' => $items, 'navType' => 'nav-tabs nav-tabs-alt']) ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group btn-group-save">
                <?= Html::submitButton('<i class="si si-paper-plane"></i> ' . Yii::t('app', 'Save'), ['class' => 'btn btn-lg btn-primary']); ?>
            </div>
        </div>
    </div>

    <?php FormBuilder::end(); ?>
</div>
