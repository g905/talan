<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 16.02.17
 * Time: 13:54
 */
?>
<?php
use backend\assets\AppAsset;
use backend\modules\analytics\widgets\headerAnalyticsWidget\HeaderAnalyticsWidget;
use yii\helpers\Html;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
    <nav id="sidebar">
        <!-- Sidebar Scroll Container -->
        <div id="sidebar-scroll">
            <!-- Sidebar Content -->
            <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
            <div class="sidebar-content">
                <!-- Side Header -->
                <div class="side-header side-content bg-white-op">
                    <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                    <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                        <i class="fa fa-times"></i>
                    </button>
                    <a class="h5 text-white" href="<?= Yii::$app->getHomeUrl() ?>">
                        <svg class='header-logo' viewBox="63.9 301.6 84.2 115.5">
                            <polygon points="85.6,301.6 63.9,301.6 68.9,321.4 90.5,321.4 "></polygon>
                            <polygon points="127,301.6 97.6,417.1 118.6,417.1 148,301.6 "></polygon>
                        </svg>

                        <span class="h4 font-w600 sidebar-mini-hide"><?= Yii::$app->name ?></span>
                    </a>
                </div>
                <!-- END Side Header -->

                <!-- Side Content -->
                <div class="side-content">
                    <?= \backend\modules\menu\widgets\SideMenu::widget() ?>
                </div>
                <!-- END Side Content -->
            </div>
            <!-- Sidebar Content -->
        </div>
        <!-- END Sidebar Scroll Container -->
    </nav>

    <!-- Header -->
    <header id="header-navbar" class="content-mini content-mini-full">

        <?= \backend\modules\menu\widgets\UserMenu::widget() ?>

        <!-- Header Navigation Left -->
        <ul class="nav-header pull-left">
            <li class="hidden-md hidden-lg">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                    <i class="fa fa-navicon"></i>
                </button>
            </li>
            <li class="hidden-xs hidden-sm">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                    <i class="fa fa-ellipsis-v"></i>
                </button>
            </li>
            <li>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </li>
        </ul>
        <!-- END Header Navigation Left -->
    </header>
    <!-- END Header -->

    <!-- Main Container -->
    <main id="main-container">
        <?php
        if (Yii::$app->controller->route == 'site/index'){
            echo HeaderAnalyticsWidget::widget();
        }
        ?>
        <!-- Page Content -->
        <div class="content content-narrow">
            <?= \backend\widgets\Alert::widget([
                'closeButton' => [
                    'label' => ''
                ]
            ]); ?>
            <div class="content">
                <?= $content ?>
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->
    <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
        <div class="pull-right">
            All Rights Reserved © <span><?= date('Y') ?></span>
        </div>
        <div class="pull-left">
            Crafted with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="https://vintage.agency/" target="_blank">Vintage</a>
        </div>
    </footer>
</div>

<div class="bs-cropper-modal modal modal-hidden fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

