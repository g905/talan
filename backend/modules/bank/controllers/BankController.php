<?php

namespace backend\modules\bank\controllers;

use yii\web\Response;
use backend\modules\bank\models\Bank;
use backend\components\BackendController;

/**
 * BankController implements the CRUD actions for Bank model.
 */
class BankController extends BackendController
{
    public function getModelClass()
    {
        return Bank::class;
    }

    /**
     * Action used together with Select2 dropdown widget.
     *
     * @param null $search
     * @param null $id
     * @param bool $showNotChoose
     * @param null $clearText
     * @return array
     */
    public function actionSearch($search = null, $id = null, $showNotChoose = false, $clearText = null)
    {
        if (!isset($clearText)){
            $clearText = bt('Choose banks', 'completed-project');
        }
        response()->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($search)) {
            $search = '';
        }

        $query = Bank::find()
            ->select(['id', 'text' => 'label'])
            ->having(['like', 'text', $search])
            ->limit(10)
            ->asArray();
        $out['results'] = array_values($query->all());

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Bank::findOne($id)->label ?? null];
        }

        if ($showNotChoose){
            $notChooseValue = ['id' => '0', 'text' => $clearText];
            array_unshift($out['results'] , $notChooseValue);
        }
        return $out;
    }
}
