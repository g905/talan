<?php

namespace backend\modules\bank\models;

use common\models\BankToHypothec as CommonBankToHypothec;
use backend\modules\page\models\Hypothec;

/**
 * This is the model class for table "{{%bank_to_hypothec}}".
 *
 * @property int $bank_id Bank
 * @property int $hypothec_id Hypothec
 * @property int $position Position
 *
 * @property Bank $bank
 * @property Hypothec $hypothec
 */
class BankToHypothec extends CommonBankToHypothec
{
    public static function tableName()
    {
        return '{{%bank_to_hypothec}}';
    }

    public function rules()
    {
        return [
            [['bank_id', 'hypothec_id'], 'required'],
            [['bank_id', 'hypothec_id', 'position'], 'integer'],
            [['bank_id', 'hypothec_id'], 'unique', 'targetAttribute' => ['bank_id', 'hypothec_id']],
            [['bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bank::class, 'targetAttribute' => ['bank_id' => 'id']],
            [['hypothec_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hypothec::class, 'targetAttribute' => ['hypothec_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'bank_id' => bt('Bank', 'bank-to-hypothec'),
            'hypothec_id' => bt('Hypothec', 'bank-to-hypothec'),
            'position' => bt('Position', 'bank-to-hypothec'),
        ];
    }

    public function getBank()
    {
        return $this->hasOne(Bank::class, ['id' => 'bank_id']);
    }

    public function getHypothec()
    {
        return $this->hasOne(Hypothec::class, ['id' => 'hypothec_id']);
    }
}
