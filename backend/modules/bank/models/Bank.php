<?php

namespace backend\modules\bank\models;

use yii\grid\SerialColumn;
use common\models\BankToHypothec;
use common\models\Bank as CommonBank;
use common\components\CommonDataModel;
use backend\components\FormBuilder;
use backend\components\BackendModel;
use backend\modules\page\models\Hypothec;
use backend\components\StylingActionColumn;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;

/**
 * This is the model class for table "{{%bank}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $mortgage_rate
 * @property integer $published
 *
 * @property BankToHypothec[] $bankToHypothecs
 * @property Hypothec[] $hypothecs
 */
class Bank extends CommonBank implements BackendModel
{
    public $image;
    public $sign;

    public function init()
    {
        parent::init();
        $this->sign = $this->sign ?: security()->generateRandomString();
    }

    public static function tableName()
    {
        return '{{%bank}}';
    }

    public function rules()
    {
        return [
            [['label', 'mortgage_rate'], 'required'],
            [['mortgage_rate'], 'number'],
            [['published'], 'integer'],
            [['label', 'sign'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'bank'),
            'label' => bt('Label', 'bank'),
            'image' => bt('Image', 'bank'),
            'mortgage_rate' => bt('Mortgage rate in percents', 'bank'),
            'published' => bt('Published', 'bank'),
        ];
    }

    public function getBankToHypothecs()
    {
        return $this->hasMany(BankToHypothec::class, ['bank_id' => 'id']);
    }

    public function getHypothecs()
    {
        return $this->hasMany(Hypothec::class, ['id' => 'hypothec_id'])->viaTable('{{%bank_to_hypothec}}', ['bank_id' => 'id']);
    }

    public function getTitle()
    {
        return bt('Bank', 'bank');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => SerialColumn::class],
                    // 'id',
                    'label',
                    'mortgage_rate',
                    'published:boolean',
                    ['class' => StylingActionColumn::class],
                ];
            case 'view':
                return [
                    'id',
                    'label',
                    'mortgage_rate',
                    'published:boolean',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new BankSearch();
    }

    public function getFormConfig()
    {
        return [
            'label' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'mortgage_rate' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getPercentText()],
            ],
            'image' => [
                'type' => FormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'multiple' => false,
                    'attribute' => 'image',
                    'aspectRatio' => false,
                    'saveAttribute' => CommonBank::ATTR_BANK_SVG_IMAGE,
                    'allowedFileExtensions' => ['svg'],
                ]),
                'hint' => 'SVG файл с логотипом банка',
            ],
            'sign' => [
                'type' => FormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
            'published' => [
                'type' => FormBuilder::INPUT_CHECKBOX,
            ],
        ];
    }

    public static function getBanksUrl()
    {
        return '/bank/bank/search';
    }
}
