<?php

namespace backend\modules\bank\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BankSearch represents the model behind the search form about `Bank`.
 */
class BankSearch extends Bank
{
    public function rules()
    {
        return [
            [['id', 'published'], 'integer'],
            [['label', 'mortgage_rate'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = BankSearch::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere([
                'id' => $this->id,
                'published' => $this->published,
                'mortgage_rate' => $this->mortgage_rate,
            ])
            ->andFilterWhere(['like', 'label', $this->label]);

        return $dataProvider;
    }
}
