<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $block \backend\modules\admin\models\IpBlock */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
?>
<div class="site-login">

    <?php if (!$block): ?>


        <div class="block block-themed">
            <div class="block-header bg-primary-darker">
                <h3 class="block-title"><?=Yii::t('admin','Login')?></h3>
            </div>
            <div class="block-content">
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'class' => 'form-horizontal push-5-t']); ?>

                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>

                <?php if (Yii::$app->user->enableAutoLogin) : ?>
                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                <?php endif; ?>

                    <div class="form-group">
                        <?= Html::submitButton('<i class="fa fa-arrow-right push-5-r"></i> '.Yii::t('admin','Login'), ['class' => 'btn btn-sm btn-primary', 'name' => 'login-button']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php else: ?>
        <div class="alert alert-danger">
            <?= bt('Ip blocked') ?>
        </div>
    <?php endif; ?>
</div>
