<?php
/**
 * @var \backend\modules\admin\widgets\authLog\AuthLogWidget $this
 * @var \common\models\UserAuthLog[] $models
 */
?>
<table class="table table-bordered">
    <tr>
        <th><?= bt('Date') ?></th>
        <th><?= bt('Error') ?></th>
        <th><?= bt('IP') ?></th>
        <th><?= bt('Host') ?></th>
        <th><?= bt('Url') ?></th>
        <th><?= bt('User Agent') ?></th>
    </tr>

    <?php foreach ($models as $model): ?>
        <tr>
            <td><?= Yii::$app->formatter->asDatetime($model->date) ?></td>
            <td><?= $model->getErrorLabel() ?></td>
            <td><?= $model->ip ?></td>
            <td><?= $model->host ?></td>
            <td><?= Yii::$app->formatter->asUrl($model->url) ?></td>
            <td><?= $model->userAgent ?></td>
        </tr>
    <?php endforeach; ?>
</table>
