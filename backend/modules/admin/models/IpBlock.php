<?php

namespace backend\modules\admin\models;

use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\db\Query;

/**
 * This is the model class for table "{{%ip_block}}".
 *
 * @property integer $id
 * @property integer $date
 * @property string $ip
 * @property string $host
 */
class IpBlock extends \common\components\model\ActiveRecord implements BackendModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ip_block}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'ip', 'host'], 'required'],
            [['date'], 'integer'],
            [['ip', 'host'], 'string', 'max' => 255],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Date'),
            'ip' => Yii::t('app', 'Ip'),
            'host' => Yii::t('app', 'Host'),
        ];
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('app', 'Ip Block');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'date:datetime',
                    'ip',
                    'host',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'date:datetime',
                    'ip',
                    'host',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new IpBlockSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        return [
            'date' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'ip' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'host' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],

        ];
    }

    /**
     * @param int $timeOut
     *
     * @return array|bool|IpBlock
     */
    public static function isBlocked($timeOut = 86400)
    {
        $time = time() - $timeOut;

        /** @var Query $query */
        $query = IpBlock::find();

        $query->andWhere(['ip' => $_SERVER['REMOTE_ADDR']]);
        $query->andWhere(['>=', 'date', $time]);

        return $query->one();
    }

}
