<?php

namespace backend\modules\menu\models;

use backend\modules\contacts\models\City;
use common\models\SiteMenu as CommonSiteMenu;
use kartik\grid\GridView;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%site_menu}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $menu_position_id
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 */
class SiteMenu extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_menu}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'menu_position_id'], 'required'],
            [['city_id'], 'validateCity'],
            [['city_id', 'menu_position_id', 'published'], 'integer'],
            [['city_id'], 'exist', 'targetClass' => \common\models\City::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
        ];
    }

    /**
     *  validate city
     */
    public function validateCity($attribute, $params)
    {
        $currentMenu = SiteMenu::find()->where([
            'id' => $this->id
        ])
        ->exists();

        $existMenu = SiteMenu::find()
            ->where([
                'city_id' => $this->city_id,
                'menu_position_id' => $this->menu_position_id,
            ]);

        if ($currentMenu){
            $existMenu = $existMenu->andWhere([
                '<>',  'id', $this->id
            ]);
        }

        $existMenu = $existMenu->exists();

        if ($existMenu) {
            $this->addError($attribute, \Yii::t('front/site-menu', 'menu for this city and position already exist'));
        }

    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/site-menu', 'ID'),
            'city_id' => Yii::t('back/site-menu', 'City'),
            'menu_position_id' => Yii::t('back/site-menu', 'Menu position'),
            'published' => Yii::t('back/site-menu', 'Published'),
            'created_at' => Yii::t('back/site-menu', 'Created At'),
            'updated_at' => Yii::t('back/site-menu', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItems()
    {
        return $this->hasMany(SiteMenuItem::className(), ['site_menu_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/site-menu', 'Site Menu');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var Home $model */
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    [
                        'attribute' => 'menu_position_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var Home $model */
                            return CommonSiteMenu::getMenuPositionDictionary()[$model->menu_position_id];
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => CommonSiteMenu::getMenuPositionDictionary(),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any position')],
                    ],
                    'published:boolean',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'city_id',
                    'menu_position_id',
                    'published:boolean',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new SiteMenuSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                Yii::t('back/home', 'General') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\City::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'menu_position_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => CommonSiteMenu::getMenuPositionDictionary(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                ],
                Yii::t('back/home', 'Menu items') => [
                    $this->getRelatedFormConfig()['menu_items']
                ],
            ],

        ];

        return $config;
    }

    public function getRelatedFormConfig()
    {
        return [
            'menu_items' => [
                'relation' => 'menuItems',
            ],
        ];
    }


}
