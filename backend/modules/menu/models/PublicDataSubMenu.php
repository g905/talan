<?php

namespace backend\modules\menu\models;

/**
 * Class PublicDataSubMenu
 *
 * @package backend\modules\menu\models
 */
class PublicDataSubMenu extends SubMenu
{
    public function menuType(): int
    {
        return self::TYPE_PUBLIC_DATA;
    }
}
