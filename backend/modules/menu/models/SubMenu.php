<?php

namespace backend\modules\menu\models;

use common\contracts\Menuable;
use yii\behaviors\TimestampBehavior;
use common\models\SubMenu as CommonSubMenu;
use backend\helpers\StdColumn;
use backend\helpers\StdInput;
use backend\components\BackendModel;

/**
 * Class SubMenu
 *
 * @package backend\modules\menu\models
 */
abstract class SubMenu extends CommonSubMenu implements BackendModel, Menuable
{
    public function init()
    {
        $this->setAttribute('type', $this->menuType());
        $this->published !== null ?: $this->setAttribute('published', true);
        parent::init();
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'sub-menu'),
            'type' => bt('Type', 'sub-menu'),
            'object_id' => bt('Object ID', 'sub-menu'),
            'label' => bt('Label', 'sub-menu'),
            'link' => bt('Link', 'sub-menu'),
            'prepared_page_id' => bt('Prepared Page ID', 'sub-menu'),
            'show_right' => bt('Show Right', 'sub-menu'),
            'published' => bt('Published', 'sub-menu'),
            'position' => bt('Position', 'sub-menu'),
        ];
    }

    public function getTitle()
    {
        return bt('Sub-menu', 'sub-menu');
    }

    public function rules()
    {
        return [
            [['type', 'object_id', 'show_right', 'published', 'position'], 'integer'],
            [['label'], 'required'],
            [['prepared_page_id'], 'safe'],
            [['label', 'link'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    'type',
                    'object_id',
                    'label',
                    'link:url',
                    'show_right:boolean',
                    'prepared_page_id',
                    'published:boolean',
                    'position',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    'type',
                    'object_id',
                    'label',
                    'link:url',
                    'show_right:boolean',
                    'prepared_page_id',
                    'published:boolean',
                    'position',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new SubMenuSearch();
    }

    public function getFormConfig()
    {
        return [
            'label' => StdInput::text(),
            'link' => StdInput::text(),
            'prepared_page_id' => StdInput::preparedPage(),
            'show_right' => StdInput::checkbox(),
            'published' => StdInput::checkbox(),
        ];
    }
}
