<?php

namespace backend\modules\menu\models;

/**
 * Class CatalogSubMenu
 *
 * @package backend\modules\menu\models
 */
class CatalogSubMenu extends SubMenu
{
    public function menuType(): int
    {
        return self::TYPE_CATALOG;
    }
}
