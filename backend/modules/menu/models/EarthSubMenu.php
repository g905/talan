<?php

namespace backend\modules\menu\models;

/**
 * Class EarthSubMenu
 *
 * @package backend\modules\menu\models
 */
class EarthSubMenu extends SubMenu
{
    public function menuType(): int
    {
        return self::TYPE_EARTH;
    }
}
