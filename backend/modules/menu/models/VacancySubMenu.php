<?php

namespace backend\modules\menu\models;

/**
 * Class VacancySubMenu
 *
 * @package backend\modules\menu\models
 */
class VacancySubMenu extends SubMenu
{
    public function menuType(): int
    {
        return self::TYPE_VACANCY;
    }
}
