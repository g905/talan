<?php

namespace backend\modules\menu\models;

/**
 * Class CareerSubMenu
 *
 * @package backend\modules\menu\models
 */
class CareerSubMenu extends SubMenu
{
    public function menuType(): int
    {
        return self::TYPE_CAREER;
    }
}
