<?php

namespace backend\modules\menu\models;

/**
 * Class BuySubMenu
 *
 * @package backend\modules\menu\models
 */
class BuySubMenu extends SubMenu
{
    public function menuType(): int
    {
        return self::TYPE_BUY_PAGE;
    }
}
