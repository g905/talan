<?php

namespace backend\modules\menu\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%site_menu_item}}".
 *
 * @property integer $id
 * @property integer $site_menu_id
 * @property string $label
 * @property string $link
 * @property integer $prepared_page_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property SiteMenu $siteMenu
 */
class SiteMenuItem extends ActiveRecord implements BackendModel
{

    public function init()
    {
        $this->published = 1;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_menu_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_menu_id', 'published', 'position'], 'integer'],
            [['label'], 'validateLink'],
            [['label'], 'required'],
            [['label', 'link', 'prepared_page_id'], 'string', 'max' => 255],
            [['site_menu_id'], 'exist', 'targetClass' => \common\models\SiteMenu::className(), 'targetAttribute' => 'id'],
            //[['link'], 'url', 'defaultScheme' => 'http'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     *  validate link
     */
    public function validateLink($attribute, $params)
    {
        if (($this->link == '') && ($this->prepared_page_id == '')) {
            $this->addError('link', \Yii::t('front/site-menu-item', 'fill link or select prepared page'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/site-menu-item', 'ID'),
            'site_menu_id' => Yii::t('back/site-menu-item', 'Site menu'),
            'label' => Yii::t('back/site-menu-item', 'Label'),
            'link' => Yii::t('back/site-menu-item', 'Link'),
            'prepared_page_id' => Yii::t('back/site-menu-item', 'Prepared page'),
            'published' => Yii::t('back/site-menu-item', 'Published'),
            'position' => Yii::t('back/site-menu-item', 'Position'),
            'created_at' => Yii::t('back/site-menu-item', 'Created At'),
            'updated_at' => Yii::t('back/site-menu-item', 'Updated At'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteMenu()
    {
        return $this->hasOne(SiteMenu::className(), ['id' => 'site_menu_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/site-menu-item', 'Site Menu Item');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'site_menu_id',
                    'label',
                    'link',
                    'prepared_page_id',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'site_menu_id',
                    'label',
                    'link',
                    'prepared_page_id',
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new SiteMenuItemSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'link' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'prepared_page_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\SiteMenu::getPreparedPageDictionary(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }


}
