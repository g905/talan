<?php

namespace backend\modules\menu\models;

/**
 * Class HypotecSubMenu
 *
 * @package backend\modules\menu\models
 */
class HypotecSubMenu extends SubMenu
{
    public function menuType(): int
    {
        return self::TYPE_HYPOTEC;
    }
}
