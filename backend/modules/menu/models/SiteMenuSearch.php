<?php

namespace backend\modules\menu\models;

use backend\modules\contacts\models\City;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SiteMenuSearch represents the model behind the search form about `SiteMenu`.
 */
class SiteMenuSearch extends SiteMenu
{

    public static function getAdditionalHeaderConfig()
    {
        $cityCount = City::find()
            ->where([
                'published' => 1
            ])
            ->count();

        $modelCount = SiteMenu::find()
            ->where([
                'published' => 1
            ])
            ->count();

        $menuPositionCount = count(\common\models\SiteMenu::getMenuPositionDictionary());

        if (($cityCount == 0) || ($menuPositionCount == 0))
        {
            $filledCities = '-';
        }
        else{
            $filledCities = round($modelCount / ($cityCount * $menuPositionCount)  * 100).'%';
        }

        return [
            [
                'class' => 'regular',
                'leftValue' => $filledCities,
                'rightValue' => \Yii::t('back/home', 'Filled cities'),
                'link' => '',
            ],


        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'menu_position_id', 'published'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteMenuSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'menu_position_id' => $this->menu_position_id,
            'published' => $this->published,
        ]);

        return $dataProvider;
    }
}
