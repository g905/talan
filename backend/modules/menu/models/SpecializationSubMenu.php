<?php

namespace backend\modules\menu\models;

/**
 * Class SpecializationSubMenu
 *
 * @package backend\modules\menu\models
 */
class SpecializationSubMenu extends SubMenu
{
    public function menuType(): int
    {
        return self::TYPE_SPECIALIZATION;
    }
}
