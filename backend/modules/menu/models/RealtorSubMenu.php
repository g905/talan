<?php

namespace backend\modules\menu\models;

/**
 * Class RealtorSubMenu
 *
 * @package backend\modules\menu\models
 */
class RealtorSubMenu extends SubMenu
{
    public function menuType(): int
    {
        return self::TYPE_REALTOR;
    }
}
