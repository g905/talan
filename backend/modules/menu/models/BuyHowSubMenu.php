<?php

namespace backend\modules\menu\models;

/**
 * Class BuyHowSubMenu
 *
 * @package backend\modules\menu\models
 */
class BuyHowSubMenu extends SubMenu
{
    public function menuType(): int
    {
        return self::TYPE_BUY_HOW_PAGE;
    }
}
