<?php

namespace backend\modules\menu\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SubMenuSearch represents the model behind the search form about `SubMenu`.
 */
class SubMenuSearch extends SubMenu
{
    public function search($params)
    {
        $query = static::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'object_id' => $this->object_id,
            'show_right' => $this->show_right,
            'prepared_page_id' => $this->prepared_page_id,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }

    public function rules()
    {
        return [
            [['id', 'type', 'object_id', 'show_right', 'prepared_page_id', 'published', 'position'], 'integer'],
            [['label', 'link'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }
}
