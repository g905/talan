<?php

namespace backend\modules\menu\models;

/**
 * Class VacanciesPageSubMenu
 *
 * @package backend\modules\menu\models
 */
class VacanciesPageSubMenu extends SubMenu
{
    public function menuType(): int
    {
        return self::TYPE_VACANCIES_PAGE;
    }
}
