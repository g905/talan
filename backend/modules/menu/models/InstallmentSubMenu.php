<?php

namespace backend\modules\menu\models;

/**
 * Class InstallmentSubMenu
 *
 * @package backend\modules\menu\models
 */
class InstallmentSubMenu extends SubMenu
{
    public function menuType(): int
    {
        return self::TYPE_INSTALLMENT;
    }
}
