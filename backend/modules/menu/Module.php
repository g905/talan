<?php

namespace backend\modules\menu;

/**
 * Menu module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\menu\controllers';
}
