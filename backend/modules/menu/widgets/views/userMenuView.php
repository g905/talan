<?php
/**
 * @var $items array
 */
?>

<?php foreach ($items as $topItem): ?>

    <!-- Header Navigation Right -->
    <ul class="nav-header pull-right">
        <li>
            <div class="btn-group">
                <button class="btn btn-default  dropdown-toggle" data-toggle="dropdown" type="button">
                    <i class="si si-lock "></i> &nbsp; <?= $topItem['label'] ?>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">

                    <?php foreach ( $topItem['items'] as $item):?>
                        <li>
                            <?php
                            $icon = $item['icon'] ?? '';
                            $linkOptions = $item['linkOptions'] ?? [];
                            ?>
                            <?=\yii\helpers\Html::a($icon.$item['label'],$item['url'][0],$linkOptions);?>
                        </li>
                    <?php endforeach;?>

                </ul>
            </div>
        </li>

    </ul>

<?php endforeach; ?>
<!-- END Header Navigation Right -->
