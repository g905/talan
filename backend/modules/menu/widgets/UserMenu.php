<?php
/**
 * Author: Pavel Naumenko
 */

namespace backend\modules\menu\widgets;

use common\models\User;
use kartik\nav\NavX;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Class RightBarMenu
 * @package backend\modules\menu\widgets
 */
class UserMenu extends Widget
{
    public $items;

    public function init()
    {
        parent::init();

        $user = Yii::$app->user;
        if ($user->isGuest) {
            return false;
        }
        $userName = $user->identity->username;

        $items = [];
        $menuPath = Yii::getAlias('@backend/config/right-bar-menu-items.php');
        $items = require($menuPath);
        

        $this->items = [
            [
                'label' => $userName,
                'items' => $items,
            ]
        ];;
    }

    public function run()
    {
        if (Yii::$app->user->isGuest) {
            return null;
        }
        return $this->render('userMenuView', [
            'items' => $this->items
        ]);
    }
}
