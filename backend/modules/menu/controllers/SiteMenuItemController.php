<?php

namespace backend\modules\menu\controllers;

use backend\components\BackendController;
use backend\modules\menu\models\SiteMenuItem;

/**
 * SiteMenuItemController implements the CRUD actions for SiteMenuItem model.
 */
class SiteMenuItemController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return SiteMenuItem::className();
    }
}
