<?php

namespace backend\modules\menu\controllers;

use backend\components\BackendController;
use backend\modules\menu\models\SiteMenu;

/**
 * SiteMenuController implements the CRUD actions for SiteMenu model.
 */
class SiteMenuController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return SiteMenu::className();
    }
}
