<?php

namespace backend\modules\menu\controllers;

use yii\web\Controller;

class MenuController extends Controller
{

    public function actionRefreshSideMenu()
    {
        session()->remove('side_menu');

        return $this->redirect('/');
    }
}
