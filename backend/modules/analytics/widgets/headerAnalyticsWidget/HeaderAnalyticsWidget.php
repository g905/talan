<?php
/**
 * Author: art
 */
namespace backend\modules\analytics\widgets\headerAnalyticsWidget;

use backend\helpers\AnalyticsHelper;


/**
 * Class Widget
 */
class HeaderAnalyticsWidget extends \yii\base\Widget
{
    public $analyticsData = [];

    public function init()
    {
        $this->analyticsData  = AnalyticsHelper::getHeaderAnalytics();
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('headerAnalyticsWidgetView',
            [
                'analyticsData' => $this->analyticsData,
            ]);
    }
}
