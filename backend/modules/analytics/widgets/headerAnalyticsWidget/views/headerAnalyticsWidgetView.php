<?php
/** @var mixed[] $analyticsData */
?>

<div class="statistic-header content overflow-hidden" style="">

    <div class="col-xs-6 col-sm-3">
        <h1 class="h2 text-white animated zoomIn"><?=Yii::t('back/analytics','Dashboard')?></h1>
        <h2 class="h5 text-white-op animated zoomIn"> <?=Yii::$app->user->identity->email?></h2>
    </div>

    <div class="col-xs-6 col-sm-3">
        <div class="font-w700 text-white animated fadeIn dayplan_title2"><?=Yii::t('back/analytics','City total')?></div>
        <div class="h2 font-w300 text-primary animated flipInX"><span><i class="fa fa-map-marker"></i> <?= $analyticsData['cityCount']?></span> <span class="text-lowercase"></span></div>
    </div>

    <div class="col-xs-6 col-sm-3">
        <div class="font-w700 text-white animated fadeIn dayplan_title2"><?=Yii::t('back/analytics','Apartment Complex total')?></div>
        <div class="h2 font-w300 text-primary animated flipInX"><span><i class="fa fa-home"></i> <?= $analyticsData['apartmentComplexCount']?></span> <span class="text-lowercase"></span></div>
    </div>

    <div class="col-xs-6 col-sm-3">
        <div class="font-w700 text-white animated fadeIn dayplan_title2"><?=Yii::t('back/analytics','Apartment  total')?></div>
        <div class="h2 font-w300 text-primary animated flipInX"><span><i class="fa fa-inbox"></i> <?= $analyticsData['apartmentCount']?></span> <span class="text-lowercase"></span></div>
    </div>

</div>