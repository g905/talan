<?php

use backend\helpers\ImgHelper;
use common\models\CommonDataModel;
use yii\helpers\Url;

?>
<?php if (isset($analyticsData['topCity'])): ?>
<div class="col-sm-6 col-lg-3">
    <div class="block-content block-content-full"
         style="background: #fff; height: 60px; margin-bottom: 20px; padding: 5px">
        <div class="pull-right">
            <i class="fa fa-star fa-2x analytics-city-icon"></i>
        </div>
        <div class="pull-left">
            <div class="font-w600 push-5"><?=Yii::t('back/analytics','Top city')?> </div>
            <div class="text-muted text-cut">
                <a href="<?= Url::to(['contacts/city/update','id' => $analyticsData['topCity']->id])?>">
                    <?php
                    if (mb_strlen($analyticsData['topCity']->label) > 20){
                        echo mb_substr($analyticsData['topCity']->label, 0 , 20).'...';
                    }
                    else{
                        echo $analyticsData['topCity']->label;
                    }
                    ?>
                </a>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if (isset($analyticsData['topComplex'])): ?>
<div class="col-sm-6 col-lg-3">
    <div class="block-content block-content-full"
         style="background: #fff; height: 60px; margin-bottom: 20px; padding: 5px">
        <div class="pull-right">
            <?= ImgHelper::getEntityImage($analyticsData['topComplex'],'generalPhoto') ?>
        </div>
        <div class="pull-left">
            <div class="font-w600 push-5"><?=Yii::t('back/analytics','Top Apartment Complex')?></div>

            <div class="text-muted text-cut">
                <a href="<?= Url::to(['apartment/apartment-complex/update','id' => $analyticsData['topComplex']->id])?>">
                    <?php
                    if (mb_strlen($analyticsData['topComplex']->label) > 20){
                        echo mb_substr($analyticsData['topComplex']->label, 0 , 20).'...';
                    }
                    else{
                        echo $analyticsData['topComplex']->label;
                    }
                    ?>
                </a>
            </div>

        </div>
    </div>
</div>
<?php endif; ?>

<?php if (isset($analyticsData['topApartment'])): ?>
<div class="col-sm-6 col-lg-3">
    <div class="block-content block-content-full"
         style="background: #fff; height: 60px; margin-bottom: 20px; padding: 5px">
        <div class="pull-right">
            <?= ImgHelper::getEntityImage($analyticsData['topApartment'],'photo') ?>
        </div>
        <div class="pull-left">
            <div class="font-w600 push-5"><?=Yii::t('back/analytics','Top Apartment')?></div>

            <div class="text-muted text-cut">
                <a href="<?= Url::to(['apartment/apartment/update','id' => $analyticsData['topApartment']->id])?>">
                    <?php
                    if (mb_strlen($analyticsData['topApartment']->label) > 20){
                        echo mb_substr($analyticsData['topApartment']->label, 0 , 20).'...';
                    }
                    else{
                        echo $analyticsData['topApartment']->label;
                    }
                    ?>
                </a>
            </div>

        </div>
    </div>
</div>
<?php endif; ?>

<?php if (isset($analyticsData['topAction'])): ?>
<div class="col-sm-6 col-lg-3">
    <div class="block-content block-content-full"
         style="background: #fff; height: 60px; margin-bottom: 20px; padding: 5px">
        <div class="pull-right">
            <?= ImgHelper::getEntityImage($analyticsData['topAction'],'actionImage') ?>
        </div>
        <div class="pull-left">
            <div class="font-w600 push-5"><?=Yii::t('back/analytics','Top Action')?></div>

            <div class="text-muted text-cut">
                <a href="<?= Url::to(['apartment/promo-action/update','id' => $analyticsData['topAction']->id])?>">
                    <?php
                    if (mb_strlen($analyticsData['topAction']->label) > 20){
                        echo mb_substr($analyticsData['topAction']->label, 0 , 20).'...';
                    }
                    else{
                        echo $analyticsData['topAction']->label;
                    }
                    ?>
                </a>
            </div>

        </div>
    </div>
</div>
<?php endif; ?>