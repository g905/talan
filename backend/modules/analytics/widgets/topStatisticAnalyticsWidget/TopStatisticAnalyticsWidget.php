<?php
/**
 * Author: art
 */
namespace backend\modules\analytics\widgets\topStatisticAnalyticsWidget;

use backend\modules\apartment\models\Apartment;
use backend\modules\apartment\models\ApartmentComplex;
use backend\modules\apartment\models\PromoAction;
use backend\modules\contacts\models\City;


/**
 * Class Widget
 */
class TopStatisticAnalyticsWidget extends \yii\base\Widget
{
    public $analyticsData = [];

    public function init()
    {
        $topCity = City::find()->orderBy(['view_count' => SORT_DESC])->one();
        $topApartment = Apartment::find()->orderBy(['view_count' => SORT_DESC])->one();
        $topComplex = ApartmentComplex::find()->orderBy(['view_count' => SORT_DESC])->one();
        $topAction = PromoAction::find()->orderBy(['view_count' => SORT_DESC])->one();

        $this->analyticsData = [
            'topCity' => $topCity,
            'topApartment' => $topApartment,
            'topComplex' => $topComplex,
            'topAction' => $topAction,
        ];

        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('topStatisticAnalyticsWidgetView',
            [
                'analyticsData' => $this->analyticsData,
            ]);
    }
}
