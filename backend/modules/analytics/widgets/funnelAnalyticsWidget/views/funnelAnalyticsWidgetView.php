<div class="block  block-bordered">
    <div class="block-header ">
        <h3 class="block-title"><?=Yii::t('back/analytics','Excursion requests funnel')?></h3>
    </div>
    <div class="block-content block-content-full bg-gray-lighter text-center chart-block">
        <?=
        \dosamigos\highcharts\HighCharts::widget([
            'clientOptions' => [
                'chart' => [
                    'type' => 'funnel'
                ],
                'exporting' => [
                    'enabled' => false
                ],
                'title' => [
                    'text' => false
                ],
                'plotOptions' => [
                    'series' => [
                        'dataLabels' => [
                            'enabled' => true,
                            'softConnector' => false,
                        ],
                        'neckWidth' => '20%',
                        'neckHeight' => '35%',
                        'width' => '80%',
                        'center' => ['45%', '50%'],

                    ],
                ],
                'legends' => [
                    'enabled' => true,
                ],
                'series' => [
                    [
                        'name' => Yii::t('back/analytics','Count:'),
                        'data' => [
                            [ Yii::t('back/analytics','Visits total'), $analyticsData['visit_count']],
                            [ Yii::t('back/analytics','Visits unique'), $analyticsData['unique_visit_count']],
                            [ Yii::t('back/analytics','Apartment view'), $analyticsData['apartment_view_count']],
                            [ Yii::t('back/analytics','Excursion'), $analyticsData['excursion_count']],
                        ]
                    ],

                ]
            ]
        ]);
        ?>
    </div>

</div>