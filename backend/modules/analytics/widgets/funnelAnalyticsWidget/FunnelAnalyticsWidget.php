<?php
/**
 * Author: art
 */
namespace backend\modules\analytics\widgets\funnelAnalyticsWidget;


use backend\modules\analytics\models\AnalyticsSummary;
use backend\modules\request\models\RequestExcursion;
use dosamigos\highcharts\HighChartsAsset;

/**
 * Class Widget
 */
class FunnelAnalyticsWidget extends \yii\base\Widget
{
    public $analyticsData = [];

    public function init()
    {

        $excursionCount = RequestExcursion::find()->count();
        $this->analyticsData = [
            'visit_count' => 0,
            'unique_visit_count' => 0,
            'apartment_complex_view_count' => 0,
            'apartment_view_count' => 0,
            'excursion_count' => intval($excursionCount),
        ];
        $analyticsSummary = AnalyticsSummary::find()->asArray()->all();
        foreach ($analyticsSummary as $summary){
            $this->analyticsData['visit_count'] += $summary['visit_count'];
            $this->analyticsData['unique_visit_count'] += $summary['unique_visit_count'];
            $this->analyticsData['apartment_complex_view_count'] += $summary['apartment_complex_view_count'];
            $this->analyticsData['apartment_view_count'] += $summary['apartment_view_count'];
        }

        parent::init();

        $this->view->registerJsFile('js/funnel.js', [
            'depends' => [HighChartsAsset::className()]
        ]);
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('funnelAnalyticsWidgetView',
            [
                'analyticsData' => $this->analyticsData,
            ]);
    }
}
