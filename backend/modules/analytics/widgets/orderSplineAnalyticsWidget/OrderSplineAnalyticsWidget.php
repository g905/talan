<?php
/**
 * Author: art
 */
namespace backend\modules\analytics\widgets\orderSplineAnalyticsWidget;

use backend\components\AnalyticsHelper;
use backend\modules\analytics\models\AnalyticsSummary;
use backend\modules\order\models\Order;
use backend\modules\request\models\RequestExcursion;

/**
 * Class Widget
 */
class OrderSplineAnalyticsWidget extends \yii\base\Widget
{
    public $analyticsData = [];

    public function init()
    {

        $excursions = RequestExcursion::find()->select([
            'excursion_count' => 'COUNT(id)',
            'excursion_day' => ' DATE(FROM_UNIXTIME(created_at))'
        ])
            ->groupBy(['excursion_day'])
            ->asArray()
            ->all();


        foreach ($excursions as $excursion){
            $excursionData = [
                (strtotime($excursion['excursion_day'])+(3*60*60))*1000,
                intval($excursion['excursion_count'])
            ];

            $this->analyticsData[] = $excursionData;
        }

        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('orderSplineAnalyticsWidgetView',
            [
                'analyticsData' => $this->analyticsData,
            ]);
    }
}
