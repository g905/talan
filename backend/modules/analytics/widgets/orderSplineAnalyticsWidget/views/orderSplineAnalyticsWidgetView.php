<div class="block  block-bordered">
    <div class="block-header ">
        <h3 class="block-title"><?= Yii::t('back/analytics', 'Excursion requests') ?> </h3>
    </div>
    <div class="block-content block-content-full bg-gray-lighter text-center chart-block">
        <?=
        \dosamigos\highcharts\HighCharts::widget([
            'clientOptions' => [
                'chart' => [
                    'type' => 'column',
                ],
                'exporting' => [
                    'enabled' => false
                ],
                'yAxis' => [
                    'title' => [
                        'text' => Yii::t('back/analytics', 'Excursion requests count')
                    ]
                ],
                'xAxis' => [
                    'type' => 'datetime',
                ],
                'title' => [
                    'text' => false
                ],
                'series' => [
                    [
                        'name' => Yii::t('back/analytics', 'Excursion requests count'),
                        'data' => $analyticsData
                    ],
                ],
            ]
        ]);
        ?>
    </div>
</div>

