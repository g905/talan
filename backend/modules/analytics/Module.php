<?php

namespace backend\modules\analytics;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\analytics\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
