<?php

namespace backend\modules\analytics\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AnalyticsSummarySearch represents the model behind the search form about `AnalyticsSummary`.
 */
class AnalyticsSummarySearch extends AnalyticsSummary
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'visit_count', 'unique_visit_count', 'apartment_complex_view_count', 'apartment_view_count'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AnalyticsSummarySearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'visit_count' => $this->visit_count,
            'unique_visit_count' => $this->unique_visit_count,
        ]);

        return $dataProvider;
    }
}
