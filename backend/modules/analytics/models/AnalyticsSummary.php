<?php

namespace backend\modules\analytics\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%analytics_summary}}".
 *
 * @property integer $id
 * @property string $date
 * @property integer $visit_count
 * @property integer $unique_visit_count
 * @property integer $apartment_complex_view_count
 * @property integer $apartment_view_count
 */
class AnalyticsSummary extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%analytics_summary}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'date'],
            [['visit_count', 'unique_visit_count', 'apartment_complex_view_count', 'apartment_view_count'], 'integer'],
            [['date'], 'default', 'value' => null],
            [['visit_count'], 'default', 'value' => 0],
            [['unique_visit_count'], 'default', 'value' => 0],
            [['apartment_view_count'], 'default', 'value' => 0],
            [['apartment_complex_view_count'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/analytics', 'ID'),
            'date' => Yii::t('back/analytics', 'Date'),
            'visit_count' => Yii::t('back/analytics', 'Visit Count'),
            'unique_visit_count' => Yii::t('back/analytics', 'Unique Visit Count'),
            'apartment_complex_view_count' => Yii::t('back/analytics', 'Apartment Complex View Count'),
            'apartment_view_count' => Yii::t('back/analytics', 'Apartment View Count'),
        ];
    }

    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/analytics', 'Analytics Summary');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'date',
                    'visit_count',
                    'unique_visit_count',
                    'product_view_count',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'date',
                    'visit_count',
                    'unique_visit_count',
                    'apartment_complex_view_count',
                    'apartment_view_count',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new AnalyticsSummarySearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'date' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \metalguardian\dateTimePicker\Widget::className(),
                'options' => [
                    'mode' => \metalguardian\dateTimePicker\Widget::MODE_DATE,
                    'options' => [
                        'class' => 'form-control',
                    ],
                ],
            ],
            'visit_count' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'unique_visit_count' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }


}
