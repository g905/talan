<?php
namespace backend\modules\api;

class Module extends \yii\base\Module
{
	public $defaultRoute = 'api-agency';

	public $controllerNamespace = 'backend\modules\api\controllers';

	public function init()
	{
		parent::init();

		// custom initialization code goes here
	}
}
