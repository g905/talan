<?php
namespace backend\modules\api\models;

use backend\modules\apartment\models\Apartment;
use Yii;

/**
 * This is the model class for table "api_customer".
 */
class ApiCustomer extends \yii\db\ActiveRecord
{
	public $confirmAgreement;
	public static function tableName()
	{
		return 'api_customer';
	}

	public function rules()
	{
		return [
			['confirmAgreement', 'required', 'on' => 'create', 'message' => 'Необходимо согласиться с политикой обработки данных'],
			['confirmAgreement', 'safe'],
			[['name', 'phone', 'email', 'apartment_id'], 'required'],
			[['comment'], 'string'],
			[['apartment_id', 'agency_id'], 'integer'],
			[['created_at'], 'safe'],
			[['name', 'email'], 'string', 'max' => 50],
			['email', 'email'],
			[['phone'], 'string', 'max' => 21],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => Yii::t('back/api', 'Name'),
			'phone' => Yii::t('back/api', 'Phone'),
			'email' => Yii::t('back/api', 'Email'),
			'comment' => Yii::t('back/api', 'Comment'),
			'apartment_id' => Yii::t('back/api', 'Apartment ID'),
			'created_at' => Yii::t('back/api', 'Created At'),
			'confirmAgreement' => '',
			'agency_id' => 'Agency Id',
		];
	}

	public function scenarios() {
		$parentScenArr = parent::scenarios();
		$createArr = array_merge([], $parentScenArr['default']);
		$createArr[] = 'confirmAgreement';

		$parentScenArr['create'] = $createArr;
		return $parentScenArr;
	}

	public function getApartament() {
		return $this->hasOne(Apartment::class, ['id' => 'apartment_id']);
	}

	public function getAgency() {
		return $this->hasOne(ApiAgency::class, ['id' => 'agency_id']);
	}
}
