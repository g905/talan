<?php
namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "api_agency".
 */
class ApiAgency extends \yii\db\ActiveRecord
{

	public static function tableName()
	{
		return 'api_agency';
	}

	public function rules()
	{
		return [
			[['name', 'email', 'token'], 'required'],
			[['active', 'show_price',], 'integer'],
			[['created_at', 'updated_at'], 'safe'],
			[['name'], 'string', 'max' => 255],
			[['email'], 'string', 'max' => 50],
			[['email'], 'email'],
			[['callback_url'], 'url'],
			[['callback_url'], 'string', 'max' => 240],
			[['token'], 'string', 'max' => 128],
		];
	}

	public function scenarios() {
		$parentScene = parent::scenarios();
		//copy default array
		$createNewArr = array_merge([], $parentScene['default']);
		$cnaTokenKey = array_search('token', $createNewArr);
		if($cnaTokenKey !== false) {
			unset($createNewArr[$cnaTokenKey]);
			$createNewArr = array_merge([], $createNewArr);
		}

		$parentScene['createNew'] = $createNewArr;
		return $parentScene;
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('back/api', 'ID'),
			'name' => Yii::t('back/api', 'Name'),
			'email' => Yii::t('back/api','Email'),
			'active' => Yii::t('back/api', 'Active'),
			'show_price' => Yii::t('back/api', 'Show Price on client side'),
			'callback_url' => Yii::t('back/api', 'Site Callback Url'),
			'token' => Yii::t('back/api', 'Token'),
			'created_at' => Yii::t('back/api', 'Created At'),
			'updated_at' => Yii::t('back/api', 'Updated At'),
		];
	}

	public function getApiAgencyStats()
	{
		return $this->hasMany(ApiAgencyStatVisits::className(), ['api_agency_id' => 'id']);
	}

	public function beforeSave($insert) {

		if($insert) {
			$someHash = md5(md5($this->email));
			$someHash2 = md5(uniqid(null, false)) . '_' . uniqid(null, true) . '_' . $someHash;
			$this->token = $someHash2;
		}
		return parent::beforeSave($insert);
	}

	public static function getNameWithEmailStat($name, $email) {
		return $name . ' (' . $email . ')';
	}

	public function getNameWithEmail() {
		return self::getNameWithEmailStat($this->name, $this->email);
	}
}
