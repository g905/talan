<?php
namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "api_agency_stat".
 */
class ApiAgencyStatVisits extends \yii\db\ActiveRecord
{
	public $visit_date;
	public $visit_count;
	public $customer_count;
	public $customer_date;
	public $conversion;

	public static function tableName()
	{
		return 'api_agency_stat_visits';
	}

	public function rules()
	{
		return [
			[['api_agency_id', ], 'required'],
			[['api_agency_id', ], 'integer'],
			[['created_at'], 'safe'],
			[['api_agency_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApiAgency::className(), 'targetAttribute' => ['api_agency_id' => 'id']],
		];
	}

	public function attributeLabels()
	{
		return [
			'api_agency_id' => 'Api Agency ID',
			'created_at' => 'Created At',
			'visit_date' => Yii::t('back/api', 'Visit date'),
			'visit_count' => Yii::t('back/api', 'Visit count'),
		];
	}

	public function getApiAgency()
	{
		return $this->hasOne(ApiAgency::className(), ['id' => 'api_agency_id']);
	}

	public static function getVisitQuery() {
		$query = self::find()
			->select([
				'api_agency_id',
				'visit_date' => 'DATE(created_at)',
				'visit_count' => 'COUNT(DATE(created_at))',
			])
			->groupBy(['api_agency_id', 'DATE(created_at)'])
		;
		return $query;
	}

	public static function getConversionQuery() {

		$sqlCustomer = "( SELECT DATE(created_at) AS customer_date, COUNT(DATE(created_at)) AS customer_count "
			. " FROM talan.api_customer "
			. " GROUP BY DATE(created_at)) "
		;

		$query1 = self::find()
			->select([
				'visit_date' => 'DATE(created_at)',
				'visit_count' => 'COUNT(DATE(created_at))',
				'ct.customer_count',
				//'ct.customer_date',
			])
			->from(['v' => self::tableName()])
			->leftJoin(['ct' => $sqlCustomer], 'ct.customer_date = DATE(created_at)')
			->groupBy(['DATE(v.created_at)', 'ct.customer_count'])
		;

		return $query1;
	}
}
