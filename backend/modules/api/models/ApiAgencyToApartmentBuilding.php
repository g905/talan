<?php
namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "api_agency_to_apartment_building".
 */
class ApiAgencyToApartmentBuilding extends \yii\db\ActiveRecord
{

	public static function tableName()
	{
		return 'api_agency_to_apartment_complex';
	}

	public function rules()
	{
		return [
			[['api_agency_id', 'apartment_complex_id'], 'required'],
			[['api_agency_id', 'apartment_complex_id'], 'integer'],
			[['api_agency_id', 'apartment_complex_id'], 'unique', 'targetAttribute' => ['api_agency_id', 'apartment_complex_id']],
		];
	}

	public function attributeLabels()
	{
		return [
			'api_agency_id' => 'Api Agency ID',
			'apartment_complex_id' => 'Apartment complex ID',
		];
	}

	public static function getComplexIdsBy($agencyId) {
		$tmpRes = ApiAgencyToApartmentBuilding::find()
			->select('apartment_complex_id')
			->where(['api_agency_id' => $agencyId])
			->asArray()
			->all();

		$complexIds = [];
		if(is_array($tmpRes) && count($tmpRes)) {
			foreach($tmpRes as $oneComplex) {
				$complexIds[$oneComplex['apartment_complex_id']] = $oneComplex['apartment_complex_id'];
			}
		}
		return $complexIds;
	}
}
