<?php
namespace backend\modules\api\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\api\models\ApiAgency;

/**
 * ApiAgencySearch represents the model behind the search form of `backend\modules\api\models\ApiAgency`.
 */
class ApiAgencySearch extends ApiAgency
{

	public function rules()
	{
		return [
			[['id', 'active'], 'integer'],
			[['name', 'email', 'callback_url', 'token', 'created_at', 'updated_at'], 'safe'],
		];
	}

	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = ApiAgency::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'active' => $this->active,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
		]);

		$query->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'email', $this->email])
			->andFilterWhere(['like', 'callback_url', $this->callback_url])
			->andFilterWhere(['like', 'token', $this->token]);

		return $dataProvider;
	}
}
