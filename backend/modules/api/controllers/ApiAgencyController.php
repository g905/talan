<?php

namespace backend\modules\api\controllers;

use backend\components\BackendController;
use backend\modules\api\models\ApiAgencyToApartmentBuilding;
use common\models\ApartmentComplex;
use common\models\City;
use vintage\i18n\models\SourceMessage;
use Yii;
use backend\modules\api\models\ApiAgency;
use backend\modules\api\models\ApiAgencySearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ApiAgencyController implements the CRUD actions for ApiAgency model.
 */
class ApiAgencyController extends BackendController
{
	public function getModelClass()
	{
		return ApiAgency::class;
	}

	public function actionIndex()
	{
		$searchModel = new ApiAgencySearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	public function actionCreate()
	{
		$model = new ApiAgency();
		$model->scenario = 'createNew';

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	protected function findModel($id)
	{
		if (($model = ApiAgency::findOne($id)) !== null) {
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}

	public function actionSelectComplex($agency_id) {

		$model = self::findModel($agency_id);

		if($model) {
			$complexArr = ApartmentComplex::getCityComplexArr();
			$exitsComplexIds = ApiAgencyToApartmentBuilding::getComplexIdsBy($model->id);

			return $this->render('select-complex', [
				'model' => $model,
				'complexArr' => $complexArr,
				'exitsComplexIds' => $exitsComplexIds,
			]);
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}

	public function actionSaveComplexToAgency() {

		if(Yii::$app->request->isAjax) {
			$retData = ['code' => 0];

			$agencyId = Yii::$app->request->post('agencyId');
			$complexIds = Yii::$app->request->post('complexIds');
			// удалим все предыдущие
			ApiAgencyToApartmentBuilding::deleteAll(['api_agency_id' => $agencyId]);
			// добавим новые
			if(is_array($complexIds) && count($complexIds)) {

				// урезание кол-ва комплексов до одного
				$complexIds = array_shift($complexIds);
				$complexIds = [$complexIds];

				foreach($complexIds as $oneComplex) {
					$aatab = new ApiAgencyToApartmentBuilding();
					$aatab->api_agency_id = $agencyId;
					$aatab->apartment_complex_id = $oneComplex;
					$aatab->save();
				}
				$retData['count'] = count($complexIds);
			}
			$retData['code'] = 1;

			Yii::$app->response->format = Response::FORMAT_JSON;
			return $retData;
		}
		throw new NotFoundHttpException();
	}
}
