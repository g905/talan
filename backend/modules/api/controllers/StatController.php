<?php
namespace backend\modules\api\controllers;

use backend\components\BackendController;
use backend\modules\api\models\ApiAgencyStatVisits;
use backend\modules\api\models\ApiCustomer;
use yii\data\ActiveDataProvider;

/**
 * Class ApiAgencyController
 */
class StatController extends BackendController
{
	public function getModelClass() {

	}

	public function actionLead() {

		$customerProvider = new ActiveDataProvider([
			'query' => ApiCustomer::find()->with('apartament')->with('agency'),
			'sort' => [
				'defaultOrder' => [
					'id' => SORT_DESC,
				]
			],
		]);

		return $this->render('lead', [
			'customerProvider' => $customerProvider,
		]);
	}

	public function actionWidgetVisits() {

		$visitsProvider = new ActiveDataProvider([
			'query' => ApiAgencyStatVisits::getVisitQuery()->with('apiAgency'),
			'sort' => [
				'defaultOrder' => [
					'visit_date' => SORT_DESC,
				],
				'attributes' => [
					'visit_date',
					'visit_count',
				],
			],
		]);

		return $this->render('widget-visits', [
			'visitsProvider' => $visitsProvider,
		]);
	}

	public function actionConversion() {

		$visitsProvider = new ActiveDataProvider([
			'query' => ApiAgencyStatVisits::getConversionQuery(),
			'sort' => [
				'defaultOrder' => [
					'visit_date' => SORT_DESC,
				],
				'attributes' => [
					'visit_date',
					'visit_count',
				],
			],
		]);

		return $this->render('conversion', [
			'visitsProvider' => $visitsProvider,
		]);
	}
}