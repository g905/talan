<?php
use yii\helpers\Html;

\kartik\select2\Select2Asset::register($this);

$this->title = Yii::t('back/api', 'Select complexes for the agency') . ': ' . $model->nameWithEmail;
$this->params['breadcrumbs'][] = ['label' => Yii::t('back/api', 'Agencies List'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('back/api', 'Select complexes');

$isSavedComplexExist = is_array($exitsComplexIds) && count($exitsComplexIds);
?>
<div class="api-agency-create">
	<div class="form-group">
		<h1><?= Html::encode($this->title)?></h1>
	</div>

	<div class="form-group">
		<div class="row">
			<div class="col-sm-6">

				<?php if(is_array($complexArr) && count($complexArr)) : ?>
					<select id="cityComplexList" class="form-control" name="complexList">
					<?php foreach($complexArr as $oneComplex):
						$isReadOnly = false;
						$complexId = $oneComplex['id'];
						if(isset($exitsComplexIds[$complexId])) {
							$isReadOnly = true;
						}

						$optionStr = '<option value="' . $complexId . '"';
						if($isReadOnly) {
							$optionStr .= ' disabled="disabled" ';
						}
						$optionStr .= '>' . $oneComplex['city_name'] . ' - ' . $oneComplex['complex_name'] . '</option>';
						echo $optionStr;
						endforeach;
					?>
					</select>
				<?php endif; ?>
			</div>
			<div class="col-sm-6">
				<?php
				$btnDisableAttr = 'disabled="disabled"';
				if(!$isSavedComplexExist) {
					$btnDisableAttr = '';
				}
				?>
				<button type="button" id="addCityComplex" class="btn btn-primary" <?= $btnDisableAttr;?>>
					<?= Yii::t('back/api', 'Add'); ?>
				</button>
			</div>
		</div>

		<div class="cityComplexTable">
			<?php
			// отобразим список существующих комплексов
			if($isSavedComplexExist) {

				foreach($complexArr as $oneComplex) {

					if(isset($exitsComplexIds[$oneComplex['id']])) {
						echo '<div class="cctItem" data-id="' . $oneComplex['id'] . '">';
						echo '<div class="cctiName">' . $oneComplex['city_name'] . ' - ' . $oneComplex['complex_name'] . '</div>';
						echo '<div class="cctiRemove"><i class="fa fa-times" aria-hidden="true"></i></div></div>';
					}
				}
			}
			?>
		</div>

		<input type="hidden" name="agencyId" id="agencyId" value="<?= $model->id;?>">
		<button type="button" id="saveSelectedComplex" class="btn btn-success">
			<?= Yii::t('back/api', 'Save'); ?>
		</button>

		<?php
		$displayNoneClass = 'tlnDisplNone';
		if($isSavedComplexExist) {
			$displayNoneClass = '';
		}
		?>
		<div class="iframeContainer <?= $displayNoneClass; ?>">
			<label for="iframeTa"><?= Yii::t('back/api', 'Embed code to the site'); ?></label>
			<textarea id="iframeTa" class="" rows="6" readonly="readonly"><?php
				$siteUrl = rtrim(Yii::$app->params['baseApiUrl'], '/');
				$frameUrl = $siteUrl . '/api/excursion/step11?token=' . $model->token;
				$iframeStr = '<div class="mrTalanAgencyContainer" data-url="' . $frameUrl . '" data-width="100%" data-height="100%"></div>'
					. '<script type="text/javascript" src="' . $siteUrl . '/js/iframe-api.js"></script>'
				;
				echo $iframeStr;
			?></textarea>
		</div>
	</div>

</div>
<?php

$this->registerCssFile('/css/api/agency.api.style.css');
$this->registerJsFile('/js/api/agency_select_complex.js', [
	'depends' => [
		\yii\web\JqueryAsset::class,
	],
]);
