<?php
use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\Url;
use backend\components\ModifiedDataColumn;

$this->title = Yii::t('back/api', 'Agencies List');
$this->params['breadcrumbs'][] = $this->title;
$noYesArr = [
	0 => Yii::t('back/api', 'No'),
	1 => Yii::t('back/api', 'Yes'),
];
?>
<div class="api-agency-index">

	<div class="row">
		<div class="col-sm-6 col-md-3">
			<a class="block block-link-hover3 text-center" href="<?= \yii\helpers\Url::to(['create'])?>">
				<div class="block-content block-content-full block-sm-button black-stat-left" >
					<div class="h1 font-w700 text-success"><i class="fa fa-plus"></i></div>
				</div>
				<div class="black-stat-right block-content block-content-full block-content-mini bg-gray-lighter text-success font-w600">
					<?=Yii::t('app', 'Add new') ?>
				</div>
			</a>
		</div>
	</div>

	<?= \kartik\grid\GridView::widget([
		'options' => [
			'class' => 'grid-view table-responsive',
		],
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'name',
			'email:email',
			[
				'attribute' => 'active',
				'value' => function($data) use($noYesArr) {
					if(isset($noYesArr[$data->active])) {
						return $noYesArr[$data->active];
					}
					return '-';
				},
				'contentOptions' => [
					'class' => 'tlnTurnOnColumn',
				]
			],
			//'callback_url:url',
			//'token',
			//'created_at',
			//'updated_at',

			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update} {delete} {select-complex}',
				'buttons' => [
					'select-complex' => function($url, $model) {
						$newUrl = Url::to(['/api/api-agency/select-complex', 'agency_id' => $model->id]);
						return Html::a('<span class="fa fa-cogs"></span>', $newUrl, [
							'title' => Yii::t('back/api', 'Select complex'),
						]);
					},
				],
				'contentOptions' => [
					'class' => 'tlnActionColumn',
				]
			],
		],
		'bordered' => false,
		'pjax'=>true,
		'pjaxSettings'=>[
			'neverTimeout'=>true,
		],
		'resizableColumns'=>true,
		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
		'panelTemplate' => '<div class="block block-themed">
			{panelHeading}
			{items}
			{panelFooter}
		</div>',
		'panelBeforeTemplate' => '',
		'panel' => [
			'heading'=>' <h1 class="block-title">'. Html::encode($this->title) .' </h1>',
			'headingOptions' => ['class' => 'block-header bg-primary-dark'],
		],

		'tableOptions' => ['class' => 'table table-striped table-hover table-filtered'],
		'dataColumnClass' => ModifiedDataColumn::className()
	]); ?>
</div>
<style>
	.tlnActionColumn {
		width: 100px;
	}
	.tlnTurnOnColumn {
		width: 100px;
	}
</style>