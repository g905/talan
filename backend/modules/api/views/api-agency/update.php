<?php
use yii\helpers\Html;

$this->title = Yii::t('back/api', 'Updating Agency') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('back/api', 'Agencies List'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('back/api', 'Update');
?>
<div class="api-agency-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
