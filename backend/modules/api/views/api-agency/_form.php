<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$noYesArr = [
	0 => Yii::t('back/api', 'No'),
	1 => Yii::t('back/api', 'Yes'),
];
?>

<div class="api-agency-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="row">
		<div class="col-sm-6">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-sm-6">
			<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?= $form->field($model, 'active')->dropDownList($noYesArr) ?>
		</div>
		<div class="col-sm-6">
			<?php //echo $form->field($model, 'callback_url')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<?= $form->field($model, 'show_price')->checkbox(); ?>
		</div>
	</div>

	<div class="form-group">
		<?= Html::submitButton(Yii::t('back/api', 'Save'), ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
