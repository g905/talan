<?php
use yii\helpers\Html;

$this->title = Yii::t('back/api', 'Creating new Agency');
$this->params['breadcrumbs'][] = ['label' => Yii::t('back/api', 'Agencies List'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-agency-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
