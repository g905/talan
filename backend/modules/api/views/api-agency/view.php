<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('back/api', 'Agencies List'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$noYesArr = [
	0 => Yii::t('back/api', 'No'),
	1 => Yii::t('back/api', 'Yes'),
];
?>
<div class="api-agency-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('back/api_', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Yii::t('back/api', 'Select complex'), ['select-complex', 'agency_id' => $model->id], ['class' => 'btn btn-primary']); ?>
		<?= Html::a(Yii::t('back/api_', 'Delete'), ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => Yii::t('back/api', 'Are you sure you want to delete this item?'),
				'method' => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'id',
			'name',
			'email:email',
			[
				'attribute' => 'active',
				'value' => function($data) use($noYesArr) {
					if(isset($noYesArr[$data->active])) {
						return $noYesArr[$data->active];
					}
					return '-';
				},
			],
			[
				'attribute' => 'show_price',
				'value' => function($data) use($noYesArr) {
					if(isset($noYesArr[$data->show_price])) {
						return $noYesArr[$data->show_price];
					}
					return '-';
				}
			],
			//'callback_url:url',
			'token',
			'created_at',
			'updated_at',
		],
	]) ?>

</div>
