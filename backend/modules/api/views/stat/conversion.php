<?php
use yii\helpers\Html;
use backend\components\ModifiedDataColumn;

$this->title = Yii::t('back/menu', 'Conversion');

$this->params['breadcrumbs'][] = Yii::t('back/menu', 'Statistic');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-agency-index">
	<?= \kartik\grid\GridView::widget([
		'options' => [
			'class' => 'grid-view table-responsive',
		],
		'dataProvider' => $visitsProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'visit_date',
			'visit_count',
			[
				'attribute' => 'conversion',
				'label' => Yii::t('back/menu', 'Conversion'),
				'value' => function($data) {
					$customerCount = 0;
					if(!empty($data->customer_count)) {
						$customerCount = $data->customer_count;
					}
					return $customerCount / $data->visit_count;
				},
			],
			//'customer_count',
		],
		'bordered' => false,
		'pjax'=>true,
		'pjaxSettings'=>[
			'neverTimeout'=>true,
		],
		'resizableColumns'=>true,
		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
		'panelTemplate' => '<div class="block block-themed">
			{panelHeading}
			{items}
			{panelFooter}
		</div>',
		'panelBeforeTemplate' => '',
		'panel' => [
			'heading'=>' <h1 class="block-title">'. Html::encode($this->title) .' </h1>',
			'headingOptions' => ['class' => 'block-header bg-primary-dark'],
		],

		'tableOptions' => ['class' => 'table table-striped table-hover table-filtered'],
		'dataColumnClass' => ModifiedDataColumn::className()
	]); ?>
</div>