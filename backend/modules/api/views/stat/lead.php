<?php
use yii\helpers\Html;
use backend\components\ModifiedDataColumn;

$this->title = Yii::t('back/api', 'Lead list');

$this->params['breadcrumbs'][] = Yii::t('back/menu', 'Statistic');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-agency-index">

	<?= \kartik\grid\GridView::widget([
		'options' => [
			'class' => 'grid-view table-responsive',
		],
		'dataProvider' => $customerProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'name',
			'phone',
			'email',
			/*
			[
				'attribute' => 'comment',
				'contentOptions' => [
					'style' => 'max-width: 250px;',
				],
			],
			/**/
			[
				'attribute' => 'agency_id',
				'value' => function($data) {
					if(isset($data->agency)) {
						return $data->agency->name;
					}
					return '-';
				},
				'label' => 'Название агенства',
			],
			[
				'attribute' => 'apartament.label',
				'label' => Yii::t('back/api', 'Apartment Name'),
				'contentOptions' => [
					'style' => 'max-width: 100px;',
				],
			],
			'created_at',
		],
		'bordered' => false,
		'pjax'=>true,
		'pjaxSettings'=>[
			'neverTimeout'=>true,
		],
		'resizableColumns'=>true,
		'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
		'panelTemplate' => '<div class="block block-themed">
			{panelHeading}
			{items}
			{panelFooter}
		</div>',
		'panelBeforeTemplate' => '',
		'panel' => [
			'heading'=>' <h1 class="block-title">'. Html::encode($this->title) .' </h1>',
			'headingOptions' => ['class' => 'block-header bg-primary-dark'],
		],

		'tableOptions' => ['class' => 'table table-striped table-hover table-filtered'],
		'dataColumnClass' => ModifiedDataColumn::className()
	]); ?>
</div>