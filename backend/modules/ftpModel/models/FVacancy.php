<?php

namespace backend\modules\ftpModel\models;

use backend\modules\ftpModel\models\base\BaseVacancyModel;
use backend\modules\hr\models\Vacancy;
use common\models\Specialization;
use Exception;
use ReflectionClass;
use ReflectionProperty;
use yii\helpers\Console;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\Json;
use ruskid\csvimporter\CSVReader;
use ruskid\csvimporter\CSVImporter;
use common\helpers\Property;
use common\models\Apartment as CommonApartment;
use backend\modules\contacts\models\City;
use backend\modules\apartment\models\Apartment;
use backend\modules\ftpModel\components\FtpHelper;
use backend\modules\ftpModel\models\base\BaseFtpModel;
use backend\modules\apartment\models\ApartmentComplex;
use backend\modules\apartment\models\ApartmentBuilding;
use backend\modules\apartment\models\ApartmentEntrance;
use backend\modules\ftpModel\models\base\FtpModelInterface;

/**
 * Class FApartment
 *
 * @author art
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package backend\modules\ftpModel\models
 */
class FVacancy extends BaseVacancyModel
{
    const YES_VAL = 'Да';
    const NO_VAL = 'Нет';

    /**
     * @var mixed api fields properties.
     */
    public $id;
    public $label;
    public $alias;
    public $city_id;
    public $specialization;
    public $description;
    public $description_label;
    public $requirements_label;
    public $requirements_content;
    public $benefits_content;
    public $responsibilities_content;
    public $benefits_label;
    public $form_label;
    public $form_description;
    public $form_agreements;
    public $form_emails;
    public $form_onsubmit;
    public $position;
    public $detail_description;

    public $published;

    private $local_specializations = [
        "IT",
        "Дирекция",
        "Маркетинг",
        "Офис",
        "Продажи",
        "Развитие",
        "СЭО и Тендеры",
        "Топ-менеджмент",
        "Управление строительством",
        "Финансы",
        "Юристы"
    ];

    private $unavailable_cities = [
        "Томск",
        "Новороссийск",
        "Красноярск",
        "Краснодар",
        "Владимир",
        "Самара",
        "Казань",
        "Сургут",
        "Екатеринбург"

    ];

    private $available_cities = [
        "Ижевск",
        "Уфа",
        "Пермь",
        "Набережные Челны",
        "Тюмень",
        "Тверь",
        "Ярославль",
        "Владивосток",
        "Хабаровск",
        "Новосибирск",
        "Нижний Новгород",
        "Нижневартовск",
        "Ханты-Мансийск",
        "Севастополь"
    ];

    public function deactivateVacanciesFromUnavailableCities()
    {
        $vacs = Vacancy::find()->all();
        foreach($vacs as $vac)
        {
            if(!in_array($vac->city->label, $this->available_cities) )
            {
                $vac->published = false;
                $vac->save();
            }

        }
        $cities = City::find()->all();
        foreach($cities as $city)
        {
            if(!in_array($city->label, $this->available_cities) )
            {
                continue;
                //$city->delete();
                //$city->save();
            }
        }
    }

    public function switchSpecializations($specialization)
    {
        switch ($specialization)
        {
            case "Информационные технологии, интернет, телеком":
                return $this->local_specializations[0];
            case "Бухгалтерия, управленческий учет, финансы предприятия":
                return $this->local_specializations[9];
            case "Строительство, недвижимость":
                return $this->local_specializations[8];
            case "Административный персонал":
                return $this->local_specializations[1];
            case "Маркетинг, реклама, PR":
                return $this->local_specializations[2];
            case "Продажи":
                return $this->local_specializations[4];
            case "Высший менеджмент":
                return $this->local_specializations[7];
            case "Управление персоналом, тренинги":
                return $this->local_specializations[3];
            default:
                dump ('Не найдена специализация');
                return null;
        }
    }

    public function deactivateVacancies()
    {
        $vacancies = Vacancy::find()->all();
        foreach ($vacancies as $vac)
        {
            $vac->published = 0;
            $vac->save();
            dump('деактивируем вакансию '. $vac->label);
        }
    }

    public function getVacanciesList()
    {
        $this->deactivateVacancies();

        $data = $this->getVacancies();

        foreach ($data['items'] as $item)
        {
            //if($item['name'] !== 'Специалист по документообороту в отдел маркетинга') continue;
            //if (in_array($item['area']['name'], $this->unavailable_cities) )
            if(!in_array($item['area']['name'], $this->available_cities) )
            {
                dump('Пропускаем город '.$item['area']['name']);
                continue;
            }
            $vacs[] = $item['type'];
            $this->id = $item['id'];
            $this->label = $item['name'];
            $this->alias = Inflector::slug($this->label);
            $city = \common\models\City::find()->where(['label' => $item['area']['name']])->one();
            if ($city !== null)
            {
                $this->city_id = $city->id;
            } else {
                dump('Город '.$item['area']['name'].' не найден, создаю новый');
                $city = new \common\models\City();
                $city->published = 0;
                $city->label = $item['area']['name'];
                $city->alias = strtolower($item['area']['name']);
                $city->save(false);
                $this->city_id = $city->id;
                //continue;
                dump('создан город '.$city->label.'; ');
            }

            $this->specialization = $this->getVacancyDetail($item['id'])['specializations'];
            $this->detail_description = $this->getVacancyDetail($item['id'])['description'];
            $this->description = $item['snippet']['responsibility'];
            $this->form_label;
            $this->form_description;
            $this->description_label;
            $this->requirements_label;
            $this->benefits_label;
            $this->form_label;
            $this->form_description;
            $this->form_agreements;
            $this->form_emails;
            $this->form_onsubmit;
            $this->position;
            $this->published;

            $this->makeDescription();
            $this->makeVacancy();
            //dump($this->id);
            //$this->deleteVacancies();

        }
        return $vacs;
    }

    public function makeDescription()
    {
        $matches = null;
        $flag = false;
        preg_match('/(<p><strong>|<strong>|<p>)Обязанности:(<\/strong><\/p>|<\/strong>|<\/p>) <ul> (.*?) <\/ul>/si', $this->detail_description, $matches);

        count($matches) > 0 ? $test = trim($matches[0]) : $test = '';

        $this->responsibilities_content = preg_replace('/(<strong>|<p>|<p><strong>)Обязанности:(<\/strong>|<\/p>|<\/strong><\/p>) /si', "", $test);
        $matches = null;

        preg_match('/(<strong>|<p>|<p><strong>)Требования:(<\/strong>|<\/p>|<\/strong><\/p>) <ul> (.*?) <\/ul>/si', $this->detail_description, $matches);

        count($matches) > 0 ? $test = trim($matches[0]) : $test = '';

        $this->requirements_content = preg_replace('/(<strong>|<p>|<p><strong>)Требования:(<\/strong>|<\/p>|<\/strong><\/p>) /si', "", $test);

        $matches = null;

        preg_match('/(<strong>|<p>|<p><strong>)Условия:(<\/strong>|<\/p>|<\/strong><\/p>) <ul> (.*?) <\/ul>/si', $this->detail_description, $matches);

        count($matches) > 0 ? $test = trim($matches[0]) : $test = '';

        $this->benefits_content = preg_replace('/(<strong>|<p>|<p><strong>)Условия:(<\/strong>|<\/p>|<\/strong><\/p>) /si', "", $test);
        $matches = null;
    }

    public function makeSpecialization()
    {
        $spec = Specialization::find()->where(['top_label' => $this->switchSpecializations($this->specialization[0]['profarea_name'])])->one();


        if ($spec === null)
        {
            $spec = new Specialization();
            $spec->city_id = $this->city_id;
            $spec->top_label = $this->specialization[0]['profarea_name'];
            $spec->mission_sub_label;
            $spec->details_description;
            $spec->details_label;
            $spec->principles_label;
            $spec->gallery_description;
            $spec->gallery_label;
            $spec->articles_btn_label;
            $spec->articles_label;
            $spec->reviews_label;
            $spec->vacancies_btn_label;
            $spec->vacancies_label;
            $spec->color;
            $spec->manager_id;
            $spec->form_agreement;
            $spec->form_description;
            $spec->form_emails;
            $spec->form_label;
            $spec->form_onsubmit;
            $spec->position;
            //$spec->published = 1;
            $spec->created_at = date('U');
        }
        $spec->updated_at = date('U');
        $spec->published = 1;
        $spec->save(false);
        //dump($spec->top_label);
    return $spec;
    }

    public function deactivateVacsAndSpecs()
    {
        $specs = Specialization::find()->all();
        foreach ($specs as $spec)
        {
            $spec->published = 0;
            $spec->save(false);
        }
        $vacs = Vacancy::find()->all();
        foreach($vacs as $vac)
        {
            $vac->published = 0;
            $vac->save(false);
        }
    }

    public function deleteVacancies()
    {
        $vac = Vacancy::find()->all();
        foreach($vac as $v)
        {
            $v->delete();
            dump('deleted: '. $v->id);
        }
    }

    public function makeVacancy()
    {

        //dump($this->specialization[0]['id']);
        //file_put_contents('/var/www/VACANCIES.txt', print_r($this->specialization, true), FILE_APPEND);
        $vacancy = Vacancy::find()->where(['id' => $this->id])->one();

        if($vacancy == null)
        {
            $vacancy = new Vacancy();
            $vacancy->id = $this->id;
            $vacancy->created_at = date('U');

        }
        $vacancy->label = $this->label;
        $vacancy->alias = $this->alias;
        $vacancy->city_id = $this->city_id;
        $vacancy->specialization_id = $this->makeSpecialization()->id;
        $vacancy->description = $this->description;
        $vacancy->detail_description = $this->detail_description;

        //dump($this->responsibilities_content);

        if($this->responsibilities_content !== "")
        {
            $vacancy->responsibilities_content = $this->responsibilities_content;
            $vacancy->responsibilities_label = 'Обязанности:';
        }

        if($this->requirements_content !== "")
        {
            $vacancy->requirements_label = 'Требования:';
            $vacancy->requirements_content = $this->requirements_content;
        }

        if($this->benefits_content !== "")
        {
            $vacancy->benefits_content = $this->benefits_content;
            $vacancy->benefits_label = 'Преимущества:';
        }

        $vacancy->position = 0;
        $vacancy->published = 1;
        $vacancy->updated_at = date('U');
        $vacancy->save();
        //$vacancy->delete();
        dump($vacancy->id);
        dump($vacancy->label);
    }

}
