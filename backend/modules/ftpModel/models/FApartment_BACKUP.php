<?php

namespace backend\modules\ftpModel\models;

use Exception;
use ReflectionClass;
use ReflectionProperty;
use yii\helpers\Console;
use yii\helpers\StringHelper;
use ruskid\csvimporter\CSVReader;
use ruskid\csvimporter\CSVImporter;
use common\helpers\Property;
use common\models\Apartment as CommonApartment;
use backend\modules\contacts\models\City;
use backend\modules\apartment\models\Apartment;
use backend\modules\ftpModel\components\FtpHelper;
use backend\modules\ftpModel\models\base\BaseFtpModel;
use backend\modules\apartment\models\ApartmentComplex;
use backend\modules\apartment\models\ApartmentBuilding;
use backend\modules\ftpModel\models\base\FtpModelInterface;

/**
 * Class FApartment
 *
 * @author art
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package backend\modules\ftpModel\models
 */
class FApartment extends BaseFtpModel implements FtpModelInterface
{
    const YES_VAL = 'Да';
    const NO_VAL = 'Нет';

    /**
     * @var mixed api fields properties.
     */
    public $building_guid_DO_NOT_USE; // For now this field is not filled correctly by 1C, so DO NOT USE IT.
    public $complex_name;
    public $building_name;
    public $delivery_date;
    public $complex_position;
    public $apartment_group;
    public $apartment_guid;
    public $apartment_number;
    public $object_type;
    public $is_secondary_property;
    public $entrance;
    public $floor;
    public $apartment_number_on_floor;
    public $block_number;
    public $section_position_number;
    public $rooms;
    public $total_area;
    public $price_per_meter;
    public $current_price;
    public $commission;
    public $is_increased_commission;
    public $is_promotional;
    public $discount;
    public $is_free_decoration;
    public $floor_count;
    public $city;
    public $district;
    public $description;
    public $state;
    public $land_area;
    public $house_state;
    public $is_for_sale;
    public $project;
    public $complex_guid;
    public $installment_price;
    public $is_investment_product;
    public $queue;
    public $living_area;
    public $building_guid;
    /** @var array properties stack. */
    private $props;

    private function importData()
    {
        $filePath = $this->getFile();
        Console::stdout("Importing from file: $filePath\n");
        if ($filePath === false) {
            return false;
        }
        $importer = new CSVImporter();
        $importer->setData(new CSVReader(['filename' => $filePath, 'fgetcsvOptions' => ['delimiter' => ';']]));

        return $importer->getData();
    }

    public function synchronizeWithDb()
    {
        try {
            $this->logSuccess(bt('Apartment synchronization start', 'ftp-log'));
            $data = $this->importData();
            $processed = 0;
            Apartment::updateAll(['published' => Apartment::IS_UNPUBLISHED], ['published' => Apartment::IS_PUBLISHED]);
            Apartment::updateAll(['deleted' => Apartment::DELETED], ['deleted' => Apartment::NOT_DELETED]);
            foreach ($data as $apartmentInfo) {
                $this->fillAttributes($apartmentInfo);
                $this->convertToRealModel();
                $processed++;
                Console::moveCursorTo(1);
                Console::stdout("$processed");
            }
            Console::moveCursorTo(1);
            Console::stdout("Processed: $processed items.\n");
            $this->doubleLogSuccess(bt('Apartment synchronization finish', 'ftp-log'), bt('Apartment synchronization finish', 'ftp-log'));
            $ftpPhoto = new FPhoto();
            $ftpPhoto->synchronizeWithDb();
        } catch (Exception $e) {
            Console::error($e->getMessage() . "\n");
            $this->doubleLogError(bt('Synchronization error', 'ftp-log'), bt('Synchronization error: ', 'ftp-log') . $e->getMessage() . ' --- ' . $e->getTraceAsString() . ' --- ' . $e->getLine());
        }
    }

    protected function  fillAttributes($data)
    {
        $attributes = $this->getProps();
        foreach ($attributes as $i => $attribute) {
             //dump($attribute->getName() . ': ' . obtain($i, $data, '----'));
            $this->{$attribute->getName()} = obtain($i, $data, null);
        }
    }

    public function convertToRealModel()
    {
        if (!$this->city || !$this->complex_name || !$this->apartment_guid) {
            return;
        }
        $city = $this->makeCity();
        $complex = $this->makeComplex($city->id);
        $building = $this->makeBuilding($complex->id);
        $this->makeApartment($complex->id, $building->id);
    }

    private function makeCity()
    {
        $city = City::find()->where(['label' => $this->city])->one();
        if ($city === null) {
            $city = new City([
                'label' => $this->city,
                'alias' => FtpHelper::generateCyrillicAlias($this->city),
            ]);
            if ($city->save(false) === false) {
                $this->doubleLogError(bt('Apartment synchronization error', 'ftp-log'), bt('Apartment synchronization error. City can not save:', 'ftp-log') . ' ' . $this->city);

                return false;
            }
        }

        return $city;
    }

    private function makeComplex($cityId)
    {
        $subtype = Property::subtypeFromLabel($this->object_type);
        $type = Property::typeFromSubtype($subtype);
        $complex = ApartmentComplex::find()->where(['type' => $type, 'guid' => $this->complex_guid])->one();
        if ($complex === null) {
            $complex = new ApartmentComplex([
                'label' => $this->complex_name,
                'type' => $type,
                'guid' => $this->complex_guid,
                'alias' => FtpHelper::generateAlias($this->complex_name),
                'city_id' => $cityId,
                'lat' => '56.8497600',
                'lng' => '53.2044800',
            ]);
        }
        // update new fields
        $complex->guid = $this->complex_guid;
        $complex->type = $type;

        if ($complex->save(false) === false) {
            $this->doubleLogError(bt('Apartment synchronization error', 'ftp-log'), bt('Apartment synchronization error. Complex can not save:', 'ftp-log') . ' ' . $this->complex_name);
        }

        return $complex;
    }

    private function makeBuilding($complexId)
    {
        $building = ApartmentBuilding::find()->where(['guid' => $this->building_guid])->one();
        if ($building === null) {
            $label = StringHelper::truncate($this->building_name, 127, '');
            $building = new ApartmentBuilding([
                'building_complex_id' => $complexId,
                'label' => $label,
                'guid' => $this->building_guid,
                'alias' => FtpHelper::generateAlias($label),
                'block_number' => $this->block_number,
                'floors' => (int)$this->floor_count,
                'position' => (int)$this->section_position_number,
            ]);
        }
        // update new fields
        $building->building_complex_id = $complexId;
        $building->guid = $this->building_guid;
        $building->entrance = $this->entrance;
        $building->floors = (int)$this->floor_count;
        $building->block_number = $this->block_number;

        if ($building->save(false) === false) {
            $this->doubleLogError(bt('Apartment synchronization error', 'ftp-log'), bt('Apartment synchronization error. Building can not save:', 'ftp-log') . ' ' . $this->building_name);
        }

        return $building;
    }

    private function makeApartment($complexId, $buildingId)
    {
        $subtype = Property::subtypeFromLabel($this->object_type);
        $type = Property::typeFromSubtype($subtype);
        $apartment = Apartment::find()
            ->where(['guid' => $this->apartment_guid])
            ->one() ?: new Apartment();

        $apartment->setAttributes([
            'type' => $type,
            'subtype' => $subtype,
            'guid' => $this->apartment_guid,
            'address' => $this->complex_position,
            'apartment_complex_id' => $complexId,
            'apartment_building_id' => $buildingId,
            'label' => $this->generateLabel(),
            'delivery_date' => $this->delivery_date,
            'entrance' => $this->entrance,
            'floor' => $this->floor,
            'rooms_number' => $this->rooms,
            'total_area' => (float)str_replace(',', '.', $this->total_area),
            'living_space' => (float)str_replace(',', '.', $this->living_area),
            'price_from' => floatval($this->current_price),
            'price_per_meter' => floatval($this->price_per_meter),
            'installment_price' => floatval($this->installment_price),
            'description_text' => $this->description,
            'published' => $this->convertState(),
            'is_secondary' => $this->isSecondary(),
            'apartment_number' => $this->apartment_number,
            'apartment_number_on_floor' => $this->apartment_number_on_floor,
            'discount' => (int)$this->discount,
            'is_promotional' => $this->isPromotional(),
            'status' => $this->convertStatus(),
            'deleted' => Apartment::NOT_DELETED,
        ]);

        if ($apartment->save(false) === false) {
            $this->doubleLogError(bt('Apartment synchronization error', 'ftp-log'), bt('Apartment synchronization error. Apartment can not save:', 'ftp-log') . ' ' . $this->apartment_guid);
        }
    }

    protected function generateLabel()
    {
        $label = '';
        if ($this->object_type && $this->object_type != Property::SYNC_SUBTYPE_APARTMENT_VN) {
            $label .= $this->object_type;
        }
        if ($this->apartment_number) {
            $label .= ' №' . $this->apartment_number . ' ';
        }
        if ($this->rooms) {
            $label .= ' ' . t('{n, plural, =0{не задано} =1{# комната} one{# комната} few{# комнаты} many{# комнат} other{# комнаты}}', 'apartment', ['n' => intval($this->rooms)]);
        }
        $label = trim($label);
        if ($label == '') {
            $label = t('_Default label', 'apartment');
        }

        return $label;
    }

    protected function getDirectoryName()
    {
        return date('Y-m'); // return '2017-12';
    }

    protected function getFileName()
    {
        return date('Y-m-d') . '-import.csv'; // return '2017-12-31-import.csv';
    }

    public function getProps()
    {
        if ($this->props === null) {
            $ref = new ReflectionClass($this);
            $this->props = $ref->getProperties(ReflectionProperty::IS_PUBLIC);
        }

        return $this->props;
    }

    private function isSecondary()
    {
        return $this->is_secondary_property === self::YES_VAL;
    }

    private function convertState()
    {
        return $this->state == Property::SYNC_STATUS_AVAILABLE
            ? Property::STATUS_AVAILABLE
            : Property::STATUS_SOLD_OUT;
    }

    private function convertStatus()
    {
        switch ($this->state) {
            case Property::SYNC_STATUS_AVAILABLE:
                return CommonApartment::STATUS_AVAILABLE;
            case Property::SYNC_STATUS_PLEDGED:
            case Property::SYNC_STATUS_ORDERED:
            case Property::SYNC_STATUS_RESERVED:
                return CommonApartment::STATUS_RESERVED;
            case Property::SYNC_STATUS_SOLD_OUT:
                return CommonApartment::STATUS_SOLD;
            default:
                return CommonApartment::STATUS_UNDEFINED;
        }
    }

    private function isPromotional()
    {
        return $this->is_promotional == self::YES_VAL;
    }
}
