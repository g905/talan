<?php

namespace backend\modules\ftpModel\models;

use Yii;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;

/**
 * This is the model class for table "synchronize".
 *
 * @property integer $id
 * @property integer $add
 * @property integer $replaced
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Synchronize extends ActiveRecord implements BackendModel
{

    public static function tableName()
    {
        return 'synchronize';
    }

    public function rules()
    {
        return [
            [['add', 'replaced', 'status'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'add' => 'Add',
            'replaced' => 'Replaced',
            'status' => 'Status',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    public function getTitle()
    {
        return bt('Synchronize', 'app');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    'add',
                    'replaced',
                    'status',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    'add',
                    'replaced',
                    'status',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new SynchronizeSearch();
    }

    public function getFormConfig()
    {
        $config = [
            'add' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'replaced' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'status' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }


}
