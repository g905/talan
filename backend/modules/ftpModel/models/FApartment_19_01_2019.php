<?php

namespace backend\modules\ftpModel\models;

use Exception;
use ReflectionClass;
use ReflectionProperty;
use yii\helpers\Console;
use yii\helpers\StringHelper;
use yii\helpers\Json;
use ruskid\csvimporter\CSVReader;
use ruskid\csvimporter\CSVImporter;
use common\helpers\Property;
use common\models\Apartment as CommonApartment;
use backend\modules\contacts\models\City;
use backend\modules\apartment\models\Apartment;
use backend\modules\ftpModel\components\FtpHelper;
use backend\modules\ftpModel\models\base\BaseFtpModel;
use backend\modules\apartment\models\ApartmentComplex;
use backend\modules\apartment\models\ApartmentBuilding;
use backend\modules\apartment\models\ApartmentEntrance;
use backend\modules\ftpModel\models\base\FtpModelInterface;

/**
 * Class FApartment
 *
 * @author art
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package backend\modules\ftpModel\models
 */
class FApartment extends BaseFtpModel implements FtpModelInterface
{
    const YES_VAL = 'Да';
    const NO_VAL = 'Нет';

    /**
     * @var mixed api fields properties.
     */
    public $building_guid_DO_NOT_USE; // For now this field is not filled correctly by 1C, so DO NOT USE IT.
    public $complex_name;
    public $building_name;
    public $delivery_date;
    public $complex_position;
    public $apartment_group;
    public $apartment_guid;
    public $apartment_number;
    public $object_type;
    public $is_secondary_property;
    public $entrance;
    public $floor;
    public $apartment_number_on_floor;
    public $block_number;
    public $section_position_number;
    public $rooms;
    public $total_area;
    public $price_per_meter;
    public $current_price;
    public $commission;
    public $is_increased_commission;
    public $is_promotional;
    public $discount;
    public $is_free_decoration;
    public $floor_count;
    public $city;
    public $district;
    public $description;
    public $state;
    public $land_area;
    public $house_state;
    public $is_for_sale;
    public $project;
    public $complex_guid;
    public $installment_price;
    public $is_investment_product;
    public $queue;
    public $living_area;
    public $building_guid;
    public $building_type;
    /** @var array properties stack. */
    private $props;

    private function importData()
    {
        $filePath = $this->getFile();
        Console::stdout("Importing from file: $filePath\n");
        if ($filePath === false) {
            return false;
        }
        $importer = new CSVImporter();
        $importer->setData(new CSVReader(['filename' => $filePath, 'fgetcsvOptions' => ['delimiter' => ';']]));

        return $importer->getData();
    }



    private function importDataNew()
    {
        $filePath = $this->getJsonFile();
        $file = file_get_contents($filePath);
        Console::stdout("Importing from file: $filePath\n");
        if ($filePath === false) {
            return false;
        }

        //$importer = new CSVImporter();

        //$importer->setData(new CSVReader(['filename' => $filePath, 'fgetcsvOptions' => ['delimiter' => ';']]));
/*
        echo "<pre>";
        print_r($imp);
        echo "</pre>";
*/
        //return $importer->getData();
        return Json::decode($file, true);
    }



    public function synchronizeWithDb()
    {
        try {
            $this->logSuccess(bt('Apartment synchronization start', 'ftp-log'));
            $data = $this->importData();
            $processed = 0;
            Apartment::updateAll(['published' => Apartment::IS_UNPUBLISHED], ['published' => Apartment::IS_PUBLISHED]);
            Apartment::updateAll(['deleted' => Apartment::DELETED], ['deleted' => Apartment::NOT_DELETED]);
            foreach ($data as $apartmentInfo) {
                $this->fillAttributes($apartmentInfo);
                $this->convertToRealModel();
                $processed++;
                Console::moveCursorTo(1);
                Console::stdout("$processed");
            }
            Console::moveCursorTo(1);
            Console::stdout("Processed: $processed items.\n");
            $this->doubleLogSuccess(bt('Apartment synchronization finish', 'ftp-log'), bt('Apartment synchronization finish', 'ftp-log'));
            $ftpPhoto = new FPhoto();
            $ftpPhoto->synchronizeWithDb();
        } catch (Exception $e) {
            Console::error($e->getMessage() . "\n");
            $this->doubleLogError(bt('Synchronization error', 'ftp-log'), bt('Synchronization error: ', 'ftp-log') . $e->getMessage() . ' --- ' . $e->getTraceAsString() . ' --- ' . $e->getLine());
        }
    }

// Для импорта из JSON

    public function synchronizeWithDbNew()
    {
        try {
            $this->logSuccess(bt('Apartment synchronization start', 'ftp-log'));
            $data = $this->importDataNew();

            $processed = 0;
            $count = 0;

            Apartment::updateAll(['published' => Apartment::IS_UNPUBLISHED], ['published' => Apartment::IS_PUBLISHED]);
            Apartment::updateAll(['deleted' => Apartment::DELETED], ['deleted' => Apartment::NOT_DELETED]);

            foreach ($data["Города"] as $city) {
            	$this->city = $city["Город"];
            	$obCity = $this->makeCity();
            	dump($obCity->label);

              foreach ($city["ЖК"] as $complex) {
              	if ($complex["НазваниеКомплекса"] !== "Фруктовая") continue;

              	$this->complex_name = $complex["НазваниеКомплекса"];
				$this->complex_position = $complex["МестоположениеКомплекса"];
				$this->is_secondary_property = $complex["ВторичнаяНедвижимость"];
				$this->complex_guid = $complex["ГУИД_ЖК"];
        		$this->building_type = $complex["ТипПомещения"];

				$obComplex = $this->makeComplex($obCity->id, $complex["ТипПомещения"]);

				dump("cmplex id: ".$obComplex->id."; ".$obComplex->label);

                foreach ($complex["Здания"] as $build) {
                	$this->building_name = $build["Здание"];

                	$obBuilding = $this->makeBuilding($obComplex->id, $this->building_type);
                  //dump("Здание: " . $build["Здание"]);

                	dump($obBuilding->id);

                  foreach ($build["Подъезды"] as $k => $entrance) {
                  	$this->entrance = $entrance["НомерПодъезда"];
                  	$obEntrance = $this->makeEntrance($obBuilding->id);
                    dump("Подъезд: " . $entrance["НомерПодъезда"]);

                  	//dump($obEntrance->id);

                    foreach ($entrance["Квартиры"] as $apartment) {
                       $this->delivery_date = $apartment["СрокСдачи"];
						$this->apartment_group = $apartment["ГруппаКвартир"];
				        $this->apartment_guid = $apartment["ГУИД_Квартиры"];
				        $this->apartment_number = $apartment["НомерКвартиры"];
				        $this->object_type = $apartment["ТипОбъекта"];
						$this->floor = $apartment["Этаж"];
						$this->apartment_number_on_floor = $apartment["КвартирыНаЛестничнойКлетке"];
				        $this->block_number = $apartment["НомерБлока"];
				        $this->section_position_number = $apartment["НомерПозицииСекции"];
				        $this->rooms = $apartment["КоличествоКомнат"];
				        $this->total_area = $apartment["ОбщаяПлощадь"];
				        $this->price_per_meter = $apartment["ЦенаЗаКвадрат"];
				        $this->current_price = $apartment["Стомость"];
				        $this->commission = $apartment["Комиссия"];
				        $this->is_increased_commission = $apartment["ПовышеннаяКомиссия"];
				        $this->is_promotional = $apartment["Акция"];
				        $this->discount = $apartment["РазмерСкидки"];
				        $this->is_free_decoration = $apartment["БесплатнаяОтделка"];
				        $this->floor_count = $apartment["КоличествоЭтажей"];
						$this->district = $apartment["Район"];
				        $this->description = $apartment["Описание"];
				        $this->state = $apartment["Состояние"];
				        $this->land_area = $apartment["ПлощадьУчастка"];
				        $this->house_state = $apartment["СтатусЗагородногоДома"];
				        $this->is_for_sale = false; // Уточнить
				        $this->project = $apartment["Проект"];
						$this->installment_price = $apartment["СтоимостьВРассрочку"];
				        $this->is_investment_product = $apartment["ИнвестиционныйПродукт"];
				        $this->queue = $apartment["Очередь"];
				        $this->living_area = $apartment["ЖилаяПлощадь"];
				        $this->building_guid = $apartment["ГУИД_Объект"];

						$this->makeApartment($obComplex->id, $obBuilding->id, $complex["ТипПомещения"]);

                      $processed++;
                      //dump($processed);
                      Console::moveCursorTo(1);
                      Console::stdout("$processed");
                    }
                  }
                }
              }
            }


            //Console::moveCursorTo(1);
            //Console::stdout("Processed: $processed items.\n");
            $this->doubleLogSuccess(bt('Apartment synchronization finish', 'ftp-log'), bt('Apartment synchronization finish', 'ftp-log'));
            $ftpPhoto = new FPhoto();
            $ftpPhoto->synchronizeWithDb();
        } catch (Exception $e) {
            Console::error($e->getMessage() . "\n");
            $this->doubleLogError(bt('Synchronization error', 'ftp-log'), bt('Synchronization error: ', 'ftp-log') . $e->getMessage() . ' --- ' . $e->getTraceAsString() . ' --- ' . $e->getLine());
        }
    }

    protected function  fillAttributes($data)
    {
        $attributes = $this->getProps();
        foreach ($attributes as $i => $attribute) {
             //dump($attribute->getName() . ': ' . obtain($i, $data, '----'));
            $this->{$attribute->getName()} = obtain($i, $data, null);
        }
    }

    protected function  fillAttributesNew($data)
    {
      //dd($data);
      try {
        $this->building_guid_DO_NOT_USE = null;
        $this->complex_name = $data["complex"]["НазваниеКомплекса"];
        $this->building_name = $data["build"]["Здание"];
        $this->delivery_date = $data["apartment"]["СрокСдачи"];
        $this->complex_position = $data["complex"]["МестоположениеКомплекса"];
        $this->apartment_group = $data["apartment"]["ГруппаКвартир"];
        $this->apartment_guid = $data["apartment"]["ГУИД_Квартиры"];
        $this->apartment_number = $data["apartment"]["НомерКвартиры"];
        $this->object_type = $data["apartment"]["ТипОбъекта"];
        $this->is_secondary_property = $data["complex"]["ВторичнаяНедвижимость"];
        $this->entrance = $data["entrance"]["НомерПодъезда"];
        $this->floor = $data["apartment"]["Этаж"];
        //$this->apartment_number_on_floor = $data["entrance"]["КоличествоПомещенийНаЭтажей"];
        $this->apartment_number_on_floor = $data["apartment"]["КвартирыНаЛестничнойКлетке"];
        $this->block_number = $data["apartment"]["НомерБлока"];
        $this->section_position_number = $data["apartment"]["НомерПозицииСекции"];
        $this->rooms = $data["apartment"]["КоличествоКомнат"];
        $this->total_area = $data["apartment"]["ОбщаяПлощадь"];
        $this->price_per_meter = $data["apartment"]["ЦенаЗаКвадрат"];
        $this->current_price = $data["apartment"]["Стомость"];
        $this->commission = $data["apartment"]["Комиссия"];
        $this->is_increased_commission = $data["apartment"]["ПовышеннаяКомиссия"];
        $this->is_promotional = $data["apartment"]["Акция"];
        $this->discount = $data["apartment"]["РазмерСкидки"];
        $this->is_free_decoration = $data["apartment"]["БесплатнаяОтделка"];
        $this->floor_count = $data["apartment"]["КоличествоЭтажей"];
        $this->city = $data["city"]["Город"];
        $this->district = $data["apartment"]["Район"];
        $this->description = $data["apartment"]["Описание"];
        $this->state = $data["apartment"]["Состояние"];
        $this->land_area = $data["apartment"]["ПлощадьУчастка"];
        $this->house_state = $data["apartment"]["СтатусЗагородногоДома"];
        $this->is_for_sale = false; // Уточнить
        $this->project = $data["apartment"]["Проект"];
        $this->complex_guid = $data["complex"]["ГУИД_ЖК"];
        $this->installment_price = $data["apartment"]["СтоимостьВРассрочку"];
        $this->is_investment_product = $data["apartment"]["ИнвестиционныйПродукт"];
        $this->queue = $data["apartment"]["Очередь"];
        $this->living_area = $data["apartment"]["ЖилаяПлощадь"];
        //$this->building_guid = $data["build"]["ГУИД_Здания"];
        $this->building_guid = $data["apartment"]["ГУИД_Объект"];
      } catch (Exception $e) {
        dump($e->getMessage());
      }
        //dump($this);
    }

    public function convertToRealModel()
    {
        if (!$this->city || !$this->complex_name || !$this->apartment_guid) {
            return;
        }
        $city = $this->makeCity();
        $complex = $this->makeComplex($city->id);
        $building = $this->makeBuilding($complex->id);
        $this->makeApartment($complex->id, $building->id);

    }

    public function convertToRealModelNew()
    {
        if (!$this->city || !$this->complex_name || !$this->apartment_guid) {
            return;
        }
        $city = $this->makeCity();
        $complex = $this->makeComplex($city->id);
        $building = $this->makeBuilding($complex->id);
        $entrance = $this->makeEntrance($building->id);
        $this->makeApartment($complex->id, $building->id);
    }

    private function makeEntrance($buildingId)
    {
        $entrance = ApartmentEntrance::find()->where(['building_id' => $buildingId, 'entrance' => $this->entrance])->one();
        $complexId = ApartmentComplex::find()->where(['guid' => $this->complex_guid])->one();
        if ($entrance === null) {
            $entrance = new ApartmentEntrance([
                'entrance' => $this->entrance,
                'building_id' => $buildingId,
                'building_complex_id' => $complexId->id,
            ]);

        }
        // update new fields
        $entrance->entrance = $this->entrance;
        $entrance->building_id = $buildingId;

        if ($entrance->save(false) === false) {
            $this->doubleLogError(bt('Apartment synchronization error', 'ftp-log'), bt('Apartment synchronization error. Building can not save:', 'ftp-log') . ' ' . $this->building_name);
        }
        //Console::moveCursorTo(6);
        //Console::stdout(' Подъезд: '.$entrance->entrance);

        return $entrance;
    }

    private function makeCity()
    {
        $city = City::find()->where(['label' => $this->city])->one();

        if ($city === null) {
            $city = new City([
                'label' => $this->city,
                'alias' => FtpHelper::generateCyrillicAlias($this->city),
            ]);
            if ($city->save(false) === false) {
                $this->doubleLogError(bt('Apartment synchronization error', 'ftp-log'), bt('Apartment synchronization error. City can not save:', 'ftp-log') . ' ' . $this->city);

                return false;
            }
        }

        return $city;
    }

    private function makeComplex($cityId, $type)
    {
        /*$subtype = Property::subtypeFromLabel($this->object_type);
        $type = Property::typeFromSubtype($subtype);*/

        $t = Property::typeFromLabel($type);

        $complex = ApartmentComplex::find()->where(['type' => $t, 'guid' => $this->complex_guid])->one();
        if ($complex === null) {
            $complex = new ApartmentComplex([
                'label' => $this->complex_name,
                'type' => $t,
                'guid' => $this->complex_guid,
                'alias' => FtpHelper::generateAlias($this->complex_name),
                'city_id' => $cityId,
                'lat' => '56.8497600',
                'lng' => '53.2044800',
            ]);
        }
        // update new fields
        $complex->guid = $this->complex_guid;
        $complex->type = $t;

        if ($complex->save(false) === false) {
            $this->doubleLogError(bt('Apartment synchronization error', 'ftp-log'), bt('Apartment synchronization error. Complex can not save:', 'ftp-log') . ' ' . $this->complex_name);
        }

        return $complex;
    }

    private function makeBuilding($complexId, $type)
    {
      $t = Property::buildingTypeFromLabel($type);
        $building = ApartmentBuilding::find()->where(['type' => $t, 'guid' => $this->building_guid])->one();
        if ($building === null) {
            $label = StringHelper::truncate($this->building_name, 127, '');
            $building = new ApartmentBuilding([
                'building_complex_id' => $complexId,
                'label' => $label,
                'guid' => $this->building_guid,
                'alias' => FtpHelper::generateAlias($label),
                'block_number' => $this->block_number,
                'floors' => (int)$this->floor_count,
                'position' => (int)$this->section_position_number,
                'type' => $t,
            ]);
        }
        // update new fields
        $building->building_complex_id = $complexId;
        $building->guid = $this->building_guid;
        $building->entrance = $this->entrance;
        $building->floors = (int)$this->floor_count;
        $building->block_number = $this->block_number;
        $building->type = $t;

        if ($building->save(false) === false) {
            $this->doubleLogError(bt('Apartment synchronization error', 'ftp-log'), bt('Apartment synchronization error. Building can not save:', 'ftp-log') . ' ' . $this->building_name);
        }

        return $building;
    }



    private function makeApartment($complexId, $buildingId, $ctype)
    {
        $subtype = Property::subtypeFromLabel($this->object_type);

        $type = Property::typeFromSubtype($subtype);

        $apartment = Apartment::find()
            ->where(['guid' => $this->apartment_guid])
            ->one() ?: new Apartment();

        $apartment->setAttributes([
            'type' => $type,
            'subtype' => $subtype,
            'guid' => $this->apartment_guid,
            'address' => $this->complex_position,
            'apartment_complex_id' => $complexId,
            'apartment_building_id' => $buildingId,
            'label' => $this->generateLabel(),
            'delivery_date' => $this->delivery_date,
            'entrance' => $this->entrance,
            'floor' => $this->floor,
            'rooms_number' => $this->rooms,
            'total_area' => (float)str_replace(',', '.', $this->total_area),
            'living_space' => (float)str_replace(',', '.', $this->living_area),
            'price_from' => floatval($this->current_price),
            'price_per_meter' => floatval($this->price_per_meter),
            'installment_price' => floatval($this->installment_price),
            'description_text' => $this->description,
            'published' => $this->convertState(),
            'is_secondary' => $this->isSecondary(),
            'apartment_number' => $this->apartment_number,
            'apartment_number_on_floor' => $this->apartment_number_on_floor,
            'discount' => (int)$this->discount,
            'is_promotional' => $this->isPromotional(),
            'status' => $this->convertStatus(),
            'deleted' => Apartment::NOT_DELETED,
        ]);

        if ($apartment->save(false) === false) {
            $this->doubleLogError(bt('Apartment synchronization error', 'ftp-log'), bt('Apartment synchronization error. Apartment can not save:', 'ftp-log') . ' ' . $this->apartment_guid);
        }
        //dump("apartment: ".$apartment->subtype."; complex: ".$apartment->apartment_complex_id);
    }

    protected function generateLabel()
    {
        $label = '';
        if ($this->object_type && $this->object_type != Property::SYNC_SUBTYPE_APARTMENT_VN) {
            $label .= $this->object_type;
        }
        if ($this->apartment_number) {
            $label .= ' №' . $this->apartment_number . ' ';
        }
        if ($this->rooms) {
            $label .= ' ' . t('{n, plural, =0{не задано} =1{# комната} one{# комната} few{# комнаты} many{# комнат} other{# комнаты}}', 'apartment', ['n' => intval($this->rooms)]);
        }
        $label = trim($label);
        if ($label == '') {
            $label = t('_Default label', 'apartment');
        }

        return $label;
    }

    protected function getDirectoryName()
    {
        return date('Y-m'); // return '2017-12';
        //return '/';
    }

    protected function getFileName()
    {
        return date('Y-m-d') . '-import.csv'; // return '2017-12-31-import.csv';
        //return 'new_import.json';
    }



    protected function getDirectoryNameNew()
    {
        //return date('Y-m'); // return '2017-12';
        return '/';
    }

    protected function getFileNameNew()
    {
        //return date('Y-m-d') . '-import.csv'; // return '2017-12-31-import.csv';
        return 'new_import.json';
    }



    public function getProps()
    {
        if ($this->props === null) {
            $ref = new ReflectionClass($this);
            $this->props = $ref->getProperties(ReflectionProperty::IS_PUBLIC);
        }

        return $this->props;
    }

    private function isSecondary()
    {
        return $this->is_secondary_property === self::YES_VAL;
    }

    private function convertState()
    {
        return $this->state == Property::SYNC_STATUS_AVAILABLE
            ? Property::STATUS_AVAILABLE
            : Property::STATUS_SOLD_OUT;
    }

    private function convertStatus()
    {
        switch ($this->state) {
            case Property::SYNC_STATUS_AVAILABLE:
                return CommonApartment::STATUS_AVAILABLE;
            case Property::SYNC_STATUS_PLEDGED:
            case Property::SYNC_STATUS_ORDERED:
            case Property::SYNC_STATUS_RESERVED:
                return CommonApartment::STATUS_RESERVED;
            case Property::SYNC_STATUS_SOLD_OUT:
                return CommonApartment::STATUS_SOLD;
            default:
                return CommonApartment::STATUS_UNDEFINED;
        }
    }

    private function isPromotional()
    {
        return $this->is_promotional == self::YES_VAL;
    }
}
