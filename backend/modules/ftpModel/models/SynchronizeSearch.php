<?php

namespace backend\modules\ftpModel\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SynchronizeSearch represents the model behind the search form about `Synchronize`.
 */
class SynchronizeSearch extends Synchronize
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'add', 'replaced', 'status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SynchronizeSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'add' => $this->add,
            'replaced' => $this->replaced,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
