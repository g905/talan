<?php

namespace backend\modules\ftpModel\models;

use Exception;
use yii\helpers\Console;
use yii\console\ExitCode;
use common\models\FpmFile;
use yii\helpers\ArrayHelper;
use common\models\Synchronize;
use common\models\EntityToFile;
use metalguardian\fileProcessor\helpers\FPM;
use common\models\Apartment as CommonApartment;
use backend\modules\apartment\models\Apartment;
use metalguardian\fileProcessor\components\FileTransfer;
use backend\modules\ftpModel\models\base\BaseFtpPhotoModel;
use backend\modules\ftpModel\models\base\FtpModelInterface;

/**
 * Class FPhoto
 *
 * @author art
 * @package backend\modules\ftpModel\models
 */
class FPhoto extends BaseFtpPhotoModel implements FtpModelInterface
{
    const FLOR_PLAN_PHOTO_TYPE = 2;

    public function synchronizeWithDb()
    {
        $deleted = 0;
        $processed = 0;

        try {
            $this->logSuccess(bt('Photo synchronization start', 'ftp-log'));

            $synchronize = new Synchronize();
            $synchronize->add = $processed;
            $synchronize->replaced = $deleted;
            $synchronize->status = Synchronize::EMPTY;
            $synchronize->save();

            $folderPath = $this->getFile();
            setAlias('@webroot', getAlias('@backend') . '/web');
            $directory = getAlias('@webroot') . '/uploads/ftp-files/' . $this->getFolderName();

            if ($folderPath === false) {
                $this->logError(bt('Photo synchronization finished, file to import was not found', 'ftp-log'));
                rmdir($directory);
                return false;
            }

            $images = array_diff(scandir($folderPath), ['.', '..']);

            $entityToFiles = FpmFile::find()
                ->select(['base_name', 'entity_to_file.file_id'])
                ->joinWith('entityToFiles', false)
                ->andWhere([
                'entity_to_file.entity_model_name' => 'Apartment',
                'entity_to_file.attribute' => [
                    CommonApartment::SAVE_ATTRIBUTE_PHOTOS,
                    CommonApartment::SAVE_ATTRIBUTE_FLOOR_PLAN_PHOTO
                ]
            ])->asArray()->all();

            $entityToFiles = ArrayHelper::map($entityToFiles, 'file_id', 'base_name');

            $delete = [];

            foreach ($images as $photoPath) {

                $search = array_search(explode('.', $photoPath)[0], $entityToFiles);

                if ($search) {
                    $delete[] = $search;
                }

                Console::stdout("Importing file: $photoPath\n");
                $apartmentName = explode('.', $photoPath)[0];
                Console::stdout("Apartment name: $apartmentName\n");

                if (!isset($apartmentName)) {
                    continue;
                }

                $apartmentData = explode('_', $apartmentName);
                $apartmentGuid = $apartmentData[0];
                $photoType = $apartmentData[1];
                $photoOrder = intval($apartmentData[2]);

                Console::stdout("Apartment GUID: $apartmentGuid\n");
                Console::stdout("File type: $photoType\n");
                Console::stdout("File order: $photoOrder\n");

                $existApartment = Apartment::find()
                    ->select(['id'])
                    ->where(['guid' => $apartmentGuid])
                    ->asArray()
                    ->one();

                if (!isset($existApartment)) {
                    $this->logError(bt('Apartment with guid', 'ftp-log') . ' ' . $apartmentGuid . ' ' . bt('does not exist', 'ftp-log'));
                    continue;
                }

                Console::stdout("This photo for apartment with ID: " . $existApartment['id'] . "\n");
                $attribute = CommonApartment::SAVE_ATTRIBUTE_PHOTOS;

                $fileId = (new FileTransfer())->saveSystemFile($directory . '/' . $photoPath, true);
                Console::stdout("File ID: $fileId\n");

                $etf = new EntityToFile();
                $etf->entity_model_name = 'Apartment';
                $etf->entity_model_id = $existApartment['id'];
                $etf->attribute = $attribute;
                $etf->file_id = $fileId;
                $etf->position = $photoOrder;
                $etf->save();

                $processed++;
            }

            if ($delete) {
                foreach ($delete as $item) {
                    FPM::deleteFile($item);
                    $deleted++;
                }

                EntityToFile::deleteAll(['file_id' => $delete]);
            }

            array_map('unlink', glob("$directory/*.*"));
            rmdir($directory);
            Console::stdout("Replaced $deleted items.\n");
            Console::stdout("Processed $processed items.\n");
            $this->doubleLogSuccess(
                bt('Photo synchronization finished (processed {number} items)', 'ftp-log', ['number' => $processed]),
                bt('Photo synchronization finished (processed {number} items)', 'ftp-log', ['number' => $processed])
            );

            if ($processed || $deleted) {
                $synchronize->status = Synchronize::SUCCSESS;
                $synchronize->save();
            }

        } catch (Exception $e) {
            $this->doubleLogError(
                bt('Synchronization error (processed {number} items)', 'ftp-log', ['number' => $processed]),
                bt('Synchronization error (processed {number} items): ', 'ftp-log', ['number' => $processed]) . $e->getMessage() . ' --- ' . $e->getTraceAsString() . ' --- ' . $e->getLine()
            );

            return ExitCode::UNSPECIFIED_ERROR;
        }

        return ExitCode::OK;
    }

    public function convertToRealModel()
    {
        return true;
    }

    protected function getDirectoryName()
    {
        return false;
    }

    protected function getFolderName()
    {
        return 'RoomsAdditional';
    }

    public function deletePhoto()
    {
        $entityToFiles = EntityToFile::find()->where([
            'entity_model_name' => 'Apartment',
            'attribute' => [
                CommonApartment::SAVE_ATTRIBUTE_PHOTOS,
                CommonApartment::SAVE_ATTRIBUTE_FLOOR_PLAN_PHOTO
            ]
        ])->asArray()->all();

        foreach ($entityToFiles as $item) {
            FPM::deleteFile($item['file_id']);
        }

        EntityToFile::deleteAll([
            'entity_model_name' => 'Apartment',
            'attribute' => [
                CommonApartment::SAVE_ATTRIBUTE_PHOTOS,
                CommonApartment::SAVE_ATTRIBUTE_FLOOR_PLAN_PHOTO
            ]
        ]);
    }
}
