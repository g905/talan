<?php

namespace backend\modules\ftpModel\models\base;

use Exception;
use yii2mod\ftp\FtpClient;
use common\models\FtpLog;
use backend\modules\ftpModel\controllers\FtpLogController;
use yii\helpers\Json;

use yii\httpclient\Client;
/**
 * Class BaseFtpModel
 *
 * @author art
 * @package backend\modules\ftpModel\models\base
 */
class BaseVacancyModel
{

    //public $baseUrl = 'https://api.hh.ru/';
    public $params = 'employer_id=64717&per_page=100';


    public function getVacancies()
    {
        try {
            $client = new Client([
                'baseUrl' => 'https://api.hh.ru/',
                'responseConfig' => [
                    //'format' => Client::FORMAT_JSON
                ],
            ]);
            $response = $client->createRequest()
                ->setMethod('get')
                ->addHeaders(['user-agent' => 'My User Agent'])
                ->setUrl('vacancies' . '?' . $this->params)
                //->setData(['name' => 'John Doe', 'email' => 'johndoe@domain.com'])
                ->send();
            if ($response->isOk) {
                $data = json_decode($response->content, 'true');
                //dd($data);
                return $data;
            } else {
                //return $response;
            }
        } catch (Exception $e) {
            $this->doubleLogError(
                bt('Headhunter Synchronization error', 'hh-log'),
                bt('Headhunter Synchronization error.', 'hh-log') . ' ' . $e->getMessage()
            );

            return false;
        }
    }

    public function getVacancyDetail($vacancy_id)
    {
        try {
            $client = new Client([
                'baseUrl' => 'https://api.hh.ru/',
                'responseConfig' => [
                    //'format' => Client::FORMAT_JSON
                ],
            ]);
            //dump($vacancy_id);
            $response = $client->createRequest()
                ->setMethod('get')
                ->addHeaders(['user-agent' => 'My User Agent'])
                ->setUrl('vacancies/' . $vacancy_id)
                //->setData(['name' => 'John Doe', 'email' => 'johndoe@domain.com'])
                ->send();
            if ($response->isOk) {
                $data = json_decode($response->content, 'true');
                //dd($data);
                return $data;
            } else {
                return $response;
            }
        } catch (Exception $e) {
            $this->doubleLogError(
                bt('Headhunter Synchronization error', 'hh-log'),
                bt('Headhunter Synchronization error.', 'hh-log') . ' ' . $e->getMessage()
            );

            return false;
        }
    }

    public function logSuccess($message)
    {
        FtpLogController::addLogMessage(FtpLog::MESSAGE_TYPE_SUCCESS, $message);
    }

    public function logError($message)
    {
        FtpLogController::addLogMessage(FtpLog::MESSAGE_TYPE_ERROR, $message);
    }

    public function doubleLogSuccess($global, $local)
    {
        FtpLogController::addDoubleLogMessage(FtpLog::MESSAGE_TYPE_SUCCESS, $global, $local);
    }

    public function doubleLogError($global, $local)
    {
        FtpLogController::addDoubleLogMessage(FtpLog::MESSAGE_TYPE_ERROR, $global, $local);
    }
}
