<?php

namespace backend\modules\ftpModel\models\base;

use Exception;
use yii2mod\ftp\FtpClient;
use common\models\FtpLog;
use backend\modules\ftpModel\controllers\FtpLogController;

/**
 * Class BaseFtpModel
 *
 * @author art
 * @package backend\modules\ftpModel\models\base
 */
class BaseFtpModel
{
    protected function getDirectoryName()
    {
        return '';
    }

    protected function getFileName()
    {
        return '';
    }

    public function getFile()
    {
        try {
            $ftp = new FtpClient();
            $ftp->connect(FtpConfig::FTP_URL, FtpConfig::FTP_USE_SSL, FtpConfig::FTP_PORT);
            $ftp->login(FtpConfig::FTP_LOGIN, FtpConfig::FTP_PASSWORD);
            $ftp->pasv(true);
            $dirName = $this->getDirectoryName();
            if ($dirName !== false) {
                $ftp->chdir($this->getDirectoryName());
            }
            $fileName = $this->getFileName();
            if ($ftp->size($fileName) == '-1') {
                $this->logError(bt('File', 'ftp-log') . ' ' . $fileName . ' ' . bt('not found on server. Trying get last file in folder', 'ftp-log'));
                //get file list
                $list = $ftp->nlist();
                if (count($list) == 0) {
                    $this->logError(bt('Folder', 'ftp-log') . ' ' . $this->getDirectoryName() . ' ' . bt('is empty', 'ftp-log'));
                }
                $fileName = $list[count($list) - 1];
                $fileName = str_replace('./', '', $fileName);
            }
            setAlias('@webroot', getAlias('@backend') . '/web');
            $filePath = getAlias('@webroot') . '/uploads/ftp-files';
            if (!file_exists($filePath)) {
                mkdir($filePath, 0777, true);
            }
            $filePath = $filePath . '/' . $fileName;
            $ftp->get($filePath, $fileName, FTP_BINARY);
            $this->logSuccess(bt('Using last file in folder ({file})', 'ftp-log', ['file' => $fileName]));

            return $filePath;
        } catch (Exception $e) {
            $this->doubleLogError(
                bt('Synchronization error', 'ftp-log'),
                bt('Synchronization error.', 'ftp-log') . ' ' . $e->getMessage()
            );

            return false;
        }
    }

    public function logSuccess($message)
    {
        FtpLogController::addLogMessage(FtpLog::MESSAGE_TYPE_SUCCESS, $message);
    }

    public function logError($message)
    {
        FtpLogController::addLogMessage(FtpLog::MESSAGE_TYPE_ERROR, $message);
    }

    public function doubleLogSuccess($global, $local)
    {
        FtpLogController::addDoubleLogMessage(FtpLog::MESSAGE_TYPE_SUCCESS, $global, $local);
    }

    public function doubleLogError($global, $local)
    {
        FtpLogController::addDoubleLogMessage(FtpLog::MESSAGE_TYPE_ERROR, $global, $local);
    }
}
