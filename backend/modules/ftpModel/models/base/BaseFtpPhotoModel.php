<?php

namespace backend\modules\ftpModel\models\base;

use DateTime;
use Exception;
use common\models\FtpLog;
use yii2mod\ftp\FtpClient;
use common\models\Synchronize;
use backend\modules\ftpModel\controllers\FtpLogController;

/**
 * Class BaseFtpPhotoModel
 *
 * @author sanja
 * @package backend\modules\ftpModel\models\base
 */
class BaseFtpPhotoModel
{
    protected function getDirectoryName()
    {
        return false;
        //return '';
    }

    protected function getFolderName()
    {
        return 'RoomsAdditional';
    }

    public function getFile()
    {
        try {
            $ftp = new FtpClient();
            $ftp->connect(FtpConfig::FTP_URL, FtpConfig::FTP_USE_SSL, FtpConfig::FTP_PORT, 3800);
            $ftp->login(FtpConfig::FTP_LOGIN, FtpConfig::FTP_PASSWORD);
            $ftp->pasv(true);
            $dirName = $this->getDirectoryName();
            if ($dirName !== false) {
                $ftp->chdir($this->getDirectoryName());
            }
            $folderName = $this->getFolderName();
            // $folderName = 'TestRoomsAdditional';
            setAlias('@webroot', getAlias('@backend') . '/web');

            $filePath = getAlias('@webroot') . '/uploads/ftp-files';

            $folderPath = $filePath . '/' . $folderName;

            if (!file_exists($folderPath)) {
                mkdir($folderPath, 0777, true);
            }

            $images = $ftp->scanDir($folderName);
//var_dump($images);

            foreach ($images as $image) {
                //if (1549611710 < $this->getDateFromName($image)) {
                //  if (1549611710 < $this->getDateFromName($image)) {
                try {
                    $ftp->get($folderPath . '/' . $this->getOriginalFileName($image),
                        $folderName . '/' . $image['name'], FTP_BINARY);
                    echo $image['name'] . "\n";
                } catch (Exception $e) {
                    echo $e->getMessage() . "\n";
                    echo $e->getCode() . "\n";

                    echo $image['name'] . "\n";
                }
                //   }
            }

            //  die;

            $this->logSuccess(bt('Using last file in folder ({file})', 'ftp-log', ['file' => $folderName]));

            return $folderPath;
        } catch (Exception $e) {
            $this->doubleLogError(
                bt('Synchronization error', 'ftp-log'),
                bt('Synchronization error.', 'ftp-log') . ' ' . $e->getMessage()
            );

            return false;
        }
    }

    public function logSuccess($message)
    {
        FtpLogController::addLogMessage(FtpLog::MESSAGE_TYPE_SUCCESS, $message);
    }

    public function logError($message)
    {
        FtpLogController::addLogMessage(FtpLog::MESSAGE_TYPE_ERROR, $message);
    }

    public function doubleLogSuccess($global, $local)
    {
        FtpLogController::addDoubleLogMessage(FtpLog::MESSAGE_TYPE_SUCCESS, $global, $local);
    }

    public function doubleLogError($global, $local)
    {
        FtpLogController::addDoubleLogMessage(FtpLog::MESSAGE_TYPE_ERROR, $global, $local);
    }

    public function getDateFromName($name)
    {


        //dump($name["name"]);

        $date = explode('_', $name['name'])[0];

        return (int)DateTime::createFromFormat('dmYHis', $date)->format('U');
    }

    public function getOriginalFileName($image)
    {
        $parsedName = explode('_', $image['name']);

        array_shift($parsedName);

        return implode('_', $parsedName);
    }
}
