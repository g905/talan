<?php
namespace backend\modules\ftpModel\models\base;
/**
 * Created by PhpStorm.
 * User: art
 * Date: 17.08.2017
 * Time: 13:51
 */

interface FtpModelInterface
{
    public function synchronizeWithDb();
    public function convertToRealModel();
}