<?php

namespace backend\modules\ftpModel\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%ftp_log}}".
 *
 * @property integer $id
 * @property integer $type
 * @property string $message
 * @property integer $is_show
 * @property integer $is_shown
 * @property integer $created_at
 * @property integer $updated_at
 */
class FtpLog extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ftp_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'is_show', 'is_shown'], 'integer'],
            [['message'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/ftp-log', 'ID'),
            'type' => Yii::t('back/ftp-log', 'message type'),
            'message' => Yii::t('back/ftp-log', 'message text'),
            'is_show' => Yii::t('back/ftp-log', 'Is Show'),
            'is_shown' => Yii::t('back/ftp-log', 'Is Shown'),
            'created_at' => Yii::t('back/ftp-log', 'Created At'),
            'updated_at' => Yii::t('back/ftp-log', 'Updated At'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/ftp-lo', 'Onebox Log');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'type',
                    // 'message:ntext',
                    'is_show:boolean',
                    'is_shown:boolean',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'type',
                    [
                        'attribute' => 'message',
                        'format' => 'html',
                    ],
                    'is_show:boolean',
                    'is_shown:boolean',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new FtpLogSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'type' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'message' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'message',
                ]
            ],
            'is_show' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'is_shown' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }


}
