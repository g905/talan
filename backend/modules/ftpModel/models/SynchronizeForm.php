<?php

namespace backend\modules\ftpModel\models;

use metalguardian\formBuilder\ActiveFormBuilder;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: art
 * Date: 20.08.2017
 * Time: 0:05
 */

class SynchronizeForm extends Model
{
    const SYNCHRONIZE_TYPE_APARTMENT = 1;
    const SYNCHRONIZE_TYPE_PHOTO = 2;
    const SYNCHRONIZE_TYPE_APARTMENT_NEW = 3;


    public $synchronizeType;

    public static function getSynchronizeTypeDictionary()
    {
        return [
            static::SYNCHRONIZE_TYPE_APARTMENT =>  \Yii::t('back/synchronize-form', 'Apartment'),
            static::SYNCHRONIZE_TYPE_PHOTO =>  \Yii::t('back/synchronize-form', 'Photo'),
            static::SYNCHRONIZE_TYPE_APARTMENT_NEW =>  \Yii::t('back/synchronize-form', 'Apartment_new_json'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['synchronizeType'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'synchronizeType' => \Yii::t('back/synchronize-form', 'synchronize type'),
        ];
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/synchronize-form', '1C synchronization');
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        return [
            'form-set' => [
                \Yii::t('back/synchronize-form', 'Synchronization') => [

                    'synchronizeType' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => self::getSynchronizeTypeDictionary()
                    ],

                ],

            ],
        ];
    }
}
