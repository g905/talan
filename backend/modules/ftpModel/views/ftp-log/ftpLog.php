<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 29.06.2017
 * Time: 1:22
 */

use backend\modules\importExport\config\ImportExportConfig;
use backend\modules\importExport\models\ImportExportMessage;
use common\models\FtpLog;
use common\models\OneboxLog;

echo \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($model, $index, $widget, $grid){

        if($model->type == FtpLog::MESSAGE_TYPE_SUCCESS){
            return ['style' => 'background-color: #90C695'];
        }elseif (($model->type == FtpLog::MESSAGE_TYPE_ERROR)){
            return ['style' => 'background-color: #F1A9A0'];
        }
    },
    'columns' => [
        [
            'attribute'=>'id',
            'label'=>  \Yii::t('back/ftp-log', 'id'),
        ],
        [
            'attribute'=>'created_at',
            'format' =>  ['date', 'YYYY-MM-dd HH:mm:ss'],
            'label'=>  \Yii::t('back/ftp-log', 'date'),
        ],
        [
            'attribute'=>'message',
            'label'=> \Yii::t('back/ftp-log', 'message'),
        ],
    ]
]);