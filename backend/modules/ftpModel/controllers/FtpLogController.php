<?php

namespace backend\modules\ftpModel\controllers;

use yii\data\ActiveDataProvider;
use backend\components\BackendController;
use backend\modules\ftpModel\models\FtpLog;

/**
 * Class FtpLogController
 *
 * @package backend\modules\ftpModel\controllers
 */
class FtpLogController extends BackendController
{
    public function getModelClass()
    {
        return FtpLog::class;
    }

    public function actionLog()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => FtpLog::find()->where(['is_show' => 0])->orderBy(['id' => SORT_DESC]),
            'pagination' => ['pageSize' => 50],
        ]);

        return $this->render('ftpLog', ['dataProvider' => $dataProvider]);
    }

    public static function addLogMessage($type, $message, $isShow = 0)
    {
        //add global message
        $newMessage = new FtpLog();
        $newMessage->type = $type;
        $newMessage->message = $message;
        $newMessage->is_show = $isShow;
        $newMessage->is_shown = 0;
        $newMessage->save(false);
    }

    public static function addDoubleLogMessage($type, $messageGlobal, $messageLocal)
    {
        //add global message
        $newMessage = new FtpLog();
        $newMessage->type = $type;
        $newMessage->message = $messageGlobal;
        $newMessage->is_show = 1;
        $newMessage->is_shown = 0;
        $newMessage->save(false);

        //add local message
        $newMessage = new FtpLog();
        $newMessage->type = $type;
        $newMessage->message = $messageLocal;
        $newMessage->is_show = 0;
        $newMessage->is_shown = 0;
        $newMessage->save(false);
    }
}
