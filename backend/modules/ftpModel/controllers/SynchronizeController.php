<?php

namespace backend\modules\ftpModel\controllers;

use backend\components\BackendController;
use backend\modules\ftpModel\models\Synchronize;

/**
 * SynchronizeController implements the CRUD actions for Synchronize model.
 */
class SynchronizeController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Synchronize::className();
    }
}
