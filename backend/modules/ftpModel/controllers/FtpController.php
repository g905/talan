<?php

namespace backend\modules\ftpModel\controllers;

use backend\modules\ftpModel\models\FApartment;
use backend\modules\ftpModel\models\FtpLog;
use backend\modules\ftpModel\models\SynchronizeForm;
use vova07\console\ConsoleRunner;
use yii\base\ErrorException;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Created by PhpStorm.
 * User: art
 * Date: 17.08.2017
 * Time: 13:36
 */
class FtpController extends Controller
{
    public function actionTest()
    {
//        $login = 'talan';
//        $password = '5BUvE08TbZBNvzKS';
//        $ftp = new \yii2mod\ftp\FtpClient();
//        $host = 'ftp.shop-loyalty.ru';
//        $ftp->connect($host,false, 35000);
//        $ftp->login($login, $password);
//        $ftp->pasv(true);
//
//        try{
//            $ftp->chdir('2017-12');
//            $ftp->get('test.csv','2017-12-31-import.csv', FTP_BINARY);
//        }
//        catch (ErrorException $e){
//            \yii\helpers\VarDumper::dump($e->getMessage(),10,true);
//            die;
//        }
//
//        $items = $ftp->scanDir();
//        \yii\helpers\VarDumper::dump($items,10,true);
//        die;

        $ftpApartment = new FApartment();
        $file =  $ftpApartment->synchronizeWithDb();
        \yii\helpers\VarDumper::dump($file,10,true);
        die;


    }

    public function actionIndex()
    {
        $model = new SynchronizeForm();

        if ($model->load(\Yii::$app->request->post())) {

            $command = '';
            $target  = '';
            switch ($model->synchronizeType){
                case SynchronizeForm::SYNCHRONIZE_TYPE_APARTMENT:
                    $command = 'ftp/synchronize-apartment';
                    $target = 'Apartment';
                    break;
                case SynchronizeForm::SYNCHRONIZE_TYPE_PHOTO:
                    $command = 'ftp/synchronize-photo';
                    $target = 'Photo';
                    break;
                case SynchronizeForm::SYNCHRONIZE_TYPE_APARTMENT_NEW:
                    $command = 'ftp/synchronize-apartment-new';
                    $target = 'Apartment';
                    break;
            }

            \Yii::$app->db->createCommand('SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;')->execute();
            $consoleRunner = new ConsoleRunner(['file' => \Yii::getAlias('@app') . '/../yii']);
            $consoleRunner->run($command);

            FtpLogController::addLogMessage(
                \common\models\FtpLog::MESSAGE_TYPE_SUCCESS,
                \Yii::t('back/ftp-log',
                    $target.' synchronization start. You will see massage when it finish (refresh the page)'),
                true);

            return $this->redirect(Url::toRoute('ftp-log/log'));
        }

        return $this->render('@backend/views/templates/create', [
            'model' => $model,
        ]);
    }


}
