<?php

namespace backend\modules\ftpModel;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\ftpModel\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
