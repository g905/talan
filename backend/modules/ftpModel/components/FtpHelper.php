<?php

namespace backend\modules\ftpModel\components;

use yii\helpers\Inflector;

/**
 * Class FtpHelper
 *
 * @author art
 * @package backend\modules\ftpModel\components
 */
class FtpHelper
{
    public static function generateAlias($string)
    {
        $alias = strtolower(Inflector::transliterate($string));
        $alias = trim($alias);
        $alias = str_replace(' ', '-', $alias); // Replaces all spaces with hyphens.
        $alias = preg_replace('/[^A-Za-z0-9\-]/', '', $alias); // Removes special chars.

        return preg_replace('/-+/', '-', $alias); // Replaces multiple hyphens with single one.
    }

    public static function generateCyrillicAlias($string)
    {
        $alias = mb_strtolower($string);
        $alias = trim($alias);
        $alias = str_replace(' ', '-', $alias); // Replaces all spaces with hyphens.

        return preg_replace('/-+/', '-', $alias); // Replaces multiple hyphens with single one.
    }

    public static function isNullOrEmpty($val)
    {
        if ((!isset($val)) || ($val == '')) {
            return true;
        }
        return false;
    }
}
