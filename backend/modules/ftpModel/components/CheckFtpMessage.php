<?php

/*
 *
 */

namespace backend\modules\ftpModel\components;


use backend\modules\ftpModel\models\FtpLog;
use common\models\FtpLog as CommonFtpLog;

class CheckFtpMessage extends \yii\base\Component{

    public function init() {

        //find message
        $newMessage = FtpLog::find()
            ->where([
                'is_show' => 1,
                'is_shown' => 0,
            ])
            ->orderBy([
                'id' => SORT_ASC
            ])
            ->one();

        if ($newMessage){
            \Yii::$app->session->setFlash(CommonFtpLog::getTypeDictionary()[$newMessage->type], $newMessage->message);
            $newMessage->updateAttributes([
                'is_shown' => 1
            ]);
        }

        parent::init();
    }

}
