<?php

namespace backend\modules\partner\controllers;

use backend\components\BackendController;
use backend\modules\partner\models\PartnerPage;

/**
 * PartnerPageController implements the CRUD actions for PartnerPage model.
 */
class PartnerPageController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PartnerPage::className();
    }
}
