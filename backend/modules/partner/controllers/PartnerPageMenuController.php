<?php

namespace backend\modules\partner\controllers;

use backend\components\BackendController;
use backend\modules\partner\models\PartnerPageMenu;

/**
 * PartnerPageMenuController implements the CRUD actions for PartnerPageMenu model.
 */
class PartnerPageMenuController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PartnerPageMenu::className();
    }
}
