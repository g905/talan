<?php

namespace backend\modules\partner;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\partner\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
