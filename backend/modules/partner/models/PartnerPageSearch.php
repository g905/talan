<?php

namespace backend\modules\partner\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PartnerPageSearch represents the model behind the search form about `PartnerPage`.
 */
class PartnerPageSearch extends PartnerPage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'manager_id'], 'integer'],
            [['label', 'published', 'form_bottom_title', 'form_bottom_description', 'form_onsubmit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PartnerPageSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'manager_id' => $this->manager_id,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'published', $this->published])
            ->andFilterWhere(['like', 'form_bottom_title', $this->form_bottom_title])
            ->andFilterWhere(['like', 'form_bottom_description', $this->form_bottom_description])
            ->andFilterWhere(['like', 'form_onsubmit', $this->form_onsubmit]);

        return $dataProvider;
    }
}
