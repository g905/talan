<?php

namespace backend\modules\partner\models;

use backend\helpers\StdInput;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\base\NotSupportedException;

/**
 * This is the model class for table "{{%partner_page_card}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $content
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $link_text
 * @property string $link_url
 * @property integer $partner_page_id
 *
 * @property PartnerPage $partnerPage
 */
class PartnerPageCard extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_page_card}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'content', 'link_url', 'link_text', 'color'], 'required'],
            [['content'], 'string', 'max' => 1000],
            [['link_url'], 'string', 'max' => 2000],
            [['link_url'], 'url'],
            [['link_text'], 'string', 'max' => 40],
            [['position', 'partner_page_id'], 'integer'],
            [['label', 'link_text', 'color'], 'string', 'max' => 255],
            [['position'], 'default', 'value' => 0],
            [['partner_page_id'], 'exist', 'targetClass' => \common\models\PartnerPage::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/PartnerPageCard', 'ID'),
            'label' => Yii::t('back/PartnerPageCard', 'Label'),
            'content' => Yii::t('back/PartnerPageCard', 'Content'),
            'position' => Yii::t('back/PartnerPageCard', 'Position'),
            'created_at' => Yii::t('back/PartnerPageCard', 'Created At'),
            'updated_at' => Yii::t('back/PartnerPageCard', 'Updated At'),
            'link_text' => Yii::t('back/PartnerPageCard', 'Link text'),
            'link_url' => Yii::t('back/PartnerPageCard', 'Link url'),
            'partner_page_id' => Yii::t('back/PartnerPageCard', 'Partner page'),
            'color' => Yii::t('back/PartnerPageCard', 'Color'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerPage()
    {
        return $this->hasOne(PartnerPage::class, ['id' => 'partner_page_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/PartnerPageCard', 'Partner Page Card');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    // 'content:ntext',
                    'position',
                    'link_text:url',
                    'link_url:url',
                    'partner_page_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'position',
                    'link_text:url',
                    'link_url:url',
                    'partner_page_id',
                ];
            break;
        }

        return [];
    }

    /**
     * @return void|\yii\db\ActiveRecord
     * @throws NotSupportedException
     */
    public function getSearchModel()
    {
        throw new NotSupportedException();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'link_text' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'link_url' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'color' => StdInput::colorSelect(),
        ];

        return $config;
    }


}
