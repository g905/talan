<?php

namespace backend\modules\partner\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\base\NotSupportedException;

/**
 * This is the model class for table "{{%partner_page_menu}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $partner_page_id
 * @property string $link
 * @property integer $show_right
 *
 * @property PartnerPage $partnerPage
 */
class PartnerPageMenu extends ActiveRecord implements BackendModel
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->published = 1;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_page_menu}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'link'], 'required'],
            [['position', 'partner_page_id', 'published', 'show_right'], 'integer'],
            [['link'], 'string'],
            [['label'], 'string', 'max' => 255],
            [['show_right'], 'default', 'value' => 1],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['partner_page_id'], 'exist', 'targetClass' => \common\models\PartnerPage::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/PartnerPageMenu', 'ID'),
            'label' => Yii::t('back/PartnerPageMenu', 'Label'),
            'published' => Yii::t('back/PartnerPageMenu', 'Published'),
            'position' => Yii::t('back/PartnerPageMenu', 'Position'),
            'created_at' => Yii::t('back/PartnerPageMenu', 'Created At'),
            'updated_at' => Yii::t('back/PartnerPageMenu', 'Updated At'),
            'partner_page_id' => Yii::t('back/PartnerPageMenu', 'Partner page'),
            'link' => Yii::t('back/PartnerPageMenu', 'Link'),
            'show_right' => Yii::t('back/PartnerPageMenu', 'Show right'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerPage()
    {
        return $this->hasOne(PartnerPage::class, ['id' => 'partner_page_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/PartnerPageMenu', 'Partner Page Menu');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    'published:boolean',
                    'position',
                    'partner_page_id',
                    'link:url',
                    'show_right',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    'published:boolean',
                    'position',
                    'partner_page_id',
                    'link:url',
                    'show_right',
                ];
            break;
        }

        return [];
    }

    /**
     * @return void|\yii\db\ActiveRecord
     * @throws NotSupportedException
     */
    public function getSearchModel()
    {
        throw new NotSupportedException();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'link' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'show_right' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }


}
