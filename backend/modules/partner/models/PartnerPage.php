<?php

namespace backend\modules\partner\models;

use backend\components\StylingActionColumn;
use backend\modules\contacts\models\City;
use backend\modules\contacts\models\Manager;
use kartik\grid\GridView;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%partner_page}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $city_id
 * @property integer $manager_id
 * @property string $form_bottom_title
 * @property string $form_bottom_description
 * @property string $form_onsubmit
 *
 * @property City $city
 * @property Manager $manager
 */
class PartnerPage extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'city_id', 'form_bottom_title'], 'required'],
            [['city_id'], 'integer'],
            [['form_bottom_description'], 'string', 'max' => 500],
            [['label', 'form_bottom_title', 'form_onsubmit'], 'string', 'max' => 255],
            [['published'], 'string', 'max' => 1],
            [['published'], 'default', 'value' => 1],
            [['city_id'], 'exist', 'targetClass' => \common\models\City::class, 'targetAttribute' => 'id'],
            [['manager_id'], 'exist', 'targetClass' => \common\models\Manager::class, 'targetAttribute' => 'id'],
            [['city_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/PartnerPage', 'ID'),
            'label' => Yii::t('back/PartnerPage', 'Label'),
            'published' => Yii::t('back/PartnerPage', 'Published'),
            'created_at' => Yii::t('back/PartnerPage', 'Created At'),
            'updated_at' => Yii::t('back/PartnerPage', 'Updated At'),
            'city_id' => Yii::t('back/PartnerPage', 'City'),
            'manager_id' => Yii::t('back/PartnerPage', 'Manager'),
            'form_bottom_title' => Yii::t('back/PartnerPage', 'Forn title'),
            'form_bottom_description' => Yii::t('back/PartnerPage', 'Form description'),
            'form_onsubmit' => Yii::t('back/PartnerPage', 'Form onsubmit'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::class,
                'defaultFieldForTitle' => 'label',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(PartnerPageMenu::class, ['partner_page_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getCards()
    {
        if ($this->isNewRecord && Yii::$app->request->isGet) {
            $this->loadDataForNewRecord();
        }

        return $this->hasMany(PartnerPageCard::class, ['partner_page_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/PartnerPage', 'Partner Page');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    [
                        'attribute' => 'manager_id',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            return $model->manager->fio ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => Manager::getItems('id', 'fio'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any manager')],
                    ],
                    'published:boolean',
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                return StylingActionColumn::createPreviewUrl($url, $model, 'partners');
                            }
                        ]
                    ]
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    'published:boolean',
                    'city_id',
                    'manager_id',
                    'form_bottom_title',
                    [
                        'attribute' => 'form_bottom_description',
                        'format' => 'html',
                    ],
                    'form_onsubmit',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PartnerPageSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        if ($this->isNewRecord) {
            $this->loadPresetData();
        }

        $config = [
            'form-set'=> [
                Yii::t('back/PartnerPage', 'Main') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                ],
                Yii::t('back/PartnerPage', 'Cards') => [
                    $this->getRelatedFormConfig()['cards']
                ],
                Yii::t('back/PartnerPage', 'Form') => [
                    'manager_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\Manager::getItems('id', 'fio'),
                        'options' => [
                            'prompt' => '--',
                        ],
                    ],
                    'form_bottom_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'form_bottom_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'options' => [
                            'maxlength' => true,
                        ]
                    ],
                    'form_onsubmit' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                ],
                Yii::t('back/PartnerPage', 'Menu') => [
                    $this->getRelatedFormConfig()['menus']
                ],
            ],
        ];

        return $config;
    }

    /**
     * @return array
     */
    public function getRelatedFormConfig()
    {
        return [
            'cards' => [
                'relation' => 'cards',
            ],
            'menus' => [
                'relation' => 'menus',
            ]
        ];
    }

    /**
     * @return void
     */
    protected function loadPresetData()
    {
        $this->label = 'Партнерам';
        $this->form_bottom_title = 'Наша компания всегда открыта к сотрудничеству';
        $this->form_bottom_description = 'Мы поможем подобрать то, что вам подойдет лучше всего. Оставьте ваши данные и наш специалист перезвонит и проконсультирует по любому вопросу.';
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    protected function loadDataForNewRecord()
    {
        $jobItems = [];
        $data = require_once(__DIR__ . '/../defaultData/partnerPageCardData.php');
        foreach ($data as $datum) {
            $jobItems[] = new PartnerPageCard($datum);
        }

        $this->populateRelation('cards', $jobItems);
    }
}
