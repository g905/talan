<?php

namespace backend\modules\agents;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\agents\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
