<?php

namespace backend\modules\agents\models;

use backend\modules\contacts\models\Manager;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\base\NotSupportedException;

/**
 * This is the model class for table "{{%agents_member}}".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $manager_id
 * @property integer $agents_id
 *
 * @property Manager $manager
 * @property Agents $agents
 */
class AgentsMembers extends ActiveRecord implements BackendModel
{

    const SAVE_ATTRIBUTE_IMAGE = 'AgentsMembersImage';
    public $image;
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = Yii::$app->security->generateRandomString();
        }
    }



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agents_members}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['fio', 'description', 'phone', 'youtube_link'], 'string'],
            [['phone_shows', 'agents_id', 'published', 'deals_amount', 'service_evaluation'], 'integer'],
            [['agents_id'], 'exist', 'targetClass' => \common\models\Agents::class, 'targetAttribute' => 'id'],

            [['sign'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/AgentsMember', 'ID'),
            'position' => Yii::t('back/AgentsMember', 'Position'),
            'created_at' => Yii::t('back/AgentsMember', 'Created At'),
            'updated_at' => Yii::t('back/AgentsMember', 'Updated At'),
            'agents_id' => Yii::t('back/AgentsMember', 'Agents'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgents()
    {
        return $this->hasOne(Agents::class, ['id' => 'agents_id']);
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'agents_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'agents_id',
                ];
                break;
        }

        return [];
    }

    /**
     * @return void|\yii\db\ActiveRecord
     * @throws NotSupportedException
     */
    public function getSearchModel()
    {
        throw new NotSupportedException();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'fio' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                'options' => [
                    'maxlength' => true,
                ]
            ],
            'phone' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ]
            ],
            'deals_amount' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'service_evaluation' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'image' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'image',
                    'saveAttribute' => $this::SAVE_ATTRIBUTE_IMAGE,
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ]),
            ],
            'youtube_link' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }

    public function getTitle()
    {
        return \Yii::t('back/AgentsMember', 'Agents Member');
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
