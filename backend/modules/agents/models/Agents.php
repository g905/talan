<?php

namespace backend\modules\agents\models;

use backend\components\StylingActionColumn;
use backend\modules\contacts\models\City;
//use common\models\AgentsMembers;
use backend\modules\agents\models\AgentsMembers;
use kartik\grid\GridView;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\base\EntityToFile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "agents".
 *
 * @property int $id
 * @property string $label Label
 * @property string $description Description
 * @property int $published Published
 * @property int $created_at Created At
 * @property int $updated_at Updated At
 * @property int $city_id City
 * @property string $agents_title Agents title
 * @property string $tab_label Tab label
 *
 * @property City $city
 */
class Agents extends ActiveRecord implements BackendModel
{
	    public $backgroundImage;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%agents}}';
    }

    public $sign;

    /**
     * @throws \yii\base\Exception
     */
    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @return void
     */
    protected function loadInitialValues()
    {
        $this->agents_title = 'Команда в филиалах';
//        $this->tab_label = 'Управляющая компания';
        $this->label = 'Команда';
        $this->description = 'Мы компания заслужившая за годы работы репутацию надежного застройщика, и за эту репутацию мы готовы отвечать лично. Каждый сотрудник компании несет ответственность за качество работы.';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['description', 'youtube_link'], 'string'],
            [['published', 'created_at', 'updated_at', 'city_id'], 'integer'],
            [['label', 'agents_title', 'tab_label'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'description' => 'Description',
            'published' => 'Published',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'city_id' => 'City ID',
            'agents_title' => 'Agents Title',
            'youtube_link' => 'Youtube video link',
            'tab_label' => 'Tab Label',
        ];
    }


    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::class,
                'defaultFieldForTitle' => 'label',
            ],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentsMembers()
    {
        return $this->hasMany(AgentsMembers::class, ['agents_id' => 'id']);
    }

    public function getTitle()
    {
        return \Yii::t('back/Agents', 'Agents');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    'published:boolean',
                    ['class' => StylingActionColumn::class],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'city_id',
                    'agents_title',
//                    'tab_label',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new AgentsSearch();
    }    

    /**
    * @return array
    */
    public function getFormConfig()
    {
        if ($this->isNewRecord) {
            $this->loadInitialValues();
        }

        $config = [
            'form-set' => [
                Yii::t('back/Agents', 'Main') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'options' => [
                            'maxlength' => true,
                        ]
                    ],
                    'backgroundImage' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'backgroundImage',
                            'saveAttribute' => \common\models\Agents::SAVE_ATTRIBUTE_BACKGROUND_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'youtube_link' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
                Yii::t('back/Agents', 'Members tabs') => [
                    'agents_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
//                    'tab_label' => [
//                        'type' => ActiveFormBuilder::INPUT_TEXT,
//                        'options' => [
//                            'maxlength' => true,
//                        ],
//                    ],
                    $this->getRelatedFormConfig()['agentsMembers']
                ]
            ],
        ];

        return $config;
    }

    /**
     * @return array
     */
    public function getRelatedFormConfig()
    {
        return [
            'agentsMembers' => [
                'relation' => 'agentsMembers',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

}
