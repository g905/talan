<?php

namespace backend\modules\agents\controllers;

use backend\components\BackendController;
use backend\modules\agents\models\Agents;
 
class AgentsController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Agents::class;
    }
}
