<?php

namespace backend\modules\team\controllers;

use backend\modules\configuration\components\ConfigurationController;
use backend\modules\team\models\TeamPageDefaultCityTab;

/**
 * TeamPageDefaultCityTabController implements the CRUD actions for TeamPageDefaultCityTab model.
 */
class TeamPageDefaultCityTabController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return TeamPageDefaultCityTab::class;
    }
}
