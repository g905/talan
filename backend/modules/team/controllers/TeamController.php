<?php

namespace backend\modules\team\controllers;

use backend\components\BackendController;
use backend\modules\team\models\Team;

/**
 * TeamController implements the CRUD actions for Team model.
 */
class TeamController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Team::class;
    }
}
