<?php

namespace backend\modules\team\models;

use backend\modules\contacts\models\Manager;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\base\NotSupportedException;

/**
 * This is the model class for table "{{%team_member}}".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $manager_id
 * @property integer $team_id
 *
 * @property Manager $manager
 * @property Team $team
 */
class TeamMember extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%team_member}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manager_id'], 'required'],
            [['position', 'manager_id', 'team_id'], 'integer'],
            [['position'], 'default', 'value' => 0],
            [['manager_id'], 'exist', 'targetClass' => \common\models\Manager::class, 'targetAttribute' => 'id'],
            [['team_id'], 'exist', 'targetClass' => \common\models\Team::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/TeamMember', 'ID'),
            'position' => Yii::t('back/TeamMember', 'Position'),
            'created_at' => Yii::t('back/TeamMember', 'Created At'),
            'updated_at' => Yii::t('back/TeamMember', 'Updated At'),
            'manager_id' => Yii::t('back/TeamMember', 'Manager'),
            'team_id' => Yii::t('back/TeamMember', 'Team'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::class, ['id' => 'team_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/TeamMember', 'Team Member');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'position',
                    'manager_id',
                    'team_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'position',
                    'manager_id',
                    'team_id',
                ];
            break;
        }

        return [];
    }

    /**
     * @return void|\yii\db\ActiveRecord
     * @throws NotSupportedException
     */
    public function getSearchModel()
    {
        throw new NotSupportedException();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'manager_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\Manager::getItems('id', 'fio'),
                'options' => [
                    'prompt' => Yii::t('back/TeamMember', 'Chose manager'),
                ],
            ],
        ];

        return $config;
    }


}
