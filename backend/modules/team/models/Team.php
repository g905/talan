<?php

namespace backend\modules\team\models;

use backend\components\StylingActionColumn;
use backend\modules\contacts\models\City;
use kartik\grid\GridView;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\base\EntityToFile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%team}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $city_id
 * @property string $team_title
 * @property string $tab_label
 *
 * @property City $city
 * @property TeamMember[] $teamMembers
 */
class Team extends ActiveRecord implements BackendModel
{

    /**
    * Attribute for imageUploader
    */
    public $backgroundImage;

    /**
    * Temporary sign which used for saving images before model save
    * @var string
    */
    public $sign;

    /**
     * @throws \yii\base\Exception
     */
    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%team}}';
    }

    /**
     * @return void
     */
    protected function loadInitialValues()
    {
        $this->team_title = 'Команда в филиалах';
//        $this->tab_label = 'Управляющая компания';
        $this->label = 'Команда';
        $this->description = 'Мы компания заслужившая за годы работы репутацию надежного застройщика, и за эту репутацию мы готовы отвечать лично. Каждый сотрудник компании несет ответственность за качество работы.';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'description', 'city_id'], 'required'],
            [['description'], 'string', 'max' => 300],
            [['city_id'], 'integer'],
            [['label', 'team_title', 'tab_label'], 'string', 'max' => 255],
            [['published'], 'string', 'max' => 1],
            [['published'], 'default', 'value' => 1],
            [['city_id'], 'exist', 'targetClass' => \common\models\City::class, 'targetAttribute' => 'id'],
            [['city_id'], 'unique'],
            [['sign'], 'string', 'max' => 255],
         ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/Team', 'ID'),
            'label' => Yii::t('back/Team', 'Label'),
            'description' => Yii::t('back/Team', 'Description'),
            'published' => Yii::t('back/Team', 'Published'),
            'created_at' => Yii::t('back/Team', 'Created At'),
            'updated_at' => Yii::t('back/Team', 'Updated At'),
            'city_id' => Yii::t('back/Team', 'City'),
            'team_title' => Yii::t('back/Team', 'Team title'),
            'tab_label' => Yii::t('back/Team', 'Tab label'),
            'backgroundImage' => Yii::t('back/Team', 'Background image'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::class,
                'defaultFieldForTitle' => 'label',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamMembers()
    {
        return $this->hasMany(TeamMember::class, ['team_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/Team', 'Team');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    'published:boolean',
                    ['class' => StylingActionColumn::class],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'city_id',
                    'team_title',
//                    'tab_label',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new TeamSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        if ($this->isNewRecord) {
            $this->loadInitialValues();
        }

        $config = [
            'form-set' => [
                Yii::t('back/Team', 'Main') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'options' => [
                            'maxlength' => true,
                        ]
                    ],
                    'backgroundImage' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'backgroundImage',
                            'saveAttribute' => \common\models\Team::SAVE_ATTRIBUTE_BACKGROUND_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
                Yii::t('back/Team', 'Manager tabs') => [
                    'team_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
//                    'tab_label' => [
//                        'type' => ActiveFormBuilder::INPUT_TEXT,
//                        'options' => [
//                            'maxlength' => true,
//                        ],
//                    ],
                    $this->getRelatedFormConfig()['teamMembers']
                ]
            ],
        ];

        return $config;
    }

    /**
     * @return array
     */
    public function getRelatedFormConfig()
    {
        return [
            'teamMembers' => [
                'relation' => 'teamMembers',
            ]
        ];
    }

    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
