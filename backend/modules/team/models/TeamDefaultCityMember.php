<?php

namespace backend\modules\team\models;

use backend\modules\contacts\models\Manager;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\base\NotSupportedException;

/**
 * This is the model class for table "{{%team_default_city_members}}".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $manager_id
 * @property integer $page_id
 *
 * @property Manager $manager
 */
class TeamDefaultCityMember extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%team_default_city_member}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position', 'manager_id', 'page_id'], 'integer'],
            [['manager_id'], 'required'],
            [['position'], 'default', 'value' => 0],
            [['manager_id'], 'exist', 'targetClass' => \common\models\Manager::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/TeamMembersDefault', 'ID'),
            'position' => Yii::t('back/TeamMembersDefault', 'Position'),
            'created_at' => Yii::t('back/TeamMembersDefault', 'Created At'),
            'updated_at' => Yii::t('back/TeamMembersDefault', 'Updated At'),
            'manager_id' => Yii::t('back/TeamMembersDefault', 'Manager'),
            'page_id' => Yii::t('back/TeamMembersDefault', 'Page'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/TeamMembersDefault', 'Team Default City Members');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'position',
                    'manager_id',
                    'page_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'position',
                    'manager_id',
                    'page_id',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        throw new NotSupportedException();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'manager_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\Manager::getItems('id', 'fio'),
                'options' => [
                    'prompt' => '',
                ],
            ],
        ];

        return $config;
    }


}
