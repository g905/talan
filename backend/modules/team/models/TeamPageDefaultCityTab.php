<?php

namespace backend\modules\team\models;

use backend\modules\contacts\models\City;
use Yii;
use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\TeamPageDefaultCityTab as CommonTeamPageDefaultCityTab;
use yii\db\ActiveQueryInterface;
use yii\helpers\ArrayHelper;

/**
* Class TeamPageDefaultCityTab*/
class TeamPageDefaultCityTab extends ConfigurationModel
{
    public $showAsConfig = false;
    /**
    * Title of the form
    *
    * @return string
    */
    public function getTitle()
    {
        return Yii::t('back/TeamMembersDefault', 'Team page default city tab settings');
    }

    /**
    * @return array
    */
    public function getFormRules()
    {
        return [
            [CommonTeamPageDefaultCityTab::DEFAULT_TAB_TEXT, 'required'],
        ];
    }

    /**
    * @return array
    */
    public function getFormTypes()
    {
        return [
            CommonTeamPageDefaultCityTab::DEFAULT_TAB_TEXT => Configuration::TYPE_STRING,
        ];
    }

    /**
    * @return array
    */
    public function getFormDescriptions()
    {
        return [
            CommonTeamPageDefaultCityTab::DEFAULT_TAB_TEXT => Yii::t('back/TeamMembersDefault', 'Default tab text'),
        ];
    }

    /**
    * @return array
    */
    public function getFormHints()
    {
        return [
            CommonTeamPageDefaultCityTab::DEFAULT_TAB_TEXT => '',
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
        ];
    }


    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                Yii::t('back/TeamMembersDefault', 'Main') => [
                    CommonTeamPageDefaultCityTab::DEFAULT_TAB_TEXT,
                ],
                Yii::t('back/TeamMembersDefault', 'Team members') => [
                    $this->getRelatedFormConfig()['teamMembers']
                ],
            ]
        ];

        return $config;
    }

    /**
    * @return array
    */
    public function getRelatedFormConfig()
    {
        $config = [
            'teamMembers' => [
                'relation' => 'teamMembers',
            ],
        ];

        return $config;
    }

    /**
    * @return ActiveQueryInterface
    */
    public function getTeamMembers()
    {
        return $this->hasMany(TeamDefaultCityMember::class, ['page_id' => 'id'])->orderBy('position');
    }
}
