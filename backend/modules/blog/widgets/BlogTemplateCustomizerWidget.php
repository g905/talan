<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 21.03.18
 * Time: 14:47
 */

namespace backend\modules\blog\widgets;


use backend\modules\blog\models\BlogListViewTemplate;
use backend\modules\blog\widgets\assets\BlogTemplateCustomizerAsset;
use yii\helpers\Html;
use yii\widgets\InputWidget;
use Yii;
use common\models\BlogListPage as CommonBlogListPage;

class BlogTemplateCustomizerWidget extends InputWidget
{
    /**
     * Sortable inner item, used in JS and php
     * @var string
     */
    public $listItemTemplate = "<div data-btc_item>{input}<button class='btn btn-sm btn-danger template_customize_remove' type='button'>X</button></div>";

    /***
     * Input name
     * @var string
     */
    public $itemName = 'item';

    /**
     * @return string
     */
    public function run()
    {
        // assets
        BlogTemplateCustomizerAsset::register(Yii::$app->view);

        // loading data directly from DB
        $data = BlogListViewTemplate::find()
            ->alias('blvt')
            ->select(['blvt.item_type_id'])
            ->orderBy(['blvt.id' => SORT_ASC])
            ->column();

        // preparing widget items for sortable widget
        $items = [];
        foreach ($data as $item) {
            $items[] = [
                'content' => $this->getItem($item),
                'options' => [
                    'class' => 'block width' . $item,
                ]
            ];
        }

        return $this->render('blogTemplateCustomizerWidgetView', [
            'items' => $items,
            'itemTemplate' => $this->getItem(),
        ]);
    }

    /**
     * @param int $value
     * @return mixed
     */
    protected function getItem($value = 1)
    {
        $searches = [
            '{input}',
        ];
        $replaces = [
            Html::dropDownList($this->itemName . "[]", $value, static::getItems()),
        ];

        return str_replace($searches, $replaces, $this->listItemTemplate);
    }

    /**
     * @return array
     */
    public static function getItems()
    {
        return [
            CommonBlogListPage::LAYOUT_LEFT => Yii::t('back/BlogListPage', 'left_layout'),
            CommonBlogListPage::LAYOUT_RIGHT => Yii::t('back/BlogListPage', 'right_layout'),
            CommonBlogListPage::LAYOUT_FULL_ROW => Yii::t('back/BlogListPage', 'full_row_layout'),
        ];
    }
}