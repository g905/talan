<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 21.03.18
 * Time: 15:30
 */

namespace backend\modules\blog\widgets\assets;


use yii\web\AssetBundle;

class BlogTemplateCustomizerAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/blog/widgets/assets';
    public $css = [
        'css/blog_template_customizer.css',
    ];
    public $js = [
        'js/blog_template_customizer.js',
    ];

    public $depends = [
        'backend\assets\AppAsset',
    ];
}