(function () {
    $(document).ready(function(e) {
        initSelect2();
    });

    $(document).on('click', '#template_customize_add', function () {
        var template = $('[data-btc_item_template]').eq(0).val();
        var sortableItem = '<li class="block width1 ui-sortable-handle">' + template + '</li>';
        $('#blog_template_customizer_widget').append(sortableItem);
        initSelect2();
    });

    $(document).on('click', '.template_customize_remove', function (e) {
        var $element = $(e.target);
        $element.parent().parent().remove();
    });

    $(document).on('change', '#blog_template_customizer_widget select', function(e) {
        var $select = $(e.target);
        var value = $select.val();
        var cssClass = 'block ui-sortable-handle width' + value;
        $select.parent().parent().attr('class', cssClass);
    });

    var initSelect2 = function() {
        $('#blog_template_customizer_widget select').select2({width: "200px"});
    };
})();