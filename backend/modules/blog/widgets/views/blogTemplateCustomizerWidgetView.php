<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 21.03.18
 * Time: 14:48
 *
 * @var $items []
 * @var $itemTemplate string
 */
?>
<button type="button" id="template_customize_add" class="btn btn-sm btn-success">+</button>
<input type="hidden" value="<?= \yii\helpers\Html::encode($itemTemplate) ?>" data-btc_item_template>
<?= \yii\jui\Sortable::widget([
    'items' => $items,
    'options' => [
        'id' => 'blog_template_customizer_widget',
        'class' => 'well'
    ]
]) ?>
