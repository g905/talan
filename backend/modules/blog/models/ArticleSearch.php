<?php

namespace backend\modules\blog\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * ArticleSearch represents the model behind the search form about `Article`.
 */
class ArticleSearch extends Article
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'published', 'position'], 'integer'],
            [['label', 'alias', 'date_published', 'themeIds', 'cityIds'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArticleSearch::find()
            ->alias('a')
            ->with([
                'blogThemes',
            ])
            ->joinWith([
                'blogThemes' => function(ActiveQuery $query) {
                    return $query->alias('btheme');
                },
                'cities' => function(ActiveQuery $query) {
                    return $query->alias('city');
                }
            ], false)->orderBy(['date_published' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'a.published' => $this->published,
            'a.position' => $this->position,
            'a.date_published' => $this->date_published,
            'btheme.id' => $this->themeIds,
            'city.id' => $this->cityIds,
        ]);

        $query->andFilterWhere(['like', 'a.label', $this->label])
            ->andFilterWhere(['like', 'a.alias', $this->alias]);

        return $dataProvider;
    }
}
