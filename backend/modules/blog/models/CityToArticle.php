<?php

namespace backend\modules\blog\models;

use Yii;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\contacts\models\City;
use common\models\City as CommonCity;
use common\models\Article as CommonArticle;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;

/**
 * This is the model class for table "{{%city_to_article}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $article_id
 *
 * @property Article $article
 * @property City $city
 */
class CityToArticle extends ActiveRecord implements BackendModel
{
    public static function tableName()
    {
        return '{{%city_to_article}}';
    }

    public function rules()
    {
        return [
            [['city_id', 'article_id'], 'required'],
            [['city_id', 'article_id'], 'integer'],
            [['city_id'], 'exist', 'targetClass' => CommonCity::class, 'targetAttribute' => 'id'],
            [['article_id'], 'exist', 'targetClass' => CommonArticle::class, 'targetAttribute' => 'id'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'article_id' => 'Article ID',
        ];
    }

    public function getArticle()
    {
        return $this->hasOne(Article::class, ['id' => 'article_id']);
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getTitle()
    {
        return Yii::t('app', 'City To Article');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'city_id',
                    'article_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            case 'view':
                return [
                    'id',
                    'city_id',
                    'article_id',
                ];
        }

        return [];
    }

    public function getSearchModel()
    {
        return new ThemeToArticleSearch();
    }

    public function getFormConfig()
    {
        return [
            'city_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => CommonCity::getItems(),
                'options' => ['prompt' => ''],
            ],
            'article_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => CommonArticle::getItems(),
                'options' => ['prompt' => ''],
            ],
        ];
    }
}
