<?php

namespace backend\modules\blog\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%theme_to_article}}".
 *
 * @property integer $id
 * @property integer $blog_theme_id
 * @property integer $article_id
 *
 * @property Article $article
 * @property BlogTheme $blogTheme
 */
class ThemeToArticle extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%theme_to_article}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blog_theme_id', 'article_id'], 'required'],
            [['blog_theme_id', 'article_id'], 'integer'],
            [['blog_theme_id'], 'exist', 'targetClass' => \common\models\BlogTheme::className(), 'targetAttribute' => 'id'],
            [['article_id'], 'exist', 'targetClass' => \common\models\Article::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'blog_theme_id' => 'Blog Theme ID',
            'article_id' => 'Article ID',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogTheme()
    {
        return $this->hasOne(BlogTheme::className(), ['id' => 'blog_theme_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Theme To Article');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'blog_theme_id',
                    'article_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'blog_theme_id',
                    'article_id',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ThemeToArticleSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'blog_theme_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\BlogTheme::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'article_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\Article::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
        ];

        return $config;
    }


}
