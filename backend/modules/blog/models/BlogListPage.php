<?php

namespace backend\modules\blog\models;

use backend\modules\blog\widgets\BlogTemplateCustomizerWidget;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\BlogListPage as CommonBlogListPage;
/**
* Class BlogListPage*/
class BlogListPage extends ConfigurationModel
{
    public $showAsConfig = false;

    public $template;

    public $videoBg;
    public $videoPoster;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_AFTER_SAVE, [$this, 'saveListTemplateWidgetData']);
    }

    /**
    * Title of the form
    *
    * @return string
    */
    public function getTitle()
    {
        return Yii::t('back/BlogListPage', 'Blog list page');
    }

    /**
    * @return array
    */
    public function getFormRules()
    {
        return [
            [CommonBlogListPage::LABEL, 'required'],
            [CommonBlogListPage::TOP_FORM_TITLE, 'required'],
            [CommonBlogListPage::TOP_FORM_TEXT, 'required'],
            [CommonBlogListPage::TOP_FORM_AGREEMENT, 'required'],
            [CommonBlogListPage::TOP_FORM_BUTTON_TEXT, 'required'],
            [CommonBlogListPage::TOP_FORM_ONSUBMIT, 'string'],
            [CommonBlogListPage::BOTTOM_FORM_BLACK_TITLE, 'required'],
            [CommonBlogListPage::BOTTOM_FORM_WHITE_TITLE, 'required'],
            [CommonBlogListPage::BOTTOM_FORM_AGREEMENT, 'required'],
            [CommonBlogListPage::BOTTOM_FORM_BUTTON_TEXT, 'required'],
            [CommonBlogListPage::BOTTOM_FORM_ONSUBMIT, 'string'],
        ];
    }

    /**
    * @return array
    */
    public function getFormTypes()
    {
        return [
            CommonBlogListPage::LABEL => Configuration::TYPE_STRING,
            CommonBlogListPage::TOP_FORM_TITLE => Configuration::TYPE_STRING,
            CommonBlogListPage::TOP_FORM_TEXT => Configuration::TYPE_TEXT,
            CommonBlogListPage::TOP_FORM_AGREEMENT => Configuration::TYPE_HTML,
            CommonBlogListPage::TOP_FORM_BUTTON_TEXT => Configuration::TYPE_STRING,
            CommonBlogListPage::TOP_FORM_ONSUBMIT => Configuration::TYPE_STRING,
            CommonBlogListPage::BOTTOM_FORM_BLACK_TITLE => Configuration::TYPE_STRING,
            CommonBlogListPage::BOTTOM_FORM_WHITE_TITLE => Configuration::TYPE_STRING,
            CommonBlogListPage::BOTTOM_FORM_AGREEMENT => Configuration::TYPE_HTML,
            CommonBlogListPage::BOTTOM_FORM_BUTTON_TEXT => Configuration::TYPE_STRING,
            CommonBlogListPage::BOTTOM_FORM_ONSUBMIT => Configuration::TYPE_STRING,
        ];
    }

    /**
    * @return array
    */
    public function getFormDescriptions()
    {
        return [
            CommonBlogListPage::LABEL => Yii::t('back/BlogListPage', 'Label'),
            CommonBlogListPage::TOP_FORM_TITLE => Yii::t('back/BlogListPage', 'Top form title'),
            CommonBlogListPage::TOP_FORM_TEXT => Yii::t('back/BlogListPage', 'Top form text'),
            CommonBlogListPage::TOP_FORM_AGREEMENT => Yii::t('back/BlogListPage', 'Top form agreement'),
            CommonBlogListPage::TOP_FORM_BUTTON_TEXT => Yii::t('back/BlogListPage', 'Top form button text'),
            CommonBlogListPage::TOP_FORM_ONSUBMIT => Yii::t('back/BlogListPage', 'Top form onsubmit'),
            CommonBlogListPage::BOTTOM_FORM_BLACK_TITLE => Yii::t('back/BlogListPage', 'Bottom foorm black title'),
            CommonBlogListPage::BOTTOM_FORM_WHITE_TITLE => Yii::t('back/BlogListPage', 'Bottom form white title'),
            CommonBlogListPage::BOTTOM_FORM_AGREEMENT => Yii::t('back/BlogListPage', 'Bottom form agreement'),
            CommonBlogListPage::BOTTOM_FORM_BUTTON_TEXT => Yii::t('back/BlogListPage', 'Bottom form button text'),
            CommonBlogListPage::BOTTOM_FORM_ONSUBMIT => Yii::t('back/BlogListPage', 'Bottom form onsubmit'),
        ];
    }

    /**
    * @return array
    */
    public function getFormHints()
    {
        return [
            CommonBlogListPage::LABEL => '',
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'template' => Yii::t('back/BlogListPage', 'Template'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ]);
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                Yii::t('back/BlogListPage', 'Main') => [
                    CommonBlogListPage::LABEL,
                    'videoPoster' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'videoPoster',
                            'saveAttribute' => CommonBlogListPage::SAVE_ATTRIBUTE_VIDEO_POSTER,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                        ])
                    ],
                    'videoBg' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'videoBg',
                            'saveAttribute' => CommonBlogListPage::SAVE_ATTRIBUTE_VIDEO_BG,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                            'allowedFileExtensions' => ['mp4'],
                        ])
                    ],
                ],
                Yii::t('back/BlogListPage', 'Top form') => [
                    CommonBlogListPage::TOP_FORM_TITLE,
                    CommonBlogListPage::TOP_FORM_TEXT,
                    CommonBlogListPage::TOP_FORM_AGREEMENT,
                    CommonBlogListPage::TOP_FORM_BUTTON_TEXT,
                    CommonBlogListPage::TOP_FORM_ONSUBMIT,
                ],
                Yii::t('back/BlogListPage', 'Template customize') => [
                    'template' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => BlogTemplateCustomizerWidget::widget([
                            'model' => $this,
                            'attribute' => 'template',
                        ])
                    ]
                ],
                Yii::t('back/BlogListPage', 'Bottom form') => [
                    CommonBlogListPage::BOTTOM_FORM_BLACK_TITLE,
                    CommonBlogListPage::BOTTOM_FORM_WHITE_TITLE,
                    CommonBlogListPage::BOTTOM_FORM_AGREEMENT,
                    CommonBlogListPage::BOTTOM_FORM_BUTTON_TEXT,
                    CommonBlogListPage::BOTTOM_FORM_ONSUBMIT,
                ],
            ]
        ];

        return $config;
    }

    /**
     * @return void
     */
    public function saveListTemplateWidgetData()
    {
        if (!Yii::$app->request->isPost) {
            return;
        }

        $widgetModel = new BlogListViewTemplate();
        $data = Yii::$app->request->post('item', []);
        $widgetModel->load($data, '');

        BlogListViewTemplate::deleteAll();

        foreach ($data as $item) {
            $widgetModel = new BlogListViewTemplate();
            $safeItemValue = (int)$item;
            if ($safeItemValue === false) {
                $safeItemValue = CommonBlogListPage::LAYOUT_LEFT;
            }
            $widgetModel->item_type_id = $safeItemValue;
            $widgetModel->save(false);
        }
    }
}
