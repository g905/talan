<?php

namespace backend\modules\blog\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ThemeToArticleSearch represents the model behind the search form about `ThemeToArticle`.
 */
class ThemeToArticleSearch extends ThemeToArticle
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'blog_theme_id', 'article_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ThemeToArticleSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'blog_theme_id' => $this->blog_theme_id,
            'article_id' => $this->article_id,
        ]);

        return $dataProvider;
    }
}
