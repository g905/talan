<?php

namespace backend\modules\blog\models;

use backend\components\ManyToManyBehavior;
use backend\helpers\Select2Helper;
use backend\modules\builder\components\BuilderBehavior;
use backend\modules\builder\models\BuilderConfig;
use backend\modules\contacts\models\Manager;
use common\helpers\SiteUrlHelper;
use common\models\Article as CommonArticle;
use common\models\City;
use kartik\grid\GridView;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%article}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $date_published
 * @property integer $manager_id
 * @property string $short_description
 * @property string $form_title
 * @property string $form_description
 * @property string $form_agreement
 * @property string $form_onsubmit
 *
 * @property BlogTheme[] $blogThemes
 * @property City[] $cities
 */
class Article extends ActiveRecord implements BackendModel
{
    public $themeIds;
    public $themeIdValues;

    public $cityIds;
    public $cityIdValues;

    /**
    * Attribute for imageUploader
    */
    public $miniImageAttr;

    /**
    * Attribute for imageUploader
    */
    public $coverImageAttr;

    /***
     * @var mixed
     */
    public $content;

    /**
    * Temporary sign which used for saving images before model save
    * @var string
    */
    public $sign;

    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = app()->security->generateRandomString();
        }
    }

    public static function tableName()
    {
        return '{{%article}}';
    }

    public function rules()
    {
        return [
            [['label'], 'required'],
            [['published', 'position', 'show_on_home'], 'integer'],
            [['date_published'], 'date'],
            [['label', 'alias', 'form_title', 'form_onsubmit'], 'string', 'max' => 255],
            [['alias'], 'unique'],
            [['short_description', 'form_description'], 'string', 'max' => 1000],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['show_on_home'], 'default', 'value' => 0],
            [['form_agreement'], 'string'],
            [['manager_id'], 'integer'],
            [['date_published'], 'default', 'value' => null],
            [['sign'], 'string', 'max' => 255],
            [['themeIds', 'cityIds'], 'safe'],
         ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'Article'),
            'label' => bt('Label', 'Article'),
            'alias' => bt('Alias', 'Article'),
            'published' => bt('Published', 'Article'),
            'position' => bt('Position', 'Article'),
            'created_at' => bt('Created At', 'Article'),
            'updated_at' => bt('Updated At', 'Article'),
            'date_published' => bt('Date published', 'Article'),
            'miniImageAttr' => bt('Mini image', 'Article'),
            'coverImageAttr' => bt('Cover image', 'Article'),
            'themeIds' => bt('Themes', 'Article'),
            'cityIds' => bt('Cities', 'Article'),
            'content' => bt('Content', 'Article'),
            'manager_id' => bt('Manager', 'Article'),
            'short_description' => bt('Short description', 'Article'),
            'form_title' => bt('Form title', 'Article'),
            'form_description' => bt('Form description', 'Article'),
            'form_agreement' => bt('Form agreement', 'Article'),
            'form_onsubmit' => bt('Form onsubmit', 'Article'),
            'show_on_home' => bt('Show on home page', 'Article'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::class,
            ],
            'manyToManyRelation' => [
                'class' => ManyToManyBehavior::class,
                'currentObj' => $this,
                'currentRelatedColumn' => 'article_id',
                'manyToManyConfig' => [
                    'promoAction' => [
                        'attribute' => 'themeIds',
                        'valueAttribute' => 'themeIdValues',
                        'relatedColumn' => 'blog_theme_id',
                        'linkTableName' => ThemeToArticle::tableName(),
                        'relatedObjTableName' => BlogTheme::tableName(),
                        'isStringReturn' => false
                    ],
                    'cities' => [
                        'attribute' => 'cityIds',
                        'valueAttribute' => 'cityIdValues',
                        'relatedColumn' => 'city_id',
                        'linkTableName' => CityToArticle::tableName(),
                        'relatedObjTableName' => \backend\modules\contacts\models\City::tableName(),
                        'isStringReturn' => false
                    ],
                ]
            ],
            'content' => [
                'class' => BuilderBehavior::class,
                'attributes' => ['content'],
            ],
        ];
    }

    public function getTitle()
    {
        return bt('Article', 'Article');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    [
                        'attribute' => 'miniImageAttr',
                        'format' => 'raw',
                        'value' => function(self $data) {
                            return FPM::image($data->miniImageAttrRelation->file_id ?? null, 'admin', 'file');
                        }
                    ],
                    [
                        'attribute' => 'themeIds',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            if (empty($model->blogThemes)){
                                return '-';
                            }

                            return $model->getHtmlBlogThemesList();
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => BlogTheme::getItems(),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => [
                            'placeholder' => bt('Any theme', 'article'),
                        ],
                    ],
                    [
                        'attribute' => 'cityIds',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            if (empty($model->cities)){
                                return '-';
                            }

                            return $model->getHtmlCitiesList();
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => City::getItems(),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => [
                            'placeholder' => bt('Any city', 'article'),
                        ],
                    ],
                    [
                        'attribute' => 'date_published',
                        'value' => function(self $model) {
                            return $model->date_published;
                        },
                        'filterType' => GridView::FILTER_DATE,
                        'filterWidgetOptions' => [
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'autoclose' => true
                            ],
                        ]
                    ],
                    'published:boolean',
                    ['class' => \backend\components\StylingActionColumn::class],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    'alias',
                    //'show_on_home:boolean',
                    'published:boolean',
                    'date_published',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ArticleSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        if ($this->isNewRecord) {
            $this->date_published = Yii::$app->formatter->asDate((new \DateTime()), 'yyyy-MM-dd');
            $this->form_agreement = "Даю <a href=\"" . SiteUrlHelper::getAgreementUrl() .  "\" target=\"_blank\">согласие</a> на обработку моих персональных данных, с условиями <a href=\"" . SiteUrlHelper::getPrivatePolicyUrl() . "\" target=\"_blank\">Политики</a> ознакомлен.";
            $this->form_title = "Мы всегда готовы ответить на любые ваши вопросы";
            $this->form_description = "Мы поможем подобрать то, что вам подойдет лучше всего. Оставьте ваши данные и наш специалист перезвонит и проконсультирует по любому вопросу.";
        }

        $config = [
            'form-set' => [
                Yii::t('back/Article', 'Main') => [
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                            'class' => 'form-control' . ($this->isNewRecord ? ' s_name' : ''),
                        ],
                    ],
                    'alias' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                            'class' => 'form-control' . ($this->isNewRecord ? ' s_alias' : '')
                        ],
                    ],
                    'miniImageAttr' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'miniImageAttr',
                            'saveAttribute' => \common\models\Article::SAVE_ATTRIBUTE_MINI_IMAGE_ATTR,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'coverImageAttr' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'coverImageAttr',
                            'saveAttribute' => \common\models\Article::SAVE_ATTRIBUTE_COVER_IMAGE_ATTR,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'date_published' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \metalguardian\dateTimePicker\Widget::className(),
                        'options' => [
                            'mode' => \metalguardian\dateTimePicker\Widget::MODE_DATE,
                            'options' => [
                                'class' => 'form-control',
                            ],
                        ],
                    ],
                    'themeIds' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'label' => $this->getAttributeLabel('themeIds'),
                        'value' => Select2Helper::getAjaxSelect2Widget(
                            $this,
                            'themeIds',
                            static::getThemesUrl(),
                            $this->themeIdValues,
                            [
                                'multiple' => true,
                            ]
                        )
                    ],
                    'cityIds' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'label' => $this->getAttributeLabel('cityIds'),
                        'value' => Select2Helper::getAjaxSelect2Widget(
                            $this,
                            'cityIds',
                            static::getCitiesUrl(),
                            $this->cityIdValues,
                            ['multiple' => true]
                        )
                    ],
                    'short_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'options' => [
                            'maxlength' => true,
                        ]
                    ],
//                    'show_on_home' => [
//                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
//                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
                Yii::t('back/article', 'Content') => [
                    'content' => BuilderConfig::config($this, 'content', [
                        'models' => [
                            \common\models\builder\BlogHtmlBuilderModel::class,
                            \common\models\builder\BlogDoubleImageBuilderModel::class,
                            \common\models\builder\BlogYoutubeVideoBuilderModel::class,
                            \common\models\builder\BlogSliderBuilderModel::class,
                            \common\models\builder\BlogImageWithTextBuilderModel::class,
                        ]
                    ]),
                ],
                Yii::t('back/article', 'Form') => [
                    'form_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true
                        ]
                    ],
                    'form_onsubmit' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true
                        ]
                    ],
                    'form_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'options' => [
                            'maxlength' => true
                        ]
                    ],
                    'form_agreement' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::class,
                        'options' => [
                            'model' => $this,
                            'attribute' => 'form_agreement',
                            'settings' => [
                                'buttons' => ['link'],
                                'formatting' => ['p', 'h3', 'a'],
                                'plugins' => [
                                    'fullscreen',
                                ],
                            ],
                        ]
                    ],
                    'manager_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => Manager::getItems('id', 'fio'),
                        'options' => [
                            'prompt' => '--'
                        ]
                    ]
                ],
            ]
        ];

        return $config;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogThemes()
    {
        return $this->hasMany(BlogTheme::class, ['id' => 'blog_theme_id'])
            ->viaTable(ThemeToArticle::tableName(), ['article_id' => 'id']);
    }

    public function getCities()
    {
        return $this->hasMany(City::class, ['id' => 'city_id'])
            ->viaTable(CityToArticle::tableName(), ['article_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    public static function getThemesUrl()
    {
        return '/blog/article/themes';
    }

    public static function getCitiesUrl()
    {
        return '/blog/article/cities';
    }

    /**
     * @return string
     */
    public function getHtmlBlogThemesList()
    {
        $items = [];
        foreach ($this->blogThemes as $blogTheme) {
            $theme = $blogTheme->label;
            if (!empty($theme)) {
                $items[] = $theme;
            }
        }

        return implode(', ', $items);
    }

    /**
     * @return string
     */
    public function getHtmlCitiesList()
    {
        $items = [];
        foreach ($this->cities as $city) {
            if (!empty($city->label)) {
                $items[] = $city->label;
            }
        }

        return implode(', ', $items);
    }

    public function getMiniImageAttrRelation()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['miniImageAttr.entity_model_name' => 'Article', 'miniImageAttr.attribute' => CommonArticle::SAVE_ATTRIBUTE_MINI_IMAGE_ATTR])
            ->alias('miniImageAttr')
            ->orderBy('miniImageAttr.position DESC');
    }
}
