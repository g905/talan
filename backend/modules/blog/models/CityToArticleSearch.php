<?php

namespace backend\modules\blog\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CityToArticleSearch represents the model behind the search form about `CityToArticle`.
 */
class CityToArticleSearch extends CityToArticle
{
    public function rules()
    {
        return [
            [['id', 'city_id', 'article_id'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = CityToArticleSearch::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'article_id' => $this->article_id,
        ]);

        return $dataProvider;
    }
}
