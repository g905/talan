<?php

namespace backend\modules\blog\controllers;

use backend\components\BackendController;
use backend\modules\blog\models\ThemeToArticle;

/**
 * ThemeToArticleController implements the CRUD actions for ThemeToArticle model.
 */
class ThemeToArticleController extends BackendController
{
    public function getModelClass()
    {
        return ThemeToArticle::class;
    }
}
