<?php

namespace backend\modules\blog\controllers;

use backend\components\BackendController;
use backend\modules\blog\models\Article;
use backend\modules\blog\models\BlogTheme;
use backend\modules\contacts\models\City;
use yii\web\Response;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Article::className();
    }

    /**
     *
     */
    public function actionThemes($search = null, $id = null, $showNotChoose = false, $clearText = null)
    {
        if (!isset($clearText)){
            $clearText = \Yii::t('back/article', 'Choose theme');
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($search)) {
            $search = '';
        }

        $query = BlogTheme::find()
            ->select([
                'id',
                'text' => 'label',
            ])
            ->where(['like', 'label', $search])
            ->limit(10)
            ->asArray();
        $out['results'] = array_values($query->all());

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => BlogTheme::findOne($id)->label ?? null];
        }

        if ($showNotChoose){
            $notChooseValue = ['id' => '0', 'text' => $clearText];
            array_unshift($out['results'] , $notChooseValue);
        }
        return $out;
    }

    public function actionCities($search = null, $id = null, $showNotChoose = false, $clearText = null)
    {
        if (!isset($clearText)){
            $clearText = \Yii::t('back/article', 'Choose city');
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($search)) {
            $search = '';
        }

        $query = City::find()
            ->select([
                'id',
                'text' => 'label',
            ])
            ->where(['like', 'label', $search])
            ->limit(10)
            ->asArray();
        $out['results'] = array_values($query->all());

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => City::findOne($id)->label ?? null];
        }

        if ($showNotChoose){
            $notChooseValue = ['id' => '0', 'text' => $clearText];
            array_unshift($out['results'] , $notChooseValue);
        }
        return $out;
    }
}
