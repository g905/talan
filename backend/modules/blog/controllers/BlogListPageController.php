<?php

namespace backend\modules\blog\controllers;

use backend\modules\configuration\components\ConfigurationController;
use backend\modules\blog\models\BlogListPage;
use backend\modules\configuration\components\ConfigurationModel;
use Yii;

/**
 * BlogListPageController implements the CRUD actions for BlogListPage model.
 */
class BlogListPageController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BlogListPage::class;
    }

    /**
     * @param null $id
     *
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id = null)
    {
        $class = $this->getModelClass();
        /** @var ConfigurationModel $model */
        $model = new $class();
        $config = $this->getRelatedFormActionConfig($model);
        if (!empty($config)) {
            return $this->relatedFormAction($model, $config);
        }

        if (Yii::$app->request->isPost) {
            $this->loadModels($model);

            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('info', Yii::t('app', 'Record successfully updated!'));
            }
        }

        return $this->render('@app/modules/configuration/views/configuration/templates/update', [
            'model' => $model,
        ]);
    }
}
