<?php

namespace backend\modules\blog\controllers;

use backend\components\BackendController;
use backend\modules\blog\models\BlogTheme;

/**
 * BlogThemeController implements the CRUD actions for BlogTheme model.
 */
class BlogThemeController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BlogTheme::class;
    }
}
