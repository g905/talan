<?php

namespace backend\modules\resale;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\resale\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
