<?php

namespace backend\modules\resale\controllers;

use backend\components\BackendController;
use backend\modules\blog\models\Article;
use backend\modules\resale\models\SecondaryRealEstate;
use yii\web\Response;

/**
 * SecondaryRealEstateController implements the CRUD actions for SecondaryRealEstate model.
 */
class SecondaryRealEstateController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return SecondaryRealEstate::class;
    }

    /**
     *
     */
    public function actionArticles($search = null, $id = null, $showNotChoose = false, $clearText = null)
    {
        if (!isset($clearText)){
            $clearText = \Yii::t('back/SecondaryRealEstate', 'Choose article');
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($search)) {
            $search = '';
        }

        $query = Article::find()
            ->select([
                'id',
                'text' => 'label',
            ])
            ->where(['like', 'label', $search])
            ->limit(10)
            ->asArray();
        $out['results'] = array_values($query->all());

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Article::findOne($id)->label ?? null];
        }

        if ($showNotChoose){
            $notChooseValue = ['id' => '0', 'text' => $clearText];
            array_unshift($out['results'] , $notChooseValue);
        }
        return $out;
    }
}
