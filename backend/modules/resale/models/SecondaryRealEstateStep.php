<?php

namespace backend\modules\resale\models;

use backend\helpers\StdInput;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\base\NotSupportedException;

/**
 * This is the model class for table "{{%secondary_real_estate_step}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $content
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $secondary_real_estate_id
 *
 * @property SecondaryRealEstate $secondaryRealEstate
 */
class SecondaryRealEstateStep extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%secondary_real_estate_step}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'content'], 'required'],
            [['content'], 'string', 'max' => 500],
            [['position', 'secondary_real_estate_id'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['position'], 'default', 'value' => 0],
            [['secondary_real_estate_id'], 'exist', 'targetClass' => \common\models\SecondaryRealEstate::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/RealEstateStep', 'ID'),
            'label' => Yii::t('back/RealEstateStep', 'Label'),
            'content' => Yii::t('back/RealEstateStep', 'Content'),
            'position' => Yii::t('back/RealEstateStep', 'Position'),
            'created_at' => Yii::t('back/RealEstateStep', 'Created At'),
            'updated_at' => Yii::t('back/RealEstateStep', 'Updated At'),
            'secondary_real_estate_id' => Yii::t('back/RealEstateStep', 'Secondary real estate'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondaryRealEstate()
    {
        return $this->hasOne(SecondaryRealEstate::class, ['id' => 'secondary_real_estate_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/RealEstateStep', 'Secondary Real Estate Step');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    // 'content:ntext',
                    'position',
                    'secondary_real_estate_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'position',
                    'secondary_real_estate_id',
                ];
            break;
        }

        return [];
    }

    /**
     * @return void|\yii\db\ActiveRecord
     * @throws NotSupportedException
     */
    public function getSearchModel()
    {
        throw new NotSupportedException();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => StdInput::text(),
            'content' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                'options' => [
                    'maxlength' => true,
                ],
            ],
        ];

        return $config;
    }


}
