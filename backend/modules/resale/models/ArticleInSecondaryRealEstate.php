<?php

namespace backend\modules\resale\models;

use backend\modules\blog\models\Article;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%article_in_secondary_real_estate}}".
 *
 * @property integer $id
 * @property integer $secondary_real_estate_id
 * @property integer $article_id
 *
 * @property SecondaryRealEstate $secondaryRealEstate
 * @property Article $article
 */
class ArticleInSecondaryRealEstate extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article_in_secondary_real_estate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['secondary_real_estate_id', 'article_id'], 'required'],
            [['secondary_real_estate_id', 'article_id'], 'integer'],
            [['secondary_real_estate_id'], 'exist', 'targetClass' => \common\models\SecondaryRealEstate::className(), 'targetAttribute' => 'id'],
            [['article_id'], 'exist', 'targetClass' => \common\models\Article::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'secondary_real_estate_id' => 'Secondary real estate',
            'article_id' => 'Article',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondaryRealEstate()
    {
        return $this->hasOne(SecondaryRealEstate::className(), ['id' => 'secondary_real_estate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Article In Secondary Real Estate');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'secondary_real_estate_id',
                    'article_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'secondary_real_estate_id',
                    'article_id',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ArticleInSecondaryRealEstateSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'secondary_real_estate_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\SecondaryRealEstate::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'article_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\Article::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
        ];

        return $config;
    }


}
