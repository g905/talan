<?php

namespace backend\modules\resale\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SecondaryRealEstateSearch represents the model behind the search form about `SecondaryRealEstate`.
 */
class SecondaryRealEstateSearch extends SecondaryRealEstate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'position', 'city_id'], 'integer'],
            [['label', 'published', 'description', 'button_text', 'conditions_label', 'opportunity_black_label', 'opportunity_white_label', 'opportunity_description', 'opportunity_button_text', 'apartments_label', 'steps_label', 'steps_description', 'steps_link_text', 'steps_link_url', 'articles_label'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SecondaryRealEstateSearch::find()
            ->with(['city']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'position' => $this->position,
            'city_id' => $this->city_id,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'published', $this->published])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'button_text', $this->button_text])
            ->andFilterWhere(['like', 'conditions_label', $this->conditions_label])
            ->andFilterWhere(['like', 'opportunity_black_label', $this->opportunity_black_label])
            ->andFilterWhere(['like', 'opportunity_white_label', $this->opportunity_white_label])
            ->andFilterWhere(['like', 'opportunity_description', $this->opportunity_description])
            ->andFilterWhere(['like', 'opportunity_button_text', $this->opportunity_button_text])
            ->andFilterWhere(['like', 'apartments_label', $this->apartments_label])
            ->andFilterWhere(['like', 'steps_label', $this->steps_label])
            ->andFilterWhere(['like', 'steps_description', $this->steps_description])
            ->andFilterWhere(['like', 'steps_link_text', $this->steps_link_text])
            ->andFilterWhere(['like', 'steps_link_url', $this->steps_link_url])
            ->andFilterWhere(['like', 'articles_label', $this->articles_label]);

        return $dataProvider;
    }
}
