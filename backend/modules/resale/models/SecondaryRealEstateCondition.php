<?php

namespace backend\modules\resale\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\base\EntityToFile;
use yii\base\NotSupportedException;

/**
 * This is the model class for table "{{%secondary_real_estate_condition}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $content
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $secondary_real_estate_id
 *
 * @property SecondaryRealEstate $secondaryRealEstate
 */
class SecondaryRealEstateCondition extends ActiveRecord implements BackendModel
{

    /**
    * Attribute for imageUploader
    */
    public $imageProp;

    /**
    * Temporary sign which used for saving images before model save
    * @var string
    */
    public $sign;

    /**
     * @throws \yii\base\Exception
     */
    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%secondary_real_estate_condition}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'content'], 'required'],
            [['content'], 'string', 'max' => 500],
            [['position', 'secondary_real_estate_id'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['position'], 'default', 'value' => 0],
            [['secondary_real_estate_id'], 'exist', 'targetClass' => \common\models\SecondaryRealEstate::class, 'targetAttribute' => 'id'],
            [['sign'], 'string', 'max' => 255],
         ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/RealEstateCondition', 'ID'),
            'label' => Yii::t('back/RealEstateCondition', 'Label'),
            'content' => Yii::t('back/RealEstateCondition', 'Content'),
            'position' => Yii::t('back/RealEstateCondition', 'Position'),
            'created_at' => Yii::t('back/RealEstateCondition', 'Created At'),
            'updated_at' => Yii::t('back/RealEstateCondition', 'Updated At'),
            'secondary_real_estate_id' => Yii::t('back/RealEstateCondition', 'Secondary real estate'),
            'imageProp' => Yii::t('back/RealEstateCondition', 'Image'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondaryRealEstate()
    {
        return $this->hasOne(SecondaryRealEstate::class, ['id' => 'secondary_real_estate_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/RealEstateCondition', 'Secondary Real Estate Condition');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    // 'content:ntext',
                    'position',
                    'secondary_real_estate_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'position',
                    'secondary_real_estate_id',
                ];
            break;
        }

        return [];
    }

    /**
     * @return void|\yii\db\ActiveRecord
     * @throws NotSupportedException
     */
    public function getSearchModel()
    {
        throw new NotSupportedException();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'imageProp' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'imageProp',
                    'saveAttribute' => \common\models\SecondaryRealEstateCondition::SAVE_ATTRIBUTE_IMAGE_PROP,
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }


    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
