<?php

namespace backend\modules\resale\models;

use backend\helpers\StdInput;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\base\NotSupportedException;

/**
 * This is the model class for table "{{%secondary_real_estate_menu}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $secondary_real_estate_id
 * @property string $link
 * @property integer $show_right
 *
 * @property SecondaryRealEstate $secondaryRealEstate
 */
class SecondaryRealEstateMenu extends ActiveRecord implements BackendModel
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->published = 1;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%secondary_real_estate_menu}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'link'], 'required'],
            [['position', 'secondary_real_estate_id', 'published', 'show_right'], 'integer'],
            [['link'], 'string'],
            [['label'], 'string', 'max' => 255],
            [['published', 'show_right'], 'string', 'max' => 1],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['secondary_real_estate_id'], 'exist', 'targetClass' => \common\models\SecondaryRealEstate::class, 'targetAttribute' => 'id'],
            [['link'], 'url', 'defaultScheme' => 'http'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/SecRealEstateMenu', 'ID'),
            'label' => Yii::t('back/SecRealEstateMenu', 'Label'),
            'published' => Yii::t('back/SecRealEstateMenu', 'Published'),
            'position' => Yii::t('back/SecRealEstateMenu', 'Position'),
            'created_at' => Yii::t('back/SecRealEstateMenu', 'Created At'),
            'updated_at' => Yii::t('back/SecRealEstateMenu', 'Updated At'),
            'secondary_real_estate_id' => Yii::t('back/SecRealEstateMenu', 'Secondary real estate'),
            'link' => Yii::t('back/SecRealEstateMenu', 'Link'),
            'show_right' => Yii::t('back/SecRealEstateMenu', 'Show right'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondaryRealEstate()
    {
        return $this->hasOne(SecondaryRealEstate::class, ['id' => 'secondary_real_estate_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/SecRealEstateMenu', 'Secondary Real Estate Menu');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    'published:boolean',
                    'position',
                    'secondary_real_estate_id',
                    'link:url',
                    'show_right',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    'published:boolean',
                    'position',
                    'secondary_real_estate_id',
                    'link:url',
                    'show_right',
                ];
            break;
        }

        return [];
    }

    /**
     * @return void|\yii\db\ActiveRecord
     * @throws NotSupportedException
     */
    public function getSearchModel()
    {
        throw new NotSupportedException();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => StdInput::text(),
            'link' => StdInput::text(),
            'show_right' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }


}
