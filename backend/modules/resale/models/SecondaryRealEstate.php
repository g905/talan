<?php

namespace backend\modules\resale\models;

use backend\components\ManyToManyBehavior;
use backend\components\StylingActionColumn;
use backend\helpers\StdInput;
use backend\modules\blog\models\Article;
use backend\modules\contacts\models\City;
use kartik\grid\GridView;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\models\base\EntityToFile;
use yii\helpers\ArrayHelper;
use backend\helpers\Select2Helper;

/**
 * This is the model class for table "{{%secondary_real_estate}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $description
 * @property string $description_only_desktop
 * @property string $button_text
 * @property string $conditions_label
 * @property string $opportunity_black_label
 * @property string $opportunity_white_label
 * @property string $opportunity_description
 * @property string $opportunity_button_text
 * @property string $apartments_label
 * @property string $steps_label
 * @property string $steps_description
 * @property string $steps_link_text
 * @property string $steps_link_url
 * @property string $articles_label
 * @property integer $city_id
 *
 * @property City $city
 */
class SecondaryRealEstate extends ActiveRecord implements BackendModel
{
    public $articleIds;
    public $articleIdValues;

    /**
    * Attribute for imageUploader
    */
    public $bgImage;

    /**
    * Attribute for imageUploader
    */
    public $bgVideo;

    /**
    * Temporary sign which used for saving images before model save
    * @var string
    */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%secondary_real_estate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'city_id', 'description'], 'required'],
            [['position', 'city_id'], 'integer'],
            [['description', 'opportunity_description', 'steps_description', 'steps_link_url', 'description_only_desktop'], 'string', 'max' => 1000],
            [['label', 'button_text', 'conditions_label', 'opportunity_black_label', 'opportunity_white_label', 'opportunity_button_text', 'apartments_label', 'steps_label', 'steps_link_text', 'articles_label'], 'string', 'max' => 255],
            [['published'], 'string', 'max' => 1],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['city_id'], 'exist', 'targetClass' => \common\models\City::class, 'targetAttribute' => 'id'],
            [['city_id'], 'unique'],
            [['sign'], 'string', 'max' => 255],
            [['articleIds'], 'safe']
         ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/SecondaryRealEstate', 'ID'),
            'label' => Yii::t('back/SecondaryRealEstate', 'Label'),
            'published' => Yii::t('back/SecondaryRealEstate', 'Published'),
            'position' => Yii::t('back/SecondaryRealEstate', 'Position'),
            'created_at' => Yii::t('back/SecondaryRealEstate', 'Created At'),
            'updated_at' => Yii::t('back/SecondaryRealEstate', 'Updated At'),
            'description' => Yii::t('back/SecondaryRealEstate', 'Description'),
            'description_only_desktop' => Yii::t('back/SecondaryRealEstate', 'Sub-description'),
            'button_text' => Yii::t('back/SecondaryRealEstate', 'Top button text'),
            'conditions_label' => Yii::t('back/SecondaryRealEstate', 'Conditions label'),
            'opportunity_black_label' => Yii::t('back/SecondaryRealEstate', 'Opportunity black label'),
            'opportunity_white_label' => Yii::t('back/SecondaryRealEstate', 'Opportunity white label'),
            'opportunity_description' => Yii::t('back/SecondaryRealEstate', 'Opportunity description'),
            'opportunity_button_text' => Yii::t('back/SecondaryRealEstate', 'Opportunity buton text'),
            'apartments_label' => Yii::t('back/SecondaryRealEstate', 'Apartments label'),
            'steps_label' => Yii::t('back/SecondaryRealEstate', 'Steps label'),
            'steps_description' => Yii::t('back/SecondaryRealEstate', 'Steps description'),
            'steps_link_text' => Yii::t('back/SecondaryRealEstate', 'Steps link text'),
            'steps_link_url' => Yii::t('back/SecondaryRealEstate', 'Steps link url'),
            'articles_label' => Yii::t('back/SecondaryRealEstate', 'Articles label'),
            'city_id' => Yii::t('back/SecondaryRealEstate', 'City'),
            'bgImage' => Yii::t('back/SecondaryRealEstate', 'Background image'),
            'bgVideo' => Yii::t('back/SecondaryRealEstate', 'Background video'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::class,
                'defaultFieldForTitle' => 'label',
            ],
            'manyToManyRelation' => [
                'class' => ManyToManyBehavior::class,
                'currentObj' => $this,
                'currentRelatedColumn' => 'secondary_real_estate_id',
                'manyToManyConfig' => [
                    'articles' => [
                        'attribute' => 'articleIds',
                        'valueAttribute' => 'articleIdValues',
                        'relatedColumn' => 'article_id',
                        'linkTableName' => ArticleInSecondaryRealEstate::tableName(),
                        'relatedObjTableName' => Article::tableName(),
                        'isStringReturn' => false
                    ],
                ]
            ],
        ];
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/SecondaryRealEstate', 'Secondary Real Estate');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    'published:boolean',
                    'position',
                    // 'description:ntext',
                    'button_text',
                    'conditions_label',
                    'opportunity_black_label',
                    // 'opportunity_white_label',
                    // 'opportunity_description:ntext',
                    // 'opportunity_button_text',
                    // 'apartments_label',
                    // 'steps_label',
                    // 'steps_description:ntext',
                    // 'steps_link_text:url',
                    // 'steps_link_url:url',
                    // 'articles_label',
                    ['class' => StylingActionColumn::class],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'city_id',
                    'label',
                    'published:boolean',
                    'position',
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    'description_only_desktop',
                    'button_text',
                    'conditions_label',
                    'opportunity_black_label',
                    'opportunity_white_label',
                    [
                        'attribute' => 'opportunity_description',
                        'format' => 'html',
                    ],
                    'opportunity_button_text',
                    'apartments_label',
                    'steps_label',
                    [
                        'attribute' => 'steps_description',
                        'format' => 'html',
                    ],
                    'steps_link_text:url',
                    'steps_link_url:url',
                    'articles_label',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new SecondaryRealEstateSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                Yii::t('back/SecondaryRealEstate', 'Main') => [
                    'city_id' => StdInput::citySelect(),
                    'label' => StdInput::text(),
                    'description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'description_only_desktop' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'options' => [
                            'maxlength' => true,
                        ],
                        'hint' => Yii::t('back/SecondaryRealEstate', 'Visible only on desktop'),
                    ],
                    'button_text' => StdInput::text(),
                    'bgImage' => StdInput::imageUpload(\common\models\SecondaryRealEstate::SAVE_ATTRIBUTE_BG_IMAGE),
                    'bgVideo' => StdInput::videoUpload(\common\models\SecondaryRealEstate::SAVE_ATTRIBUTE_BG_VIDEO),
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
                Yii::t('back/SecondaryRealEstate', 'Best conditions') => [
                    'conditions_label' => StdInput::text(),
                    $this->getRelatedFormConfig()['conditions']
                ],
                Yii::t('back/SecondaryRealEstate', 'Opportunity') => [
                    'opportunity_black_label' => StdInput::text(),
                    'opportunity_white_label' => StdInput::text(),
                    'opportunity_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'options' => [
                            'maxlength' => true,
                        ]
                    ],
                    'opportunity_button_text' => StdInput::text(),
                ],
                Yii::t('back/SecondaryRealEstate', 'Apartments') => [
                    'apartments_label' => StdInput::text(),
                ],
                Yii::t('back/SecondaryRealEstate', 'Steps') => [
                    'steps_label' => StdInput::text(),
                    'steps_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'steps_link_text' => StdInput::text(),
                    'steps_link_url' => StdInput::text(),
                    $this->getRelatedFormConfig()['steps']
                ],
                Yii::t('back/SecondaryRealEstate', 'Articles') => [
                    'articles_label' => StdInput::text(),
                    'articleIds' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'label' => $this->getAttributeLabel('articleIds'),
                        'value' => Select2Helper::getAjaxSelect2Widget(
                            $this,
                            'articleIds',
                            static::getArticlesDictionaryUrl(),
                            $this->articleIdValues,
                            [
                                'multiple' => true,
                            ]
                        ),
                        'hint' => Yii::t('back/SecondaryRealEstate', 'Up to 3 articles')
                    ],
                ],
                Yii::t('back/SecondaryRealEstate', 'Menu') => [
                    $this->getRelatedFormConfig()['menus'],
                ]
            ],
        ];

        return $config;
    }

    /**
     * @return array
     */
    public function getRelatedFormConfig()
    {
        return [
            'conditions' => [
                'relation' => 'conditions'
            ],
            'steps' => [
                'relation' => 'steps'
            ],
            'menus' => [
                'relation' => 'menus'
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConditions()
    {
        return $this->hasMany(SecondaryRealEstateCondition::class, ['secondary_real_estate_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSteps()
    {
        return $this->hasMany(SecondaryRealEstateStep::class, ['secondary_real_estate_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(SecondaryRealEstateMenu::class, ['secondary_real_estate_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return string
     */
    public static function getArticlesDictionaryUrl()
    {
        return '/resale/secondary-real-estate/articles';
    }

    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
