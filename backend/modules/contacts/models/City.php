<?php

namespace backend\modules\contacts\models;

use yii\behaviors\TimestampBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use conquer\codemirror\CodemirrorAsset;
use conquer\codemirror\CodemirrorWidget;
use common\components\model\ActiveRecord;
use backend\helpers\Select2Helper;
use backend\components\FormBuilder;
use backend\components\BackendModel;
use backend\components\StylingActionColumn;
use backend\validators\InputScriptValidator;
use backend\validators\EmailsInStringValidator;
use backend\modules\request\models\CustomPopup;

/**
 * This is the model class for table "{{%city}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property integer $sxgeo_city_id
 * @property integer $is_default
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $view_count
 * @property string $common_script
 * @property string $head_script
 * @property string $phone
 * @property int $custom_popup_id
 * @property string $link_ig
 * @property string $link_ok
 * @property string $link_vk
 * @property string $link_fb
 * @property string $notification_emails
 * @property integer $show_prices
 */
class City extends ActiveRecord implements BackendModel
{
    public static function tableName()
    {
        return '{{%city}}';
    }

    public function rules()
    {
        return [
            [['label', 'alias'], 'required'],
            [['is_default', 'published', 'position', 'sxgeo_city_id', 'view_count', 'custom_popup_id', 'show_prices'], 'integer'],
            [['label', 'alias', 'phone', 'link_ig', 'link_ok', 'link_vk', 'link_fb', 'link_itunes', 'link_play'], 'string', 'max' => 255],
            [['common_script', 'notification_emails', 'head_script'], 'string'],
            [['link_ig', 'link_ok', 'link_vk', 'link_fb', 'link_itunes', 'link_play'], 'url'],
            [['common_script', 'head_script'], InputScriptValidator::class],
            [['is_default'], 'default', 'value' => 0],
            [['published', 'show_prices'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['notification_emails'], 'trim'],
            [['notification_emails'], EmailsInStringValidator::class],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'city'),
            'label' => bt('Label', 'city'),
            'alias' => bt('Alias', 'city'),
            'is_default' => bt('Is default', 'city'),
            'sxgeo_city_id' => bt('Relation with auto-detect city', 'city'),
            'published' => bt('Published', 'city'),
            'position' => bt('Position', 'city'),
            'created_at' => bt('Created At', 'city'),
            'updated_at' => bt('Updated At', 'city'),
            'common_script' => bt('Common scripts', 'city'),
            'head_script' => bt('Head scripts', 'city'),
            'phone' => bt('Phone', 'city'),
            'custom_popup_id' => bt('Custom popup', 'city'),
            'link_ig' => bt('Social instagram link', 'city'),
            'link_ok' => bt('Social odnoklasniki link', 'city'),
            'link_vk' => bt('Social vkontakte link', 'city'),
            'link_fb' => bt('Social facebook link', 'city'),
            'link_itunes' => bt('ITunes link', 'city'),
            'link_play' => bt('GPlay link', 'city'),
            'notification_emails' => bt('Notification emails', 'city'),
            'show_prices' => bt('Show prices', 'city'),
        ];
    }

    public function attributeHints()
    {
        return [
            'common_script' => 'Должно начинаться с <code>&lt;script</code> и заканчиваться <code>script&gt;</code>',
            'head_script' => 'Должно начинаться с <code>&lt;script&gt;</code> и заканчиваться <code>&lt;/script&gt;</code>'
        ];
    }

    public function getSxgeoCity()
    {
        return $this->hasOne(SxgeoCities::class, ['id' => 'sxgeo_city_id']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'seo' => MetaTagBehavior::class,
        ];
    }

    public function getTitle()
    {
        return bt('City', 'city');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    'alias',
                    'is_default:boolean',
                    'published:boolean',
                    'show_prices:boolean',
                    'position',
                    ['class' => StylingActionColumn::class],
                ];
            case 'view':
                return [
                    'id',
                    'label',
                    'alias',
                    'is_default:boolean',
                    'published:boolean',
                    'show_prices:boolean',
                    'position',
                    'notification_emails:ntext',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new CitySearch();
    }

    public static function getSxgeoCityUrl()
    {
        $clearText = bt('Choose auto-detect city', 'city');

        return '/contacts/sxgeo-cities/get-select-items?showNotChoose=1&clearText='.$clearText;
    }

    public function generateSxgeoCitySelect2Value()
    {
        if ((isset($this->sxgeo_city_id)) && (isset($this->sxgeoCity))) {
            return [$this->sxgeo_city_id => $this->sxgeoCity->name_ru];
        } else {
            return [0 => bt('Choose auto-detect city', 'city')];
        }
    }

    public function getFormConfig()
    {
        return [
            'label' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true], //'class' => 's_name form-control'
            ],
            'alias' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true], //'class' => 's_alias form-control'
            ],
            'phone' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'sxgeo_city_id' => [
                'type' => FormBuilder::INPUT_RAW,
                'label' => $this->getAttributeLabel('sxgeo_city_id'),
                'value' => Select2Helper::getAjaxSelect2Widget(
                    $this,
                    'sxgeo_city_id',
                    static::getSxgeoCityUrl(),
                    $this->generateSxgeoCitySelect2Value(),
                    ['multiple' => false],
                    false
                )
            ],
            'link_ig' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'link_ok' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'link_vk' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'link_fb' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'link_itunes' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'link_play' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'common_script' => [
                'type' => FormBuilder::INPUT_RAW,
                'value' => CodemirrorWidget::widget([
                    'model' => $this,
                    'attribute' => 'common_script',
                    'preset'=> 'javascript',
                    'assets' => [CodemirrorAsset::THEME_SOLARIZED],
                    'settings' => ['theme' => 'solarized'],
                    'options' => ['rows' => 20],
                ]),
            ],
            'head_script' => [
                'type' => FormBuilder::INPUT_RAW,
                'value' => CodemirrorWidget::widget([
                    'model' => $this,
                    'attribute' => 'head_script',
                    'preset'=> 'javascript',
                    'assets' => [CodemirrorAsset::THEME_SOLARIZED],
                    'settings' => ['theme' => 'solarized'],
                    'options' => ['rows' => 20],
                ]),
            ],
            'custom_popup_id' => [
                'type' => FormBuilder::INPUT_DROPDOWN_LIST,
                'items' => CustomPopup::getItems('id', 'white_label'),
                'options' => ['prompt' => '--'],
            ],
            'position' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
            ],
            'notification_emails' => [
                'type' => FormBuilder::INPUT_TEXTAREA,
                'options' => ['rows' => 6]
            ],
            'is_default' => [
                'type' => FormBuilder::INPUT_CHECKBOX,
            ],
            'show_prices' => [
                'type' => FormBuilder::INPUT_CHECKBOX,
            ],
            'published' => [
                'type' => FormBuilder::INPUT_CHECKBOX,
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->sxgeo_city_id == 0){
            $this->sxgeo_city_id = null;
        }
        return parent::beforeSave($insert);
    }
}
