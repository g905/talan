<?php

namespace backend\modules\contacts\models;

use backend\validators\EmailsInStringValidator;
use yii\behaviors\TimestampBehavior;
use common\models\EntityToFile;
use common\components\model\ActiveRecord;
use common\models\Manager as CommonManager;
use backend\components\FormBuilder;
use backend\components\BackendModel;
use backend\components\ImperaviContent;
use backend\components\StylingActionColumn;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;

/**
 * This is the model class for table "{{%manager}}".
 *
 * @property integer $id
 * @property string $fio
 * @property string $job_position
 * @property string $phone
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $notification_emails
 * @property string $contact_email
 */
class Manager extends ActiveRecord implements BackendModel
{
    public $photo;
    public $sign;

    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = app()->security->generateRandomString();
        }
    }

    public static function tableName()
    {
        return '{{%manager}}';
    }

    public function rules()
    {
        return [
            [['fio'], 'required'],
            [['published'], 'integer'],
            [['fio', 'job_position', 'phone', 'contact_email', 'sign'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['contact_email'], 'email'],
            [['notification_emails'], 'string'],
            [['notification_emails'], 'trim'],
            ['notification_emails', EmailsInStringValidator::class],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'manager'),
            'fio' => bt('Fio', 'manager'),
            'job_position' => bt('Job position', 'manager'),
            'phone' => bt('phone', 'manager'),
            'published' => bt('Published', 'manager'),
            'created_at' => bt('Created At', 'manager'),
            'updated_at' => bt('Updated At', 'manager'),
            'photo' => bt('photo', 'manager'),
            'notification_emails' => bt('Notification emails', 'manager'),
            'contact_email' => bt('Contact email', 'manager'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getTitle()
    {
        return bt('Manager', 'manager');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'fio',
                    'job_position',
                    'phone',
                    'published:boolean',
                    ['class' => StylingActionColumn::class],
                ];
            case 'view':
                return [
                    'id',
                    'fio',
                    'job_position',
                    'phone',
                    'published:boolean',
                    'notification_emails:ntext',
                    'contact_email:email'
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new ManagerSearch();
    }

    public function getFormConfig()
    {
        return [
            'fio' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'job_position' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'phone' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'photo' => [
                'type' => FormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'photo',
                    'saveAttribute' => CommonManager::SAVE_ATTRIBUTE_PHOTO,
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'notification_emails' => [
                'type' => FormBuilder::INPUT_TEXTAREA,
                'options' => ['rows' => 6]
            ],
            'contact_email' => [
                'type' => FormBuilder::INPUT_TEXT,
                'option' => [
                    'maxlength' => true,
                ],
                'hint' => bt('Used on Team page mailto buttons', 'manager'),
            ],
            'published' => [
                'type' => FormBuilder::INPUT_CHECKBOX,
            ],
            'sign' => [
                'type' => FormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
