<?php

namespace backend\modules\contacts\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%sxgeo_cities}}".
 *
 * @property integer $id
 * @property integer $region_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $lat
 * @property string $lon
 * @property string $okato
 */
class SxgeoCities extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sxgeo_cities}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'region_id', 'name_ru', 'name_en', 'lat', 'lon', 'okato'], 'required'],
            [['id', 'region_id'], 'integer'],
            [['lat', 'lon'], 'number'],
            [['name_ru', 'name_en'], 'string', 'max' => 128],
            [['okato'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/sxgeo-cities', 'ID'),
            'region_id' => Yii::t('back/sxgeo-cities', 'Region ID'),
            'name_ru' => Yii::t('back/sxgeo-cities', 'Name Ru'),
            'name_en' => Yii::t('back/sxgeo-cities', 'Name En'),
            'lat' => Yii::t('back/sxgeo-cities', 'Lat'),
            'lon' => Yii::t('back/sxgeo-cities', 'Lon'),
            'okato' => Yii::t('back/sxgeo-cities', 'Okato'),
        ];
    }

    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/sxgeo-cities', 'Sxgeo Cities');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'region_id',
                    'name_ru',
                    'name_en',
                    'lat',
                    'lon',
                    'okato',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'region_id',
                    'name_ru',
                    'name_en',
                    'lat',
                    'lon',
                    'okato',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new SxgeoCitiesSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'region_id' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
            ],
            'name_ru' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'name_en' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'lat' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'lon' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'okato' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
        ];

        return $config;
    }


}
