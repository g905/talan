<?php

namespace backend\modules\contacts\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CitySearch represents the model behind the search form about `City`.
 */
class CitySearch extends City
{
    public function rules()
    {
        return [
            [['id', 'is_default', 'published', 'position'], 'integer'],
            [['label', 'alias', 'notification_emails'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = CitySearch::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_default' => $this->is_default,
            'published' => $this->published,
            'position' => $this->position,
        ])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'notification_emails', $this->notification_emails]);

        return $dataProvider;
    }
}
