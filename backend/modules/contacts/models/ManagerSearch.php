<?php

namespace backend\modules\contacts\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ManagerSearch represents the model behind the search form about `Manager`.
 */
class ManagerSearch extends Manager
{
    public function rules()
    {
        return [
            [['id', 'published'], 'integer'],
            [['fio', 'job_position', 'phone', 'notification_emails'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = ManagerSearch::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id, 'published' => $this->published])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'job_position', $this->job_position])
            ->andFilterWhere(['like', 'notification_emails', $this->notification_emails])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}
