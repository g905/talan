<?php

namespace backend\modules\contacts\controllers;

use backend\components\BackendController;
use backend\modules\contacts\models\SxgeoCities;
use yii\db\Query;
use yii\web\Response;

/**
 * SxgeoCitiesController implements the CRUD actions for SxgeoCities model.
 */
class SxgeoCitiesController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return SxgeoCities::className();
    }

    public function actionGetSelectItems($search = null, $id = null, $showNotChoose = false, $clearText = null, $q = null)
    {
        if (!isset($clearText)){
            $clearText = \Yii::t('back/product', 'Choose product');
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];


        if (is_null($search)) {

            if (isset($q)){
                $search = $q;
            }
            else{
                $search = '';
            }

        }

        $query = new Query();
        $query->select('id, name_ru AS text')
            ->from(SxgeoCities::tableName())
            ->where(['like', 'name_ru', $search])
            ->limit(5);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => SxgeoCities::find($id)->name];
        }

        if ($showNotChoose){
            $notChooseValue = ['id' => '0', 'text' => $clearText];
            array_unshift($out['results'] , $notChooseValue);
        }
        return $out;
    }
}
