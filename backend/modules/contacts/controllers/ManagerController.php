<?php

namespace backend\modules\contacts\controllers;

use backend\components\BackendController;
use backend\modules\contacts\models\Manager;

/**
 * ManagerController implements the CRUD actions for Manager model.
 */
class ManagerController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Manager::className();
    }
}
