<?php

namespace backend\modules\contacts\controllers;

use backend\components\BackendController;
use backend\modules\contacts\models\City;

/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return City::className();
    }
}
