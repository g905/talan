<?php

use yii\helpers\Html;
use backend\components\FormBuilder;

/**
 * @var \backend\modules\imagesUpload\models\FileMetaData $model
 * @var string $action
 * @var array $translationModels
 */
?>

<?= Html::errorSummary(merge([$model], $translationModels), ['class' => 'alert alert-danger']) ?>
<?php /** @var FormBuilder $form */
$form = FormBuilder::begin([
    'action' => $action,
    'enableClientValidation' => false,
    'options' => ['id' => 'meta-data-form', 'enctype' => 'multipart/form-data', 'data' => ['pjax' => true]],
]); ?>

<?= $form->prepareRows($model, $model->getFormConfig(), $translationModels) ?>

<div class="row">
    <div class="col-xs-12">
        <?= Html::submitButton(bt('Save', 'fileMetaData'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(bt('Cancel', 'fileMetaData'), '#', ['class' => 'btn btn-warning cancel-crop']) ?>
    </div>
</div>

<?php FormBuilder::end(); ?>
