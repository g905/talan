<?php

namespace backend\modules\imagesUpload\controllers;

use backend\components\BackendController;
use backend\modules\imagesUpload\models\FileMetaData;
use common\components\model\Translateable;
use Yii;

/**
 * MetaDataController implements the CRUD actions for FileMetaData model.
 */
class MetaDataController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return FileMetaData::className();
    }

    public function actionGenerateForm($id): string
    {
        if (!Yii::$app->getRequest()->isAjax) {
            return $this->redirect('index');
        }

        if (!$id || !is_numeric($id)) {
            return $this->renderAjax('//templates/no-file');
        }

        $model = FileMetaData::getModelByFileId((int)$id);

        return $this->_renderMetaDataForm($model);
    }

    public function actionSaveForm($id): string
    {
        if (!Yii::$app->getRequest()->isAjax) {
            return $this->redirect('index');
        }

        $model = FileMetaData::getModelByFileId((int)$id);

        if ($this->loadModels($model) && $model->save()) {
            return $this->renderAjax('//templates/meta-data-save-complete');
        }

        return $this->_renderMetaDataForm($model);
    }

    /**
     * @param FileMetaData $model
     *
     * @return string
     */
    private function _renderMetaDataForm(FileMetaData $model): string
    {
        return $this->renderAjax('meta-data-form', [
            'model'             => $model,
            'action'            => $model->getMetaDataSaveUrl(),
            'translationModels' => $model instanceof Translateable ? $model->getTranslationModels() : [],
        ]);
    }
}
