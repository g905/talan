<?php
/**
 * @var string $url
 */
?>
<a class="meta-data-link btn btn-xs btn-default pull-right " data-toggle="modal" href="<?= $url ; ?>" {dataKey} data-target=".modal-hidden">
    <i class="glyphicon glyphicon-tag file-icon-large text-success"></i>
</a>
