<?php

namespace backend\modules\imagesUpload\models;

use backend\components\TranslateableTrait;
use backend\modules\imagesUpload\helpers\ImageUploadUrlHelper;
use common\components\model\Translateable;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

class FileMetaData extends \common\models\FileMetaData implements Translateable
{
    use TranslateableTrait;

    /**
     * @param int $fileId
     *
     * @return \backend\modules\imagesUpload\models\FileMetaData
     */
    public static function getModelByFileId(int $fileId): FileMetaData
    {
        $model = FileMetaData::find()
            ->andWhere(['file_id' => $fileId])
            ->one();

        if ($model === null) {
            $model = new FileMetaData();
            $model->file_id = $fileId;
        }

        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alt'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'alt'     => Yii::t('back/fileMetaData', 'Text'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            ['timestamp' => ['class' => TimestampBehavior::className()]]
        );
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        return [
            'alt' => [
                'type'    => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
        ];
    }

    public function getMetaDataSaveUrl()
    {
        return ImageUploadUrlHelper::getMetaDataFormSaveUrl(['id' => $this->file_id]);
    }


}
