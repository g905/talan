<?php
namespace backend\modules\configuration\components;

use backend\components\BackendController;
use common\helpers\LanguageHelper;
use Yii;

/**
 * ConfigurationController implements the CRUD actions for Configuration model.
 */
abstract class ConfigurationController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function actionIndex()
    {
       return $this->actionView();
    }

    /**
     * @param null $id
     *
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id = null)
    {
        $class = $this->getModelClass();
        /** @var ConfigurationModel $model */
        $model = new $class();
        $config = $this->getRelatedFormActionConfig($model);
        if (!empty($config)) {
            return $this->relatedFormAction($model, $config);
        }

        if (Yii::$app->request->isPost) {
            $this->loadModels($model);

            if ($model->save()) {
                return $this->redirect(['view']);
            }
        }

        return $this->render('@app/modules/configuration/views/configuration/templates/update', [
            'model' => $model,
        ]);
    }

    /**
     * @param null $id
     *
     * @return mixed
     */
    public function actionView($id = null)
    {
        $class = $this->getModelClass();
        /** @var ConfigurationModel $model */
        $model = new $class();
        return $this->render('@app/modules/configuration/views/configuration/templates/view', [
            'model' => $model,
        ]);
    }

    /**
     * @param ConfigurationModel $model
     *
     * @return bool
     */
    public function loadModels($model)
    {
        $loaded = true;

        $models = $model->getModels();

        foreach ($models as $key => $item) {
            if ($item instanceof \common\components\model\Translateable) {
                $languages = LanguageHelper::getLanguageModels();

                $items = [];
                foreach ($languages as $language) {
                    if ($language->locale === LanguageHelper::getDefaultLanguage()->locale) {
                        continue;
                    }
                    $items[$language->locale]= $item->getTranslation($language->locale);
                }
                if ($items) {
                    $loaded &= ConfigurationModel::loadMultiple($items, Yii::$app->request->post(), null, $key);
                }
            }
        }

        return $loaded & ConfigurationModel::loadMultiple($models, Yii::$app->request->post());
    }
}
