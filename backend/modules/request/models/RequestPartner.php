<?php

namespace backend\modules\request\models;

use backend\modules\request\helpers\SearchHelper;
use Yii;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;

/**
 * This is the model class for table "partner_request".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $referer
 * @property integer $created_at
 * @property integer $updated_at
 */
class RequestPartner extends ActiveRecord implements BackendModel
{

    public static function tableName()
    {
        return 'partner_request';
    }

    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['name', 'phone'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/request', 'ID'),
            'name' => Yii::t('back/request', 'Name'),
            'phone' => Yii::t('back/request', 'Phone'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    public function getTitle()
    {
        return bt('Request Partner', 'app');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    'name',
                    'phone',
                    SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    'name',
                    'phone',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new RequestPartnerSearch();
    }

    public function getFormConfig()
    {
        $config = [
            'name' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'phone' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
        ];

        return $config;
    }


}
