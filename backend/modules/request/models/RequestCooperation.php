<?php

namespace backend\modules\request\models;

use yii\behaviors\TimestampBehavior;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%request_cooperation}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $company_name
 * @property string $chief_phone
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 */
class RequestCooperation extends ActiveRecord implements BackendModel
{
    public static function tableName()
    {
        return '{{%request_cooperation}}';
    }

    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['name', 'phone', 'email', 'company_name', 'chief_phone'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'targetClass' => City::class, 'targetAttribute' => 'id'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'request-cooperation'),
            'city_id' => bt('City ID', 'request-cooperation'),
            'name' => bt('Name', 'request-cooperation'),
            'phone' => bt('Phone', 'request-cooperation'),
            'email' => bt('Email', 'request-cooperation'),
            'company_name' => bt('Company Name', 'request-cooperation'),
            'chief_phone' => bt('Chief Phone', 'request-cooperation'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getTitle()
    {
        return bt('Request Cooperation', 'app');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    StdColumn::city(),
                    'name',
                    'phone',
                    'email:email',
                    'company_name',
                    'chief_phone',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    StdColumn::city(),
                    'name',
                    'phone',
                    'email:email',
                    'company_name',
                    'chief_phone',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new RequestCooperationSearch();
    }

    public function getFormConfig()
    {
        return [
            'city_id' => StdInput::citySelect(),
            'name' => StdInput::text(),
            'phone' => StdInput::text(),
            'email' => StdInput::text(),
            'company_name' => StdInput::text(),
            'chief_phone' => StdInput::text(),
        ];
    }
}
