<?php

namespace backend\modules\request\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\request\helpers\SearchHelper;

/**
 * RequestInstallmentPlanSearch represents the model behind the search form about `RequestInstallmentPlan`.
 */
class RequestInstallmentPlanSearch extends RequestInstallmentPlan
{
    public function rules()
    {
        return [
            [['id', 'city_id'], 'integer'],
            [['name', 'phone', 'created_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = self::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);
        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere(['id' => $this->id, 'city_id' => $this->city_id]);
            SearchHelper::filterByDateRange($this->created_at, 'created_at', $query);
            $query->andFilterWhere(['like', 'name', $this->name]);
            $query->andFilterWhere(['like', 'phone', $this->phone]);
        }

        return $dataProvider;
    }
}
