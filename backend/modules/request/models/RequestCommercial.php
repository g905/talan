<?php

namespace backend\modules\request\models;

use Yii;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\components\StylingActionColumn;
use backend\modules\request\helpers\SearchHelper;
use backend\modules\apartment\models\ApartmentComplex;

/**
 * Class RequestCommercial
 * @package backend\modules\request\models
 */
class RequestCommercial extends \common\models\RequestCommercial implements BackendModel
{
    public function rules()
    {
        return [
            [['city_id', 'ac_id'], 'integer'],
            [['name', 'phone'], 'required'],
            [['name', 'phone'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/request-commercial', 'ID'),
            'city_id' => Yii::t('back/request-commercial', 'City'),
            'ac_id' => Yii::t('back/request-commercial', 'Apartment complex'),
            'name' => Yii::t('back/request-commercial', 'Name'),
            'phone' => Yii::t('back/request-commercial', 'Phone'),
            'created_at' => Yii::t('back/request-commercial', 'Created At'),
            'updated_at' => Yii::t('back/request-commercial', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => ['class' => \yii\behaviors\TimestampBehavior::class],
        ];
    }

    public function getTitle()
    {
        return Yii::t('app', 'Request commercial');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return '<b>' . optional($model->city)->label . '</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any city')],
                    ],
                    [
                        'attribute' => 'ac_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->apartmentComplex)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(ApartmentComplex::find()->orderBy('city_id')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any complex')],
                    ],
                    SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
                    'name',
                    'phone',
                    ['class' => StylingActionColumn::class],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'name',
                    'phone',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new RequestCommercialSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'city_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => City::getItems(),
                'options' => ['prompt' => ''],
            ],
            'ac_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => ApartmentComplex::getItems(),
                'options' => ['prompt' => ''],
            ],
            'name' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'phone' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
        ];

        return $config;
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
