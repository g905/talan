<?php

namespace backend\modules\request\models;

use Yii;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use backend\components\FormBuilder;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\components\StylingActionColumn;
use backend\modules\request\helpers\SearchHelper;

/**
 * Class RequestInstallmentPlan
 *
 * @package backend\modules\request\models
 */
class RequestInstallmentPlan extends \common\models\RequestInstallmentPlan implements BackendModel
{
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['name', 'phone'], 'required'],
            [['name', 'phone'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    public function getTitle()
    {
        return bt('Request installment plan', 'request-instalment-plan');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->city)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any city')],
                    ],
                    SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
                    'name',
                    'phone',
                    ['class' => StylingActionColumn::class],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'name',
                    'phone',
                ];
            break;
        }

        return [];
    }

    public function getSearchModel()
    {
        return new RequestInstallmentPlanSearch();
    }

    public function getFormConfig()
    {
        return [
            'city_id' => [
                'type' => FormBuilder::INPUT_DROPDOWN_LIST,
                'items' => City::getItems(),
                'options' => ['prompt' => ''],
            ],
            'name' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'phone' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
