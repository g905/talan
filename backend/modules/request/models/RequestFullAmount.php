<?php
namespace backend\modules\request\models;

use common\helpers\MailerHelper;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use common\models\City;
use \kartik\grid\GridView;
use backend\components\BackendModel;
use backend\components\StylingActionColumn;
use backend\modules\request\helpers\SearchHelper;

class RequestFullAmount extends \common\components\model\ActiveRecord implements BackendModel {

	public $iAgree;
	public $cityName;
    public $apartment_id;
    public $apartment_complex_id;

	public static function tableName()
	{
		return '{{%request_full_amount}}';
	}

	public function rules() {
		return [
			[['city_id'], 'integer'],
			[['name', 'phone',], 'required'],
			[['iAgree'], 'safe'],
            [['apartment_id', 'apartment_complex_id'], 'safe'],
			[['name', 'phone', 'cityName',], 'string', 'max' => 240,],
			[['city_id'], 'exist', 'targetClass' => \common\models\City::class, 'targetAttribute' => 'id'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('back/request-fullamount', 'ID'),
			'city_id' => Yii::t('back/request-fullamount', 'City'),
			'ac_id' => Yii::t('back/request-fullamount', 'Apartment complex'),
			'name' => Yii::t('back/request-fullamount', 'Name'),
			'phone' => Yii::t('back/request-fullamount', 'Phone'),
			'created_at' => Yii::t('back/request-fullamount', 'Created At'),
			'updated_at' => Yii::t('back/request-fullamount', 'Updated At'),
		];
	}

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => \yii\behaviors\TimestampBehavior::class,
			],
		];
	}

	public function getCity() {
		return $this->hasOne(City::class, ['id' => 'city_id']);
	}

	public function getTitle() {
		// Заявки на всю сумму
		return Yii::t('back/menu', 'Request for full amount');
	}

	public function getColumns($page)
	{
		switch ($page) {
			case 'index':
				return [
					['class' => 'yii\grid\SerialColumn'],

					'id',
					[
						'attribute' => 'city_id',
						'format' => 'raw',
						'value' => function ($model) {
							/** @var self $model */
							return optional($model->city)->label ?? null;
						},
						'filterType' => GridView::FILTER_SELECT2,
						'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
						'filterWidgetOptions' => [
							'pluginOptions' => ['allowClear' => true],
						],
						'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any city')],
					],
					'name',
					'phone',
					SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
					['class' => StylingActionColumn::class],
				];
			case 'view':
				return [
					'id',
					//'city_id',
					'name',
					'phone',
				];
			default:
				return [];
		}
	}

	public function getSearchModel() {
		return new RequestFullAmountSearch();
	}

	public function getFormConfig()
	{

		$config = [
			'city_id' => [
				'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
				'items' => \common\models\City::getItems(),
				'options' => [
					'prompt' => '',
				],
			],
			'name' => [
				'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
				'options' => [
					'maxlength' => true,

				],
			],
			'phone' => [
				'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
				'options' => [
					'maxlength' => true,

				],
			],
		];
		return $config;
	}

	public function saveAndSendEmailAndCrm($sendEmail = true, $sendCrm = true) {

		if($this->save(true)) {
			// добавим город
			if (!empty($this->city->label)) {
				$this->cityName = $this->city->name;
			}
			// отправим почту
			if($sendEmail) {
				MailerHelper::sendNotificationEmail('Заявки Полная сумма', [
					'Имя' => $this->name,
					'Телефон' => $this->phone,
				]);
			}
			// подготовим данные для Битрикс
			if($sendCrm) {
				$bitrixArr = $this->toArray(['name', 'phone', 'cityName','apartment_id','apartment_complex_id']);

				app()->trigger('bitrixData', new Event([
					'sender' => [
						'title' => 'Полная сумма',
						'source_description' => 'заявка с сайта - Полная сумма',
						//'form' => $bitrixArr,
                        'form' => [
                            'name' => $this->name,
                            'phone' => $this->phone,
                            'cityName' => $this->cityName,
                            'comment' => $this->apartment_complex_id.", ".$this->apartment_id,
                        ]

					],
				]));
			}
			return true;
		}

		return false;
	}
}