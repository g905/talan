<?php

namespace backend\modules\request\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use kartik\grid\GridView;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\components\StylingActionColumn;
use backend\modules\request\helpers\SearchHelper;
use backend\modules\apartment\models\ApartmentComplex;

/**
 * Class SubscribeRequest
 * @package backend\modules\request\models
 */
class SubscribeRequest extends \common\models\SubscribeRequest implements BackendModel
{
    public function rules()
    {
        return [
            [['city_id', 'ac_id'], 'integer'],
            [['email'], 'required'],
            [['email'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'SubscribeRequest'),
            'city_id' => bt('City', 'SubscribeRequest'),
            'ac_id' => bt('Apartment complex', 'SubscribeRequest'),
            'email' => bt('Email', 'SubscribeRequest'),
            'created_at' => bt('Created At', 'SubscribeRequest'),
            'updated_at' => bt('Updated At', 'SubscribeRequest'),
        ];
    }

    public function behaviors()
    {
        return ['timestamp' => TimestampBehavior::class];
    }

    public function getTitle()
    {
        return Yii::t('app', 'Subscribe Request');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return '<b>' . optional($model->city)->label . '</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any city')],
                    ],
                    [
                        'attribute' => 'ac_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->apartmentComplex)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(ApartmentComplex::find()->orderBy('city_id')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any complex')],
                    ],
                    SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
                    'email:email',
                    ['class' => StylingActionColumn::class],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'email:email',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new SubscribeRequestSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        return [
            'city_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => City::getItems(),
                'options' => ['prompt' => ''],
            ],
            'ac_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => ApartmentComplex::getItems(),
                'options' => ['prompt' => ''],
            ],
            'email' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => ['maxlength' => true],
            ],
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
