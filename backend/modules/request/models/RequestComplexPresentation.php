<?php

namespace backend\modules\request\models;

use Yii;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\modules\request\helpers\SearchHelper;
use backend\modules\apartment\models\ApartmentComplex;
use backend\modules\apartment\models\ApartmentBuilding;

/**
 * This is the model class for table "{{%request_excursion}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $ac_id
 * @property integer $ab_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property ApartmentComplex $apartmentComplex
 * @property ApartmentBuilding $apartmentBuilding
 */
class RequestComplexPresentation extends \common\models\RequestComplexPresentation implements BackendModel
{
    public function rules()
    {
        return [
            [['ac_id', 'ab_id', 'city_id'], 'integer'],
            [['name', 'email'], 'required'],
            [['email'], 'email'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
            [['ac_id', 'ab_id'], 'exist', 'targetClass' => \common\models\ApartmentComplex::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/request-presentation', 'ID'),
            'city_id' => Yii::t('back/request-presentation', 'City'),
            'ac_id' => Yii::t('back/request-presentation', 'Apartment Complex'),
            'ab_id' => Yii::t('back/request-presentation', 'Apartment Building'),
            'name' => Yii::t('back/request-presentation', 'Name'),
            'phone' => Yii::t('back/request-presentation', 'Phone'),
            'email' => Yii::t('back/request-presentation', 'Email'),
            'created_at' => Yii::t('back/request-presentation', 'Created At'),
            'updated_at' => Yii::t('back/request-presentation', 'Updated At'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => ['class' => \yii\behaviors\TimestampBehavior::class],
        ];
    }

    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'ac_id']);
    }

    public function getApartmentBuilding()
    {
        return $this->hasOne(ApartmentBuilding::class, ['id' => 'ab_id']);
    }

    public function getTitle()
    {
        return Yii::t('back/request-presentation', 'Request Excursion');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->city)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    [
                        'attribute' => 'ac_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var RequestComplexPresentation $model */
                            return optional($model->apartmentComplex)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(ApartmentComplex::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any complex')],
                    ],
                    [
                        'attribute' => 'ab_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var RequestComplexPresentation $model */
                            return optional($model->apartmentBuilding)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(ApartmentBuilding::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any building')],
                    ],
                    SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
                    'name',
                    'phone',
                    'email',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'ac_id',
                    'ab_id',
                    'name',
                    'phone',
                    'email',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new RequestComplexPresentationSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'city_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => City::getItems(),
                'options' => ['prompt' => ''],
            ],
            'ac_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ApartmentComplex::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'ab_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ApartmentBuilding::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'name' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'phone' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'email' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
        ];

        return $config;
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
