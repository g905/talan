<?php

namespace backend\modules\request\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\request\helpers\SearchHelper;
use backend\modules\apartment\models\ApartmentComplex;

/**
 * RequestExcursionSearch represents the model behind the search form about `RequestExcursion`.
 */
class RequestExcursionSearch extends RequestExcursion
{
    public function rules()
    {
        return [
            [['id', 'city_id', 'apartment_id', 'ac_id','request_type'], 'integer'],
            [['name', 'phone', 'created_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $request = \Yii::$app->request;
        $type = $request->get('type');
        if(empty($type)){
            $type=1;
        }

        $query = RequestExcursionSearch::find()->where(["request_type"=>$type])->alias('re')->joinWith(['apartmentComplex']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere([
                're.id' => $this->id,
                're.city_id' => $this->city_id,
                ApartmentComplex::tableName() . '.id' => $this->ac_id,
                're.apartment_id' => $this->apartment_id,
            ]);
            SearchHelper::filterByDateRange($this->created_at, 're.created_at', $query);
            $query->andFilterWhere(['like', 're.name', $this->name]);
            $query->andFilterWhere(['like', 're.phone', $this->phone]);
        }

        return $dataProvider;
    }
}
