<?php

namespace backend\modules\request\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RequestCustomSearch represents the model behind the search form about `RequestCustom`.
 */
class CustomPopupSearch extends CustomPopup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'raise_time'], 'integer'],
            [['white_label', 'black_label', 'sub_label', 'label_placeholder', 'phone_placeholder', 'email_placeholder', 'agreement_text', 'button_label', 'url_param_value', 'is_default'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomPopupSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'raise_time' => $this->raise_time,
        ]);

        $query->andFilterWhere(['like', 'white_label', $this->white_label])
            ->andFilterWhere(['like', 'black_label', $this->black_label])
            ->andFilterWhere(['like', 'sub_label', $this->sub_label])
            ->andFilterWhere(['like', 'label_placeholder', $this->label_placeholder])
            ->andFilterWhere(['like', 'phone_placeholder', $this->phone_placeholder])
            ->andFilterWhere(['like', 'email_placeholder', $this->email_placeholder])
            ->andFilterWhere(['like', 'agreement_text', $this->agreement_text])
            ->andFilterWhere(['like', 'button_label', $this->button_label])
            ->andFilterWhere(['like', 'url_param_value', $this->url_param_value])
            ->andFilterWhere(['like', 'is_default', $this->is_default]);

        return $dataProvider;
    }
}
