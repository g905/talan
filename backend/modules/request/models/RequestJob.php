<?php

namespace backend\modules\request\models;

use backend\modules\request\helpers\SearchHelper;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use kartik\grid\GridView;
use backend\components\FormBuilder;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\components\StylingActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class RequestHypothec
 *
 * @package backend\modules\request\models
 */
class RequestJob extends \common\models\RequestJob implements BackendModel
{
    public function rules()
    {
        return [
            [['city_id', 'file_id'], 'integer'],
            [['name', 'phone', 'email'], 'required'],
            [['email'], 'email'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getTitle()
    {
        return bt('Request job', 'request-job');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->city)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any city')],
                    ],
                    SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
                    'name',
                    'phone',
                    'email',
                    [
                        'attribute' => 'file_id',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            return $model->file_id
                                ? Html::a('Резюме', FPM::originalSrc($model->file_id), ['data-pjax' => 0])
                                : null;
                        }
                    ],
                    ['class' => StylingActionColumn::class],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'name',
                    'phone',
                    'email',
                    [
                        'attribute' => 'file_id',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            return $model->file_id
                                ? Html::a('Резюме', FPM::originalSrc($model->file_id), ['data-pjax' => 0])
                                : null;
                        }
                    ],
                ];
            break;
        }

        return [];
    }

    public function getSearchModel()
    {
        return new RequestJobSearch();
    }

    public function getFormConfig()
    {
        return [
            'city_id' => [
                'type' => FormBuilder::INPUT_DROPDOWN_LIST,
                'items' => City::getItems(),
                'options' => ['prompt' => ''],
            ],
            'name' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'phone' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'email' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'file_id' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'prompt' => '',
                    'disabled' => 'disabled',
                    'value' => Url::base(true) . FPM::originalSrc($this->file_id),
                    'iconRightContent' => Html::a('Скачать резюме', FPM::originalSrc($this->file_id), ['data-pjax' => 0])
                ],
            ],
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
