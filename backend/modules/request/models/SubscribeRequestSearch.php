<?php

namespace backend\modules\request\models;

use backend\modules\request\helpers\SearchHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SubscribeRequestSearch represents the model behind the search form about `SubscribeRequest`.
 */
class SubscribeRequestSearch extends SubscribeRequest
{
    public function rules()
    {
        return [
            [['id', 'city_id', 'ac_id'], 'integer'],
            [['email', 'created_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = SubscribeRequestSearch::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere([
                'id' => $this->id,
                'city_id' => $this->city_id,
                'ac_id' => $this->ac_id,
            ]);
            SearchHelper::filterByDateRange($this->created_at, 'created_at', $query);
            $query->andFilterWhere(['like', 'email', $this->email]);
        }

        return $dataProvider;
    }
}
