<?php

namespace backend\modules\request\models;

use backend\modules\apartment\models\ApartmentBuilding;
use Yii;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\modules\request\helpers\SearchHelper;
use backend\modules\apartment\models\ApartmentComplex;

/**
 * Class RequestQuestion
 * @package backend\modules\request\models
 */
class RequestQuestion extends \common\models\RequestQuestion implements BackendModel
{
    public function rules()
    {
        return [
            [['city_id', 'ac_id', 'ab_id'], 'integer'],
            [['question'], 'string'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/request-question', 'ID'),
            'city_id' => Yii::t('back/request-question', 'city'),
            'ac_id' => Yii::t('back/request-question', 'apartment complex'),
            'ab_id' => Yii::t('back/request-question', 'Apartment building'),
            'name' => Yii::t('back/request-question', 'user name'),
            'phone' => Yii::t('back/request-question', 'user phone'),
            'email' => Yii::t('back/request-question', 'user email'),
            'question' => Yii::t('back/request-question', 'question text'),
            'created_at' => Yii::t('back/request-question', 'Created At'),
            'updated_at' => Yii::t('back/request-question', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    public function getTitle()
    {
        return Yii::t('back/request-question', 'Request Question');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return '<b>' . optional($model->city)->label . '</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    [
                        'attribute' => 'ac_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->apartmentComplex)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(ApartmentComplex::find()->orderBy('city_id')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any complex')],
                    ],
                    [
                        'attribute' => 'ab_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->apartmentBuilding)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(ApartmentBuilding::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any building')],
                    ],
                    SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
                    'name',
                    'phone',
                    'email:email',
                    // 'question:ntext',
                    ['class' => \backend\components\StylingActionColumn::class],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'name',
                    'phone',
                    'email:email',
                    [
                        'attribute' => 'question',
                        'format' => 'html',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new RequestQuestionSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'city_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => City::getItems(),
                'options' => ['prompt' => ''],
            ],
            'ac_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => ApartmentComplex::getItems(),
                'options' => ['prompt' => ''],
            ],
            'ab_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => ApartmentBuilding::getItems(),
                'options' => ['prompt' => ''],
            ],
            'name' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'phone' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'email' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'question' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::class,
                'options' => [
                    'model' => $this,
                    'attribute' => 'question',
                ]
            ],
        ];

        return $config;
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
