<?php

namespace backend\modules\request\models;

use backend\modules\request\helpers\SearchHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RequestCustomGuaranteeSearch represents the model behind the search form about `RequestCustomGuarantee`.
 */
class RequestCustomGuaranteeSearch extends RequestCustomGuarantee
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city', 'file_id'], 'integer'],
            [['name', 'phone', 'email', 'comment', 'created_at', 'pdf_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestCustomGuaranteeSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere(['id' => $this->id, 'city' => $this->city, 'file_id' => $this->file_id]);
            SearchHelper::filterByDateRange($this->created_at, 'created_at', $query);
            $query->andFilterWhere(['like', 'name', $this->name]);
            $query->andFilterWhere(['like', 'phone', $this->phone]);
            $query->andFilterWhere(['like', 'pdf_id', $this->pdf_id]);
            $query->andFilterWhere(['like', 'email', $this->email]);
            $query->andFilterWhere(['like', 'comment', $this->comment]);
        }


        return $dataProvider;
    }
}
