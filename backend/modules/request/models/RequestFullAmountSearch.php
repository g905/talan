<?php
namespace backend\modules\request\models;

use backend\modules\request\helpers\SearchHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RequestFullAmountSearch represents the model behind the search form about `RequestFullAmount`.
 */
class RequestFullAmountSearch extends RequestFullAmount
{
	public function rules()
	{
		return [
			[['id', 'city_id'], 'integer'],
			[['name', 'phone'], 'safe'],
		];
	}

	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	public function behaviors()
	{
		return [];
	}

	/**
	 * Creates data provider instance with search query applied
	 */
	public function search($params)
	{
		$query = RequestFullAmountSearch::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'=> ['defaultOrder' => ['id' => SORT_DESC]],
		]);

		$this->load($params);

		if ($this->validate()) {
			$query->andFilterWhere([
				'id' => $this->id,
				'city_id' => $this->city_id,
			]);
			SearchHelper::filterByDateRange($this->created_at, 'created_at', $query);
			$query->andFilterWhere(['like', 'name', $this->name])
				->andFilterWhere(['like', 'phone', $this->phone]);
		}

		return $dataProvider;
	}
}
