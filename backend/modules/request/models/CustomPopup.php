<?php

namespace backend\modules\request\models;

use backend\components\StylingActionColumn;
use common\helpers\SiteUrlHelper;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%request_custom}}".
 *
 * @property integer $id
 * @property string $white_label
 * @property string $black_label
 * @property string $sub_label
 * @property string $label_placeholder
 * @property string $phone_placeholder
 * @property string $email_placeholder
 * @property string $agreement_text
 * @property string $button_label
 * @property integer $raise_time
 * @property string $url_param_value
 * @property integer $is_default
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $form_onsubmit
 */
class CustomPopup extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%custom_popup}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['white_label', 'black_label', 'sub_label', 'agreement_text'], 'string'],
            [['raise_time'], 'integer'],
            [['label_placeholder', 'phone_placeholder', 'email_placeholder', 'button_label', 'url_param_value', 'form_onsubmit'], 'string', 'max' => 255],
            [['is_default'], 'integer', 'max' => 1],
            [['raise_time'], 'default', 'value' => 0],
            [['is_default'], 'default', 'value' => 0],
            [['is_default'], 'validateUniqueDefault']
        ];
    }

    /**
     * @return void
     */
    public function validateUniqueDefault()
    {
        $error = Yii::t('back/RequestCustom', 'Only one popup may be default!');
        $defaultIsExistsId = self::find()->where(['is_default' => 1])->select(['id'])->column();
        if ($this->is_default == 1 && !empty($defaultIsExistsId[0])) {
            if ($this->isNewRecord) {
                $this->addError('is_default', $error);
            } elseif ((int)$defaultIsExistsId[0] !== $this->id) {
                $this->addError('is_default', $error);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/RequestCustom', 'ID'),
            'white_label' => Yii::t('back/RequestCustom', 'White title'),
            'black_label' => Yii::t('back/RequestCustom', 'Black title'),
            'sub_label' => Yii::t('back/RequestCustom', 'Sub-title'),
            'label_placeholder' => Yii::t('back/RequestCustom', 'Label placeholder'),
            'phone_placeholder' => Yii::t('back/RequestCustom', 'Phone placeholder'),
            'email_placeholder' => Yii::t('back/RequestCustom', 'Email placeholder'),
            'agreement_text' => Yii::t('back/RequestCustom', 'Agreement text'),
            'button_label' => Yii::t('back/RequestCustom', 'Send button text'),
            'raise_time' => Yii::t('back/RequestCustom', 'Time for popup appearing'),
            'url_param_value' => Yii::t('back/RequestCustom', 'Url param value to show this popup'),
            'is_default' => Yii::t('back/RequestCustom', 'Is default popup'),
            'form_onsubmit' => Yii::t('back/RequestCustom', 'Form onsubmit'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return Yii::t('back/RequestCustom', 'Custom popup');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                     'white_label:ntext',
                    // 'black_label:ntext',
                    // 'sub_label:ntext',
//                    'label_placeholder',
//                    'phone_placeholder',
//                    'email_placeholder:email',
                    // 'agreement_text:ntext',
//                    'button_label',
                    'raise_time',
                    'url_param_value:url',
                    [
                        'attribute' => 'is_default',
                        'value' => function (CustomPopup $model) {
                            return $model->is_default
                                ? Yii::t('app', 'Yes')
                                : Yii::t('app', 'No');
                        },
                        'filter' => [
                            Yii::t('app', 'No'),
                            Yii::t('app', 'Yes'),
                        ]
                    ],
                    ['class' => StylingActionColumn::class],
                ];
            break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'white_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'black_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'sub_label',
                        'format' => 'html',
                    ],
                    'label_placeholder',
                    'phone_placeholder',
                    'email_placeholder:email',
                    [
                        'attribute' => 'agreement_text',
                        'format' => 'html',
                    ],
                    'button_label',
                    'raise_time:datetime',
                    'url_param_value:url',
                    'is_default',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new CustomPopupSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        if ($this->isNewRecord) {
            $this->agreement_text = "Даю <a href=\"" . SiteUrlHelper::getAgreementUrl() . "\" target=\"_blank\">согласие</a> на обработку моих персональных данных, с условиями <a href=\"" . SiteUrlHelper::getPrivatePolicyUrl() . "\" target=\"_blank\">Политики</a> ознакомлен.";
            $this->button_label = 'Отправить';
        }
        $config = [
            'form-set' => [
                Yii::t('back/RequestCustom', 'Main') => [
                    'white_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ]
                    ],
                    'black_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ]
                    ],
                    'sub_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ]
                    ],
                    'raise_time' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'url_param_value' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'is_default' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                ],
                Yii::t('back/RequestCustom', 'Form') => [
                    'label_placeholder' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ]
                    ],
                    'phone_placeholder' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ]
                    ],
                    'email_placeholder' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ]
                    ],
                    'agreement_text' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::class,
                        'options' => [
                            'model' => $this,
                            'attribute' => 'agreement_text',
                            'settings' => [
                                'buttons' => ['link'],
                                'formatting' => ['p', 'h3', 'a'],
                                'plugins' => [
                                    'fullscreen',
                                ],
                            ],
                        ]
                    ],
                    'button_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'form_onsubmit' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => ['maxlength' => true],
                    ],
                ]
            ],
        ];

        return $config;
    }


}
