<?php

namespace backend\modules\request\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RequestCooperationSearch represents the model behind the search form about `RequestCooperation`.
 */
class RequestCooperationSearch extends RequestCooperation
{
    public function rules()
    {
        return [
            [['id', 'city_id'], 'integer'],
            [['name', 'phone', 'email', 'company_name', 'chief_phone'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = RequestCooperationSearch::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'chief_phone', $this->chief_phone]);

        return $dataProvider;
    }
}
