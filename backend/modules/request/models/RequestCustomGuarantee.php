<?php

namespace backend\modules\request\models;

use backend\components\FormBuilder;
use backend\components\StylingActionColumn;
use backend\modules\apartment\models\Apartment;
use backend\modules\request\helpers\SearchHelper;
use common\models\ApartmentComplex;
use common\models\City;
use kartik\grid\GridView;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\base\EntityToFile;
use yii\db\ActiveQueryInterface;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%request_custom_guarantee}}".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $city
 * @property string $comment
 * @property integer $file_id
 */
class RequestCustomGuarantee extends ActiveRecord implements BackendModel
{
    public static function tableName()
    {
        return '{{%request_custom_guarantee}}';
    }

    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'city', 'comment', 'file_id', 'pdf_id', 'complex', 'building_number', 'apartment_number'], 'required'],
            [['city', 'file_id', 'complex', 'building_number', 'apartment_number'], 'integer'],
            [['comment'], 'string'],
            [['name', 'phone', 'email', 'pdf_id'], 'string', 'max' => 255],
         ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/request', 'ID'),
            'created_at' => Yii::t('back/request', 'Created At'),
            'updated_at' => Yii::t('back/request', 'Updated At'),
            'name' => Yii::t('back/request', 'Name'),
            'phone' => Yii::t('back/request', 'Phone'),
            'email' => Yii::t('back/request', 'Email'),
            'complex' => Yii::t('back/request', 'Complex'),
            'building_number' => Yii::t('back/request', 'Building'),
            'apartment_number' => Yii::t('back/request', 'Apartment'),
            'city' => Yii::t('back/request', 'City'),
            'comment' => Yii::t('back/request', 'Comment'),
            'file_id' => Yii::t('back/request', 'File Id'),
            'pdf_id' => Yii::t('back/request', 'Request Id'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    public function getTitle()
    {
        return bt('Request Custom Guarantee', 'app');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    [
                        'attribute' => 'city',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->cityObj)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any city')],
                    ],
                    SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
                    'phone',
                    'email',
                    [
                        'attribute' => 'file_id',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            return $model->file_id
                                ? Html::a('Файл', FPM::originalSrc($model->file_id), ['data-pjax' => 0, 'target' => '_blank'])
                                : null;
                        }
                    ],
                    'pdf_id',
                    ['class' => StylingActionColumn::class],
                ];
            case 'view':
                return [
                    'id',
                    'name',
                    'phone',
                    'email:email',
                    'city',
                    [
                        'attribute' => 'comment',
                        'format' => 'html',
                    ],
                    'file_id:file',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new RequestCustomGuaranteeSearch();
    }

    public function getFormConfig()
    {
        $config = [
            'city' => [
                'type' => FormBuilder::INPUT_DROPDOWN_LIST,
                'items' => City::getItems(),
                'options' => ['prompt' => ''],
            ],
            'name' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true
                ],
            ],
            'phone' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'email' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'complex' => [
                'type' => FormBuilder::INPUT_DROPDOWN_LIST,
                'items' => ApartmentComplex::getItems(),
                'options' => ['prompt' => ''],
            ],
            'building_number' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'apartment_number' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'file_id' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'prompt' => '',
                    'disabled' => 'disabled',
                    'value' => Url::base(true) . FPM::originalSrc($this->file_id),
                    'iconRightContent' => Html::a('Скачать файл', FPM::originalSrc($this->file_id), ['data-pjax' => 0, 'target' => '_blank'])
                ],
            ],
            'pdf_id' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'prompt' => '',
                    'disabled' => 'disabled',
                    'value' => Url::base(true) . '/uploads/pdf/'. $this->pdf_id . '.pdf',
                    'iconRightContent' => Html::a('Скачать PDF', '/uploads/pdf/'. $this->pdf_id . '.pdf', ['data-pjax' => 0, 'target' => '_blank'])
                ],
            ],
            'comment' => [
                'type' => FormBuilder::INPUT_TEXTAREA,
                'options' => [
                    'maxlength' => true,

                ],
            ],
        ];

        return $config;
    }

    public function getCityObj()
    {
        return $this->hasOne(City::class, ['id' => 'city']);
    }

}
