<?php

namespace backend\modules\request\models;

use backend\helpers\StdInput;
use backend\modules\apartment\models\ApartmentBuilding;
use backend\modules\contacts\models\City;
use backend\modules\request\helpers\SearchHelper;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/**
 * Class RequestGuarantee
 * @package backend\modules\request\models
 */
class RequestGuarantee extends \common\models\RequestGuarantee implements BackendModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'building_id', 'problem_type'], 'integer'],
            [['problem'], 'string'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'targetClass' => \common\models\City::className(), 'targetAttribute' => 'id'],
            [['building_id'], 'exist', 'targetClass' => \common\models\ApartmentBuilding::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City'),
            'building_id' => Yii::t('app', 'Building'),
            'problem_type' => Yii::t('app', 'Problem type'),
            'name' => Yii::t('app', 'Name'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'problem' => Yii::t('app', 'Problem'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Request Guarantee');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'problem_type',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->city)->label;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    [
                        'attribute' => 'building_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->building)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(ApartmentBuilding::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any complex')],
                    ],
                    SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
                    'name',
                    'phone',
                    'email:email',
                    // 'problem:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'city_id',
                    'building_id',
                    'problem_type',
                    'name',
                    'phone',
                    'email:email',
                    [
                        'attribute' => 'problem',
                        'format' => 'html',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new RequestGuaranteeSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'city_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\City::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'building_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ApartmentBuilding::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'problem_type' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\RequestProblem::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'name' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'phone' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'email' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'problem' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'problem',
                ]
            ],
        ];

        return $config;
    }


}
