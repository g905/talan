<?php

namespace backend\modules\request\models;

use backend\components\FormBuilder;
use backend\modules\request\helpers\SearchHelper;
use common\models\ApartmentComplex;
use common\models\City;
use Yii;
use backend\helpers\StdColumn;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\ConstructorRequest as CommonRequest;

/**
 * This is the model class for table "constructor_request".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property integer $price
 * @property integer $city
 * @property integer $complex
 * @property integer $flat
 * @property string $data
 * @property string $type
 * @property integer $created_at
 * @property integer $updated_at
 */
class ConstructorRequest extends ActiveRecord implements BackendModel
{

    public static function tableName()
    {
        return 'constructor_request';
    }

    public function rules()
    {
        return [
            [['name', 'phone', 'price', 'city_id', 'complex_id', 'flat_id', 'data'], 'required'],
            [['city_id', 'complex_id', 'flat_id'], 'integer'],
            [['data'], 'string'],
            [['name', 'phone', 'type'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'price' => 'Price',
            'city_id' => 'City',
            'complex_id' => 'Complex',
            'flat_id' => 'Flat',
            'data' => 'Data',
            'type' => 'Type',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    public function getTitle()
    {
        return bt('Constructor Request', 'app');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->city)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any city')],
                    ],
                    [
                        'attribute' => 'complex_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var RequestComplexPresentation $model */
                            return optional($model->apartmentComplex)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(ApartmentComplex::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any complex')],
                    ],
                    'name',
                    'phone',
                    SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    'name',
                    'phone',
                    'price',
                    'city_id',
                    'complex_id',
                    'flat_id',
                    'data',
                    'type',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new ConstructorRequestSearch();
    }

    public function getFormConfig()
    {
        $config = [
            'name' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'disabled' => $this->isNewRecord ? false : true,
                ],
            ],
            'phone' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'disabled' => $this->isNewRecord ? false : true,
                ],
            ],
            'price' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'disabled' => $this->isNewRecord ? false : true,
                ]
            ],
            'city_id' => [
                'type' => FormBuilder::INPUT_DROPDOWN_LIST,
                'items' => City::getItems(),
                'options' => [
                    'prompt' => '',
                    'disabled' => $this->isNewRecord ? false : true,
                ],
            ],
            'complex_id' => [
                'type' => FormBuilder::INPUT_DROPDOWN_LIST,
                'items' => ApartmentComplex::getItems(),
                'options' => [
                    'prompt' => '',
                    'disabled' => $this->isNewRecord ? false : true,
                ],
            ],
            'flat_id' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'disabled' => $this->isNewRecord ? false : true,
                ]
            ],
            'data' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                'options' => [
                    'disabled' => $this->isNewRecord ? false : true,
                ],
            ],
            'type' => [
                'type' => FormBuilder::INPUT_DROPDOWN_LIST,
                'items' => CommonRequest::getTypeDictionary(),
                'options' => [
                    'prompt' => '',
                    'disabled' => $this->isNewRecord ? false : true,
                ],
            ],
        ];

        return $config;
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'complex_id']);
    }

}
