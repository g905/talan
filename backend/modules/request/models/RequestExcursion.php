<?php

namespace backend\modules\request\models;

use Yii;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\modules\apartment\models\Apartment;
use backend\modules\request\helpers\SearchHelper;
use backend\modules\apartment\models\ApartmentComplex;

/**
 * Class RequestExcursion
 * @package backend\modules\request\models
 */
class RequestExcursion extends \common\models\RequestExcursion implements BackendModel
{
    public $ac_id;

    public function rules()
    {
        return [
            [['apartment_id', 'city_id', 'ac_id','request_type'], 'integer'],
            [['name', 'phone'], 'required'],
            [['name', 'phone'], 'string', 'max' => 255],
            [['apartment_id'], 'exist', 'targetClass' => \common\models\Apartment::class, 'targetAttribute' => 'id'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/request-excursion', 'ID'),
            'city_id' => Yii::t('back/request-fullamount', 'City'),
            'ac_id' => Yii::t('back/request-excursion', 'Apartment complex'),
            'apartment_id' => Yii::t('back/request-excursion', 'Apartment'),
            'name' => Yii::t('back/request-fullamount', 'Name'),
            'phone' => Yii::t('back/request-fullamount', 'Phone'),
            'created_at' => Yii::t('back/request-fullamount', 'Created At'),
            'updated_at' => Yii::t('back/request-fullamount', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => ['class' => \yii\behaviors\TimestampBehavior::class],
        ];
    }

    public function getApartment()
    {
        return $this->hasOne(Apartment::class, ['id' => 'apartment_id']);
    }

    public function getTitle()
    {
        return Yii::t('back/menu', 'Request prices');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return '<b>' . optional($model->city)->label . '</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any city')],
                    ],
                    [
                        'attribute' => 'ac_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->apartmentComplex)->label ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(ApartmentComplex::find()->orderBy('city_id')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any complex')],
                    ],
                    SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
                    [
                        'attribute' => 'apartment_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var RequestExcursion $model */
                            if (isset($model->apartment)){
                                return '<b>'.$model->apartment->label.'</b>';
                            }
                            else{
                                return '-';
                            }

                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(Apartment::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => Yii::t('back/site-menu', 'Any apartment')],
                    ],
                    'name',
                    'phone',
                    ['class' => \backend\components\StylingActionColumn::class],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'apartment_id',
                    'name',
                    'phone',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new RequestExcursionSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'city_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => City::getItems(),
                'options' => ['prompt' => ''],
            ],
            'apartment_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\Apartment::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'ac_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ApartmentComplex::getItems(),
                'options' => [
                    'value' => optional($this->apartmentComplex)->id,
                    'prompt' => '',
                    'disabled' => true
                ],
            ],
            'name' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'phone' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
        ];

        return $config;
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
