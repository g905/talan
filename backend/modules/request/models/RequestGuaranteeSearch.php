<?php

namespace backend\modules\request\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RequestGuaranteeSearch represents the model behind the search form about `RequestGuarantee`.
 */
class RequestGuaranteeSearch extends RequestGuarantee
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'building_id', 'problem_type'], 'integer'],
            [['name', 'phone', 'email', 'problem'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestGuaranteeSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'building_id' => $this->building_id,
            'problem_type' => $this->problem_type,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'problem', $this->problem]);

        return $dataProvider;
    }
}
