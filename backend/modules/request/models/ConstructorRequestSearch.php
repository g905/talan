<?php

namespace backend\modules\request\models;

use backend\modules\request\helpers\SearchHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ConstructorRequestSearch represents the model behind the search form about `ConstructorRequest`.
 */
class ConstructorRequestSearch extends ConstructorRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'price', 'city_id', 'complex_id', 'flat_id'], 'integer'],
            [['name', 'phone', 'data', 'type', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ConstructorRequestSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere([
                'id' => $this->id,
                'complex_id' => $this->complex_id,
                'city_id' => $this->city_id,
            ]);
            SearchHelper::filterByDateRange($this->created_at, 'created_at', $query);
            $query->andFilterWhere(['like', 'name', $this->name]);
            $query->andFilterWhere(['like', 'phone', $this->phone]);
        }

        return $dataProvider;
    }
}
