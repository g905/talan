<?php

namespace backend\modules\request\models;

use backend\helpers\StdInput;
use backend\modules\apartment\models\ApartmentBuilding;
use backend\modules\contacts\models\City;
use backend\modules\request\helpers\SearchHelper;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\RequestSecondary as CommonModel;

/**
 * Class RequestSecondary
 * @package backend\modules\request\models
 */
class RequestSecondary extends CommonModel implements BackendModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'age', 'redevelopment'], 'integer'],
            [['name', 'phone', 'address', 'square'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'targetClass' => \common\models\City::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Request Secondary');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->city)->label;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
                    'name',
                    'phone',
                    'square',
                    'address',
                    'age',
                    // 'problem:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'city_id',
                    'name',
                    'phone',
                    'square',
                    'address',
                    'age',
                    'redevelopment',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new RequestSecondarySearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'city_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\City::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'name' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'phone' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'square' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'address' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'age' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'redevelopment' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }


}
