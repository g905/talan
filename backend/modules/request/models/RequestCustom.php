<?php

namespace backend\modules\request\models;

use yii\behaviors\TimestampBehavior;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\models\CustomPopup;
use common\models\City as CommonCity;
use common\models\ApartmentComplex as CommonApartmentComplex;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\components\StylingActionColumn;
use backend\modules\request\helpers\SearchHelper;
use backend\modules\apartment\models\ApartmentComplex;

/**
 * Class RequestCustom
 * @package backend\modules\request\models
 */
class RequestCustom extends \common\models\RequestCustom implements BackendModel
{
    public function rules()
    {
        return [
            [['city_id', 'custom_popup_id', 'ac_id'], 'integer'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'targetClass' => CommonCity::class, 'targetAttribute' => 'id'],
            [['custom_popup_id'], 'exist', 'targetClass' => CustomPopup::class, 'targetAttribute' => 'id'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'RequestCustom'),
            'name' => bt('Name', 'RequestCustom'),
            'phone' => bt('Phone', 'RequestCustom'),
            'email' => bt('Email', 'RequestCustom'),
            'city_id' => bt('City', 'RequestCustom'),
            'ac_id' => bt('Apartment complex', 'RequestCustom'),
            'custom_popup_id' => bt('Popup', 'RequestCustom'),
            'created_at' => bt('Created at', 'RequestCustom'),
            'updated_at' => bt('Updated at', 'RequestCustom'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getCustomPopup()
    {
        return $this->hasOne(CustomPopup::class, ['id' => 'custom_popup_id']);
    }

    public function getTitle()
    {
        return bt('Request Custom', 'RequestCustom');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'name',
                    'phone',
                    'email:email',
                    [
                        'attribute' => 'city_id',
                        'value' => function(RequestCustom $data) {
                            return $data->city->label ?? null;
                        },
                        'filter' => City::getItems(),
                    ],
                    SearchHelper::getDateRangeColumnConfig($this, 'created_at'),
                    [
                        'attribute' => 'ac_id',
                        'value' => function(RequestCustom $data) {
                            return optional($data->apartmentComplex)->label ?? null;
                        },
                        'filter' => ApartmentComplex::getItems(),
                    ],
                    [
                        'attribute' => 'custom_popup_id',
                        'value' => function(RequestCustom $data) {
                            if (empty($data->customPopup)) {
                                return '--';
                            }
                            return $data->customPopup->white_label . ' (' . $data->customPopup->url_param_value . ')';
                        },
                        'filter' => City::getItems(),
                    ],
                    ['class' => StylingActionColumn::class],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'name',
                    'phone',
                    'email:email',
                    [
                        'attribute' => 'city_id',
                        'value' => $this->city->label ?? null
                    ],
                    [
                        'attribute' => 'custom_popup_id',
                        'value' => empty($this->customPopup)
                            ? ''
                            : $this->customPopup->white_label . ' (' . $this->customPopup->url_param_value . ')',
                    ],
                ];
            break;
        }

        return [];
    }

    public function getSearchModel()
    {
        return new RequestCustomSearch();
    }

    public function getFormConfig()
    {
        return [
            'name' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'phone' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'email' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'city_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => CommonCity::getItems(),
                'options' => [
                    'prompt' => '--',
                ],
            ],
            'ac_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => CommonApartmentComplex::getItems(),
                'options' => [
                    'prompt' => '--',
                ],
            ],
            'custom_popup_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => CustomPopup::getItems('id', 'white_label'),
                'options' => [
                    'prompt' => '--',
                ],
            ],
        ];
    }

    public function showCreateButton()
    {
        return false;
    }
}
