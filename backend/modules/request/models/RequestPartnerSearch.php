<?php

namespace backend\modules\request\models;

use backend\modules\request\helpers\SearchHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RequestPartnerSearch represents the model behind the search form about `RequestPartner`.
 */
class RequestPartnerSearch extends RequestPartner
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'phone', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestPartnerSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere([
                'id' => $this->id,
            ]);
            $query->andFilterWhere(['like', 'name', $this->name]);
            $query->andFilterWhere(['like', 'phone', $this->phone]);
            SearchHelper::filterByDateRange($this->created_at, 'created_at', $query);
            return $dataProvider;
        }



        return $dataProvider;
    }
}
