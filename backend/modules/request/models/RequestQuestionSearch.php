<?php

namespace backend\modules\request\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\request\helpers\SearchHelper;

/**
 * RequestQuestionSearch represents the model behind the search form about `RequestQuestion`.
 */
class RequestQuestionSearch extends RequestQuestion
{
    public function rules()
    {
        return [
            [['id', 'city_id', 'ac_id'], 'integer'],
            [['name', 'phone', 'email', 'question', 'created_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = RequestQuestionSearch::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere([
                'id' => $this->id,
                'city_id' => $this->city_id,
                'ac_id' => $this->ac_id,
            ]);
            SearchHelper::filterByDateRange($this->created_at, 'created_at', $query);
            $query->andFilterWhere(['like', 'name', $this->name]);
            $query->andFilterWhere(['like', 'phone', $this->phone]);
            $query->andFilterWhere(['like', 'email', $this->email]);
            $query->andFilterWhere(['like', 'question', $this->question]);
        }

        return $dataProvider;
    }
}
