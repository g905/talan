<?php

namespace backend\modules\request\helpers;

use Cake\Chronos\Chronos;
use kartik\grid\GridView;
use yii\db\ActiveQuery;

/**
 * Class SearchHelper
 *
 * @package backend\modules\request\helpers
 */
class SearchHelper
{

    public static function getDateRangeColumnConfig($model, $attributeName)
    {
        return [
            'attribute' => $attributeName,
            'format' => 'raw',
            'headerOptions' => ['class' => 'col-md-3'],
            'value' => function ($data) use ($attributeName) {
                return formatter()->asDatetime($data->$attributeName, 'php:d-m-Y');
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'model' => $model,
                'attribute' => $attributeName,
                // 'presetDropdown' => true,
                'convertFormat' => false,
                'pluginOptions' => [
                    'separator' => ' - ',
                    'format' => 'DD-MM-YYYY',
                    'locale' => [
                        'format' => 'DD-MM-YYYY'
                    ],
                ],
                'pluginEvents' => [
                    'cancel.daterangepicker' => 'function(ev, picker) {
                        picker.element.val("").trigger("change");
                    }',
                ],
            ]
        ];
    }
    /**
     * @param string $value attribute value.
     * @param string $attr attribute name.
     * @param ActiveQuery $query active query to filter.
     * @param string $format date format.
     * @param string $divider range divider.
     */
    public static function filterByDateRange($value, string $attr, ActiveQuery &$query, $format = 'd-m-Y', $divider = ' - ')
    {
        [$dateFrom, $dateTo] = self::createTimeObjectsFromInterval($value, $format, $divider);

        if ($dateFrom && $dateTo) {
            $query->andFilterWhere(['between', $attr, $dateFrom, $dateTo]);
        }
    }
    /**
     * @param string $dateRange attribute value that holds dates range.
     * @param string $format date format.
     * @param string $divider range divider.
     * @return array
     */
    private static function createTimeObjectsFromInterval($dateRange, $format, $divider): array
    {
        $output = [false, false];
        $dates = explode($divider, $dateRange);
        if (count($dates) > 1) {
            foreach ($dates as $i => $date) {
                $isRangeEdge = $i === 1;
                $hours = $isRangeEdge ? 23 : 0;
                $minutes = $isRangeEdge ? 59 : 0;
                $seconds = $isRangeEdge ? 59 : 0;
                $output[$i] = Chronos::createFromFormat($format, $date)->setTime($hours, $minutes, $seconds)->getTimestamp();
            }
        }

        return $output;
    }
}
