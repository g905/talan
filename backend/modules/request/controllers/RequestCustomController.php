<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestCustom;

/**
 * RequestCustomController implements the CRUD actions for RequestCustom model.
 */
class RequestCustomController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return RequestCustom::className();
    }
}
