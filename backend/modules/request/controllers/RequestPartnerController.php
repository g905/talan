<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestPartner;

/**
 * RequestPartnerController implements the CRUD actions for RequestPartner model.
 */
class RequestPartnerController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return RequestPartner::className();
    }
}
