<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestInstallmentPlan;

/**
 * RequestInstalmentPlanController implements the CRUD actions for RequestInstallmentPlan model.
 */
class RequestInstalmentPlanController extends BackendController
{
    public function getModelClass()
    {
        return RequestInstallmentPlan::class;
    }
}
