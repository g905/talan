<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestCooperation;

/**
 * RequestCooperationController implements the CRUD actions for RequestCooperation model.
 */
class RequestCooperationController extends BackendController
{
    public function getModelClass()
    {
        return RequestCooperation::class;
    }
}
