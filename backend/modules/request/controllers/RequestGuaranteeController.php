<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestGuarantee;

/**
 * RequestGuaranteeController implements the CRUD actions for RequestGuarantee model.
 */
class RequestGuaranteeController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return RequestGuarantee::className();
    }
}
