<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestCommercial;

/**
 * RequestCommercialController implements the CRUD actions for RequestCommercial model.
 */
class RequestCommercialController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return RequestCommercial::class;
    }
}
