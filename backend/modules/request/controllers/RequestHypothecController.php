<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestHypothec;

/**
 * RequestHypothecController implements the CRUD actions for RequestHypothec model.
 */
class RequestHypothecController extends BackendController
{
    public function getModelClass()
    {
        return RequestHypothec::class;
    }
}
