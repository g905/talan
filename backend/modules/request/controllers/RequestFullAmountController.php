<?php
namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestFullAmount;

/**
 * RequestFullAmountController implements the CRUD actions for RequestFullAmount model.
 */
class RequestFullAmountController extends BackendController
{
	public function getModelClass()
	{
		return RequestFullAmount::class;
	}
}
