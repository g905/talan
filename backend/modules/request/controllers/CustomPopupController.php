<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\CustomPopup;

/**
 * RequestCustomController implements the CRUD actions for RequestCustom model.
 */
class CustomPopupController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return CustomPopup::class;
    }
}
