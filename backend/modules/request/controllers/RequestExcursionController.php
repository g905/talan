<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestExcursion;

/**
 * RequestExcursionController implements the CRUD actions for RequestExcursion model.
 */
class RequestExcursionController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return RequestExcursion::className();
    }
}
