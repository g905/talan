<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\SubscribeRequest;

/**
 * SubscribeRequestController implements the CRUD actions for SubscribeRequest model.
 */
class SubscribeRequestController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return SubscribeRequest::className();
    }
}
