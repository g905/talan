<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\ConstructorRequest;

/**
 * ConstructorRequestController implements the CRUD actions for ConstructorRequest model.
 */
class ConstructorRequestController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ConstructorRequest::className();
    }
}
