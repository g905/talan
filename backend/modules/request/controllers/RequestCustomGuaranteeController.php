<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestCustomGuarantee;

/**
 * RequestCustomGuaranteeController implements the CRUD actions for RequestCustomGuarantee model.
 */
class RequestCustomGuaranteeController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return RequestCustomGuarantee::className();
    }
}
