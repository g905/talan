<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestCallback;

/**
 * RequestCallbackController implements the CRUD actions for RequestCallback model.
 */
class RequestCallbackController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return RequestCallback::className();
    }
}
