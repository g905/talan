<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestSecondary;

/**
 * RequestSecondaryController implements the CRUD actions for RequestSecondary model.
 */
class RequestSecondaryController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return RequestSecondary::className();
    }
}
