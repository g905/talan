<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestJob;

/**
 * RequestJobController implements the CRUD actions for RequestJob model.
 */
class RequestJobController extends BackendController
{
    public function getModelClass()
    {
        return RequestJob::class;
    }
}
