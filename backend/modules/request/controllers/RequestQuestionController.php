<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestQuestion;

/**
 * RequestQuestionController implements the CRUD actions for RequestQuestion model.
 */
class RequestQuestionController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return RequestQuestion::className();
    }
}
