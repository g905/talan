<?php

namespace backend\modules\request\controllers;

use backend\components\BackendController;
use backend\modules\request\models\RequestProblem;

/**
 * RequestProblemController implements the CRUD actions for RequestProblem model.
 */
class RequestProblemController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return RequestProblem::className();
    }
}
