<?php

namespace backend\modules\block\models\block;

use common\models\EntityToFile;
use common\contracts\BlockBuildable;
use common\models\blocks\VacancyPrinciple as CommonModel;
use backend\helpers\StdInput;
use backend\modules\block\models\Block;

/**
 * Class VacancyPrinciple
 *
 * @package common\models\blocks
 */
class VacancyPrinciple extends Block implements BlockBuildable
{
    public function blockType(): int
    {
        return self::VACANCY_PRINCIPLE;
    }

    public function getFormConfig()
    {
        return merge(
            ['image' => StdInput::imageUpload(CommonModel::ATTR_ITEM_IMAGE)],
            parent::getFormConfig()
        );
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
