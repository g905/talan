<?php

namespace backend\modules\block\models\block;

use common\models\EntityToFile;
use common\contracts\BlockBuildable;
use common\models\blocks\BuyAdvantage as CommonModel;
use backend\helpers\StdInput;
use backend\modules\block\models\Block;
use frontend\helpers\FrontendHelper;

/**
 * Class ApartmentComplexSlideVideo
 *
 * @package backend\modules\block\models\block
 */
class ApartmentComplexSlideVideo extends Block implements BlockBuildable
{
    /**
     * @return int
     */
    public function blockType(): int
    {
        return self::APARTMENT_COMPLEX_SLIDER_VIDEO;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'title' => bt('Link', 'block'),
        ];
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return bt('Video', 'block');
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        return [
            'object_id' => StdInput::hidden(),
            'type' => StdInput::hidden(),
            'title' => StdInput::text(),
            'published' => StdInput::checkbox(),
            'position' => StdInput::hidden(),
            'sign' => StdInput::hidden(),
        ];
    }
}
