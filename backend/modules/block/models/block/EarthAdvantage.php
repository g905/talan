<?php

namespace backend\modules\block\models\block;

use common\models\EntityToFile;
use common\contracts\BlockBuildable;
use common\models\blocks\EarthAdvantage as CommonModel;
use backend\helpers\StdInput;
use backend\modules\block\models\Block;

/**
 * Class EarthAdvantage
 *
 * @package backend\modules\block\models\block
 */
class EarthAdvantage extends Block implements BlockBuildable
{
    public function blockType(): int
    {
        return self::EARTH_ADVANTAGE;
    }

    public function getFormConfig()
    {
        return merge(
            ['image' => StdInput::imageUpload(CommonModel::ATTR_ITEM_IMAGE)],
            parent::getFormConfig(),
            [
                'sub_title' => StdInput::text(),
                'link' => StdInput::text(),
                'link_name' => StdInput::text(),
            ]
        );
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
