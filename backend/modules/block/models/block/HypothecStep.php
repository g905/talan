<?php

namespace backend\modules\block\models\block;

use common\contracts\BlockBuildable;
use backend\modules\block\models\Block;

/**
 * Class HypothecStep
 *
 * @package backend\modules\block\models\block
 */
class HypothecStep extends Block implements BlockBuildable
{
    public function blockType(): int
    {
        return self::HYPOTHEC_STEP_BLOCK;
    }
}
