<?php

namespace backend\modules\block\models\block;

use backend\helpers\StdInput;
use common\contracts\BlockBuildable;
use backend\modules\block\models\Block;

/**
 * Class InstallmentPlanStep
 *
 * @package backend\modules\block\models\block
 */
class ContactPageHowToGetStep extends Block implements BlockBuildable
{
    public function blockType(): int
    {
        return self::CONTACT_HOW_TO_GET_STEP;
    }

    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'sub_title' => bt('Button label', 'block'),
            'link' => bt('Button link', 'block'),
        ]);
    }

    public function getFormConfig()
    {
        return merge(parent::getFormConfig(), [
            'description' => StdInput::textArea(),
            'sub_title' => StdInput::text(),
            'link' => StdInput::text(),
        ]);
    }
}
