<?php

namespace backend\modules\block\models\block;

use common\models\EntityToFile;
use common\contracts\BlockBuildable;
use common\models\blocks\HypothecBenefit as CommonModel;
use backend\components\FormBuilder;
use backend\components\ImperaviContent;
use backend\modules\block\models\Block;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;

/**
 * Class HypothecBenefit
 *
 * @package backend\modules\block\models\block
 */
class HypothecBenefit extends Block implements BlockBuildable
{
    public $image;
    public $sign;

    public function init()
    {
        parent::init();
        $this->sign = $this->sign ?: security()->generateRandomString();
    }

    public function rules()
    {
        return merge(parent::rules(), [
            [['sign'], 'string', 'max' => 255],
        ]);
    }

    public function blockType(): int
    {
        return self::HYPOTHEC_BENEFIT_BLOCK;
    }

    public function getFormConfig()
    {
        return [
            'type' => [
                'type' => FormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
            'image' => [
                'type' => FormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'multiple' => false,
                    'attribute' => 'image',
                    'aspectRatio' => false,
                    'saveAttribute' => CommonModel::ATTR_ITEM_IMAGE,
                ])
            ],
            'title' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'description' => [
                'type' => FormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::class,
                'options' => ['model' => $this, 'attribute' => 'description']
            ],
            'published' => [
                'type' => FormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
            ],
            'sign' => [
                'type' => FormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
