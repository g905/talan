<?php

namespace backend\modules\block\models\block;

use common\models\EntityToFile;
use common\contracts\BlockBuildable;
use common\models\blocks\BuyStep as CommonModel;
use backend\helpers\StdInput;
use backend\modules\block\models\Block;

/**
 * Class BuyStep
 *
 * @package backend\modules\block\models\block
 */
class BuyStep extends Block implements BlockBuildable
{
    public function blockType(): int
    {
        return self::BUY_STEP;
    }

    public function getFormConfig()
    {
        return merge(
            parent::getFormConfig(),
            ['sub_title' => StdInput::text()]
        );
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
