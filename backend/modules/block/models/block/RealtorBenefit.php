<?php

namespace backend\modules\block\models\block;

use common\models\EntityToFile;
use common\contracts\BlockBuildable;
use common\models\blocks\RealtorBenefit as CommonModel;
use backend\helpers\StdInput;
use backend\modules\block\models\Block;

/**
 * Class RealtorBenefit
 *
 * @package common\models\blocks
 *
 * @property EntityToFile $image
 * @property string|null $imageSrc
 */
class RealtorBenefit extends Block implements BlockBuildable
{
    public function getTitle()
    {
        return 'Преимущество';
    }

    public function blockType(): int
    {
        return self::REALTOR_BENEFIT;
    }

    public function getFormConfig()
    {
        return merge(parent::getFormConfig(), [
            'image' => StdInput::imageUpload(CommonModel::ATTR_ITEM_IMAGE),
        ]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
