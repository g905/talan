<?php

namespace backend\modules\block\models\block;

use common\models\EntityToFile;
use common\contracts\BlockBuildable;
use common\models\blocks\RealtorFile as CommonModel;
use backend\helpers\StdInput;
use backend\modules\block\models\Block;

/**
 * Class RealtorFile
 *
 * @package common\models\blocks
 *
 * @property EntityToFile $image
 * @property string|null $imageSrc
 */
class RealtorFile extends Block implements BlockBuildable
{
    public $file;

    public function getTitle()
    {
        return 'Файл';
    }

    public function blockType(): int
    {
        return self::REALTOR_FILE;
    }

    public function getFormConfig()
    {
        return [
            'object_id' => StdInput::hidden(),
            'type' => StdInput::hidden(),
            'title' => StdInput::text(),
            'file' => StdInput::fileUpload(CommonModel::ATTR_ITEM_FILE),
            'published' => StdInput::checkbox(),
            'position' => StdInput::hidden(),
            'sign' => StdInput::hidden(),
        ];
    }

    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'file' => bt('File', 'block'),
        ]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
