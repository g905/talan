<?php

namespace backend\modules\block\models\block;

use common\models\EntityToFile;
use common\contracts\BlockBuildable;
use common\models\blocks\RealtorPartner as CommonModel;
use backend\helpers\StdInput;
use backend\modules\block\models\Block;

/**
 * Class RealtorPartners
 *
 * @package common\models\blocks
 *
 * @property EntityToFile $image
 * @property string|null $imageSrc
 */
class RealtorPartner extends Block implements BlockBuildable
{
    public function getTitle()
    {
        return 'Партнер';
    }

    public function blockType(): int
    {
        return self::REALTOR_PARTNER;
    }

    public function getFormConfig()
    {
        return merge(parent::getFormConfig(), [
            'sub_title' => StdInput::text(),
            'description' => StdInput::text(),
            'image' => StdInput::imageUpload(CommonModel::ATTR_ITEM_IMAGE),
        ]);
    }

    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'sub_title' => bt('Address', 'block'),
            'description' => bt('Phone', 'block'),
            'link' => bt('Link', 'block'),
        ]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
