<?php

namespace backend\modules\block\models\block;

use common\contracts\BlockBuildable;
use backend\modules\block\models\Block;

/**
 * Class InstallmentPlanStep
 *
 * @package backend\modules\block\models\block
 */
class InstallmentPlanStep extends Block implements BlockBuildable
{
    public function blockType(): int
    {
        return self::INSTALLMENT_PLAN_STEP_BLOCK;
    }
}
