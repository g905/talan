<?php

namespace backend\modules\block\models\block;

use common\models\EntityToFile;
use common\contracts\BlockBuildable;
use common\models\blocks\EarthCompany as CommonModel;
use backend\helpers\StdInput;
use backend\modules\block\models\Block;

/**
 * Class EarthAdvantage
 *
 * @package backend\modules\block\models\block
 */
class EarthCompany extends Block implements BlockBuildable
{
    public function blockType(): int
    {
        return self::EARTH_COMPANY;
    }

    public function getFormConfig()
    {
        return merge(
            parent::getFormConfig(),
            ['sub_title' => StdInput::text()]
        );
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
