<?php

namespace backend\modules\block\models\block;

use common\models\EntityToFile;
use common\contracts\BlockBuildable;
use common\models\blocks\BuildingAdvantage as CommonModel;
use backend\helpers\StdInput;
use backend\modules\block\models\Block;

/**
 * Class BuildingAdvantage
 *
 * @package backend\modules\block\models\block
 */
class FloorPlan extends Block implements BlockBuildable
{
    public function blockType(): int
    {
        return self::FLOOR_PLAN;
    }

    public function getFormConfig()
    {
        return merge(
            ['title' => StdInput::text()],
            ['image' => StdInput::imageUpload('floorPlanImage')]
            //parent::getFormConfig(),

        );
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
    const ATTR_ITEM_IMAGE = 'floorPlanImage';

    public function getImage()
    {
        return $this->getEntityModel(self::ATTR_ITEM_IMAGE, 'floorPlanImage');
    }

    public function getImageSrc()
    {
        return $this->getEntityThumb('image', 'building', 'floorPlanImage');
    }
}
