<?php

namespace backend\modules\block\models\block;

use common\contracts\BlockBuildable;
use backend\modules\block\models\Block;

/**
 * Class VacancyPageFaqItem
 *
 * @package common\models\blockss
 */
class VacancyPageFaqItem extends Block implements BlockBuildable
{
    public function blockType(): int
    {
        return self::VACANCY_PAGE_FAQ_ITEM;
    }
 
    public function getFormConfig()
    {
        return parent::getFormConfig();
    }
}
