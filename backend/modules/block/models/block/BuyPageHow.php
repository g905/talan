<?php

namespace backend\modules\block\models\block;

use common\models\EntityToFile;
use common\contracts\BlockBuildable;
use common\models\blocks\BuyAdvantage as CommonModel;
use backend\helpers\StdInput;
use backend\modules\block\models\Block;

/**
 * Class BuyPageHow
 *
 * @package backend\modules\block\models\block
 */
class BuyPageHow extends Block implements BlockBuildable
{
    public function blockType(): int
    {
        return self::BUY_PAGE_HOW;
    }

    public function getFormConfig()
    {
        return merge(
            parent::getFormConfig(),
            [
                'link_name' => StdInput::text(),
                'link' => StdInput::text(),
                'color' => StdInput::colorSelect(),
            ]
        );
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
