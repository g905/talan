<?php

namespace backend\modules\block\models\block;

use common\contracts\BlockBuildable;
use backend\modules\block\models\Block;

/**
 * Class VacancyFaqItem
 *
 * @package common\models\blockss
 */
class VacancyFaqItem extends Block implements BlockBuildable
{
    public function blockType(): int
    {
        return self::VACANCY_FAQ_ITEM;
    }

    public function getFormConfig()
    {
        return parent::getFormConfig();
    }
}
