<?php

namespace backend\modules\block\models\block;

use backend\helpers\StdInput;
use common\contracts\BlockBuildable;
use backend\modules\block\models\Block;

/**
 * Class RealtorFact
 *
 * @package backend\modules\block\models\block
 */
class RealtorFact extends Block implements BlockBuildable
{
    public function getTitle()
    {
        return 'Факт';
    }

    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => true],
            [['title', 'sub_title', 'description', 'type'], 'required'],
            [['id', 'object_id', 'type', 'published', 'position'], 'integer'],
            [['sub_title'], 'number'],
            [['type'], 'compare', 'compareValue' => $this->blockType(), 'operator' => '==', 'type' => 'number'],
            [['description'], 'string'],
        ];
    }

    public function blockType(): int
    {
        return self::REALTOR_FACT;
    }

    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'sub_title' => bt('Number', 'block'),
        ]);
    }

    public function getFormConfig()
    {
        return [
            'object_id' => StdInput::hidden(),
            'type' => StdInput::hidden(),
            'sub_title' => StdInput::text(),
            'title' => StdInput::text(),
            'description' => StdInput::editor(),
            'published' => StdInput::checkbox(),
            'position' => StdInput::hidden(),
            'sign' => StdInput::hidden(),
        ];
    }
}
