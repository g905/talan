<?php

namespace backend\modules\block\models\block;

use common\models\EntityToFile;
use common\contracts\BlockBuildable;
use common\models\blocks\SpecializationBenefit as CommonModel;
use backend\helpers\StdInput;
use backend\modules\block\models\Block;

/**
 * Class SpecializationBenefit
 *
 * @package common\models\blocks
 *
 * @property EntityToFile $image
 * @property string|null $imageSrc
 */
class SpecializationBenefit extends Block implements BlockBuildable
{
    public function blockType(): int
    {
        return self::SPECIALIZATION_BENEFIT;
    }

    public function getFormConfig()
    {
        return merge(
            ['image' => StdInput::imageUpload(CommonModel::ATTR_ITEM_IMAGE)],
            parent::getFormConfig()
        );
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
