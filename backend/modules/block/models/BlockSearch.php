<?php

namespace backend\modules\block\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BlockSearch represents the model behind the search form about `Block`.
 */
abstract class BlockSearch extends Block
{
    public function rules()
    {
        return [
            [['id', 'object_id', 'type', 'position'], 'integer'],
            [['title', 'description', 'published'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = BlockSearch::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'object_id' => $this->object_id,
            'position' => $this->position,
            'published' => $this->published,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
