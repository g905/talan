<?php

namespace backend\modules\block\models;

use common\contracts\BlockBuildable;
use common\models\Block as CommonBlock;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\components\BackendModel;

/**
 * This is the model class for table "{{%block}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $object_id
 * @property string $title
 * @property string $sub_title
 * @property string $description
 * @property string $link_name
 * @property string $link
 * @property string $color
 * @property integer $position
 * @property integer $published
 *
 * @property BlockSearch $searchModel
 *
 * @property array $formConfig
 */
abstract class Block extends CommonBlock implements BackendModel, BlockBuildable
{
    public $image;
    public $sign;

    public function init()
    {
        parent::init();
        $this->setAttribute('type', $this->blockType());
        $this->published !== null ?: $this->setAttribute('published', true);
        $this->sign = $this->sign ?: security()->generateRandomString();
    }

    public function rules()
    {
        return [
            [['description', 'youtube_link'], 'string'],
            [['title', 'sub_title', 'link', 'link_name', 'color', 'sign'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => true],
            [['title', 'type'], 'required'],
            [['youtube_link'], 'url'],
            [['id', 'object_id', 'type', 'published', 'position'], 'integer'],
            [['type'], 'compare', 'compareValue' => $this->blockType(), 'operator' => '==', 'type' => 'number'],
            [['image'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'block'),
            'type' => bt('Object type', 'block'),
            'object_id' => bt('Object ID', 'block'),
            'image' => bt('Image', 'block'),
            'title' => bt('Title', 'block'),
            'sub_title' => bt('Sub title', 'block'),
            'description' => bt('Description', 'block'),
            'youtube_link' => bt('Youtube link', 'block'),
            'link' => bt('Link', 'block'),
            'link_name' => bt('Link name', 'block'),
            'color' => bt('Color', 'block'),
            'position' => bt('Position', 'block'),
            'published' => bt('Published', 'block'),
        ];
    }

    public function getTitle()
    {
        return bt('Block', 'block');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    'title',
                    // 'sub_title',
                    // 'description:ntext',
                    'published:boolean',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    'title',
                    // 'sub_title',
                    // 'description:ntext',
                    'published:boolean',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return null;
    }

    public function getFormConfig()
    {
        return [
            'object_id' => StdInput::hidden(),
            'type' => StdInput::hidden(),
            'title' => StdInput::text(),
            'description' => StdInput::editor(),
            'published' => StdInput::checkbox(),
            'position' => StdInput::hidden(),
            'sign' => StdInput::hidden(),
        ];
    }
}
