<?php

namespace backend\modules\hr\models;

use backend\components\StylingActionColumn;
use backend\helpers\StdColumn;
use backend\modules\menu\models\VacancySubMenu;
use yii\behaviors\TimestampBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\models\Vacancy as CommonVacancy;
use backend\helpers\StdInput;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\modules\contacts\models\Manager;
use backend\modules\block\models\block\VacancyFaqItem;
use backend\modules\block\models\block\VacancyPrinciple;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%vacancy}}".
 *
 * @property City $city
 * @property Manager $manager
 * @property Specialization $specialization
 */
class Vacancy extends CommonVacancy implements BackendModel
{
    public $sign;

    public function init()
    {
        parent::init();
        $this->sign = $this->sign ?: security()->generateRandomString();
    }

    public static function tableName()
    {
        return '{{%vacancy}}';
    }

    public function rules()
    {
        return [
            [['city_id', 'specialization_id', 'manager_id', 'view_count', 'position', 'published', 'responses'], 'integer'],
            [['responsibilities_content', 'requirements_content', 'benefits_content', 'form_description', 'form_emails'], 'string'],
            [[
                'label', 'alias', 'description', 'responsibilities_label', 'requirements_label', 'benefits_label',
                'form_label', 'form_agreement', 'form_onsubmit', 'related_label', 'faq_label', 'principles_label', 'sign'
            ], 'string', 'max' => 255],
            [['city_id'], 'exist', 'targetClass' => City::class, 'targetAttribute' => 'id'],
            [['specialization_id'], 'exist', 'targetClass' => Specialization::class, 'targetAttribute' => 'id'],
            [['manager_id'], 'exist', 'targetClass' => Manager::class, 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'vacancy'),
            'city_id' => bt('City ID', 'vacancy'),
            'specialization_id' => bt('Specialization ID', 'vacancy'),
            'label' => bt('Label', 'vacancy'),
            'alias' => bt('Alias', 'vacancy'),
            'description' => bt('Description', 'vacancy'),
            'related_label' => bt('Related Label', 'vacancy'),
            'responsibilities_label' => bt('Responsibilities Label', 'vacancy'),
            'responsibilities_content' => bt('Responsibilities Content', 'vacancy'),
            'requirements_label' => bt('Requirements Label', 'vacancy'),
            'requirements_content' => bt('Requirements Content', 'vacancy'),
            'benefits_label' => bt('Benefits Label', 'vacancy'),
            'benefits_content' => bt('Benefits Content', 'vacancy'),
            'faq_label' => bt('FAQ label', 'vacancy'),
            'principles_label' => bt('Principles Label', 'vacancy'),
            'manager_id' => bt('Manager ID', 'vacancy'),
            'form_label' => bt('Form Label', 'vacancy'),
            'form_description' => bt('Form Description', 'vacancy'),
            'form_agreement' => bt('Form Agreement', 'vacancy'),
            'form_emails' => bt('Form Emails', 'vacancy'),
            'form_onsubmit' => bt('Form Onsubmit', 'vacancy'),
            'view_count' => bt('View Count', 'vacancy'),
            'position' => bt('Position', 'vacancy'),
            'published' => bt('Published', 'vacancy'),
            'responses' => bt('Responses', 'vacancy'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'seo' => MetaTagBehavior::class,
        ];
    }

    public function getTitle()
    {
        return bt('Vacancy', 'vacancy');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    StdColumn::city(),
                    StdColumn::specialization(),
                    'label',
                    // 'alias',
                    'description',
                    // 'responsibilities_label',
                    // 'responsibilities_content:ntext',
                    // 'requirements_label',
                    // 'requirements_content:ntext',
                    // 'benefits_label',
                    // 'benefits_content:ntext',
                    StdColumn::manager(),
                    'responses',
                    // 'form_label',
                    // 'form_description:ntext',
                    // 'form_agreement',
                    // 'form_emails:ntext',
                    // 'form_onsubmit',
                    StdColumn::viewCount(),
                    StdColumn::position(),
                    StdColumn::published(),
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{related-clone}',
                        'buttons'  => [
                            'related-clone' => function($url, $model) {
                                return Html::a(Html::tag('span', '',['class' => 'si si-layers']), $url, ['class' => 'btn btn-xs btn-default']);
                            }
                        ]
                    ]
                ];
            case 'view':
                return [
                    'id',
                    'city_id',
                    'specialization_id',
                    'label',
                    'alias',
                    'description',
                    'responsibilities_label',
                    'responsibilities_content:html',
                    'requirements_label',
                    'requirements_content:html',
                    'benefits_label',
                    'benefits_content:html',
                    'manager_id',
                    'form_label',
                    'form_description:html',
                    'form_agreement',
                    'form_emails:html',
                    'form_onsubmit',
                    'responses',
                    'view_count',
                    'position',
                    'published:boolean',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new VacancySearch();
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General', 'vacancy') => [
                    'city_id' => StdInput::citySelect(),
                    'specialization_id' => StdInput::specializationSelect(),
                    'label' => StdInput::label($this),
                    'alias' => StdInput::alias($this),
                    'description' => StdInput::textArea(),
                    'related_label' => StdInput::text(),
                    'view_count' => StdInput::viewCount(),
                    'position' => StdInput::text(),
                    'published' => StdInput::text(),
                    'sign' => StdInput::hidden(),
                ],
                bt('Responsibilities', 'vacancy') => [
                    'responsibilities_label' => StdInput::text(),
                    'responsibilities_content' => StdInput::editor(),
                ],
                bt('Requirements', 'vacancy') => [
                    'requirements_label' => StdInput::text(),
                    'requirements_content' => StdInput::editor(),
                ],
                bt('Benefits', 'vacancy') => [
                    'benefits_label' => StdInput::text(),
                    'benefits_content' => StdInput::editor(),
                ],
                /*bt('Faq', 'vacancy') => [
                    'faq_label' => StdInput::text(),
                    $this->getRelatedFormConfig()['faq_blocks'],
                ],*/
                bt('Principles', 'vacancy') => [
                    'principles_label' => StdInput::text(),
                    $this->getRelatedFormConfig()['principle_blocks'],
                ],
                bt('Form', 'vacancy') => [
                    'manager_id' => StdInput::managerSelect(),
                    'form_label' => StdInput::text(),
                    'form_description' => StdInput::editor(),
                    'form_agreement' => StdInput::editor(),
                    'form_emails' => StdInput::textArea(),
                    'form_onsubmit' => StdInput::text(),
                ],
                bt('Menu', 'vacancy') => [
                    $this->getRelatedFormConfig()['sub_menu'],
                ],
            ],
        ];
    }

    public function getRelatedFormConfig()
    {
        return [
            'faq_blocks' => ['relation' => 'faqBlocks'],
            'principle_blocks' => ['relation' => 'principleBlocks'],
            'sub_menu' => ['relation' => 'subMenus'],
        ];
    }

    public function getFaqBlocks()
    {
        return $this->hasMany(VacancyFaqItem::class, ['object_id' => 'id'])
            ->onCondition(['type' => VacancyFaqItem::VACANCY_FAQ_ITEM]);
    }

    public function getPrincipleBlocks()
    {
        return $this->hasMany(VacancyPrinciple::class, ['object_id' => 'id'])
            ->onCondition(['type' => VacancyFaqItem::VACANCY_PRINCIPLE]);
    }

    public function getSubMenus()
    {
        return $this->hasMany(VacancySubMenu::class, ['object_id' => 'id'])
            ->onCondition(['type' => VacancySubMenu::TYPE_VACANCY]);
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getSpecialization()
    {
        return $this->hasOne(Specialization::class, ['id' => 'specialization_id']);
    }
}
