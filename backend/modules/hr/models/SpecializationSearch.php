<?php

namespace backend\modules\hr\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SpecializationSearch represents the model behind the search form about `Specialization`.
 */
class SpecializationSearch extends Specialization
{
    public function rules()
    {
        return [
            [['id', 'city_id', 'manager_id', 'view_count', 'position', 'published'], 'integer'],
            [[
                'top_label', 'top_description', 'top_btn_label', 'top_btn_link', 'details_label', 'details_description',
                'mission_label', 'mission_sub_label', 'principles_label', 'gallery_label', 'gallery_description',
                'articles_label', 'articles_btn_label', 'reviews_label', 'vacancies_label', 'vacancies_btn_label',
                'color', 'form_label', 'form_description', 'form_agreement', 'form_emails', 'form_onsubmit',
            ], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = SpecializationSearch::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'manager_id' => $this->manager_id,
            'view_count' => $this->view_count,
            'position' => $this->position,
            'published' => $this->published,
            'color' => $this->color,
        ]);

        $query->andFilterWhere(['like', 'top_label', $this->top_label])
            ->andFilterWhere(['like', 'top_description', $this->top_description])
            ->andFilterWhere(['like', 'top_btn_label', $this->top_btn_label])
            ->andFilterWhere(['like', 'top_btn_link', $this->top_btn_link])
            ->andFilterWhere(['like', 'details_label', $this->details_label])
            ->andFilterWhere(['like', 'details_description', $this->details_description])
            ->andFilterWhere(['like', 'mission_label', $this->mission_label])
            ->andFilterWhere(['like', 'mission_sub_label', $this->mission_sub_label])
            ->andFilterWhere(['like', 'principles_label', $this->principles_label])
            ->andFilterWhere(['like', 'gallery_label', $this->gallery_label])
            ->andFilterWhere(['like', 'gallery_description', $this->gallery_description])
            ->andFilterWhere(['like', 'articles_label', $this->articles_label])
            ->andFilterWhere(['like', 'articles_btn_label', $this->articles_btn_label])
            ->andFilterWhere(['like', 'reviews_label', $this->reviews_label])
            ->andFilterWhere(['like', 'vacancies_label', $this->vacancies_label])
            ->andFilterWhere(['like', 'vacancies_btn_label', $this->vacancies_btn_label])
            ->andFilterWhere(['like', 'form_label', $this->form_label])
            ->andFilterWhere(['like', 'form_description', $this->form_description])
            ->andFilterWhere(['like', 'form_agreement', $this->form_agreement])
            ->andFilterWhere(['like', 'form_emails', $this->form_emails])
            ->andFilterWhere(['like', 'form_onsubmit', $this->form_onsubmit]);

        return $dataProvider;
    }
}
