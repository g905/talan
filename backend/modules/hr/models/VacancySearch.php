<?php

namespace backend\modules\hr\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * VacancySearch represents the model behind the search form about `Vacancy`.
 */
class VacancySearch extends Vacancy
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'specialization_id', 'manager_id', 'view_count', 'position', 'published', 'responses'], 'integer'],
            [
                [
                    'label',
                    'alias',
                    'description',
                    'responsibilities_label',
                    'responsibilities_content',
                    'requirements_label',
                    'requirements_content',
                    'benefits_label',
                    'benefits_content',
                    'form_label',
                    'form_description',
                    'form_agreement',
                    'form_emails',
                    'form_onsubmit'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VacancySearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'specialization_id' => $this->specialization_id,
            'manager_id' => $this->manager_id,
            'view_count' => $this->view_count,
            'position' => $this->position,
            'published' => $this->published,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'responsibilities_label', $this->responsibilities_label])
            ->andFilterWhere(['like', 'responsibilities_content', $this->responsibilities_content])
            ->andFilterWhere(['like', 'requirements_label', $this->requirements_label])
            ->andFilterWhere(['like', 'requirements_content', $this->requirements_content])
            ->andFilterWhere(['like', 'benefits_label', $this->benefits_label])
            ->andFilterWhere(['like', 'benefits_content', $this->benefits_content])
            ->andFilterWhere(['like', 'form_label', $this->form_label])
            ->andFilterWhere(['like', 'form_description', $this->form_description])
            ->andFilterWhere(['like', 'form_agreement', $this->form_agreement])
            ->andFilterWhere(['like', 'form_emails', $this->form_emails])
            ->andFilterWhere(['like', 'form_onsubmit', $this->form_onsubmit]);

        return $dataProvider;
    }
}
