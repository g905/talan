<?php

namespace backend\modules\hr\models;

use backend\components\StylingActionColumn;
use yii\db\ActiveQuery;
use yii\behaviors\TimestampBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\models\ArticleAssign;
use common\models\EntityToFile;
use common\models\Specialization as CommonSpecialization;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\components\BackendModel;
use backend\modules\blog\models\Article;
use backend\modules\contacts\models\City;
use backend\modules\contacts\models\Manager;
use backend\behaviors\linker\LinkerBehavior;
use backend\modules\menu\models\SpecializationSubMenu;
use backend\modules\block\models\block\SpecializationReview;
use backend\modules\block\models\block\SpecializationBenefit;
use backend\modules\block\models\block\SpecializationPrinciple;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%specialization}}".
 *
 * @property City $city
 * @property Manager $manager
 * @property Vacancy[] $vacancies
 */
class Specialization extends CommonSpecialization implements BackendModel
{
    public $topImage;
    public $topVideo;
    public $previewImage;
    public $galleryImages;
    public $sign;

    public function init()
    {
        parent::init();
        $this->sign = $this->sign ?: security()->generateRandomString();
    }

    public static function tableName()
    {
        return '{{%specialization}}';
    }

    public function rules()
    {
        return [
            [['city_id', 'top_label', 'top_description', 'details_label', 'mission_label'], 'required'],
            [['city_id', 'manager_id', 'view_count', 'position', 'published'], 'integer'],
            [['top_description', 'details_description', 'gallery_description', 'form_description', 'form_emails'], 'string'],
            [[
                'top_label', 'top_btn_label', 'top_btn_link', 'details_label', 'mission_label', 'mission_sub_label',
                'principles_label', 'gallery_label', 'articles_label', 'articles_btn_label', 'reviews_label',
                'vacancies_label', 'vacancies_btn_label', 'form_label', 'form_agreement', 'form_onsubmit', 'color', 'sign'
            ], 'string', 'max' => 255],
            [['city_id'], 'exist', 'targetClass' => City::class, 'targetAttribute' => 'id'],
            [['manager_id'], 'exist', 'targetClass' => Manager::class, 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['topImage', 'topVideo', 'previewImage', 'galleryImages', 'articlesField'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'specialization'),
            'city_id' => bt('City ID', 'specialization'),
            'top_label' => bt('Top Label', 'specialization'),
            'top_description' => bt('Top Description', 'specialization'),
            'top_btn_label' => bt('Top Btn Label', 'specialization'),
            'top_btn_link' => bt('Top Btn Link', 'specialization'),
            'details_label' => bt('Details Label', 'specialization'),
            'details_description' => bt('Details Description', 'specialization'),
            'mission_label' => bt('Mission Label', 'specialization'),
            'mission_sub_label' => bt('Mission Sub Label', 'specialization'),
            'principles_label' => bt('Principles Label', 'specialization'),
            'gallery_label' => bt('Gallery Label', 'specialization'),
            'gallery_description' => bt('Gallery Description', 'specialization'),
            'articles_label' => bt('Articles Label', 'specialization'),
            'articles_btn_label' => bt('Articles Btn Label', 'specialization'),
            'reviews_label' => bt('Reviews Label', 'specialization'),
            'vacancies_label' => bt('Vacancies Label', 'specialization'),
            'vacancies_btn_label' => bt('Vacancies Btn Label', 'specialization'),
            'color' => bt('Color', 'specialization'),
            'manager_id' => bt('Manager ID', 'specialization'),
            'form_label' => bt('Form Label', 'specialization'),
            'form_description' => bt('Form Description', 'specialization'),
            'form_agreement' => bt('Form Agreement', 'specialization'),
            'form_emails' => bt('Form Emails', 'specialization'),
            'form_onsubmit' => bt('Form Onsubmit', 'specialization'),
            'view_count' => bt('View Count', 'specialization'),
            'position' => bt('Position', 'specialization'),
            'published' => bt('Published', 'specialization'),
            'topImage' => bt('Top image', 'specialization'),
            'topVideo' => bt('Top video', 'specialization'),
            'previewImage' => bt('Preview image', 'specialization'),
            'galleryImages' => bt('Gallery images', 'specialization'),
            'articlesField' => bt('Articles', 'specialization'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'seo' => MetaTagBehavior::class,
            'linker' => [
                'class' => LinkerBehavior::class,
                'relations' => [
                    'articlesField' => [
                        'articles',
                        'updater' => [
                            // 'class' => ManyToManySmartUpdater::class,
                            'viaTableAttributesValue' => [
                                'type' => ArticleAssign::TYPE_SPECIALIZATION_ARTICLES,
                                'position' => function () {
                                    static $i = 0;
                                    return ++$i;
                                }
                            ],
                            'viaTableCondition' => [
                                'type' => ArticleAssign::TYPE_SPECIALIZATION_ARTICLES,
                            ],
                        ],
                        'get' => function($value) {
                            return implode(',', $value);
                        },
                        'set' => function($value) {
                            return empty($value) ? [] : explode(',', $value);
                        },
                    ],
                ],
            ],
        ];
    }

    public function getTitle()
    {
        return bt('Specialization', 'specialization');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    StdColumn::city(),
                    'top_label',
                    // 'top_description:ntext',
                    // 'top_btn_label',
                    // 'top_btn_link:url',
                    // 'details_label',
                    // 'details_description:ntext',
                    // 'mission_label',
                    // 'mission_sub_label',
                    // 'principles_label',
                    // 'gallery_label',
                    // 'gallery_description:ntext',
                    // 'articles_label',
                    // 'articles_btn_label',
                    // 'reviews_label',
                    // 'vacancies_label',
                    // 'vacancies_btn_label',
                    // 'form_label',
                    // 'form_description:ntext',
                    // 'form_agreement',
                    // 'form_emails:ntext',
                    // 'form_onsubmit',
                    //StdColumn::color($this),
                    StdColumn::viewCount(),
                    StdColumn::position(),
                    StdColumn::published(),
                    StdColumn::manager(),
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{related-clone}',
                        'buttons'  => [
                            'related-clone' => function($url, $model) {
                                return Html::a(Html::tag('span', '',['class' => 'si si-layers']), $url, ['class' => 'btn btn-xs btn-default']);
                            }
                        ]
                    ]
                ];
                break;
            case 'view':
                return [
                    'id',
                    'city_id',
                    'top_label',
                    'top_description:html',
                    'top_btn_label',
                    'top_btn_link:url',
                    'details_label',
                    'details_description:html',
                    'mission_label',
                    'mission_sub_label',
                    'principles_label',
                    'gallery_label',
                    'gallery_description:html',
                    'articles_label',
                    'articles_btn_label',
                    'reviews_label',
                    'vacancies_label',
                    'vacancies_btn_label',
                    'manager_id',
                    'form_label',
                    'form_description:html',
                    'form_agreement:html',
                    'form_emails:email',
                    'form_onsubmit',
                    'view_count',
                    'position',
                    'published:boolean',
                ];
                break;
        }

        return [];
    }

    public function getSearchModel()
    {
        return new SpecializationSearch();
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General', 'specialization') => [
                    'city_id' => StdInput::citySelect(),
                    'previewImage' => StdInput::imageUpload(self::ATTR_PREVIEW_IMAGE),
                    'color' => StdInput::colorSelect(),
                    'view_count' => StdInput::text(),
                    'position' => StdInput::text(),
                    'published' => StdInput::text(),
                    'sign' => StdInput::hidden(),
                ],
                bt('Top block', 'specialization') => [
                    'top_label' => StdInput::text(),
                    'top_description' => StdInput::editor(),
                    'top_btn_label' => StdInput::text(),
                    'top_btn_link' => StdInput::text(),
                    'topImage' => StdInput::imageUpload(self::ATTR_TOP_IMAGE),
                    'topVideo' => StdInput::videoUpload(self::ATTR_TOP_VIDEO),
                ],
                bt('Details block', 'specialization') => [
                    'details_label' => StdInput::text(),
                    'details_description' => StdInput::textArea(),
                ],
                bt('Benefits block', 'specialization') => [
                    $this->getRelatedFormConfig()['benefit_blocks'],
                ],
                bt('Mission block', 'specialization') => [
                    'mission_label' => StdInput::text(),
                    'mission_sub_label' => StdInput::text(),
                ],
                bt('Principles block', 'specialization') => [
                    'principles_label' => StdInput::text(),
                    $this->getRelatedFormConfig()['principle_blocks'],
                ],
                bt('Gallery block', 'specialization') => [
                    'gallery_label' => StdInput::text(),
                    'gallery_description' => StdInput::editor(),
                    'galleryImages' => StdInput::imageUpload(self::ATTR_GALLERY_IMAGES, true),
                ],
                bt('Articles block', 'specialization') => [
                    'articles_label' => StdInput::text(),
                    'articles_btn_label' => StdInput::text(),
                    'articlesField' => StdInput::relationInput(Article::class),
                ],
                bt('Reviews block', 'specialization') => [
                    'reviews_label' => StdInput::text(),
                    $this->getRelatedFormConfig()['review_blocks'],
                ],
                bt('Vacancies block', 'specialization') => [
                    'vacancies_label' => StdInput::text(),
                    'vacancies_btn_label' => StdInput::text(),
                ],
                bt('Form block', 'specialization') => [
                    'manager_id' => StdInput::managerSelect(),
                    'form_label' => StdInput::text(),
                    'form_description' => StdInput::editor(),
                    'form_agreement' => StdInput::editor(),
                    'form_emails' => StdInput::textArea(),
                    'form_onsubmit' => StdInput::text(),
                ],
                bt('Menu', 'specialization') => [
                    $this->getRelatedFormConfig()['sub_menu'],
                ],
            ],
        ];
    }

    public function getRelatedFormConfig()
    {
        return [
            'review_blocks' => ['relation' => 'reviewBlocks'],
            'benefit_blocks' => ['relation' => 'benefitBlocks'],
            'principle_blocks' => ['relation' => 'principleBlocks'],
            'sub_menu' => ['relation' => 'subMenus'],
        ];
    }

    public function getReviewBlocks()
    {
        return $this->hasMany(SpecializationReview::class, ['object_id' => 'id'])
            ->onCondition(['type' => SpecializationReview::SPECIALIZATION_REVIEW]);
    }

    public function getBenefitBlocks()
    {
        return $this->hasMany(SpecializationBenefit::class, ['object_id' => 'id'])
            ->onCondition(['type' => SpecializationBenefit::SPECIALIZATION_BENEFIT]);
    }

    public function getPrincipleBlocks()
    {
        return $this->hasMany(SpecializationPrinciple::class, ['object_id' => 'id'])
            ->onCondition(['type' => SpecializationPrinciple::SPECIALIZATION_PRINCIPLE]);
    }

    public function getSubMenus()
    {
        return $this->hasMany(SpecializationSubMenu::class, ['object_id' => 'id'])
            ->onCondition(['type' => SpecializationSubMenu::TYPE_SPECIALIZATION]);
    }

    public function getArticles()
    {
        return $this->hasMany(Article::class, ['id' => 'article_id'])
            ->alias('a')
            ->where(['a.published' => self::IS_PUBLISHED])
            ->viaTable(ArticleAssign::tableName(), ['object_id' => 'id'], function (ActiveQuery $q) {
                $q->alias('aa')->andWhere(['aa.type' => ArticleAssign::TYPE_SPECIALIZATION_ARTICLES]);
            })
            ->innerJoin(ArticleAssign::tableName() . ' aa', 'aa.article_id = a.id')
            ->orderBy(['aa.position' => SORT_ASC]);
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getVacancies()
    {
        return $this->hasMany(Vacancy::class, ['specialization_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
