<?php

namespace backend\modules\hr\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CareerSearch represents the model behind the search form about `Career`.
 */
class CareerSearch extends Career
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'manager_id', 'view_count', 'position', 'published'], 'integer'],
            [
                [
                    'top_label',
                    'top_description',
                    'top_btn_label',
                    'top_btn_link',
                    'specializations_label',
                    'director_name',
                    'director_post',
                    'director_text',
                    'reviews_label',
                    'articles_label',
                    'articles_btn_label',
                    'form_label',
                    'form_description',
                    'form_agreement',
                    'form_emails',
                    'form_onsubmit'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CareerSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'manager_id' => $this->manager_id,
            'view_count' => $this->view_count,
            'position' => $this->position,
            'published' => $this->published,
        ]);

        $query->andFilterWhere(['like', 'top_label', $this->top_label])
            ->andFilterWhere(['like', 'top_description', $this->top_description])
            ->andFilterWhere(['like', 'top_btn_label', $this->top_btn_label])
            ->andFilterWhere(['like', 'top_btn_link', $this->top_btn_link])
            ->andFilterWhere(['like', 'specializations_label', $this->specializations_label])
            ->andFilterWhere(['like', 'director_name', $this->director_name])
            ->andFilterWhere(['like', 'director_post', $this->director_post])
            ->andFilterWhere(['like', 'director_text', $this->director_text])
            ->andFilterWhere(['like', 'reviews_label', $this->reviews_label])
            ->andFilterWhere(['like', 'articles_label', $this->articles_label])
            ->andFilterWhere(['like', 'articles_btn_label', $this->articles_btn_label])
            ->andFilterWhere(['like', 'form_label', $this->form_label])
            ->andFilterWhere(['like', 'form_description', $this->form_description])
            ->andFilterWhere(['like', 'form_agreement', $this->form_agreement])
            ->andFilterWhere(['like', 'form_emails', $this->form_emails])
            ->andFilterWhere(['like', 'form_onsubmit', $this->form_onsubmit]);

        return $dataProvider;
    }
}
