<?php

namespace backend\modules\hr\models;

use backend\components\StylingActionColumn;
use yii\db\ActiveQuery;
use yii\behaviors\TimestampBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\models\EntityToFile;
use common\models\ArticleAssign;
use common\models\Career as CommonCareer;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\components\BackendModel;
use backend\modules\blog\models\Article;
use backend\modules\contacts\models\City;
use backend\behaviors\linker\LinkerBehavior;
use backend\modules\contacts\models\Manager;
use backend\modules\menu\models\CareerSubMenu;
use backend\modules\block\models\block\CareerReview;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%career}}".
 *
 * @property City $city
 * @property Manager $manager
 */
class Career extends CommonCareer implements BackendModel
{
    public $topImage;
    public $topVideo;
    public $directorPhoto;
    public $sign;

    public function init()
    {
        parent::init();
        $this->sign = $this->sign ?: security()->generateRandomString();
    }

    public static function tableName()
    {
        return '{{%career}}';
    }

    public function rules()
    {
        return [
            [['city_id', 'top_label', 'specializations_label'], 'required'],
            [['city_id', 'manager_id', 'view_count', 'position', 'published'], 'integer'],
            [['top_description', 'director_text', 'form_description', 'form_emails'], 'string'],
            [
                [
                    'top_label',
                    'top_btn_label',
                    'top_btn_link',
                    'specializations_label',
                    'director_name',
                    'director_post',
                    'reviews_label',
                    'articles_label',
                    'articles_btn_label',
                    'form_label',
                    'form_agreement',
                    'form_onsubmit',
                    'sign'
                ],
                'string',
                'max' => 255
            ],
            [['city_id'], 'exist', 'targetClass' => City::class, 'targetAttribute' => 'id'],
            [['manager_id'], 'exist', 'targetClass' => Manager::class, 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['topImage', 'topVideo', 'directorPhoto', 'articlesField'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'career'),
            'city_id' => bt('City ID', 'career'),
            'top_label' => bt('Top Label', 'career'),
            'top_description' => bt('Top Description', 'career'),
            'top_btn_label' => bt('Top Btn Label', 'career'),
            'top_btn_link' => bt('Top Btn Link', 'career'),
            'specializations_label' => bt('Specializations Label', 'career'),
            'director_name' => bt('Director Name', 'career'),
            'director_post' => bt('Director Post', 'career'),
            'director_text' => bt('Director Text', 'career'),
            'reviews_label' => bt('Reviews Label', 'career'),
            'articles_label' => bt('Articles Label', 'career'),
            'articles_btn_label' => bt('Articles Btn Label', 'career'),
            'manager_id' => bt('Manager ID', 'career'),
            'form_label' => bt('Form Label', 'career'),
            'form_description' => bt('Form Description', 'career'),
            'form_agreement' => bt('Form Agreement', 'career'),
            'form_emails' => bt('Form Emails', 'career'),
            'form_onsubmit' => bt('Form Onsubmit', 'career'),
            'view_count' => bt('View Count', 'career'),
            'position' => bt('Position', 'career'),
            'published' => bt('Published', 'career'),
            'topImage' => bt('Top image', 'career'),
            'topVideo' => bt('Top video', 'career'),
            'directorPhoto' => bt('Director photo', 'career'),
            'articlesField' => bt('Articles', 'career'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'seo' => MetaTagBehavior::class,
            'linker' => [
                'class' => LinkerBehavior::class,
                'relations' => [
                    'articlesField' => [
                        'articles',
                        'updater' => [
                            // 'class' => ManyToManySmartUpdater::class,
                            'viaTableAttributesValue' => [
                                'type' => ArticleAssign::TYPE_CAREER_ARTICLES,
                                'position' => function () {
                                    static $i = 0;
                                    return ++$i;
                                }
                            ],
                            'viaTableCondition' => [
                                'type' => ArticleAssign::TYPE_CAREER_ARTICLES,
                            ],
                        ],
                        'get' => function($value) {
                            return implode(',', $value);
                        },
                        'set' => function($value) {
                            return empty($value) ? [] : explode(',', $value);
                        },
                    ],
                ],
            ],
        ];
    }

    public function getTitle()
    {
        return bt('Career', 'career');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    StdColumn::city(),
                    'top_label',
                    // 'top_description:ntext',
                    'top_btn_label',
                    'top_btn_link:url',
                    'specializations_label',
                    'director_name',
                    // 'director_post',
                    // 'director_text:ntext',
                    // 'reviews_label',
                    // 'articles_label',
                    // 'articles_btn_label',
                    StdColumn::manager(),
                    // 'form_label',
                    // 'form_description:ntext',
                    // 'form_agreement',
                    // 'form_emails:ntext',
                    // 'form_onsubmit',
                    // 'view_count',
                    StdColumn::position(),
                    StdColumn::published(),
					[
						'class'    => StylingActionColumn::class,
						'template' => '{update}{delete}{related-clone}',
						'buttons'  => [
							'related-clone' => function($url, $model) {
								return Html::a(Html::tag('span', '',['class' => 'si si-layers']), $url, ['class' => 'btn btn-xs btn-default']);
							}
						]
					]
                ];
            case 'view':
                return [
                    'id',
                    'city_id',
                    'top_label',
                    'top_description:html',
                    'top_btn_label',
                    'top_btn_link:url',
                    'specializations_label',
                    'director_name',
                    'director_post',
                    'director_text:html',
                    'reviews_label',
                    'articles_label',
                    'articles_btn_label',
                    'manager_id',
                    'form_label',
                    'form_description:html',
                    'form_agreement',
                    'form_emails:html',
                    'form_onsubmit',
                    'view_count',
                    'position',
                    'published:boolean',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new CareerSearch();
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General', 'career') => [
                    'top_label' => StdInput::text(),
                    'top_description' => StdInput::editor(),
                    'top_btn_label' => StdInput::text(),
                    'top_btn_link' => StdInput::text(),
                    'topImage' => StdInput::imageUpload(self::ATTR_TOP_IMAGE),
                    'topVideo' => StdInput::videoUpload(self::ATTR_TOP_VIDEO),
                    'city_id' => StdInput::citySelect(),
                    'view_count' => StdInput::viewCount(),
                    'position' => StdInput::text(),
                    'published' => StdInput::checkbox(),
                    'sign' => StdInput::hidden(),
                ],
                bt('Specialization', 'career') => [
                    'specializations_label' => StdInput::text(),
                ],
                bt('Reviews', 'career') => [
                    'reviews_label' => StdInput::text(),
                    $this->getRelatedFormConfig()['review_blocks'],
                ],
                bt('Director', 'career') => [
                    'director_name' => StdInput::text(),
                    'director_post' => StdInput::text(),
                    'director_text' => StdInput::editor(),
                    'directorPhoto' => StdInput::imageUpload(self::ATTR_DIRECTOR_PHOTO),
                ],
                bt('Articles', 'career') => [
                    'articles_label' => StdInput::text(),
                    'articles_btn_label' => StdInput::text(),
                    'articlesField' => StdInput::relationInput(Article::class),
                ],
                bt('Form', 'career') => [
                    'manager_id' => StdInput::managerSelect(),
                    'form_label' => StdInput::text(),
                    'form_description' => StdInput::editor(),
                    'form_agreement' => StdInput::editor(),
                    'form_emails' => StdInput::textArea(),
                    'form_onsubmit' => StdInput::text(),
                ],
                bt('Menu', 'career') => [
                    $this->getRelatedFormConfig()['sub_menu'],
                ],
            ],
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getArticles()
    {
        return $this->hasMany(Article::class, ['id' => 'article_id'])
            ->alias('a')
            ->where(['a.published' => self::IS_PUBLISHED])
            ->viaTable(ArticleAssign::tableName(), ['object_id' => 'id'], function (ActiveQuery $q) {
                $q->alias('aa')->andWhere(['aa.type' => ArticleAssign::TYPE_CAREER_ARTICLES]);
            })
            ->innerJoin(ArticleAssign::tableName() . ' aa', 'aa.article_id = a.id')
            ->orderBy(['aa.position' => SORT_ASC]);
    }

    public function getRelatedFormConfig()
    {
        return [
            'review_blocks' => ['relation' => 'reviewBlocks'],
            'sub_menu' => ['relation' => 'subMenus'],
        ];
    }

    public function getReviewBlocks()
    {
        return $this->hasMany(CareerReview::class, ['object_id' => 'id'])
            ->onCondition(['type' => CareerReview::CAREER_REVIEW]);
    }

    public function getSubMenus()
    {
        return $this->hasMany(CareerSubMenu::class, ['object_id' => 'id'])
            ->onCondition(['type' => CareerSubMenu::TYPE_CAREER]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
