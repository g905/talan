<?php

namespace backend\modules\hr\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * VacanciesPageSearch represents the model behind the search form about `VacanciesPage`.
 */
class VacanciesPageSearch extends VacanciesPage
{
    public function rules()
    {
        return [
            [['id', 'city_id', 'manager_id', 'view_count', 'position', 'published'], 'integer'],
            [['label', 'alias', 'form_label', 'form_description', 'form_agreement', 'form_emails', 'form_onsubmit'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = VacanciesPageSearch::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'manager_id' => $this->manager_id,
            'view_count' => $this->view_count,
            'position' => $this->position,
            'published' => $this->published,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'form_emails', $this->form_emails])
            ->andFilterWhere(['like', 'form_onsubmit', $this->form_onsubmit]);

        return $dataProvider;
    }
}
