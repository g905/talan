<?php

namespace backend\modules\hr\models;

use backend\modules\block\models\block\VacancyFaqItem;
use backend\modules\block\models\block\VacancyPageFaqItem;
use yii\behaviors\TimestampBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\models\VacanciesPage as CommonVacanciesPage;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\modules\contacts\models\Manager;
use backend\modules\menu\models\VacanciesPageSubMenu;

/**
 * This is the model class for table "{{%vacancies_page}}".
 *
 * @property City $city
 * @property Manager $manager
 */
class VacanciesPage extends CommonVacanciesPage implements BackendModel
{
    public function rules()
    {
        return [
            [['city_id', 'manager_id', 'view_count', 'position', 'published', 'created_at', 'updated_at'], 'integer'],
            [['label', 'alias'], 'required'],
            [['form_description', 'form_emails'], 'string'],
            [['label', 'alias', 'form_label', 'form_agreement', 'form_onsubmit'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::class, 'targetAttribute' => ['city_id' => 'id']],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => Manager::class, 'targetAttribute' => ['manager_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'vacancies-page'),
            'city_id' => bt('City ID', 'vacancies-page'),
            'label' => bt('Label', 'vacancies-page'),
            'alias' => bt('Alias', 'vacancies-page'),
            'manager_id' => bt('Manager ID', 'vacancies-page'),
            'form_label' => bt('Form Label', 'vacancies-page'),
            'form_description' => bt('Form Description', 'vacancies-page'),
            'form_agreement' => bt('Form Agreement', 'vacancies-page'),
            'form_emails' => bt('Form Emails', 'vacancies-page'),
            'form_onsubmit' => bt('Form Onsubmit', 'vacancies-page'),
            'view_count' => bt('View Count', 'vacancies-page'),
            'position' => bt('Position', 'vacancies-page'),
            'published' => bt('Published', 'vacancies-page'),
            'created_at' => bt('Created At', 'vacancies-page'),
            'updated_at' => bt('Updated At', 'vacancies-page'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'seo' => MetaTagBehavior::class,
        ];
    }

    public function getTitle()
    {
        return bt('Vacancies page', 'vacancy-page');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    StdColumn::city(),
                    'label',
                    // 'alias',
                    // 'content:ntext',
                    StdColumn::manager(),
                    StdColumn::viewCount(),
                    StdColumn::published(),
                    StdColumn::position(),
                    // 'form_emails:ntext',
                    // 'form_onsubmit',
                    StdColumn::actionColumn()
                ];
            case 'view':
                return [
                    'id',
                    'city_id',
                    'label',
                    'alias',
                    'manager_id',
                    'view_count',
                    'published:boolean',
                    'position',
                    'form_emails:html',
                    'form_onsubmit',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new VacanciesPageSearch();
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General', 'vacancies-page') => [
                    'label' =>StdInput::label($this),
                    'alias' => StdInput::alias($this),
                    'city_id' => StdInput::citySelect(),
                    'view_count' => StdInput::text(),
                    'position' => StdInput::text(),
                    'published' => StdInput::text(),
                ],
                bt('Form block', 'vacancies-page') => [
                    'manager_id' => StdInput::managerSelect(),
                    'form_label' => StdInput::text(),
                    'form_description' => StdInput::editor(),
                    'form_agreement' => StdInput::editor(),
                    'form_emails' => StdInput::textArea(),
                    'form_onsubmit' => StdInput::text(),
                ],
                bt('Menu', 'vacancies-page') => [
                    $this->getRelatedFormConfig()['sub_menu'],
                ],
                bt('Faq', 'vacancy') => [
                    //'faq_label' => StdInput::text(),
                    $this->getRelatedFormConfig()['faq_blocks'],
                ],
            ],
        ];
    }

    public function getRelatedFormConfig()
    {
        return [
            'sub_menu' => ['relation' => 'subMenus'],
            'faq_blocks' => ['relation' => 'faqBlocks'],
        ];
    }

    public function getFaqBlocks()
    {
        return $this->hasMany(VacancyPageFaqItem::class, ['object_id' => 'id'])
            ->onCondition(['type' => VacancyPageFaqItem::VACANCY_PAGE_FAQ_ITEM]);
    }

    public function getSubMenus()
    {
        return $this->hasMany(VacanciesPageSubMenu::class, ['object_id' => 'id'])
            ->orderBy(['position' => SORT_ASC])
            ->onCondition(['type' => VacanciesPageSubMenu::TYPE_VACANCIES_PAGE]);
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }
}
