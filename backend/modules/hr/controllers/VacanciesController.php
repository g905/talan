<?php

namespace backend\modules\hr\controllers;

use backend\components\BackendController;
use backend\modules\hr\models\VacanciesPage;

/**
 * VacanciesController implements the CRUD actions for VacanciesPage model.
 */
class VacanciesController extends BackendController
{
    public function getModelClass()
    {
        return VacanciesPage::class;
    }
}
