<?php

namespace backend\modules\hr\controllers;

use backend\components\BackendController;
use backend\modules\hr\models\Career;

/**
 * CareerController implements the CRUD actions for Career model.
 */
class CareerController extends BackendController
{
    public function getModelClass()
    {
        return Career::class;
    }
}
