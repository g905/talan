<?php

namespace backend\modules\hr\controllers;

use backend\components\BackendController;
use backend\modules\hr\models\Specialization;

/**
 * SpecializationController implements the CRUD actions for Specialization model.
 */
class SpecializationController extends BackendController
{
    public function getModelClass()
    {
        return Specialization::class;
    }
}
