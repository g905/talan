<?php

namespace backend\modules\hr\controllers;

use backend\components\BackendController;
use backend\modules\hr\models\Vacancy;

/**
 * VacancyController implements the CRUD actions for Vacancy model.
 */
class VacancyController extends BackendController
{
    public function getModelClass()
    {
        return Vacancy::class;
    }
}
