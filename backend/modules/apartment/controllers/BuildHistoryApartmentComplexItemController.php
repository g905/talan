<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\BuildHistoryApartmentComplexItem;

/**
 * BuildHistoryApartmentComplexItemController implements the CRUD actions for BuildHistoryApartmentComplexItem model.
 */
class BuildHistoryApartmentComplexItemController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BuildHistoryApartmentComplexItem::className();
    }
}
