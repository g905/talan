<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\ApartmentMap;

/**
 * ApartmentMapController implements the CRUD actions for ApartmentMap model.
 */
class ApartmentMapController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ApartmentMap::class;
    }
}
