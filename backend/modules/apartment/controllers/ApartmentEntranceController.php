<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\ApartmentEntrance;

class ApartmentEntranceController extends BackendController
{
  /**
   * @return string
   */
  public function getModelClass()
  {
      return ApartmentEntrance::className();
  }

}
