<?php

namespace backend\modules\apartment\controllers;

use backend\modules\apartment\models\BuildHistoryApartmentComplex;
use backend\components\BackendController;

/**
 * ApartmentComplexController implements the CRUD actions for ApartmentComplex model.
 */
class BuildHistoryApartmentComplexController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BuildHistoryApartmentComplex::class;
    }

    /**
     * @param int $id
     * @return mixed|string
     */
    public function actionDelete($id)
    {
        return 'Not supported';
    }
}
