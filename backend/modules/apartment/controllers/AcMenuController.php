<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\AcMenu;

/**
 * AcMenuController implements the CRUD actions for AcMenu model.
 */
class AcMenuController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return AcMenu::className();
    }
}
