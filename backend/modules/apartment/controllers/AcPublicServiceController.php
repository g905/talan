<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\AcPublicService;

/**
 * AcPublicServiceController implements the CRUD actions for AcPublicService model.
 */
class AcPublicServiceController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return AcPublicService::className();
    }
}
