<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\PromoAction;
use yii\db\Query;
use yii\web\Response;

/**
 * PromoActionController implements the CRUD actions for PromoAction model.
 */
class PromoActionController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PromoAction::className();
    }

    public function actionGetSelectItems($search = null, $id = null, $showNotChoose = false, $clearText = null)
    {
        if (!isset($clearText)){
            $clearText = \Yii::t('back/back/apartment-complex', 'Choose apartment complex');
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($search)) {
            $search = '';
        }

        $query = new Query();
        $query->select('id, label AS text')
            ->from(PromoAction::tableName())
            ->where(['like', 'label', $search])
            ->limit(5);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => PromoAction::find($id)->label];
        }

        if ($showNotChoose){
            $notChooseValue = ['id' => '0', 'text' => $clearText];
            array_unshift($out['results'] , $notChooseValue);
        }
        return $out;
    }
}
