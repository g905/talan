<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\HowBuy;

/**
 * HowBuyController implements the CRUD actions for HowBuy model.
 */
class HowBuyController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return HowBuy::className();
    }
}
