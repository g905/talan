<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\ComplexList;

/**
 * ComplexListController implements the CRUD actions for ComplexList model.
 */
class ComplexListController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ComplexList::class;
    }
}
