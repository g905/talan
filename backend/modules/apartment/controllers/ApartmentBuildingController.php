<?php

namespace backend\modules\apartment\controllers;

use Yii;
use backend\components\BackendController;
use common\models\ApartmentBuilding as CommonApartmentBuilding;
use common\models\ApartmentEntrance as CommonApartmentEntrance;
use backend\modules\apartment\models\ApartmentBuilding;
use yii\db\Query;
use yii\web\Response;

/**
 * ApartmentBuildingController implements the CRUD actions for ApartmentBuilding model.
 */
class ApartmentBuildingController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ApartmentBuilding::className();
    }

    public function actionGetSelectItems($search = null, $id = null, $showNotChoose = false, $clearText = null)
    {
        if (!isset($clearText)){
            $clearText = Yii::t('back/back/apartment-complex', 'Choose apartment complex');
        }
        response()->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($search)) {
            $search = '';
        }

        $query = new Query();
        $query->select('id, label AS text')
            ->from(CommonApartmentBuilding::tableName())
            ->where(['like', 'label', $search])
            ->limit(5);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CommonApartmentBuilding::find($id)->label];
        }

        if ($showNotChoose){
            $notChooseValue = ['id' => '0', 'text' => $clearText];
            array_unshift($out['results'] , $notChooseValue);
        }
        return $out;
    }

    public function actionGetSelectItems2($search = null, $id = null, $showNotChoose = false, $clearText = null)
    {
        if (!isset($clearText)){
            $clearText = Yii::t('back/back/apartment-complex', 'Choose apartment entr');
        }
        response()->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($search)) {
            $search = '';
        }

        $query = new Query();
        $query->select('id, entrance AS text')
            ->from(CommonApartmentEntrance::tableName())
            ->where(['like', 'entrance', $search])
            ->limit(5);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CommonApartmentEntrance::find($id)->entrance];
        }

        if ($showNotChoose){
            $notChooseValue = ['id' => '0', 'text' => $clearText];
            array_unshift($out['results'] , $notChooseValue);
        }
        return $out;
    }
}
