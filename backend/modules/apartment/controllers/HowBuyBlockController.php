<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\HowBuyBlock;

/**
 * HowBuyBlockController implements the CRUD actions for HowBuyBlock model.
 */
class HowBuyBlockController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return HowBuyBlock::className();
    }
}
