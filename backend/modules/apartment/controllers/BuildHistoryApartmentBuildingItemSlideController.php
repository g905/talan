<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\BuildHistoryApartmentBuildingItemSlide;

class BuildHistoryApartmentBuildingItemSlideController extends BackendController
{
    public function getModelClass()
    {
        return BuildHistoryApartmentBuildingItemSlide::class;
    }
}
