<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\PromoActionToAc;

/**
 * PromoActionToAcController implements the CRUD actions for PromoActionToAc model.
 */
class PromoActionToAcController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PromoActionToAc::className();
    }
}
