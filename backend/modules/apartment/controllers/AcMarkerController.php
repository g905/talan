<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\AcMarker;

/**
 * AcMarkerController implements the CRUD actions for AcMarker model.
 */
class AcMarkerController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return AcMarker::className();
    }
}
