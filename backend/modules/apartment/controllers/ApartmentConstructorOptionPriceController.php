<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\ApartmentConstructorOptionPrice;

/**
 * ApartmentConstructorPriceController implements the CRUD actions for ApartmentConstructorPrice model.
 */
class ApartmentConstructorOptionPriceController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ApartmentConstructorOptionPrice::className();
    }
}
