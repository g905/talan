<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\Apartment;

/**
 * ApartmentController implements the CRUD actions for Apartment model.
 */
class ApartmentController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Apartment::className();
    }
}
