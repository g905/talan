<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\ApartmentConstructorPrice;

/**
 * ApartmentConstructorPriceController implements the CRUD actions for ApartmentConstructorPrice model.
 */
class ApartmentConstructorPriceController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ApartmentConstructorPrice::className();
    }
}
