<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\AcAdvantages;

/**
 * AcAdvantagesController implements the CRUD actions for AcAdvantages model.
 */
class AcAdvantagesController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return AcAdvantages::className();
    }
}
