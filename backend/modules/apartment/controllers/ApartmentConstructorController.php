<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\ApartmentConstructor;

/**
 * ApartmentConstructorController implements the CRUD actions for ApartmentConstructor model.
 */
class ApartmentConstructorController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ApartmentConstructor::className();
    }
}
