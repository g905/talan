<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\CompletedProject;
use yii\web\Response;

/**
 * CompletedProjectController implements the CRUD actions for CompletedProject model.
 */
class CompletedProjectController extends BackendController
{
    public function getModelClass()
    {
        return CompletedProject::class;
    }

    /**
     * Action used together with Select2 dropdown widget.
     *
     * @param null $search
     * @param null $id
     * @param bool $showNotChoose
     * @param null $clearText
     * @return array
     */
    public function actionSearch($search = null, $id = null, $showNotChoose = false, $clearText = null)
    {
        if (!isset($clearText)){
            $clearText = bt('Choose completed project', 'completed-project');
        }
        response()->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($search)) {
            $search = '';
        }

        $query = CompletedProject::find()
            ->select(['id', 'text' => 'title'])
            ->having(['like', 'text', $search])
            ->limit(10)
            ->asArray();
        $out['results'] = array_values($query->all());

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CompletedProject::findOne($id)->title ?? null];
        }

        if ($showNotChoose){
            $notChooseValue = ['id' => '0', 'text' => $clearText];
            array_unshift($out['results'] , $notChooseValue);
        }
        return $out;
    }
}
