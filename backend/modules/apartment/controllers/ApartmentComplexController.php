<?php

namespace backend\modules\apartment\controllers;

use Yii;
use yii\db\Query;
use yii\web\Response;
use backend\assets\MapAsset;
use backend\components\BackendController;
use backend\modules\apartment\models\ApartmentComplex;

/**
 * ApartmentComplexController implements the CRUD actions for ApartmentComplex model.
 */
class ApartmentComplexController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ApartmentComplex::class;
    }

    public function actionUpdate($id)
    {
        MapAsset::register(view());
        return parent::actionUpdate($id);
    }

    public function actionCreate()
    {
        MapAsset::register(view());
        return parent::actionCreate();
    }


    public function actionGetSelectItems($search = null, $id = null, $showNotChoose = false, $clearText = null)
    {
        if (!isset($clearText)){
            $clearText = Yii::t('back/back/apartment-complex', 'Choose apartment complex');
        }
        response()->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($search)) {
            $search = '';
        }

        $query = new Query();
        $query->select('id, label AS text')
            ->from(ApartmentComplex::tableName())
            ->where(['like', 'label', $search])
            ->limit(5);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => ApartmentComplex::find($id)->label];
        }

        if ($showNotChoose){
            $notChooseValue = ['id' => '0', 'text' => $clearText];
            array_unshift($out['results'] , $notChooseValue);
        }
        return $out;
    }
}
