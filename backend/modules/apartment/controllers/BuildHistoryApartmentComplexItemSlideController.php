<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\BuildHistoryApartmentComplexItemSlide;

/**
 * BuildHistoryApartmentComplexItemSlideController implements the CRUD actions for BuildHistoryApartmentComplexItemSlide model.
 */
class BuildHistoryApartmentComplexItemSlideController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BuildHistoryApartmentComplexItemSlide::className();
    }
}
