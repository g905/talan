<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\BuildHistoryApartmentBuilding;

class BuildHistoryApartmentBuildingController extends BackendController
{
    public function getModelClass()
    {
        return BuildHistoryApartmentBuilding::class;
    }

    public function actionDelete($id)
    {
        return 'Not supported';
    }
}
