<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\PromoActionToAb;

/**
 * PromoActionToAbController implements the CRUD actions for PromoActionToAb model.
 */
class PromoActionToAbController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PromoActionToAb::className();
    }
}
