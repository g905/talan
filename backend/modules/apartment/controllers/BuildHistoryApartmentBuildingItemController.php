<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\BuildHistoryApartmentBuildingItem;

class BuildHistoryApartmentBuildingItemController extends BackendController
{
    public function getModelClass()
    {
        return BuildHistoryApartmentBuildingItem::class;
    }
}
