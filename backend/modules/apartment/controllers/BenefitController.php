<?php

namespace backend\modules\apartment\controllers;

use backend\components\BackendController;
use backend\modules\apartment\models\Benefit;
use yii\web\Response;

/**
 * Class BenefitController
 *
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package backend\modules\apartment\controllers
 */
class BenefitController extends BackendController
{
    public function getModelClass()
    {
        return Benefit::class;
    }

    /**
     * Action used together with Select2 dropdown widget.
     *
     * @param null $search
     * @param null $id
     * @param bool $showNotChoose
     * @param null $clearText
     * @return array
     */
    public function actionSearch($search = null, $id = null, $showNotChoose = false, $clearText = null)
    {
        if (!isset($clearText)){
            $clearText = bt('Choose benefit', 'benefit');
        }
        response()->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($search)) {
            $search = '';
        }

        $query = Benefit::find()
            ->select(['id', 'text' => 'title'])
            ->having(['like', 'text', $search])
            ->limit(10)
            ->asArray();
        $out['results'] = array_values($query->all());

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Benefit::findOne($id)->title ?? null];
        }

        if ($showNotChoose){
            $notChooseValue = ['id' => '0', 'text' => $clearText];
            array_unshift($out['results'] , $notChooseValue);
        }
        return $out;
    }
}
