<?php

namespace backend\modules\apartment\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PromoActionToAcSearch represents the model behind the search form about `PromoActionToAc`.
 */
class PromoActionToAcSearch extends PromoActionToAc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'ac_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PromoActionToAcSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'action_id' => $this->action_id,
            'ac_id' => $this->ac_id,
        ]);

        return $dataProvider;
    }
}
