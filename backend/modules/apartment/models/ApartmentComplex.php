<?php

namespace backend\modules\apartment\models;

use backend\components\StylingActionColumn;
use backend\modules\apartment\models\ApartmentComplexWidget;
use Yii;
use yii\behaviors\TimestampBehavior;
use conquer\codemirror\CodemirrorAsset;
use conquer\codemirror\CodemirrorWidget;
use metalguardian\formBuilder\ActiveFormBuilder;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\helpers\Property;
use common\models\EntityToFile;
use common\components\CommonDataModel;
use common\components\model\ActiveRecord;
use backend\helpers\StdColumn;
use backend\helpers\Select2Helper;
use backend\components\FormBuilder;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\components\ManyToManyBehavior;
use backend\validators\InputScriptValidator;
use backend\modules\contacts\models\Manager;
use backend\modules\request\models\CustomPopup;
use backend\modules\page\models\PublicDataWidget;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\modules\block\models\block\ApartmentComplexSlideVideo;

/**
 * This is the model class for table "{{%apartment_complex}}".
 *
 * @property integer $id
 * @property integer $type
 * @property string $label
 * @property string $top_screen_btn_label
 * @property string $top_screen_btn_link
 * @property string $general_progress_title
 * @property string $general_progress_percent
 * @property string $general_description
 * @property string $general_docs_link
 * @property string $general_docs_link_title
 * @property string $general_fb_link
 * @property string $general_vk_link
 * @property string $general_ig_link
 * @property string $form_top_title
 * @property string $form_top_description
 * @property string $form_top_emails
 * @property string $advantages_title
 * @property string $map_title
 * @property string $map_subtitle
 * @property string $map_description
 * @property string $public_service_title
 * @property string $architecture_title
 * @property string $architecture_fio
 * @property string $architecture_job_position
 * @property string $architecture_description
 * @property string $form_bottom_title
 * @property string $form_bottom_description
 * @property string $form_bottom_email
 * @property integer $manager_id
 * @property string $filter_price_deviation
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $city_id
 * @property integer $view_count
 * @property double $lat
 * @property double $lng
 * @property string $alias
 * @property boolean $show_on_home
 * @property integer $position_on_home
 * @property string $common_script
 * @property string $guid
 * @property integer $map_zoom
 * @property string $choose_apartment_onclick
 * @property string $form_top_onsubmit
 * @property string $form_bottom_onsubmit
 * @property integer $custom_popup_id
 * @property string $build_start_date
 * @property string $build_end_date
 * @property string $build_video
 * @property string $build_history_end_text
 * @property string $build_history_current_text
 * @property string $build_history_title
 * @property string $btn_label
 * @property string $btn_link
 * @property boolean $show_label
 * @property boolean $show_prices
 * @property string $cta_black_label
 * @property string $cta_white_label
 * @property string $cta_description
 * @property string $cta_button_label
 * @property string $cta_button_link
 * @property int $cta_published
 * @property int $room_1
 * @property int $room_2
 * @property int $room_3
 * @property int $room_4
 * @property int $show_calc
 * @property string $submit_apartment_script
 *
 * @property Manager $manager
 * @property City $city
 */
class ApartmentComplex extends ActiveRecord implements BackendModel
{
    public $promoIds;
    public $promoIdsValue;

    /**
     * @var mixed attributes for imageUploader.
     */
    public $top_screen_background_image;
    public $top_screen_background_video;
    public $general_photos;
    public $architecture_photo;
    public $architecture_bottom_photo;
    public $widget_call_image;
    public $view_documentation;

    /**
     * Temporary sign which used for saving images before model save
     * @var string
     */
    public $sign;

    public static function tableName()
    {
        return '{{%apartment_complex}}';
    }

    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = security()->generateRandomString();
        }
    }

    public function rules()
    {
        return [
            [['label', 'city_id', 'lat', 'lng', 'alias', 'type'], 'required'],
            [
                [
                    'general_description',
                    'form_top_description',
                    'form_top_emails',
                    'map_description',
                    'architecture_description',
                    'form_bottom_description',
                    'form_bottom_email',
                    'common_script',
                    'guid',
                    'cta_black_label',
                    'cta_white_label',
                    'cta_description',
                    'cta_button_label',
                    'cta_button_link',
                    'room_1',
                    'room_2',
                    'room_3',
                    'room_4',
                    'director',
                    'address',
                    'custom_choose_label',
                    'custom_choose_link',
                    'map_image',
                    'apartment_chess_text',
                    'button_view_complex_text',
                    'button_view_complex_link',
					'special_offer_description',
                ],
                'string'
            ],
            [['common_script', 'head_script'], InputScriptValidator::class],
            [['lat', 'lng'], 'number'],
            [['show_choose_house_block', 'manager_id', 'published', 'custom_choose_active', 'position', 'view_count', 'show_on_home', 'position_on_home', 'type', 'map_zoom', 'custom_popup_id', 'cta_published', 'show_calc', 'guarantee_list'], 'integer'],
            [['filter_price_deviation'], 'number',  'min' => 0, 'max' => 100],
            [['address'], 'string', 'max' => 15],
            [['director'], 'string', 'max' => 20],
            [['general_progress_percent'], 'number',  'min' => 0, 'max' => 100],
            [
                [
                    'label',
                    'alias',
                    'top_screen_btn_label',
                    'top_screen_btn_link',
                    'general_progress_title',
                    'general_docs_link',
                    'general_docs_link_title',
                    'general_fb_link',
                    'general_vk_link',
                    'general_ig_link',
                    'form_top_title',
                    'advantages_title',
                    'map_title',
                    'map_subtitle',
                    'public_service_title',
                    'architecture_title',
                    'architecture_fio',
                    'architecture_job_position',
                    'form_bottom_title',
                    'choose_apartment_onclick',
                    'form_top_onsubmit',
                    'form_bottom_onsubmit',
                    'btn_label',
                    'btn_link',
                    'submit_apartment_script',
					'special_offer_text',
					'special_offer_link',
                    'choose_house_title',
                    'choose_house_subtitle'
                ],
                'string',
                'max' => 255
            ],
            [['manager_id'], 'exist', 'targetClass' => \common\models\Manager::class, 'targetAttribute' => 'id'],
            [['show_choose_house_block', 'published', 'show_label', 'show_calc', 'custom_choose_active'], 'default', 'value' => 1],
            [['is_sale'], 'default', 'value' => 0],
            [['cta_published', 'guarantee_list'], 'default', 'value' => 0],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
            [['promoIds'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'apartment-complex'),
            'type' => bt('property type', 'apartment-complex'),
            'label' => bt('label', 'apartment-complex'),
            'city_id' => bt('city', 'apartment-complex'),
            'top_screen_btn_label' => bt('top screen btn label', 'apartment-complex'),
            'top_screen_btn_link' => bt('top screen btn link', 'apartment-complex'),
            'general_progress_title' => bt('progress title', 'apartment-complex'),
            'general_progress_percent' => bt('progress percent', 'apartment-complex'),
            'general_description' => bt('description', 'apartment-complex'),
            'general_docs_link' => bt('docs link', 'apartment-complex'),
            'general_docs_link_title' => bt('docs link title', 'apartment-complex'),
            'general_fb_link' => bt('fb link', 'apartment-complex'),
            'general_vk_link' => bt('vk link', 'apartment-complex'),
            'general_ig_link' => bt('instagram link', 'apartment-complex'),
            'form_top_title' => bt('form top title', 'apartment-complex'),
            'form_top_description' => bt('form top description', 'apartment-complex'),
            'form_top_emails' => bt('form top emails', 'apartment-complex'),
            'advantages_title' => bt('advantages title', 'apartment-complex'),
            'map_title' => bt('map title', 'apartment-complex'),
            'map_subtitle' => bt('map subtitle', 'apartment-complex'),
            'map_description' => bt('map description', 'apartment-complex'),
            'public_service_title' => bt('public service title', 'apartment-complex'),
            'architecture_title' => bt('title', 'apartment-complex'),
            'architecture_fio' => bt('fio', 'apartment-complex'),
            'architecture_job_position' => bt('job position', 'apartment-complex'),
            'architecture_description' => bt('description', 'apartment-complex'),
            'form_bottom_title' => bt('form bottom title', 'apartment-complex'),
            'form_bottom_description' => bt('form bottom description', 'apartment-complex'),
            'form_bottom_email' => bt('form bottom email', 'apartment-complex'),
            'manager_id' => bt('manager', 'apartment-complex'),
            'filter_price_deviation' => bt('price deviation', 'apartment-complex'),
            'published' => bt('Published', 'apartment-complex'),
            'position' => bt('Position', 'apartment-complex'),
            'created_at' => bt('Created At', 'apartment-complex'),
            'updated_at' => bt('Updated At', 'apartment-complex'),
            'submit_apartment_script' => bt('Default onsubmit script for apartments', 'apartment-complex'),
            'top_screen_background_image' => bt('Top screen background image', 'apartment-complex'),
			'view_documentation' => 'Файл скачать презентацию',
            'top_screen_background_video' => bt('Top screen background video', 'apartment-complex'),
            'general_photos' => bt('photos', 'apartment-complex'),
            'architecture_photo' => bt('Architecture photo', 'apartment-complex'),
            'architecture_bottom_photo' => bt('Architecture bottom photo', 'apartment-complex'),
            'show_on_home' => bt('Show on home', 'apartment-complex'),
            'position_on_home' => bt('Position on home', 'apartment-complex'),
            'promoIds' => bt('Promo actions', 'apartment-complex'),
            'common_script' => bt('Common script', 'apartment-complex'),
            'head_script' => bt('Head script', 'apartment-complex'),
            'map_zoom' => bt('Map zoom'),
            'choose_apartment_onclick' => bt('Choose apartment onclick', 'apartment-complex'),
            'form_top_onsubmit' => bt('Form top_onsubmit', 'apartment-complex'),
            'form_bottom_onsubmit' => bt('Form bottom onsubmit', 'apartment-complex'),
            'custom_popup_id' => bt('Custom popup', 'apartment-complex'),
            'show_label' => bt('Show label', 'apartment-complex'),
            'show_prices' => bt('Show prices', 'apartment-complex'),
            'btn_label' => bt('Button label', 'apartment-complex'),
            'btn_link' => bt('Button link', 'apartment-complex'),
            'cta_black_label' => bt('Cta black label', 'apartment-complex'),
            'cta_white_label' => bt('Cta white label', 'apartment-complex'),
            'cta_description' => bt('Cta description', 'apartment-complex'),
            'cta_button_label' => bt('Cta button label', 'apartment-complex'),
            'cta_button_link' => bt('Cta button link', 'apartment-complex'),
            'cta_published' => bt('Cta published', 'apartment-complex'),
            'room_1' => bt('One Room Tour', 'apartment-complex'),
            'room_2' => bt('Two Room Tour', 'apartment-complex'),
            'room_3' => bt('Three Room Tour', 'apartment-complex'),
            'room_4' => bt('Four Room Tour', 'apartment-complex'),
            'show_calc' => bt('Show Calculator', 'apartment-complex'),
            'is_sale' => bt('Sale', 'apartment-complex'),
            'director' => bt('Director', 'apartment-complex'),
            'address' => bt('Address', 'apartment-complex'),
            'custom_choose_label' => bt('Custom choose label', 'apartment-complex'),
            'custom_choose_link' => bt('Custom choose link', 'apartment-complex'),
            'custom_choose_active' => bt('Custom choose active', 'apartment-complex'),
            'map_image' => bt('Map Image', 'apartment-complex'),
            'guarantee_list' => bt('Show on Guarantee page', 'apartment-complex'),
            'view_complex_text' => bt('Text for view complex button', 'apartment-complex'),
            'choose_house_title' => bt('Text for choose house title', 'apartment-complex'),
            'choose_house_subtitle' => bt('Text for choose house subtitle', 'apartment-complex'),
			'special_offer_text' => 'Текст для кнопки спец.предложение',
			'special_offer_link' => 'Ссылка для кнопки спец.предложение',
			'special_offer_description' => 'Текст описание спец.предложения',
            'show_choose_house_block' => 'Показывать блок с выбором дома'
        ];
    }


    public function attributeHints()
    {
        return [
            'common_script' => 'Должно начинаться с <code>&lt;script</code> и заканчиваться <code>script&gt;</code>',
            'head_script' => 'Должно начинаться с <code>&lt;script</code> и заканчиваться <code>script&gt;</code>'
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
            'seo' => [
                'class' => MetaTagBehavior::class,
            ],
            'manyToManyRelation' => [
                'class' => ManyToManyBehavior::class,
                'currentObj' => $this,
                'currentRelatedColumn' => 'ac_id',
                'manyToManyConfig' => [
                    'promoAction' => [
                        'attribute' => 'promoIds',
                        'valueAttribute' => 'promoIdsValue',
                        'relatedColumn' => 'action_id',
                        'linkTableName' => PromoActionToAc::tableName(),
                        'relatedObjTableName' => PromoAction::tableName(),
                        'isStringReturn' => false
                    ],
                ]
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getTitle()
    {
        return bt('Apartment Complex', 'apartment-complex');
    }

    public function getDduChart()
    {
        return $this->hasOne(PublicDataWidget::class, ['complex_id' => 'id'])->where(['type' => PublicDataWidget::TYPE_DIAGRAM_DDU]);
    }

    public function getScheduleChart()
    {
        return $this->hasOne(PublicDataWidget::class, ['complex_id' => 'id'])->where(['type' => PublicDataWidget::TYPE_DIAGRAM_SCHEDULE]);
    }

    /**
     * @return string
     */
    public static function getPromoUrl()
    {
        return '/apartment/promo-action/get-select-items';
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    StdColumn::image('general_photos', 'generalPhoto'),
                    'label',
                    StdColumn::propertyType(),
                    StdColumn::city(),
                    StdColumn::percentage('general_progress_percent'),
                    // 'general_docs_link_title:url',
                    // 'general_fb_link:url',
                    // 'general_vk_link:url',
                    // 'general_ig_link:url',
                    // 'form_top_title',
                    // 'form_top_description:ntext',
                    // 'form_top_emails:ntext',
                    // 'advantages_title',
                    // 'map_title',
                    // 'map_subtitle',
                    // 'map_description:ntext',
                    // 'public_service_title',
                    // 'architecture_title',
                    // 'architecture_fio',
                    // 'architecture_job_position',
                    // 'architecture_description:ntext',
                    // 'form_bottom_title',
                    // 'form_bottom_description:ntext',
                    // 'form_bottom_email:ntext',
                    // 'filter_price_deviation',
                    StdColumn::manager(),
                    StdColumn::published(),
                    'is_sale:boolean',
                    StdColumn::boolean('show_on_home'),
                    StdColumn::boolean('show_prices'),
                    StdColumn::position(),
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                if ($model->type == Property::TYPE_RESIDENTIAL) {
                                    return StylingActionColumn::createPreviewUrl($url, $model, 'apartment-complex');
                                } else {
                                    return StylingActionColumn::createPreviewUrl($url, $model, 'commercial-complex');
                                }
                            }
                        ]
                    ]
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    'top_screen_btn_label',
                    'top_screen_btn_link:url',
                    'general_progress_title',
                    'general_progress_percent',
                    'btn_label',
                    'btn_link',
                    [
                        'attribute' => 'general_description',
                        'format' => 'html',
                    ],
                    'general_docs_link:url',
                    'general_docs_link_title:url',
                    'general_fb_link:url',
                    'general_vk_link:url',
                    'general_ig_link:url',
                    'form_top_title',
                    [
                        'attribute' => 'form_top_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'form_top_emails',
                        'format' => 'html',
                    ],
                    'advantages_title',
                    'map_title',
                    'map_subtitle',
                    [
                        'attribute' => 'map_description',
                        'format' => 'html',
                    ],
                    'map_image',
                    'public_service_title',
                    'architecture_title',
                    'architecture_fio',
                    'architecture_job_position',
                    [
                        'attribute' => 'architecture_description',
                        'format' => 'html',
                    ],
                    'form_bottom_title',
                    [
                        'attribute' => 'form_bottom_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'form_bottom_email',
                        'format' => 'html',
                    ],
                    'manager_id',
                    'filter_price_deviation',
                    'published:boolean',
                    'guarantee_list:boolean',
                    'show_label:boolean',
                    'show_prices:boolean',
                    'position',
					'special_offer_text',
					'special_offer_link',
					'special_offer_description',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ApartmentComplexSearch();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentComplexMenu()
    {
        return $this->hasMany(AcMenu::class, ['apartment_complex_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentComplexAdvantages()
    {
        return $this->hasMany(AcAdvantages::class, ['apartment_complex_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentComplexMarker()
    {
        return $this->hasMany(AcMarker::class, ['apartment_complex_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentComplexPublicService()
    {
        return $this->hasMany(AcPublicService::class, ['apartment_complex_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getApartmentComplexWidget()
    {
        return $this->hasMany(ApartmentComplexWidget::className(), ['complex_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentComplexSliderVideo()
    {
        return $this->hasMany(ApartmentComplexSlideVideo::class, ['object_id' => 'id'])
            ->onCondition(['type' => ApartmentComplexSlideVideo::APARTMENT_COMPLEX_SLIDER_VIDEO]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneralPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'general_photos.entity_model_name' => 'ApartmentComplex',
                'general_photos.attribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_GENERAL_PHOTOS
            ])
            ->alias('general_photos')
            ->orderBy('general_photos.position DESC');
    }

    public function getFormConfig()
    {
        $mapId = uniqid();
        $config = [
            'form-set' => [
                Yii::t('back/apartment-complex', 'General') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\City::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'type' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => Property::types(),
                        'options' => ['prompt' => ''],
                    ],
                    'label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                            'class' => $this->isNewRecord ? 's_name form-control' : 'form-control'
                        ],
                    ],
                    'alias' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                            'class' => $this->isNewRecord ? 's_alias form-control' : 'form-control'
                        ],
                    ],
                    'director' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'address' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'show_label' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'show_prices' => [
                        'type' => FormBuilder::INPUT_CHECKBOX,
                    ],
                    'btn_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'btn_link' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'filter_price_deviation' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                            'iconRightContent' => CommonDataModel::getPercentText(),
                        ],
                    ],
                    'promoIds' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'label' => $this->getAttributeLabel('promoIds'),
                        'value' => Select2Helper::getAjaxSelect2Widget(
                            $this,
                            'promoIds',
                            static::getPromoUrl(),
                            $this->promoIdsValue,
                            [
                                'multiple' => true,
                            ]
                        )
                    ],
                    'show_on_home' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'position_on_home' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'choose_apartment_onclick' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'common_script' => [
                        //'hint' =>  addslashes(Yii::t('back/apartment-complex', 'Important: &lt;script&gt; &lt;/script&gt; wrapper is required')),
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => CodemirrorWidget::widget([
                            'model' => $this,
                            'attribute' => 'common_script',
                            'preset'=> 'javascript',
                            'assets' => [
                                CodemirrorAsset::THEME_SOLARIZED
                            ],
                            'settings' => [
                                'theme' => 'solarized',
                            ],
                            'options' => ['rows' => 20],
                        ]),
                    ],
                    'head_script' => [
                        //'hint' =>  addslashes(Yii::t('back/apartment-complex', 'Important: &lt;script&gt; &lt;/script&gt; wrapper is required')),
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => CodemirrorWidget::widget([
                            'model' => $this,
                            'attribute' => 'head_script',
                            'preset'=> 'javascript',
                            'assets' => [
                                CodemirrorAsset::THEME_SOLARIZED
                            ],
                            'settings' => [
                                'theme' => 'solarized',
                            ],
                            'options' => ['rows' => 20],
                        ]),
                    ],
                    'submit_apartment_script' => [
                        //'hint' =>  addslashes(Yii::t('back/apartment-complex', 'Important: &lt;script&gt; &lt;/script&gt; wrapper is required')),
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => CodemirrorWidget::widget([
                            'model' => $this,
                            'attribute' => 'submit_apartment_script',
                            'preset'=> 'javascript',
                            'assets' => [
                                CodemirrorAsset::THEME_SOLARIZED
                            ],
                            'settings' => [
                                'theme' => 'solarized',
                            ],
                            'options' => ['rows' => 20],
                        ]),
                    ],
                    'custom_popup_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => CustomPopup::getItems('id', 'white_label'),
                        'options' => [
                            'prompt' => '--',
                        ],
                    ],
                    'custom_choose_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'custom_choose_link' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'custom_choose_active' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'position' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [],
                    ],
                    'show_calc' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'is_sale' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'guarantee_list' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                    'apartment_chess_text' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'label' => Yii::t('back/apartment-complex', 'Apartment_chess_text'),
                    ],
                    'button_view_complex_text' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'button_view_complex_link' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'choose_house_title' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'choose_house_subtitle' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
					'special_offer_text' => [
						'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
						'options' => ['maxlength' => true],
					],
					'special_offer_link' => [
						'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
						'options' => ['maxlength' => true],
					],
					'special_offer_description' => [
						'type' => ActiveFormBuilder::INPUT_TEXTAREA,
						'options' => [
							'rows' => 4,
						],
					],
                    'show_choose_house_block' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
				],
                Yii::t('back/apartment-complex', 'Menu') => [
                    $this->getRelatedFormConfig()['apartment_complex_menu']
                ],
                /*Yii::t('back/apartment-complex', 'Top screen') => [
                    // 'top_screen_btn_label' => [
                    //     'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    //     'options' => [
                    //         'maxlength' => true,
                    //     ],
                    // ],
                    // 'top_screen_btn_link' => [
                    //     'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                    //     'options' => [
                    //         'maxlength' => true
                    //     ],
                    // ],
                    'top_screen_background_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'top_screen_background_image',
                            'saveAttribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'top_screen_background_video' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['mp4'],
                            'attribute' => 'top_screen_background_video',
                            'saveAttribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],*/
                Yii::t('back/apartment-complex', 'Complex info') => [
                    'top_screen_background_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'top_screen_background_image',
                            'saveAttribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'general_progress_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'general_progress_percent' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
					'view_documentation' => [
						'type' => ActiveFormBuilder::INPUT_RAW,
						'value' => ImageUpload::widget([
							'model' => $this,
							// 'allowedFileExtensions' => ['pdf'],
							'attribute' => 'view_documentation',
							'saveAttribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_VIEW_DOCUMENTATION,
							'aspectRatio' => false, //Пропорция для кропа
							'multiple' => false, //Вкл/выкл множественную загрузку
						])
					],
                    'general_docs_link' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'general_docs_link_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'general_fb_link' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'general_vk_link' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'general_ig_link' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'room_1' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'room_2' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'room_3' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'room_4' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],/*
                    'general_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::class,
                        'options' => [
                            'model' => $this,
                            'attribute' => 'general_description',
                        ]
                    ],*/
                ],
                Yii::t('back/apartment-complex', 'Photos') => [
                    'general_photos' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'general_photos',
                            'saveAttribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_GENERAL_PHOTOS,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => true, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> <i class="si si-grid"></i> '.Yii::t('back/apartment-complex', 'Videos').'</div>',
                        'label' => false,
                    ],
                    $this->getRelatedFormConfig()['apartment_complex_slide_video'],
                ],
                Yii::t('back/apartment-complex', 'Forms') => [
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> '.Yii::t('back/apartment-complex', 'Form - top').'</div>',
                        'label' => false,
                    ],
                    'form_top_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'form_top_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'form_top_description',
                        ]
                    ],
                    'form_top_emails' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'form_top_onsubmit' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'separator2' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> '.Yii::t('back/apartment-complex', 'Form - bottom').'</div>',
                        'label' => false,
                    ],
                    'form_bottom_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_bottom_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'form_bottom_description',
                        ]
                    ],
                    'form_bottom_email' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'form_bottom_onsubmit' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'manager_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\Manager::getItems('id', 'fio'),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                ],
                Yii::t('back/apartment-complex', 'Advantages') => [
                    'advantages_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> <i class="si si-grid"></i> '.Yii::t('back/apartment-complex', 'advantages - blocks').'</div>',
                        'label' => false,
                    ],
                    $this->getRelatedFormConfig()['apartment_complex_advantages'],
                ],
                Yii::t('back/apartment-complex', 'Map') => [
                    'map_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'map_subtitle' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'map_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::class,
                        'options' => [
                            'model' => $this,
                            'attribute' => 'map_description',
                        ]
                    ],
                    'map_zoom' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    [
                        'label' => false,
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div id="product_map_' . $mapId . '" class="product_map"></div>'
                    ],
                    'lat' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                            'readonly' => true,
                            'id' => 'product_map_lat_' . $mapId,
                            'class' => 'form-control',
                            'value' => $this->isNewRecord ? '56.8497600' : $this->lat
                        ],
                    ],
                    'lng' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'hint' => Yii::t(
                            'back/product-storehouse',
                            'To fill parameters of latitude and longitude, just click on the map'
                        ),
                        'options' => [
                            'readonly' => true,
                            'maxlength' => true,
                            'id' => 'product_map_lng_' . $mapId,
                            'class' => 'form-control',
                            'value' => $this->isNewRecord ? '53.2044800' : $this->lng
                        ],
                    ],
                    'map_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'map_image',
                            'saveAttribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_MAP_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> <i class="si si-grid"></i> '.Yii::t('back/apartment-complex', 'map - markers').'</div>',
                        'label' => false,
                    ],
                    
                    $this->getRelatedFormConfig()['apartment_complex_marker'],
                ],
                Yii::t('back/apartment-complex', 'Public service') => [
                    'public_service_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> <i class="si si-grid"></i> '.Yii::t('back/apartment-complex', 'public service - blocks').'</div>',
                        'label' => false,
                    ],
                    $this->getRelatedFormConfig()['apartment_complex_public_service'],
                ],
                Yii::t('back/apartment-complex', 'Architecture') => [
                    'architecture_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'architecture_fio' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'architecture_job_position' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'architecture_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'architecture_description',
                        ]
                    ],
                    'architecture_photo' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'architecture_photo',
                            'saveAttribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_ARCHITECTURE_PHOTO,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'architecture_bottom_photo' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'architecture_bottom_photo',
                            'saveAttribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_ARCHITECTURE_BOTTOM_PHOTO,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                Yii::t('back/apartment-complex', 'CTA') => [
                    'cta_black_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'cta_white_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'cta_description' => [
                        'type' => FormBuilder::INPUT_TEXTAREA,
                    ],
                    'cta_button_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'cta_button_link' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'cta_published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                ],
                Yii::t('back/apartment-complex', 'Widget') => [
                    'widget_call_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'widget_call_image',
                            'saveAttribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_WIDGET_CALL_IMAGE,
                            'aspectRatio' => false,
                            'multiple' => true,
                        ])
                    ],
                    $this->getRelatedFormConfig()['apartment_complex_widget'],
                ]
            ],
        ];

        return $config;
    }

    public function getRelatedFormConfig()
    {
        return [
            'apartment_complex_menu' => [
                'relation' => 'apartmentComplexMenu',
            ],
            'apartment_complex_advantages' => [
                'relation' => 'apartmentComplexAdvantages',
            ],
            'apartment_complex_marker' => [
                'relation' => 'apartmentComplexMarker',
            ],
            'apartment_complex_public_service' => [
                'relation' => 'apartmentComplexPublicService',
            ],
            'apartment_complex_slide_video' => [
                'relation' => 'apartmentComplexSliderVideo',
            ],
            'apartment_complex_widget' => [
                'relation' => 'apartmentComplexWidget',
                'limitMax' => 4,
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
