<?php

namespace backend\modules\apartment\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ApartmentEntranceSearch represents the model behind the search form about `ApartmentEntrance`.
 */
class ApartmentEntranceSearch extends ApartmentEntrance
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'entrance', 'position', 'building_id', 'building_complex_id'], 'integer'],
            [['published'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApartmentEntranceSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'entrance' => $this->entrance,
            'position' => $this->position,
            'building_id' => $this->building_id,
            'building_complex_id' => $this->building_complex_id
        ]);

        $query->andFilterWhere(['like', 'published', $this->published]);

        return $dataProvider;
    }
}
