<?php

namespace backend\modules\apartment\models;

use backend\modules\request\helpers\SearchHelper;
use Yii;
use Cake\Chronos\Chronos;
use kartik\grid\GridView;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;

/**
 * This is the model class for table "{{%build_history_apartment_complex_item}}".
 *
 * @property integer $id
 * @property integer $apartment_complex_id
 * @property string $date
 * @property string $percent
 * @property string $percent_plan
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ApartmentComplex $apartmentComplex
 */
class BuildHistoryApartmentComplexItem extends ActiveRecord implements BackendModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%build_history_apartment_complex_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['apartment_complex_id', 'date'], 'required'],
            [['apartment_complex_id'], 'integer'],
            [['date'], 'date'],
            ['date', 'validateSameMonthAndYear', 'when' => function($model){
                return $this->isNewRecord;
            }],
            [['percent'], 'number', 'min' => 0, 'max' => 100],
            [['percent_plan'], 'number', 'min' => 0, 'max' => 100],
            [['apartment_complex_id'], 'exist', 'targetClass' => \common\models\ApartmentComplex::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * Check if inputted year+month is not exists in DB
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function validateSameMonthAndYear()
    {
        $year = (integer)app()->formatter->asDate($this->date, 'YYYY');
        $month = (integer)app()->formatter->asDate($this->date, 'MM');
        $exist = self::find()
            ->where(['apartment_complex_id' => $this->apartment_complex_id])
            ->andWhere("MONTH(date) = $month AND YEAR(date) = $year")
            ->exists();
        if ($exist) {
            $this->addError('date', 'Данный месяц в этом году уже существует');
        }

        $complex = $this->apartmentComplex;

        if ($complex) {
            $current = Chronos::now();
            $date = new Chronos($this->date);
            $start = new Chronos($complex->build_start_date);
            $end = new Chronos($complex->build_end_date);

            $edge = $current->lte($end) ? $current : $end;

            if (!$date->between($start, $edge)) {
                $this->addError('date', 'Дата должна быть в диапазоне: '. $start->toDateString() . ' - ' . $edge->toDateString());
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/BHApartmentComplexItem', 'ID'),
            'apartment_complex_id' => Yii::t('back/BHApartmentComplexItem', 'Apartment complex'),
            'date' => Yii::t('back/BHApartmentComplexItem', 'Date (month + year)'),
            'percent' => Yii::t('back/BHApartmentComplexItem', 'Percent of completition'),
            'percent_plan' => Yii::t('back/BHApartmentComplexItem', 'Percent of completition plan'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'apartment_complex_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/BHApartmentComplexItem', 'Build History Apartment Complex Item');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'apartment_complex_id',
                        'value' => function(self $data) {
                            return $data->apartmentComplex->label ?? null;
                        },
                        'filter' => ApartmentComplex::getItems(),
                    ],
                    SearchHelper::getDateRangeColumnConfig($this, 'date'),
                    'percent',
                    'percent_plan',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'apartment_complex_id',
                        'value' => $this->apartmentComplex->label ?? null,
                    ],
                    'date',
                    'percent',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new BuildHistoryApartmentComplexItemSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'apartment_complex_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ApartmentComplex::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'date' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \metalguardian\dateTimePicker\Widget::class,
                'options' => [
                    'mode' => \metalguardian\dateTimePicker\Widget::MODE_DATE,
                    'options' => [
                        'class' => 'form-control',
                    ],
                ],
                'hint' => \Yii::t('back/BHApartmentComplexItem', 'Only month and year are important, day may be anyone'),
            ],
            'percent' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'percent_plan' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
        ];

        return $config;
    }


}
