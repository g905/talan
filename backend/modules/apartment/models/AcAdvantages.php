<?php

namespace backend\modules\apartment\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%ac_advantages}}".
 *
 * @property integer $id
 * @property integer $apartment_complex_id
 * @property string $title
 * @property string $description
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ApartmentComplex $apartmentComplex
 */
class AcAdvantages extends ActiveRecord implements BackendModel
{

    /**
    * Attribute for imageUploader
    */
    public $photo;

    /**
    * Temporary sign which used for saving images before model save
    * @var string
    */
    public $sign;

    public function init()
    {
        $this->published = 1;
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ac_advantages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['apartment_complex_id', 'published', 'position'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['apartment_complex_id'], 'exist', 'targetClass' => \common\models\ApartmentComplex::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
         ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/ac-advantages', 'ID'),
            'apartment_complex_id' => Yii::t('back/ac-advantages', 'apartment complex'),
            'title' => Yii::t('back/ac-advantages', 'title'),
            'description' => Yii::t('back/ac-advantages', 'description'),
            'published' => Yii::t('back/ac-advantages', 'Published'),
            'position' => Yii::t('back/ac-advantages', 'Position'),
            'created_at' => Yii::t('back/ac-advantages', 'Created At'),
            'updated_at' => Yii::t('back/ac-advantages', 'Updated At'),
            'photo' => Yii::t('back/ac-advantages', 'photo'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::className(), ['id' => 'apartment_complex_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/ac-advantages', 'Ac Advantages');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'apartment_complex_id',
                    'title',
                    // 'description:ntext',
                    'published:boolean',
                    'position',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'apartment_complex_id',
                    'title',
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new AcAdvantagesSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'title' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'description',
                ]
            ],
            'photo' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'photo',
                    'saveAttribute' => \common\models\AcAdvantages::SAVE_ATTRIBUTE_PHOTO,
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }


    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
