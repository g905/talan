<?php

namespace backend\modules\apartment\models;

use backend\components\ImperaviContent;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use kartik\grid\GridView;
use kartik\select2\Select2;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%build_history_apartment_complex_item_slide}}".
 *
 * @property integer $id
 * @property integer $build_history_apartment_complex_item_id
 * @property string $day
 * @property string $content
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property BuildHistoryApartmentComplexItem $buildHistoryApartmentComplexItem
 */
class BuildHistoryApartmentComplexItemSlide extends ActiveRecord implements BackendModel
{
    public $image;
    public $sign;

    /**
     * @throws \yii\base\Exception
     */
    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%build_history_apartment_complex_item_slide}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['build_history_apartment_complex_item_id'], 'integer'],
            [['day', 'build_history_apartment_complex_item_id'], 'required'],
            [['content'], 'string'],
            [['day'], 'integer', 'min' => 1, 'max' => 31],
            [['build_history_apartment_complex_item_id'], 'exist', 'targetClass' => \common\models\BuildHistoryApartmentComplexItem::class, 'targetAttribute' => 'id'],
            [['sign'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/BHApartmentComplexItemSlide', 'ID'),
            'build_history_apartment_complex_item_id' => Yii::t('back/BHApartmentComplexItemSlide', 'Build history apartment complex item'),
            'day' => Yii::t('back/BHApartmentComplexItemSlide', 'Day'),
            'content' => Yii::t('back/BHApartmentComplexItemSlide', 'Content'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuildHistoryApartmentComplexItem()
    {
        return $this->hasOne(BuildHistoryApartmentComplexItem::class, ['id' => 'build_history_apartment_complex_item_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/BHApartmentComplexItemSlide', 'Build History Apartment Complex Item Slide');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'build_history_apartment_complex_item_id',
                        'format' => 'raw',
                        'value' => function (self $data) {
                            $item = $data->buildHistoryApartmentComplexItem ?? null;
                            $complex = $data->buildHistoryApartmentComplexItem->apartmentComplex ?? null;
                            if ($item && $complex) {
                                $formattedDate = app()->formatter->asDate($item->date, 'YYYY LLLL');
                                return $complex->label . " [$formattedDate]";
                            }
                            return null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => self::getItemsDictionary(),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => '--'],
                    ],
                    'day',
                    // 'content:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'build_history_apartment_complex_item_id',
                    'day',
                    [
                        'attribute' => 'content',
                        'format' => 'text',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new BuildHistoryApartmentComplexItemSlideSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'build_history_apartment_complex_item_id' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => Select2::class,
                'options' => [
                    'data' => self::getItemsDictionary(),
                    'options' => [
                        'placeholder' => '--',
                    ]
                ]
            ],
            'day' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::class,
                'options' => ['model' => $this, 'attribute' => 'content']
            ],
            'image' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                    'attribute' => 'image',
                    'saveAttribute' => \common\models\BuildHistoryApartmentComplexItemSlide::SAVE_ATTRIBUTE_IMAGE,
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => true, //Вкл/выкл множественную загрузку
                ])
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
            ]
        ];

        return $config;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    /**
     * @return array
     */
    public static function getItemsDictionary()
    {
        $items = BuildHistoryApartmentComplexItem::find()
            ->alias('i')
            ->joinWith([
                'apartmentComplex' => function(\yii\db\ActiveQuery $query) {
                    return $query->alias('c');
                }
            ], false)
            ->orderBy(['c.label' => SORT_ASC, 'i.date' => SORT_DESC])
            ->select(['i.id', 'c.label', 'i.date'])
            ->asArray()
            ->all();

        $result = [];
        foreach ($items as $item) {
            $formattedDate = app()->formatter->asDate($item['date'], 'YYYY LLLL');
            $result[$item['id']] = $item['label'] . " [$formattedDate]";
        }

        return $result;
    }
}
