<?php

namespace backend\modules\apartment\models;

use common\models\CompletedProjectToComplexList as CommonCompletedProjectToComplexList;

/**
 * This is the model class for table "{{%completed_project_to_complex_list}}".
 *
 * @property integer $complex_list_id
 * @property integer $completed_project_id
 * @property integer $position
 *
 * @property ComplexList $complexList
 * @property CompletedProject $completedProject
 */
class CompletedProjectToComplexList extends CommonCompletedProjectToComplexList
{
    public function attributeLabels()
    {
        return [
            'complex_list_id' => bt('Complex list ID', 'completed-project-to-complex-list'),
            'completed_project_id' => bt('Completed project ID', 'completed-project-to-complex-list'),
            'position' => bt('Position', 'completed-project-to-complex-list'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplexList()
    {
        return $this->hasOne(ComplexList::class, ['id' => 'complex_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompletedProject()
    {
        return $this->hasOne(CompletedProject::class, ['id' => 'completed_project_id']);
    }
}
