<?php

namespace backend\modules\apartment\models;

use backend\modules\contacts\models\City;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ApartmentComplexSearch represents the model behind the search form about `ApartmentComplex`.
 */
class ApartmentComplexSearch extends ApartmentComplex
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'manager_id', 'published', 'position', 'city_id', 'show_on_home', 'type', 'show_label'], 'integer'],
            [['label', 'top_screen_btn_label', 'top_screen_btn_link', 'general_progress_title', 'general_progress_percent', 'general_description', 'general_docs_link', 'general_docs_link_title', 'general_fb_link', 'general_vk_link', 'form_top_title', 'form_top_description', 'form_top_emails', 'advantages_title', 'map_title', 'map_subtitle', 'map_description', 'public_service_title', 'architecture_title', 'architecture_fio', 'architecture_job_position', 'architecture_description', 'form_bottom_title', 'form_bottom_description', 'form_bottom_email', 'custom_choose_label'], 'safe'],
            [['filter_price_deviation'], 'number'],
        ];
    }

    public static function getAdditionalHeaderConfig()
    {
        $cityCount = City::find()
            ->where([
                'published' => 1
            ])
            ->count();

        $modelCount = ApartmentComplex::find()
            ->select(['city_id'])
            ->where([
                'published' => 1
            ])
            ->groupBy(['city_id'])
            ->count();

        if ($cityCount == 0){
            $filledCities = '-';
        }
        else{
            $filledCities = round($modelCount / $cityCount * 100).'%';
        }


        return [
            [
                'class' => 'regular',
                'leftValue' => $filledCities,
                'rightValue' => \Yii::t('back/back/apartment-complex', 'Filled cities'),
                'link' => '',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApartmentComplexSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'manager_id' => $this->manager_id,
            'filter_price_deviation' => $this->filter_price_deviation,
            'published' => $this->published,
            'position' => $this->position,
            'show_on_home' => $this->show_on_home,
            'city_id' => $this->city_id,
            'show_label' => $this->show_label,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'top_screen_btn_label', $this->top_screen_btn_label])
            ->andFilterWhere(['like', 'top_screen_btn_link', $this->top_screen_btn_link])
            ->andFilterWhere(['like', 'general_progress_title', $this->general_progress_title])
            ->andFilterWhere(['like', 'general_progress_percent', $this->general_progress_percent])
            ->andFilterWhere(['like', 'general_description', $this->general_description])
            ->andFilterWhere(['like', 'general_docs_link', $this->general_docs_link])
            ->andFilterWhere(['like', 'general_docs_link_title', $this->general_docs_link_title])
            ->andFilterWhere(['like', 'general_fb_link', $this->general_fb_link])
            ->andFilterWhere(['like', 'general_vk_link', $this->general_vk_link])
            ->andFilterWhere(['like', 'form_top_title', $this->form_top_title])
            ->andFilterWhere(['like', 'form_top_description', $this->form_top_description])
            ->andFilterWhere(['like', 'form_top_emails', $this->form_top_emails])
            ->andFilterWhere(['like', 'advantages_title', $this->advantages_title])
            ->andFilterWhere(['like', 'map_title', $this->map_title])
            ->andFilterWhere(['like', 'map_subtitle', $this->map_subtitle])
            ->andFilterWhere(['like', 'map_description', $this->map_description])
            ->andFilterWhere(['like', 'public_service_title', $this->public_service_title])
            ->andFilterWhere(['like', 'architecture_title', $this->architecture_title])
            ->andFilterWhere(['like', 'architecture_fio', $this->architecture_fio])
            ->andFilterWhere(['like', 'architecture_job_position', $this->architecture_job_position])
            ->andFilterWhere(['like', 'architecture_description', $this->architecture_description])
            ->andFilterWhere(['like', 'form_bottom_title', $this->form_bottom_title])
            ->andFilterWhere(['like', 'form_bottom_description', $this->form_bottom_description])
            ->andFilterWhere(['like', 'form_bottom_email', $this->form_bottom_email]);

        return $dataProvider;
    }
}
