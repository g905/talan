<?php

namespace backend\modules\apartment\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ApartmentSearch represents the model behind the search form about `Apartment`.
 */
class ApartmentSearch extends Apartment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'entrance', 'apartment_complex_id', 'rooms_number', 'published', 'position', 'subtype', 'type'], 'integer'],
            [['label', 'delivery_date', 'description_title', 'description_subtitle', 'description_text', 'floor_plan_title'], 'safe'],
            [['total_area', 'living_space', 'price_from'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApartmentSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'subtype' => $this->subtype,
            'apartment_complex_id' => $this->apartment_complex_id,
            'rooms_number' => $this->rooms_number,
            'total_area' => $this->total_area,
            'living_space' => $this->living_space,
            'delivery_date' => $this->delivery_date,
            'price_from' => $this->price_from,
            'published' => $this->published,
            'position' => $this->position,
            'entrance' => $this->entrance
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'description_title', $this->description_title])
            ->andFilterWhere(['like', 'description_subtitle', $this->description_subtitle])
            ->andFilterWhere(['like', 'description_text', $this->description_text])
            ->andFilterWhere(['like', 'floor_plan_title', $this->floor_plan_title]);

        return $dataProvider;
    }
}
