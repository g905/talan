<?php

namespace backend\modules\apartment\models;

use backend\components\ImperaviContent;
use yii\behaviors\TimestampBehavior;
use kartik\grid\GridView;
use kartik\select2\Select2;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\components\BackendModel;
use common\models\EntityToFile;
use common\components\model\ActiveRecord;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;

/**
 * This is the model class for table "{{%build_history_apartment_building_item_slide}}".
 *
 * @property integer $id
 * @property integer $build_history_apartment_building_item_id
 * @property string $day
 * @property string $content
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property BuildHistoryApartmentBuildingItem $buildHistoryApartmentBuildingItem
 */
class BuildHistoryApartmentBuildingItemSlide extends ActiveRecord implements BackendModel
{
    public $image;
    public $sign;

    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = security()->generateRandomString();
        }
    }

    public static function tableName()
    {
        return '{{%build_history_apartment_building_item_slide}}';
    }

    public function rules()
    {
        return [
            [['build_history_apartment_building_item_id'], 'integer'],
            [['day', 'build_history_apartment_building_item_id'], 'required'],
            [['content'], 'string'],
            [['day'], 'integer', 'min' => 1, 'max' => 31],
            [
                ['build_history_apartment_building_item_id'],
                'exist',
                'targetClass' => \common\models\BuildHistoryApartmentBuildingItem::class,
                'targetAttribute' => 'id'
            ],
            [['sign'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'BHBuildingItemSlide'),
            'build_history_apartment_building_item_id' => bt('Build history apartment building item', 'BHBuildingItemSlide'),
            'day' => bt('Day', 'BHBuildingItemSlide'),
            'content' => bt('Content', 'BHBuildingItemSlide'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getBuildHistoryApartmentBuildingItem()
    {
        return $this->hasOne(BuildHistoryApartmentBuildingItem::class,
            ['id' => 'build_history_apartment_building_item_id']);
    }

    public function getTitle()
    {
        return bt('Build History Apartment Building Item Slide', 'BHBuildingItemSlide');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'build_history_apartment_building_item_id',
                        'format' => 'raw',
                        'value' => function (self $data) {
                            $item = $data->buildHistoryApartmentBuildingItem ?? null;
                            $complex = $data->buildHistoryApartmentBuildingItem->apartmentBuilding ?? null;
                            if ($item && $complex) {
                                $formattedDate = app()->formatter->asDate($item->date, 'YYYY LLLL');
                                return $complex->label . " [$formattedDate]";
                            }
                            return null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => self::getItemsDictionary(),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => '--'],
                    ],
                    'day',
                    // 'content:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'build_history_apartment_building_item_id',
                    'day',
                    [
                        'attribute' => 'content',
                        'format' => 'text',
                    ],
                ];
                break;
        }

        return [];
    }

    public function getSearchModel()
    {
        return new BuildHistoryApartmentBuildingItemSlideSearch();
    }

    public function getFormConfig()
    {
        $config = [
            'build_history_apartment_building_item_id' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => Select2::class,
                'options' => [
                    'data' => self::getItemsDictionary(),
                    'options' => [
                        'placeholder' => '--',
                    ]
                ]
            ],
            'day' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::class,
                'options' => ['model' => $this, 'attribute' => 'content']
            ],
            'image' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                    'attribute' => 'image',
                    'saveAttribute' => \common\models\BuildHistoryApartmentBuildingItemSlide::SAVE_ATTRIBUTE_IMAGE,
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => true, //Вкл/выкл множественную загрузку
                ])
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
            ]
        ];

        return $config;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    public static function getItemsDictionary()
    {
        $items = BuildHistoryApartmentBuildingItem::find()
            ->alias('i')
            ->joinWith([
                'apartmentBuilding' => function (\yii\db\ActiveQuery $query) {
                    return $query->alias('c');
                }
            ], false)
            ->orderBy(['c.label' => SORT_ASC, 'i.date' => SORT_DESC])
            ->select(['i.id', 'c.label', 'i.date'])
            ->asArray()
            ->all();

        $result = [];
        foreach ($items as $item) {
            $formattedDate = app()->formatter->asDate($item['date'], 'YYYY LLLL');
            $result[$item['id']] = $item['label'] . " [$formattedDate]";
        }

        return $result;
    }
}
