<?php

namespace backend\modules\apartment\models;

use backend\modules\block\models\block\FloorPlan;
use Yii;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use common\helpers\Property;
/**
 * This is the model class for table "{{%apartment_entrance}}".
 *
 * @property integer $id
 * @property integer $entrance
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $building_id
 * @property integer $building_complex_id
 *
 * @property ApartmentBuilding $building
 */
class ApartmentEntrance extends ActiveRecord implements BackendModel
{

    public static function tableName()
    {
        return '{{%apartment_entrance}}';
    }

    public function rules()
    {
        return [
            [['entrance', 'building_complex_id'], 'required'],
            [['entrance', 'published', 'building_id', 'building_complex_id'], 'integer'],
            [['building_id'], 'exist', 'targetClass' => \common\models\ApartmentBuilding::className(), 'targetAttribute' => 'id'],
            [['building_complex_id'], 'exist', 'targetClass' => \common\models\ApartmentComplex::className(), 'targetAttribute' => 'id'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'entrance' => Yii::t('app', 'Entrance'),
            /*'published' => Yii::t('app', 'Published'),
            'position' => Yii::t('app', 'Position'),*/
            'building_id' => Yii::t('app', 'Building ID'),
            'building_complex_id' => Yii::t('app', 'Complex ID'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    public function getBuilding()
    {
        return $this->hasOne(ApartmentBuilding::className(), ['id' => 'building_id']);
    }

    public function getTitle()
    {
        return bt('Apartment Entrance', 'app');
    }

    public function getLabels() {
      $type = ApartmentComplex::find()
        ->select(['id', 'type', 'label'])
        ->orderBy('position')
        ->asArray()
        ->all();
        foreach ($type as $t) {
          $ar[$t['id']] = $t['label'].' ('.Property::types()[$t['type']].')';
        }
      return $ar;
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'building_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            $building = ApartmentBuilding::find()->where(['id' => $model->building_id])->one();
                            return optional($building)->id;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(ApartmentBuilding::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any building')],
                    ],


                    'entrance',
                    [
                        'attribute' => 'building_complex_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */

                            $complex = ApartmentComplex::find()->where(['id' => $model->building_complex_id])->one();
                            return optional($complex)->label;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => $this->getLabels(),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/apartment-complex', 'Any complex')],
                    ],
                    /*'published:boolean',
                    'position',*/
                    //'building_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            case 'view':
                return [
                    'id',
                    'entrance',
                    /*'published:boolean',
                    'position',*/
                    'building_id',
                    'building_complex_id'
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new ApartmentEntranceSearch();
    }

    public function getFormConfig()
    {

        $config = [
            'form-set' => [
                Yii::t('back/apartment-entrance', 'General') => [
                    'entrance' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],/*
                    'position' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],*/
                    'building_complex_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\ApartmentComplex::getItems(),
                        'options' => [
                            'prompt' => 'ch',
                        ],
                    ],
                    'building_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\ApartmentBuilding::getItems(),
                        'options' => [
                            'prompt' => 'hfgh',
                        ],
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                ],
                Yii::t('back/apartment-entrance', 'Floor Plans') => [
                    'entrance' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> <i class="si si-grid"></i> '.Yii::t('back/apartment-entrance', 'floor plans').'</div>',
                        'label' => false,
                    ],
                    $this->getRelatedFormConfig()['floor_plans'],
                ],
            ]
        ];

        return $config;
    }

    public function getRelatedFormConfig()
    {
        return [
            'floor_plans' => [
                'relation' => 'floorPlans'
            ],
        ];
    }

    public function getFloorPlans()
    {
        return $this->hasMany(FloorPlan::class, ['object_id' => 'id'])
            ->onCondition(['type' => FloorPlan::FLOOR_PLAN]);
    }
}
