<?php

namespace backend\modules\apartment\models;

use Yii;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\models\EntityToFile;
use common\models\CompletedProject as CommonCompletedProject;
use backend\components\BackendModel;
use backend\components\ImperaviContent;
use backend\components\StylingActionColumn;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;

/**
 * This is the model class for table "{{%completed_project}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $published
 * @property CompletedProjectToComplexList[] $completedProjectToComplexLists
 * @property ComplexList[] $complexLists
 * @property EntityToFile $photo
 */
class CompletedProject extends CommonCompletedProject implements BackendModel
{
    /**
     * @var string attribute for image uploader.
     */
    public $photoWidget;
    /**
     * @var string temporary sign which used for saving images before model save.
     */
    public $sign;

    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = Yii::$app->security->generateRandomString();
        }
    }

    public static function tableName()
    {
        return '{{%completed_project}}';
    }

    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['description'], 'string'],
            [['title', 'url'], 'string', 'max' => 255],
            [['published'], 'string', 'max' => 3],
            [['published'], 'default', 'value' => 1],
            [['sign'], 'string', 'max' => 255],
            [['photoWidget'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'completed-project'),
            'title' => bt('Title', 'completed-project'),
            'description' => bt('Description', 'completed-project'),
            'photoWidget' => bt('Photo', 'completed-project'),
            'published' => bt('Published', 'completed-project'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompletedProjectToComplexLists()
    {
        return $this->hasMany(CompletedProjectToComplexList::class, ['completed_project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplexLists()
    {
        return $this->hasMany(ComplexList::class, ['id' => 'complex_list_id'])
            ->viaTable('{{%completed_project_to_complex_list}}', ['completed_project_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return bt('Completed project', 'completed-project');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'title',
                    'description:html',
                    'published:boolean',
                    ['class' => StylingActionColumn::class],
                ];
            case 'view':
                return [
                    'id',
                    'title',
                    'description:html',
                    [
                        'attribute' => 'photoWidget',
                        'format' => 'image',
                        'value' => function (self $model) {
                            return $model->getPhotoPreviewSrc();
                        }
                    ],
                    'published:boolean',
                ];
            default:
                return [];
        }
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new CompletedProjectSearch();
    }

    public function getFormConfig()
    {
        return [
            'title' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => ['maxlength' => true],
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::class,
                'options' => ['model' => $this, 'attribute' => 'description']
            ],
            'url' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => ['maxlength' => true],
            ],
            'photoWidget' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'fieldOptions' => ['options' => ['class' => 'form-group required']],
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'photoWidget',
                    'saveAttribute' => self::SAVE_ATTRIBUTE_PHOTO,
                    'aspectRatio' => false,
                    'multiple' => false,
                ]),
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
