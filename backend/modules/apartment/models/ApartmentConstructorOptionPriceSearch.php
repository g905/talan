<?php

namespace backend\modules\apartment\models;

use common\models\ApartmentConstructorPrice as CommonPrice;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ApartmentConstructorPriceSearch represents the model behind the search form about `ApartmentConstructorPrice`.
 */
class ApartmentConstructorOptionPriceSearch extends ApartmentConstructorOptionPrice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'price', 'option'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'price' => $this->price,
            'option' => $this->option,
        ]);

        return $dataProvider;
    }
}
