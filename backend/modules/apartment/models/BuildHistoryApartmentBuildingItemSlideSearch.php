<?php

namespace backend\modules\apartment\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class BuildHistoryApartmentBuildingItemSlideSearch extends BuildHistoryApartmentBuildingItemSlide
{
    public function rules()
    {
        return [
            [['id', 'build_history_apartment_building_item_id'], 'integer'],
            [['day', 'content'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = BuildHistoryApartmentBuildingItemSlideSearch::find()->with([
        'buildHistoryApartmentBuildingItem' => function (\yii\db\ActiveQuery $query) {
            return $query->with(['apartmentBuilding']);
        }]);
        $dataProvider = new ActiveDataProvider(['query' => $query]);
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'build_history_apartment_building_item_id' => $this->build_history_apartment_building_item_id,
        ])
            ->andFilterWhere(['like', 'day', $this->day])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
