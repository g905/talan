<?php

namespace backend\modules\apartment\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ComplexListSearch represents the model behind the search form about `ComplexList`.
 *
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package backend\modules\apartment\models
 */
class ComplexListSearch extends ComplexList
{
    public function rules()
    {
        return [
            [['id', 'type', 'city_id', 'manager_id', 'created_at', 'updated_at'], 'integer'],
            [
                [
                    'top_title',
                    'complex_more_btn_label',
                    'completed_projects_title',
                    'completed_projects_description',
                    'completed_projects_more_btn_label',
                    'form_title',
                    'form_description',
                    'form_email',
                ],
                'safe'
            ],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'city_id' => $this->city_id,
            'manager_id' => $this->manager_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'top_title', $this->top_title]);
        $query->andFilterWhere(['like', 'complex_more_btn_label', $this->complex_more_btn_label]);
        $query->andFilterWhere(['like', 'completed_projects_title', $this->completed_projects_title]);
        $query->andFilterWhere(['like', 'completed_projects_description', $this->completed_projects_description]);
        $query->andFilterWhere(['like', 'completed_projects_more_btn_label', $this->completed_projects_more_btn_label]);
        $query->andFilterWhere(['like', 'form_title', $this->form_title]);
        $query->andFilterWhere(['like', 'form_description', $this->form_description]);
        $query->andFilterWhere(['like', 'form_email', $this->form_email]);

        return $dataProvider;
    }
}
