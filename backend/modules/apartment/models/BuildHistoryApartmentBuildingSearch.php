<?php

namespace backend\modules\apartment\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class BuildHistoryApartmentBuildingSearch extends BuildHistoryApartmentBuilding
{
    public function rules()
    {
        return [
            [['label', 'build_start_date', 'build_end_date'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = BuildHistoryApartmentBuildingSearch::find();
        $dataProvider = new ActiveDataProvider(['query' => $query]);
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'build_end_date' => $this->build_end_date,
            'build_start_date' => $this->build_start_date,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label]);

        return $dataProvider;
    }
}
