<?php

namespace backend\modules\apartment\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AcMenuSearch represents the model behind the search form about `AcMenu`.
 */
class AcMenuSearch extends AcMenu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'apartment_complex_id', 'show_right', 'prepared_page_id', 'published', 'position'], 'integer'],
            [['label', 'alias', 'link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AcMenuSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'apartment_complex_id' => $this->apartment_complex_id,
            'show_right' => $this->show_right,
            'prepared_page_id' => $this->prepared_page_id,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}
