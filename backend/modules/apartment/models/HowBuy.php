<?php

namespace backend\modules\apartment\models;

use backend\modules\contacts\models\City;
use kartik\grid\GridView;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%how_buy}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $city_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 */
class HowBuy extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%how_buy}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'city_id'], 'required'],
            [['city_id', 'published', 'position'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'targetClass' => \common\models\City::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/how-buy', 'ID'),
            'label' => Yii::t('back/how-buy', 'Label'),
            'city_id' => Yii::t('back/how-buy', 'City'),
            'published' => Yii::t('back/how-buy', 'Published'),
            'position' => Yii::t('back/how-buy', 'Position'),
            'created_at' => Yii::t('back/how-buy', 'Created At'),
            'updated_at' => Yii::t('back/how-buy', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/how-buy', 'How Buy');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var ApartmentComplex $model */
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    'published:boolean',
                    'position',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    'city_id',
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new HowBuySearch();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHowBuyBlocks()
    {
        return $this->hasMany(HowBuyBlock::className(), ['how_buy_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                Yii::t('back/apartment-complex', 'General') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\City::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'label' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'position' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                ],
                Yii::t('back/apartment-complex', 'Blocks') => [
                    $this->getRelatedFormConfig()['how_buy_blocks'],
                ],
            ],
        ];

        return $config;
    }


    public function getRelatedFormConfig()
    {
        return [
            'how_buy_blocks' => [
                'relation' => 'howBuyBlocks',
            ],
        ];
    }

}
