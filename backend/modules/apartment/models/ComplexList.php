<?php

namespace backend\modules\apartment\models;

use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use yii\grid\SerialColumn;
use yii\behaviors\TimestampBehavior;
use kartik\grid\GridView;
use metalguardian\formBuilder\ActiveFormBuilder;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\helpers\Property;
use common\models\ComplexList as CommonComplexList;
use backend\helpers\Select2Helper;
use backend\components\FormBuilder;
use backend\components\BackendModel;
use backend\components\ImperaviContent;
use backend\modules\contacts\models\City;
use backend\components\ManyToManyBehavior;
use backend\components\StylingActionColumn;
use backend\modules\contacts\models\Manager;

/**
 * This is the model class for table "{{%complex_list}}".
 *
 * @property int $id
 * @property int $type
 * @property int $city_id City
 * @property string $top_title top title
 * @property string $menu_title menu title
 * @property string $complex_more_btn_label complex more button label
 * @property string $completed_projects_title completed projects title
 * @property string $completed_projects_description completed projects description
 * @property string $completed_projects_more_btn_label completed projects more button label
 * @property string $benefits_title benefits block title
 * @property string $form_title form title
 * @property string $form_description form description
 * @property string $form_email form email
 * @property string $form_agreement form agreement
 * @property int $manager_id manager id
 * @property int $created_at created at
 * @property int $updated_at updated at
 *
 * @property City $city
 * @property Manager $manager
 */
class ComplexList extends CommonComplexList implements BackendModel
{
    public $poster;
    public $videoMp4;
    public $presentationFile;

    public $projectIds;
    public $projectIdValues;

    public $benefitIds;
    public $benefitIdValues;

    /**
     * @var string temporary sign which used for saving images before model save.
     */
    public $sign;

    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = security()->generateRandomString();
        }
    }

    public function rules()
    {
        return [
            [['city_id', 'top_title', 'type', 'menu_title'], 'required'],
            [['city_id', 'manager_id', 'type'], 'integer'],
            [['completed_projects_description', 'completed_projects_more_btn_label', 'form_description', 'form_email', 'benefits_title'], 'string'],
            [['top_title', 'menu_title', 'complex_more_btn_label', 'completed_projects_title', 'form_title', 'form_agreement'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::class, 'targetAttribute' => ['city_id' => 'id']],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => Manager::class, 'targetAttribute' => ['manager_id' => 'id']],
            [['projectIds', 'benefitIds', 'poster', 'videoMp4', 'presentationFile'], 'safe'],
            [['sign'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'complex-list'),
            'type' => bt('type', 'complex-list'),
            'city_id' => bt('city', 'complex-list'),
            'top_title' => bt('top title', 'complex-list'),
            'menu_title' => bt('menu title', 'complex-list'),
            'complex_more_btn_label' => bt('complex more button label', 'complex-list'),
            'completed_projects_title' => bt('completed projects title', 'complex-list'),
            'completed_projects_description' => bt('completed projects description', 'complex-list'),
            'completed_projects_more_btn_label' => bt('completed projects more button label', 'complex-list'),
            'form_title' => bt('form title', 'complex-list'),
            'form_description' => bt('form description', 'complex-list'),
            'form_email' => bt('form email', 'complex-list'),
            'form_agreement' => bt('form agreement', 'complex-list'),
            'manager_id' => bt('manager', 'complex-list'),
            'created_at' => bt('created at', 'complex-list'),
            'updated_at' => bt('updated at', 'complex-list'),
            'projectIds' => bt('completed projects', 'complex-list'),
            'poster' => bt('poster', 'complex-list'),
            'videoMp4' => bt('video mp4', 'complex-list'),
            'presentationFile' => bt('presentation file', 'complex-list'),
            'benefitIds' => bt('benefits', 'complex-list'),
            'benefits_title' => bt('benefits block title', 'complex-list'),
        ];
    }

    public function behaviors()
    {
        return merge(parent::behaviors(), [
            'seo' => MetaTagBehavior::class,
            'timestamp' => TimestampBehavior::class,
            'manyToManyRelation' => [
                'class' => ManyToManyBehavior::class,
                'currentObj' => $this,
                'currentRelatedColumn' => 'complex_list_id',
                'manyToManyConfig' => [
                    'projects' => [
                        'attribute' => 'projectIds',
                        'valueAttribute' => 'projectIdValues',
                        'relatedColumn' => 'completed_project_id',
                        'linkTableName' => CompletedProjectToComplexList::tableName(),
                        'relatedObjTableName' => CompletedProject::tableName(),
                        'isStringReturn' => false,
                        'labelColumn' => 'title'
                    ],
                    'benefits' => [
                        'attribute' => 'benefitIds',
                        'valueAttribute' => 'benefitIdValues',
                        'relatedColumn' => 'benefit_id',
                        'linkTableName' => BenefitToComplexList::tableName(),
                        'relatedObjTableName' => Benefit::tableName(),
                        'isStringReturn' => false,
                        'labelColumn' => 'title'
                    ],
                ]
            ],
        ]);
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getTitle()
    {
        return bt('Complex list page', 'complex-list');
    }

    public function getSearchModel()
    {
        return new ComplexListSearch();
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => SerialColumn::class],
                    // 'id',
                    [
                        'attribute' => 'type',
                        'value' => function (self $model) {
                            return Property::typeLabel($model->type);
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => Property::types(),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => bt('Any type', 'complex-list')],
                    ],
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var ApartmentComplex $model */
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => bt('Any city', 'site-menu')],
                    ],
                    'top_title',
                    // 'menu_title',
                    // 'complex_more_btn_label',
                    // 'completed_projects_title',
                    // 'completed_projects_description',
                    // 'completed_projects_more_btn_label',
                    // 'form_title',
                    // 'form_description',
                    // 'form_email:email',
                    // 'form_agreement:html',
                    [
                        'attribute' => 'manager_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var ApartmentComplex $model */
                            if (isset($model->manager)){
                                return '<b>'.$model->manager->fio.'</b>';
                            }
                            return '-';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => map(Manager::find()->orderBy('fio')->asArray()->all(), 'id', 'fio'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => bt('Any manager', 'apartment-complex')],
                    ],
                    //'created_at',
                    //'updated_at',
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                if ($model->isResidential()) {
                                    return StylingActionColumn::createPreviewUrl($url, $model, 'apartment-complex/list');
                                } else {
                                    return StylingActionColumn::createPreviewUrl($url, $model, 'commercial-complex/list');
                                }
                            }
                        ]
                    ]
                ];
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'type',
                        'value' => function (self $model) {
                            return Property::typeLabel($model->type);
                        },
                    ],
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var ApartmentComplex $model */
                            return '<b>'.$model->city->label.'</b>';
                        },
                    ],
                    'top_title',
                    'menu_title',
                    'complex_more_btn_label',
                    'completed_projects_title',
                    'completed_projects_description:html',
                    'completed_projects_more_btn_label',
                    'form_title',
                    'form_description:html',
                    'form_email:email',
                    'form_agreement:html',
                    [
                        'attribute' => 'manager_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var ApartmentComplex $model */
                            if (isset($model->manager)){
                                return '<b>'.$model->manager->fio.'</b>';
                            }
                            return '-';
                        },
                    ],
                ];
            default:
                return [];
        }
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General', 'complex-list') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => City::getItems(),
                        'options' => ['prompt' => ''],
                    ],
                    'type' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => Property::types(),
                        'options' => ['prompt' => ''],
                    ],
                    'top_title' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'menu_title' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'complex_more_btn_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                ],
                bt('completed projects block', 'complex-list') => [
                    'completed_projects_title' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'completed_projects_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::class,
                        'options' => ['model' => $this, 'attribute' => 'completed_projects_description'],
                    ],
                    'completed_projects_more_btn_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'projectIds' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'label' => $this->getAttributeLabel('projectIds'),
                        'value' => Select2Helper::getAjaxSelect2Widget(
                            $this,
                            'projectIds',
                            static::getCompletedProjectsUrl(),
                            $this->projectIdValues,
                            ['multiple' => true]
                        )
                    ],
                ],
                bt('benefits block', 'complex-list') => [
                    'benefits_title' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'benefitIds' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'label' => $this->getAttributeLabel('benefitIds'),
                        'value' => Select2Helper::getAjaxSelect2Widget(
                            $this,
                            'benefitIds',
                            static::getBenefitsUrl(),
                            $this->benefitIdValues,
                            ['multiple' => true]
                        )
                    ],
                ],
                bt('form', 'complex-list') => [
                    'form_title' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'form_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::class,
                        'options' => ['model' => $this, 'attribute' => 'form_description'],
                    ],
                    'form_email' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'form_agreement' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::class,
                        'options' => [
                            'model' => $this,
                            'attribute' => 'form_agreement',
                            'settings' => [
                                'buttons' => ['link'],
                                'formatting' => ['p', 'h3', 'a'],
                                'plugins' => ['fullscreen'],
                            ],
                        ]
                    ],
                    'manager_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => Manager::getItems('id', 'fio'),
                        'options' => ['prompt' => ''],
                    ],
                ],
                bt('Poster screen', 'complex-list') => [
                    'poster' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'poster',
                            'saveAttribute' => self::ATTRIBUTE_POSTER,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'videoMp4' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['mp4'],
                            'attribute' => 'videoMp4',
                            'saveAttribute' => self::ATTRIBUTE_VIDEO_MP4,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'presentationFile' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            //'allowedFileExtensions' => ['pdf', 'docx', 'doc', 'ppt', 'pptx'],
                            'attribute' => 'presentationFile',
                            'saveAttribute' => self::ATTRIBUTE_PRESENTATION_FILE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
            ],
        ];
    }

    public static function getCompletedProjectsUrl()
    {
        return '/apartment/completed-project/search';
    }

    public static function getBenefitsUrl()
    {
        return '/apartment/benefit/search';
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
