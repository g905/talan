<?php

namespace backend\modules\apartment\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%promo_action_to_ac}}".
 *
 * @property integer $action_id
 * @property integer $ac_id
 *
 * @property ApartmentComplex $ac
 * @property PromoAction $action
 */
class PromoActionToAc extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_action_to_ac}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'ac_id'], 'integer'],
            [['action_id'], 'exist', 'targetClass' => \common\models\PromoAction::className(), 'targetAttribute' => 'id'],
            [['ac_id'], 'exist', 'targetClass' => \common\models\ApartmentComplex::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'action_id' => Yii::t('back/promo-action-to-ac', 'action'),
            'ac_id' => Yii::t('back/promo-action-to-ac', 'apartment complex'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAc()
    {
        return $this->hasOne(ApartmentComplex::className(), ['id' => 'ac_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(PromoAction::className(), ['id' => 'action_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/promo-action-to-ac', 'Promo Action To Ac');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    'action_id',
                    'ac_id',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
            break;
            case 'view':
                return [
                    'action_id',
                    'ac_id',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PromoActionToAcSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'action_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\PromoAction::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'ac_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ApartmentComplex::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
        ];

        return $config;
    }


}
