<?php

namespace backend\modules\apartment\models;

use backend\components\ManyToManyBehavior;
use backend\helpers\ImgHelper;
use backend\helpers\Select2Helper;
use backend\modules\contacts\models\Manager;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%promo_action}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $content
 * @property integer $show_on_home
 * @property integer $position_on_home
 * @property integer $manager_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $view_count
 * @property string $form_onsubmit
 * @property integer $start_date
 * @property integer $end_date
 *
 * @property Manager $manager
 */
class PromoAction extends ActiveRecord implements BackendModel
{
    /**
    * Attribute for imageUploader
    */
    public $image;
    /**
     * @var
     */
    public $complexIds;
    public $complexIdsValue;
    public $buildingIds;
    public $buildingIdsValue;

    /**
    * Temporary sign which used for saving images before model save
    * @var string
    */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_action}}';
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function rules()
    {
        return [
            [['label', 'manager_id'], 'required'],
            [['content'], 'string'],
            [['show_on_home', 'position_on_home', 'manager_id', 'published', 'position', 'view_count'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['position_on_home'], 'default', 'value' => 0],
            [['manager_id'], 'exist', 'targetClass' => \common\models\Manager::class, 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign', 'form_onsubmit'], 'string', 'max' => 255],
            [['complexIds', 'buildingIds'], 'safe'],
            [
                ['start_date'],
                'datetime',
//                'maxString' => self::formatAsDate(time()),
                'format' => 'yyyy-mm-dd',
            ],
            [
                ['end_date'],
                'datetime',
//                'maxString' => self::formatAsDate(time()),
                'format' => 'yyyy-mm-dd',
            ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/promo-action', 'ID'),
            'label' => Yii::t('back/promo-action', 'Label'),
            'content' => Yii::t('back/promo-action', 'Content'),
            'show_on_home' => Yii::t('back/promo-action', 'Show on home'),
            'position_on_home' => Yii::t('back/promo-action', 'Position on home'),
            'manager_id' => Yii::t('back/promo-action', 'manager'),
            'published' => Yii::t('back/promo-action', 'Published'),
            'position' => Yii::t('back/promo-action', 'Position'),
            'created_at' => Yii::t('back/promo-action', 'Created At'),
            'updated_at' => Yii::t('back/promo-action', 'Updated At'),
            'image' => Yii::t('back/promo-action', 'image'),
            'complexIds' => Yii::t('back/promo-action', 'Apartment complex'),
            'buildingIds' => Yii::t('back/promo-action', 'Apartment building'),
            'form_onsubmit' => Yii::t('back/promo-action', 'Form onsubmit'),
            'start_date' => Yii::t('back/promo-action', 'Start Date'),
            'end_date' => Yii::t('back/promo-action', 'End Date'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::class,
            ],
            'manyToManyRelation' => [
                'class' => ManyToManyBehavior::class,
                'currentObj' => $this,
                'currentRelatedColumn' => 'action_id',
                'manyToManyConfig' => [
                    'complex' => [
                        'attribute' => 'complexIds',
                        'valueAttribute' => 'complexIdsValue',
                        'relatedColumn' => 'ac_id',
                        'linkTableName' => PromoActionToAc::tableName(),
                        'relatedObjTableName' => ApartmentComplex::tableName(),
                        'isStringReturn' => false
                    ],
                    'building' => [
                        'attribute' => 'buildingIds',
                        'valueAttribute' => 'buildingIdsValue',
                        'relatedColumn' => 'ab_id',
                        'linkTableName' => PromoActionToAb::tableName(),
                        'relatedObjTableName' => \common\models\ApartmentBuilding::tableName(),
                        'isStringReturn' => false
                    ],
                ]

            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/promo-action', 'Promo Action');
    }

    /**
     * @return string
     */
    public static function getComplexUrl()
    {
        return '/apartment/apartment-complex/get-select-items';
    }

    /**
     * @return string
     */
    public static function getBuildingUrl()
    {
        return '/apartment/apartment-building/get-select-items';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionImage()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition(['image.entity_model_name' => 'PromoAction',
                'image.attribute' => \common\models\PromoAction::SAVE_ATTRIBUTE_IMAGE])
            ->alias('image')
            ->orderBy('image.position DESC');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'actionImage',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return ImgHelper::getEntityImage($data, 'actionImage');
                        }
                    ],
                    'label',
                    // 'content:ntext',
                    'show_on_home:boolean',
                    [
                        'attribute' => 'manager_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var ApartmentComplex $model */
                            if (isset($model->manager)){
                                return '<b>'.$model->manager->fio.'</b>';
                            }
                            return '-';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(Manager::find()->orderBy('fio')->asArray()->all(), 'id', 'fio'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/apartment-complex', 'Any manager')],
                    ],
                    'published:boolean',
                    'show_on_home:boolean',
                    [
                        'attribute' => 'position',
                        'width' => '30px'
                    ],
                    ['class' => \backend\components\StylingActionColumn::class],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'show_on_home:boolean',
                    'position_on_home',
                    'manager_id',
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PromoActionSearch();
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'image' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'image',
                    'saveAttribute' => \common\models\PromoAction::SAVE_ATTRIBUTE_IMAGE,
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'complexIds' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'label' => $this->getAttributeLabel('complexIds'),
                'value' => Select2Helper::getAjaxSelect2Widget(
                    $this,
                    'complexIds',
                    static::getComplexUrl(),
                    $this->complexIdsValue,
                    [
                        'multiple' => true,
                    ]
                )
            ],
            'buildingIds' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'label' => $this->getAttributeLabel('buildingIds'),
                'value' => Select2Helper::getAjaxSelect2Widget(
                    $this,
                    'buildingIds',
                    static::getBuildingUrl(),
                    $this->buildingIdsValue,
                    [
                        'multiple' => true,
                    ]
                )
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::class,
                'options' => [
                    'model' => $this,
                    'attribute' => 'content',
                ]
            ],
            'form_onsubmit' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'manager_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\Manager::getItems('id', 'fio'),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'start_date' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => DatePicker::class,
                'options' => [
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'startDate' => '1970-01-01',
                        'endDate' => '2038-01-19',
                        'todayBtn' => true,
                        'autoclose' => true,
                        'timezone' => 'Europe/Kiev',
                        'format' => 'yyyy-mm-dd',
                    ],
                    'options' => [
                        'value' => $this->getIsNewRecord()
                            ? self::formatAsDate('now')
                            : self::formatAsDate($this->start_date),
                        'class' => 'form-control',
                        'readonly' => true,
                    ],
                ],
            ],
            'end_date' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => DatePicker::class,
                'options' => [
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'startDate' => '1970-01-01',
                        'endDate' => '2038-01-19',
                        'todayBtn' => true,
                        'autoclose' => true,
                        'timezone' => 'Europe/Kiev',
                        'format' => 'yyyy-mm-dd',
                    ],
                    'options' => [
                        'value' => $this->getIsNewRecord()
                            ? self::formatAsDate('now')
                            : self::formatAsDate($this->end_date),
                        'class' => 'form-control',
                        'readonly' => true,
                    ],
                ],
            ],
            'show_on_home' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position_on_home' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
            ],
            'position' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->start_date = \Yii::$app->getFormatter()->asTimestamp($this->start_date);
        $this->end_date = \Yii::$app->getFormatter()->asTimestamp($this->end_date);

        return parent::beforeSave($insert);
    }

    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->start_date = self::formatAsDate($this->start_date);
        $this->end_date = self::formatAsDate($this->end_date);
    }

    /**
     * @param $date
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public static function formatAsDate($date): string
    {
        return $date ? \Yii::$app->getFormatter()->asDate($date, 'yyyy-M-dd') : '';
    }
}
