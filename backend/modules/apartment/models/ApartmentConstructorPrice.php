<?php

namespace backend\modules\apartment\models;

use backend\components\StylingActionColumn;
use backend\modules\contacts\models\City;
use backend\helpers\StdColumn;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;

/**
 * This is the model class for table "apartment_constructor_price".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $price
 * @property integer $created_at
 * @property integer $updated_at
 */
class ApartmentConstructorPrice extends ActiveRecord implements BackendModel
{

    public static function tableName()
    {
        return 'apartment_constructor_price';
    }

    public function rules()
    {
        return [
            [['city_id', 'room_1', 'room_2', 'room_3', 'room_4'], 'required'],
            [['city_id', 'room_1', 'room_2', 'room_3', 'room_4', 'published'], 'integer'],
            [['city_id'], 'unique'],
            [['published'], 'default', 'value' => 1],
            [['room_1', 'room_2', 'room_3', 'room_4'], 'integer', 'max' => 999999999],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City',
            'room_1' => 'One Room Price',
            'room_2' => 'Two Rooms Price',
            'room_3' => 'Three Rooms Price',
            'room_4' => 'Four Rooms Price',
        ];
    }

    public function getTitle()
    {
        return bt('Apartment Constructor Price', 'app');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    StdColumn::city(),
                    'room_1',
                    'room_2',
                    'room_3',
                    'room_4',
                    'published:boolean',
                    [
                        'class' => StylingActionColumn::class,
                        'template' => "{update}"
                    ],
                ];
            case 'view':
                return [
                    'id',
                    'city_id',
                    'room_1',
                    'room_2',
                    'room_3',
                    'room_4',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new ApartmentConstructorPriceSearch();
    }

    public function getFormConfig()
    {
        $config = [
            'city_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\City::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'room_1' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'room_2' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'room_3' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'room_4' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /***
     * @return bool
     */
    public function showDeleteButton()
    {
        return false;
    }

}
