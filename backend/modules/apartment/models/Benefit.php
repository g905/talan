<?php

namespace backend\modules\apartment\models;

use Yii;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\models\EntityToFile;
use common\models\Benefit as CommonBenefit;
use backend\components\BackendModel;
use backend\components\ImperaviContent;
use backend\components\StylingActionColumn;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;

/**
 * This is the model class for table "{{%benefit}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $published
 * @property BenefitToComplexList[] $BenefitToComplexLists
 * @property ComplexList[] $complexLists
 * @property EntityToFile $photo
 */
class Benefit extends CommonBenefit implements BackendModel
{
    /**
     * @var string attribute for image uploader.
     */
    public $photoWidget;
    /**
     * @var string temporary sign which used for saving images before model save.
     */
    public $sign;

    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = security()->generateRandomString();
        }
    }

    public static function tableName()
    {
        return '{{%benefit}}';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['published'], 'string', 'max' => 3],
            [['published'], 'default', 'value' => 1],
            [['sign'], 'string', 'max' => 255],
            [['photoWidget'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'benefit'),
            'title' => bt('Title', 'benefit'),
            'description' => bt('Description', 'benefit'),
            'photoWidget' => bt('Photo', 'benefit'),
            'published' => bt('Published', 'benefit'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBenefitToComplexLists()
    {
        return $this->hasMany(BenefitToComplexList::class, ['benefit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplexLists()
    {
        return $this->hasMany(ComplexList::class, ['id' => 'complex_list_id'])
            ->viaTable('{{%benefit_to_complex_list}}', ['benefit_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return bt('Benefits', 'completed-project');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'title',
                    'description:html',
                    [
                        'attribute' => 'photoWidget',
                        'format' => 'image',
                        'value' => function (self $model) {
                            return $model->getPhotoPreviewSrc();
                        }
                    ],
                    'published:boolean',
                    ['class' => StylingActionColumn::class],
                ];
            case 'view':
                return [
                    'id',
                    'title',
                    'description:html',
                    [
                        'attribute' => 'photoWidget',
                        'format' => 'image',
                        'value' => function (self $model) {
                            return $model->getPhotoPreviewSrc();
                        }
                    ],
                    'published:boolean',
                ];
            default:
                return [];
        }
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new BenefitSearch();
    }

    public function getFormConfig()
    {
        return [
            'title' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => ['maxlength' => true],
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::class,
                'options' => ['model' => $this, 'attribute' => 'description']
            ],
            'photoWidget' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'fieldOptions' => ['options' => ['class' => 'form-group required']],
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'photoWidget',
                    'saveAttribute' => self::SAVE_ATTRIBUTE_PHOTO,
                    'aspectRatio' => false,
                    'multiple' => false,
                ]),
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
