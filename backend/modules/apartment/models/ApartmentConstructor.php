<?php

namespace backend\modules\apartment\models;

use backend\components\StylingActionColumn;
use backend\helpers\StdColumn;
use common\models\City;
use kartik\grid\GridView;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;
use common\models\ApartmentConstructor as CommonConstructor;

/**
 * This is the model class for table "apartment_constructor".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property integer $price
 * @property integer $type
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 */
class ApartmentConstructor extends ActiveRecord implements BackendModel
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'apartment_constructor';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['label', 'type', 'description', 'city_id'], 'required'],
            [['type', 'position', 'city_id'], 'integer'],
            [['label'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 600],
            [['position'], 'default', 'value' => 0],
            [['position'], 'integer', 'max' => 999999999],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'description' => 'Description',
            'type' => 'Type',
            'position' => 'Position',
            'city_id' => 'City',
        ];
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return bt('Apartment Constructor', 'app');
    }

    /**
     * @param $page
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    StdColumn::city(),
                    [
                        'attribute' => 'type',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return CommonConstructor::getTypeDictionary()[$model->type] ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => CommonConstructor::getTypeDictionary(),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any type')],
                    ],
                    'label',
                    StdColumn::position(),
                    [
                        'class' => StylingActionColumn::class,
                        'template' => "{update}"
                    ],
                ];
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    'type',
                    'position',
                ];
            default:
                return [];
        }
    }

    /**
     * @return ApartmentConstructorSearch|\yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ApartmentConstructorSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'city_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\City::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }

    /***
     * @return bool
     */
    public function showCreateButton()
    {
        return false;
    }

    /***
     * @return bool
     */
    public function showDeleteButton()
    {
        return false;
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

}
