<?php

namespace backend\modules\apartment\models;

use common\models\BenefitToComplexList as CommonBenefitToComplexList;

/**
 * This is the model class for table "{{%benefit_to_complex_list}}".
 *
 * @property integer $complex_list_id
 * @property integer $benefit_id
 * @property integer $position
 *
 * @property ComplexList $complexList
 * @property Benefit $benefit
 */
class BenefitToComplexList extends CommonBenefitToComplexList
{
    public function attributeLabels()
    {
        return [
            'complex_list_id' => bt('Complex list ID', 'benefit-to-complex-list'),
            'benefit_id' => bt('Benefit ID', 'benefit-to-complex-list'),
            'position' => bt('Position', 'benefit-to-complex-list'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplexList()
    {
        return $this->hasOne(ComplexList::class, ['id' => 'complex_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBenefit()
    {
        return $this->hasOne(Benefit::class, ['id' => 'benefit_id']);
    }
}
