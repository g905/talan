<?php

namespace backend\modules\apartment\models;

use backend\behaviors\linker\LinkerBehavior;
use backend\components\FormBuilder;
use backend\components\ImperaviContent;
use backend\components\ManyToManyBehavior;
use backend\helpers\StdColumn;
use backend\helpers\StdInput;
use backend\modules\block\models\block\BuildingAdvantage;
use backend\modules\blog\models\Article;
use backend\modules\contacts\models\Manager;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\helpers\Select2Helper;
use backend\helpers\ImgHelper;
use backend\modules\request\models\CustomPopup;
use backend\modules\request\models\RequestProblem;
use common\models\CompletedProjectAssign;
use yii\db\ActiveQuery;
use common\models\ArticleAssign;
use common\models\EntityToFile;
use kartik\grid\GridView;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use Yii;
use backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
use common\models\RequestProblemAssign;

/**
 * Class ApartmentBuilding
 * @package backend\modules\apartment\models
 */
class ApartmentBuilding extends \common\models\ApartmentBuilding implements BackendModel
{
    public $promoIds;
    public $promoIdsValue;

    /**
     * @var mixed attributes for imageUploader.
     */
    public $top_screen_background_image;
    public $top_screen_background_video;
    public $general_photos;
    public $investment_image;
    public $sections_image;

    /**
     * Temporary sign which used for saving images before model save
     * @var string
     */
    public $sign;

    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = security()->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['building_complex_id', 'manager_id', 'form_middle_problem', 'view_count', 'custom_popup_id', 'published', 'position', 'floors', 'entrance', 'cta_published', 'show_chess', 'type'], 'integer'],
            [['label', 'alias'], 'required'],
            [['top_screen_description', 'general_description', 'investment_description', 'form_top_description', 'form_top_emails', 'form_middle_description', 'form_middle_emails', 'form_bottom_description', 'form_bottom_email','cta_black_label', 'cta_white_label', 'cta_description', 'cta_button_label', 'cta_button_link', 'special_offer_description',], 'string'],
            [['label', 'alias', 'top_screen_btn_label', 'top_screen_btn_link', 'general_progress_title', 'general_progress_percent', 'general_docs_link', 'general_docs_link_title', 'general_fb_link', 'general_vk_link', 'general_in_link', 'investment_title_first', 'investment_title_second', 'investment_btn_label', 'investment_btn_link', 'articles_label', 'projects_label', 'projects_description', 'advantages_title_first_part', 'advantages_title_second_part', 'form_top_title_first', 'form_top_title_second', 'form_middle_title_first', 'form_middle_title_second', 'form_bottom_title', 'guid', 'form_top_onsubmit', 'form_middle_onsubmit', 'form_bottom_onsubmit', 'special_offer_text', 'special_offer_link',
				], 'string', 'max' => 255],
            [['show_label'], 'string', 'max' => 1],
            [['building_complex_id'], 'exist', 'targetClass' => \common\models\ApartmentComplex::className(), 'targetAttribute' => 'id'],
            [['entrance'], 'exist', 'targetClass' => \common\models\ApartmentEntrance::className(), 'targetAttribute' => 'id'],
            [['manager_id'], 'exist', 'targetClass' => \common\models\Manager::className(), 'targetAttribute' => 'id'],
            [['form_middle_problem'], 'exist', 'targetClass' => \common\models\RequestProblem::className(), 'targetAttribute' => 'id'],
            [['view_count'], 'default', 'value' => 0],
            [['published', 'cta_published'], 'default', 'value' => 1],
            [['position', 'show_chess'], 'default', 'value' => 0],
            [['sign', 'block_number'], 'string', 'max' => 255],
            [['promoIds', 'articlesField', 'projectsField', 'problemsField'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'seo' => [
                'class' => MetaTagBehavior::className(),
            ],
            'manyToManyRelation' => [
                'class' => ManyToManyBehavior::class,
                'currentObj' => $this,
                'currentRelatedColumn' => 'ab_id',
                'manyToManyConfig' => [
                    'promoAction' => [
                        'attribute' => 'promoIds',
                        'valueAttribute' => 'promoIdsValue',
                        'relatedColumn' => 'action_id',
                        'linkTableName' => PromoActionToAb::tableName(),
                        'relatedObjTableName' => PromoAction::tableName(),
                        'isStringReturn' => false
                    ],
                ]
            ],
            'linker' => [
                'class' => LinkerBehavior::class,
                'relations' => [
                    'articlesField' => [
                        'articles',
                        'updater' => [
                            // 'class' => ManyToManySmartUpdater::class,
                            'viaTableAttributesValue' => [
                                'type' => ArticleAssign::TYPE_BUILDING_ARTICLES,
                                'position' => function () {
                                    static $i = 0;
                                    return ++$i;
                                }
                            ],
                            'viaTableCondition' => [
                                'type' => ArticleAssign::TYPE_BUILDING_ARTICLES,
                            ],
                        ],
                        'get' => function ($value) {
                            return implode(',', $value);
                        },
                        'set' => function ($value) {
                            return empty($value) ? [] : explode(',', $value);
                        },
                    ],
                    'projectsField' => [
                        'projects',
                        'updater' => [
                            // 'class' => ManyToManySmartUpdater::class,
                            'viaTableAttributesValue' => [
                                'type' => CompletedProjectAssign::TYPE_BUILDING_PROJECTS,
                                'position' => function () {
                                    static $i = 0;
                                    return ++$i;
                                }
                            ],
                            'viaTableCondition' => [
                                'type' => CompletedProjectAssign::TYPE_BUILDING_PROJECTS,
                            ],
                        ],
                        'get' => function ($value) {
                            return implode(',', $value);
                        },
                        'set' => function ($value) {
                            return empty($value) ? [] : explode(',', $value);
                        },
                    ],
                    'problemsField' => [
                        'problems',
                        'updater' => [
                            // 'class' => ManyToManySmartUpdater::class,
                            'viaTableAttributesValue' => [
                                'type' => RequestProblemAssign::TYPE_BUILDING_PROBLEMS,
                                'position' => function () {
                                    static $i = 0;
                                    return ++$i;
                                }
                            ],
                            'viaTableCondition' => [
                                'type' => RequestProblemAssign::TYPE_BUILDING_PROBLEMS,
                            ],
                        ],
                        'get' => function ($value) {
                            return implode(',', $value);
                        },
                        'set' => function ($value) {
                            return empty($value) ? [] : explode(',', $value);
                        },
                    ],
                ],
            ],
        ]);
    }

    public function getProblem()
    {
        return $this->hasOne(RequestProblem::className(), ['id' => 'form_middle_problem']);
    }

    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Apartment Building');
    }

    /**
     * @return string
     */
    public static function getPromoUrl()
    {
        return '/apartment/promo-action/get-select-items';
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {

            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'general_photos',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return ImgHelper::getEntityImage($data, 'generalPhoto');
                        }
                    ],
                    [
                        'attribute' => 'building_complex_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var self $model */
                            return optional($model->buildingComplex)->label;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(ApartmentComplex::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any complex')],
                    ],
                    'label',
                    // 'top_screen_description:ntext',
                    //'top_screen_btn_label',
                    //'top_screen_btn_link:url',
                    //'general_progress_title',
                    [
                        'attribute' => 'general_progress_percent',
                        'value' => function ($model) {
                            if ($model->general_progress_percent == '') {
                                return '-';
                            }
                            return $model->general_progress_percent.'%';
                        }
                    ],
                    [
                        'attribute' => 'manager_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var ApartmentComplex $model */
                            if (isset($model->manager)) {
                                return '<b>'.$model->manager->fio.'</b>';
                            }
                            return '-';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(Manager::find()->orderBy('fio')->asArray()->all(), 'id', 'fio'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/apartment-complex', 'Any manager')],
                    ],
                    // 'general_progress_percent',
                    // 'general_description:ntext',
                    // 'general_docs_link:url',
                    // 'general_docs_link_title:url',
                    // 'general_fb_link:url',
                    // 'general_vk_link:url',
                    // 'advantages_title_first_part',
                    // 'advantages_title_second_part',
                    // 'form_top_title',
                    // 'form_top_description:ntext',
                    // 'form_top_emails:ntext',
                    // 'form_bottom_title',
                    // 'form_bottom_description:ntext',
                    // 'form_bottom_email:ntext',
                    // 'manager_id',
                    // 'view_count',
                    // 'guid',
                    // 'form_top_onsubmit',
                    // 'form_bottom_onsubmit',
                    // 'show_label',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'building_complex_id',
                    'label',
                    [
                        'attribute' => 'top_screen_description',
                        'format' => 'html',
                    ],
                    'top_screen_btn_label',
                    'top_screen_btn_link:url',
                    'general_progress_title',
                    'general_progress_percent',
                    [
                        'attribute' => 'general_description',
                        'format' => 'html',
                    ],
                    'general_docs_link:url',
                    'general_docs_link_title:url',
                    'general_fb_link:url',
                    'general_vk_link:url',
                    'general_in_link:url',
                    'investment_title_first',
                    'investment_title_second',
                    [
                        'attribute' => 'investment_description',
                        'format' => 'html',
                    ],
                    'investment_btn_label',
                    'investment_btn_link:url',
                    'articles_label',
                    'projects_label',
                    'projects_description',
                    'advantages_title_first_part',
                    'advantages_title_second_part',
                    'form_top_title_first',
                    'form_top_title_second',
                    [
                        'attribute' => 'form_top_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'form_top_emails',
                        'format' => 'html',
                    ],
                    'form_middle_title_first',
                    'form_middle_title_second',
                    [
                        'attribute' => 'form_middle_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'form_middle_emails',
                        'format' => 'html',
                    ],
                    'form_middle_problem',
                    'form_bottom_title',
                    [
                        'attribute' => 'form_bottom_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'form_bottom_email',
                        'format' => 'html',
                    ],
                    'manager_id',
                    'view_count',
                    'guid',
                    'form_top_onsubmit',
                    'form_bottom_onsubmit',
                    'show_label',
                    'custom_popup_id',
                    'published:boolean',
                    'position',
					'special_offer_text',
					'special_offer_link',
					'special_offer_description',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ApartmentBuildingSearch();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentBuildingMenu()
    {
        return $this->hasMany(AcMenu::class, ['apartment_building_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getProjects()
    {
        return $this->hasMany(CompletedProject::class, ['id' => 'project_id'])
            ->alias('p')
            ->where(['p.published' => self::IS_PUBLISHED])
            ->viaTable(CompletedProjectAssign::tableName(), ['object_id' => 'id'], function (ActiveQuery $q) {
                $q->alias('pa')->andWhere(['pa.type' => CompletedProjectAssign::TYPE_BUILDING_PROJECTS]);
            })
            ->innerJoin(CompletedProjectAssign::tableName() . ' pa', 'pa.project_id = p.id')
            ->orderBy(['pa.position' => SORT_ASC]);
    }

    public function getProblems()
    {
        return $this->hasMany(RequestProblem::class, ['id' => 'problem_id'])
            ->alias('p')
            ->where(['p.published' => self::IS_PUBLISHED])
            ->viaTable(RequestProblemAssign::tableName(), ['object_id' => 'id'], function (ActiveQuery $q) {
                $q->alias('pa')->andWhere(['pa.type' => RequestProblemAssign::TYPE_BUILDING_PROBLEMS]);
            })
            ->innerJoin(RequestProblemAssign::tableName() . ' pa', 'pa.problem_id = p.id')
            ->orderBy(['pa.position' => SORT_ASC]);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                Yii::t('back/apartment-building', 'General') => [
                    'building_complex_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\ApartmentComplex::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                            'class' => 's_name form-control'
                        ],
                    ],
                    'alias' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                            'class' => 's_alias form-control'
                        ],
                    ],
                    'show_label' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'top_screen_btn_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'top_screen_btn_link' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
//                    'promoIds' => [
//                        'type' => ActiveFormBuilder::INPUT_RAW,
//                        'label' => $this->getAttributeLabel('promoIds'),
//                        'value' => Select2Helper::getAjaxSelect2Widget(
//                            $this,
//                            'promoIds',
//                            static::getPromoUrl(),
//                            $this->promoIdsValue,
//                            [
//                                'multiple' => true,
//                            ]
//                        )
//                    ],
                    'sections_image' => StdInput::imageUpload(self::SAVE_ATTRIBUTE_SECTIONS_IMAGE),
                    'custom_popup_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => CustomPopup::getItems('id', 'white_label'),
                        'options' => [
                            'prompt' => '--',
                        ],
                    ],
                    'show_chess' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'position' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
					'special_offer_text' => [
						'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
						'options' => ['maxlength' => true],
					],
					//
//					'special_offer_link' => [
//						'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
//						'options' => ['maxlength' => true],
//					],
					'special_offer_description' => [
						'type' => ActiveFormBuilder::INPUT_TEXTAREA,
						'options' => [
							'rows' => 4,
						],
					],
                ],
                Yii::t('back/apartment-building', 'Menu') => [
                    $this->getRelatedFormConfig()['apartment_building_menu']
                ],
                Yii::t('back/apartment-building', 'Top screen') => [
                    'top_screen_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'top_screen_description',
                        ]
                    ],
                    'top_screen_background_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'top_screen_background_image',
                            'saveAttribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'top_screen_background_video' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['mp4'],
                            'attribute' => 'top_screen_background_video',
                            'saveAttribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                Yii::t('back/apartment-building', 'Building info') => [
                    'general_progress_title' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'general_progress_percent' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'general_docs_link' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'general_docs_link_title' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'general_fb_link' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'general_vk_link' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'general_in_link' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'general_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'general_description',
                        ]
                    ],
                ],
                Yii::t('back/apartment-building', 'Photos') => [
                    'general_photos' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'general_photos',
                            'saveAttribute' => \common\models\ApartmentComplex::SAVE_ATTRIBUTE_GENERAL_PHOTOS,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => true, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                Yii::t('back/apartment-complex', 'CTA') => [
                    'cta_black_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'cta_white_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'cta_description' => [
                        'type' => FormBuilder::INPUT_TEXTAREA,
                    ],
                    'cta_button_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'cta_button_link' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                    ],
                    'cta_published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                ],
                Yii::t('back/apartment-building', 'Investment') => [
                    'investment_title_first' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'investment_title_second' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'investment_btn_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'investment_btn_link' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'investment_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'investment_description',
                        ]
                    ],
                    'investment_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'investment_image',
                            'saveAttribute' => \common\models\ApartmentBuilding::SAVE_ATTRIBUTE_INVESTMENT_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                Yii::t('back/apartment-building', 'Articles') => [
                    'articles_label' => StdInput::text(),
                    'articlesField' => StdInput::relationInput(Article::class),
                ],
                Yii::t('back/apartment-building', 'Projects') => [
                    'projects_label' => StdInput::text(),
                    'projects_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'projects_description',
                        ]
                    ],
                    'projectsField' => StdInput::relationInput(CompletedProject::class, null, 'id', 'title'),
                ],
                Yii::t('back/apartment-building', 'Forms') => [
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> '.Yii::t('back/apartment-building', 'Form - top').'</div>',
                        'label' => false,
                    ],
                    'form_top_title_first' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_top_title_second' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_top_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'form_top_description',
                        ]
                    ],
                    'form_top_emails' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'form_top_onsubmit' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'separator2' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> '.Yii::t('back/apartment-building', 'Form - middle').'</div>',
                        'label' => false,
                    ],
                    'form_middle_title_first' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_middle_title_second' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_middle_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'form_middle_description',
                        ]
                    ],
//                    'form_middle_problem' => [
//                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
//                        'items' => \common\models\RequestProblem::getItems('id', 'label'),
//                        'options' => [
//                            'prompt' => '',
//                        ],
//                    ],
                    'problemsField' => StdInput::relationInput(RequestProblem::class),
                    'form_middle_emails' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'form_middle_onsubmit' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'separator3' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> '.Yii::t('back/apartment-building', 'Form - bottom').'</div>',
                        'label' => false,
                    ],
                    'form_bottom_title' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_bottom_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'form_bottom_description',
                        ]
                    ],
                    'form_bottom_email' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'form_bottom_onsubmit' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'manager_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\Manager::getItems('id', 'fio'),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                ],
                Yii::t('back/apartment-building', 'Advantages') => [
                    'advantages_title_first_part' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'advantages_title_second_part' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> <i class="si si-grid"></i> '.Yii::t('back/apartment-building', 'advantages - blocks').'</div>',
                        'label' => false,
                    ],
                    $this->getRelatedFormConfig()['advantage_blocks'],
                ],
            ],
        ];

        return $config;
    }

    public function getRelatedFormConfig()
    {
        return [
            'apartment_building_menu' => [
                'relation' => 'apartmentBuildingMenu',
            ],
            'advantage_blocks' => [
                'relation' => 'advantageBlocks'
            ],
        ];
    }

    public function getAdvantageBlocks()
    {
        return $this->hasMany(BuildingAdvantage::class, ['object_id' => 'id'])
            ->onCondition(['type' => BuildingAdvantage::BUILDING_ADVANTAGE]);
    }

    public function getArticles()
    {
        return $this->hasMany(Article::class, ['id' => 'article_id'])
            ->alias('a')
            ->where(['a.published' => self::IS_PUBLISHED])
            ->viaTable(ArticleAssign::tableName(), ['object_id' => 'id'], function (ActiveQuery $q) {
                $q->alias('aa')->andWhere(['aa.type' => ArticleAssign::TYPE_BUILDING_ARTICLES]);
            })
            ->innerJoin(ArticleAssign::tableName() . ' aa', 'aa.article_id = a.id')
            ->orderBy(['aa.position' => SORT_ASC]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
