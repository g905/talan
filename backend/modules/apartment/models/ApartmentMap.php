<?php

namespace backend\modules\apartment\models;

use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use conquer\codemirror\CodemirrorAsset;
use conquer\codemirror\CodemirrorWidget;
use Yii;
use backend\helpers\StdColumn;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;
use common\models\ApartmentComplex;

/**
 * This is the model class for table "apartment_map".
 *
 * @property integer $id
 * @property string $label
 * @property string $class
 * @property string $content
 * @property string $mob_content
 * @property integer $complex_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class ApartmentMap extends ActiveRecord implements BackendModel
{

    /**
     * @var
     */
    public $image;
    /**
     * @var
     */
    public $sign;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'apartment_map';
    }

    /**
     * @throws \yii\base\Exception
     */
    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = security()->generateRandomString();
        }
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['label', 'complex_id', 'class'], 'required'],
            [['content', 'mob_content'], 'string'],
            [['complex_id'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['sign'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'class' => 'Class Name',
            'content' => 'Content',
            'mob_content' => 'Mobile Content',
            'complex_id' => 'Complex',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return bt('Apartment Map', 'back/map');
    }

    /**
     * @param $page
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    'label',
                    [
                        'attribute' => 'complex_id',
                        'value' => function (self $model) {
                            return $model->complex;
                        }
                    ],
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'complex_id',
                        'value' => function (self $model) {
                            return $model->complex;
                        }
                    ],
                ];
            default:
                return [];
        }
    }

    /**
     * @return ApartmentMapSearch|\yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ApartmentMapSearch();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'class' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'image' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'allowedFileExtensions' => ['jpg', 'png'],
                    'attribute' => 'image',
                    'saveAttribute' => \common\models\ApartmentMap::IMAGE,
                    'multiple' => false,
                ])
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => CodemirrorWidget::widget([
                    'model' => $this,
                    'attribute' => 'content',
                    'preset'=> 'php',
                    'assets' => [
                        CodemirrorAsset::THEME_SOLARIZED
                    ],
                    'settings' => [
                        'theme' => 'solarized',
                    ],
                    'options' => ['rows' => 20],
                ]),
            ],
            'mob_content' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => CodemirrorWidget::widget([
                    'model' => $this,
                    'attribute' => 'mob_content',
                    'preset'=> 'php',
                    'assets' => [
                        CodemirrorAsset::THEME_SOLARIZED
                    ],
                    'settings' => [
                        'theme' => 'solarized',
                    ],
                    'options' => ['rows' => 20],
                ]),
            ],
            'complex_id' => [
                'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items'   => ApartmentComplex::getItems(),
                'options' => [
                    'prompt' => Yii::t('back/videoAlbum', 'Select complex'),
                ],
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false
            ]
        ];

        return $config;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    /**
     * @return mixed
     */
    public function getComplex()
    {
        $complex = ApartmentComplex::find()->where(['id' => $this->complex_id])->one();

        if ($complex) {
            return $complex->label;
        }
    }

}
