<?php

namespace backend\modules\apartment\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BuildHistoryApartmentComplexItemSlideSearch represents the model behind the search form about `BuildHistoryApartmentComplexItemSlide`.
 */
class BuildHistoryApartmentComplexItemSlideSearch extends BuildHistoryApartmentComplexItemSlide
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'build_history_apartment_complex_item_id'], 'integer'],
            [['day', 'content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BuildHistoryApartmentComplexItemSlideSearch::find()
            ->with([
                'buildHistoryApartmentComplexItem' => function(\yii\db\ActiveQuery $query) {
                    return $query->with(['apartmentComplex']);
                }
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'build_history_apartment_complex_item_id' => $this->build_history_apartment_complex_item_id,
        ]);

        $query->andFilterWhere(['like', 'day', $this->day])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
