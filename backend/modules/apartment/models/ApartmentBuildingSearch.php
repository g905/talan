<?php

namespace backend\modules\apartment\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ApartmentBuildingSearch represents the model behind the search form about `ApartmentBuilding`.
 */
class ApartmentBuildingSearch extends ApartmentBuilding
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'building_complex_id', 'manager_id', 'view_count', 'published', 'position'], 'integer'],
            [['label', 'top_screen_description', 'top_screen_btn_label', 'top_screen_btn_link', 'general_progress_title', 'general_progress_percent', 'general_description', 'general_docs_link', 'general_docs_link_title', 'general_fb_link', 'general_vk_link', 'advantages_title_first_part', 'advantages_title_second_part', 'form_top_description', 'form_top_emails', 'form_bottom_title', 'form_bottom_description', 'form_bottom_email', 'guid', 'form_top_onsubmit', 'form_bottom_onsubmit', 'show_label'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApartmentBuildingSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'building_complex_id' => $this->building_complex_id,
            'manager_id' => $this->manager_id,
            'view_count' => $this->view_count,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'top_screen_description', $this->top_screen_description])
            ->andFilterWhere(['like', 'top_screen_btn_label', $this->top_screen_btn_label])
            ->andFilterWhere(['like', 'top_screen_btn_link', $this->top_screen_btn_link])
            ->andFilterWhere(['like', 'general_progress_title', $this->general_progress_title])
            ->andFilterWhere(['like', 'general_progress_percent', $this->general_progress_percent])
            ->andFilterWhere(['like', 'general_description', $this->general_description])
            ->andFilterWhere(['like', 'general_docs_link', $this->general_docs_link])
            ->andFilterWhere(['like', 'general_docs_link_title', $this->general_docs_link_title])
            ->andFilterWhere(['like', 'general_fb_link', $this->general_fb_link])
            ->andFilterWhere(['like', 'general_vk_link', $this->general_vk_link])
            ->andFilterWhere(['like', 'advantages_title_first_part', $this->advantages_title_first_part])
            ->andFilterWhere(['like', 'advantages_title_second_part', $this->advantages_title_second_part])
            ->andFilterWhere(['like', 'form_top_description', $this->form_top_description])
            ->andFilterWhere(['like', 'form_top_emails', $this->form_top_emails])
            ->andFilterWhere(['like', 'form_bottom_title', $this->form_bottom_title])
            ->andFilterWhere(['like', 'form_bottom_description', $this->form_bottom_description])
            ->andFilterWhere(['like', 'form_bottom_email', $this->form_bottom_email])
            ->andFilterWhere(['like', 'guid', $this->guid])
            ->andFilterWhere(['like', 'form_top_onsubmit', $this->form_top_onsubmit])
            ->andFilterWhere(['like', 'form_bottom_onsubmit', $this->form_bottom_onsubmit])
            ->andFilterWhere(['like', 'show_label', $this->show_label]);

        return $dataProvider;
    }
}
