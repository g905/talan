<?php

namespace backend\modules\apartment\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class BuildHistoryApartmentBuildingItemSearch extends BuildHistoryApartmentBuildingItem
{
    public function rules()
    {
        return [
            [['id', 'apartment_building_id'], 'integer'],
            [['date'], 'safe'],
            [['percent'], 'number'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = BuildHistoryApartmentBuildingItemSearch::find();
        $dataProvider = new ActiveDataProvider(['query' => $query]);
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'apartment_building_id' => $this->apartment_building_id,
            'percent' => $this->percent,
        ]);
        $query->filterByDateRange('date', $this->date, 'd-m-Y');

        return $dataProvider;
    }
}
