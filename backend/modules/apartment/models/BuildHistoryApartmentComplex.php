<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 25.04.18
 * Time: 16:39
 */

namespace backend\modules\apartment\models;

use Cake\Chronos\Chronos;
use kartik\grid\GridView;
use metalguardian\formBuilder\ActiveFormBuilder;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use backend\helpers\StdInput;
use backend\components\StylingActionColumn;

class BuildHistoryApartmentComplex extends ApartmentComplex
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
            'seo' => [
                'class' => MetaTagBehavior::class,
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['build_start_date', 'build_end_date'], 'date'],
            [['build_start_date'], 'validateStartEndDate'],
            [['build_end_date'], 'validateStartEndDate'],
            [['build_video'], 'string'],
            [['build_history_end_text', 'build_history_current_text', 'build_history_title'], 'string', 'max' => 255],
        ];
    }

    public function validateStartEndDate()
    {
        $start = new Chronos($this->build_start_date);
        $end = new Chronos($this->build_end_date);

        if ($start->gt($end)) {
            $this->addError('build_start_date', 'Дата должна быть меньше ' . $end->toDateString());
        }

        if ($end->lte($start)) {
            $this->addError('build_end_date', 'Дата должна быть больше '. $start->toDateString());
        }
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/bh-apartment-complex', 'Build history apartment complex');
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'build_start_date' => \Yii::t('back/bh-apartment-complex', 'Start date'),
            'build_end_date' => \Yii::t('back/bh-apartment-complex', 'Finish date'),
            'build_video' => \Yii::t('back/bh-apartment-complex', 'Video stream'),
            'build_history_end_text' => \Yii::t('back/bh-apartment-complex', 'Build end text'),
            'build_history_current_text' => \Yii::t('back/bh-apartment-complex', 'Current build history text'),
            'build_history_title' => \Yii::t('back/bh-apartment-complex', 'Build history block title')
        ];
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    'label',
                    [
                        'attribute' => 'build_start_date',
                        'value' => function(self $model) {
                            return $model->build_start_date;
                        },
                        'filterType' => GridView::FILTER_DATE,
                        'filterWidgetOptions' => [
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'autoclose' => true
                            ],
                        ]
                    ],
                    [
                        'attribute' => 'build_end_date',
                        'value' => function(self $model) {
                            return $model->build_end_date;
                        },
                        'filterType' => GridView::FILTER_DATE,
                        'filterWidgetOptions' => [
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'autoclose' => true
                            ],
                        ]
                    ],
                    [
                        'class' => StylingActionColumn::class,
                        'template' => "{update}"
                    ],
                ];
                break;
            case 'view':
                return [
                    'label',
                    'build_start_date',
                    'build_end_date',
                    'build_video',
                    'build_history_current_text',
                    'build_history_end_text',
                    'build_history_title',
                ];
                break;
        }

        return [];
    }

    public function getFormConfig()
    {
        return [
            'build_start_date' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \metalguardian\dateTimePicker\Widget::class,
                'options' => [
                    'mode' => \metalguardian\dateTimePicker\Widget::MODE_DATE,
                    'options' => [
                        'class' => 'form-control',
                    ],
                ],
            ],
            'build_end_date' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \metalguardian\dateTimePicker\Widget::class,
                'options' => [
                    'mode' => \metalguardian\dateTimePicker\Widget::MODE_DATE,
                    'options' => [
                        'class' => 'form-control',
                    ],
                ],
            ],
            'build_history_current_text' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'build_history_end_text' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'build_history_title' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'build_video' => StdInput::codeMirror($this, 'build_video'),
        ];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new BuildHistoryApartmentComplexSearch();
    }

    /***
     * @return bool
     */
    public function showCreateButton()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function showDeleteButton()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return array
     */
    public function  getRelatedFormConfig()
    {
        // reset parent config to exclude deleting related models
        return [];
    }
}
