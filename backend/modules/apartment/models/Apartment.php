<?php

namespace backend\modules\apartment\models;

use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use kartik\grid\GridView;
use metalguardian\formBuilder\ActiveFormBuilder;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\helpers\Property;
use common\models\EntityToFile;
use common\components\CommonDataModel;
use common\components\model\ActiveRecord;
use common\models\Apartment as CommonApartment;
use common\models\ApartmentComplex as CommonApartmentComplex;
use common\models\ApartmentBuilding as CommonApartmentBuilding;
use backend\helpers\ImgHelper;
use backend\components\FormBuilder;
use backend\components\BackendModel;
use backend\components\ImperaviContent;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;

/**
 * This is the model class for table "{{%apartment}}".
 *
 * @property integer $id
 * @property integer $apartment_complex_id
 * @property string $label
 * @property integer $type
 * @property integer $subtype
 * @property integer $entrance
 * @property integer $rooms_number
 * @property string $total_area
 * @property string $living_space
 * @property string $delivery_date
 * @property string $price_from
 * @property string $description_title
 * @property string $description_subtitle
 * @property string $description_text
 * @property string $floor_plan_title
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $view_count
 * @property integer $floor
 * @property integer $queue
 * @property string $guid
 * @property string $form_onsubmit
 * @property string $profile
 * @property string $income
 * @property string $recoupment
 * @property bool|int $deleted
 * @property ApartmentComplex $apartmentComplex
 */
class Apartment extends ActiveRecord implements BackendModel
{
    /**
     * @var mixed attributes for imageUploader
     */
    public $photos;
    public $description_photo;
    public $floor_plan_photo;
    /**
     * @var string temporary sign which used for saving images before model save.
     */
    public $sign;

    public static function tableName()
    {
        return '{{%apartment}}';
    }

    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = security()->generateRandomString();
        }
    }

    public function rules()
    {
        return [
            [['apartment_complex_id', 'rooms_number', 'published', 'position', 'view_count', 'floor', 'type', 'subtype', 'entrance', 'status', 'deleted', 'apartment_number', 'apartment_number_on_floor', 'queue'], 'integer'],
            [['label', 'total_area', 'price_from', 'floor'], 'required'],
            [['total_area', 'living_space', 'price_from', 'installment_price', 'price_per_meter', 'discount'], 'number'],
            [['description_text'], 'string'],
            [['label', 'description_title', 'description_subtitle', 'floor_plan_title', 'delivery_date', 'guid'], 'string', 'max' => 255],
            [['apartment_complex_id'], 'exist', 'targetClass' => CommonApartmentComplex::class, 'targetAttribute' => 'id'],
            [['delivery_date', 'entrance'], 'default', 'value' => null],
            [['published'], 'default', 'value' => 1],
            [['address'], 'string'],
            [['position', 'is_promotional', 'is_secondary'], 'default', 'value' => 0],
            [['sign', 'form_onsubmit', 'profile', 'income', 'recoupment'], 'string', 'max' => 255],
            [['apartment_building_id'], 'exist', 'targetClass' => CommonApartmentBuilding::class, 'targetAttribute' => 'id'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'apartment'),
            'apartment_complex_id' => bt('Apartment complex', 'apartment'),
            'label' => bt('Label', 'apartment'),
            'address' => bt('Address', 'apartment'),
            'type' => bt('Type', 'apartment'),
            'subtype' => bt('Subtype', 'apartment'),
            'entrance' => bt('Entrance number', 'apartment'),
            'rooms_number' => bt('Rooms number', 'apartment'),
            'total_area' => bt('Total area', 'apartment'),
            'living_space' => bt('Living space', 'apartment'),
            'delivery_date' => bt('Delivery date', 'apartment'),
            'price_from' => bt('Price from', 'apartment'),
            'description_title' => bt('Description title', 'apartment'),
            'description_subtitle' => bt('Description subtitle', 'apartment'),
            'description_text' => bt('Description text', 'apartment'),
            'floor_plan_title' => bt('Floor plan title', 'apartment'),
            'published' => bt('Published', 'apartment'),
            'position' => bt('Position', 'apartment'),
            'created_at' => bt('Created At', 'apartment'),
            'updated_at' => bt('Updated At', 'apartment'),
            'photos' => bt('photos', 'apartment'),
            'description_photo' => bt('description photo', 'apartment'),
            'floor_plan_photo' => bt('floor plan photo', 'apartment'),
            'floor' => bt('floor', 'apartment'),
            'guid' => bt('1C guid', 'apartment'),
            'form_onsubmit' => bt('Form onsubmit', 'apartment'),
            'profile' => bt('profile', 'apartment'),
            'income' => bt('income', 'apartment'),
            'recoupment' => bt('recoupment', 'apartment'),
            'queue' => bt('queue', 'apartment'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'seo' => MetaTagBehavior::class,
        ];
    }

    public function getApartmentComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'apartment_complex_id']);
    }

    public function getTitle()
    {
        return bt('Apartment', 'apartment');
    }

    public function getPhoto()
    {
        return $this->hasOne(EntityToFile::class, ['entity_model_id' => 'id'])
            ->andOnCondition([
                'photos.entity_model_name' => 'Apartment',
                'photos.attribute' => CommonApartment::SAVE_ATTRIBUTE_PHOTOS
            ])
            ->alias('photos')
            ->orderBy('photos.position DESC');
    }

    public function getLabels() {
      $type = ApartmentComplex::find()
        ->select(['id', 'type', 'label'])
        ->orderBy('position')
        ->asArray()
        ->all();
        foreach ($type as $t) {
          $ar[$t['id']] = $t['label'].' ('.Property::types()[$t['type']].')';
        }
      return $ar;
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'photos',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return ImgHelper::getEntityImage($data, 'photo');
                        }
                    ],
                    [
                        'attribute' => 'entrance',
                        'format' => 'raw',
                        'value' => function ($model) {
                          return $model->entrance;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(ApartmentEntrance::find()->orderBy('position')->asArray()->all(), 'entrance', 'entrance'),
                        'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true]],
                        'filterInputOptions' => ['placeholder' => bt('Any entrance', 'entrance')],
                    ],
                    [
                        'attribute' => 'apartment_complex_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var Apartment $model */
                            return isset($model->apartmentComplex)
                                ? '<b>' . $model->apartmentComplex->label . '</b>'
                                : '-';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => $this->getLabels(),
                        'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true]],
                        'filterInputOptions' => ['placeholder' => bt('Any complex', 'apartment')],
                    ],
                    [
                        'attribute' => 'type',
                        'format' => 'raw',
                        'filter' => Property::types(),
                        'value' => function (self $model) {
                            return Property::typeLabel($model->type);
                        }
                    ],
                    [
                        'attribute' => 'subtype',
                        'format' => 'raw',
                        'filter' => Property::subtypes(),
                        'value' => function (self $model) {
                            return Property::subtypeLabel($model->subtype);
                        }
                    ],
                    'label',
                    'rooms_number',
                    'total_area',
                    'living_space',
                    'delivery_date',
                    'queue',
                    // 'price_from',
                    // 'description_title',
                    // 'description_subtitle',
                    // 'description_text:ntext',
                    // 'floor_plan_title',
                    'published:boolean',
                    [
                        'attribute' => 'position',
                        'width' => '10px'
                    ],
                    ['class' => \backend\components\StylingActionColumn::class],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'apartment_complex_id',
                    [
                        'attribute' => 'type',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            return Property::typeLabel($model->type);
                        }
                    ],
                    [
                        'attribute' => 'subtype',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            return Property::subtypeLabel($model->subtype);
                        }
                    ],
                    'label',
                    'rooms_number',
                    'total_area',
                    'living_space',
                    'delivery_date',
                    'price_from',
                    'description_title',
                    'description_subtitle',
                    'queue',
                    [
                        'attribute' => 'description_text',
                        'format' => 'html',
                    ],
                    'floor_plan_title',
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    public function getSearchModel()
    {
        return new ApartmentSearch();
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General', 'apartment') => [

                    'apartment_complex_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => CommonApartmentComplex::getItems(),
                        'options' => ['prompt' => ''],
                    ],
                    'apartment_building_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => CommonApartmentBuilding::getItems(),
                        'options' => ['prompt' => ''],
                    ],
                    'delivery_date' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'entrance' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => CommonApartmentComplex::getItems(),
                        'options' => ['prompt' => ''],
                    ],
                    'type' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => Property::types(),
                        'options' => ['prompt' => ''],
                    ],
                    'subtype' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => Property::subtypes(),
                        'options' => ['prompt' => ''],
                    ],
                    'guid' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'readonly' => true],
                    ],
                    /*'entrance' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],*/
                    'rooms_number' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [],
                    ],
                    'total_area' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getAreaUnit()],
                    ],
                    'living_space' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getAreaUnit()],
                    ],
                    'price_from' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getCurrencyText()],
                    ],
                    'floor' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'profile' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'income' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'recoupment' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'form_onsubmit' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'position' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [],
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
                bt('Photos', 'apartment') => [
                    'photos' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'photos',
                            'saveAttribute' => CommonApartment::SAVE_ATTRIBUTE_PHOTOS,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => true, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                bt('Description', 'apartment') => [
                    'description_title' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'description_subtitle' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'description_text' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::class,
                        'options' => ['model' => $this, 'attribute' => 'description_text']
                    ],
                    'description_photo' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'description_photo',
                            'saveAttribute' => CommonApartment::SAVE_ATTRIBUTE_DESCRIPTION_PHOTO,
                            'aspectRatio' => false,
                            'multiple' => false,
                        ])
                    ],
                ],
                bt('Floor plan', 'apartment') => [
                    'floor_plan_title' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'floor_plan_photo' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'floor_plan_photo',
                            'saveAttribute' => CommonApartment::SAVE_ATTRIBUTE_FLOOR_PLAN_PHOTO,
                            'aspectRatio' => false,
                            'multiple' => false,
                        ])
                    ],
                ],
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
