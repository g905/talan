<?php

namespace backend\modules\apartment\models;

use backend\components\StylingActionColumn;
use backend\modules\contacts\models\City;
use backend\helpers\StdColumn;
use kartik\grid\GridView;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;
use common\models\ApartmentConstructorOptionPrice as CommonPrice;

/**
 * This is the model class for table "apartment_constructor_price".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $price
 * @property integer $created_at
 * @property integer $updated_at
 */
class ApartmentConstructorOptionPrice extends ActiveRecord implements BackendModel
{

    public static function tableName()
    {
        return 'apartment_constructor_option_price';
    }

    public function rules()
    {
        return [
            [['city_id', 'price', 'option'], 'required'],
            [['city_id', 'price', 'option', 'published'], 'integer'],
            [['city_id'], 'unique', 'targetAttribute' => ['option', 'city_id']],
            [['published'], 'default', 'value' => 1],
            [['price'], 'integer', 'max' => 999999999],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City',
            'price' => 'Constructor Price',
        ];
    }

    public function getTitle()
    {
        return bt('Apartment Constructor Price', 'app');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    StdColumn::city(),
                    [
                        'attribute' => 'option',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return CommonPrice::getTypeDictionary()[$model->option] ?? null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => CommonPrice::getTypeDictionary(),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any option')],
                    ],
                    'price',
                    'published:boolean',
                    [
                        'class' => StylingActionColumn::class,
                        'template' => "{update}"
                    ],
                ];
            case 'view':
                return [
                    'id',
                    'city_id',
                    'price',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new ApartmentConstructorOptionPriceSearch();
    }

    public function getFormConfig()
    {
        $config = [
            'city_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\City::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'option' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => CommonPrice::getTypeDictionary(),
                'options' => ['prompt' => ''],
            ],
            'price' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /***
     * @return bool
     */
    public function showDeleteButton()
    {
        return false;
    }

}
