<?php

namespace backend\modules\apartment\models;

use backend\modules\request\helpers\SearchHelper;
use yii\behaviors\TimestampBehavior;
use Cake\Chronos\Chronos;
use kartik\grid\GridView;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;

/**
 * This is the model class for table "{{%build_history_apartment_building_item}}".
 *
 * @property integer $id
 * @property integer $apartment_building_id
 * @property string $date
 * @property string $percent
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ApartmentBuilding $apartmentBuilding
 */
class BuildHistoryApartmentBuildingItem extends ActiveRecord implements BackendModel
{
    public static function tableName()
    {
        return '{{%build_history_apartment_building_item}}';
    }

    public function rules()
    {
        return [
            [['apartment_building_id', 'date'], 'required'],
            [['apartment_building_id'], 'integer'],
            [['date'], 'date'],
            ['date', 'validateSameMonthAndYear', 'when' => function($model){
                return $this->isNewRecord;
            }],
            [['percent','percent_plan'], 'number', 'min' => 0, 'max' => 100],
            [
                ['apartment_building_id'],
                'exist',
                'targetClass' => \common\models\ApartmentBuilding::class,
                'targetAttribute' => 'id'
            ],
        ];
    }

    /**
     * Check if inputted year+month is not exists in DB
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function validateSameMonthAndYear()
    {
        $year = (integer)app()->formatter->asDate($this->date, 'YYYY');
        $month = (integer)app()->formatter->asDate($this->date, 'MM');
        $exist = self::find()
            ->where(['apartment_building_id' => $this->apartment_building_id])
            ->andWhere("MONTH(date) = $month AND YEAR(date) = $year")
            ->exists();
        if ($exist) {
            $this->addError('date', 'Данный месяц в этом году уже существует');
        }

        $building = $this->apartmentBuilding;

        if ($building) {
            $current = Chronos::now();
            $date = new Chronos($this->date);
            $start = new Chronos($building->build_start_date);
            $end = new Chronos($building->build_end_date);

            $edge = $current->lte($end) ? $current : $end;

            if (!$date->between($start, $edge)) {
                $this->addError('date', 'Дата должна быть в диапазоне: '. $start->toDateString() . ' - ' . $edge->toDateString());
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'BHApartmentBuildingItem'),
            'apartment_building_id' => bt('Apartment building', 'BHApartmentBuildingItem'),
            'date' => bt('Date (month + year)', 'BHApartmentBuildingItem'),
            'percent' => bt('Percent of completition', 'BHApartmentBuildingItem'),
            'percent_plan' => bt('Percent of completition plan', 'BHApartmentBuildingItem'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getApartmentBuilding()
    {
        return $this->hasOne(ApartmentBuilding::class, ['id' => 'apartment_building_id']);
    }

    public function getTitle()
    {
        return bt('Build History Apartment Building Item', 'BHApartmentBuildingItem');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'apartment_building_id',
                        'value' => function (self $data) {
                            return $data->apartmentBuilding->label ?? null;
                        },
                        'filter' => ApartmentBuilding::getItems(),
                    ],
                    SearchHelper::getDateRangeColumnConfig($this, 'date'),
                    'percent',
                    'percent_plan',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'apartment_building_id',
                        'value' => $this->apartmentBuilding->label ?? null,
                    ],
                    'date',
                    'percent',
                    'percent_plan',
                ];
                break;
        }

        return [];
    }

    public function getSearchModel()
    {
        return new BuildHistoryApartmentBuildingItemSearch();
    }

    public function getFormConfig()
    {
        $config = [
            'apartment_building_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ApartmentBuilding::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'date' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \metalguardian\dateTimePicker\Widget::class,
                'options' => [
                    'mode' => \metalguardian\dateTimePicker\Widget::MODE_DATE,
                    'options' => [
                        'class' => 'form-control',
                    ],
                ],
                'hint' => bt('Only month and year are important, day may be anyone', 'BHApartmentBuildingItem'),
            ],
            'percent' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'percent_plan' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
        ];

        return $config;
    }
}
