<?php

namespace backend\modules\apartment\models;

use Cake\Chronos\Chronos;
use yii\behaviors\TimestampBehavior;
use kartik\grid\GridView;
use metalguardian\formBuilder\ActiveFormBuilder;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use backend\helpers\StdInput;
use backend\components\StylingActionColumn;

class BuildHistoryApartmentBuilding extends ApartmentBuilding
{
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'seo' => MetaTagBehavior::class,
        ];
    }

    public function rules()
    {
        return [
            [['build_start_date', 'build_end_date'], 'date'],
            [['build_start_date'], 'validateStartEndDate'],
            [['build_end_date'], 'validateStartEndDate'],
            [['build_video'], 'string'],
            [['build_history_end_text', 'build_history_current_text', 'build_history_title'], 'string', 'max' => 255],
        ];
    }

    public function validateStartEndDate()
    {
        $start = new Chronos($this->build_start_date);
        $end = new Chronos($this->build_end_date);

        if ($start->gt($end)) {
            $this->addError('build_start_date', 'Дата должна быть меньше ' . $end->toDateString());
        }

        if ($end->lte($start)) {
            $this->addError('build_end_date', 'Дата должна быть больше '. $start->toDateString());
        }
    }

    public function getTitle()
    {
        return bt('Build history apartment building', 'bh-apartment-building');
    }

    public function attributeLabels()
    {
        return [
            'build_start_date' => bt('Start date', 'bh-apartment-building'),
            'build_end_date' => bt('Finish date', 'bh-apartment-building'),
            'build_video' => bt('Video stream', 'bh-apartment-building'),
            'build_history_end_text' => bt('Build end text', 'bh-apartment-building'),
            'build_history_current_text' => bt('Current build history text', 'bh-apartment-building'),
            'build_history_title' => bt('Build history block title', 'bh-apartment-building'),
        ];
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    'label',
                    [
                        'attribute' => 'build_start_date',
                        'value' => function(self $model) {
                            return $model->build_start_date;
                        },
                        'filterType' => GridView::FILTER_DATE,
                        'filterWidgetOptions' => [
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'autoclose' => true
                            ],
                        ]
                    ],
                    [
                        'attribute' => 'build_end_date',
                        'value' => function(self $model) {
                            return $model->build_end_date;
                        },
                        'filterType' => GridView::FILTER_DATE,
                        'filterWidgetOptions' => [
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'autoclose' => true
                            ],
                        ]
                    ],
                    [
                        'class' => StylingActionColumn::class,
                        'template' => "{update}"
                    ],
                ];
                break;
            case 'view':
                return [
                    'label',
                    'build_start_date',
                    'build_end_date',
                    'build_video',
                    'build_history_current_text',
                    'build_history_end_text',
                    'build_history_title',
                ];
                break;
        }

        return [];
    }

    public function getFormConfig()
    {
        return [
            'build_start_date' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \metalguardian\dateTimePicker\Widget::class,
                'options' => [
                    'mode' => \metalguardian\dateTimePicker\Widget::MODE_DATE,
                    'options' => [
                        'class' => 'form-control',
                    ],
                ],
            ],
            'build_end_date' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \metalguardian\dateTimePicker\Widget::class,
                'options' => [
                    'mode' => \metalguardian\dateTimePicker\Widget::MODE_DATE,
                    'options' => [
                        'class' => 'form-control',
                    ],
                ],
            ],
            'build_history_current_text' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'build_history_end_text' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'build_history_title' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'build_video' => StdInput::codeMirror($this, 'build_video'),
        ];
    }

    public function getSearchModel()
    {
        return new BuildHistoryApartmentBuildingSearch();
    }

    public function showCreateButton()
    {
        return false;
    }

    public function showDeleteButton()
    {
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }

    public function  getRelatedFormConfig()
    {
        // reset parent config to exclude deleting related models
        return [];
    }
}
