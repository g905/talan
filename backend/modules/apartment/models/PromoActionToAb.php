<?php

namespace backend\modules\apartment\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%promo_action_to_ab}}".
 *
 * @property integer $action_id
 * @property integer $ab_id
 *
 * @property ApartmentBuilding $ab
 * @property PromoAction $action
 */
class PromoActionToAb extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_action_to_ab}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'ab_id'], 'integer'],
            [['action_id'], 'exist', 'targetClass' => \common\models\PromoAction::className(), 'targetAttribute' => 'id'],
            [['ab_id'], 'exist', 'targetClass' => \common\models\ApartmentBuilding::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'action_id' => Yii::t('back/promo-action-to-ab', 'Action'),
            'ab_id' => Yii::t('back/promo-action-to-ab', 'Apartment building'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAb()
    {
        return $this->hasOne(ApartmentBuilding::className(), ['id' => 'ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(PromoAction::className(), ['id' => 'action_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Promo Action To Ab');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    'action_id',
                    'ab_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'action_id',
                    'ab_id',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PromoActionToAbSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'action_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\PromoAction::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'ab_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ApartmentBuilding::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
        ];

        return $config;
    }


}
