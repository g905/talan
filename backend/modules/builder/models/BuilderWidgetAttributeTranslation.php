<?php

namespace backend\modules\builder\models;

use Yii;

/**
* This is the model class for table "{{%builder_widget_attribute_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $value
*/
class BuilderWidgetAttributeTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%builder_widget_attribute_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Related model id' . ' [' . $this->language . ']',
            'language' => 'Language' . ' [' . $this->language . ']',
            'value' => 'Value' . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['value'], 'string'],
         ];
    }
}
