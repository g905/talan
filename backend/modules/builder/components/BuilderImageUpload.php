<?php
namespace backend\modules\builder\components;

use backend\assets\AdobeAsset;
use backend\modules\imagesUpload\helpers\ImageUploadUrlHelper;
use backend\modules\imagesUpload\models\ImagesUploadModel;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use kartik\file\FileInput;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * Class BuilderImageUpload
 *
 * @package backend\modules\builder\components
 */
class BuilderImageUpload extends ImageUpload
{
    /**
     * @return string
     */
    public function run()
    {
        if (!$this->model || !$this->attribute) {
            return null;
        }

        $this->saveAttribute = preg_replace("/^\[\d+\]/", "", $this->attribute);

        if ($this->adobeSdk) {
            $this->applyAdobeJs();
        }

        $uploadExtraData = $this->model->isNewRecord
            ? ['sign' => $this->model->sign]
            : ['id' => $this->model->id];

        $allowedExtensions = $this->allowedFileExtensions;
        $extensionsAcceptMask = empty($this->allowedFileExtensions)
            ? '*'
            : call_user_func(function () use ($allowedExtensions) {
                $exts = [];
                // clone array, do not use existing one
                $exts = ArrayHelper::merge($exts, $allowedExtensions);
                array_walk($exts, function (&$item) {
                    $item = '.' . $item;
                });
                $result = implode(',', $exts);
                return $result;
            });
        $existModelImages = $this->collectExistingImages();

        $initialPreview = [];
        $initialPreviewConfig = [];
        /**
         * @var \common\models\EntityToFile $file
         */
        foreach ($existModelImages as $file) {
            $fileName = $file->file->base_name . '.' . $file->file->extension;
            $initialPreview[] = $this->getPreviewContent($file);
            $initialPreviewConfig[] = [
                'caption' => $fileName,
                'width' => '120px',
                'url' => ImagesUploadModel::deleteImageUrl(['id' => $file->id]),
                'key' => $file->id,
                'frameClass' => in_array($file->file->extension, static::getCropableImagesExtensions())
                    ? ''
                    : 'not-image',
            ];
        }

        $multiple = $this->multiple ? 'true' : 'false';

        $output = Html::hiddenInput('urlForSorting', ImagesUploadModel::sortImagesUrl(), ['id' => 'urlForSorting']);
        $output .= Html::hiddenInput('aspectRatio', $this->aspectRatio, ['class' => 'aspect-ratio']);
        $output .= Html::hiddenInput('isMultipleUpload', $this->multiple, ['class' => 'is-multiple-upload']);

        $index = $this->model->relModelIndex;
        $attribute = $index === null ? $this->attribute : "[$index]$this->attribute";
        $uploadUrl = ImagesUploadModel::uploadUrl([
            'model_name' => $this->model->className(),
            'attribute' => $attribute,
            'entity_attribute' => $this->saveAttribute,
        ]);

        $cropableTypes = self::getCropableImagesTypes();

        $output .= FileInput::widget(
            [
                'model' => $this->model,
                'attribute' => $attribute,
                'options' => [
                    'multiple' => $this->multiple,
                    'accept' => $extensionsAcceptMask,
                ],
                'pluginOptions' => [
                    'dropZoneEnabled' => false,
                    'browseClass' => 'btn btn-success',
                    'browseIcon' => '<i class="si si-picture"></i> ',
                    'removeClass' => "btn btn-danger",
                    'removeIcon' => '<i class="si si-trash"></i> ',
                    'uploadClass' => "btn btn-info",
                    'uploadIcon' => '<i class="si si-cloud-upload"></i> ',
                    'uploadUrl' => $uploadUrl,
                    'allowedFileExtensions' => $this->allowedFileExtensions,
                    'allowedPreviewTypes' => ['image', 'video'], // for all other types - preview will be with simple icon
                    'uploadExtraData' => $uploadExtraData,
                    'initialPreview' => $initialPreview,
                    'initialPreviewConfig' => $initialPreviewConfig,
                    'overwriteInitial' => false,
                    'showRemove' => false,
                    'otherActionButtons' => $this->getOtherActionButtons(),
                    'fileActionSettings' => [
                        'indicatorSuccess' => $this->render('@backend/modules/imagesUpload/widgets/imagesUpload/views/_success_buttons_template')
                    ],
                    'previewSettings' => $this->getPreviewSettings(),
                    'previewTemplates' => $this->getPreviewTemplates(),
                ],
                'pluginEvents' => [
                    'fileuploaded' => 'function(event, data, previewId, index) {
                       var elem = $("#"+previewId).find(".file-actions .file-footer-buttons .kv-file-remove");
                       var cropElem = $("#"+previewId).find(".file-actions .crop-link");
                       var img = $("#"+previewId).find("img");
                       //id for cropped image replace
                       img.attr("id", "preview-image-"+data.response.imgId);

                       elem.attr("data-url", data.response.deleteUrl);
                       elem.attr("data-key", data.response.id);
                       cropElem.attr("href", data.response.cropUrl);
                       img.attr("src", data.response.url);
                       img.attr("data-id" , data.response.id);
                       
                       //fix file delete after uploading
                       elem.addClass("new-uploaded-image");
                       $(".file-upload-indicator .kv-file-remove").remove();
         
                       //Resort files
                       saveSort();

                       //Fix crop url for old images
                       fixMultiUploadImageCropUrl();
                       
                        //fix file index for correct file delete
                       $("#"+previewId).attr("data-fileindex", "init_"+$(this).data("fileindex"));
                    }',
                    'fileloaded' => "function(file, reader, previewId, index) {
                        //Fix url for old images
                        fixMultiUploadImageCropUrl();
                        
                        // Remove crop icon for non images
                        var cropableTypes = $cropableTypes;
                        if ($.inArray(reader.type, cropableTypes) === -1) {
                            $('#' + previewId).find('.crop-link').remove();
                        };
                    }",
                ]
            ]
        );

        $output .= '<br>';
        return $output;
    }

    private function getOtherActionButtons()
    {
        $result = '';

        switch (true) {
            case $this->adobeSdk:
                $result .= $this->render('@backend/modules/imagesUpload/widgets/imagesUpload/views/_all_buttons');
            case $this->showMetaDataBtn:
                $result .= $this->render('@backend/modules/imagesUpload/widgets/imagesUpload/views/_meta_data_btn', ['url' => $this->renderMetaDataFormUrl ?: ImageUploadUrlHelper::getMetaDataFormGenerateUrl(['id' => ''])]);
            default:
                $result .= $this->render('@backend/modules/imagesUpload/widgets/imagesUpload/views/_crop_button');
        };

        return $result;
    }

    /**
     *  Void function register js for adobe SDK, more information:
     *  https://github.com/CreativeSDK/web-getting-started-samples/blob/master/image-editor-ui/image-editor-ui-jquery/readme.md
     *
     */
    private function applyAdobeJs()
    {
        AdobeAsset::register($this->view);
        $key = self::API_KEY;
        $script = <<< JS
             window.adobeKey = "$key"
JS;
        $this->view->registerJs($script ,View::POS_END);
    }
}
