<?php

namespace backend\modules\builder\controllers;

use backend\components\BackendController;
use backend\modules\builder\models\BuilderWidgetAttribute;

/**
 * BuilderWidgetAttributeController implements the CRUD actions for BuilderWidgetAttribute model.
 */
class BuilderWidgetAttributeController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BuilderWidgetAttribute::className();
    }
}
