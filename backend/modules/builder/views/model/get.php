<?php

use backend\modules\builder\components\DummyFormBuilder;

/**
 * @var $this yii\web\View
 * @var $id int
 * @var $key int
 * @var $attribute string
 * @var $widget common\components\BuilderModel
 * @var $model common\components\model\ActiveRecord
 */


$name = $widget::className();
$fields = $widget->getConfig();
$localized = $widget->getLocalized();
$locales = common\helpers\LanguageHelper::getApplicationLanguages();
$defaultLocale = common\helpers\LanguageHelper::getDefaultLanguage();
$defaultLocale = yii\helpers\ArrayHelper::getValue($defaultLocale, 'locale');
$class = ($widget->widget_visible) ? '' : 'hidden';
$display = ($widget->widget_visible) ? 'block' : 'none';
?>

<div class="block block-themed block-rounded builder-row">
    <?php $form = DummyFormBuilder::begin(); ?>
    <div class="block-header bg-primary-light">
        <ul class="block-options">
            <li><button type="button" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button></li>
            <li><button type="button" data-toggle="block-option" data-action="content_toggle" class="content-row-visible"><i class="si si-arrow-up"></i></button></li>
            <li><button type="button" data-toggle="block-option" class="content-row-trash"><i class="si si-trash"></i></button></li>
            <li><button type="button" data-toggle="block-option" class="content-row-trigger-sort"><i class="si si-cursor-move"></i></button></li>
        </ul>
        <h3 class="block-title"><?= $widget->getName() ?> <span class="block-number-figure"><?= $key + 1 ?></span></h3>
    </div>

    <div class="block-content content-form <?= $class ?>" style="display: <?= $display ?>">
        <?php foreach ($fields as $attr => $config) : ?>
            <?php if (in_array($attr, $localized)) {
                foreach ($locales as $locale) {
                    $tmpAttr = $attr;
                    if ($locale == $defaultLocale) {
                        echo $form->renderField($widget, "[{$key}]{$attr}", $config);
                    } else {
                        $tmpAttr = $attr . "_{$locale}";
                        echo $form->renderField($widget, "[{$key}]{$tmpAttr}", $config);
                    }
                }
            } else {
                echo $form->renderField($widget, "[{$key}]{$attr}", $config);
            } ?>
        <?php endforeach; ?>
    </div>

    <?php $form::end(); ?>
</div>
