<?php
namespace backend\modules\builder\widgets\builderWidget;

use backend\components\FormBuilder;
use backend\modules\builder\models\BuilderConfig;
use backend\modules\builder\Module;
use common\components\model\ActiveRecord;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;

/**
 * Class BuilderFieldWidget
 *
 * @package backend\modules\builder\widgets\builderWidget
 */
class BuilderFieldWidget extends Widget
{
    /** @var ActiveRecord */
    public $model;

    public $attribute;

    /** @var BuilderConfig */
    public $element;

    /** @var FormBuilder */
    public $form;

    public function run()
    {
        $model = $this->model;

        $sign = (isset($model->sign)) ? $model->sign : null;

        if (is_null($sign)) {
            throw new ServerErrorHttpException('Add to model: ' . $model::className() . ' random attribute "sign"');
        }

        /** @var Module $module */
        $module = \Yii::$app->getModule('builder');

        $models = [];

        $tmpModels = $module->models;

        if ($this->element->models) {
            $tmpModels = $this->element->getModels();
        }

        foreach ($tmpModels as $model) {
            $models[$model::className()] = $model->getName();
        }

        return $this->render('default', [
            'model' => $this->model,
            'attribute' => $this->attribute,
            'element' => $this->element,
            'form' => $this->form,
            'models' => $models,
            'module' => $module,
        ]);
    }
}
