<?php
/**
 * @var \yii\web\View $this
 * @var \common\components\model\ActiveRecord $model
 * @var string $attribute
 * @var string $element
 * @var \backend\components\FormBuilder $form
 * @var [] $models
 * @var \backend\modules\builder\Module $module
 */

use backend\modules\builder\widgets\builderWidget\BuilderFieldAssets;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$selectorClass = ($module->enablePreview) ? 'col-lg-6' : 'col-lg-9';

BuilderFieldAssets::register($this);
?>

<div class="builder-widgets">
    <div class="adder">
        <div class="input-group">
            <?= Html::dropDownList('widget', null, $models, ['class' => 'form-control input-lg add-content-builder-list']) ?>
            <span class="input-group-btn">
                <?= Html::button('<i class="si si-plus"></i> ' . bt('Add'), [
                    'type' => 'button',
                    'class' => 'btn btn-default btn-lg add-content-builder',
                    'data' => [
                        'href' => Url::toRoute('/builder/model/get'),
                        'key' => $model->$attribute === null ? 0 : count($model->$attribute),
                        'params' => [request()->csrfParam => request()->csrfToken, 'form' => $form::className(), 'model' => $model::className(), 'id' => $model->getPrimaryKey(), 'attribute' => $attribute],
                    ],
                ]) ?>
            </span>
        </div>
    </div>

    <div class="row place-row">
        <?php
        $items = [];

        if ($model->$attribute) {
            $model->$attribute = ArrayHelper::map($model->$attribute, 'position', function ($data) {
                return $data;
            });

            ArrayHelper::multisort($model->$attribute, 'position');

            foreach ($model->$attribute as $key => $widget) {
                $items[] = $this->render('@backend/modules/builder/views/model/get', [
                    'model' => $model,
                    'id' => $model->getPrimaryKey(),
                    'attribute' => $attribute,
                    'widget' => $widget,
                    'key' => $key,
                ]);
            }
        }

        echo implode("\n", $items);
        ?>
    </div>
</div>
