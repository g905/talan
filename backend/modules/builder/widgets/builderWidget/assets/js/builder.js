$(document).ready(function () {
    $(document).on('click', '.add-content-builder', function (e) {
        e.preventDefault();

        var link = $(this);
        var key = link.data('key');
        var widget = $('.add-content-builder-list').find('option:selected').val();

        $.ajax({
            url: link.data('href'),
            dataType: 'html',
            type: 'POST',
            data: $.extend(link.data('params'), {widget: widget}, {key: key}),
            success: function (html) {
                $('.place-row').append(html);

                link.data('key', key + 1);
            },
            error: function (a, text) {
                alert(text);
            }
        });
    });

    $(document).on('click', '.preview-content-builder', function (e) {
        e.preventDefault();

        var link = $(this);

        var arr = $('#main-form').serialize();

        $.ajax({
            url: link.data('href'),
            dataType: 'json',
            type: 'POST',
            data: $.extend(link.data('params'), {data: arr}),
            success: function (json) {
                var w = window.open(json.link);
            },
            error: function (a, text) {
                alert(text);
            }
        });
    });

    $(document).on('click', '.add-history', function (e) {
        e.preventDefault();

        var link = $(this);

        var arr = $('#main-form').serialize();

        $.ajax({
            url: link.data('href'),
            dataType: 'json',
            type: 'POST',
            data: $.extend(link.data('params'), {data: arr}),
            success: function (json) {
            },
            error: function (a, text) {
                alert(text);
            }
        });
    });

    $(document).on('click', '.content-row-trash', function (e) {
        e.preventDefault();

        var link = $(this);
        var row = link.parents('.builder-row');
        row.remove();
    });

    $(document).on('click', '.content-row-visible', function (e) {
        e.preventDefault();

        var link = $(this);
        var form = link.parents('.builder-row').find('.content-form');
        var visible = form.find('input.widget_visible');

        form.toggle();
        form.toggleClass('hidden');

        if (form.hasClass('hidden')) {
            visible.val(0);
        } else {
            visible.val(1);
        }
    });

    $(".builder-widgets .place-row").sortable({
        items: ".builder-row",
        handle: ".content-row-trigger-sort",
        cancel: '',
        update: function (event, ui) {
            var _sorter = $(this);
            _sorter.find('.builder-row').each(function (k, v) {
                var _row = $(v);
                var $inp = $(_row).find(':input');
                $inp.each(function (j, input) {
                    input.name = input.name.replace(/(\[\d+\])/, '[' + k + ']');
                });
                var $inp = $(_row).find('textarea');
                $inp.each(function (j, input) {
                    input.name = input.name.replace(/(\[\d+\])/, '[' + k + ']');
                });
                var $inp = $(_row).find('select');
                $inp.each(function (j, input) {
                    input.name = input.name.replace(/(\[\d+\])/, '[' + k + ']');
                });

                $(_row).find('.block-number-figure').text(k + 1);
            });
        }
    });

    // for sticky builder menu
    $(document).ready(function () {
        var stickyNavTop = 150;

        var stickyNav = function () {
            var scrollTop = $(window).scrollTop();

            if (scrollTop > stickyNavTop) {
                $('.adder').addClass('sticky');
            } else {
                $('.adder').removeClass('sticky');
            }
        };

        stickyNav();

        $(window).scroll(function () {
            stickyNav();
        });
    });
});
