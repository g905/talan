<?php

namespace backend\modules\page\models;

use backend\components\StylingActionColumn;
use backend\modules\contacts\models\City;
use backend\modules\contacts\models\Manager;
use kartik\grid\GridView;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%custom_page}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $label
 * @property string $alias
 * @property string $content
 * @property string $form_emails
 * @property integer $manager_id
 * @property integer $view_count
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $form_onsubmit
 *
 * @property City $city
 * @property Manager $manager
 */
class CustomPage extends ActiveRecord implements BackendModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%custom_page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'manager_id', 'view_count', 'published', 'position'], 'integer'],
            [['label', 'alias'], 'required'],
            [['content', 'form_emails'], 'string'],
            [['label', 'alias', 'form_onsubmit'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'targetClass' => \common\models\City::className(), 'targetAttribute' => 'id'],
            [['manager_id'], 'exist', 'targetClass' => \common\models\Manager::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/custom-page', 'ID'),
            'city_id' => Yii::t('back/custom-page', 'City'),
            'label' => Yii::t('back/custom-page', 'Label'),
            'alias' => Yii::t('back/custom-page', 'Alias'),
            'content' => Yii::t('back/custom-page', 'Content'),
            'manager_id' => Yii::t('back/custom-page', 'Manager'),
            'view_count' => Yii::t('back/custom-page', 'View count'),
            'published' => Yii::t('back/custom-page', 'Published'),
            'position' => Yii::t('back/custom-page', 'Position'),
            'created_at' => Yii::t('back/custom-page', 'Created At'),
            'updated_at' => Yii::t('back/custom-page', 'Updated At'),
            'form_emails' => Yii::t('back/custom-page', 'Form emails'),
            'form_onsubmit' => Yii::t('back/custom-page', 'Form onsubmit'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['id' => 'manager_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/custom-page', 'Custom Page');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var CustomPage $model */
                            if (isset($model->city)){
                                return '<b>'.$model->city->label.'</b>';
                            }
                            return \Yii::t('back/custom-page', 'for all cities');
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    'label',
                    'alias',
                    // 'content:ntext',
                    [
                        'attribute' => 'manager_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var CustomPage $model */
                            if (isset($model->manager)){
                                return '<b>'.$model->manager->fio.'</b>';
                            }
                            return '-';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(Manager::find()->orderBy('fio')->asArray()->all(), 'id', 'fio'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/apartment-complex', 'Any manager')],
                    ],
                   // 'view_count',
                    'published:boolean',
                    'position',
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                return StylingActionColumn::createPreviewUrl($url, $model, 'page');
                            }
                        ]
                    ]
                ];
            break;
            case 'view':
                return [
                    'id',
                    'city_id',
                    'label',
                    'alias',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'manager_id',
                    'view_count',
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new CustomPageSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'city_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\City::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'label' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_name form-control'
                ],
            ],
            'alias' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_alias form-control'
                ],
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'content',
                ]
            ],
            'manager_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\Manager::getItems('id', 'fio'),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'form_emails' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA,
            ],
            'form_onsubmit' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => ['maxlength' => true],
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
            ],
        ];

        return $config;
    }


}
