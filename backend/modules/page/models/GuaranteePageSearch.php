<?php

namespace backend\modules\page\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * GuaranteePageSearch represents the model behind the search form about `GuaranteePage`.
 */
class GuaranteePageSearch extends GuaranteePage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'manager_id', 'city_id'], 'integer'],
            [['label', 'published', 'alias', 'form_title', 'form_description', 'form_onsubmit', 'emails'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GuaranteePageSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'manager_id' => $this->manager_id,
            'city_id' => $this->city_id,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'published', $this->published])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'form_title', $this->form_title])
            ->andFilterWhere(['like', 'form_description', $this->form_description])
            ->andFilterWhere(['like', 'form_onsubmit', $this->form_onsubmit]);

        return $dataProvider;
    }
}
