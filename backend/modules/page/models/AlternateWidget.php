<?php

namespace backend\modules\page\models;

use vadymsemenykv\imageRequireValidator\ImageRequireValidator;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;

class AlternateWidget extends ActiveRecord implements BackendModel
{

    /**
    * Attribute for imageUploader
    */
    public $image;

    /**
    * Temporary sign which used for saving images before model save
    * @var string
    */
    public $sign;

    public function init()
    {
        $this->published = 1;
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%alternate_widget}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'link'], 'required'],
            [['home_page_id', 'published', 'position'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['home_page_id'], 'exist', 'targetClass' => \common\models\Home::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
            ['image',
                ImageRequireValidator::className(),
                'errorMessage' => Yii::t('back/home', 'Files are required for uploading'),
                'imageRelation' => 'imageRelation',
                'skipOnEmpty' => false
            ],
         ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/home-how-we-build', 'ID'),
            'label' => Yii::t('back/home-how-we-build', 'Title'),
            'link' => Yii::t('back/home-how-we-build', 'Link'),
            'home_page_id' => Yii::t('back/home-how-we-build', 'Home page'),
            'published' => Yii::t('back/home-how-we-build', 'Published'),
            'position' => Yii::t('back/home-how-we-build', 'Position'),
            'created_at' => Yii::t('back/home-how-we-build', 'Created At'),
            'updated_at' => Yii::t('back/home-how-we-build', 'Updated At'),
            'image' => Yii::t('back/home-how-we-build', 'image'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHomePage()
    {
        return $this->hasOne(Home::className(), ['id' => 'home_page_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/home-how-we-build', 'Alternate Widget');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    // 'content:ntext',
                    'home_page_id',
                    'published:boolean',
                    'position',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'home_page_id',
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new AlternateWidgetSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'link' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'image' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                    'attribute' => 'image',
                    'saveAttribute' => \common\models\AlternateWidget::SAVE_ATTRIBUTE_IMAGE,
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }


    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    public function getImageRelation()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition([
                'image.entity_model_name' => static::formName(),
                'image.attribute' => \common\models\AlternateWidget::SAVE_ATTRIBUTE_IMAGE
            ])
            ->alias('image')
            ->orderBy('image.position DESC');
    }
}
