<?php

namespace backend\modules\page\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%about_company_menu}}".
 *
 * @property integer $id
 * @property integer $about_company_id
 * @property string $label
 * @property string $alias
 * @property string $link
 * @property integer $show_right
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AboutCompany $aboutCompany
 */
class AboutCompanyMenu extends ActiveRecord implements BackendModel
{

    public function init()
    {
        $this->published = 1;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%about_company_menu}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['about_company_id', 'show_right', 'published', 'position'], 'integer'],
            [['label'], 'required'],
            [['label', 'alias', 'link'], 'string', 'max' => 255],
            [['about_company_id'], 'exist', 'targetClass' => \common\models\AboutCompany::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/about-company-menu', 'ID'),
            'about_company_id' => Yii::t('back/about-company-menu', 'About company'),
            'label' => Yii::t('back/about-company-menu', 'Label'),
            'alias' => Yii::t('back/about-company-menu', 'Alias'),
            'link' => Yii::t('back/about-company-menu', 'Link'),
            'show_right' => Yii::t('back/about-company-menu', 'Show right'),
            'published' => Yii::t('back/about-company-menu', 'Published'),
            'position' => Yii::t('back/about-company-menu', 'Position'),
            'created_at' => Yii::t('back/about-company-menu', 'Created At'),
            'updated_at' => Yii::t('back/about-company-menu', 'Updated At'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompany()
    {
        return $this->hasOne(AboutCompany::className(), ['id' => 'about_company_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('about-company-menu', 'About Company Menu');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'about_company_id',
                    'label',
                    //'alias',
                    'link',
                    'show_right:boolean',
                    'published:boolean',
                    'position',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'about_company_id',
                    'label',
                    //'alias',
                    'link',
                    'show_right:boolean',
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new AboutCompanyMenuSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    'class' => 'form-control'
                ],
            ],
//            'alias' => [
//                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
//                'options' => [
//                    'maxlength' => true,
//                    'class' => 'form-control'
//                ],
//            ],
            'link' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'show_right' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }


}
