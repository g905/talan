<?php

namespace backend\modules\page\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * HowBuyPageSearch represents the model behind the search form about `HowBuyPage`.
 */
class HowBuyPageSearch extends HowBuyPage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'manager_id', 'published', 'position'], 'integer'],
            [['label', 'form_bottom_title', 'form_bottom_description', 'form_bottom_email', 'form_bottom_onsubmit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HowBuyPageSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'manager_id' => $this->manager_id,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'form_bottom_title', $this->form_bottom_title])
            ->andFilterWhere(['like', 'form_bottom_description', $this->form_bottom_description])
            ->andFilterWhere(['like', 'form_bottom_email', $this->form_bottom_email])
            ->andFilterWhere(['like', 'form_bottom_onsubmit', $this->form_bottom_onsubmit]);

        return $dataProvider;
    }
}
