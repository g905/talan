<?php

namespace backend\modules\page\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CatalogSearch represents the model behind the search form about `Catalog`.
 */
class CatalogSearch extends Catalog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'position', 'manager_id'], 'integer'],
            [['residental_label', 'published', 'commerical_label', 'completed_label', 'form_title', 'form_description', 'form_onsubmit', 'resale_label'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatalogSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'position' => $this->position,
            'manager_id' => $this->manager_id,
        ]);

        $query->andFilterWhere(['like', 'residental_label', $this->residental_label])
            ->andFilterWhere(['like', 'published', $this->published])
            ->andFilterWhere(['like', 'commerical_label', $this->commerical_label])
            ->andFilterWhere(['like', 'completed_label', $this->completed_label])
            ->andFilterWhere(['like', 'form_title', $this->form_title])
            ->andFilterWhere(['like', 'form_description', $this->form_description])
            ->andFilterWhere(['like', 'form_onsubmit', $this->form_onsubmit])
            ->andFilterWhere(['like', 'resale_label', $this->resale_label]);

        return $dataProvider;
    }
}
