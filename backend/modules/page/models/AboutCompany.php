<?php

namespace backend\modules\page\models;

use backend\components\StylingActionColumn;
use backend\modules\contacts\models\City;
use common\models\AboutCompany as CommonAboutCompany;
use common\models\EntityToFile;
use kartik\grid\GridView;
use vadymsemenykv\imageRequireValidator\ImageRequireValidator;
use Yii;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%about_company}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $top_screen_title
 * @property string $top_screen_description
 * @property string $mission_label
 * @property string $company_values_title
 * @property string $ceo_name
 * @property string $ceo_position
 * @property string $ceo_quotation
 * @property string $form_title
 * @property string $form_description
 * @property string $form_emails
 * @property string $form_onsubmit
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 */
class AboutCompany extends ActiveRecord implements BackendModel
{

    /**
    * Attribute for imageUploader
    */
    public $top_screen_background_image;

    /**
    * Attribute for imageUploader
    */
    public $top_screen_background_video;

    /**
    * Attribute for imageUploader
    */
    public $map_image;

    /**
    * Attribute for imageUploader
    */
    public $ceo_photo;

    /**
    * Temporary sign which used for saving images before model save
    * @var string
    */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%about_company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id','top_screen_title', 'ceo_name', 'ceo_position', 'ceo_quotation','form_title', 'form_description'], 'required'],
            [['city_id', 'published'], 'integer'],
            [['city_id'], 'unique'],
            [['top_screen_description', 'mission_label', 'ceo_quotation', 'form_description', 'form_emails', 'form_onsubmit'], 'string'],
            [['top_screen_title', 'company_values_title', 'ceo_name', 'ceo_position', 'form_title'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'targetClass' => \common\models\City::class, 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['sign'], 'string', 'max' => 255],
            ['top_screen_background_image',
                ImageRequireValidator::className(),
                'errorMessage' => Yii::t('back/home', 'Files are required for uploading'),
                'imageRelation' => 'topScreenBackgroundImageRelation',
                'skipOnEmpty' => false
            ],
            ['ceo_photo',
                ImageRequireValidator::className(),
                'errorMessage' => Yii::t('back/home', 'Files are required for uploading'),
                'imageRelation' => 'ceoPhotoRelation',
                'skipOnEmpty' => false
            ],
         ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopScreenBackgroundImageRelation()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['imageAttr.entity_model_name' => static::formName(), 'imageAttr.attribute' => CommonAboutCompany::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE])
            ->alias('imageAttr')
            ->orderBy('imageAttr.position DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCeoPhotoRelation()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['imageAttr.entity_model_name' => static::formName(), 'imageAttr.attribute' => CommonAboutCompany::SAVE_ATTRIBUTE_CEO_PHOTO,])
            ->alias('imageAttr')
            ->orderBy('imageAttr.position DESC');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/about-company', 'ID'),
            'city_id' => Yii::t('back/about-company', 'City'),
            'top_screen_title' => Yii::t('back/about-company', 'Top screen title'),
            'top_screen_description' => Yii::t('back/about-company', 'Top screen description'),
            'mission_label' => Yii::t('back/about-company', 'Mission label'),
            'company_values_title' => Yii::t('back/about-company', 'Company values title'),
            'ceo_name' => Yii::t('back/about-company', 'Ceo name'),
            'ceo_position' => Yii::t('back/about-company', 'Ceo position'),
            'ceo_quotation' => Yii::t('back/about-company', 'Ceo quotation'),
            'form_title' => Yii::t('back/about-company', 'Form_title'),
            'form_description' => Yii::t('back/about-company', 'Form_description'),
            'form_emails' => Yii::t('back/about-company', 'Form_emails'),
            'form_onsubmit' => Yii::t('back/about-company', 'Form onsubmit'),
            'published' => Yii::t('back/about-company', 'Published'),
            'created_at' => Yii::t('back/about-company', 'Created At'),
            'updated_at' => Yii::t('back/about-company', 'Updated At'),
            'top_screen_background_image' => Yii::t('back/about-company', 'Top screen background image'),
            'top_screen_background_video' => Yii::t('back/about-company', 'Top screen background video'),
            'map_image' => Yii::t('back/about-company', 'Map image'),
            'ceo_photo' => Yii::t('back/about-company', 'CEO photo'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompanyValues()
    {
        return $this->hasMany(AboutCompanyValues::className(), ['about_company_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompanyMenu()
    {
        return $this->hasMany(AboutCompanyMenu::className(), ['about_company_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompanyStat()
    {
        return $this->hasMany(AboutCompanyStat::className(), ['about_company_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }


    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/about-company', 'About Company');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var Home $model */
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    'top_screen_title',
                    'company_values_title',
                    'ceo_name',
                    'published:boolean',
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                return StylingActionColumn::createPreviewUrl($url, $model, 'about');
                            }
                        ]
                    ]
                ];
            break;
            case 'view':
                return [
                    'id',
                    'city_id',
                    'top_screen_title',
                    [
                        'attribute' => 'top_screen_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'mission_label',
                        'format' => 'html',
                    ],
                    'company_values_title',
                    'ceo_name',
                    'ceo_position',
                    [
                        'attribute' => 'ceo_quotation',
                        'format' => 'html',
                    ],
                    'form_title',
                    [
                        'attribute' => 'form_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'form_emails',
                        'format' => 'html',
                    ],
                    'published:boolean',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new AboutCompanySearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                Yii::t('back/about-company', 'General') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\City::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                ],
                Yii::t('back/about-company', 'Top screen') => [
                    'top_screen_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'top_screen_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'top_screen_description',
                        ]
                    ],
                    'top_screen_background_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'top_screen_background_image',
                            'saveAttribute' => CommonAboutCompany::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'top_screen_background_video' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['mp4'],
                            'attribute' => 'top_screen_background_video',
                            'saveAttribute' => CommonAboutCompany::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                Yii::t('back/about-company', 'Menu') => [
                    $this->getRelatedFormConfig()['about_company_menu']
                ],
                Yii::t('back/about-company', 'Statistic') => [
                    $this->getRelatedFormConfig()['about_company_stat']
                ],
                Yii::t('back/about-company', 'Mission') => [
                    'mission_label' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'mission_label',
                        ]
                    ],
                ],
                Yii::t('back/about-company', 'Map') => [
                    'map_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'map_image',
                            'saveAttribute' => CommonAboutCompany::SAVE_ATTRIBUTE_MAP_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                Yii::t('back/about-company', 'Company values') => [
                    'company_values_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> <i class="si si-grid"></i> '.Yii::t('back/about-company', 'about company values - blocks').'</div>',
                        'label' => false,
                    ],
                    $this->getRelatedFormConfig()['about_company_values']
                ],
                Yii::t('back/about-company', 'CEO') => [
                    'ceo_name' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'ceo_position' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'ceo_quotation' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'ceo_quotation',
                        ]
                    ],
                    'ceo_photo' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'ceo_photo',
                            'saveAttribute' => CommonAboutCompany::SAVE_ATTRIBUTE_CEO_PHOTO,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                Yii::t('back/about-company', 'Form') => [
                    'form_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_onsubmit' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'form_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'form_description',
                        ]
                    ],
                    'form_emails' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                ],
            ],

        ];

        return $config;
    }


    public function getRelatedFormConfig()
    {
        return [
            'about_company_values' => [
                'relation' => 'aboutCompanyValues',
            ],
            'about_company_menu' => [
                'relation' => 'aboutCompanyMenu',
            ],
            'about_company_stat' => [
                'relation' => 'aboutCompanyStat',
            ],
        ];
    }

    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
