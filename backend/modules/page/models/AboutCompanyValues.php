<?php

namespace backend\modules\page\models;

use common\models\AboutCompanyValues as CommonAboutCompanyValues;
use vadymsemenykv\imageRequireValidator\ImageRequireValidator;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%about_company_values}}".
 *
 * @property integer $id
 * @property integer $about_company_id
 * @property string $label
 * @property string $content
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AboutCompany $aboutCompany
 */
class AboutCompanyValues extends ActiveRecord implements BackendModel
{

    /**
    * Attribute for imageUploader
    */
    public $image;

    /**
    * Temporary sign which used for saving images before model save
    * @var string
    */
    public $sign;

    public function init()
    {
        $this->published = 1;
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%about_company_values}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['about_company_id', 'published', 'position'], 'integer'],
            [['label'], 'required'],
            [['content'], 'string'],
            [['label'], 'string', 'max' => 255],
            [['about_company_id'], 'exist', 'targetClass' => \common\models\AboutCompany::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
            ['image',
                ImageRequireValidator::className(),
                'errorMessage' => Yii::t('back/home', 'Files are required for uploading'),
                'imageRelation' => 'imageRelation',
                'skipOnEmpty' => false
            ],
         ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImageRelation()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['imageAttr.entity_model_name' => static::formName(), 'imageAttr.attribute' => CommonAboutCompanyValues::SAVE_ATTRIBUTE_IMAGE])
            ->alias('imageAttr')
            ->orderBy('imageAttr.position DESC');
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/about-company-values', 'ID'),
            'about_company_id' => Yii::t('back/about-company-values', 'About company'),
            'label' => Yii::t('back/about-company-values', 'Label'),
            'content' => Yii::t('back/about-company-values', 'Content'),
            'published' => Yii::t('back/about-company-values', 'Published'),
            'position' => Yii::t('back/about-company-values', 'Position'),
            'created_at' => Yii::t('back/about-company-values', 'Created At'),
            'updated_at' => Yii::t('back/about-company-values', 'Updated At'),
            'image' => Yii::t('back/about-company-values', 'Image'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompany()
    {
        return $this->hasOne(AboutCompany::className(), ['id' => 'about_company_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/about-company-values', 'About Company Values');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'about_company_id',
                    'label',
                    // 'content:ntext',
                    'published:boolean',
                    'position',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'about_company_id',
                    'label',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new AboutCompanyValuesSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'content',
                ]
            ],
            'image' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                    'attribute' => 'image',
                    'saveAttribute' => CommonAboutCompanyValues::SAVE_ATTRIBUTE_IMAGE,
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }


    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
