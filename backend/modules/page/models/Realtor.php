<?php

namespace backend\modules\page\models;

use backend\components\StylingActionColumn;
use common\models\EntityToFile;
use yii\behaviors\TimestampBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\models\Realtor as CommonRealtor;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\modules\contacts\models\Manager;
use backend\modules\menu\models\RealtorSubMenu;
use backend\modules\block\models\block\RealtorFile;
use backend\modules\block\models\block\RealtorFact;
use backend\modules\block\models\block\RealtorBenefit;
use backend\modules\block\models\block\RealtorPartner;

/**
 * This is the model class for table "{{%realtor}}".
 *
 * @property City $city
 * @property Manager $manager
 * @property RealtorSubMenu[] $subMenus
 * @property RealtorFile[] $fileBlocks
 * @property RealtorFact[] $factBlocks
 * @property RealtorBenefit[] $benefitBlocks
 * @property RealtorPartner[] $partnerBlocks
 */
class Realtor extends CommonRealtor implements BackendModel
{
    public $sign;
    public $topImage;
    public $topLinkFile;
    public $appLinkFile;
    public $appImageFirst;
    public $appImageSecond;

    public function init()
    {
        parent::init();
        $this->sign = $this->sign ?: security()->generateRandomString();
    }

    public function getTitle()
    {
        return bt('Realtor', 'realtor');
    }

    public static function tableName()
    {
        return '{{%realtor}}';
    }

    public function rules()
    {
        return [
            [['city_id', 'manager_id', 'view_count', 'published', 'position'], 'integer'],
            [['city_id', 'top_label'], 'required'],
            [
                [
                    'top_description',
                    'benefits_description',
                    'about_description',
                    'app_description',
                    'form_description',
                    'form_agreement'
                ],
                'string'
            ],
            [
                [
                    'top_label',
                    'top_link_label',
                    'top_btn_label',
                    'benefits_label',
                    'about_label',
                    'partners_label',
                    'app_label',
                    'app_link_label',
                    'app_btn_ios_label',
                    'app_btn_ios_link',
                    'app_btn_android_label',
                    'app_btn_android_link',
                    'form_label',
                    'form_onsubmit',
                    'files_label',
                    'sign',
                ],
                'string',
                'max' => 255
            ],
            [['city_id'], 'exist', 'targetClass' => City::class, 'targetAttribute' => 'id'],
            [['manager_id'], 'exist', 'targetClass' => Manager::class, 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['topLinkFile', 'appLinkFile', 'topImage', 'appImageFirst', 'appImageSecond'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'realtor'),
            'city_id' => bt('City ID', 'realtor'),
            'top_label' => bt('Top Label', 'realtor'),
            'top_description' => bt('Top Description', 'realtor'),
            'top_link_label' => bt('Top Link Label', 'realtor'),
            'top_btn_label' => bt('Top Btn Label', 'realtor'),
            'benefits_label' => bt('Benefits Label', 'realtor'),
            'benefits_description' => bt('Benefits Description', 'realtor'),
            'about_label' => bt('About Label', 'realtor'),
            'about_description' => bt('About Description', 'realtor'),
            'partners_label' => bt('Partners Label', 'realtor'),
            'app_label' => bt('App Label', 'realtor'),
            'app_description' => bt('App Description', 'realtor'),
            'app_link_label' => bt('App Link Label', 'realtor'),
            'app_btn_ios_label' => bt('App Btn Ios Label', 'realtor'),
            'app_btn_ios_link' => bt('App btn ios link', 'realtor'),
            'app_btn_android_link' => bt('App btn android link', 'realtor'),
            'app_btn_android_label' => bt('App Btn Android Label', 'realtor'),
            'form_label' => bt('Form Label', 'realtor'),
            'form_onsubmit' => bt('Form Onsubmit', 'realtor'),
            'form_description' => bt('Form Description', 'realtor'),
            'form_agreement' => bt('Form Agreement', 'realtor'),
            'manager_id' => bt('Manager ID', 'realtor'),
            'files_label' => bt('Files Label', 'realtor'),
            'view_count' => bt('View Count', 'realtor'),
            'published' => bt('Published', 'realtor'),
            'position' => bt('Position', 'realtor'),
            'topImage' => bt('Top image', 'realtor'),
            'topLinkFile' => bt('Top link file', 'realtor'),
            'appLinkFile' => bt('App link file', 'realtor'),
            'appImageFirst' => bt('App image first', 'realtor'),
            'appImageSecond' => bt('App image second', 'realtor'),
        ];
    }

    public function attributeHints()
    {
        return [
            'topLinkFile' => 'Поддерживаются форматы: pdf, doc, docx, ppt, pptx',
            'appLinkFile' => 'Поддерживаются форматы: pdf, doc, docx, ppt, pptx',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'seo' => MetaTagBehavior::class,
        ];
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    StdColumn::city(),
                    'top_label',
                    // 'top_description:ntext',
                    // 'top_link_label:url',
                    // 'top_btn_label',
                    // 'benefits_label',
                    // 'benefits_description:ntext',
                    // 'about_label',
                    // 'about_description:ntext',
                    // 'partners_label',
                    // 'app_label',
                    // 'app_description:ntext',
                    // 'app_link_label:url',
                    // 'app_btn_ios_label',
                    // 'app_btn_android_label',
                    // 'form_label',
                    // 'form_onsubmit',
                    // 'form_description:ntext',
                    // 'form_agreement:ntext',
                    StdColumn::manager(),
                    // 'files_label',
                    StdColumn::viewCount(),
                    StdColumn::published(),
                    StdColumn::position(),
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                return StylingActionColumn::createPreviewUrl($url, $model, 'realtors');
                            }
                        ]
                    ]
                ];
            case 'view':
                return [
                    'id',
                    StdColumn::city(),
                    'top_label',
                    'top_description:html',
                    'top_link_label:url',
                    'top_btn_label',
                    'benefits_label',
                    'benefits_description:html',
                    'about_label',
                    'about_description:html',
                    'partners_label',
                    'app_label',
                    'app_description:html',
                    'app_link_label:url',
                    'app_btn_ios_label',
                    'app_btn_android_label',
                    'form_label',
                    'form_onsubmit',
                    'form_description:html',
                    'form_agreement:html',
                    StdColumn::manager(),
                    // 'files_label',
                    'view_count',
                    'published:boolean',
                    'position',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new RealtorSearch();
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General') => [
                    'city_id' => StdInput::citySelect(),
                    'topImage' => StdInput::imageUpload(self::TOP_IMAGE),
                    'top_label' => StdInput::text(),
                    'top_description' => StdInput::editor(),
                    'top_link_label' => StdInput::text(),
                    'topLinkFile' => StdInput::fileUpload(self::TOP_LINK_FILE, false, ['pdf', 'doc', 'docx', 'ppt', 'pptx']),
                    'top_btn_label' => StdInput::text(),
                    'view_count' => StdInput::viewCount(),
                    'published' => StdInput::checkbox(),
                    'sign' => StdInput::hidden(),
                    // 'position' => StdInput::text(),
                ],
                bt('Benefits') => [
                    'benefits_label' => StdInput::text(),
                    'benefits_description' => StdInput::textArea(),
                    'separator' => StdInput::separator('Преимущества'),
                    obtain('benefits', $this->getRelatedFormConfig()),
                ],
                bt('About') => [
                    'about_label' => StdInput::text(),
                    'about_description' => StdInput::textArea(),
                    'separator' => StdInput::separator('Факты'),
                    obtain('facts', $this->getRelatedFormConfig()),
                ],
                bt('Partners') => [
                    'partners_label' => StdInput::text(),
                    'separator' => StdInput::separator('Партнеры'),
                    obtain('partners', $this->getRelatedFormConfig()),
                ],
                bt('App') => [
                    'app_label' => StdInput::text(),
                    'app_description' => StdInput::editor(),
                    'appImageFirst' => StdInput::imageUpload(self::APP_FIRST_IMAGE),
                    'appImageSecond' => StdInput::imageUpload(self::APP_SECOND_IMAGE),
                    'app_link_label' => StdInput::text(),
                    'appLinkFile' => StdInput::fileUpload(self::APP_LINK_FILE, false, ['pdf', 'doc', 'docx', 'ppt', 'pptx']),
                    'app_btn_ios_label' => StdInput::text(),
                    'app_btn_ios_link' => StdInput::text(),
                    'app_btn_android_label' => StdInput::text(),
                    'app_btn_android_link' => StdInput::text(),
                ],
                bt('Files') => [
                    'files_label' => StdInput::text(),
                    'separator' => StdInput::separator('Файлы'),
                    obtain('files', $this->getRelatedFormConfig()),
                ],
                bt('Form') => [
                    'form_label' => StdInput::text(),
                    'form_onsubmit' => StdInput::text(),
                    'form_description' => StdInput::textArea(),
                    'form_agreement' => StdInput::editor(),
                    'manager_id' => StdInput::managerSelect(),
                ],
                bt('Menu') => [
                    'separator' => StdInput::separator('Меню'),
                    $this->getRelatedFormConfig()['sub_menu'],
                ],
            ]
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getBenefitBlocks()
    {
        return $this->hasMany(RealtorBenefit::class, ['object_id' => 'id'])
            ->onCondition(['type' => RealtorBenefit::REALTOR_BENEFIT]);
    }

    public function getPartnerBlocks()
    {
        return $this->hasMany(RealtorPartner::class, ['object_id' => 'id'])
            ->onCondition(['type' => RealtorPartner::REALTOR_PARTNER]);
    }

    public function getFileBlocks()
    {
        return $this->hasMany(RealtorFile::class, ['object_id' => 'id'])
            ->onCondition(['type' => RealtorFile::REALTOR_FILE]);
    }

    public function getFactBlocks()
    {
        return $this->hasMany(RealtorFact::class, ['object_id' => 'id'])
            ->onCondition(['type' => RealtorFact::REALTOR_FACT]);
    }

    public function getSubMenus()
    {
        return $this->hasMany(RealtorSubMenu::class, ['object_id' => 'id'])
            ->onCondition(['type' => RealtorSubMenu::TYPE_REALTOR]);
    }

    public function getRelatedFormConfig()
    {
        return [
            'benefits' => ['relation' => 'benefitBlocks'],
            'partners' => ['relation' => 'partnerBlocks'],
            'facts' => ['relation' => 'factBlocks'],
            'files' => ['relation' => 'fileBlocks'],
            'sub_menu' => ['relation' => 'subMenus']
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
