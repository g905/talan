<?php

namespace backend\modules\page\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RealtorSearch represents the model behind the search form about `Realtor`.
 */
class RealtorSearch extends Realtor
{
    public function rules()
    {
        return [
            [['id', 'city_id', 'manager_id', 'view_count', 'published', 'position'], 'integer'],
            [['top_label', 'top_description', 'top_link_label', 'top_btn_label', 'benefits_label', 'benefits_description', 'about_label', 'about_description', 'partners_label', 'app_label', 'app_description', 'app_link_label', 'app_btn_ios_label', 'app_btn_android_label', 'form_label', 'form_onsubmit', 'form_description', 'form_agreement', 'files_label'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = RealtorSearch::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'manager_id' => $this->manager_id,
            'view_count' => $this->view_count,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'top_label', $this->top_label])
            ->andFilterWhere(['like', 'top_description', $this->top_description])
            ->andFilterWhere(['like', 'top_link_label', $this->top_link_label])
            ->andFilterWhere(['like', 'top_btn_label', $this->top_btn_label])
            ->andFilterWhere(['like', 'benefits_label', $this->benefits_label])
            ->andFilterWhere(['like', 'benefits_description', $this->benefits_description])
            ->andFilterWhere(['like', 'about_label', $this->about_label])
            ->andFilterWhere(['like', 'about_description', $this->about_description])
            ->andFilterWhere(['like', 'partners_label', $this->partners_label])
            ->andFilterWhere(['like', 'app_label', $this->app_label])
            ->andFilterWhere(['like', 'app_description', $this->app_description])
            ->andFilterWhere(['like', 'app_link_label', $this->app_link_label])
            ->andFilterWhere(['like', 'app_btn_ios_label', $this->app_btn_ios_label])
            ->andFilterWhere(['like', 'app_btn_android_label', $this->app_btn_android_label])
            ->andFilterWhere(['like', 'form_label', $this->form_label])
            ->andFilterWhere(['like', 'form_onsubmit', $this->form_onsubmit])
            ->andFilterWhere(['like', 'form_description', $this->form_description])
            ->andFilterWhere(['like', 'form_agreement', $this->form_agreement])
            ->andFilterWhere(['like', 'files_label', $this->files_label]);

        return $dataProvider;
    }
}
