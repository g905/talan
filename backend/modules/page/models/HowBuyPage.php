<?php

namespace backend\modules\page\models;

use backend\components\StylingActionColumn;
use backend\modules\block\models\block\BuyPageHow;
use backend\modules\menu\models\BuyHowSubMenu;
use common\models\City;
use common\models\EntityToFile;
use common\models\Manager;
use kartik\grid\GridView;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use Yii;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;
use common\models\HowBuyPage as CommonModel;
use yii\helpers\ArrayHelper;

/**
 * Class HowBuyPage
 * @package backend\modules\page\models
 * @author dimarychek
 */
class HowBuyPage extends CommonModel implements BackendModel
{
    /**
     * Temporary sign which used for saving images before model save
     * @var string
     */
    public $sign;

    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = security()->generateRandomString();
        }
    }

    public function rules()
    {
        return [
            [['city_id', 'label'], 'required'],
            [['city_id', 'manager_id', 'published', 'position'], 'integer'],
            [['form_bottom_description', 'form_bottom_email'], 'string'],
            [['label', 'form_bottom_title', 'form_bottom_onsubmit'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'targetClass' => City::className(), 'targetAttribute' => 'id'],
            [['sign'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'seo' => [
                'class' => MetaTagBehavior::className(),
            ],
        ]);
    }

    public function getCity0()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getTitle()
    {
        return bt('How Buy Page', 'app');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var Home $model */
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    'label',
                    //'form_bottom_title',
                    // 'form_bottom_description:ntext',
                    // 'form_bottom_email:ntext',
                    //'manager_id',
                    //'form_bottom_onsubmit',
                    'published:boolean',
                    'position',
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                return StylingActionColumn::createPreviewUrl($url, $model, 'how-to-buy');
                            }
                        ]
                    ]
                ];
            case 'view':
                return [
                    'id',
                    'city_id',
                    'label',
                    'form_bottom_title',
                    [
                        'attribute' => 'form_bottom_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'form_bottom_email',
                        'format' => 'html',
                    ],
                    'manager_id',
                    'form_bottom_onsubmit',
                    'published:boolean',
                    'position',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new HowBuyPageSearch();
    }

    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                Yii::t('back/buy', 'General') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => City::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'position' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
                Yii::t('back/earth', 'Menu') => [
                    $this->getRelatedFormConfig()['sub_menu'],
                ],
                Yii::t('back/buy', 'Blocks') => [
                    $this->getRelatedFormConfig()['how_blocks'],
                ],
                Yii::t('back/buy', 'Forms') => [
                    'form_bottom_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_bottom_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'form_bottom_description',
                        ]
                    ],
                    'form_bottom_email' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'manager_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => Manager::getItems('id', 'fio'),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'form_bottom_onsubmit' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                ],
            ]
        ];

        return $config;
    }

    public function getRelatedFormConfig()
    {
        return [
            'how_blocks' => [
                'relation' => 'howBlocks'
            ],
            'sub_menu' => [
                'relation' => 'subMenus'
            ],
        ];
    }

    public function getSubMenus()
    {
        return $this->hasMany(BuyHowSubMenu::class, ['object_id' => 'id'])
            ->orderBy(['position' => SORT_ASC])
            ->onCondition(['type' => BuyHowSubMenu::TYPE_BUY_HOW_PAGE]);
    }

    public function getHowBlocks()
    {
        return $this->hasMany(BuyPageHow::class, ['object_id' => 'id'])
            ->onCondition(['type' => BuyPageHow::BUY_PAGE_HOW]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
