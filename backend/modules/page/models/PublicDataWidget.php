<?php

namespace backend\modules\page\models;

use common\models\PublicDataWidget as CommonPublicDataWidget;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\modules\apartment\models\ApartmentComplex;

/**
 * This is the model class for table "{{%public_data_widget}}".
 *
 * @property City $city
 * @property ApartmentComplex $complex
 * @property PublicDataChartData[] $chartData
 */
class PublicDataWidget extends CommonPublicDataWidget implements BackendModel
{
    public static function tableName()
    {
        return '{{%public_data_widget}}';
    }

    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type', 'city_id', 'plan', 'fact', 'published', 'position'], 'integer'],
            [['label', 'chart_name', 'link', 'placed', 'seria'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
            [['value'], 'string'],
            [['value'], 'default', 'value' => null],
            [['link'], 'url', 'defaultScheme' => 'http'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'public-data-widget'),
            'type' => bt('Type', 'public-data-widget'),
            'city_id' => bt('City ID', 'public-data-widget'),
            'label' => bt('Label', 'public-data-widget'),
            'chart_name' => bt('Chart Name', 'public-data-widget'),
            'link' => bt('Link', 'public-data-widget'),
            'value' => bt('Value', 'public-data-widget'),
            'status' => bt('Status', 'public-data-widget'),
            'placed' => bt('Placed', 'public-data-widget'),
            'seria' => bt('Seria', 'public-data-widget'),
            'plan' => bt('Plan', 'public-data-widget'),
            'fact' => bt('Fact', 'public-data-widget'),
            'published' => bt('Published', 'public-data-widget'),
            'position' => bt('Position', 'public-data-widget'),
        ];
    }

    public function getTitle()
    {
        return bt('Public Data Widget', 'public-data-widget');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    'type',
                    'city_id',
                    'label',
                    'chart_name',
                    'link:url',
                    'value',
                    // 'status',
                    // 'placed',
                    // 'seria',
                    // 'plan',
                    // 'fact',
                    'published:boolean',
                    'position',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    'type',
                    'city_id',
                    'label',
                    'chart_name',
                    'link:url',
                    'value',
                    'status',
                    'placed',
                    'seria',
                    'plan',
                    'fact',
                    'published:boolean',
                    'position',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new PublicDataWidgetSearch();
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General') => [
                    'type' => StdInput::hidden(),
                    'city_id' => StdInput::citySelect(),
                    'complex_id' => StdInput::complexSelect(),
                    'label' => StdInput::text(),
                    'chart_name' => StdInput::text(),
                    'link' => StdInput::text(),
                    'value' => StdInput::text(),
                    'status' => StdInput::checkbox(),
                    'placed' => StdInput::text(),
                    'seria' => StdInput::text(),
                    'plan' => StdInput::text(),
                    'fact' => StdInput::text(),
                    'published' => StdInput::checkbox(),
                    'position' => StdInput::text(),
                ],
                bt('Chart') => [
                    'separator' => StdInput::separator(bt('Chart')),
                    $this->getRelatedFormConfig()['chartData'],
                ]
            ],
        ];
    }

    public function getRelatedFormConfig()
    {
        return [
            'chartData' => [
                'relation' => 'chartData',
                'limitMin' => 1,
            ],
        ];
    }

    public function getChartData()
    {
        return $this->hasMany(PublicDataChartData::class, ['widget_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getComplex()
    {
        return $this->hasOne(ApartmentComplex::class, ['id' => 'complex_id']);
    }
}
