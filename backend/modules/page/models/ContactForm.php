<?php

namespace backend\modules\page\models;

use yii\behaviors\TimestampBehavior;
use common\models\ContactForm as CommonContactForm;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\components\BackendModel;
use backend\modules\contacts\models\Manager;

/**
 * This is the model class for table "{{%contact_form}}".
 *
 * @property Manager $manager
 */
class ContactForm extends CommonContactForm implements BackendModel
{
    public static function tableName()
    {
        return '{{%contact_form}}';
    }

    public function rules()
    {
        return [
            [['contact_page_id', 'manager_id', 'position', 'published'], 'integer'],
            [['nav_label'], 'required'],
            [['form_description', 'form_agreement'], 'string'],
            [['nav_label', 'form_label'], 'string', 'max' => 255],
            [['manager_id'], 'exist', 'targetClass' => Manager::class, 'targetAttribute' => 'id'],
            [['contact_page_id'], 'exist', 'targetClass' => ContactPage::class, 'targetAttribute' => 'id'],
            [['position'], 'default', 'value' => 0],
            [['published'], 'default', 'value' => 1],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'contact-form'),
            'contact_page_id' => bt('Contact Page ID', 'contact-form'),
            'nav_label' => bt('Nav Label', 'contact-form'),
            'form_label' => bt('Form Label', 'contact-form'),
            'form_description' => bt('Form Description', 'contact-form'),
            'form_agreement' => bt('Form Agreement', 'contact-form'),
            'manager_id' => bt('Manager ID', 'contact-form'),
            'position' => bt('Position', 'contact-form'),
            'published' => bt('Published', 'contact-form'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getTitle()
    {
        return bt('Contact Form', 'contact-form');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    // 'contact_page_id',
                    'nav_label',
                    'form_label',
                    'form_description:html',
                    'form_agreement:html',
                    StdColumn::manager(),
                    StdColumn::position(),
                    StdColumn::published(),
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    // 'contact_page_id',
                    'nav_label',
                    'form_label',
                    'form_description:html',
                    'form_agreement:html',
                    'manager_id',
                    'position',
                    'published:boolean',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new ContactFormSearch();
    }

    public function getFormConfig()
    {
        return [
            // 'contact_page_id' => StdInput::text(),
            'nav_label' => StdInput::text(),
            'form_label' => StdInput::text(),
            'form_description' => StdInput::editor(),
            'form_agreement' => StdInput::editor(),
            'manager_id' => StdInput::managerSelect(),
            'position' => StdInput::text(),
            'published' => StdInput::checkbox(),
        ];
    }
}
