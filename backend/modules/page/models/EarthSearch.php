<?php

namespace backend\modules\page\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EarthSearch represents the model behind the search form about `Earth`.
 */
class EarthSearch extends Earth
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id'], 'integer'],
            [['top_screen_title', 'top_screen_description', 'presentation_link', 'company_title', 'company_description', 'map_title', 'map_subtitle', 'map_demands', 'plan_title', 'boss_title', 'boss_name', 'boss_position', 'boss_description', 'offer_title_light', 'offer_title_dark', 'offer_link', 'advantages_title', 'model_title', 'model_description', 'manager_title', 'published'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EarthSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
        ]);

        $query->andFilterWhere(['like', 'top_screen_title', $this->top_screen_title])
            ->andFilterWhere(['like', 'top_screen_description', $this->top_screen_description])
            ->andFilterWhere(['like', 'presentation_link', $this->presentation_link])
            ->andFilterWhere(['like', 'company_title', $this->company_title])
            ->andFilterWhere(['like', 'company_description', $this->company_description])
            ->andFilterWhere(['like', 'map_title', $this->map_title])
            ->andFilterWhere(['like', 'map_subtitle', $this->map_subtitle])
            ->andFilterWhere(['like', 'map_demands', $this->map_demands])
            ->andFilterWhere(['like', 'plan_title', $this->plan_title])
            ->andFilterWhere(['like', 'boss_title', $this->boss_title])
            ->andFilterWhere(['like', 'boss_name', $this->boss_name])
            ->andFilterWhere(['like', 'boss_position', $this->boss_position])
            ->andFilterWhere(['like', 'boss_description', $this->boss_description])
            ->andFilterWhere(['like', 'offer_title_light', $this->offer_title_light])
            ->andFilterWhere(['like', 'offer_title_dark', $this->offer_title_dark])
            ->andFilterWhere(['like', 'offer_link', $this->offer_link])
            ->andFilterWhere(['like', 'advantages_title', $this->advantages_title])
            ->andFilterWhere(['like', 'model_title', $this->model_title])
            ->andFilterWhere(['like', 'model_description', $this->model_description])
            ->andFilterWhere(['like', 'manager_title', $this->manager_title])
            ->andFilterWhere(['like', 'published', $this->published]);

        return $dataProvider;
    }
}
