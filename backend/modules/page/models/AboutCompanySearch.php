<?php

namespace backend\modules\page\models;

use backend\modules\contacts\models\City;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AboutCompanySearch represents the model behind the search form about `AboutCompany`.
 */
class AboutCompanySearch extends AboutCompany
{
    public static function getAdditionalHeaderConfig()
    {
        $cityCount = City::find()
            ->where([
                'published' => 1
            ])
            ->count();

        $modelCount = AboutCompany::find()
            ->where([
                'published' => 1
            ])
            ->count();

        if ($cityCount == 0){
            $filledCities = '-';
        }
        else{
            $filledCities = round($modelCount / $cityCount * 100).'%';
        }

        return [
            [
                'class' => 'regular',
                'leftValue' => $filledCities,
                'rightValue' => \Yii::t('back/home', 'Filled cities'),
                'link' => '',
            ],


        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'published'], 'integer'],
            [['top_screen_title', 'top_screen_description', 'mission_label', 'company_values_title', 'ceo_name', 'ceo_position', 'ceo_quotation', 'form_title', 'form_description', 'form_emails'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AboutCompanySearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'published' => $this->published,
        ]);

        $query->andFilterWhere(['like', 'top_screen_title', $this->top_screen_title])
            ->andFilterWhere(['like', 'top_screen_description', $this->top_screen_description])
            ->andFilterWhere(['like', 'mission_label', $this->mission_label])
            ->andFilterWhere(['like', 'company_values_title', $this->company_values_title])
            ->andFilterWhere(['like', 'ceo_name', $this->ceo_name])
            ->andFilterWhere(['like', 'ceo_position', $this->ceo_position])
            ->andFilterWhere(['like', 'ceo_quotation', $this->ceo_quotation])
            ->andFilterWhere(['like', 'form_title', $this->form_title])
            ->andFilterWhere(['like', 'form_description', $this->form_description])
            ->andFilterWhere(['like', 'form_emails', $this->form_emails]);

        return $dataProvider;
    }
}
