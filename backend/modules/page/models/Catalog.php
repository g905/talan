<?php

namespace backend\modules\page\models;

use backend\components\StylingActionColumn;
use backend\helpers\StdInput;
use backend\modules\menu\models\CatalogSubMenu;
use Yii;
use common\models\City;
use yii\helpers\Json;
use common\models\Manager;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\helpers\StdColumn;
use common\models\CompletedProject;
use backend\components\BackendModel;
use common\components\model\ActiveRecord;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%catalog}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $residental_label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $commerical_label
 * @property string $completed_label
 * @property string $form_title
 * @property string $form_description
 * @property string $form_onsubmit
 * @property string $resale_label
 * @property integer $manager_id
 *
 * @property City $city
 */
class Catalog extends ActiveRecord implements BackendModel
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%catalog}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['city_id', 'residental_label', 'commerical_label', 'completed_label', 'form_title', 'form_description', 'resale_label', 'label', 'completed_sublabel'], 'required'],
            [['city_id', 'position', 'manager_id', 'invest_active'], 'integer'],
            [['form_description'], 'string'],
            [[
                'residental_label',
                'commerical_label',
                'completed_label',
                'form_title',
                'form_onsubmit',
                'resale_label',
                'label',
                'completed_sublabel',
                'invest_title',
                'invest_desc',
                'invest_button_label',
                'invest_button_link'], 'string', 'max' => 255],
            [['invest_active'], 'default', 'value' => 0],
            [['published'], 'string', 'max' => 1],
            [['city_id'], 'exist', 'targetClass' => \common\models\City::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            ['projects_id', 'safe'],
         ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/catalog', 'ID'),
            'city_id' => Yii::t('back/catalog', 'City Id'),
            'residental_label' => Yii::t('back/catalog', 'Residental Label'),
            'published' => Yii::t('back/catalog', 'Published'),
            'position' => Yii::t('back/catalog', 'Position'),
            'created_at' => Yii::t('back/catalog', 'Created At'),
            'updated_at' => Yii::t('back/catalog', 'Updated At'),
            'commerical_label' => Yii::t('back/catalog', 'Commerical Label'),
            'completed_label' => Yii::t('back/catalog', 'Completed Label'),
            'completed_sublabel' => Yii::t('back/catalog', 'Completed Sublabel'),
            'form_title' => Yii::t('back/catalog', 'Form Title'),
            'form_description' => Yii::t('back/catalog', 'Form Description'),
            'form_onsubmit' => Yii::t('back/catalog', 'Form Onsubmit'),
            'resale_label' => Yii::t('back/catalog', 'Resale Label'),
            'manager_id' => Yii::t('back/catalog', 'Manager Id'),
            'label' => Yii::t('back/catalog', 'Label'),
            'invest_title' => Yii::t('back/home', 'Invest block title'),
            'invest_desc' => Yii::t('back/home', 'Invest block description'),
            'invest_button_label' => Yii::t('back/home', 'Invest block button label'),
            'invest_button_link' => Yii::t('back/home', 'Invest block button link'),
            'invest_active' => Yii::t('back/home', 'Show invest block'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return bt('Catalog', 'app');
    }

    /**
     * @param $page
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    [
                        'attribute' => 'manager_id',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            if ($model->manager) {
                                return '<b>'.$model->manager->fio.'</b>';
                            }
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => Manager::getItems('id', 'fio'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any manager')],
                    ],
                    'published:boolean',
                    'position',
                    // 'form_title',
                    // 'form_description:ntext',
                    // 'form_onsubmit',
                    // 'resale_label',
                    // 'manager_id',
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                return StylingActionColumn::createPreviewUrl($url, $model, 'catalog');
                            }
                        ]
                    ]
                ];
            case 'view':
                return [
                    'id',
                    'city_id',
                    'residental_label',
                    'published:boolean',
                    'position',
                    'commerical_label',
                    'completed_label',
                    'form_title',
                    [
                        'attribute' => 'form_description',
                        'format' => 'html',
                    ],
                    'form_onsubmit',
                    'resale_label',
                    'manager_id',
                    'invest_title',
                    'invest_desc',
                    'invest_button_label',
                    'invest_button_link',
                    'invest_active',
                ];
            default:
                return [];
        }
    }

    /**
     * @return CatalogSearch|\yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new CatalogSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'form-set'=> [
                Yii::t('back/page', 'Main') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'residental_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'commerical_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'resale_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'completed_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'completed_sublabel' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'projects_id' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => Select2::class,
                        'options' => [
                            'data' => CompletedProject::getItems('id', 'title'),
                            'options' => [
                                'multiple' => true,
                            ]
                        ]
                    ],
                    'position' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                ], Yii::t('back/page', 'Form') => [
                    'form_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'form_description',
                        ]
                    ],
                    'form_onsubmit' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'manager_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => Manager::getItems('id', 'fio'),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                ],
                Yii::t('back/home', 'Invest') => [
                    'invest_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'invest_desc' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'invest_button_label' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'invest_button_link' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'invest_active' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                ],
                bt('Menu') => [
                    'separator' => StdInput::separator('Меню'),
                    $this->getRelatedFormConfig()['sub_menu'],
                ],
            ]
        ];

        return $config;
    }

    public function getRelatedFormConfig()
    {
        return [
            'sub_menu' => ['relation' => 'subMenus']
        ];
    }

    public function getSubMenus()
    {
        return $this->hasMany(CatalogSubMenu::class, ['object_id' => 'id'])
            ->orderBy(['position' => SORT_ASC])
            ->onCondition(['type' => CatalogSubMenu::TYPE_CATALOG]);
    }

    /**
     *
     */
    public function afterFind()
    {
        $this->projects_id = Json::decode($this->projects_id);

        parent::afterFind();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->projects_id = Json::encode($this->projects_id);

        return parent::beforeSave($insert);
    }
}
