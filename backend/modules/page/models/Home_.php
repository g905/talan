<?php

namespace backend\modules\page\models;

use backend\components\StylingActionColumn;
use backend\modules\contacts\models\City;
use common\models\EntityToFile;
use common\models\Home as CommonHome;
use kartik\grid\GridView;
use vadymsemenykv\imageRequireValidator\ImageRequireValidator;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "{{%home}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $top_screen_title
 * @property string $top_screen_btn_label
 * @property string $top_screen_btn_link
 * @property string $about_company_title
 * @property string $about_company_desc
 * @property string $about_company_link_label
 * @property string $about_company_link
 * @property string $how_we_build_title
 * @property string $apartment_complex_title
 * @property string $alternate_title
 * @property string $alternate_widget
 * @property string $apartment_complex_subtitle
 * @property string $apartment_complex_description
 * @property string $apartment_complex_link_label
 * @property string $promo_title
 * @property string $promo_link_label
 * @property string $review_title
 * @property string $final_cta_title
 * @property string $final_cta_description
 * @property string $final_cta_btn_label
 * @property string $final_cta_btn_link
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $apartment_complex_link
 * @property string $top_screen_btn_onclick
 *
 * @property City $city
 */
class Home extends ActiveRecord implements BackendModel
{
    /**
     * Attribute for imageUploader
     */
    public $top_screen_background_image;

    /**
     * Attribute for imageUploader
     */
    public $top_screen_background_video;

    /**
     * Attribute for imageUploader
     */
    public $about_company_image;

    /**
     * Temporary sign which used for saving images before model save
     * @var string
     */
    public $sign;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%home}}';
    }

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'top_screen_title', 'about_company_title', 'about_company_desc', 'how_we_build_title'], 'required'],
            [['city_id', 'published'], 'integer'],
            [['city_id'], 'unique'],
            [['about_company_desc', 'apartment_complex_description', 'final_cta_description', 'alternate_title'], 'string'],
            [
                [
                    'top_screen_title',
                    'top_screen_btn_label',
                    'top_screen_btn_link',
                    'about_company_title',
                    'about_company_link_label',
                    'about_company_link',
                    'how_we_build_title',
                    'apartment_complex_title',
                    'apartment_complex_subtitle',
                    'apartment_complex_link_label',
                    'promo_title',
                    'promo_link_label',
                    'review_title',
                    'final_cta_title',
                    'final_cta_btn_label',
                    'final_cta_btn_link',
                    'apartment_complex_link',
                    'top_screen_btn_onclick',
                ],
                'string',
                'max' => 255
            ],
            [['city_id'], 'exist', 'targetClass' => \common\models\City::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['alternate_widget'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],

            ['top_screen_background_image',
                ImageRequireValidator::className(),
                'errorMessage' => Yii::t('back/home', 'Files are required for uploading'),
                'imageRelation' => 'topScreenBackgroundImageRelation',
                'skipOnEmpty' => false
            ],
            ['about_company_image',
                ImageRequireValidator::className(),
                'errorMessage' => Yii::t('back/home', 'Files are required for uploading'),
                'imageRelation' => 'aboutCompanyImageRelation',
                'skipOnEmpty' => false
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopScreenBackgroundImageRelation()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['imageAttr.entity_model_name' => static::formName(), 'imageAttr.attribute' => CommonHome::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE])
            ->alias('imageAttr')
            ->orderBy('imageAttr.position DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompanyImageRelation()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['imageAttr.entity_model_name' => static::formName(), 'imageAttr.attribute' => CommonHome::SAVE_ATTRIBUTE_ABOUT_COMPANY_IMAGE])
            ->alias('imageAttr')
            ->orderBy('imageAttr.position DESC');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/home', 'ID'),
            'city_id' => Yii::t('back/home', 'City'),
            'top_screen_title' => Yii::t('back/home', 'Top screen title'),
            'top_screen_btn_label' => Yii::t('back/home', 'Top screen btn label'),
            'top_screen_btn_link' => Yii::t('back/home', 'Top screen btn link'),
            'about_company_title' => Yii::t('back/home', 'Title'),
            'about_company_desc' => Yii::t('back/home', 'Description'),
            'about_company_link_label' => Yii::t('back/home', 'Link label'),
            'about_company_link' => Yii::t('back/home', 'Link'),
            'how_we_build_title' => Yii::t('back/home', 'How we build title'),
            'alternate_widget' => Yii::t('back/home', 'Alternate widget'),
            'alternate_title' => Yii::t('back/home', 'Alternate title'),
            'apartment_complex_title' => Yii::t('back/home', 'title'),
            'apartment_complex_subtitle' => Yii::t('back/home', 'subtitle'),
            'apartment_complex_description' => Yii::t('back/home', 'description'),
            'apartment_complex_link_label' => Yii::t('back/home', 'link label'),
            'apartment_complex_link' => Yii::t('back/home', 'link'),
            'promo_title' => Yii::t('back/home', 'Promo title'),
            'promo_link_label' => Yii::t('back/home', 'Promo link label'),
            'review_title' => Yii::t('back/home', 'Review title'),
            'final_cta_title' => Yii::t('back/home', 'title'),
            'final_cta_description' => Yii::t('back/home', 'description'),
            'final_cta_btn_label' => Yii::t('back/home', 'btn label'),
            'final_cta_btn_link' => Yii::t('back/home', 'btn link'),
            'published' => Yii::t('back/home', 'Published'),
            'created_at' => Yii::t('back/home', 'Created At'),
            'updated_at' => Yii::t('back/home', 'Updated At'),
            'top_screen_background_image' => Yii::t('back/home', 'Background image'),
            'top_screen_background_video' => Yii::t('back/home', 'Background video'),
            'about_company_image' => Yii::t('back/home', 'About company img'),
            'top_screen_btn_onclick' => Yii::t('back/home', 'Top screen btn onclick'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHowWeBuild()
    {
        return $this->hasMany(HowWeBuild::className(), ['home_page_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlternateWidget()
    {
        return $this->hasMany(AlternateWidget::className(), ['home_page_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReview()
    {
        return $this->hasMany(Review::className(), ['home_page_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/home', 'Home');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var Home $model */
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    'top_screen_title',
                    'about_company_title',
                    'how_we_build_title',
//                    'apartment_complex_title',
//                    'review_title',
//                    'final_cta_title',
                    'published:boolean',
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                return StylingActionColumn::createPreviewUrl($url, $model, '');
                            }
                        ]
                    ]
                ];
                break;
            case 'view':
                return [
                    'id',
                    'city_id',
                    'top_screen_title',
                    'top_screen_btn_label',
                    'top_screen_btn_link',
                    'about_company_title',
                    [
                        'attribute' => 'about_company_desc',
                        'format' => 'html',
                    ],
                    'about_company_link_label',
                    'about_company_link',
                    'how_we_build_title',
                    'apartment_complex_title',
                    'apartment_complex_subtitle',
                    [
                        'attribute' => 'apartment_complex_description',
                        'format' => 'html',
                    ],
                    'apartment_complex_link_label',
                    'promo_title',
                    'promo_link_label',
                    'review_title',
                    'final_cta_title',
                    [
                        'attribute' => 'final_cta_description',
                        'format' => 'html',
                    ],
                    'final_cta_btn_label',
                    'final_cta_btn_link',
                    'published:boolean',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new HomeSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                Yii::t('back/home', 'General') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\City::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
                Yii::t('back/home', 'Top screen') => [
                    'top_screen_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'top_screen_btn_label' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'top_screen_btn_link' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'top_screen_btn_onclick' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'top_screen_background_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'top_screen_background_image',
                            'saveAttribute' => CommonHome::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'top_screen_background_video' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['mp4'],
                            'attribute' => 'top_screen_background_video',
                            'saveAttribute' => CommonHome::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                Yii::t('back/home', 'About company') => [
                    'about_company_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'about_company_link_label' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'about_company_link' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'about_company_desc' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'about_company_desc',
                        ]
                    ],
                    'about_company_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'about_company_image',
                            'saveAttribute' => CommonHome::SAVE_ATTRIBUTE_ABOUT_COMPANY_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                Yii::t('back/home', 'How we build') => [
                    'how_we_build_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> <i class="si si-grid"></i> '.Yii::t('back/home', 'how we build - blocks').'</div>',
                        'label' => false,
                    ],
                    $this->getRelatedFormConfig()['how_we_build_blocks']
                ],
                Yii::t('back/home', 'Apartment complex')  => [
                    'apartment_complex_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'apartment_complex_subtitle' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'apartment_complex_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'apartment_complex_description',
                        ]
                    ],
                    'apartment_complex_link_label' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'apartment_complex_link' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'alternate_widget' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'alternate_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    $this->getRelatedFormConfig()['alternate_widget_block']
                ],
                Yii::t('back/home', 'Promo')  => [
                    'promo_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'promo_link_label' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                ],
                Yii::t('back/home', 'Review') => [
                    'review_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> <i class="si si-grid"></i> '.Yii::t('back/home', 'review - blocks').'</div>',
                        'label' => false,
                    ],
                    $this->getRelatedFormConfig()['review_blocks']
                ],
                Yii::t('back/home', 'Final CTA') => [
                    'final_cta_title' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'final_cta_btn_label' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'final_cta_btn_link' => [
                        'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'final_cta_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'final_cta_description',
                        ]
                    ],
                ],

            ],
        ];

        return $config;
    }

    public function getRelatedFormConfig()
    {
        return [
            'how_we_build_blocks' => [
                'relation' => 'howWeBuild',
            ],
            'review_blocks' => [
                'relation' => 'review',
            ],
            'alternate_widget_block' => [
                'relation' => 'alternateWidget',
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
