<?php

namespace backend\modules\page\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ContactPageSearch represents the model behind the search form about `ContactPage`.
 */
class ContactPageSearch extends ContactPage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'map_zoom', 'published'], 'integer'],
            [['label', 'sales_phones', 'reception_phones', 'address', 'email', 'timetable', 'hot_to_get_label', 'forms_label'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactPageSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'map_zoom' => $this->map_zoom,
            'published' => $this->published,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'sales_phones', $this->sales_phones])
            ->andFilterWhere(['like', 'reception_phones', $this->reception_phones])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'timetable', $this->timetable])
            ->andFilterWhere(['like', 'hot_to_get_label', $this->hot_to_get_label])
            ->andFilterWhere(['like', 'forms_label', $this->forms_label]);

        return $dataProvider;
    }
}
