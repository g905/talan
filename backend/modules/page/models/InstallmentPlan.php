<?php

namespace backend\modules\page\models;

use backend\helpers\StdInput;
use backend\modules\menu\models\InstallmentSubMenu;
use backend\validators\EmailsInStringValidator;
use yii\grid\SerialColumn;
use yii\behaviors\TimestampBehavior;
use kartik\grid\GridView;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\models\City as CommonCity;
use common\components\CommonDataModel;
use common\models\Manager as CommonManager;
use common\models\InstallmentPlan as CommonInstallmentPlan;
use backend\components\FormBuilder;
use backend\components\BackendModel;
use backend\components\ImperaviContent;
use backend\modules\contacts\models\City;
use backend\components\StylingActionColumn;
use backend\modules\contacts\models\Manager;
use backend\modules\block\models\block\InstallmentPlanStep;
use backend\modules\block\models\block\InstallmentPlanBenefit;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;

/**
 * This is the model class for table "{{%installment_plan}}".
 *
 * @property City $city
 * @property Manager $manager
 */
class InstallmentPlan extends CommonInstallmentPlan implements BackendModel
{
    public $sign;
    public $description_photo;

    public function init()
    {
        parent::init();
        $this->sign = $this->sign ?: security()->generateRandomString();
    }

    public static function tableName()
    {
        return '{{%installment_plan}}';
    }

    public function rules()
    {
        return [
            [['city_id', 'manager_id', 'default_apartment_price', 'default_time_term', 'default_initial_fee', 'view_count', 'min_term', 'max_term'], 'integer'],
            [['city_id', 'label', 'alias'], 'required'],
            [['description_text'], 'string'],
            [['published'], 'string', 'max' => 1],
            [['published'], 'default', 'value' => 1],
            [['city_id'], 'exist', 'targetClass' => CommonCity::class, 'targetAttribute' => 'id'],
            [['manager_id'], 'exist', 'targetClass' => CommonManager::class, 'targetAttribute' => 'id'],
            [[
                'label', 'alias', 'description_label',
                'benefits_block_label', 'steps_block_label',
                'form_calc_onsubmit', 'form_bottom_onsubmit', 'sign'
            ], 'string', 'max' => 255],
            [['notification_email'], 'trim'],
            [['notification_email'], EmailsInStringValidator::class],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'installment-plan'),
            'city_id' => bt('City', 'installment-plan'),
            'manager_id' => bt('Manager', 'installment-plan'),
            'label' => bt('Label', 'installment-plan'),
            'alias' => bt('Alias', 'installment-plan'),
            'description_photo' => bt('Description image', 'installment-plan'),
            'description_label' => bt('Description label', 'installment-plan'),
            'description_text' => bt('Description text', 'installment-plan'),
            'default_apartment_price' => bt('Apartment price in rub', 'installment-plan'),
            'default_time_term' => bt('Time term in months', 'installment-plan'),
            'default_initial_fee' => bt('Initial fee percent', 'installment-plan'),
            'form_calc_onsubmit' => bt('Calculator form OnSubmit event', 'installment-plan'),
            'form_bottom_onsubmit' => bt('Bottom form OnSubmit event', 'installment-plan'),
            'benefits_block_label' => bt('Benefits block label', 'installment-plan'),
            'steps_block_label' => bt('Steps block label', 'installment-plan'),
            'view_count' => bt('View count', 'installment-plan'),
            'published' => bt('Published', 'installment-plan'),
            'min_term' => bt('Min term', 'installment-plan'),
            'max_term' => bt('Max term', 'installment-plan'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'seo' => MetaTagBehavior::class,
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getTitle()
    {
        return bt('Installment Plan', 'installment-plan');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => SerialColumn::class],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true]],
                        'filterInputOptions' => ['placeholder' => bt('Any city', 'site-menu')],
                    ],
                    'manager_id',
                    'label',
                    'alias',
                    // 'description_label',
                    // 'description_text:ntext',
                    // 'default_apartment_price',
                    // 'default_time_term:datetime',
                    // 'default_initial_fee',
                    // 'form_calc_onsubmit',
                    // 'form_bottom_onsubmit',
                    'view_count',
                    'published:boolean',
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                return StylingActionColumn::createPreviewUrl($url, $model, 'installment-plan', false);
                            }
                        ]
                    ]
                ];
            case 'view':
                return [
                    'id',
                    'city_id',
                    'manager_id',
                    'label',
                    'alias',
                    'description_label',
                    [
                        'attribute' => 'description_text',
                        'format' => 'html',
                    ],
                    'default_apartment_price',
                    'default_time_term:datetime',
                    'default_initial_fee',
                    'form_calc_onsubmit',
                    'form_bottom_onsubmit',
                    'view_count',
                    'published:boolean',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new InstallmentPlanSearch();
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General', 'installment-plan') => [
                    'label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                            'class' => $this->isNewRecord ? 's_name form-control' : 'form-control'
                        ],
                    ],
                    'alias' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                            'class' => $this->isNewRecord ? 's_alias form-control' : 'form-control'
                        ],
                    ],
                    'city_id' => [
                        'type' => FormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => CommonCity::getItems(),
                        'options' => ['prompt' => ''],
                    ],
                    'benefits_block_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                        'hint' => 'Используйте &lt;span&gt; тег чтобы выделить слово',
                    ],
                    'steps_block_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'published' => [
                        'type' => FormBuilder::INPUT_CHECKBOX,
                    ],
                    'sign' => [
                        'type' => FormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
                bt('Top block', 'installment-plan') => [
                    'description_photo' => [
                        'type' => FormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'description_photo',
                            'saveAttribute' => self::ATTR_DESCRIPTION_IMAGE,
                            'aspectRatio' => false,
                            'multiple' => false,
                        ])
                    ],
                    'description_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'description_text' => [
                        'type' => FormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::class,
                        'options' => ['model' => $this, 'attribute' => 'description_text']
                    ],
                ],
                bt('Calculator', 'installment-plan') => [
                    'default_apartment_price' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getCurrencyText()],
                    ],
                    'default_time_term' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getMonthsText()],
                    ],
                    'default_initial_fee' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getPercentText()],
                    ],
                    'min_term' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getMonthsText()],
                    ],
                    'max_term' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getMonthsText()],
                    ],
                    'notification_email' => [
                        'type' => FormBuilder::INPUT_TEXTAREA,
                        'options' => ['rows' => 6]
                    ],
                    'form_calc_onsubmit' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                ],
                bt('Benefits block', 'installment-plan') => [
                    $this->getRelatedFormConfig()['benefit_blocks']
                ],
                bt('Steps block', 'installment-plan') => [
                    $this->getRelatedFormConfig()['step_blocks']
                ],
                bt('Bottom form', 'installment-plan') => [
                    'manager_id' => [
                        'type' => FormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => CommonManager::getItems('id', 'fio'),
                        'options' => ['prompt' => ''],
                    ],
                    'form_bottom_onsubmit' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                ],
                bt('Menu') => [
                    'separator' => StdInput::separator('Меню'),
                    $this->getRelatedFormConfig()['sub_menu'],
                ],
            ]
        ];
    }

    public function getRelatedFormConfig()
    {
        return [
            'benefit_blocks' => ['relation' => 'benefitBlocks'],
            'step_blocks' => ['relation' => 'stepBlocks'],
            'sub_menu' => ['relation' => 'subMenus']
        ];
    }

    public function getSubMenus()
    {
        return $this->hasMany(InstallmentSubMenu::class, ['object_id' => 'id'])
            ->orderBy(['position' => SORT_ASC])
            ->onCondition(['type' => InstallmentSubMenu::TYPE_INSTALLMENT]);
    }

    public function getBenefitBlocks()
    {
        return $this->hasMany(InstallmentPlanBenefit::class, ['object_id' => 'id'])
            ->onCondition(['type' => InstallmentPlanBenefit::INSTALLMENT_PLAN_BENEFIT_BLOCK]);
    }

    public function getStepBlocks()
    {
        return $this->hasMany(InstallmentPlanStep::class, ['object_id' => 'id'])
            ->onCondition(['type' => InstallmentPlanStep::INSTALLMENT_PLAN_STEP_BLOCK]);
    }
}
