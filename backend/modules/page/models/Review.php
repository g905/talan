<?php

namespace backend\modules\page\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%review}}".
 *
 * @property integer $id
 * @property string $fio
 * @property string $label
 * @property string $review_text
 * @property string $youtube_video
 * @property integer $home_page_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Home $homePage
 */
class Review extends ActiveRecord implements BackendModel
{

    /**
    * Attribute for imageUploader
    */
    public $photo;

    /**
    * Temporary sign which used for saving images before model save
    * @var string
    */
    public $sign;

    public function init()
    {
        $this->published = 1;
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%review}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'review_text'], 'required'],
            [['review_text'], 'string'],
            [['youtube_video'], 'url'],
            [['home_page_id', 'published', 'position'], 'integer'],
            [['fio', 'label', 'youtube_video'], 'string', 'max' => 255],
            [['home_page_id'], 'exist', 'targetClass' => \common\models\Home::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
         ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/home-review', 'ID'),
            'fio' => Yii::t('back/home-review', 'Name'),
            'label' => Yii::t('back/home-review', 'Short description'),
            'review_text' => Yii::t('back/home-review', 'Review text'),
            'youtube_video' => Yii::t('back/home-review', 'Youtube video'),
            'home_page_id' => Yii::t('back/home-review', 'Home page'),
            'published' => Yii::t('back/home-review', 'Published'),
            'position' => Yii::t('back/home-review', 'Position'),
            'created_at' => Yii::t('back/home-review', 'Created At'),
            'updated_at' => Yii::t('back/home-review', 'Updated At'),
            'photo' => Yii::t('back/home-review', 'photo'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHomePage()
    {
        return $this->hasOne(Home::className(), ['id' => 'home_page_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/home-review', 'Review');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'fio',
                    'label',
                    // 'review_text:ntext',
                    'youtube_video',
                    'home_page_id',
                    'published:boolean',
                    'position',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'fio',
                    'label',
                    [
                        'attribute' => 'review_text',
                        'format' => 'html',
                    ],
                    'youtube_video',
                    'home_page_id',
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ReviewSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'fio' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'label' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'youtube_video' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
                'hint' => 'Youtube url, eg https://www.youtube.com/watch?v=viDeO-CodE'
            ],
            'review_text' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'review_text',
                ]
            ],
            'photo' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                    'attribute' => 'photo',
                    'saveAttribute' => \common\models\Review::SAVE_ATTRIBUTE_PHOTO,
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }


    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
