<?php

namespace backend\modules\page\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ContactFormSearch represents the model behind the search form about `ContactForm`.
 */
class ContactFormSearch extends ContactForm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contact_page_id', 'manager_id', 'position', 'published'], 'integer'],
            [['nav_label', 'form_label', 'form_description', 'form_agreement'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactFormSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'contact_page_id' => $this->contact_page_id,
            'manager_id' => $this->manager_id,
            'position' => $this->position,
            'published' => $this->published,
        ]);

        $query->andFilterWhere(['like', 'nav_label', $this->nav_label])
            ->andFilterWhere(['like', 'form_label', $this->form_label])
            ->andFilterWhere(['like', 'form_description', $this->form_description])
            ->andFilterWhere(['like', 'form_agreement', $this->form_agreement]);

        return $dataProvider;
    }
}
