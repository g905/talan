<?php

namespace backend\modules\page\models;

use common\models\PublicDataChartData as CommonPublicDataChartData;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\components\BackendModel;

/**
 * This is the model class for table "{{%public_data_chart_data}}".
 *
 * @property integer $id
 * @property integer $widget_id
 * @property string $label
 * @property string $value
 * @property string $color
 * @property integer $position
 *
 * @property PublicDataWidget $widget
 */
class PublicDataChartData extends CommonPublicDataChartData implements BackendModel
{
    public static function tableName()
    {
        return '{{%public_data_chart_data}}';
    }

    public function rules()
    {
        return [
            [['widget_id', 'position'], 'integer'],
            [['label', 'value', 'color'], 'string', 'max' => 255],
            [['widget_id'], 'exist', 'targetClass' => PublicDataWidget::class, 'targetAttribute' => 'id'],
            [['position'], 'default', 'value' => 0],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'public-data-widget'),
            'widget_id' => bt('Widget', 'public-data-widget'),
            'label' => bt('Label', 'public-data-widget'),
            'value' => bt('Value', 'public-data-widget'),
            'color' => bt('Color', 'public-data-widget'),
            'position' => bt('Position', 'public-data-widget'),
        ];
    }

    public function getTitle()
    {
        return bt('Public Data Chart Data', 'public-data-widget');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    'widget_id',
                    'label',
                    'value',
                    'color',
                    'position',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    'widget_id',
                    'label',
                    'value',
                    'color',
                    'position',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new PublicDataChartDataSearch();
    }

    public function getFormConfig()
    {
        return [
            'widget_id' => StdInput::hidden(),
            'label' => StdInput::text(),
            'value' => StdInput::text(),
            'color' => StdInput::colorSelect(),
            'position' => StdInput::hidden(),
        ];
    }

    public function getWidget()
    {
        return $this->hasOne(PublicDataWidget::class, ['id' => 'widget_id']);
    }
}
