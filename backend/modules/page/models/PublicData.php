<?php

namespace backend\modules\page\models;

use yii\behaviors\TimestampBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\models\EntityToFile;
use common\models\PublicData as CommonPublicData;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\components\BackendModel;
use backend\modules\builder\models\BuilderConfig;
use backend\modules\menu\models\PublicDataSubMenu;
use backend\modules\builder\components\BuilderBehavior;

/**
 * This is the model class for table "{{%public_data}}".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property string $label
 * @property string $alias
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PublicDataMenu $menu
 * @property PublicDataSubMenu[] $subMenus
 */
class PublicData extends CommonPublicData implements BackendModel
{
    public $sign;
    public $content;

    public function init()
    {
        parent::init();
        $this->sign = $this->sign ?: security()->generateRandomString();
    }

    public static function tableName()
    {
        return '{{%public_data}}';
    }

    public function rules()
    {
        return [
            [['menu_id', 'published', 'position'], 'integer'],
            [['label'], 'required'],
            [['label'], 'string', 'max' => 255],
            [['menu_id'], 'exist', 'targetClass' => PublicDataMenu::class, 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['menu_id'], 'default', 'value' => null],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
            [['content'], 'safe'],
            [['alias'], 'match', 'pattern' => '/^[a-z0-9]+(?:-[a-z0-9]+)*$/'],
            [['alias'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'public-data'),
            'menu_id' => bt('Menu', 'public-data'),
            'label' => bt('Label', 'public-data'),
            'alias' => bt('Alias', 'public-data'),
            'published' => bt('Published', 'public-data'),
            'position' => bt('Position', 'public-data'),
        ];
    }

    public function getTitle()
    {
        return bt('Public data', 'public-data');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    StdColumn::publicDataMenu(),
                    'label',
                    // 'alias:ntext',
                    StdColumn::published(),
                    StdColumn::position(),
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    StdColumn::publicDataMenu(),
                    'label',
                    'alias',
                    StdColumn::published(),
                    StdColumn::position(),
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new PublicDataSearch();
    }


    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'seo' => MetaTagBehavior::class,
            'content' => [
                'class' => BuilderBehavior::class,
                'attributes' => ['content'],
                'widgets' => [
                    'common\models\builder\publicData\ProudFigure',
                    // 'common\models\builder\publicData\DiagramTable',
                    // 'common\models\builder\publicData\DiagramSection',
                    'common\models\builder\publicData\ColumnTable',
                    'common\models\builder\publicData\ColumnSection',
                    'common\models\builder\publicData\QuantityDdu',
                    'common\models\builder\publicData\Schedule',
                    'common\models\builder\publicData\ContentSection',
                    'common\models\builder\publicData\ServiceLevel',
                    'common\models\builder\publicData\RatingSection',
                    'common\models\builder\publicData\FilesSection',
                    'common\models\builder\publicData\CustomDduSection',
                ],
            ],
        ];
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General') => [
                    'menu_id' => StdInput::publicDataMenuSelect(),
                    'label' => StdInput::label($this),
                    'alias' => StdInput::alias($this),
                    'published' => StdInput::checkbox(),
                    'position' => StdInput::text(),
                ],
                bt('Constructor') => [
                    'content' => BuilderConfig::config($this, 'content', [
                        'models' => [
                            'common\models\builder\publicData\ProudFigure',
                            'common\models\builder\publicData\ColumnTable',
                            'common\models\builder\publicData\ColumnSection',
                            // 'common\models\builder\publicData\DiagramTable',
                            // 'common\models\builder\publicData\DiagramSection',
                            'common\models\builder\publicData\QuantityDdu',
                            'common\models\builder\publicData\Schedule',
                            'common\models\builder\publicData\ContentSection',
                            'common\models\builder\publicData\ServiceLevel',
                            'common\models\builder\publicData\RatingSection',
                            'common\models\builder\publicData\FilesSection',
                            'common\models\builder\publicData\CustomDduSection',
                        ],
                    ])
                ],
                bt('Menu') => [
                    'separator' => StdInput::separator('Меню'),
                    $this->getRelatedFormConfig()['sub_menu'],
                ],
            ],
        ];
    }

    public function getRelatedFormConfig()
    {
        return [
            'sub_menu' => ['relation' => 'subMenus']
        ];
    }

    public function getMenu()
    {
        return $this->hasOne(PublicDataMenu::class, ['id' => 'menu_id']);
    }

    public function getSubMenus()
    {
        return $this->hasMany(PublicDataSubMenu::class, ['object_id' => 'id'])
            ->orderBy(['position' => SORT_ASC])
            ->onCondition(['type' => PublicDataSubMenu::TYPE_PUBLIC_DATA]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
