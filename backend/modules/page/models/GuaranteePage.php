<?php

namespace backend\modules\page\models;

use backend\components\StylingActionColumn;
use Yii;
use common\models\City;
use kartik\grid\GridView;
use common\models\Manager;
use yii\helpers\ArrayHelper;
use backend\helpers\StdColumn;
use backend\components\BackendModel;
use common\components\model\ActiveRecord;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%guarantee_page}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $manager_id
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $city_id
 * @property string $alias
 * @property string $form_title
 * @property string $form_description
 * @property string $form_onsubmit
 */
class GuaranteePage extends ActiveRecord implements BackendModel
{
    public static function tableName()
    {
        return '{{%guarantee_page}}';
    }

    public function rules()
    {
        return [
            [['label', 'city_id', 'alias', 'form_title', 'form_description', 'form_onsubmit'], 'required'],
            [['manager_id', 'city_id'], 'integer'],
            [['label', 'alias', 'form_title', 'form_onsubmit'], 'string', 'max' => 255],
            [['published'], 'string', 'max' => 1],
            [['published'], 'default', 'value' => 1],
            [['form_description', 'form_agree', 'emails'], 'string'],
            [['alias'], 'unique'],
         ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/page', 'ID'),
            'label' => Yii::t('back/page', 'Label'),
            'manager_id' => Yii::t('back/page', 'Manager'),
            'published' => Yii::t('back/page', 'Published'),
            'created_at' => Yii::t('back/page', 'Created At'),
            'updated_at' => Yii::t('back/page', 'Updated At'),
            'city_id' => Yii::t('back/page', 'City'),
            'emails' => Yii::t('back/page', 'Emails'),
            'form_agree' => Yii::t('back/page', 'Agree Text'),
            'alias' => Yii::t('back/page', 'Alias'),
            'form_title' => Yii::t('back/page', 'Form Title'),
            'form_description' => Yii::t('back/page', 'Form Description'),
            'form_onsubmit' => Yii::t('back/page', 'Form Onsubmit'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    public function getTitle()
    {
        return bt('Guarantee Page', 'app');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    'label',
                    'alias',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    [
                        'attribute' => 'manager_id',
                        'format' => 'raw',
                        'value' => function (self $model) {
                            if ($model->manager) {
                                return '<b>'.$model->manager->fio ?? null.'</b>';
                            }
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => Manager::getItems('id', 'fio'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any manager')],
                    ],
                    'published:boolean',
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                return StylingActionColumn::createPreviewUrl($url, $model, 'guarantee');
                            }
                        ]
                    ]
                ];
            case 'view':
                return [
                    'id',
                    'label',
                    'manager_id',
                    'published:boolean',
                    'city_id',
                    'alias',
                    'form_title',
                    'form_description',
                    'form_onsubmit',
                ];
            default:
                return [];
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getSearchModel()
    {
        return new GuaranteePageSearch();
    }

    public function getFormConfig()
    {
        $config = [
            'form-set'=> [
                Yii::t('back/page', 'Main') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                            'class' => 's_name form-control'
                        ],
                    ],
                    'alias' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                            'class' => 's_alias form-control'
                        ],
                    ],
//                    'position' => [
//                        'type' => ActiveFormBuilder::INPUT_TEXT,
//                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                ],
                Yii::t('back/page', 'Form') => [
                    'form_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_onsubmit' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_agree' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'emails' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'manager_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\Manager::getItems('id', 'fio'),
                        'options' => [
                            'prompt' => '--',
                        ],
                    ],
                ]
            ]
        ];

        return $config;
    }
}
