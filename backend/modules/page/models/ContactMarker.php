<?php

namespace backend\modules\page\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\models\ContactMarker as CommonContactMarker;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\components\BackendModel;

/**
 * This is the model class for table "{{%contact_marker}}".
 *
 * @property integer $id
 * @property integer $contact_page_id
 * @property string $lat
 * @property string $lng
 * @property string $label
 * @property string $phone
 * @property string $address
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ContactPage $contactPage
 */
class ContactMarker extends CommonContactMarker implements BackendModel
{
    public static function tableName()
    {
        return '{{%contact_marker}}';
    }

    public function rules()
    {
        return [
            [['contact_page_id', 'position'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['label', 'phone', 'address'], 'string', 'max' => 255],
            [['published'], 'string', 'max' => 1],
            [['contact_page_id'], 'exist', 'targetClass' => ContactPage::class, 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'contact-marker'),
            'contact_page_id' => bt('Contact Page ID', 'contact-marker'),
            'lat' => bt('Lat', 'contact-marker'),
            'lng' => bt('Lng', 'contact-marker'),
            'label' => bt('Label', 'contact-marker'),
            'phone' => bt('Phone', 'contact-marker'),
            'address' => bt('Address', 'contact-marker'),
            'published' => bt('Published', 'contact-marker'),
            'position' => bt('Position', 'contact-marker'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getContactPage()
    {
        return $this->hasOne(ContactPage::class, ['id' => 'contact_page_id']);
    }

    public function getTitle()
    {
        return bt('Contact Marker', 'app');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    'contact_page_id',
                    'lat',
                    'lng',
                    'label',
                    'phone',
                    'address',
                    'published:boolean',
                    'position',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    'contact_page_id',
                    'lat',
                    'lng',
                    'label',
                    'phone',
                    'address',
                    'published:boolean',
                    'position',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new ContactMarkerSearch();
    }

    public function getFormConfig()
    {
        $uniqueId = uniqid();

        return [
            // 'contact_page_id' => [
            //     'type' => FormBuilder::INPUT_DROPDOWN_LIST,
            //     'items' => \common\models\ContactPage::getItems(),
            //     'options' => ['prompt' => ''],
            // ],
            StdInput::map($uniqueId),
            'lat' => StdInput::mapLat($this, $uniqueId),
            'lng' => StdInput::mapLng($this, $uniqueId),
            'label' => StdInput::text(),
            'phone' => StdInput::text(),
            'address' => StdInput::text(),
            'published' => StdInput::checkbox(),
            'position' => StdInput::text(),
        ];
    }
}
