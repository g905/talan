<?php

namespace backend\modules\page\models;

use yii\behaviors\TimestampBehavior;
use common\models\PublicDataMenu as CommonPublicDataMenu;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\components\BackendModel;

/**
 * This is the model class for table "{{%public_data_menu}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PublicData[] $publicData
 */
class PublicDataMenu extends CommonPublicDataMenu implements BackendModel
{
    public static function tableName()
    {
        return '{{%public_data_menu}}';
    }

    public function rules()
    {
        return [
            [['label'], 'required'],
            [['published', 'position'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'public-data-menu'),
            'label' => bt('Label', 'public-data-menu'),
            'published' => bt('Published', 'public-data-menu'),
            'position' => bt('Position', 'public-data-menu'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getTitle()
    {
        return bt('Public data menu', 'app');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    'label',
                    StdColumn::published(),
                    StdColumn::position(),
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    'label',
                    StdColumn::published(),
                    StdColumn::position(),
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new PublicDataMenuSearch();
    }

    public function getFormConfig()
    {
        return [
            'label' => StdInput::text(),
            'published' => StdInput::checkbox(),
            'position' => StdInput::text(),
        ];
    }

    public function getPublicData()
    {
        return $this->hasMany(PublicData::class, ['menu_id' => 'id']);
    }
}
