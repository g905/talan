<?php

namespace backend\modules\page\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BuyPageSearch represents the model behind the search form about `BuyPage`.
 */
class BuyPageSearch extends BuyPage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'form_top_square', 'manager_id', 'published', 'position'], 'integer'],
            [['top_screen_title', 'top_screen_description', 'top_screen_btn', 'form_top_title', 'form_top_emails', 'form_top_btn', 'form_top_onsubmit', 'advantages_title', 'steps_title', 'form_bottom_title', 'form_bottom_description', 'form_bottom_email', 'form_bottom_onsubmit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BuyPageSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'form_top_square' => $this->form_top_square,
            'manager_id' => $this->manager_id,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'top_screen_title', $this->top_screen_title])
            ->andFilterWhere(['like', 'top_screen_description', $this->top_screen_description])
            ->andFilterWhere(['like', 'top_screen_btn', $this->top_screen_btn])
            ->andFilterWhere(['like', 'form_top_title', $this->form_top_title])
            ->andFilterWhere(['like', 'form_top_emails', $this->form_top_emails])
            ->andFilterWhere(['like', 'form_top_btn', $this->form_top_btn])
            ->andFilterWhere(['like', 'form_top_onsubmit', $this->form_top_onsubmit])
            ->andFilterWhere(['like', 'advantages_title', $this->advantages_title])
            ->andFilterWhere(['like', 'steps_title', $this->steps_title])
            ->andFilterWhere(['like', 'form_bottom_title', $this->form_bottom_title])
            ->andFilterWhere(['like', 'form_bottom_description', $this->form_bottom_description])
            ->andFilterWhere(['like', 'form_bottom_email', $this->form_bottom_email])
            ->andFilterWhere(['like', 'form_bottom_onsubmit', $this->form_bottom_onsubmit]);

        return $dataProvider;
    }
}
