<?php

namespace backend\modules\page\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PublicDataWidgetSearch represents the model behind the search form about `PublicDataWidget`.
 */
class PublicDataWidgetSearch extends PublicDataWidget
{
    public function rules()
    {
        return [
            [['id', 'type', 'city_id', 'plan', 'fact', 'published', 'position'], 'integer'],
            [['label', 'chart_name', 'link', 'value', 'status', 'placed', 'seria'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = PublicDataWidgetSearch::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'city_id' => $this->city_id,
            'plan' => $this->plan,
            'fact' => $this->fact,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'chart_name', $this->chart_name])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'placed', $this->placed])
            ->andFilterWhere(['like', 'seria', $this->seria]);

        return $dataProvider;
    }
}
