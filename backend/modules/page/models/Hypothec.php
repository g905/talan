<?php

namespace backend\modules\page\models;

use backend\helpers\StdInput;
use backend\modules\menu\models\HypotecSubMenu;
use backend\validators\EmailsInStringValidator;
use yii\grid\SerialColumn;
use yii\behaviors\TimestampBehavior;
use kartik\grid\GridView;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\models\City as CommonCity;
use common\models\Manager as CommonManager;
use common\models\Hypothec as CommonHypothec;
use common\components\CommonDataModel;
use backend\components\FormBuilder;
use backend\helpers\Select2Helper;
use backend\components\BackendModel;
use backend\modules\bank\models\Bank;
use backend\components\ImperaviContent;
use backend\modules\contacts\models\City;
use backend\components\ManyToManyBehavior;
use backend\components\StylingActionColumn;
use backend\modules\contacts\models\Manager;
use backend\modules\bank\models\BankToHypothec;
use backend\modules\block\models\block\HypothecStep;
use backend\modules\block\models\block\HypothecBenefit;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;

/**
 * This is the model class for table "{{%hypothec}}".
 *
 * @property City $city
 * @property Manager $manager
 */
class Hypothec extends CommonHypothec implements BackendModel
{
    public $descriptionImage;
    public $benefitImage;
    public $sign;

    public $bankIds;
    public $bankIdValues;

    public function init()
    {
        parent::init();
        $this->sign = $this->sign ?: security()->generateRandomString();
    }

    public static function tableName()
    {
        return '{{%hypothec}}';
    }

    public function rules()
    {
        return [
            [['label', 'alias'], 'required'],
            [['published'], 'string', 'max' => 1],
            [['published'], 'default', 'value' => 1],
            [['description_text', 'benefit_text'], 'string'],
            [['bankIds', 'descriptionImage', 'benefitImage'], 'safe'],
            [['city_id'], 'exist', 'targetClass' => CommonCity::class, 'targetAttribute' => 'id'],
            [['manager_id'], 'exist', 'targetClass' => CommonManager::class, 'targetAttribute' => 'id'],
            [[
                'city_id', 'manager_id', 'default_apartment_price',
                'default_time_term', 'min_term', 'max_term', 'default_initial_fee', 'view_count'
            ], 'integer'],
            [[
                'label', 'alias', 'description_label', 'benefit_label',
                'form_calc_onsubmit', 'form_bottom_onsubmit',
                'benefits_block_label', 'steps_block_label', 'sign'
            ], 'string', 'max' => 255],
            [['notification_email'], 'trim'],
            [['notification_email'], EmailsInStringValidator::class],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'hypothec'),
            'city_id' => bt('City', 'hypothec'),
            'manager_id' => bt('Manager', 'hypothec'),
            'label' => bt('Label', 'hypothec'),
            'alias' => bt('Alias', 'hypothec'),
            'descriptionImage' => bt('Description image', 'hypothec'),
            'description_label' => bt('Description label', 'hypothec'),
            'description_text' => bt('Description text', 'hypothec'),
            'benefitImage' => bt('Benefit image', 'hypothec'),
            'benefit_label' => bt('Benefit label', 'hypothec'),
            'benefit_text' => bt('Benefit text', 'hypothec'),
            'default_apartment_price' => bt('Apartment price in rub', 'hypothec'),
            'default_time_term' => bt('Time term in months', 'hypothec'),
            'default_initial_fee' => bt('Initial fee percent', 'hypothec'),
            'benefits_block_label' => bt('Benefits block label', 'hypothec'),
            'steps_block_label' => bt('Steps block label', 'hypothec'),
            'form_calc_onsubmit' => bt('Calculator form OnSubmit event', 'hypothec'),
            'form_bottom_onsubmit' => bt('Bottom form OnSubmit event', 'hypothec'),
            'view_count' => bt('View count', 'hypothec'),
            'published' => bt('Published', 'hypothec'),
            'bankIds' => bt('Banks', 'hypothec'),
            'min_term' => bt('Min term', 'hypothec'),
            'max_term' => bt('Max term', 'hypothec'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'seo' => MetaTagBehavior::class,
            'manyToManyRelation' => [
                'class' => ManyToManyBehavior::class,
                'currentObj' => $this,
                'currentRelatedColumn' => 'hypothec_id',
                'manyToManyConfig' => [
                    'banks' => [
                        'attribute' => 'bankIds',
                        'valueAttribute' => 'bankIdValues',
                        'relatedColumn' => 'bank_id',
                        'linkTableName' => BankToHypothec::tableName(),
                        'relatedObjTableName' => Bank::tableName(),
                        'isStringReturn' => false,
                        'labelColumn' => 'label'
                    ],
                ]
            ],
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function getTitle()
    {
        return bt('Hypothec', 'hypothec');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => SerialColumn::class],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return '<b>' . optional($model->city)->label . '</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true]],
                        'filterInputOptions' => ['placeholder' => bt('Any city', 'site-menu')],
                    ],
                    // 'manager_id',
                    'label',
                    'alias',
                    // 'description_label',
                    // 'description_text:ntext',
                    // 'benefit_label',
                    // 'benefit_text:ntext',
                    // 'default_apartment_price',
                    // 'default_time_term:datetime',
                    // 'default_initial_fee',
                    // 'form_calc_onsubmit',
                    // 'form_bottom_onsubmit',
                    // 'view_count',
                    'published:boolean',
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                return StylingActionColumn::createPreviewUrl($url, $model, 'hypothec', false);
                            }
                        ]
                    ]
                ];
            case 'view':
                return [
                    'id',
                    'city_id',
                    'manager_id',
                    'label',
                    'alias',
                    'description_label',
                    'description_text:html',
                    'benefit_label',
                    'benefit_text:html',
                    'default_apartment_price',
                    'default_time_term:datetime',
                    'default_initial_fee',
                    'benefits_block_label',
                    'steps_block_label',
                    'form_calc_onsubmit',
                    'form_bottom_onsubmit',
                    'view_count',
                    'published:boolean',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new HypothecSearch();
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General', 'hypothec') => [
                    'label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                            'class' => $this->isNewRecord ? 's_name form-control' : 'form-control'
                        ],
                    ],
                    'alias' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                            'class' => $this->isNewRecord ? 's_alias form-control' : 'form-control'
                        ],
                    ],
                    'city_id' => [
                        'type' => FormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => CommonCity::getItems(),
                        'options' => ['prompt' => ''],
                    ],
                    'benefits_block_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                        'hint' => 'Используйте &lt;span&gt; тег чтобы выделить слово',
                    ],
                    'steps_block_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'published' => [
                        'type' => FormBuilder::INPUT_CHECKBOX,
                    ],
                ],
                bt('Top block', 'hypothec') => [
                    'descriptionImage' => [
                        'type' => FormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'multiple' => false,
                            'attribute' => 'descriptionImage',
                            'aspectRatio' => false,
                            'saveAttribute' => CommonHypothec::ATTR_DESCRIPTION_IMAGE,
                        ]),
                    ],
                    'description_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'description_text' => [
                        'type' => FormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::class,
                        'options' => ['model' => $this, 'attribute' => 'description_text']
                    ],
                ],
                bt('Calculator', 'hypothec') => [
                    'default_apartment_price' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getCurrencyText()],
                    ],
                    'default_time_term' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getMonthsText()],
                    ],
                    'default_initial_fee' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getPercentText()],
                    ],
                    'min_term'=> [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getMonthsText()],
                    ],
                    'max_term'=> [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true, 'iconRightContent' => CommonDataModel::getMonthsText()],
                    ],
                    'bankIds' => [
                        'type' => FormBuilder::INPUT_RAW,
                        'label' => $this->getAttributeLabel('bankIds'),
                        'value' => Select2Helper::getAjaxSelect2Widget(
                            $this, 'bankIds', Bank::getBanksUrl(), $this->bankIdValues, ['multiple' => true]
                        )
                    ],
                    'notification_email' => [
                        'type' => FormBuilder::INPUT_TEXTAREA,
                        'options' => ['rows' => 6]
                    ],
                    'form_calc_onsubmit' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                ],
                bt('Benefit custom block', 'hypothec') => [
                    'benefitImage' => [
                        'type' => FormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'multiple' => false,
                            'attribute' => 'benefitImage',
                            'aspectRatio' => false,
                            'saveAttribute' => CommonHypothec::ATTR_BENEFIT_IMAGE,
                        ]),
                    ],
                    'benefit_label' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                    'benefit_text' => [
                        'type' => FormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::class,
                        'options' => ['model' => $this, 'attribute' => 'benefit_text']
                    ],
                ],
                bt('Benefits block', 'hypothec') => [
                    $this->getRelatedFormConfig()['benefit_blocks']
                ],
                bt('Steps block', 'hypothec') => [
                    $this->getRelatedFormConfig()['step_blocks']
                ],
                bt('Bottom form', 'hypothec') => [
                    'manager_id' => [
                        'type' => FormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => CommonManager::getItems('id', 'fio'),
                        'options' => ['prompt' => ''],
                    ],
                    'form_bottom_onsubmit' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => ['maxlength' => true],
                    ],
                ],
                bt('Menu') => [
                    'separator' => StdInput::separator('Меню'),
                    $this->getRelatedFormConfig()['sub_menu'],
                ],
            ],
        ];
    }

    public function getRelatedFormConfig()
    {
        return [
            'benefit_blocks' => ['relation' => 'benefitBlocks'],
            'step_blocks' => ['relation' => 'stepBlocks'],
            'sub_menu' => ['relation' => 'subMenus']
        ];
    }

    public function getSubMenus()
    {
        return $this->hasMany(HypotecSubMenu::class, ['object_id' => 'id'])
            ->orderBy(['position' => SORT_ASC])
            ->onCondition(['type' => HypotecSubMenu::TYPE_HYPOTEC]);
    }

    public function getBenefitBlocks()
    {
        return $this->hasMany(HypothecBenefit::class, ['object_id' => 'id'])
            ->onCondition(['type' => HypothecBenefit::HYPOTHEC_BENEFIT_BLOCK]);
    }

    public function getStepBlocks()
    {
        return $this->hasMany(HypothecStep::class, ['object_id' => 'id'])
            ->onCondition(['type' => HypothecStep::HYPOTHEC_STEP_BLOCK]);
    }
}
