<?php

namespace backend\modules\page\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * InstallmentPlanSearch represents the model behind the search form about `InstallmentPlan`.
 */
class InstallmentPlanSearch extends InstallmentPlan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'manager_id', 'default_apartment_price', 'default_time_term', 'default_initial_fee', 'view_count'], 'integer'],
            [['label', 'alias', 'description_label', 'description_text', 'form_calc_onsubmit', 'form_bottom_onsubmit', 'published'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InstallmentPlanSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'manager_id' => $this->manager_id,
            'default_apartment_price' => $this->default_apartment_price,
            'default_time_term' => $this->default_time_term,
            'default_initial_fee' => $this->default_initial_fee,
            'view_count' => $this->view_count,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'description_label', $this->description_label])
            ->andFilterWhere(['like', 'description_text', $this->description_text])
            ->andFilterWhere(['like', 'form_calc_onsubmit', $this->form_calc_onsubmit])
            ->andFilterWhere(['like', 'form_bottom_onsubmit', $this->form_bottom_onsubmit])
            ->andFilterWhere(['like', 'published', $this->published]);

        return $dataProvider;
    }
}
