<?php

namespace backend\modules\page\models;

use backend\modules\contacts\models\City;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * HomeSearch represents the model behind the search form about `Home`.
 */
class HomeSearch extends Home
{

    public static function getAdditionalHeaderConfig()
    {
        $cityCount = City::find()
            ->where([
                'published' => 1
            ])
            ->count();

        $modelCount = Home::find()
            ->where([
                'published' => 1
            ])
            ->count();

        if ($cityCount == 0){
            $filledCities = '-';
        }
        else{
            $filledCities = round($modelCount / $cityCount * 100).'%';
        }


        return [
            [
                'class' => 'regular',
                'leftValue' => $filledCities,
                'rightValue' => \Yii::t('back/home', 'Filled cities'),
                'link' => '',
            ],


        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'published', 'city_id'], 'integer'],
            [
                [
                    'top_screen_title',
                    'top_screen_btn_label',
                    'top_screen_btn_link',
                    'about_company_title',
                    'about_company_desc',
                    'about_company_link_label',
                    'about_company_link',
                    'how_we_build_title',
                    'apartment_complex_title',
                    'apartment_complex_subtitle',
                    'apartment_complex_description',
                    'apartment_complex_link_label',
                    'promo_title',
                    'promo_link_label',
                    'review_title',
                    'final_cta_title',
                    'final_cta_description',
                    'final_cta_btn_label',
                    'final_cta_btn_link',
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HomeSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'published' => $this->published,
            'city_id' => $this->city_id,
        ]);

        $query->andFilterWhere(['like', 'top_screen_title', $this->top_screen_title])
            ->andFilterWhere(['like', 'top_screen_btn_label', $this->top_screen_btn_label])
            ->andFilterWhere(['like', 'top_screen_btn_link', $this->top_screen_btn_link])
            ->andFilterWhere(['like', 'about_company_title', $this->about_company_title])
            ->andFilterWhere(['like', 'about_company_desc', $this->about_company_desc])
            ->andFilterWhere(['like', 'about_company_link_label', $this->about_company_link_label])
            ->andFilterWhere(['like', 'about_company_link', $this->about_company_link])
            ->andFilterWhere(['like', 'how_we_build_title', $this->how_we_build_title])
            ->andFilterWhere(['like', 'apartment_complex_title', $this->apartment_complex_title])
            ->andFilterWhere(['like', 'apartment_complex_subtitle', $this->apartment_complex_subtitle])
            ->andFilterWhere(['like', 'apartment_complex_description', $this->apartment_complex_description])
            ->andFilterWhere(['like', 'apartment_complex_link_label', $this->apartment_complex_link_label])
            ->andFilterWhere(['like', 'promo_title', $this->promo_title])
            ->andFilterWhere(['like', 'promo_link_label', $this->promo_link_label])
            ->andFilterWhere(['like', 'review_title', $this->review_title])
            ->andFilterWhere(['like', 'final_cta_title', $this->final_cta_title])
            ->andFilterWhere(['like', 'final_cta_description', $this->final_cta_description])
            ->andFilterWhere(['like', 'final_cta_btn_label', $this->final_cta_btn_label])
            ->andFilterWhere(['like', 'final_cta_btn_link', $this->final_cta_btn_link]);

        return $dataProvider;
    }
}
