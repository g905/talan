<?php

namespace backend\modules\page\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%about_company_stat}}".
 *
 * @property integer $id
 * @property integer $about_company_id
 * @property string $number
 * @property string $label
 * @property string $content
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AboutCompany $aboutCompany
 */
class AboutCompanyStat extends ActiveRecord implements BackendModel
{

    public function init()
    {
        $this->published = 1;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%about_company_stat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['about_company_id', 'published', 'position'], 'integer'],
            [['number', 'label', 'content'], 'required'],
            [['number', 'label', 'content'], 'string', 'max' => 255],
            [['about_company_id'], 'exist', 'targetClass' => \common\models\AboutCompany::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/about-company-stat', 'ID'),
            'about_company_id' => Yii::t('back/about-company-stat', 'About Company ID'),
            'number' => Yii::t('back/about-company-stat', 'Number'),
            'label' => Yii::t('back/about-company-stat', 'Label'),
            'content' => Yii::t('back/about-company-stat', 'Content'),
            'published' => Yii::t('back/about-company-stat', 'Published'),
            'position' => Yii::t('back/about-company-stat', 'Position'),
            'created_at' => Yii::t('back/about-company-stat', 'Created At'),
            'updated_at' => Yii::t('back/about-company-stat', 'Updated At'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompany()
    {
        return $this->hasOne(AboutCompany::className(), ['id' => 'about_company_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('about-company-stat', 'About Company Stat');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'about_company_id',
                    'number',
                    'label',
                    'content',
                    'published:boolean',
                    'position',
                    ['class' => \backend\components\StylingActionColumn::className()],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'about_company_id',
                    'number',
                    'label',
                    'content',
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new AboutCompanyStatSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'number' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'label' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'content' => [
                'type' => \backend\components\FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }


}
