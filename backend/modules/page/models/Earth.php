<?php

namespace backend\modules\page\models;

use backend\behaviors\linker\LinkerBehavior;
use backend\components\FormBuilder;
use backend\components\ImperaviContent;
use backend\components\StylingActionColumn;
use backend\helpers\StdInput;
use backend\modules\block\models\block\EarthAdvantage;
use backend\modules\block\models\block\EarthCompany;
use backend\modules\block\models\block\EarthPlan;
use backend\modules\contacts\models\Manager;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\modules\menu\models\EarthSubMenu;
use backend\validators\EmailsInStringValidator;
use common\models\ManagerAssign;
use kartik\grid\GridView;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\models\Earth as CommonEarth;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use common\models\City;
use common\models\EntityToFile;
use yii\helpers\Html;

/**
 * Class Earth
 * @package backend\modules\page\models
 */
class Earth extends CommonEarth implements BackendModel
{
    /**
     * @var mixed attributes for imageUploader.
     */
    public $top_screen_background_image;
    public $top_screen_background_video;
    public $model_image;
    public $boss_image;

    /**
     * Temporary sign which used for saving images before model save
     * @var string
     */
    public $sign;

    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = security()->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%earth}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'map_lat', 'map_lng'], 'required'],
            [['city_id', 'map_zoom'], 'integer'],
            [['map_lat', 'map_lng'], 'number'],
            [['top_screen_description', 'company_description', 'map_demands', 'boss_description', 'boss_video', 'model_description', 'form_onsubmit'], 'string'],
            [['top_screen_title', 'presentation_link', 'company_title', 'map_title', 'map_subtitle', 'plan_title', 'boss_title', 'boss_name', 'boss_position', 'offer_title_light', 'offer_title_dark', 'offer_link', 'advantages_title', 'model_title', 'manager_title'], 'string', 'max' => 255],
            [['published'], 'string', 'max' => 1],
            [['sign'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'targetClass' => City::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['managersField'], 'safe'],
            [['notification_email'], 'trim'],
            [['notification_email'], EmailsInStringValidator::class],
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'seo' => [
                'class' => MetaTagBehavior::className(),
            ],
            'linker' => [
                'class' => LinkerBehavior::class,
                'relations' => [
                    'managersField' => [
                        'managers',
                        'updater' => [
                            // 'class' => ManyToManySmartUpdater::class,
                            'viaTableAttributesValue' => [
                                'type' => ManagerAssign::TYPE_EARTH_MANAGER,
                                'position' => function () {
                                    static $i = 0;
                                    return ++$i;
                                }
                            ],
                            'viaTableCondition' => [
                                'type' => ManagerAssign::TYPE_EARTH_MANAGER,
                            ],
                        ],
                        'get' => function ($value) {
                            return implode(',', $value);
                        },
                        'set' => function ($value) {
                            return empty($value) ? [] : explode(',', $value);
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEarthMarkers()
    {
        return $this->hasMany(EarthMarker::className(), ['earth_id' => 'id']);
    }

    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Earth');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var Home $model */
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    'top_screen_title',
                    // 'top_screen_description:ntext',
                    //'presentation_link:url',
                    //'company_title',
                    // 'company_description:ntext',
                    //'map_title',
                    //'map_subtitle',
                    // 'map_demands:ntext',
                    // 'plan_title',
                    // 'boss_title',
                    // 'boss_name',
                    // 'boss_position',
                    // 'boss_description:ntext',
                    // 'offer_title_light',
                    // 'offer_title_dark',
                    // 'offer_link:url',
                    // 'advantages_title',
                    // 'model_title',
                    // 'model_description:ntext',
                    // 'manager_title',
                    'published:boolean',
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{related-clone}{preview}',
                        'buttons'  => [
                            'related-clone' => function($url, $model) {
                                return Html::a(Html::tag('span', '',['class' => 'si si-layers']), $url, ['class' => 'btn btn-xs btn-default']);
                            },
                            'preview' => function($url, $model) {
                                return StylingActionColumn::createPreviewUrl($url, $model, 'land-purchase');
                            }
                        ]
                    ]
                ];
            break;
            case 'view':
                return [
                    'id',
                    'city_id',
                    'top_screen_title',
                    [
                        'attribute' => 'top_screen_description',
                        'format' => 'html',
                    ],
                    'presentation_link:url',
                    'company_title',
                    [
                        'attribute' => 'company_description',
                        'format' => 'html',
                    ],
                    'map_title',
                    'map_subtitle',
                    [
                        'attribute' => 'map_demands',
                        'format' => 'html',
                    ],
                    'map_lat',
                    'map_lng',
                    'map_zoom',
                    'plan_title',
                    'boss_title',
                    'boss_name',
                    'boss_position',
                    [
                        'attribute' => 'boss_description',
                        'format' => 'html',
                    ],
                    'boss_video',
                    'offer_title_light',
                    'offer_title_dark',
                    'offer_link:url',
                    'advantages_title',
                    'model_title',
                    [
                        'attribute' => 'model_description',
                        'format' => 'html',
                    ],
                    'manager_title',
                    'published:boolean',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new EarthSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $mapId = uniqid();
        $config = [
            'form-set' => [
                Yii::t('back/earth', 'General') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => City::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
                Yii::t('back/PartnerPage', 'Form') => [
                    'form_onsubmit' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'notification_email' => [
                        'type' => FormBuilder::INPUT_TEXTAREA,
                        'options' => ['rows' => 6]
                    ],
                ],
                Yii::t('back/earth', 'Top screen') => [
                    'top_screen_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'top_screen_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'top_screen_description',
                        ]
                    ],
                    'presentation_link' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'top_screen_background_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'top_screen_background_image',
                            'saveAttribute' => CommonEarth::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'top_screen_background_video' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['mp4'],
                            'attribute' => 'top_screen_background_video',
                            'saveAttribute' => CommonEarth::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_VIDEO,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                Yii::t('back/earth', 'Menu') => [
                    $this->getRelatedFormConfig()['sub_menu'],
                ],
                Yii::t('back/earth', 'Company') => [
                    'company_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'company_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'company_description',
                        ]
                    ],
                    'separator' => StdInput::separator(bt('company - blocks', 'apartment-building')),
                    $this->getRelatedFormConfig()['company_blocks'],
                ],
                Yii::t('back/earth', 'Map') => [
                    'map_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'map_subtitle' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'map_demands' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'map_demands',
                        ]
                    ],
                    'map_zoom' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    [
                        'label' => false,
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div id="product_map_' . $mapId . '" class="product_map"></div>'
                    ],
                    'map_lat' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'options' => [
                            'maxlength' => true,
                            'readonly' => true,
                            'id' => 'product_map_lat_' . $mapId,
                            'class' => 'form-control',
                            'value' => $this->isNewRecord ? '56.8497600' : $this->map_lat
                        ],
                    ],
                    'map_lng' => [
                        'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                        'hint' => Yii::t(
                            'back/product-storehouse',
                            'To fill parameters of latitude and longitude, just click on the map'
                        ),
                        'options' => [
                            'readonly' => true,
                            'maxlength' => true,
                            'id' => 'product_map_lng_' . $mapId,
                            'class' => 'form-control',
                            'value' => $this->isNewRecord ? '53.2044800' : $this->map_lng
                        ],
                    ],
                    'map_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'map_image',
                            'saveAttribute' => \common\models\Earth::SAVE_ATTRIBUTE_MAP_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'separator' => StdInput::separator(bt('map - markers', 'apartment-complex')),
                    $this->getRelatedFormConfig()['earth_markers'],
                ],
                Yii::t('back/earth', 'Plan') => [
                    'plan_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'separator' => StdInput::separator(bt('plan - blocks', 'apartment-building')),
                    $this->getRelatedFormConfig()['plan_blocks'],
                ],
                Yii::t('back/earth', 'Boss') => [
                    'boss_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'boss_name' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'boss_position' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'boss_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'boss_description',
                        ]
                    ],
                    'boss_video' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'boss_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'boss_image',
                            'saveAttribute' => CommonEarth::SAVE_ATTRIBUTE_BOSS_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                Yii::t('back/earth', 'Offer') => [
                    'offer_title_light' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'offer_title_dark' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'offer_link' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                ],
                Yii::t('back/earth', 'Advantages') => [
                    'advantages_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'separator' => StdInput::separator(bt('advantages - blocks', 'apartment-building')),
                    $this->getRelatedFormConfig()['advantage_blocks'],
                ],
                Yii::t('back/earth', 'Model') => [
                    'model_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'model_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'model_description',
                        ]
                    ],
                    'model_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'model_image',
                            'saveAttribute' => CommonEarth::SAVE_ATTRIBUTE_MODEL_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                Yii::t('back/earth', 'Managers') => [
                    'manager_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'managersField' => StdInput::relationInput(Manager::class, 3, 'id', 'fio'),
                ],
            ],
        ];

        return $config;
    }

    public function getRelatedFormConfig()
    {
        return [
            'earth_markers' => [
                'relation' => 'earthMarkers',
            ],
            'advantage_blocks' => [
                'relation' => 'advantageBlocks'
            ],
            'company_blocks' => [
                'relation' => 'companyBlocks'
            ],
            'plan_blocks' => [
                'relation' => 'planBlocks'
            ],
            'sub_menu' => [
                'relation' => 'subMenus'
            ],
        ];
    }

    public function getAdvantageBlocks()
    {
        return $this->hasMany(EarthAdvantage::class, ['object_id' => 'id'])
            ->onCondition(['type' => EarthAdvantage::EARTH_ADVANTAGE]);
    }

    public function getCompanyBlocks()
    {
        return $this->hasMany(EarthCompany::class, ['object_id' => 'id'])
            ->onCondition(['type' => EarthCompany::EARTH_COMPANY]);
    }

    public function getPlanBlocks()
    {
        return $this->hasMany(EarthPlan::class, ['object_id' => 'id'])
            ->onCondition(['type' => EarthPlan::EARTH_PLAN]);
    }

    public function getManagers()
    {
        return $this->hasMany(Manager::class, ['id' => 'manager_id'])
            ->alias('m')
            ->where(['m.published' => self::IS_PUBLISHED])
            ->viaTable(ManagerAssign::tableName(), ['object_id' => 'id'], function (ActiveQuery $q) {
                $q->alias('ma')->andWhere(['ma.type' => ManagerAssign::TYPE_EARTH_MANAGER]);
            })
            ->innerJoin(ManagerAssign::tableName() . ' ma', 'ma.manager_id = m.id')
            ->orderBy(['ma.position' => SORT_ASC]);
    }

    public function getSubMenus()
    {
        return $this->hasMany(EarthSubMenu::class, ['object_id' => 'id'])
            ->onCondition(['type' => EarthSubMenu::TYPE_EARTH]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
