<?php

namespace backend\modules\page\models\publicDataWidget;

use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\modules\page\models\PublicDataWidget;

/**
 * Class ServiceChartRow
 *
 * @package backend\modules\page\models\publicDataWidget
 */
class ServiceChartRow extends PublicDataWidget
{
    public function init()
    {
        parent::init();
        $this->setAttribute('type', self::TYPE_SERVICE_CHART_ROW);
        $this->published !== null ?: $this->setAttribute('published', true);
    }

    public function getTitle()
    {
        return bt('Service chart row', 'public-data-widget');
    }

    public function getSearchModel()
    {
        return new ServiceChartRowSearch();
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'public-data-widget'),
            'type' => bt('Type', 'public-data-widget'),
            'city_id' => bt('City ID', 'public-data-widget'),
            'value' => bt('Value', 'public-data-widget'),
            'status' => bt('Status', 'public-data-widget'),
            'chart_name' => bt('Chart name', 'public-data-widget'),
            'published' => bt('Published', 'public-data-widget'),
            'position' => bt('Position', 'public-data-widget'),
        ];
    }

    public function rules()
    {
        return [
            [['type', 'city_id', 'value', 'status', 'chart_name'], 'required'],
            [['type', 'city_id', 'published'], 'integer'],
            [['value'], 'number'],
            [['chart_name'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General') => [
                    'type' => StdInput::hidden(),
                    'city_id' => StdInput::citySelect(),
                    'value' => StdInput::text(),
                    'status' => StdInput::checkbox(),
                    'published' => StdInput::hidden(),
                    'position' => StdInput::text(),
                ],
                bt('Chart') => [
                    'chart_name' => StdInput::text(),
                    'separator' => StdInput::separator(bt('Chart')),
                    $this->getRelatedFormConfig()['chartData'],
                ]
            ],
        ];
    }

    public function getChartData()
    {
        return $this->hasMany(ServiceChartData::class, ['widget_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    StdColumn::city(),
                    'chart_name',
                    'status',
                    'value',
                    'published:boolean',
                    'position',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    // 'type',
                    'city_id',
                    'label',
                    'value',
                    'status',
                    'published:boolean',
                    'position',
                ];
            default:
                return [];
        }
    }
}
