<?php

namespace backend\modules\page\models\publicDataWidget;

use backend\helpers\StdColumn;
use backend\helpers\StdInput;
use backend\modules\apartment\models\ApartmentComplex;
use backend\modules\page\models\PublicDataWidget;

/**
 * Class DiagramDdu
 *
 * @package backend\modules\page\models\publicDataWidget
 */
class DiagramDdu extends PublicDataWidget
{
    public function init()
    {
        parent::init();
        $this->setAttribute('type', self::TYPE_DIAGRAM_DDU);
        $this->published !== null ?: $this->setAttribute('published', true);
    }

    public function getTitle()
    {
        return bt('Diagram ddu', 'public-data-widget');
    }

    public function getSearchModel()
    {
        return new DiagramDduSearch();
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'public-data-widget'),
            'type' => bt('Type', 'public-data-widget'),
            'complex_id' => bt('Complex ID', 'public-data-widget'),
            'chart_name' => bt('Chart Name', 'public-data-widget'),
            'published' => bt('Published', 'public-data-widget'),
        ];
    }

    public function rules()
    {
        return [
            [['type', 'complex_id', 'status', 'label', 'chart_name'], 'required'],
            [['type', 'complex_id', 'published'], 'integer'],
            //[['complex_id'], 'unique', 'filter' => ['type' => PublicDataWidget::TYPE_DIAGRAM_DDU]],
            [['complex_id'], 'exist', 'targetClass' => ApartmentComplex::class, 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General') => [
                    'type' => StdInput::hidden(),
                    'complex_id' => StdInput::complexSelect(),
                    'label' => StdInput::text(),
                    'chart_name' => StdInput::text(),
                ],
                bt('Chart') => [
                    'separator' => StdInput::separator(bt('Chart')),
                    $this->getRelatedFormConfig()['chartData'],
                ]
            ],
        ];
    }

    public function getChartData()
    {
        return $this->hasMany(DiagramDduChart::class, ['widget_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    StdColumn::complex(),
                    'label',
                    'chart_name',
                    'position',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    StdColumn::complex(),
                    'label',
                    'chart_name',
                    'position',
                ];
            default:
                return [];
        }
    }
}
