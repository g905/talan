<?php

namespace backend\modules\page\models\publicDataWidget;

use backend\modules\page\models\PublicDataChartData;
use backend\modules\page\models\PublicDataWidget;

/**
 * Class DiagramCircleChart
 *
 * @package backend\modules\page\models\publicDataWidget
 */
class DiagramCircleChart extends PublicDataChartData
{
    public function rules()
    {
        return [
            [['label', 'value', 'color'], 'required'],
            [['widget_id', 'position'], 'integer'],
            [['label', 'color'], 'string', 'max' => 255],
            [['widget_id'], 'exist', 'targetClass' => PublicDataWidget::class, 'targetAttribute' => 'id'],
            [['position'], 'default', 'value' => 0],
            [['value'], 'number'],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }
}
