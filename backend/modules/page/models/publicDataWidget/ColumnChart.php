<?php

namespace backend\modules\page\models\publicDataWidget;

use backend\helpers\StdColumn;
use backend\helpers\StdInput;
use backend\modules\page\models\PublicDataWidget;

/**
 * Class ColumnChart
 *
 * @package backend\modules\page\models\publicDataWidget
 */
class ColumnChart extends PublicDataWidget
{
    public function init()
    {
        parent::init();
        $this->setAttribute('type', self::TYPE_COLUMN_CHART);
        $this->published !== null ?: $this->setAttribute('published', true);
    }

    public function getTitle()
    {
        return bt('Column chart', 'public-data-widget');
    }

    public function getSearchModel()
    {
        return new ColumnChartSearch();
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'public-data-widget'),
            'label' => bt('Label', 'public-data-widget'),
            'type' => bt('Type', 'public-data-widget'),
            'status' => bt('Status', 'public-data-widget'),
            'published' => bt('Published', 'public-data-widget'),
            'position' => bt('Position', 'public-data-widget'),
        ];
    }

    public function rules()
    {
        return [
            [['type', 'status'], 'required'],
            [['type', 'published', 'position'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General') => [
                    'type' => StdInput::hidden(),
                    'label' => StdInput::text(),
                    'status' => StdInput::checkbox(),
                    'published' => StdInput::hidden(),
                    'position' => StdInput::text(),
                ],
                bt('Chart') => [
                    'separator' => StdInput::separator(bt('Chart')),
                    $this->getRelatedFormConfig()['chartData'],
                ]
            ],
        ];
    }

    public function getChartData()
    {
        return $this->hasMany(ColumnRowChart::class, ['widget_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    'label',
                    'published:boolean',
                    'position',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    // 'type',
                    'label',
                    'published:boolean',
                    'position',
                ];
            default:
                return [];
        }
    }
}
