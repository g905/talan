<?php

namespace backend\modules\page\models\publicDataWidget;

use backend\helpers\StdInput;
use backend\modules\page\models\PublicDataChartData;
use backend\modules\page\models\PublicDataWidget;

/**
 * Class RatingTable
 *
 * @package backend\modules\page\models\publicDataWidget
 */
class RatingTable extends PublicDataChartData
{
    public function rules()
    {
        return [
            [['label', 'value'], 'required'],
            [['widget_id'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['value'], 'number'],
            [['widget_id'], 'exist', 'targetClass' => PublicDataWidget::class, 'targetAttribute' => 'id'],
            [['position'], 'default', 'value' => 0],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function getFormConfig()
    {
        return [
            'widget_id' => StdInput::hidden(),
            'label' => StdInput::text(),
            'value' => StdInput::text(),
            'position' => StdInput::hidden(),
        ];
    }
}
