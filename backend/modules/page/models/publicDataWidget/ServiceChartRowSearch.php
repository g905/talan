<?php

namespace backend\modules\page\models\publicDataWidget;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class ServiceChartRowSearch
 *
 * @package backend\modules\page\models\publicDataWidget
 */
class ServiceChartRowSearch extends ServiceChartRow
{
    public function init()
    {
        parent::init();
        $this->setAttribute('type', self::TYPE_SERVICE_CHART_ROW);
        $this->published !== null ?: $this->setAttribute('published', true);
    }

    public function rules()
    {
        return [
            [['id', 'type', 'city_id', 'plan', 'fact', 'published', 'position'], 'integer'],
            [['label', 'chart_name', 'link', 'value', 'status', 'placed', 'seria'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = self::find();
        $dataProvider = new ActiveDataProvider(compact('query'));
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'city_id' => $this->city_id,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'chart_name', $this->chart_name])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
