<?php

namespace backend\modules\page\models\publicDataWidget;

use backend\helpers\StdColumn;
use backend\helpers\StdInput;
use backend\modules\page\models\PublicDataWidget;

/**
 * Class DiagramSchedule
 *
 * @package backend\modules\page\models\publicDataWidget
 */
class DiagramSchedule extends PublicDataWidget
{
    public $label_plan;
    public $label_fact;

    public function init()
    {
        parent::init();
        $this->setAttribute('type', self::TYPE_DIAGRAM_SCHEDULE);
        $this->published !== null ?: $this->setAttribute('published', true);
    }

    public function getTitle()
    {
        return bt('Diagram Schedule', 'public-data-widget');
    }

    public function getSearchModel()
    {
        return new DiagramScheduleSearch();
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'public-data-widget'),
            'type' => bt('Type', 'public-data-widget'),
            'complex_id' => bt('Complex ID', 'public-data-widget'),
            'chart_name' => bt('Chart Name', 'public-data-widget'),
            'value' => bt('Value', 'public-data-widget'),
        ];
    }

    public function rules()
    {
        return [
            [['type', 'complex_id', 'status', 'label_plan', 'label_fact', 'label', 'plan', 'fact'], 'required'],
            [['type', 'complex_id', 'published'], 'integer'],
            [['plan', 'fact'], 'number', 'max' => 100],
            ['complex_id', 'unique', 'filter' => ['type' => PublicDataWidget::TYPE_DIAGRAM_SCHEDULE]],
            [['position'], 'default', 'value' => 0],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General') => [
                    'type' => StdInput::hidden(),
                    'complex_id' => StdInput::complexSelect(),
                    'label' => StdInput::text(),
                    'label_plan' => StdInput::text(),
                    'label_fact' => StdInput::text(),
                    'plan' => StdInput::text(),
                    'fact' => StdInput::text(),
                ],
                bt('Chart') => [
                    'separator' => StdInput::separator(bt('Chart')),
                    $this->getRelatedFormConfig()['chartData'],
                ]
            ],
        ];
    }

    public function getChartData()
    {
        return $this->hasMany(DiagramScheduleChart::class, ['widget_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    StdColumn::complex(),
                    'label',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    StdColumn::complex(),
                    'label',
                ];
            default:
                return [];
        }
    }

    public function beforeSave($insert)
    {
        $parent = parent::beforeSave($insert);
        if ($parent) {
            $this->chart_name = implode(',', [$this->label_plan, $this->label_fact]);
        }
        return $parent;
    }

    public function afterFind()
    {
        $labels = explode(',', $this->chart_name);
        $this->label_plan = obtain(0, $labels);
        $this->label_fact = obtain(1, $labels);
    }
}
