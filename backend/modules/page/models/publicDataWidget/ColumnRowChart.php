<?php

namespace backend\modules\page\models\publicDataWidget;

use backend\helpers\StdColumn;
use backend\helpers\StdInput;
use backend\modules\page\models\PublicDataChartData;
use backend\modules\page\models\PublicDataWidget;

/**
 * Class DiagramRowChart
 *
 * @package backend\modules\page\models\publicDataWidget
 */
class ColumnRowChart extends PublicDataChartData
{
    public $col1value;
    public $col2value;
    public $col3value;

    public function rules()
    {
        return [
            [['label'], 'required'],
            [['widget_id'], 'integer'],
            [['label', 'color'], 'string', 'max' => 255],
            [['col1value', 'col2value', 'col3value'], 'number', 'min' => 0],
            [['widget_id'], 'exist', 'targetClass' => PublicDataWidget::class, 'targetAttribute' => 'id'],
            [['position'], 'default', 'value' => 0],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function getFormConfig()
    {
        return [
            'widget_id' => StdInput::hidden(),
            'label' => StdInput::monthPicker(),
            'col1value' => StdInput::text(),
            'col2value' => StdInput::text(),
            'col3value' => StdInput::text(),
            'position' => StdInput::hidden(),
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'public-data-widget'),
            'widget_id' => bt('Widget', 'public-data-widget'),
            'col1value' => bt('Column 1 value', 'public-data-widget'),
            'col2value' => bt('Column 2 value', 'public-data-widget'),
            'col3value' => bt('Column 3 value', 'public-data-widget'),
            'position' => bt('Position', 'public-data-widget'),
        ];
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    'widget_id',
                    'label',
                    'value',
                    'color',
                    'position',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    'widget_id',
                    'label',
                    'value',
                    'color',
                    'position',
                ];
            default:
                return [];
        }
    }

    public function beforeSave($insert)
    {
        $parent = parent::beforeSave($insert);
        if ($parent) {
            $this->value = implode(',', [$this->col1value, $this->col2value, $this->col3value]);
        }

        return $parent;
    }

    public function afterFind()
    {
        $values = explode(',', $this->value);

        $this->col1value = obtain(0, $values);
        $this->col2value = obtain(1, $values);
        $this->col3value = obtain(2, $values);
    }
}
