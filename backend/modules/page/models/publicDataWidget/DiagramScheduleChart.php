<?php

namespace backend\modules\page\models\publicDataWidget;

use backend\helpers\StdInput;
use backend\modules\page\models\PublicDataChartData;
use backend\modules\page\models\PublicDataWidget;

/**
 * Class DiagramScheduleChart
 *
 * @package backend\modules\page\models\publicDataWidget
 */
class DiagramScheduleChart extends PublicDataChartData
{
    public $label_plan;
    public $label_fact;
    public $value_plan;
    public $value_fact;

    public function rules()
    {
        return [
            [['label_plan', 'label_fact', 'value_plan', 'value_fact', 'color'], 'required'],
            [['widget_id'], 'integer'],
            [['value_plan', 'value_fact'], 'number', 'max' => 100],
            [['widget_id'], 'exist', 'targetClass' => PublicDataWidget::class, 'targetAttribute' => 'id'],
            [['position'], 'default', 'value' => 0],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'public-data-widget'),
            'widget_id' => bt('Widget', 'public-data-widget'),
            'label' => bt('Label', 'public-data-widget'),
            'value' => bt('Value', 'public-data-widget'),
            'color' => bt('Label', 'public-data-widget'),
            'position' => bt('Position', 'public-data-widget'),
        ];
    }

    public function getFormConfig()
    {
        return [
            'widget_id' => StdInput::hidden(),
            'color' => StdInput::text(),
            'label_plan' => StdInput::text(),
            'label_fact' => StdInput::text(),
            'value_plan' => StdInput::text(),
            'value_fact' => StdInput::text(),
            'position' => StdInput::hidden(),
        ];
    }

    public function beforeSave($insert)
    {
        $parent = parent::beforeSave($insert);
        if ($parent) {
            $this->label = implode(',', [$this->label_plan, $this->label_fact]);
            $this->value = implode(',', [$this->value_plan, $this->value_fact]);
        }
        return $parent;
    }

    public function afterFind()
    {
        $labels = explode(',', $this->label);
        $values = explode(',', $this->value);
        $this->label_plan = obtain(0, $labels);
        $this->label_fact = obtain(1, $labels);
        $this->value_plan = obtain(0, $values);
        $this->value_fact = obtain(1, $values);
    }
}
