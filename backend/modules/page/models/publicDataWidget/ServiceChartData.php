<?php

namespace backend\modules\page\models\publicDataWidget;

use backend\helpers\StdInput;
use backend\modules\page\models\PublicDataChartData;
use backend\modules\page\models\PublicDataWidget;

/**
 * Class DiagramRowChart
 *
 * @package backend\modules\page\models\publicDataWidget
 */
class ServiceChartData extends PublicDataChartData
{
    public function getTitle()
    {
        return bt('Service chart data', 'public-data-widget');
    }

    public function rules()
    {
        return [
            [['label', 'value'], 'required'],
            [['widget_id'], 'integer'],
            [['label', 'color'], 'string', 'max' => 255],
            [['widget_id'], 'exist', 'targetClass' => PublicDataWidget::class, 'targetAttribute' => 'id'],
            [['position'], 'default', 'value' => 0],
            [['value'], 'number'],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function getFormConfig()
    {
        return [
            'widget_id' => StdInput::hidden(),
            'label' => StdInput::monthPicker(),
            'value' => StdInput::text(),
            'position' => StdInput::hidden(),
        ];
    }
}
