<?php

namespace backend\modules\page\models\publicDataWidget;

use backend\helpers\StdColumn;
use backend\helpers\StdInput;
use backend\modules\page\models\PublicDataWidget;

/**
 * Class DiagramRow
 *
 * @package backend\modules\page\models\publicDataWidget
 */
class DiagramRow extends PublicDataWidget
{
    public function init()
    {
        parent::init();
        $this->setAttribute('type', self::TYPE_DIAGRAM_ROW);
        $this->published !== null ?: $this->setAttribute('published', true);
    }

    public function getTitle()
    {
        return bt('Diagram row', 'public-data-widget');
    }

    public function getSearchModel()
    {
        return new DiagramRowSearch();
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'public-data-widget'),
            'type' => bt('Type', 'public-data-widget'),
            'city_id' => bt('City ID', 'public-data-widget'),
            'value' => bt('Value', 'public-data-widget'),
            'status' => bt('Status', 'public-data-widget'),
            'published' => bt('Published', 'public-data-widget'),
            'position' => bt('Position', 'public-data-widget'),
        ];
    }

    public function rules()
    {
        return [
            [['type', 'city_id', 'value', 'status'], 'required'],
            [['type', 'city_id', 'published', 'position'], 'integer'],
            [['value'], 'number'],
            [['status'], 'string', 'max' => 1],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General') => [
                    'type' => StdInput::hidden(),
                    'city_id' => StdInput::citySelect(),
                    'value' => StdInput::text(),
                    'status' => StdInput::checkbox(),
                    'published' => StdInput::hidden(),
                    'position' => StdInput::text(),
                ],
                bt('Chart') => [
                    'separator' => StdInput::separator(bt('Chart')),
                    $this->getRelatedFormConfig()['chartData'],
                ]
            ],
        ];
    }

    public function getChartData()
    {
        return $this->hasMany(DiagramRowChart::class, ['widget_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    StdColumn::city(),
                    'value',
                    StdColumn::arrowStatus(),
                    'published:boolean',
                    'position',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    // 'type',
                    StdColumn::city(),
                    // 'label',
                    'value',
                    StdColumn::arrowStatus(),
                    'status',
                    'published:boolean',
                    'position',
                ];
            default:
                return [];
        }
    }
}
