<?php

namespace backend\modules\page\models\publicDataWidget;

use backend\helpers\StdColumn;
use backend\helpers\StdInput;
use backend\modules\contacts\models\City;
use backend\modules\page\models\PublicDataWidget;

/**
 * Class Rating
 *
 * @package backend\modules\page\models\publicDataWidget
 */
class Rating extends PublicDataWidget
{
    public function init()
    {
        parent::init();
        $this->setAttribute('type', self::TYPE_RATING);
        $this->published !== null ?: $this->setAttribute('published', true);
    }

    public function getTitle()
    {
        return bt('Rating table', 'public-data-widget');
    }

    public function getSearchModel()
    {
        return new RatingSearch();
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'public-data-widget'),
            'type' => bt('Type', 'public-data-widget'),
            'city_id' => bt('City ID', 'public-data-widget'),
            'value' => bt('Table', 'public-data-widget'),
            'published' => bt('Published', 'public-data-widget'),
        ];
    }

    public function rules()
    {
        return [
            [['type', 'city_id', 'status', 'label', 'value'], 'required'],
            [['type', 'city_id', 'published'], 'integer'],
            // [['city_id'], 'unique', 'filter' => ['type' => PublicDataWidget::TYPE_RATING]],
            [['city_id'], 'exist', 'targetClass' => City::class, 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General') => [
                    'type' => StdInput::hidden(),
                    'city_id' => StdInput::citySelect(),
                    'label' => StdInput::text(),
                    'value' => StdInput::editorTableOnly(),
                ],
                /*bt('Chart') => [
                    'separator' => StdInput::separator(bt('Chart')),
                    $this->getRelatedFormConfig()['chartData'],
                ]*/
            ],
        ];
    }

    /*public function getChartData()
    {
        return $this->hasMany(RatingTable::class, ['widget_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }*/

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    StdColumn::city(),
                    'label',
                    // 'value',
                    'position',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    // StdColumn::complex(),
                    'label',
                    'value:html',
                    'position',
                ];
            default:
                return [];
        }
    }
}
