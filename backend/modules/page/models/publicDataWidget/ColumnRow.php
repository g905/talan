<?php

namespace backend\modules\page\models\publicDataWidget;

use backend\helpers\StdColumn;
use backend\helpers\StdInput;
use backend\modules\page\models\PublicDataWidget;

/**
 * Class ColumnRow
 *
 * @package backend\modules\page\models\publicDataWidget
 */
class ColumnRow extends PublicDataWidget
{
    public $colName1;
    public $colName2;
    public $colName3;

    public $colColor1;
    public $colColor2;
    public $colColor3;

    public function init()
    {
        parent::init();
        $this->setAttribute('type', self::TYPE_COLUMN_CHART_ROW);
        $this->published !== null ?: $this->setAttribute('published', true);
    }

    public function getTitle()
    {
        return bt('Column chart row', 'public-data-widget');
    }

    public function getSearchModel()
    {
        return new ColumnRowSearch();
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'public-data-widget'),
            'type' => bt('Type', 'public-data-widget'),
            'city_id' => bt('City ID', 'public-data-widget'),
            'value' => bt('Value', 'public-data-widget'),
            'status' => bt('Status', 'public-data-widget'),
            'colName1' => bt('Column 1 name', 'public-data-widget'),
            'colName2' => bt('Column 2 name', 'public-data-widget'),
            'colName3' => bt('Column 3 name', 'public-data-widget'),
            'colColor1' => bt('Column 1 color', 'public-data-widget'),
            'colColor2' => bt('Column 2 color', 'public-data-widget'),
            'colColor3' => bt('Column 3 color', 'public-data-widget'),
            'published' => bt('Published', 'public-data-widget'),
            'position' => bt('Position', 'public-data-widget'),
        ];
    }

    public function rules()
    {
        return [
            [['type', 'city_id', 'value', 'status'], 'required'],
            [['type', 'city_id', 'published', 'position'], 'integer'],
            [['value'], 'number', 'max' => 2147483647, 'min' => -2147483648],
            [['status'], 'string', 'max' => 1],
            [['colName1', 'colName2', 'colName3'], 'string', 'max' => 255 / 3],
            [['colColor1', 'colColor2', 'colColor3'], 'string', 'max' => 255 / 3],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['position'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
        ];
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General') => [
                    'type' => StdInput::hidden(),
                    'city_id' => StdInput::citySelect(),
                    'value' => StdInput::text(),
                    'status' => StdInput::checkbox(),
                    'published' => StdInput::hidden(),
                    'position' => StdInput::text(),
                ],
                bt('Chart') => [
                    'colName1' => StdInput::text(),
                    'colColor1' => StdInput::colorSelect(),
                    'colName2' => StdInput::text(),
                    'colColor2' => StdInput::colorSelect(),
                    'colName3' => StdInput::text(),
                    'colColor3' => StdInput::colorSelect(),
                    'separator' => StdInput::separator(bt('Chart')),
                    $this->getRelatedFormConfig()['chartData'],
                ]
            ],
        ];
    }

    public function getChartData()
    {
        return $this->hasMany(ColumnRowChart::class, ['widget_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    StdColumn::city(),
                    'value',
                    StdColumn::arrowStatus(),
                    'published:boolean',
                    'position',
                    StdColumn::actionColumn(),
                ];
            case 'view':
                return [
                    'id',
                    // 'type',
                    StdColumn::city(),
                    // 'label',
                    'value',
                    StdColumn::arrowStatus(),
                    'status',
                    'published:boolean',
                    'position',
                ];
            default:
                return [];
        }
    }

    public function beforeSave($insert)
    {
        $parent = parent::beforeSave($insert);
        if ($parent) {
            $this->chart_name = implode(',', [$this->colName1, $this->colName2, $this->colName3]);
            $this->seria = implode('|', [$this->colColor1, $this->colColor2, $this->colColor3]);
        }

        return $parent;
    }

    public function afterFind()
    {
        $colNames = explode(',', $this->chart_name);
        $colors = explode('|', $this->seria);

        $this->colName1 = obtain(0, $colNames);
        $this->colName2 = obtain(1, $colNames);
        $this->colName3 = obtain(2, $colNames);

        $this->colColor1 = obtain(0, $colors);
        $this->colColor2 = obtain(1, $colors);
        $this->colColor3 = obtain(2, $colors);
    }
}
