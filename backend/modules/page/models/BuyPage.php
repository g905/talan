<?php

namespace backend\modules\page\models;

use backend\components\StylingActionColumn;
use backend\modules\block\models\block\BuyAdvantage;
use backend\modules\block\models\block\BuyStep;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\modules\menu\models\BuySubMenu;
use common\models\EntityToFile;
use common\models\Manager;
use kartik\grid\GridView;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use Yii;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;
use common\models\BuyPage as CommonModel;
use common\models\City;
use yii\helpers\ArrayHelper;

/**
 * Class BuyPage
 *
 * @package backend\modules\page\models
 * @author dimarychek
 */
class BuyPage extends CommonModel implements BackendModel
{
    /**
     * @var mixed attributes for imageUploader.
     */
    public $top_screen_background_image;

    /**
     * Temporary sign which used for saving images before model save
     * @var string
     */
    public $sign;

    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = security()->generateRandomString();
        }
    }

    public function rules()
    {
        return [
            [['city_id'], 'required'],
            [['city_id', 'form_top_square', 'manager_id', 'published', 'position'], 'integer'],
            [['top_screen_description', 'form_top_emails', 'form_bottom_description', 'form_bottom_email'], 'string'],
            [['top_screen_title', 'top_screen_sub_title', 'top_screen_btn', 'form_top_title', 'form_top_btn', 'form_top_onsubmit', 'advantages_title', 'advantages_title_dark', 'steps_title', 'form_bottom_title', 'form_bottom_onsubmit'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'targetClass' => City::className(), 'targetAttribute' => 'id'],
            [['sign'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'seo' => [
                'class' => MetaTagBehavior::className(),
            ],
        ]);
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getTitle()
    {
        return bt('Buy Page', 'app');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    [
                        'attribute' => 'city_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var Home $model */
                            return '<b>'.$model->city->label.'</b>';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(City::find()->orderBy('label')->asArray()->all(), 'id', 'label'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => \Yii::t('back/site-menu', 'Any city')],
                    ],
                    'top_screen_title',
                    // 'top_screen_description:ntext',
                    //'top_screen_btn',
                    //'form_top_title',
                    // 'form_top_emails:ntext',
                    //'form_top_square',
                    //'form_top_btn',
                    // 'form_top_onsubmit',
                    // 'advantages_title',
                    // 'steps_title',
                    // 'form_bottom_title',
                    // 'form_bottom_description:ntext',
                    // 'form_bottom_email:ntext',
                    // 'manager_id',
                    // 'form_bottom_onsubmit',
                    'published:boolean',
                    'position',
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                return StylingActionColumn::createPreviewUrl($url, $model, 'apartment-complex/resale');
                            }
                        ]
                    ]
                ];
            case 'view':
                return [
                    'id',
                    'city_id',
                    'top_screen_title',
                    [
                        'attribute' => 'top_screen_description',
                        'format' => 'html',
                    ],
                    'top_screen_btn',
                    'form_top_title',
                    [
                        'attribute' => 'form_top_emails',
                        'format' => 'html',
                    ],
                    'form_top_square',
                    'form_top_btn',
                    'form_top_onsubmit',
                    'advantages_title',
                    'steps_title',
                    'form_bottom_title',
                    [
                        'attribute' => 'form_bottom_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'form_bottom_email',
                        'format' => 'html',
                    ],
                    'manager_id',
                    'form_bottom_onsubmit',
                    'published:boolean',
                    'position',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new BuyPageSearch();
    }

    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                Yii::t('back/buy', 'General') => [
                    'city_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\City::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'position' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
                Yii::t('back/earth', 'Menu') => [
                    $this->getRelatedFormConfig()['sub_menu'],
                ],
                Yii::t('back/buy', 'Top screen') => [
                    'top_screen_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'top_screen_sub_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'top_screen_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'top_screen_description',
                        ]
                    ],
                    'top_screen_btn' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'top_screen_background_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'top_screen_background_image',
                            'saveAttribute' => CommonModel::SAVE_ATTRIBUTE_TOP_SCREEN_BACKGROUND_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
                Yii::t('back/buy', 'Advantages') => [
                    'advantages_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'advantages_title_dark' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> <i class="si si-grid"></i> '.Yii::t('back/apartment-building', 'advantages - blocks').'</div>',
                        'label' => false,
                    ],
                    $this->getRelatedFormConfig()['advantage_blocks'],
                ],
                Yii::t('back/buy', 'Steps') => [
                    'steps_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> <i class="si si-grid"></i> '.Yii::t('back/apartment-building', 'steps - blocks').'</div>',
                        'label' => false,
                    ],
                    $this->getRelatedFormConfig()['step_blocks'],
                ],
                Yii::t('back/buy', 'Forms') => [
                    'separator' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> '.Yii::t('back/apartment-building', 'Form - top').'</div>',
                        'label' => false,
                    ],
                    'form_top_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_top_emails' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'form_top_square' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'form_top_btn' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_top_onsubmit' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'separator2' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div class="form-config-separator"> '.Yii::t('back/apartment-building', 'Form - bottom').'</div>',
                        'label' => false,
                    ],
                    'form_bottom_title' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'form_bottom_description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'form_bottom_description',
                        ]
                    ],
                    'form_bottom_email' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'manager_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => Manager::getItems('id', 'fio'),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'form_bottom_onsubmit' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                ],
            ]
        ];

        return $config;
    }

    public function getRelatedFormConfig()
    {
        return [
            'advantage_blocks' => [
                'relation' => 'advantageBlocks'
            ],
            'step_blocks' => [
                'relation' => 'stepBlocks'
            ],
            'sub_menu' => [
                'relation' => 'subMenus'
            ],
        ];
    }

    public function getSubMenus()
    {
        return $this->hasMany(BuySubMenu::class, ['object_id' => 'id'])
            ->onCondition(['type' => BuySubMenu::TYPE_BUY_PAGE]);
    }

    public function getAdvantageBlocks()
    {
        return $this->hasMany(BuyAdvantage::class, ['object_id' => 'id'])
            ->onCondition(['type' => BuyAdvantage::BUY_ADVANTAGE]);
    }

    public function getStepBlocks()
    {
        return $this->hasMany(BuyStep::class, ['object_id' => 'id'])
            ->onCondition(['type' => BuyStep::BUY_STEP]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
