<?php

namespace backend\modules\page\models;

use backend\components\StylingActionColumn;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\behaviors\TimestampBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\models\ContactPage as CommonContactPage;
use backend\helpers\StdInput;
use backend\helpers\StdColumn;
use backend\components\BackendModel;
use backend\modules\contacts\models\City;
use backend\modules\block\models\block\ContactPageHowToGetStep;

/**
 * This is the model class for table "{{%contact_page}}".
 *
 * @property City $city
 * @property ContactMarker[] $markers
 * @property ContactPageHowToGetStep[] $howToGetStepBlocks
 */
class ContactPage extends CommonContactPage implements BackendModel
{
    public $sign;

    public static function tableName()
    {
        return '{{%contact_page}}';
    }

    public function rules()
    {
        return [
            [['city_id', 'map_zoom', 'published', 'view_count'], 'integer'],
            [['label'], 'required'],
            [['sign'], 'safe'],
            [['timetable'], 'string'],
            [['label', 'sales_phones', 'reception_phones', 'address', 'email', 'hot_to_get_label', 'forms_label', 'map_image'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'targetClass' => City::class, 'targetAttribute' => 'id'],
            [['map_zoom'], 'default', 'value' => 15],
            [['published'], 'default', 'value' => 1],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => bt('ID', 'contact-page'),
            'city_id' => bt('City ID', 'contact-page'),
            'label' => bt('Label', 'contact-page'),
            'sales_phones' => bt('Sales Phones', 'contact-page'),
            'reception_phones' => bt('Reception Phones', 'contact-page'),
            'address' => bt('Address', 'contact-page'),
            'email' => bt('Email', 'contact-page'),
            'timetable' => bt('Timetable', 'contact-page'),
            'hot_to_get_label' => bt('Hot To Get Label', 'contact-page'),
            'forms_label' => bt('Forms Label', 'contact-page'),
            'map_zoom' => bt('Map Zoom', 'contact-page'),
            'published' => bt('Published', 'contact-page'),
            'view_count' => bt('View count', 'contact-page'),
            'map_image' => bt('Map Image', 'contact-page'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'seo' => MetaTagBehavior::class,
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getTitle()
    {
        return bt('Contact page', 'contact-page');
    }

    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    StdColumn::serialColumn(),
                    // 'id',
                    StdColumn::city(),
                    'label',
                    StdColumn::nl2br('sales_phones'),
                    StdColumn::nl2br('reception_phones'),
                    'email:email',
                    // 'address:html',
                    // 'timetable:ntext',
                    // 'hot_to_get_label',
                    // 'forms_label',
                    // 'map_zoom',
                    StdColumn::viewCount(),
                    StdColumn::published(),
                    [
                        'class'    => StylingActionColumn::class,
                        'template' => '{update}{delete}{preview}',
                        'buttons'  => [
                            'preview' => function($url, $model) {
                                return StylingActionColumn::createPreviewUrl($url, $model, 'contact');
                            }
                        ]
                    ]
                ];
            case 'view':
                return [
                    'id',
                    StdColumn::city(),
                    'label',
                    'sales_phones',
                    'reception_phones',
                    'address',
                    'email:email',
                    'timetable:html',
                    'hot_to_get_label',
                    'forms_label',
                    'map_zoom',
                    'published:boolean',
                ];
            default:
                return [];
        }
    }

    public function getSearchModel()
    {
        return new ContactPageSearch();
    }

    public function getFormConfig()
    {
        return [
            'form-set' => [
                bt('General') => [
                    'city_id' => StdInput::citySelect(),
                    'label' => StdInput::text(),
                    'sales_phones' => StdInput::splittedTextArea(),
                    'reception_phones' => StdInput::splittedTextArea(),
                    'address' => StdInput::editor(),
                    'email' => StdInput::text(),
                    'timetable' => StdInput::editor(),
                    'published' => StdInput::checkbox(),
                ],
                bt('Map') => [
                    'map_zoom' => StdInput::text(),
                    obtain('markers', $this->getRelatedFormConfig()),
                    'map_image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png'],
                            'attribute' => 'map_image',
                            'saveAttribute' => \common\models\ContactPage::SAVE_ATTRIBUTE_MAP_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ]
                ],
                bt('Hot to get') => [
                    'hot_to_get_label' => StdInput::text(),
                    obtain('steps', $this->getRelatedFormConfig())
                ],
                bt('Forms') => [
                    'forms_label' => StdInput::text(),
                    obtain('forms', $this->getRelatedFormConfig())
                ],
            ],
        ];
    }

    public function getMarkers()
    {
        return $this->hasMany(ContactMarker::class, ['contact_page_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getForms()
    {
        return $this->hasMany(ContactForm::class, ['contact_page_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getHowToGetStepBlocks()
    {
        return $this->hasMany(ContactPageHowToGetStep::class, ['object_id' => 'id'])
            ->onCondition(['type' => ContactPageHowToGetStep::CONTACT_HOW_TO_GET_STEP]);
    }

    public function getRelatedFormConfig()
    {
        return [
            'markers' => ['relation' => 'markers'],
            'forms' => ['relation' => 'forms'],
            'steps' => ['relation' => 'howToGetStepBlocks']
        ];
    }
}
