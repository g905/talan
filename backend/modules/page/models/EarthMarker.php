<?php

namespace backend\modules\page\models;

use backend\components\FormBuilder;
use backend\components\ImperaviContent;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\models\EarthMarker as CommonEarthMarker;
use common\models\Earth as CommonEarth;

/**
 * Class EarthMarker
 * @package backend\modules\page\models
 */
class EarthMarker extends CommonEarthMarker implements BackendModel
{
    public function init()
    {
        $this->published = 1;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['marker_type'], 'required'],
            [['earth_id', 'marker_type'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['description'], 'string'],
            [['label'], 'string', 'max' => 255],
            [['published'], 'string', 'max' => 1],
            [['earth_id'], 'exist', 'targetClass' => CommonEarth::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEarth()
    {
        return $this->hasOne(Earth::className(), ['id' => 'earth_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Earth Marker');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    'earth_id',
                    'marker_type',
                    'lat',
                    'lng',
                    // 'description:ntext',
                    'published:boolean',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    'earth_id',
                    'marker_type',
                    'lat',
                    'lng',
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    'published:boolean',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new EarthMarkerSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $mapId = uniqid();
        $config = [
//            'label' => [
//                'type' => ActiveFormBuilder::INPUT_TEXT,
//                'options' => [
//                    'maxlength' => true,
//
//                ],
//            ],
//            'earth_id' => [
//                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
//                'items' => \common\models\Earth::getItems(),
//                'options' => [
//                    'prompt' => '',
//                ],
//            ],
            'marker_type' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => CommonEarthMarker::getMarkerTypeDictionary(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            [
                'label' => false,
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => '<div id="product_map_' . $mapId . '" class="product_map"></div>'
            ],
            'lat' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'options' => [
                    'maxlength' => true,
                    'readonly' => true,
                    'id' => 'product_map_lat_' . $mapId,
                    'class' => 'form-control',
                    'value' => $this->isNewRecord ? '56.8497600' : $this->lat
                ],
            ],
            'lng' => [
                'type' => FormBuilder::INPUT_TEXT_WITH_ICON,
                'hint' => Yii::t(
                    'back/product-storehouse',
                    'To fill parameters of latitude and longitude, just click on the map'
                ),
                'options' => [
                    'readonly' => true,
                    'maxlength' => true,
                    'id' => 'product_map_lng_' . $mapId,
                    'class' => 'form-control',
                    'value' => $this->isNewRecord ? '53.2044800' : $this->lng
                ],
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'description',
                ]
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }
}
