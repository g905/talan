<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\Home;

/**
 * HomeController implements the CRUD actions for Home model.
 */
class HomeController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Home::className();
    }
}
