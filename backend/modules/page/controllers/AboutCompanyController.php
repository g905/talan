<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\AboutCompany;

/**
 * AboutCompanyController implements the CRUD actions for AboutCompany model.
 */
class AboutCompanyController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return AboutCompany::className();
    }
}
