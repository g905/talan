<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\Catalog;

/**
 * CatalogController implements the CRUD actions for Catalog model.
 */
class CatalogController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Catalog::className();
    }
}
