<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\AboutCompanyMenu;

/**
 * AboutCompanyMenuController implements the CRUD actions for AboutCompanyMenu model.
 */
class AboutCompanyMenuController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return AboutCompanyMenu::className();
    }
}
