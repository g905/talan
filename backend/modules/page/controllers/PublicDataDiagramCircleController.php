<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\publicDataWidget\DiagramCircle;

/**
 * PublicDataDiagramCircleController implements the CRUD actions for PublicDataWidget model.
 */
class PublicDataDiagramCircleController extends BackendController
{
    public function getModelClass()
    {
        return DiagramCircle::class;
    }
}
