<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\AboutCompanyStat;

/**
 * AboutCompanyStatController implements the CRUD actions for AboutCompanyStat model.
 */
class AboutCompanyStatController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return AboutCompanyStat::className();
    }
}
