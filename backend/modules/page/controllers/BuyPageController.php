<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\BuyPage;

/**
 * BuyPageController implements the CRUD actions for BuyPage model.
 */
class BuyPageController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BuyPage::className();
    }
}
