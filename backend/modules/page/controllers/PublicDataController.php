<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\PublicData;

/**
 * PublicDataController implements the CRUD actions for PublicData model.
 */
class PublicDataController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PublicData::className();
    }
}
