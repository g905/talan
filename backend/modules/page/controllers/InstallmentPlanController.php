<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\InstallmentPlan;

/**
 * InstallmentPlanController implements the CRUD actions for InstallmentPlan model.
 */
class InstallmentPlanController extends BackendController
{
    public function getModelClass()
    {
        return InstallmentPlan::class;
    }
}
