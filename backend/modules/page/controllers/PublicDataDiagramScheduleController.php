<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\publicDataWidget\DiagramSchedule;

/**
 * PublicDataDiagramScheduleController implements the CRUD actions for PublicDataWidget model.
 */
class PublicDataDiagramScheduleController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return DiagramSchedule::class;
    }
}
