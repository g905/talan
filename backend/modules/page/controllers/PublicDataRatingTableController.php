<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\publicDataWidget\Rating;

/**
 * PublicDataDiagramScheduleController implements the CRUD actions for PublicDataWidget model.
 */
class PublicDataRatingTableController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Rating::class;
    }
}
