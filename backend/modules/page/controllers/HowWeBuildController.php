<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\HowWeBuild;

/**
 * HowWeBuildController implements the CRUD actions for HowWeBuild model.
 */
class HowWeBuildController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return HowWeBuild::className();
    }
}
