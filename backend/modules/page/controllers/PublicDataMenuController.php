<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\PublicDataMenu;

/**
 * PublicDataMenuController implements the CRUD actions for PublicDataMenu model.
 */
class PublicDataMenuController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PublicDataMenu::className();
    }
}
