<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\GuaranteePage;

/**
 * GuaranteePageController implements the CRUD actions for GuaranteePage model.
 */
class GuaranteePageController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return GuaranteePage::class;
    }
}
