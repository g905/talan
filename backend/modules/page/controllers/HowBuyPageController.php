<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\HowBuyPage;

/**
 * HowBuyPageController implements the CRUD actions for HowBuyPage model.
 */
class HowBuyPageController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return HowBuyPage::className();
    }
}
