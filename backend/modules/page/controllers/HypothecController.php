<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\Hypothec;

/**
 * HypothecController implements the CRUD actions for Hypothec model.
 */
class HypothecController extends BackendController
{
    public function getModelClass()
    {
        return Hypothec::class;
    }
}
