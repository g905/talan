<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\publicDataWidget\ServiceChartRow;

/**
 * PublicDataServiceChartRowController implements the CRUD actions for PublicDataWidget model.
 */
class PublicDataServiceChartRowController extends BackendController
{
    public function getModelClass()
    {
        return ServiceChartRow::class;
    }
}
