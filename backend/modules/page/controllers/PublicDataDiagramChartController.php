<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\publicDataWidget\DiagramDdu;

/**
 * PublicDataDiagramChartController implements the CRUD actions for PublicDataWidget model.
 */
class PublicDataDiagramChartController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return DiagramDdu::class;
    }
}
