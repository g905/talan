<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\Review;

/**
 * ReviewController implements the CRUD actions for Review model.
 */
class ReviewController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Review::className();
    }
}
