<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\publicDataWidget\CustomDdu;

/**
 * PublicDataDiagramScheduleController implements the CRUD actions for PublicDataWidget model.
 */
class PublicDataCustomDduController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return CustomDdu::class;
    }
}
