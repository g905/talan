<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\assets\MapAsset;
use backend\modules\page\models\Earth;

/**
 * EarthController implements the CRUD actions for Earth model.
 */
class EarthController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Earth::className();
    }

    public function actionUpdate($id)
    {
        MapAsset::register(view());
        return parent::actionUpdate($id);
    }

    public function actionCreate()
    {
        MapAsset::register(view());
        return parent::actionCreate();
    }
}
