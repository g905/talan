<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\EarthMarker;

/**
 * EarthMarkerController implements the CRUD actions for EarthMarker model.
 */
class EarthMarkerController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return EarthMarker::className();
    }
}
