<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\publicDataWidget\DiagramRow;
/**
 * PublicDataDiagramRowController implements the CRUD actions for PublicDataWidget model.
 */
class PublicDataDiagramRowController extends BackendController
{
    public function getModelClass()
    {
        return DiagramRow::class;
    }
}
