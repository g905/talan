<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\AboutCompanyValues;

/**
 * AboutCompanyValuesController implements the CRUD actions for AboutCompanyValues model.
 */
class AboutCompanyValuesController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return AboutCompanyValues::className();
    }
}
