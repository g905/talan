<?php

namespace backend\modules\page\controllers;

use backend\assets\MapAsset;
use backend\components\BackendController;
use backend\modules\page\models\ContactPage;

/**
 * ContactPageController implements the CRUD actions for ContactPage model.
 */
class ContactPageController extends BackendController
{
    public function getModelClass()
    {
        return ContactPage::class;
    }

    public function actionUpdate($id)
    {
        MapAsset::register(view());
        return parent::actionUpdate($id);
    }

    public function actionCreate()
    {
        MapAsset::register(view());
        return parent::actionCreate();
    }
}
