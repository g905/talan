<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\Realtor;

/**
 * RealtorController implements the CRUD actions for Realtor model.
 */
class RealtorController extends BackendController
{
    public function getModelClass()
    {
        return Realtor::class;
    }
}
