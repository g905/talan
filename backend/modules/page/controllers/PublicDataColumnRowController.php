<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\publicDataWidget\ColumnRow;

/**
 * PublicDataColumnRowController implements the CRUD actions for PublicDataWidget model.
 */
class PublicDataColumnRowController extends BackendController
{
    public function getModelClass()
    {
        return ColumnRow::class;
    }
}
