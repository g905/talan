<?php

namespace backend\modules\page\controllers;

use backend\components\BackendController;
use backend\modules\page\models\CustomPage;

/**
 * CustomPageController implements the CRUD actions for CustomPage model.
 */
class CustomPageController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return CustomPage::className();
    }
}
