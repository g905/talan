<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/select2_v4.0.3.css',
        'css/jquery.mCustomScrollbar.min_v2.8.1.css',
//        'css/new_admin.css',
//        'http://fonts.googleapis.com/css?family=Roboto:400,300&subset=latin,cyrillic',
//        'http://fonts.googleapis.com/css?family=Roboto+Condensed&subset=latin,cyrillic',
//        'css/site.css',
//        'css/new_admin_for_user.css',
        'css/oneui/oneui.css',
        'css/oneui/oneui_melon.css',
        'css/backend.css',
        '//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700',
    ];
    public $js = [
        'js/jquery.li-translit.js',
        'js/jquery.mCustomScrollbar.min_v2.8.1.js',
        'js/select2.full.min__v4.0.3.js',
//        'js/new_admin.js',
        'js/backend.js',
        'js/relatedFormLimit.js',
        'js/oneui/core/jquery.placeholder.min.js',
        'js/oneui/core/jquery.scrollLock.min.js',
        'js/oneui/core/jquery.slimscroll.min.js',
        'js/oneui/app.js',
        'js/oneui/oneui_melon.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'backend\assets\CropperAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
