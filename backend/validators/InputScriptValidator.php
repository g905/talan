<?php

namespace backend\validators;

use yii\validators\Validator;

/**
 * Class InputScriptValidator
 */
class InputScriptValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        if (substr($model->$attribute, 0, 7 ) !== "<script") {
            $this->addError($model, $attribute, 'Поле должно содержать "<script" вначале');
        }
        if (substr_compare($model->$attribute , 'script>', -strlen( 'script>')) !== 0) {
            $this->addError($model, $attribute, 'Поле должно содержать "script>" в конце');
        }
    }
}
