//--------- map js code -----------
$(document).on('click', '.nav-tabs li', function () {
    $(".product_map").each(function () {
        initMap($(this).attr('id'));
    });
});

$(document).on('click', '.button-add', function () {
    setTimeout(function () {
        initMap($(".product_map").last().attr('id'));
    }, 1000);
});

setTimeout(function () {
    $(".product_map").each(function () {
        initMap($(this).attr('id'));
    });
}, 1000);

function initMap(mapId) {
    if (mapId == undefined) {
        var lastMap = $('.product_map').last();
        var lastMapId = lastMap.attr('id');
    }
    else {
        lastMapId = mapId;
    }

    var uniqueId = lastMapId.replace('product_map_', '');

    var nearestLatInput = $('#product_map_lat_' + uniqueId);
    var nearestLongInput = $('#product_map_lng_' + uniqueId);

    var coordsFromBaseLat = parseFloat(nearestLatInput.val());
    var coordsFromBaseLon = parseFloat(nearestLongInput.val());
    var coordsFromBase = {lat: coordsFromBaseLat, lng: coordsFromBaseLon};

    var map = new google.maps.Map(document.getElementById(lastMapId), {
        zoom: 15,
        elementId: lastMapId,
        center: coordsFromBase,
        scrollwheel: false,
        styles: [{
            stylers: [{
                saturation: -100
            }]
        }]
    });

    var markers = [
        new google.maps.Marker({
            position: coordsFromBase,
            map: map
        })
    ];

    //try to get coordinates with stackoverflow
    google.maps.event.addListener(map, "click", function (event) {

        var lat = event.latLng.lat();
        var lng = event.latLng.lng();
        // populate yor box/field with lat, lng

        $('#product_map_lat_' + uniqueId).val(lat);
        $('#product_map_lng_' + uniqueId).val(lng);

        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }

        mark = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map
        });
        markers.push(mark);
    });
}