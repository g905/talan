(function($) {

	function AgencyComplex() {

	}

	AgencyComplex.prototype.init = function() {
		var $cityComplexList = $('#cityComplexList');
		$cityComplexList.select2({
			'placeholder': "Выберите ЖК",
			allowClear: true
		});
		$cityComplexList.val(null).trigger('change');


		this.initAddButton();
		this.initSaveButton();
		// инициализируем все кнопки удалить
		this.initRemoveComplexBtn();

	}

	AgencyComplex.prototype.initRemoveComplexBtn = function($buttons) {

		if(!$buttons || !$buttons.length) {
			$buttons = $('.cctItem .cctiRemove');
		}

		$buttons.on('click', function() {
			var $currBtn = $(this)
			,	$complexItem = $currBtn.closest('.cctItem')
			,	complexId = $complexItem.attr('data-id')
			;

			if(!$currBtn.attr('disabled')) {
				$currBtn.attr('disabled', 'disabled');

				if(complexId) {
					var $selOption = $('#cityComplexList option[value="' + complexId + '"]')
					,	dataSelOpt = $selOption.data()
					;

					$selOption.removeAttr('disabled');
					if(dataSelOpt && dataSelOpt.data) {
						dataSelOpt.data.disabled = false;
					}
					$complexItem.remove();

					// если элементов - нет, значит нужно открыть доступ к
					if(!$('.cityComplexTable .cctItem').length) {
						$('#addCityComplex').removeAttr('disabled');
					}
				}
			}
			$currBtn.removeAttr('disabled');
		})
	}

	AgencyComplex.prototype.initAddButton = function() {
		var selfAc = this
		,	$addButton = $('#addCityComplex')
		;
		$addButton.on('click', function() {

			var $cloneItem = $('<div class="cctItem">')
			,	$cloneNameDiv = $('<div class="cctiName">')
			,	$cloneRemoveBl = $('<div class="cctiRemove"><i class="fa fa-times" aria-hidden="true"></i></div>')
			,	$selectSel = $('#cityComplexList')
			,	$selectedOption = $('#cityComplexList option:selected')
			,	$addButton = $(this)
			,	complexId = $selectSel.val()
			;
			$addButton.attr('disabled', 'disabled');

			if(complexId) {
				// добавим элемент в таблицу
				$cloneNameDiv.text($selectedOption.text());
				$cloneItem.attr('data-id', complexId);
				$cloneItem.append($cloneNameDiv);
				$cloneItem.append($cloneRemoveBl);

				// заблокируем опцию в Селекте
				$selectedOption.attr('disabled', 'disabled');
				// в виджете Select2
				$selectSel.select2('data')[0].disabled = true;

				$selectSel.val(null);
				$selectSel.trigger('change');

				$('.cityComplexTable').append($cloneItem);
				//
				selfAc.initRemoveComplexBtn($cloneRemoveBl);
			} else {
				alert('Выберите комплекс');
				$addButton.removeAttr('disabled');
			}
		});
	}

	AgencyComplex.prototype.initSaveButton = function() {

		var $saveButton = $('#saveSelectedComplex')
		;
		$saveButton.on('click', function(event) {
			if(!$saveButton.attr('disabled')) {
				$saveButton.attr('disabled', 'disabled');

				var $complexItems = $('.cityComplexTable .cctItem')
				,	complexIds = []
				,	agencyId = $('#agencyId').val()
				,	ajaxData = {
						'agencyId': agencyId,
					}
				;
				$complexItems.each(function(ind1, item1) {
					var $currItem = $(item1)
					,	complexOneId = $currItem.attr('data-id')
					;
					if(complexOneId && !isNaN(complexOneId)) {
						complexIds.push(complexOneId);
					}
				});
				ajaxData['complexIds'] = complexIds;

				$('.iframeContainer').addClass('tlnDisplNone');
				$.ajax({
					'url': '/api/api-agency/save-complex-to-agency',
					'dataType': 'json',
					'method': 'POST',
					'data': ajaxData,
				})
				.done(function(resp) {
					if(resp && resp.code == 1) {
						if(resp.count) {
							$('.iframeContainer').removeClass('tlnDisplNone');
						} else {
							alert('Для отображения кода - необходимо добавить ЖК и сохранить');
						}
					} else {
						alert('Возникла ошибка');
					}
				})
				.always(function(x1, x2, x3) {
					$saveButton.removeAttr('disabled');
				});
			}
		});
	}

	var acObj = new AgencyComplex();
	$(document).ready(function() {
		acObj.init();
	});

}) (jQuery);