function initCheckbox() {
    $('input[type="checkbox"]').each(function () {
        var that = $(this);
        if (!that.parents('.checkbox').length) {
           // that.parent('label').wrap('<div class="checkbox"></div>')
        }
    });
}

$('form').on('afterValidate', function () {
    checkTabErrors();
});

function checkTabErrors() {
    var tabs = $('.tab-content .tab-pane');
    if (tabs.length) {
        tabs.each(function (index, el) {
            var that = $(el);
            that.children().each(function () {
                if ($(this).hasClass('has-error')) {
                    var id = that.attr('id');
                    $('a[href="#' + id + '"]').addClass('tab-error');
                }
            })
            if (that.find('.has-error').length) {
                var id = that.attr('id');
                $('a[href="#' + id + '"]').addClass('tab-error');
            }
        });
    }
}
